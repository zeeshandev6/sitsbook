<?php
    
	include('common/connection.php');
	include 'common/config.php';
    include 'common/classes/md.php';
	include('common/classes/items.php');
	include('common/classes/measure.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/godown-details.php');
    include 'common/classes/mobile-purchase-details.php';
	
    //Permission
    if(!in_array('demand-report',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

	$objItems                  = new Items;
	$objItemsCategory          = new itemCategory;
	$objDepartments            = new Departments;
	$objMeasures               = new Measures;
    $objMobilePurchaseDetails  = new ScanPurchaseDetails;
	$objGodownDetails          = new GodownDetails;
    $objConfigs                = new Configs();

    $company_name_list = $objItems->getCompanyNameList();

    $company_name = isset($_GET['c'])?mysql_real_escape_string($_GET['c']):"";

    $stocks = $objItems->getDemandReport($company_name);
?>
<!DOCTYPE html 
>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" />
    <style>
        .caption{
            padding: 5px;
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>

    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
            $("select").selectpicker();
            $(".printThis").click(function(){
                var MaxHeight = 884;
                var RunningHeight = 0;
                var PageNo = 1;
                //Sum Table Rows (tr) height Count Number Of pages
                $('table.tableBreak>tbody>tr').each(function(){
                    
                    if(PageNo == 1){
                        MaxHeight = 750;
                    }
                    
                    if(PageNo != 1){
                        MaxHeight = 820;
                    }
                    
                    if (RunningHeight + $(this).height() > MaxHeight){
                        RunningHeight = 0;
                        PageNo += 1;
                    }
                    
                    RunningHeight += $(this).height();
                    //store page number in attribute of tr
                    $(this).attr("data-page-no", PageNo);
                });
                //Store Table thead/tfoot html to a variable
                var tableHeader = $(".tHeader").html();
                var tableFooter = $(".tableFooter").html();
                var repoDate = $(".repoDate").text();
                //remove previous thead/tfoot
                $(".tHeader").remove();
                $(".tableFooter").remove();
                $(".repoDate").remove();
                //Append .tablePage Div containing Tables with data.
                for(i = 1; i <= PageNo; i++){
                    
                    if(i == 1){
                        MaxHeight = 750;
                    }
                    
                    if(i != 1){
                        MaxHeight = 820;
                    }
                    
                    $('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
                    //get trs by pagenumber stored in attribute
                    var rows = $('table tr[data-page-no="' + i + '"]');
                    $('#Table' + i).find("thead").append(tableHeader);
                    $('#Table' + i).find("tbody").append(rows);
                }
                $(".tablePage").each(function(i,e){
                    $(this).prepend($(".pageHeader").first().clone());
                });
                $(".pageHeader").first().remove();
                $(".newTable").last().append(tableFooter);
                $('table.tableBreak').remove();
                $(".printDiv").printThis({
                  debug: false,               
                  importCSS: false,            
                  printContainer: true,
                  loadCSS: 'resource/css/reports.css',
                  pageTitle: "Sitsbook.com",
                  removeInline: false,
                  printDelay: 500,
                  header: null
              });
            });
        });
    </script>
</head>
      
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p> Inventory Demand Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <div class="caption">Company Name :</div>
                        <div class="field">
                            <form>
                                <div class="btn-group">
                                    <select name="c" class="form-control show-tick pull-left btn btn-default" data-width="200">
                                        <option value="">All</option>
                                        <?php
                                            if(mysql_num_rows($company_name_list)){
                                                while($company = mysql_fetch_assoc($company_name_list)){
                                                    ?><option value="<?php echo strtolower($company['COMPANY']); ?>"><?php echo $company['COMPANY']; ?></option><?php
                                                }
                                            }
                                        ?>
                                    </select>
                                    <button type="submit" class="btn btn-default"> <i class="fa fa-arrow-right"></i> </button>
                                </div>
                            </form>
                        </div>
                        <div class="clear"></div>
                        <div class="button printThis pull-right"><i class="fa fa-print"></i> Print </div>
                    </div>
                    <div class="clear"></div>
                    <div id="form" class="printDiv">
                        <div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                            <p style="text-align: left;font-size:20px;margin: 0px;padding:0px;">Inventory Demand List</p>
                            <div class="clear" style="height:20px;"></div>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                        </div>
                        <table cellspacing="0" class="tableBreak" >
                            <thead class="tHeader">
                                <tr>
                                   <th width="5%" style="text-align:center">Sr#</th>
                                   <th width="10%" style="text-align:center;">Company</th>
                                   <th width="10%" style="text-align:center;">Category</th>
                                   <th width="15%" style="text-align:center;">Item</th>
                                   <th width="10%" style="text-align:center;">Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $counter = 1;
                            if(mysql_num_rows($stocks)){
                                while($row = mysql_fetch_assoc($stocks)){
                                    $category = $objItemsCategory->getTitle($row['ITEM_CATG_ID']);
                                    $row['STOCK_QTY'] += $objGodownDetails->getStockSum($row['ID']);
                                    if($row['INV_TYPE']=='B'){
                                        $row['STOCK_QTY'] += $objMobilePurchaseDetails->get_item_stock($row['ID']);
                                    }
                                    if($row['STOCK_QTY'] == 0){
                                        continue;
                                    }
                            ?>
                                <tr id="recordPanel" data-cat-id="<?php echo $row['ITEM_CATG_ID']; ?>">
                                    <td style="text-align:center"><?php echo $counter; ?></td>
                                    <td style="text-align:center"><?php echo $row['COMPANY']; ?></td>
                                    <td style="text-align:center"><?php echo $category; ?></td>
                                    <td style="text-align:center !important;padding-left:5px;"><?php echo $row['NAME']; ?></td>
                                    <td style="text-align:center"><?php echo $row['STOCK_QTY']; ?></td>
                                </tr>
                            <?php
                                    $counter++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script><?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?> tab('2', '1', '2'); <?php } ?></script>
