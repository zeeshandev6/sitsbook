<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/ledger_report.php';
	include 'common/classes/accounts.php';
	
	//Permission
	if(!in_array('invoice-ledger',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--
	
	$objAccounts = new ChartOfAccounts;
	$objLedgerReport = new ledgerReport;
	$accountsList = $objLedgerReport->getAccountsList();
	
	if(isset($_SESSION['gl_code'])){
		$postedAccCode  = $_SESSION['gl_code'];
		$postedfromDate = '';
		$postedtoDate   = date('Y-m-d',strtotime($_SESSION['gl_date']));
		$postedAccTitle  = $objAccounts->getAccountTitleByCode($postedAccCode); 
		if($postedAccCode != ''){
			$voucherDetailsList = $objLedgerReport->getVoucherDetailListByDateRange($postedfromDate,$postedtoDate,$postedAccCode);
			$dateForBalance = date('Y-m-d',strtotime($postedfromDate." - 1 days"));
			$ledgerOpeningBalance = $objLedgerReport->getLedgerOpeningBalance($dateForBalance,$postedAccCode);
			$account_type = substr($postedAccCode,0,2);
			if($ledgerOpeningBalance >= 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'DR';
			}elseif($ledgerOpeningBalance < 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'CR';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance >= 0){
				$ledgerOpeningBalanceType = 'CR';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance < 0){
				$ledgerOpeningBalanceType = 'DR';
			}
			$ledgerOpeningBalance = str_replace('-','',$ledgerOpeningBalance);
		}else{
			$message = "Account was not selected !";
		}
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="resource/css/bootstrap-select.css" rel="stylesheet">
        <style>
			html{
			}
			.ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized{
				float:left;
				width: 152px;
				padding-left: 5px;
				border: 1px solid #CCC;
				height:30px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;
				border:1px solid #DDDDDD;
				
				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;
				
				-moz-border-radius-topleft:3px; 
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;
				
				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;
				
				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			}
			.input_sized:hover{
				border-color: #9ecaed;
				box-shadow: 0 0 2px #9ecaed;
			}
		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
			$(function(){
				$( document ).tooltip({position:{'my':'left center','at':'right center'}});
			});
			$(window).on('load', function () {
				$('.selectpicker').selectpicker();
				$(".printThis").click(function(){
					var pageHeaderHeight = $(".pageHeader").height();
					var MaxHeight = 770;
					var RunningHeight = 0;
					var PageNo = 1;
					var MaxHeight_after = 0;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if(PageNo == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						if($(this).height() > 29){
							
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var repoDate = $(".repoGen").text();
					$(".repoGen").remove();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						if(i == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+" '><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					
					$('table.tableBreak').remove();
					$(".printTable").printThis({
					  debug: false,
					  importCSS: false,
					  printContainer: true,
					  loadCSS: 'resource/css/reports.css',
					  pageTitle: "SIT Solution",
					  removeInline: false,
					  printDelay: 500,
					  header: null
				  });
				});
			});
		</script>
      	<script type = "text/javascript" src = "resource/scripts/reports.configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
   
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
      	
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >Party Ledger Reporting</p>
                            <div class="clear"></div>
                  		</div><!-- End .content-box-header -->  
            		</div> <!-- End .content-box-content -->	
<?php 
                    	if(isset($voucherDetailsList)){
?>
        			<div class="content-box-content">
                    	<span style="float:right;"><button class="button printThis">Print</button></span>
                		<div id="bodyTab" class='printTable'   style="margin: 0 auto;">
                            <div style="text-align:left;margin-bottom:20px;" class="pageHeader">
                                <p style="font-size:18px;padding:0;text-align:center;"><?php echo $postedAccTitle; ?> (<?php echo $postedAccCode; ?>) </p>
                                <p style="font-size:12px;padding:0;text-align:center;">
                                		General Ledger Details 
                                        From : <u> <?php echo date('d-m-Y',strtotime($postedfromDate)); ?></u> to : <u> <?php echo date('d-m-Y',strtotime($postedtoDate)); ?></u>
                                </p>
                                <p style="float:left; margin-left:0px; font-size:14px" class="repoGen">
                                		Report generated on : <?php echo date('d-m-Y'); ?>
                                </p>
                            </div>
                            <div style="clear:both; height:1;"></div>
                            <table style="margin:0 10px; width:98%;" cellspacing="0" class="tableBreak">
                         
                                <thead class="tHeader">
                                    <tr>
                                       <th width="8%"  colspan="2" style="text-align:center;border:1px solid #ddd;" >Ref</th>
                                       <th width="10%" style="border:1px solid #ddd;"  >EntryDate</th>
                                       <th width="45%" style="border:1px solid #ddd;" >Description</th>
                                       <th width="10%" style="text-align:center;border:1px solid #ddd;">Debit</th>
                                       <th width="10%" style="text-align:center;border:1px solid #ddd;">Credit</th>
                                       <th width="15%" style="text-align:center;border:1px solid #ddd;">Balance</th>
                                    </tr>
                                </thead>
                         
                                <tbody>
<?php
						if(mysql_num_rows($voucherDetailsList)){
							$debitTotal = 0;
							$creditTotal = 0;
?>
                                	<tr>
                                    	<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
                                        <td style="text-align:right;border-right:1px solid #E5E5E5;border-left:none">Opening Balance:</td>
                                        <td colspan="2" style="border-right:none;border-left:none"></td>
                                        <td style="border-left:none;border-right:none;text-align:right">
											<?php echo number_format($ledgerOpeningBalance,2); ?>
                                            <?php echo strtoupper($ledgerOpeningBalanceType); ?>
                                        </td>
                                    </tr>
<?php 
							$final_column = $ledgerOpeningBalance;
							$final_column_type = '';
							 while($row = mysql_fetch_array($voucherDetailsList)){
								 $voucherType = 'JV';
								 $sale_id = $objLedgerReport->getSaleId($row['VOUCHER_ID']);
								 if($sale_id != '' && $sale_id != 0){
								 	$row['NARRATION'] = '<a target="_blank" href="sales-invoice.php?id='.$sale_id.'">'.$row['NARRATION'].'</a>';
								 }
								 $debitTotal += ($row['TRANSACTION_TYPE']=='Dr')?$row['AMOUNT']:0;
								 $creditTotal += ($row['TRANSACTION_TYPE']=='Cr')?$row['AMOUNT']:0;
								 if($account_type == '01' || $account_type == '03'){
								 	if($ledgerOpeningBalanceType=='CR'&&$row['TRANSACTION_TYPE']=='Cr'){
								 		$final_column += $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='CR'&&$row['TRANSACTION_TYPE']=='Dr'){
								 		$final_column -= $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='DR'&&$row['TRANSACTION_TYPE']=='Dr'){
								 		$final_column += $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='DR'&&$row['TRANSACTION_TYPE']=='Cr'){
								 		$final_column -= $row['AMOUNT'];
								 	}
								 	if($final_column >= 0){
										$final_column_type = "DR";
									}else{
										$final_column_type = "CR";
									}
								 }elseif($account_type == '02' || $account_type == '04' || $account_type == '05'){
								 	if($ledgerOpeningBalanceType=='CR'&&$row['TRANSACTION_TYPE']=='Cr'){
								 		$final_column += $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='CR'&&$row['TRANSACTION_TYPE']=='Dr'){
								 		$final_column -= $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='DR'&&$row['TRANSACTION_TYPE']=='Dr'){
								 		$final_column += $row['AMOUNT'];
								 	}elseif($ledgerOpeningBalanceType=='DR'&&$row['TRANSACTION_TYPE']=='Cr'){
								 		$final_column -= $row['AMOUNT'];
								 	}
								 	if($final_column <= 0){
										$final_column_type = "DR";
									}else{
										$final_column_type = "CR";
									}
								 }
?>
                                    <tr>
                                       <td style="text-align:center"><?php echo $voucherType; ?>#</td>
                                       <td style="text-align:center"><?php echo $row['VOUCHER_NO']; ?></td>
                                       <td align="left"><?php echo date('d-m-Y',strtotime($row['VOUCHER_DATE'])); ?></td>
                                       <td align="left"><?php echo $row['NARRATION']; ?></td>
                                       <td style="text-align:right" class="debitColumn"><?php echo ($row['TRANSACTION_TYPE']=='Dr')?number_format($row['AMOUNT'],2):""; ?></td>
                                       <td style="text-align:right;"  class="creditColumn"><?php echo ($row['TRANSACTION_TYPE']=='Cr')?number_format($row['AMOUNT'],2):""; ?></td>
                                       <td style="text-align:right;"><?=number_format($final_column,2)?> <?=$final_column_type;?></td>
                                    </tr>
<?php
							}
						}else{
?>
									<tr>
                                    	<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
                                        <td colspan="3" style="text-align:right;border-right:1px solid #666;;border-left:none">Opening Balance:</td>
                                    	<td style="border-left:none;border-right:none;text-align:right">
											<?php echo number_format($ledgerOpeningBalance,2); ?>
                                            <?php echo strtoupper($ledgerOpeningBalanceType); ?>
                                        </td>
                                    </tr>
									<tr>
										<th colspan="10" style="text-align:center;">Found 0 Enteries</th>
                                    </tr>
                                    <tr>
                                    	<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
                                        <td colspan="3" style="text-align:right;border-right:1px solid #666;;border-left:none">Closing Balance:</td>
                                    	<td style="border-left:none;border-right:none;text-align:right">
											<?php echo number_format($ledgerOpeningBalance,2); ?>
                                            <?php echo strtoupper($ledgerOpeningBalanceType); ?>
                                        </td>
                                    </tr>
<?php
						}
								if(mysql_num_rows($voucherDetailsList)){//second numrows 
?>
                                    <tr style="background:none">
                                    	<td colspan="4" style="text-align:right;border:1px solid #e5e5e5;">Period Total:</td>
                                        <td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($debitTotal,2); ?></td>
                                        <td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($creditTotal,2); ?></td>
<?php
										if($account_type == '01' || $account_type == '03'){
											$difference = $debitTotal - $creditTotal;
											if($difference >= 0){
												$defferenceType = 'DR';
											}else{
												$defferenceType = 'CR';
											}
											$difference = str_replace('-','',$difference);
											if($defferenceType == 'DR'&&$ledgerOpeningBalanceType=='DR'){
												$closingBalance = $ledgerOpeningBalance + $difference;
												$closingBalanceType = "DR";
											}elseif($defferenceType == 'CR'&&$ledgerOpeningBalanceType=='CR'){
												$closingBalance = $ledgerOpeningBalance + $difference;
												$closingBalanceType = "CR";
											}elseif($defferenceType == 'CR'&&$ledgerOpeningBalanceType=='DR'){
												$closingBalance = $ledgerOpeningBalance - $difference;
												if($closingBalance >= 0){
													$closingBalanceType = "DR";
												}else{
													$closingBalanceType = "CR";
												}
											}elseif($defferenceType == 'DR'&&$ledgerOpeningBalanceType=='CR'){
												$closingBalance = $ledgerOpeningBalance - $difference;
												if($closingBalance >= 0){
													$closingBalanceType = "DR";
												}else{
													$closingBalanceType = "CR";
												}
											}
										}elseif($account_type == '02' || $account_type == '04' || $account_type == '05'){
											$difference = $debitTotal - $creditTotal;
											if($difference >= 0){
												$defferenceType = 'CR';
											}else{
												$defferenceType = 'DR';
											}
											if($defferenceType == 'DR'&&$ledgerOpeningBalanceType=='DR'){
												$closingBalance = $ledgerOpeningBalance + $difference;
												$closingBalanceType = "DR";
											}elseif($defferenceType == 'CR'&&$ledgerOpeningBalanceType=='CR'){
												$closingBalance = $ledgerOpeningBalance + $difference;
												$closingBalanceType = "CR";
											}elseif($defferenceType == 'CR'&&$ledgerOpeningBalanceType=='DR'){
												$closingBalance = $ledgerOpeningBalance - $difference;
												if($closingBalance <= 0){
													$closingBalanceType = "DR";
												}else{
													$closingBalanceType = "CR";
												}
											}elseif($defferenceType == 'DR'&&$ledgerOpeningBalanceType=='CR'){
												$closingBalance = $ledgerOpeningBalance - $difference;
												if($closingBalance <= 0){
													$closingBalanceType = "DR";
												}else{
													$closingBalanceType = "CR";
												}
											}
										}

?>
                                        <td style="border-left:none;text-align:right;background-color:#eee;"></td>
                                    </tr>
                                    <tr>
                                    	<td colspan="4" style="text-align:right;">Closing Balance:</td>
                                        <td colspan="2" style=""></td>
                                        <td style="text-align:right" class="closingBalance">
                                        	<?php echo number_format($closingBalance,2); ?>
                                            <?php echo strtoupper($closingBalanceType); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                    	</div><!--bodyTab2-->
						<?php
								}//second numrows end
                           	}
                        ?>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
        	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(window).load(function(){
		$("button.dropdown-toggle").focus();
		$("ul.selectpicker li a").click(function(){
			var accountTitle = $(this).children("span.text").text();
			if(accountTitle!==""){
				$("select.selectpicker option").each(function(index, element){
					if($(this).text()==accountTitle){
						var accountsCode = $(this).val();
						$("input.insertAccTitle").val(accountTitle);
						$("input.insertAccCode").val(accountsCode);
						setTimeout(function(){
							$(".datepicker").first().focus();
							//$("input[type='submit']").focus();
						},300);
					}
				});
			}else{
				$("input.insertAccTitle").val("");
				$("input.insertAccCode").val("");
			}
		});
		$(".datepicker").first().setFocusTo(".datepicker:last");
		$(".datepicker").last().setFocusTo("input[type='submit']");
	});
</script>