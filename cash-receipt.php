<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/accounts.php';
	include 'common/classes/customers.php';
	include 'common/classes/suppliers.php';
	include 'common/classes/sms_templates.php';
	include 'common/classes/sitsbook_sms_api.php';

	$objChartOfAccounts = new ChartOfAccounts();
	$objJournalVoucher  = new JournalVoucher();
	$objUserAccounts    = new UserAccounts();
	$objCustomers 		 	= new Customers();
	$objSuppliers 		 	= new suppliers();
	$objSmsTemplates    = new SmsTemplates();
	$objSitsbookSmsApi  = new SitsbookSmsApi();
	$objConfigs 				= new Configs();

	//Permission
	if(!in_array('cash-receipt',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$sms_config 					= $objConfigs->get_config('SMS');

	if(isset($_POST['sms_voucher_id'])){
		if($sms_config == 'N'){
			exit();
		}
		$voucher_id 			= (int)mysql_real_escape_string($_POST['sms_voucher_id']);
		$voucher_date 		= $objJournalVoucher->getVoucherDate($voucher_id);
		$voucher_date     = date("d-m-Y",strtotime($voucher_date));
		$accounts_for_sms =  $objJournalVoucher->getVoucherCustomerSuppliersList($voucher_id);

		$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
		$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');

		$mobile_numbers   = array();

		if(mysql_num_rows($accounts_for_sms)){
			while($acc_row = mysql_fetch_assoc($accounts_for_sms)){
				$mobile = '';
				if(substr($acc_row['ACCOUNT_CODE'],0,6) == '010104'){
					$mobile = $objCustomers->getMobileIfActive($acc_row['ACCOUNT_CODE']);
				}
				if(substr($acc_row['ACCOUNT_CODE'],0,6) == '040101'){
					$mobile = $objSuppliers->getMobileIfActive($acc_row['ACCOUNT_CODE']);
				}
				if($mobile!=''){
					$mobile_numbers[$mobile] = $acc_row['AMOUNT'];
				}
			}
		}
		$sms_template = $objSmsTemplates->get_template(1);
		foreach($mobile_numbers as $mobile_number => $amount){
			$sms_body = $sms_template['SMS_BODY'];

			$sms_body = str_replace("[DATE]",$voucher_date,$sms_body);
			$sms_body = str_replace("[AMOUNT]",$amount,$sms_body);

			$no_of_sms = 0;
			$acc_id  	 = 0;
			$reply = $objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
			echo $reply;
		}
		exit();
	}

	$default_cash_in_hand = '0101010001';
	$newJvNumber 					= $objJournalVoucher->genJvNumber();
	$newCrNumber 					= $objJournalVoucher->genTypeNumber('CR');
	$voucher_type 				= 'CR';

	$cash_accounts_list 	= $objChartOfAccounts->getAccountByCatAccCode('010101');

	if(isset($_GET['jid'])){
		$jv_id 			= mysql_real_escape_string($_GET['jid']);
		$jVoucher 	    = $objJournalVoucher->getRecordDetailsVoucher($jv_id);
		$voucher_type   = $jVoucher['VOUCHER_TYPE'];
		$jv_detail_list = $objJournalVoucher->getVoucherDetailList($jv_id);
	}
	$order_taker_selected = '';
	$salesman_id_selected = '';

	if($distSalesAddon == 'Y'){
		$order_taker_selected = ($sessionUserDetails['DESIGNATION_TYPE']=='O')?$sessionUserDetails['ID']:"";
		$salesman_id_selected = ($sessionUserDetails['DESIGNATION_TYPE']=='S')?$sessionUserDetails['ID']:"";
	}
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
				<style media="screen">
					th{
						font-family: "Roboto" !important;
						font-weight: bold !important;
					}
					td{
						padding: 8px !important;
					}
				</style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/cash.receipt.configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
          var refresh_accounts = function(){
            $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(html){
              var options = $(html).find('select.accCodeSelector').html();
              $("select.accCodeSelector").html(options);
              $("select.accCodeSelector").selectpicker("refresh");
            });
          };
          $(function(){
            $(".size_caption_alt").css({'width':'100px'}).nextAll(".size_caption_alt").css({'margin-left':'0px'});
            $(".field").css({'width':'160px'});
          });
        </script>
   </head>
   <body>
      <input type="hidden" class="cash-in-hand" value="<?php echo $cash_in_hand_acc_code; ?>" />
			<input type="hidden" class="sms-config" value="<?php echo $sms_config; ?>">
      <div id = "body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php"); ?>
        </div> <!-- End #sidebar -->
      	<div id = "bodyWrapper">
        	<div class = "content-box-top" style="overflow:visible;">
            	<div class="summery_body">
               		<div class="content-box-header">
                  		<p >Cash Receipt Panel</p>
                      <div class="clear"></div>
               		</div><!-- End .content-box-header -->
<?php
			if($voucher_type == 'CR'){
?>
              		<div id="bodyTab1">
                      <div id="form">
                           <input type="hidden" name="jv_id" class="jv_id" value="<?php echo isset($jVoucher)?$jv_id:"0"; ?>" />
                            <div class="caption">CR#</div>
                            <div class="field">
                               <input class="form-control" name="crNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['TYPE_NO']:$newCrNumber; ?>" style="width:160px;" />
                            </div>
                            <div class="caption size_caption_alt">Voucher#</div>
                            <div class="field">
                               <input class="form-control" name="jvNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['VOUCHER_NO']:$newJvNumber; ?>" style="width:152px;" />
                            </div>
                            <div class="caption size_caption_alt">Date</div>
                            <div class="field">
                               <input class="form-control datepicker" type="text" name="jvDate" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['VOUCHER_DATE'])):""; ?>" style="width: 150px;" />
                            </div>
														<div class="clear"></div>

                            <?php if(in_array('cash-visible',$permissionz) || $admin == true){ ?>
                            <div class="caption">Cash</div>
                            <div class="field" style="width: 150px;">
                              <input class="form-control cashBalance" type="text" value="" />
                            </div>
                            <?php } ?>
														<?php if($embProdAddon=='Y' && ($sessionUserDetails['ALLOW_OTHER_CASH'] == 'Y' || $admin)){ ?>
														<div class="caption" style="width: 100px;">Cash A/c </div>
                            <div class="field">
															<select class="cash_accounts_list" name="cash_accounts_list">
																<?php
																	if(mysql_num_rows($cash_accounts_list)){
																		while($cash_acc=mysql_fetch_assoc($cash_accounts_list)){
																			$cash_selected=($cash_acc['ACC_CODE'] == $sessionUserDetails['CASH_ACC_CODE'])?"selected":"";
																			?>
																			<option value="<?php echo $cash_acc['ACC_CODE']; ?>" <?php echo $cash_selected; ?>><?php echo $cash_acc['ACC_TITLE']; ?></option>
																			<?php
																		}
																	}
																?>
															</select>
                            </div>
														<?php } ?>
                            <div class="clear"></div>
                            <div style="display:none;">
                              <div class="caption size_caption_alt" style="width:100px;">Reference</div>
                              <div class="field" style="width:160px;">
                                 <input class="form-control" style="width:160px;" type="text" name="jvReference" value="<?php echo (isset($jVoucher))?$jVoucher['REFERENCE']:''; ?>" />
                              </div>
                              <div class="caption size_caption_alt" style="margin-left:0px;width:150px;">Reference Date</div>
                              <div class="field" style="width:150px">
                                 <input class="form-control datepicker" type="text" name="refDate" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['REF_DATE'])):''; ?>"  style="width: 150px;" />
  							              </div>
                            </div>
                            <div class="clear" style="height:10px;"></div>
<?php
			if($distSalesAddon == 'Y'){
                      $sales_man_arr=array();
                      $users_list=$objUserAccounts->getActiveList();
                      if(mysql_num_rows($users_list)){
                        while($user=mysql_fetch_assoc($users_list)){
                          if($user['DESIGNATION_TYPE'] == ''){
                            //$user['DESIGNATION_TYPE']='S';
                            continue;
                          }
                          if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
                            $sales_man_arr[$user['DESIGNATION_TYPE']]=array();
                          }
                          $sales_man_arr[$user['DESIGNATION_TYPE']][]=$user;
                        }
                      }
 ?>
			                      <div class="caption" >OrderTaker</div>
			                      <div class="field" >
			                        <select class="order_taker" name="order_taker_id">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['O'])){
			                            foreach ($sales_man_arr['O'] as $key => $user){
			                                $user_selected=(isset($jVoucher)&&$jVoucher['ORDER_TAKER_ID']==$user['ID'] || $order_taker_selected == $user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>

			                      <div class="caption" style="width:370px;">Salesman</div>
			                      <div class="field" style="width:270px;position:relative;">
			                        <select class="user_id" name="salesman_id">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['S'])){
			                            foreach ($sales_man_arr['S'] as $key => $user){
			                                $user_selected=(isset($jVoucher)&&$jVoucher['SALESMAN_ID']==$user['ID']  || $salesman_id_selected == $user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>
			                      <div class="clear"></div>
														<hr />

<?php } ?>
														<div class="clear" style="height:10px;"></div>
                            <fieldset class="panel panel-default" style="width:90%;margin:0px auto;border-radius: 0px !important;">
                            	<!--<span class="legend">Journal Voucher</span>-->
                                <table style="width:100%;" align="left" class="voucherDrCrTable">
                                    <thead>
                                        <th class="bg-color">
                                          <span style="float:left;margin:10px;">G/L Account</span>
                                          <a href="#" onclick="refresh_accounts();" class="btn btn-default btn-xs pull-right" ><i class="fa fa-refresh" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                          <a href="accounts-management.php" target="_blank" class="btn btn-default btn-xs pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                        </th>
                                        <th class="bg-color">Narration</th>
                                        <th class="bg-color" style="text-align:center;">Amount</th>
                                        <th class="bg-color" style="text-align:center;">Action</th>
                                    </thead>
                                    <tbody id="d">
                                        <tr class="entry_row">
                                            <td width="21%" style="padding:2px 2px">
                                                <select class="accCodeSelector show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                                                   <option selected value=""></option>
<?php
											$accountsList=$objChartOfAccounts->getLevelFourList();
											if(mysql_num_rows($accountsList)){
												while($account=mysql_fetch_array($accountsList)){
													if($account['ACC_CODE'] == $default_cash_in_hand){
														continue;
													}
?>
                                                   		<option value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
<?php
												}
											}
?>
                                                </select>
                                            </td>
                                            <td width="41%"><input type="text" class="form-control narration" value="Amount received in cash" /></td>
                                            <td width="15%"><input type="text" class="form-control amount text-center" /></td>
                                            <td width="8%"><input type="button" class="btn btn-default cdr_btn btn-block" value="Enter" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="display:none;height:15px;padding:1px;padding-left:10px;font-size:14px;" class="insertAccTitle"></td>
                                        </tr>
                                    </tbody>
                            	</table>
                                <div class="clear"></div>
                            </fieldset>
                            <div class="clear"></div>
                      </div><!--form-->
						          <div style="clear:both;"></div>
                      <div id="form">
                        <div class="panel panel-default" style="width:90%;margin:0px auto;border-radius: 0px !important;">
                      <div class="clear"></div>
                      <table cellspacing="0" align="center" style="width:100%;">
                         <thead>
                            <tr>
                               <th width="15%" style="text-align:left;"   class="bg-color">Account</th>
                               <th width="15%" style="text-align:left;"   class="bg-color">Account Title</th>
                               <th width="40%" style="text-align:left;"   class="bg-color">Narrtion</th>
                               <th width="10%" style="text-align:center;" class="bg-color">Amount</th>
                               <th width="10%" style="text-align:center;" class="bg-color">Action</th>
                            </tr>
                         </thead>

                         <tbody>
                          <tr class="transactions" style="display:none;"></tr>

<?php
            if(isset($jv_detail_list) && mysql_num_rows($jv_detail_list)){
              while($jv_row=mysql_fetch_array($jv_detail_list)){
                if($jv_row['TRANSACTION_TYPE'] == 'Dr'){
                  continue;
                }
?>
                            <tr class="transactions amountRow">
                              <td style="text-align:right;"  class="accCode"><?php echo $jv_row['ACCOUNT_CODE']; ?></td>
                                <td style="text-align:left;"   class="accTitle"><?php echo $jv_row['ACCOUNT_TITLE']; ?></td>
                                <td style="text-align:left;"   class="narration"><?php echo $jv_row['NARRATION']; ?></td>
                                <td style="text-align:center;" class="creditColumn"><?php echo $jv_row['AMOUNT']; ?></td>
                                <td style="text-align:center;"></td>
                            </tr>
<?php
              }
            }
?>

              <tr style="background-color:#F8F8F8;height: 30px;">
                              <td colspan="3" style="text-align:right;"> Total: </td>
                                <td class="creditTotal" style="text-align:center;color:#BD0A0D;"></td>
                                <td></td>
                            </tr>
                         </tbody>
                      </table>
<?php
    if($voucher_type == 'CR'){
?>
                      <div class="underTheTable" style="padding:10px;">
                        <?php
                          if(!isset($jVoucher)){
                        ?>
                          <button class="button pull-right saveJournal">Save</button>
                        <?php
              }
                        ?>
                          <button class="button pull-right mr-10" onclick="window.location.href='<?php echo basename($_SERVER['PHP_SELF']); ?>';">New Voucher</button>
													<div class="clear"></div>
                      </div>
<?php
    }
?>
                      <div class="clear"></div>
            </div><!--content-box-content-->
                      </div>
               		</div>
<?php
			}
?>
            	</div><!-- End summer -->
         	</div>   <!-- End .content-box-top -->
	</div>  <!--body-wrapper-->
</div>  <!--body-wrapper-->
        <div id="fade"></div>
        <div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>
<script>
	$(document).ready(function(){
		$('select').selectpicker();

		$("input[name='jvReference']").focus();

		$(".receiptNum,.amount").numericFloatOnly();

		$("input[name='jvReference']").setFocusTo("input[name='refDate']");
		$("input[name='refDate']").setFocusTo("div.accCodeSelector button");
		$('select.accCodeSelector').change(function(){

		});
    $("select.accCodeSelector").change(function(e){
        setTimeout(function(){
          if($("select.accCodeSelector option:selected").val() != ''){
            var account_code=$('select.accCodeSelector option:selected').val();
            getAccountBalance(account_code,".insertAccTitle");
            $(".narration").focus();
          }
        },100);
    });
    $("input.narration").keyup(function(e){
      if(e.keyCode == 13 && $(this).val() != ''){
        $("input.amount").focus();
      }
    });
    $("input.amount").keyup(function(e){
      if(e.keyCode == 13 && $(this).val() != ''){
        $(this).quickSubmitDrCr();
      }
    });
		$(".saveJournal").click(function(){
			$(this).saveJournal();
		});
    $(window).keyup(function(e){
        if(e.altKey == true&&e.keyCode == 83){
            e.preventDefault();
            $("#form").click();
            $(".saveJournal").click();
            return false;
        }
    });
    $(".cdr_btn").on('click',function(){
      $(this).quickSubmitDrCr();
    });
    $(".cdr_btn").on('keydown',function(e){
      if(e.keyCode == 13){
        $(this).quickSubmitDrCr();
      }
    });
		getCashBalance();
		$("select.cash_accounts_list").change(getCashBalance);
<?php
		if($voucher_type != 'CR'){
?>
			displayMessage("Voucher Type Is Not Supported!");
<?php
		}
?>

    });
</script>
