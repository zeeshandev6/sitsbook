<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/stock_taking.php');
	include('common/classes/items.php');
	include('common/classes/sheds.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');

	//Permission
	if( (!in_array('stock-taking',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objStockTaking  		= new stockTaking();
	$objItems 		 			= new items();
	$objSheds        		= new Sheds();
	$objJournalVoucher  = new JournalVoucher();
	$objChartOfAccounts = new ChartOfAccounts();

	//$total
	$totalRows   = $objStockTaking->countRows();
	$shedList 	 = $objSheds->getList();
	$stockList 	 = $objItems->getActiveList();
	$item_list   = array();

	$total 			 = $objConfigs->get_config('PER_PAGE');

	if(isset($_GET['delete'])){
		$id = (int)(mysql_real_escape_string($_POST['id']));
		$voucher_id = $objStockTaking->getVoucherId($id);
		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		$objStockTaking->delete($id);
		echo json_encode(array('OKAY'=>'Y'));
		exit();
	}

	if(isset($_POST['submit'])){
		$stock_taking_id 									= (isset($_POST['stock_taking_id']))?(int)mysql_real_escape_string($_POST['stock_taking_id']):0;
		$objStockTaking->entry_date  			= date('Y-m-d',strtotime($_POST['entry_date']));
		$objStockTaking->shed_id 					= mysql_real_escape_string($_POST['shed_id']);
		$objStockTaking->item_id 					= mysql_real_escape_string($_POST['item_id']);
		$objStockTaking->quantity 				= mysql_real_escape_string($_POST['quantity']);
		$objStockTaking->unitRate 				= mysql_real_escape_string($_POST['unitRate']);
		$objStockTaking->amount 					= mysql_real_escape_string($_POST['amount']);

		$voucher_id  = 0;
		if($stock_taking_id>0){
			$objStockTaking->update($stock_taking_id);
			$voucher_id 	= $objStockTaking->getVoucherId($stock_taking_id);
		}else{
			$stock_taking_id 	= $objStockTaking->save();
		}

		exit();

		$shedAccCode 		= $objSheds->getAccCode($objStockTaking->shed_id);
		$shedAccTitle 	= $objChartOfAccounts->getAccountTitleByCode($shedAccCode);

		$objJournalVoucher->jVoucherNum    	= $objJournalVoucher->genJvNumber();
		$objJournalVoucher->voucherDate    	= date('Y-m-d',strtotime($objStockTaking->entry_date));
		$objJournalVoucher->reference   	  = "Shed ".$shedAccTitle;
		$objJournalVoucher->voucherType   	= 'PV';
		$objJournalVoucher->reference_date 	= date('Y-m-d',strtotime($objStockTaking->entry_date));
		$objJournalVoucher->status	  			= 'P';

		if($voucher_id==0){
			$voucher_id = $objJournalVoucher->saveVoucher();
		}else{
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->updateVoucherDate($voucher_id);
		}

		$objJournalVoucher->voucherId    		= $voucher_id;
		$objJournalVoucher->accountCode  		= '0101060001';
		$objJournalVoucher->accountTitle 		= $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
		$objJournalVoucher->narration    		= "Month Ending  ".$objStockTaking->entry_date." Stock Adjusting Entry In ".$shedAccTitle;
		$objJournalVoucher->transactionType = 'Dr';
		$objJournalVoucher->amount       		= $objStockTaking->amount;

		$objJournalVoucher->saveVoucherDetail();

		$objJournalVoucher->accountCode  		= '0301040001';
		$objJournalVoucher->accountTitle 		= $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
		$objJournalVoucher->narration    		= "Closing Stock For The Month Ending ".$objStockTaking->entry_date;
		$objJournalVoucher->transactionType = 'Cr';

		$objJournalVoucher->saveVoucherDetail();

		$objStockTaking->insertVoucherId($stock_taking_id,$voucher_id);

		$pagePreFix 		= (isset($_GET['page']))?"&page=".$_GET['page']:"";
		header('location:stock-taking.php?saved'.$pagePreFix);
		exit();
	}

	$this_page = 1;
	$start     = 0;
	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}else{
			$start = 0;
		}
	}

	$objStockTaking->fromDate     = isset($_POST['fromDate'])?date('Y-m-d',strtotime($_POST['fromDate'])):"";
	$objStockTaking->toDate       = isset($_POST['toDate'])?date('Y-m-d',strtotime($_POST['toDate'])):"";
	$objStockTaking->item_id      = isset($_POST['item_id'])?mysql_real_escape_string($_POST['item_id']):"";
	$objStockTaking->shed_id 			= isset($_POST['shed_id'])?mysql_real_escape_string($_POST['shed_id']):"";
	$stockTakingList 							= $objStockTaking->search($start,$total);
	if(isset($_GET['id'])){
		$id = (int)mysql_real_escape_string($_GET['id']);
		$stockTakingDetails = $objStockTaking->getDetails($id);
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       							/>
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     							/>
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     							/>
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/animate.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  								/>
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  							/>
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
		<style media="screen">
			#bodyTab2 table td{
				font-size: 12px !important;
				padding: 6px !important;
			}
			#bodyTab2 table th{
				font-size: 12px !important;
				padding: 6px !important;
			}
		</style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript">
			$(function(){
				$('select').selectpicker();
				$("input.datepicker").setFocusTo("button.dropdown-toggle:first");
				$("select[name='item_id']").change(function(){
					$("input[name='quantity']").focus();
				});
				$("input[name='quantity']").setFocusTo("input[name='unitRate']");
				$("input[name='unitRate']").setFocusTo("input[type='submit']");
				$("input[name='quantity']").numericInputOnly();
				$("input[name='unitRate']").numericInputOnly();
				$("input[name='amount']").numericInputOnly();
				$("input[name='quantity']").keyup(function(){
					$(this).multiplyTwoFeilds("input[name='unitRate']","input[name='amount']");
				});
				$("input[name='unitRate']").keyup(function(){
					$(this).multiplyTwoFeilds("input[name='quantity']","input[name='amount']");
				});
				$("button.dropdown-toggle").first().focus();
			});
		</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
					<?PHP include("common/left_menu.php") ?>
    		</div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Stock Taking Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tab" id="tab2" onclick="tab('2','1','2');">List</div>
                        <div class="tab" data-toggle="modal" data-target="#myModal">Search</div>
                        <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">Stock Taking</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">
                            <div class="caption">Stock taking Date</div>
                            <div class="field" style="width:150px">
                                <input type="text" name="entry_date" value="<?php echo (isset($stockTakingDetails))?date('d-m-Y',strtotime($stockTakingDetails['STOCK_TAKING_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px" required />
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Shed :</div>
                            <div class="field" >
                                <select class="selectpicker show-tick form-control" data-live-search="true" name="shed_id" required>
                                       <option value="0"></option>
<?php
                                if(mysql_num_rows($shedList)){
                                    while($account = mysql_fetch_array($shedList)){
																			$shedSelected = (isset($stockTakingDetails)&&$stockTakingDetails['SHED_ID'] == $account['SHED_ID'])?"selected='selected'":"";
?>
                                       <option value="<?php echo $account['SHED_ID']; ?>" <?php echo $shedSelected; ?> ><?php echo $account['SHED_TITLE']; ?></option>
<?php
                                    }
                                }
?>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Item :</div>
                            <div class="field">
                                <select class="selectpicker show-tick form-control"  data-live-search="true" name="item_id" required >
                                       <option selected value=""></option>
<?php
                                if(mysql_num_rows($stockList)){
                                    while($account = mysql_fetch_array($stockList)){
																			if($account['INV_TYPE']=='B'){
																				continue;
																			}
																			$accSelected = "";
																			if(isset($stockTakingDetails)){
																				if($stockTakingDetails['ITEM_ID'] == $account['ID']){
																					$accSelected = "selected";
																				}
																			}
																			$item_list[] = $account;
?>
                                       <option value="<?php echo $account['ID']; ?>" <?php echo $accSelected; ?> data-subtext="<?php echo $account['ITEM_BARCODE']; ?>" ><?php echo $account['NAME']; ?></option>
<?php
                                    }
                                }
?>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Units:</div>
                            <div class="field" >
                                <input type="text" name="quantity" value="<?php echo (isset($stockTakingDetails))?$stockTakingDetails['REM_QUANTITY']:""; ?>" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Unit Price:</div>
                            <div class="field" >
                                <input type="text" name="unitRate" value="<?php echo (isset($stockTakingDetails))?$stockTakingDetails['UNIT_RATE']:""; ?>" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Amount:</div>
                            <div class="field" >
                                <input type="text" name="amount" value="<?php echo (isset($stockTakingDetails))?$stockTakingDetails['AMOUNT']:""; ?>" class="form-control" readonly="readonly" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption"></div>
                            <div class="field">
<?php
						if(isset($stockTakingDetails)){
?>
								<input type="hidden" name="stock_taking_id" value="<?php echo (isset($stockTakingDetails))?$stockTakingDetails['ID']:""; ?>" />
								<input type="submit" name="submit" value="Update" class="button" />
								<input type="button" name="" value="New Form" class="button" onclick="window.location.href = '<?php echo $fileName; ?>';" />
<?php
						}else{
?>
								<input type="submit" name="submit" value="Save" class="button" />
<?php
						}
?>
                            </div>
                            <div class="clear"></div>
                        </form>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
                <div id="bodyTab2">
                   <table style="margin:0 25px; width:95%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th width="20%" style="text-align:center">Stock Taking Date</th>
                               <th width="20%" style="text-align:center">Shed Title</th>
                               <th width="20%" style="text-align:center">Item Title</th>
                               <th width="10%" style="text-align:center">Quantity</th>
                               <th width="10%" style="text-align:center">Amount</th>
                               <th width="10%" style="text-align:center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
					if(mysql_num_rows($stockTakingList)){
						$counter = 1;
						while($row = mysql_fetch_array($stockTakingList)){
							$itemTitle = $objItems->getItemTitle($row['ID']);
							$shedTitle = $objSheds->getTitleStr($row['SHED_ID']);
?>
                        	<tr>
                            	<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['STOCK_TAKING_DATE'])); ?></td>
                                <td style="text-align:center;"><?php echo $shedTitle; ?></td>
                                <td style="text-align:left;"><?php echo $itemTitle; ?></td>
                                <td style="text-align:center;"><?php echo $row['REM_QUANTITY']; ?></td>
                                <td style="text-align:center;"><?php echo $row['AMOUNT']; ?></td>
                                <td style="text-align:center;">
<?Php
								if(isset($_GET['page'])){
									$page = "&page=".$_GET['page'];
								}else{
									$page = "";
								}
?>
                                	<a id="view_button" href="<?php echo $fileName; ?>?id=<?php echo $row['ID'].$page; ?>" title="View"><i class="fa fa-pencil"></i></a>
                                	<a class="pointer deleteRow" do="<?php echo $row['ID']; ?>" title="Delete"><i class="fa fa-times"></i></a>
                                </td>
													</tr>

<?php
						$counter++;
						}
					}
?>
                        </tbody>

												<tfoot>
<?php
					if(!isset($_POST['search'])){
						$sum = $objStockTaking->getTotals();
?>
													<tr>
														<td colspan="3" style="text-align:right;">Total</td>
															<td style="text-align:center;"><?php echo (float)$sum['TOTAL_LENGTH']; ?></td>
															<td style="text-align:center;"><?php echo (float)$sum['TOTAL_AMOUNT']; ?></td>
															<td style="text-align:center;"> - - - </td>
													</tr>
<?php
					}else{
?>
													<tr>
														<td colspan="6" style="text-align:center;">
															<button class="btn btn-sm btn-default" onclick="window.location.href='<?php echo $fileName; ?>';"><i class="fa fa-refresh"></i> Clear Search</button>
															<button class="btn btn-sm btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> New Search</button>
														</td>
													</tr>
<?php
					}
?>
												</tfoot>
                    </table>
                </div> <!-- End #tab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <h4 class="modal-title">Search</h4>
				      </div>
				      <div class="modal-body">
								<form method="post" action="<?php echo $fileName; ?>">
												<div class="form-group col-xs-6">
											    <label for="fromDate">From Date:</label>
											    <input type="text" name="fromDate" class="form-control datepicker" id="fromDate">
											  </div>
												<div class="clear"></div>

												<div class="form-group col-xs-6">
											    <label for="toDate">To Date:</label>
											    <input type="text" name="toDate" class="form-control datepicker" id="toDate" value="<?php echo date('d-m-Y'); ?>">
											  </div>
												<div class="clear"></div>

												<div class="form-group col-xs-6">
											    <label for="shed_id_select">Sheds:</label>
													<select class="form-control show-tick" name="shed_id" id="shed_id_select">
														<option value=""></option>
								<?php
													$shedList 	 = $objSheds->getList();
													if(mysql_num_rows($shedList)){
														while($shed = mysql_fetch_array($shedList)){
								?>
														<option value="<?php echo $shed['SHED_ID']; ?>"><?php echo $shed['SHED_TITLE']; ?></option>
								<?php
														}
													}
								?>
													</select>
											  </div>
												<div class="clear"></div>

												<div class="form-group col-xs-6">
											    <label for="item_id_select">Items:</label>
													<select class="form-control show-tick" name="item_id" id="item_id_select">
														<option value=""></option>
								<?php
													foreach ($item_list as $key => $item) {
								?>
														<option value="<?php echo $item['ID']; ?>"><?php echo $item['NAME']; ?></option>
								<?php
													}
								?>
													</select>
											  </div>
												<input type="hidden" class="button" name="search" value="Search" />
												<div class="clear"></div>
					    	</form>
				      </div>
				      <div class="modal-footer">
								<button type="button" class="btn btn-primary btn-sm" onclick="$('#myModal form').submit();"> <i class="fa fa-search"></i>	 </button>
				        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
				      </div>
				    </div>

				  </div>
				</div>
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="popUpForm"></div>
		<script type="text/javascript">
			$(document).ready(function(){
					$(".deleteRow").click(function(){
						$(this).deleteRowDefault('<?php echo $fileName; ?>?delete');
					});
					$("#datepicker").datepicker({
						dateFormat: 'dd-mm-yy',
						showAnim : 'show',
						changeMonth: true,
						changeYear: true,
						yearRange: '2000:+10'
					});
		  });
			if((parseInt($("input[name=stock_taking_id]").val())||0)>0){
				tab('1','1','2');
			}else{
				tab('2','1','2');
			}
		</script>
</body>
</html>
<?php include('conn.close.php'); ?>
