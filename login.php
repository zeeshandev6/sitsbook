<?php
	$out_buffer = ob_start();
	if(is_file('report.php')){
		include 'report.php';
	}
	include 'common/connection.php';
	include 'msgs.php';
	include 'common/classes/settings.php';
	include 'common/classes/userAccounts.php';
	include 'common/classes/sitsbook_sms_api.php';
	include 'common/classes/sms_templates.php';
	include 'common/classes/nothing.php';
	include 'common/classes/md.php';

	$objUserAccounts 		= new UserAccounts();
	$objConfigs      		= new Configs();
	$objSitsbookSmsApi 		= new SitsbookSmsApi();
	$objSmsTemplates 		= new SmsTemplates();
	$objNothing 	 		= new Nothing();

	//version type
	$online         = $objConfigs->get_config('ONLINE');
	$sms_config 	= $objConfigs->get_config('SMS');
	$sms_user_name 	= $objConfigs->get_config('USER_NAME');
	$desktop        = ($online == 'N')?true:false;
	$error  		= '';

	if(!defined('EXP')){
		define('EXP', '1990-01-01');
	}
	if($desktop){
		$diff = 1;
	}else{
		$diff = 0;
	}

	if(isset($_POST['confirm_code'])){
		$code    = mysql_real_escape_string($_POST['confirm_code']);
		$code    = trim($code);
		$user_id = (int)$objUserAccounts->getIdBySecretCode($code);
		if($user_id&&$code!=''){
			$_SESSION['forgot_user_id'] = $user_id;
		}else{
			echo "N";
		}
		exit();
	}

	if(isset($_POST['forgot_method'])){
		$user_name = mysql_real_escape_string($_POST['forgot_method']);
		$user_details = $objUserAccounts->getDetailByUserName($user_name);
		if($user_details['CONTACT_NO']!=''){
			echo "S";
		}
		if($user_details['EMAIL']!=''){
			echo "S";
		}
		exit();
	}
	if(isset($_POST['get_method'])){
		$method 			= mysql_real_escape_string($_POST['get_method']);
		$user_name 		= mysql_real_escape_string($_POST['user_name']);

		$user_details = $objUserAccounts->getDetailByUserName($user_name);
		if($method == 'S'){
			echo "*****".substr($user_details['CONTACT_NO'],7,11);
		}
		if($method == 'E'){
			echo substr($user_details['EMAIL'],0,3)."*****".substr($user_details['EMAIL'],strpos($user_details['EMAIL'],'@'),10);
		}
		exit();
	}
	if(isset($_POST['forgot_user'])){
		$user_name 		= mysql_real_escape_string($_POST['forgot_user']);
		$user_details = $objUserAccounts->getDetailByUserName($user_name);

		if($user_details == NULL){
			echo "Error! Invalid Username.";
			exit();
		}
		$sms_template = $objSmsTemplates->get_template(7);
		$sms_body 		= $sms_template['SMS_BODY'];
		$code 				= rand(1,9).rand(0,9).rand(0,7).rand(1,7);
		$objUserAccounts->setSecretCode($code,$user_details['ID']);
		$sms_body 		= str_replace("[CODE]",$code,$sms_body);
		echo $objSitsbookSmsApi->postSoapRequest($sms_user_name,'N',$user_details['CONTACT_NO'],$sms_body,0,0);
		exit();
	}
	if(isset($_POST['LogIn'])&&isset($_POST['UserName'])&&isset($_POST['Password'])){
		if($desktop){
			$get_mac        = mysql_query("SELECT V_A_L FROM config WHERE K_E_Y = 'SITS'");
			$this_mac       = mysql_fetch_array($get_mac);

			$allowedUUID    = $this_mac['V_A_L'];

			if((strpos($_SERVER['SERVER_SOFTWARE'],'Linux') !== false) || (strpos($_SERVER['SERVER_SOFTWARE'],'Ubuntu') !== false) || strpos($_SERVER['SERVER_SOFTWARE'],'Unix') !== false){
				$command = 'hostid';
				$system_uuid = shell_exec($command);
				if($system_uuid == ''){
					linkBootstrap();
					errorMessage('Error! Cannot access product profile (cat).');
					exit();
				}
				$system_uuid = preg_replace('/\s+/', "", $system_uuid);
				$system_uuid = 'wish'.$system_uuid.'wish';
				$system_uuid = hash('sha256',$system_uuid);
			}
			if((strpos($_SERVER['SERVER_SOFTWARE'],'Win32') !== false) || strpos($_SERVER['SERVER_SOFTWARE'],'Win64') !== false){
				$uuid = shell_exec("wmic csproduct get UUID");
				$system_uuid = str_replace("UUID", "", $uuid);
				$system_uuid = preg_replace('/\s+/', "", $system_uuid);
				$system_uuid = 'wish'.$system_uuid.'wish';
				$system_uuid = hash('sha256',$system_uuid);
			}
			if($allowedUUID != $system_uuid){
				// header('location:login.php?invalid-key');
				// exit();
			}
		}else{
			$client_auth_code = $objConfigs->get_config('SITS');
			$options = array('location' => 'http://sitsbook.com/auth.server.php',
							 'uri'      => 'http://sitsbook.com/');
			$authServerApi = new SoapClient(NULL, $options);
			$authentication = $authServerApi->authenticate($client_auth_code);
			if($authentication == 'N'){
				$_SESSION['key'] = 'New';
				header('location:enter-product-key.php');
				exit();
			}
		}

		$u = mysql_real_escape_string(trim($_POST['UserName']));
		$p = mysql_real_escape_string(trim($_POST['Password']));
		$user_id = $objUserAccounts->checkLogin($u,$p);
		if($user_id > 0){
			if($diff >= 0){
				$_SESSION['classuseid'] = $user_id;
				header('location:settings.php');
			}else{
				$expired  = true;
			}
		}else{
			$error = 'Incorrect Username or Password!';
		}
	}
	if(isset($expired)){
		$error = 'Warning! Trial has expired.';
	}
	if(isset($_GET['autherror'])){
		$error = 'Incorrect Username or Password!';
	}
	if(isset($_GET['invalid-key'])){
		$error = 'Invalid Product Key!';
	}
?>
	<!DOCTYPE html>


	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>User Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="resource/css/login-style.css" rel='stylesheet' type='text/css' />
		<script src="resource/scripts/jquery.1.11.min.js" type="text/javascript"></script>
		<script src="resource/scripts/jquery-ui.min.js" type="text/javascript"></script>
		<script src="resource/scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="resource/scripts/configuration.js" type="text/javascript"></script>
	</head>

	<body>
		<div class="main" style="display: none;">
			<div class="login-form">
				<div class="head">
					<img id="logo" src="resource/images/medical.png" alt="Sits Book" title="About Sits Book Software" />
				</div>
				<div class="clear"></div>
				<div class="form-container">
					<form method="post" class="login_fields">
						<input type="text" class="text" value="" placeholder="User Name" name="UserName" required />
						<input type="password" value="" placeholder="Password" name="Password" required />
						<div class="clear"></div>
						<div class="submit">
							<?php if($sms_config=='Y'){ ?>
							<a href="#" class="text-danger" onclick="forgot_password_init();">Forgot Password ?</a>
							<?php } ?>
							<input type="submit" class="btn btn-default" name="LogIn" value="LOGIN">
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</form>
					<?php if($sms_config=='Y'){ ?>
					<form action="" method="post" class="forgot_fields">
						<input type="text" id="forgot_user" class="text" value="" placeholder="User Name" name="forgot_user" autocomplete="off" />
						<p class="margin-clear" style="margin-left: 90px;margin-top: -10px;margin-bottom: 10px;">
							<input type="radio" id="css-label-radio-1" class="css-checkbox" name="method" value="S" />
							<label for="css-label-radio-1" class="css-label-radio">SMS</label>
							<input type="radio" id="css-label-radio-2" class="css-checkbox" name="method" value="E" />
							<label for="css-label-radio-2" class="css-label-radio">Email</label>
							<div class="clear"></div>
						</p>
						<div class="clear"></div>
						<button type="button" class="pull-right btn btn-primary ml-10" onclick="forgot_password_send(this);"> <i class="fa fa-send"></i> Send Code</button>
						<span class="cancel-button" onclick="forgot_password_close();"> Cancel</span>
						<div class="clear"></div>
					</form>
					<form action="" method="post" class="secret_fields" style="display:none;">
						<p class="margin-clear">
							<label> Code : <i class="fa fa-link"></i></label>
							<input class="text-input" id="secret_code" value="" type="text" name="secret_code" autocomplete="off" />
						</p>
						<div class="clear"></div>
						<button type="button" class="pull-right btn btn-primary ml-10" onclick="confirm_code(this);">Confirm</button>
						<span class="cancel-button" onclick="forgot_password_close();"> Cancel</span>
						<div class="clear"></div>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="ft-info" style="display:none;">
			<span class="ft-ex left" style="padding-top:5px;">
        		Designed &amp; Developed By <a href="http://www.sitsol.net" target="_blank" class="sits-link-a" >SIT Solutions</a>
        		&nbsp;&nbsp;
        		<i class="fa fa-envelope"></i> <span class="ft-ex"> <a href="mailto:info@sitsbook.com">info@sitsbook.com</a> </span>			[ <small>Current Version : 3.0</small> ]
			</span>
			<span class="ft-ex right">
        		<div class="pull-right">
        			<a href="https://facebook.com/sitsbook" class="ft-ex-social ft-ex-social-f" target="_blank" ></a>
        			<a href="https://plus.google.com/108334373454545379362/posts" class="ft-ex-social ft-ex-social-g" target="_blank" ></a>
        			<a href="https://twitter.com/sitsbook" class="ft-ex-social ft-ex-social-t" target="_blank" ></a>
        		</div>
        		<div class="pull-right" style="padding-top:5px;">
	        		<a href="http://blog.sitsbook.com" target="_blank" class="ft-ex-links"> blog</a> |
	        		<a href="http://forum.sitsbook.com" target="_blank" class="ft-ex-links">Forums</a> |
	        		<a href="http://sitsbook.com/faq.php" target="_blank" class="ft-ex-links">FAQ</a> |
	        		<a href="http://sitsbook.com/user-panel/user-login.php" target="_blank" class="ft-ex-links">Support</a> |
	        		<a href="http://blog.sitsbook.com" target="_blank" class="ft-ex-links"> News &amp; Events </a>
	        	</div>
        	</span>
		</div>
	</body>

	</html>
	<?php include('conn.close.php'); ?>
	<script type="text/javascript">
	var forgot_password_init = function(){
		$(".login_fields").hide('fade',function(){
			$(".forgot_fields").show('fade');
		});
	};
	var forgot_password_close = function(){
		$(".forgot_fields").hide('fade',function(){
			$(".login_fields").show('fade');
			$("form.forgot_fields")[0].reset();
			$("form.forgot_fields button:disabled").prop('disabled',false);
			$(".secret_fields").hide('fade',function(){
				$(".login_fields").show('fade');
			});
		});
	};
	var forgot_password_send = function(elm){
		$(elm).prop("disabled",true);
		var forgot_user = $("input[name=forgot_user]").val();
		var method = $("input[name=method]:checked").length?$("input[name=method]:checked").val():"";
		if(forgot_user==''){
			displayMessage("Please enter your username.");
			$(elm).prop("disabled",false);
			return;
		}
		if(method==''){
			displayMessage("Please select recovery method. ");
			$(elm).prop("disabled",false);
			return;
		}
		$.post("?",{forgot_user:forgot_user,method:method},function(data){
			displayMessage(data);
			$(".forgot_fields").hide('fade',function(){
				$(".secret_fields").show('fade');
			});
		});
	};
	var confirm_code = function(elm){
		$(elm).prop("disabled",true);
		var code = $("input[name=secret_code]").val();
		$.post("?",{confirm_code:code},function(data){
			if(data=='N'){
				displayMessage("Error! Incorrect secret code.");
				return;
			}
			window.location.href = "forget-password.php";
			$(elm).prop("disabled",false);
		});
	}
	$(document).ready(function(){
		forgot_password_close();
		$("input[name=forgot_user]").keydown(function(e){
			if(e.keyCode==13){
				forgot_password_send();
				e.preventDefault();
				return false;
			}
		});
		$("input[type=text]").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				return false;
			}
		});
		$("input[name=method]").change(function(){
			var method = $("input[name=method]:checked").val();
			var user_name = $("input[name=forgot_user]").val();
			$.post("?",{get_method:method,user_name:user_name},function(data){
				method = (method=='S')?"mobile number.":"email address.";
				displayMessage("Confirmation code will be sent to "+data+"  "+method);
			});
		});
		$("input[name=forgot_user]").change(function(){
			var user_name = $(this).val();
			$.post("?",{forgot_method:user_name},function(data){

			});
		});
		$("input[name='UserName']").keydown(function(e){
			if(e.keyCode==13 && $("input[name='UserName']").val() != ''){
				e.preventDefault();
				$("input[name='Password']").focus();
			}
		});
		<?php if($error != ''){ ?>
			displayMessage("<?php echo $error ?>");
		<?php } ?>
    });
	$(window).load(function(){
		$("body").fadeIn(1000,function(){
			$("div.main").show();
			$("input[name='UserName']").focus();
		});
	});
</script>
