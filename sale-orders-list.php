<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/sale.php');
	include('common/classes/accounts.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/sale_orders.php');
	include('common/classes/sale_order_details.php');
	include('common/classes/j-voucher.php');
	include('common/classes/items.php');
	include('common/classes/customers.php');

	//Permission
	if( (!in_array('sale-orders',$permissionz)) && (!in_array('sales-panel',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objSale               = new Sale();
	$objJournalVoucher     = new JournalVoucher();
	$objAccountCodes       = new ChartOfAccounts();
	$objScanSaleDetails 	 = new ScanSaleDetails();
	$objSaleOrders         = new SaleOrders();
	$objSaleOrderDetails   = new SaleOrderDetails();
	$objItems              = new Items();
	$objCustomers          = new Customers();
	$objConfigs            = new Configs();

	if(isset($_POST['cid'])){
		$sale_order_id = (int)(mysql_real_escape_string($_POST['cid']));
		$sale_order_numbers = $objSaleOrderDetails->getOrderNumbersByOrderId($sale_order_id);
		$sale_order_numbers = implode(',',$sale_order_numbers);
		$used 							= $objScanSaleDetails->order_numbers_used($sale_order_numbers);
		if($used==0){
			$objSaleOrders->delete($sale_order_id);
			$objSaleOrderDetails->deleteFullOrder($sale_order_id);
			echo "Y";
		}else{
			echo "N";
		}
		exit();
	}

	$orders_list 					 = $objSaleOrders->getList();
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		table td{
			padding: 7px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sale.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".selectpicker").selectpicker();
			$("a.pointer").click(function(){
				var idValue    = $(this).attr("do");
				var currentRow = $(this).parent().parent();
				$("#xfade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete button'>Confirm</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#xfade").fadeIn();
				var win_hi = $(window).height()/2;
				var win_width = $(window).width()/2;
				win_hi = win_hi-$("#popUpDel").height()/2;
				win_width = win_width-$("#popUpDel").width()/2;
				$("#popUpDel").css({
					'position': 'fixed',
					'top': win_hi,
					'left': win_width
				});
				$("#popUpDel .confirm").text("Are Sure you Want To Delete ? ");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post("sale-orders-list.php", {cid : idValue}, function(data){
						if(data == 'Y'){
							$("tr[data-row-id='"+idValue+"']").hide();
						}
						$("#popUpDel .confirm").text(data['MSG']);
						$(".nodelete").text('Close');
						$(".dodelete").hide();
					});
				});
				$(".nodelete").click(function(){
					$("#popUpDel").slideUp(function(){
						$(this).remove();
					});
					$("#xfade").fadeOut();
				});
			});
		});
	</script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sale Orders</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<a href="sale-orders-list.php"><div class="tabSelected" id="tab1">List</div></a>
						<a href="sale-orders.php"><div class="tab" id="tab1">Details</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form" class="">
						<table>
							<thead>
								<tr>
									<th class="text-center">Order#</th>
									<th class="text-center">Order Date</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
<?php
					if(mysql_num_rows($orders_list)){
						while($order_row  = mysql_fetch_assoc($orders_list)){
							$or_no_list_arr = $objSaleOrderDetails->getOrderNumbersByOrderId($order_row['ID']);

?>
									<tr>
										<td>
												<?php
													foreach($or_no_list_arr as $key => $str){
														$used 							= $objScanSaleDetails->order_numbers_used($str);
													 ?>
													<span class="badge btn-<?php echo $used?"danger":"info"; ?>"><?php echo $str; ?></span>
												<?php
													}
													?>
										</td>
										<td><?php echo date('d-m-Y',strtotime($order_row['ORDER_DATE'])); ?></td>
										<td class="text-center">
											<a href="sale-orders.php?id=<?php echo $order_row['ID']; ?>" id="view_button"><i class="fa fa-pencil"> </i> View</a>
											<a do="<?php echo $order_row['ID']; ?>" class="pointer"> <i class="fa fa-times"></i> </a>
										</td>
									</tr>
<?php
						}
					}
?>
							</tbody>
						</table>
					</div><!--End form-->
				</div> <!--End bodyTab1-->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</div><!--body-wrapper-->
	<div id="xfade"></div>
</body>
</html>
