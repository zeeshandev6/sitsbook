<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/accounts.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sale-returns',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts          = new ChartOfAccounts();
	$objSale              = new Sale();
	$objSaleDetails       = new SaleDetails();
	$objSaleReturn        = new SaleReturn();
	$objSaleReturnDetails = new SaleReturnDetails();
	$objItems             = new Items();
	$objItemCategory      = new itemCategory();
	$objTaxRates          = new TaxRates();
	$objDepartments       = new Departments();

	$suppliersList   = $objSaleReturn->getCustomersList();
	$departmentList  = $objDepartments->getList();
	$taxRateList     = $objTaxRates->getList();

	$sale_id = 0;
	$sale_return_id = 0;

	if(isset($_GET['sid'])){
		$sale_id = mysql_real_escape_string($_GET['sid']);
		if($sale_id != '' && $sale_id > 0){
			$sale = $objSale->getDetail($sale_id);
			$saleDetails = $objSaleDetails->getList($sale_id);
            $sale_discount_type      = $sale['DISCOUNT_TYPE'];
            $sale_discount_type_name = ($sale['DISCOUNT_TYPE'] == 'P')?"Percentage":"Amount";
		}
	}else{
		header('location:sale.php');
		eixt();
	}
	$items_array = array();
	$ref_url = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'sale.php';
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
			.wel_made td{
				padding: 7px !important;
			}
			.wel_made th{
				padding: 5px !important;
				font-weight: bold !important;
			}
			.totals td{
				font-weight: bold !important;
				background-color: #F5F5F5;
			}
			.entry_table th{
				padding: 5px !important;
				font-weight: bold !important;
			}
			.entry_table td{
				padding: 7px !important;
			}
			.panel{
				padding: 0px;
			}
			hr{
				margin: 10px;
			}
			input.taxRate{
					text-align: center;
			}
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sale.return.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".over_discount").calculateFinalAmount();
        });
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sale Return Panel</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="sale.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="sale.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Return</div>
                        <a href="sale-details.php"><div class="tab">Sale</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div class="col-md-12">
                            <input type="hidden" class="discount_type" value="<?php echo $sale_discount_type; ?>" />
                    		<div class="panel_title">Sale Details </div>
                            <div class="panel panel-default">
                            	<div class="panel-heading">
                            		<div class="caption_simple pull-left">Sale Date :  <?php echo date("d-m-Y",strtotime($sale['SALE_DATE'])); ?></div>
                            		<?php
                            				if(substr($sale['CUST_ACC_CODE'], 0,6) == '010101'){
                            					$customer_name = $sale['CUSTOMER_NAME'];
											}else{
												$customer_name = $objAccounts->getAccountTitleByCode($sale['CUST_ACC_CODE']);
											}
											if(substr($sale['CUST_ACC_CODE'], 0,6) == '010101'){
                            		?>
                                    <div class="caption_simple pull-left">Cash Sales</div>
                            		<?php
											}
                                            if(substr($sale['CUST_ACC_CODE'], 0,6) != '010101'){
                            		?>
                            		<div class="caption_simple pull-left">Customer :  	   <?php echo $customer_name; ?></div>
                                    <?php
                                            }
                                    ?>
                            		<div class="caption_simple pull-left">Bill Number :    <?php echo (isset($sale))?$sale['BILL_NO']:''; ?></div>
                                    <div class="caption_simple pull-left">Discount Type :    <?php echo (isset($sale_discount_type_name))?$sale_discount_type_name:''; ?></div>
	                            	<div class="clearfix"></div>
                            	</div>
		                        <input type="hidden" class="sale_id" value="<?php echo $sale_id; ?>" />
		                        <input type="hidden" class="sale_return_id" value="<?php echo $sale_return_id; ?>" />
<?php
						if(isset($saleDetails) && mysql_num_rows($saleDetails)){
							$qty = 0;
							$stotal = 0;
							$ttotal = 0;
							$atotal = 0;
                            $dicount_stotal = 0;
?>
                            <table class="wel_made">
                            <thead>
                                <tr>
                                   <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
																	 <?php if($medicalStoreAddon=='Y'){ ?>
																	 <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Batch#</th>
																	 <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">ExpiryDate</th>
																	 <?php } ?>
                                   <th width="6%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="6%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="6%"  style="font-size:12px;font-weight:normal;text-align:center">Discount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="6%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                        	<!--doNotRemoveThisLine-->
                                <tr style="display:none;" class="calculations"></tr>
                            <!--doNotRemoveThisLine-->
<?php
                            $data_desc = 0;
							while($invRow = mysql_fetch_array($saleDetails)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
								$items_array[$invRow['ITEM_ID']] = $objItems->getRecordDetails($invRow['ITEM_ID']);

                                if($sale_discount_type == 'P'){
                                    $data_desc =  ($invRow['QUANTITY']*$invRow['UNIT_PRICE']) - (($invRow['QUANTITY']*$invRow['UNIT_PRICE']*$invRow['SALE_DISCOUNT'])/100);
                                }else{
                                    $data_desc = $invRow['SALE_DISCOUNT'];
                                }

?>
                                <tr class="alt-row bill-rows" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td style="text-align:left;" class="itemName" data-item='<?php echo $invRow['ITEM_ID']; ?>' data-batch-no="<?php echo $invRow['BATCH_NO'] ?>"><?php echo $itemName; ?> ( <?php echo $items_array[$invRow['ITEM_ID']]['ITEM_BARCODE'] ?> )</td>
																		<?php if($medicalStoreAddon=='Y'){ ?>
																		<td style="text-align:center;" class="batch_no_td"><?php echo $invRow['BATCH_NO'] ?></td>
																		<td style="text-align:center;" class="expiry_date_td"><?php echo date('d-m-Y',strtotime($invRow['EXPIRY_DATE'])); ?></td>
																		<?php } ?>
                                    <td style="text-align:center;"><?php echo $invRow['QUANTITY'] ?></td>
                                    <td style="text-align:center;" data-saleprice-id="<?php echo $invRow['ITEM_ID']; ?>"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td style="text-align:center;" data-dsct="<?php echo $data_desc; ?>" ><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                </tr>
<?php
								$qty += $invRow['QUANTITY'];
								$stotal += $invRow['SUB_AMOUNT'];
								$ttotal += $invRow['TAX_AMOUNT'];
								$atotal += $invRow['TOTAL_AMOUNT'];

                                if($sale_discount_type == 'P'){
                                    $dicount_stotal += (($invRow['QUANTITY']*$invRow['UNIT_PRICE'])*$invRow['SALE_DISCOUNT'])/100;
                                }else{
                                    $dicount_stotal += $invRow['SALE_DISCOUNT'];
                                }
							}
?>
                            </tbody>
														<tfoot>
															<tr class="totals">
																<td class="text-center">Total</td>
																<?php if($medicalStoreAddon=='Y'){ ?>
																<td class="text-center" colspan="2"> - - - </td>
																<?php } ?>
																<td class="text-center" ><?php echo $qty; ?></td>
																<td class="text-center" > - - - </td>
																<td class="text-center" > - - - </td>
																<td class="text-center" ><?php echo number_format($stotal,2,'.',''); ?></td>
																<td class="text-center" > - - - </td>
																<td class="text-center" ><?php echo number_format($ttotal,2,'.',''); ?></td>
																<td class="text-center" ><?php echo number_format($atotal,2,'.',''); ?></td>
															</tr>
														</tfoot>
                        </table>

                        <div class="clear"></div>
                        </div>

                        <div class="clear"></div>

                        <?php
                            $grand_total = 0;
                            $grand_total+= $atotal;

                            if($sale_discount_type == 'P'){
                                $grand_total-= (($atotal*$sale['DISCOUNT'])/100);
                            }else{
                                $grand_total-= $sale['DISCOUNT'];
                            }

                            $grand_total+= $sale['CHARGES'];
                        ?>

                        <div class="pull-right" style="padding: 0em 1em;width:80%;">
                            <div class="pull-right">

                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;opacity:0.0;height: 30px;">Discount :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right"  value="<?php echo $sale['DISCOUNT']; ?>" readonly="readonly" style="opacity:0.0;" />
                                </div>

                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Total Discount :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right total_discount" readonly="readonly" value="<?php echo $sale['SALE_DISCOUNT']; ?>" />
                                </div>
                                <div class="clear" style="height:5px;"></div>
                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right inv_charges" readonly="readonly" value="<?php echo $sale['CHARGES']; ?>" />
                                </div>
                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right" readonly="readonly" value="<?php echo number_format($grand_total,2,'.',''); ?>" />
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="clear"></div>

<?php
						}
?>
						<div class="panel_title">Sale Return Panel </div>

                        <div class="panel panel-default" style="padding:0em;border:none;box-shadow:none;">
                                <div id="form" class="scanner_div" style="text-align: center;margin:0px;;">
                                    <input type="text" class="barcode_input form-control" value="" placeholder="Scan Barcode" style="height:50px;font-size:26px;">
                                </div> <!-- End form -->
                            <div class="clear"></div>
                        </div>

						<div class="panel panel-default">
						<div class="panel-heading">
							<div class="caption_simple">
								Return Date
								<input type="text" name="rDate" value="<?php echo date('d-m-Y'); ?>" class="datepicker input_ghost" style="width:150px;padding-left: 40px;margin-left: -30px;" />
							</div>
							<div class="clear"></div>
						</div>
							<table class="entry_table">
                <thead>
                    <tr>
                       <th width="10%" class="text-center">Item</th>
											 <?php if($medicalStoreAddon=='Y'){ ?>
											 <th width="5%" 	class="text-center">Batch#</th>
											 <th width="8%" 	class="text-center">ExpiryDate</th>
											 <?php } ?>
                       <th width="5%" class="text-center">Quantity</th>
                       <th width="5%" class="text-center">Unit Price</th>
                       <th width="5%" class="text-center">Discount</th>
                       <th width="7%" class="text-center">Sub Total</th>
                       <th width="5%" class="text-center"> Tax @ </th>
                       <th width="7%" class="text-center">Tax Amount</th>
                       <th width="8%" class="text-center">Total Amount</th>
                       <th width="5%"  class="text-center">StockInHand</th>
                       <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="quickSubmit" style="background:none">
                        <td style="padding:0; line-height:0">
                            <select class="itemSelector show-tick form-control" data-style="btn-default" data-live-search="true">
                               <option selected value=""></option>
<?php
					foreach ($items_array as $key => $value){
						$cat = $objItemCategory->getTitle($key)
														?><option  value="<?php echo $value['ID']; ?>" ><?php echo $value['ITEM_BARCODE'] ?>-<?php echo $value['NAME']; ?></option><?php
					}
?>
                            </select>
                        </td>
												<?php if($medicalStoreAddon=='Y'){ ?>
												<td style="padding:0; line-height:0">
														<select class="form-control batch_no" name="batch_no">
															<option value="">Select</option>
														</select>
												</td>
												<td style="padding:0; line-height:0">
														<select class="form-control expiry_date" name="expiry_date">
															<option value="">Select</option>
														</select>
												</td>
												<?php } ?>
                        <td style="padding:0; line-height:0">
                            <input type="text" class="quantity form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0">
                            <input type="text" class="unitPrice form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0">
                            <input type="text" class="discount form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0;">
                            <input type="text"  readonly value="0" class="subAmount form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0;" class="taxTd text-center">
                        	<input class="taxRate form-control"></input>
                        </td>
                        <td style="padding:0; line-height:0;">
                            <input type="text"  readonly value="0" class="taxAmount form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0;">
                            <input type="text"  readonly value="0" class="totalAmount form-control text-center"/>
                        </td>
                        <td style="padding:0; line-height:0">
                            <input type="text" class="inStock form-control text-center" theStock="0" readonly />
                            <input type="hidden" class="qty_limit" /><input type="hidden" class="qty_returned" />
                        </td>
                        <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                    </tr>
             	</tbody>
                <tbody>
										<tr style="display:none;" class="calculations"></tr>
                </tbody>
								<tfoot>
									<tr class="totals">
										<td class="text-center"  >Total</td>
										<?php if($medicalStoreAddon=='Y'){ ?>
										<td class="text-center" colspan="2">- - -</td>
										<?php } ?>
										<td class="text-center qtyTotal"  ></td>
										<td class="text-center"  >- - -</td>
										<td class="text-center discountAmount"  ></td>
										<td class="text-center amountSub"  ></td>
										<td class="text-center"  >- - -</td>
										<td class="text-center amountTax"  ></td>
										<td class="text-center amountTotal"  ></td>
										<td class="text-center"  >- - -</td>
										<td class="text-center"  >- - -</td>
									</tr>
								</tfoot>
            	</table>
            </div>
            <div class="underTheTable">
	            <div class="pull-right col-xs-4" style="margin-right: 0px !important;">
	                <div class="col-xs-6 text-right hide" style="line-height:30px;">Discount</div>
	                <div class="pull-right col-xs-6 hide">
	                    <input type="text" class="form-control text-right over_discount" />
	                </div>
	                <div class="clear"></div>
	                <div class="col-xs-6 text-right" style="line-height:30px;">Total Discount</div>
	                <div class="pull-right col-xs-6">
	                    <input type="text" class="form-control text-right bill_discount" />
	                </div>
	                <div class="clear" style="height:10px;"></div>
	                <div class="col-xs-6 text-right" style="line-height:30px;">Total</div>
	                <div class="pull-right col-xs-6">
	                    <input type="text" class="form-control text-right over_total" readonly />
	                </div>
	                <div class="clear" style="height:10px;"></div>
	            </div>
	            <div class="clear"></div>
          		<div class="button pull-right saveSale">Save</div>
            </div><!--underTheTable-->
						<div class="clear"></div>
						<div style="margin-top:10px"></div>
						<div class="panel_title">Recent Sale Return </div>
						<div class="recent_returns"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
		$('select').selectpicker();
		$(document).calculateColumnTotals();
		 $("div.recent_returns").on('click','button.delete_sale_return',function(){
			$(this).deletePanel("db/del-sale-return.php");
		});
		$("#form").change(function(){
			get_recent_sale_returns("div.recent_returns");
		});
		$("input.bill_number").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.supplierSelector button");
		$("div.supplierSelector button").keyup(function(e){
			var supp = $("select.supplierSelector option:selected").val();
			if(e.keyCode == 13 && supp != ''){
				$(".bill_number").focus();
			}
		});
		$("input.bill_number").setFocusTo("div.itemSelector button");
		$('div.itemSelector button').keyup(function(e){
			if(e.keyCode ==13 && $("select.itemSelector option:selected").val() != ''){
        $(".barcode_input").attr('data-focus','');
				$(this).getItemReturnDetails();
				if($("select.batch_no").length>0){
					$("select.batch_no").populateBatchNumberList();
					$("div.batch_no button").focus();
				}else{
					$("input.quantity").focus();
				}
			}
		});
    $('select.itemSelector').change(function(){
        if($("select.itemSelector option:selected").val() != ''){
            $(this).getItemReturnDetails();
						if($("select.batch_no").length>0){
							$("select.batch_no").populateBatchNumberList();
							$("div.batch_no button").focus();
						}else{
							$("input.quantity").focus();
						}
        }
    });
		$("div.batch_no button").keydown(function(e){
			if(e.keyCode==13){
				$("select.expiry_date").populateExpiryDateList();
				$(this).getItemReturnDetails();
				$("div.expiry_date button").focus();
			}
		});
		$("div.expiry_date button").keydown(function(e){
			if(e.keyCode==13){
				$(this).getItemReturnDetails();
				$("input.quantity").focus();
			}
		});
		$("input.quantity").on('keyup blur',function(e){
			stockOs();
		});
		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13){
				$("input.unitPrice").focus();
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});
        $("input.taxRate").keydown(function(e){
            if(e.keyCode == 13){
                $(this).calculateRowTotal();
                $(".addDetailRow").focus();
            }
        });
		$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
		$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");
		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
		});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".saveSale").click(function(){
			saveSale();
		});
		get_recent_sale_returns("div.recent_returns");
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Sales Return Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Sales Return Updated Successfully!');
<?php
		}
?>

</script>
