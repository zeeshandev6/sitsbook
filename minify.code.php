<?php
  if(!isset($_GET['fy'])){
    exit();
  }
  $src = "resource/scripts/";
  $dst = "resource/scripts.min/";
  if(!is_dir($dst)){
    mkdir($dst,0777);
  }
  $extensions = array();
  $extensions[] = 'js';

  $file_count = 1;
  $file_encrypted = 0;
  if ($h = opendir($src)) {
    $dirs = array();
    while (false !== ($f = readdir($h)))
    {
      if (('.'==$f) || ('..'==$f)) continue;
      $source = $src .'/'.$f;
      $destination = $dst.'/'.$f;
      if (is_dir($source)) {
        $dirs[] = $f;
      } else {
        $pos = strrpos($f,'.');
        if (false !== $pos){
          $ext = strtolower(substr($f, $pos+1));
          if (in_array($ext, $extensions)){
            echo '<br>Encrypted : ',$source;
            $file_handle = fopen($source,'r');
            $file_contents = '';
            while($line = fgets($file_handle)){
              $file_contents .= $line;
            }
            fclose($file_handle);
            $file_contents = str_replace(array('\r','\n'), '', $file_contents);
            $new_fhandle = fopen($destination,'w+');
            fwrite($new_fhandle,$file_contents);
            fclose($new_fhandle);
            //foreach ($a as $key => $value) {
              //echo $a;
            //}
            //$b=kriptola($a,$dst.'/'.$f);
            $file_encrypted++;
            continue;
          }
        }
        //copy($source, $dst.'/'.$f);
      }
      $file_count++;
    }
    echo "<br><b>Files Minified ".$file_encrypted."</b><br>";
    echo "<b>Total Files ".$file_count."</b><br>";
    closedir($h);
  }
?>
