<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_sale.php');
	include('common/classes/emb_sale_details.php');
	include('common/classes/customers.php');
	include('common/classes/banks.php');
	include('common/classes/machines.php');
	include('common/classes/services.php');
	include('common/classes/itemCategory.php');
	include('common/classes/tax-rates.php');
	include('common/settings/captions.php');

	//Permission
	if( (!in_array('sales',$permissionz)) && (!in_array('sales-panel',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes    = new ChartOfAccounts();
	$objSale         	= new EmbroiderySale();
	$objSaleDetails  	= new EmbroiderySaleDetails();
	$objMachines        = new Machines();
	$objItemCategory 	= new itemCategory();
	$objServices     	= new Services();
	$objCustomers    	= new Customers();
	$objBanks        	= new Banks();
	$objConfigs      	= new Configs();

	$totalRows = $objSale->countRows();

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	//Pagination Settings
	$objPagination  = new Pagination();
	$total          = $objPagination->getPerPage();
	//Pagination Settings { END }

	$invoiceSize = $invoice_format[0]; // S  L

	if($invoiceSize == 'L'){
		$invoiceFile = 'emb-sales-invoice.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile = 'emb-sales-invoice-duplicates.php';
	}elseif($invoiceSize == 'S'){
		$invoiceFile = 'emb-sales-invoice-small.php';
	}
	$origin_invoice = $invoiceFile;

	$customersList = $objCustomers->getList();
	$cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');

	if(isset($_GET['search'])){
		$objSale->from_date 			= ($_GET['fromDate'] == '')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objSale->to_date 				= ($_GET['toDate'] == '')?"":date('Y-m-d',strtotime($_GET['toDate']));
		$objSale->account_code 			= mysql_real_escape_string($_GET['supplierAccCode']);
		$objSale->bill_no   			= mysql_real_escape_string($_GET['billNum']);
	}

	if(!$admin){
		$objSale->user_id = $user_id;
	}

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$saleList = $objSale->search($start, $total);
	$found_records = $objSale->found_records;
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
	table td{
		padding: 7px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/emb.sale.config.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" >
	$(document).ready(function() {
		$(".selectpicker").selectpicker();
		$("a.delete_record").click(function(){
			$(this).deleteMainRow("db/del-emb-sale.php");
		});
		$("td[data-items-list]").on("mouseover",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data);
		});
		$("td[data-items-list]").on("mouseout",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data.substr(0,40));
		});
	});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sale Management - <?php echo $found_records; ?> Records</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
						<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
						<a href="emb-sale-details.php"><div class="tab">New</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div id="bodyTab1" style="display:block;">
					<table width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th style="text-align:center;width:5%;">Bill#</th>
								<th style="text-align:center;width:10%;">SaleDate</th>
								<th style="text-align:center;width:15%;">Customer</th>
								<th style="text-align:center;width:20%;">Items</th>
								<th style="text-align:center;width:10%">Stitches</th>
								<th style="text-align:center;width:10%;">Total Amount(<?php echo $currency_type; ?>)</th>
								<th style="text-align:center;width:5%;">Action</th>
							</tr>
						</thead>

						<tbody>
							<?php
							if(mysql_num_rows($saleList)){
								while($saleRow = mysql_fetch_array($saleList)){
									$supplierTitle 			= $objAccountCodes->getAccountTitleByCode($saleRow['CUST_ACC_CODE']);
									$billTotal 					= $objSaleDetails->getInventoryAmountSum($saleRow['ID']);
									$total_stitches 		= $objSaleDetails->getStitchesPerBill($saleRow['ID']);
									$itemsArray    			= $objSaleDetails->getMachineIdArrayPerBill($saleRow['ID']);
									$itemsList = '';

									if($itemsList != ''){
										$itemsList .= ', ';
									}
									foreach($itemsArray as $key => $item_id){
										if($key > 0){
											$itemsList .= ', ';
										}
										$itemsList .= $objMachines->getName($item_id);
									}
									?>
									<tr data-row-id="<?php echo $saleRow['ID']; ?>">
										<td style="text-align:center"><?php echo $saleRow['BILL_NO']; ?></td>
										<td style="text-align:center"><?php echo date('d-m-Y',strtotime($saleRow['SALE_DATE'])); ?></td>
										<td style="text-align:left"><?php echo $supplierTitle; ?></td>
										<td style="text-align:left;" class="pl-10" data-items-list="<?php echo $itemsList; ?>"><?php echo substr($itemsList, 0,40); ?></td>
										<td style="text-align:center"><?php echo $total_stitches; ?></td>
										<td style="text-align:center"><?php echo number_format($billTotal,2); ?></td>
										<td style="text-align:center;padding:5px 0px !important;">
											<div class="dropdown">
												<button class="dropdown-toggle" id="view_button" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
													<span class="caret"></span></button>
													<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="emb-sale-details.php?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-pencil"></i> Edit </a></li>
														<li><a href="emb-sale-details.php?c&id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-copy"></i> Copy</a></li>
														<li><a target="_blank" href="<?php echo $invoiceFile ?>?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-print"></i> Invoice</a></li>
														<li><a target="_blank" href="voucher-print.php?id=<?php echo $saleRow['VOUCHER_ID']; ?>" title="Print"> <i class="fa fa-print"></i> Voucher</a></li>
														<?php if(in_array('delete-sales',$permissionz) || $admin == true){ ?>
															<li><a href="#" do="<?php echo $saleRow['ID']; ?>" class="delete_record" title="Delete"> <i class="fa fa-times"></i> Delete</a></li>
															<?php } ?>
															</ul>
														</div>
													</td>
												</tr>
												<?php
											}
										}else{
											?>
											<tr>
												<th colspan="100" style="text-align:center;">
													<?php echo (isset($_POST['searchInventory']))?"0 Records Found!!":"0 Records!!" ?>
												</th>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
								<div class="col-xs-12 text-center">
									<?php
									if($found_records > $total){
										$get_url = "";
										foreach($_GET as $key => $value){
											$get_url .= ($key == 'page')?"":"&".$key."=".$value;
										}
										?>
										<nav>
											<ul class="pagination">
												<?php
												$count = $found_records;
												$total_pages = ceil($count/$total);
												$i = 1;
												$thisFileName = basename($_SERVER['PHP_SELF']);
												if(isset($this_page) && $this_page>0){
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
													</li>
													<?php
												}
												if(isset($this_page) && $this_page>=1){
													$prev = $this_page;
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
													</li>
													<?php
												}
												$this_page_act = $this_page;
												$this_page_act++;
												while($total_pages>=$i){
													$left = $this_page_act-5;
													$right = $this_page_act+5;
													if($left<=$i && $i<=$right){
														$current_page = ($i == $this_page_act)?"active":"";
														?>
														<li class="<?php echo $current_page; ?>">
															<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
														</li>
														<?php
													}
													$i++;
												}
												$this_page++;
												if(isset($this_page) && $this_page<$total_pages){
													$next = $this_page;
													?>
													<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
													<?php
												}
												if(isset($this_page) && $this_page<$total_pages){
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
													</li>
												</ul>
											</nav>
											<?php
										}
									}
									?>
								</div>
							</div> <!--End bodyTab1-->
							<div style="height:0px;clear:both"></div>
							<div id="bodyTab2" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"block":"none"; ?>;" >
								<div id="form">
									<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

										<div class="caption"></div>
										<div class="message red"><?php echo (isset($message))?$message:""; ?></div>
										<div class="clear"></div>

										<div class="caption">From Date </div>
										<div class="field">
											<input type="text" class="form-control datepicker" name="fromDate" style="width:150px;" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Date </div>
										<div class="field">
											<input type="text" class="form-control datepicker" name="toDate" style="width:150px;" value="<?php echo date('d-m-Y'); ?>" />
										</div>
										<div class="clear"></div>

										<div class="caption">Customer</div>
										<div class="field" style="width:305px;">
											<select class="selectpicker form-control"
											name='supplierAccCode'
											data-style="btn-default"
											data-live-search="true" style="border:none">
											<option selected value=""></option>
											<?php
											if(mysql_num_rows($cashAccounts)){
												while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
													?>
													<option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
													<?php
												}
											}
											?>
											<?php
											if(mysql_num_rows($customersList)){
												while($account = mysql_fetch_array($customersList)){
													?>
													<option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
									<div class="clear"></div>

									<div class="caption">Bill No</div>
									<div class="field">
										<input type="text" value="" name="billNum" class="form-control" />
									</div>
									<div class="clear"></div>
									<div class="caption"></div>
									<div class="field">
										<input type="submit" value="Search" name="search" class="button"/>
									</div>
									<div class="clear"></div>
								</form>
							</div><!--form-->
						</div> <!-- End bodyTab2 -->
					</div> <!-- End .content-box-content -->
				</div> <!-- End .content-box -->
			</div><!--body-wrapper-->
			<div id="xfade"></div>
		</body>
		</html>
		<?php include('conn.close.php'); ?>
		<script>
		$(document).ready(function() {
			$(".shownCode,.loader").hide();
			$("input.supplierTitle").keyup(function(){
				$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
			});
			$(window).keydown(function(e){
				if(e.keyCode==113){
					e.preventDefault();
					window.location.href = "<?php echo "inventory-details.php"; ?>";
				}
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>
				$("#bodyTab2 .dropdown-toggle").focus();
				$("#bodyTab2 billNum").setFocusTo("#bodyTab2 input[type='submit']");
				$("#bodyTab2 select").change(function(){
					$("#bodyTab2 input[name='billNum']").focus();
				});
				<?php } ?>
			});
			<?php echo (isset($_POST['searchInventory']))?"tab('1', '1', '2');":""; ?>

			</script>
