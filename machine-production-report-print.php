<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/machine_production.php');
	include('common/classes/machine_production_details.php');
	include('common/classes/machines.php');
	include('common/classes/customers.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_inward.php');
	include('common/classes/emb_products.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('machine-production-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objMachineProduction 				= new MachineProduction();
	$objMachineProductionDetails  = new MachineProductionDetails();
	$objMachines  								= new machines();
	$objCustomers 								= new customers();
	$objChartOfAccounts  					= new ChartOfAccounts();
	$objEmbroideryInward   				= new EmbroideryInward();
	$objEmbProducts  							= new EmbProducts();
	$objCompanyDetails 						= new CompanyDetails();

	$productList = $objEmbroideryInward->getProductList();

	$machine_production_id 				= 0;
	$machine_production_detail_id = 0;

	$report_date  = isset($_GET['report_date'])?$_GET['report_date']:date('Y-m-d');
	$report_date  = date('Y-m-d',strtotime($report_date));
	$company_info = $objCompanyDetails->getActiveProfile();

	if(isset($_GET['action'])){
		$message = 'Record '.$_GET['action'].' Successfully';
	}

	$customerList 	= $objCustomers->getList();

	$report_main 		= NULL;
	$report_details = NULL;

	$report 				= false;

	$machine_production_summary_report = array();

	/*
	$machine_production_summary_report structure

	indexes => 1 => MACHINE_ID
	indexes => 1 => PARTY_ACC_CODE
	indexes => 1 => array() => ROWS
	*/
?>
<!DOCTYPE html>

<head>
		<meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" 															type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" 															type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" 														type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" 															type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" 															type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" 														type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/scrollbar.css"	 												type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" 											type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css"   		type="text/css"	type="text/css" />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css"   type="text/css" />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css"   type="text/css" />
	<style media="screen">
		div.new_item_div table{
			width: 1490px !important;
		}
		div.content-box-content{
			overflow: auto !important;
			width: 100%;
		}
		div.new_item_div{
			width: 100%;
		}
		div#bodyTab1,div#form{
			width: 1490px !important;
		}
		div.new_item_div table input[type=text]{
			padding: 2px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/machine.production.config.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!--End#sidebar-->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Daily Machine Production Report</p>
								<span id="tabPanel">
									<div class="tabPanel">
										<a href="machine-production-report-list.php"><div class="tab">List</div></a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>"><div class="tabSelected">Report</div></a>
									</div>
								</span>
                <div class="clear"></div>
            </div><!--End.content-box-header-->
            <div class="content-box-content">
                <div id="bodyTab1" style="padding:10px;">
									<button id="printReport" class="btn btn-default pull-left printStart noprint" style="margin-right: 10px;"><i class="fa fa-print"></i> Print </button>
									<div class="clear"></div>
									<hr />
									<div class="clear"></div>
									<div class="print_able">
										<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
											<p style="text-align: left;font-size:20px;margin: 0px;padding: 0px;">
												<?php echo $company_info['NAME']; ?>
												<br>
												Machine Production Report - <?php echo date("d-m-Y",strtotime($report_date)); ?>
												<span class="pull-right" style="font-size:14px !important;">Report Generated On: <?php echo date('d-m-Y'); ?></span>
											</p>
										</div>
										<div class="clear"></div>
<?php
									$final_pl_amount = 0;
									$machines = $objMachines->getList();
									if(mysql_num_rows($machines)){
										while($machine = mysql_fetch_array($machines)){
											$machine_prod_main = $objMachineProduction->getDetailsByMachineId($machine['ID'],$report_date);
?>
									<div class="machine_container tablePage">
										<div class="col-md-12" style="margin-bottom:5px;margin-top:5px">
											<h4>
												<span style="float:left;"><?php echo $machine['NAME']; ?></span>
												<span style="float:right;font-size: 16px !important;font-weight: normal !important;margin-left: 10px;"><?php echo ($machine_prod_main['COMMANDER_SHIFT_A']=='')?"":"Shift A Commander : ".$machine_prod_main['COMMANDER_SHIFT_A']; ?>  </span>
												<div class="clear"></div>
												<span style="float: left"><?php echo ($machine_prod_main['NIDDLE_SETTING']=='')?"":" Niddle Set As: ".$machine_prod_main['NIDDLE_SETTING']; ?> </span>
												<span style="float:right;font-size: 16px !important;font-weight: normal !important;margin-left: 10px;"><?php echo ($machine_prod_main['COMMANDER_SHIFT_B']=='')?"":"Shift B Commander : ".$machine_prod_main['COMMANDER_SHIFT_B']; ?>  </span>
												<div class="clear"></div>
											</h4>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
										<div class="table-responssive">
											<table class="table">
													<tbody>
														<tr>
															 <th class="text-center" style="width: 8%;"><b>Party</b></th>
															 <th class="text-center" style="width: 6%;"><b>Fabric Type</b></th>
															 <th class="text-center" style="width: 6%;"><b>Thread Type</b></th>
															 <th class="text-center" style="width: 3%;"><b>Design#</b></th>
															 <th class="text-center" style="width: 6%;"><b>Product</b></th>
															 <th class="text-center" style="width: 3%;"><b>Prod Shift A</b></th>
															 <th class="text-center" style="width: 3%;"><b>Prod Shift B</b></th>
															 <th class="text-center" style="width: 3%;"><b>TP</b></th>
															 <th class="text-center" style="width: 3%;"><b>Yard</b></th>
															 <th class="text-center" style="width: 3%;"><b>Thread Wt (KG)</b></th>
															 <th class="text-center" style="width: 3%;"><b>Rate/KG</b></th>
															 <th class="text-center" style="width: 3%;"><b>Thread Cost/KG</b></th>
															 <th class="text-center" style="width: 3%;"><b>Thread Cost/TP</b></th>
															 <th class="text-center" style="width: 2%;"><b>PerThaan Stitch</b></th>
															 <th class="text-center" style="width: 4%;"><b>Stitch Sh. A</b></th>
															 <th class="text-center" style="width: 4%;"><b>Stitch Sh. B</b></th>
															 <th class="text-center" style="width: 3%;"><b>Rate</b></th>
															 <th class="text-center" style="width: 3%;"><b>Sale</b></th>
														</tr>
<?php

											if(isset($machine_production_summary_report[$machine['ID']])){
												$machine_production_summary_report[$machine['ID']] = array();
											}
											$machine_id = $machine['ID'];
											$machineProductions = $objMachineProduction->getMachineProductionList($report_date,$machine_id);

											if(mysql_num_rows($machineProductions)){
												$stitch_row 					= 0;
												$shift_a_row 					= 0;
												$shift_b_row 					= 0;
												$tp_row 							= 0;
												$yard_row 						= 0;
												$sale_row 						= 0;
												$wght_thread_row 			= 0;
												$stitch_a_row 				= 0;
												$stitch_b_row 				= 0;
												$tota_stitch_row			= 0;
												$thread_price_kg_total= 0;
												$thread_price_total		= 0;
												while($machineProduction = mysql_fetch_assoc($machineProductions)){
													if(!isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']])){
														$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']] = array();
													}
		?>
					                            <tr data-id="<?php echo $machineProduction['ID']; ?>" class="machine">
																				<td style="text-align:left" class="fab_type"><?php echo $objChartOfAccounts->getAccountTitleByCode($machineProduction['PARTY_ACC_CODE']); ?></td>
					                               <td style="text-align:center" class="fab_type"><?php echo $machineProduction['FABRIC_TYPE']; ?></td>
					                               <td style="text-align:center" class="thd_type"><?php echo $machineProduction['THREAD_TYPE']; ?></td>
					                               <td style="text-align:center" class="design_no"><?php echo $machineProduction['DESIGN_NO']; ?></td>
					                               <td style="text-align:center" class="item1"><?php echo $objEmbProducts->getTitle($machineProduction['ITEM']); ?></td>
					                               <td style="text-align:center" class="shifta"><?php echo $machineProduction['SHIFT_A']; ?></td>
					                               <td style="text-align:center" class="shiftb"><?php echo $machineProduction['SHIFT_B']; ?></td>
					                               <td style="text-align:center" class="tp1"><?php echo $machineProduction['TP']; ?></td>
					                               <td style="text-align:center" class="yard1"><?php echo $machineProduction['YARD']; ?></td>
					                               <td style="text-align:center" class="thr_weight"><?php echo $machineProduction['THREAD_WEIGHT']; ?></td>
					                               <td style="text-align:center" class="at_rate"><?php echo $machineProduction['AT_RATE']; ?></td>
					                               <td style="text-align:center" class="thrd_price"><?php echo number_format($machineProduction['THREAD_PRICE'],2); ?></td>
					                               <td style="text-align:center" class="to_price"><?php echo number_format($machineProduction['TOTAL_PRICE'],2); ?></td>
					                               <td style="text-align:center" class="stitcht"><?php echo $machineProduction['STITCH']; ?></td>
					                               <td style="text-align:center" class="stitcha"><?php echo $machineProduction['STITCH_A']; ?></td>
					                               <td style="text-align:center" class="stitchb"><?php echo $machineProduction['STITCH_B']; ?></td>
					                               <td style="text-align:center" class="sa_rate"><?php echo number_format($machineProduction['RATE'],2); ?></td>
					                               <td style="text-align:center" class="sale1"><?php echo number_format($machineProduction['SALE'],2); ?></td>
					                            </tr>
<?php
											$stitch_row 				+= $machineProduction['STITCH'];
											$shift_a_row 				+= $machineProduction['SHIFT_A'];
											$shift_b_row 				+= $machineProduction['SHIFT_B'];
											$tp_row 						+= $machineProduction['TP'];
											$yard_row 					+= $machineProduction['YARD'];
											$sale_row 					+= $machineProduction['SALE'];
											$wght_thread_row 		+= $machineProduction['THREAD_WEIGHT'];
											$thread_price_kg_total += $machineProduction['THREAD_PRICE'];
											$thread_price_total += $machineProduction['TOTAL_PRICE'];
											$stitch_a_row 			+= $machineProduction['STITCH_A'];
											$stitch_b_row 			+= $machineProduction['STITCH_B'];
											$tota_stitch_row 		+= $machineProduction['STITCH'];

											if(isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['STITCHES'])){
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['STITCHES'] += $machineProduction['STITCH_A']+$machineProduction['STITCH_B'];
											}else{
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['STITCHES']  = $machineProduction['STITCH_A']+$machineProduction['STITCH_B'];
											}
											if(isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['REQUIRED_STITCHES'])){
												//echo $machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['REQUIRED_STITCHES'] += $machine_prod_main['STITCH_SHIFT_A']+$machine_prod_main['STITCH_SHIFT_B'];
											}else{
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['REQUIRED_STITCHES']  = $machine_prod_main['STITCH_SHIFT_A']+$machine_prod_main['STITCH_SHIFT_B'];
											}
											if(isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['DIFFERENCE_STITCHES'])){
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['DIFFERENCE_STITCHES'] += 0; //$difference;
											}else{
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['DIFFERENCE_STITCHES']  = 0; //$difference;
											}
											if(isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['SALE'])){
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['SALE'] += $machineProduction['SALE'];
											}else{
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['SALE']  = $machineProduction['SALE'];
											}
											if(isset($machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['THAANS'])){
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['THAANS'] += $machineProduction['TP'];
											}else{
												$machine_production_summary_report[$machine['ID']][$machineProduction['PARTY_ACC_CODE']]['THAANS']  = $machineProduction['TP'];
											}
										}
							}

							$trace_one 				= ($machineProduction['STITCH_SHIFT_A']+$machineProduction['STITCH_SHIFT_B']);
							$sale_rate 				= $machineProduction['SALE_RATE'];
							$saleCal 	 				= (($stitch_a_row+$stitch_b_row)/1000)*15;
							$trace_two 				= ($trace_one==0)?0:number_format($sale_row/$trace_one,2);
							$thread_expense 	= $thread_price_total+$machineProduction['EXPENSE'];
							$sale_final 			= $sale_row - $thread_expense;
							$final_pl_amount += $sale_final;
							$difference 			= ($machine_prod_main['STITCH_SHIFT_A']+$machine_prod_main['STITCH_SHIFT_B']) - ( $stitch_a_row+$stitch_b_row );
							$aveStitch 				= ($machineProduction['STITCH_SHIFT_A']+$machineProduction['STITCH_SHIFT_B'])/2;
?>
					                        </tbody>
					                        <tfoot>
					                        	<tr>
					                        		<td class="text-center" colspan="5"><b> - - - </b></td>
					                        		<td class="text-center"><b><?php echo $shift_a_row; ?></b></td>
					                        		<td class="text-center"><b><?php echo $shift_b_row; ?></b></td>
					                        		<td class="text-center"><b><?php echo $tp_row; ?></b></td>
					                        		<td class="totalYards text-center"><b><?php echo $yard_row; ?></b></td>
					                        		<td class="text-center"><b><?php echo $wght_thread_row; ?></b></td>
					                        		<td class="text-center"><b> - - - </b></td>
					                        		<td class="text-center"><b><?php echo number_format($thread_price_kg_total,2); ?></b></td>
					                        		<td class="text-center"><b><?php echo number_format($thread_price_total,2); ?></b></td>
					                        		<td class="text-center"><b><?php echo $tota_stitch_row; ?></b></td>
					                        		<td class="text-center"><b><?php echo $stitch_a_row; ?></b></td>
					                        		<td class="text-center"><b><?php echo $stitch_b_row; ?></b></td>
					                        		<td class="text-center"><b> - - - </b></td>
					                        		<td class="text-center"><b><?php echo $sale_row; ?></b></td>
					                        	</tr>
					                        	<tr>
					                        		<td class="text-center" colspan="10"><?php echo ($machine_prod_main['COMMENT']=='')?"":"Fault Note :  ".$machine_prod_main['COMMENT']; ?></td>
																			<td class="text-center" colspan="4">Machine (Max) Stitches Shiftwise</td>
					                        		<td class="text-center"><?php echo $machine_prod_main['STITCH_SHIFT_A']; ?></td>
					                        		<td class="text-center"><?php echo $machine_prod_main['STITCH_SHIFT_B'] ; ?> </td>
					                        		<td class="text-center" colspan="2"> - - - </td>
					                        	</tr>
					                        </tfoot>
					                    </table>
					                 	</div><!--table-responssive-->
														<div class="clear"></div>
														<?php /*
														<div class="detail">
									                 	<div class="clear"></div>
									                 		<table style="width: 50%;float: left;">
										                 		<tbody>
										                 			<tr>
										                 				<td style="text-align: right;font-size: 12px !important;"> Total Expense : </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo $thread_price_total; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"> + </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo $machineProduction['EXPENSE']; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"> = </td>
										                 				<td class="totalExpense" style="text-align: center; font-size: 12px !important;"><?php echo $thread_expense; ?></td>
										                 			</tr>

										                 			<tr>
										                 				<td style="text-align: right; font-size: 12px !important;">Sale:<?php //echo $party_title; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo ($stitch_a_row+$stitch_b_row)."/1000*15 "; ?></td>
																						<td style="text-align: center; font-size: 12px !important;"> = <?php //echo  ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo round($saleCal,2)." X " . $sale_rate; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"> = <?php //echo  ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo number_format($saleCal*$sale_rate,2) ; ?></td>
										                 			</tr>

										                 			<tr>
										                 				<td style="text-align: right; font-size: 12px !important;">Income/Loss:</td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo number_format($saleCal*$sale_rate,2) ; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;"> - </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo $thread_expense; ?></td>
										                 				<td style="text-align: center; font-size: 12px !important;">=</td>
										                 				<td style="text-align: center; font-size: 12px !important;" class="incomeLoss"><?php echo ($saleCal*$sale_rate)-$thread_expense; ?></td>
										                 			</tr>

										                 		</tbody>
										                 	</table>
									                 		<table style="width: 50%;float: left;">
										                 		<tbody>
										                 			<tr>
										                 				<td style="text-align: right; font-size: 12px !important;">Per 1000 Stitches Cost</td>
										                 				<td style="text-align: center; font-size: 12px !important;"> = </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo (($machineProduction['STITCH_SHIFT_A']+$machineProduction['STITCH_SHIFT_B'])==0)?"0":round(($thread_expense/($machineProduction['STITCH_SHIFT_A']+$machineProduction['STITCH_SHIFT_B'])) * 1000,3); ?></td>
										                 				<td style="text-align: right; font-size: 12px !important;">Per Yard Cost</td>
										                 				<td style="text-align: center; font-size: 12px !important;"> = </td>
										                 				<td style="text-align: center; font-size: 12px !important;" class="pyCost"></td>
										                 			</tr>
										                 			<tr>
										                 				<td style="text-align: right; font-size: 12px !important;">Stitch (<?php echo $difference<0?"excess":"less"; ?>) </td>
										                 				<td style="text-align: center; font-size: 12px !important;"> =  </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo round(str_ireplace('-','',$difference),2); ?></td>
										                 				<td style="text-align: right; font-size: 12px !important;">Total Stitches Ave</td>
										                 				<td style="text-align: center; font-size: 12px !important;"> = </td>
										                 				<td style="text-align: center; font-size: 12px !important;"><?php echo round($aveStitch,2); //echo $thread_expense; ?></td>
										                 			</tr>
										                 			<tr>
										                 				<td style="text-align: right; font-size: 12px !important;">Fault Note:</td>
										                 				<td style="text-align: left; font-size: 12px !important;" colspan="5"><?php echo $machineProduction['COMMENT']; ?></td>
										                 			</tr>
										                 		</tbody>
										                 	</table>
									                 </div>
																	 */ ?>
									     		</div><!--machine_container-->
													<div class="clear"></div>
									<?php
								}

									?>
									<div class="tablePage">
										<div class="pull-left">
											<p style="padding: 10px;font-size: 18px;">
												Daily Production Report
											</p>
										</div>
										<div class="pull-right">
											<p style="padding: 10px;font-size: 18px;">
												<?php  /*
												<?php echo ($final_pl_amount>=0)?"Total Income":"Total Loss"; ?> = <span class="incomeLossTotal"><?php echo str_ireplace('-','',number_format($final_pl_amount,2)); ?></span>
												*/ ?>
											</p>
										</div>
										<table style="width: 100%;margin-top: 25px 0;text-align: center">
											<tr>
												<th class="text-center">Date</th>
												<th class="text-center">Previous</th>
												<th class="text-center">Current</th>
												<th class="text-center">Total</th>
											</tr>
											<tr>
												<td class="text-center">
													<?php echo date("d-M-Y") ?>
												</td>
												<td class="text-center">
													<?php echo $prev_pnl = round($objMachineProductionDetails->getPrevMachineProductionPnL($report_date),2); ?>
												</td>
												<td class="text-center">
													<?php echo round($final_pl_amount,2); ?>
												</td>
												<td class="text-center">
													<?php echo number_format($final_pl_amount+$prev_pnl,2); ?>
												</td>
											</tr>
										</table>
									</div>
<?php
	}
	if(count($machine_production_summary_report)){
?>
<div class="tablePage">
	<div class="pull-left">
		<p style="padding: 10px;font-size: 18px;">
			Machine Production Summary  - <?php echo date('d-m-Y',strtotime($report_date)); ?>
		</p>
	</div>
	<table style="width: 100%;margin-top: 25px 0;text-align: center;">
		<tbody>
			<tr>
				<th class="text-center"><b>Machine #</b></th>
				<th class="text-center"><b>Party Name</b></th>
				<th class="text-center"><b>Stitches Actual</b></th>
				<th class="text-center"><b>Stitches Required</b></th>
				<th class="text-center"><b>Difference Stitches</b></th>
				<th class="text-center"><b>Efficiency</b></th>
				<th class="text-center"><b>Amount</b></th>
				<th class="text-center"><b>Thaans</b></th>
			</tr>
<?php
	$this_mach 						= 'x';

	$grand_total_stitches 			= 0;
	$grand_total_req_stitches 	= 0;
	$grand_total_diff_stitches 	= 0;
	$grand_total_efficiency 		= 0;
	$grand_total_amount 				= 0;
	$grand_total_thaans 				= 0;

	foreach ($machine_production_summary_report as $machine_id => $party_code_rows){
		$total_stitches 			= 0;
		$total_req_stitches 	= 0;
		$total_diff_stitches 	= 0;
		$total_efficiency 		= 0;
		$total_amount 				= 0;
		$total_thaans 				= 0;
		foreach ($party_code_rows as $party_code => $records) {
?>
			<tr>
				<td class="text-center"><?php echo ($this_mach!=$machine_id)?$objMachines->getRecordDetailsValue($machine_id,'MACHINE_NO'):""; ?></td>
				<td class="text-left"><?php echo $objChartOfAccounts->getAccountTitleByCode($party_code); ?></td>
				<td class="text-center"><?php echo $records['STITCHES']; ?></td>
				<td class="text-center"><?php //echo $records['REQUIRED_STITCHES']; ?></td>
				<td class="text-center"><?php //echo $records['DIFFERENCE_STITCHES']; ?></td>
				<td class="text-center"><?php //echo ($records['REQUIRED_STITCHES']==0)?0:number_format((($records['STITCHES']/$records['REQUIRED_STITCHES']) - 1) * 100,2); ?></td>
				<td class="text-center"><?php echo number_format( $records['SALE'] ,2); ?></td>
				<td class="text-center"><?php echo $records['THAANS']; ?></td>
			</tr>
<?php
			$total_stitches      += $records['STITCHES'];
			$total_diff_stitches += $records['DIFFERENCE_STITCHES'];
			if($records['REQUIRED_STITCHES']>0){
				$total_efficiency    += (($records['STITCHES']/$records['REQUIRED_STITCHES']) - 1) * 100;
			}
			$total_amount        += $records['SALE'];
			$total_thaans        += $records['THAANS'];
		}
		$total_req_stitches  += $records['REQUIRED_STITCHES'];
?>
			<tr>
				<th class="text-center" colspan="2"> <b>Total</b> </th>
				<th class="text-center"><b><?php echo $total_stitches; ?></b></th>
				<th class="text-center"><b><?php echo $total_req_stitches; ?></b></th>
				<th class="text-center"><b><?php echo $total_stitches-$total_req_stitches; ?></b></th>
				<th class="text-center"><b><?php echo number_format( ($total_stitches/$total_req_stitches)-1 ,2); ?> % </b></th>
				<th class="text-center"><b><?php echo number_format($total_amount,2); ?></b></th>
				<th class="text-center"><b><?php echo $total_thaans; ?></b></th>
			</tr>
<?php

		$grand_total_stitches 			+= $total_stitches;
		$grand_total_req_stitches 	+= $total_req_stitches;
		$grand_total_diff_stitches 	+= ($total_stitches-$total_req_stitches);
		$grand_total_efficiency 		+= ($total_stitches/$total_req_stitches)-1;
		$grand_total_amount 				+= $total_amount;
		$grand_total_thaans 				+= $total_thaans;

	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th class="text-center" colspan="2"> <b>Day Total : </b> </th>
				<th class="text-center"><b><?php echo $grand_total_stitches; ?></b></th>
				<th class="text-center"><b><?php echo $grand_total_req_stitches; ?></b></th>
				<th class="text-center"><b><?php echo $grand_total_diff_stitches; ?></b></th>
				<th class="text-center"><b><?php echo number_format($grand_total_efficiency,2); ?> % </b></th>
				<th class="text-center"><b><?php echo number_format($grand_total_amount,2); ?> </b></th>
				<th class="text-center"><b><?php echo $grand_total_thaans; ?></b></th>
			</tr>
		</tfoot>
	</table>
</div>
<?php
}
?>

									</div><!--print_able--->
                </div> <!--End bodyTab1-->
								<div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
	<div id="xfade"></div>
	<div id="fade"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).calculateIncomeLossTotals();
		$(".pyCost").text(((parseFloat($('.totalExpense').text())||0)/(parseFloat($('.totalYards').text())||0)).toFixed(3));
		//daily diff report calculation start
		$(".curDiff").val($("td.incomeLossTotal").text()) || 0;
		$(".totalDiff").val($("td.incomeLossTotal").text()) || 0;
		$(".prvDiff").bind('keyup blur',function(){
			var curDiff = parseFloat($(".curDiff").val()) || 0;
			var prvDiff = parseFloat($(".prvDiff").val()) || 0;
			$(".totalDiff").val(prvDiff + curDiff);
		});
		//daily diff report close
		$("input[name='prod_shiftA'],input[name='prod_shiftB']").bind('keyup blur',function(){
			var pShiftA =  parseInt($("input[name='prod_shiftA']").val()) || 0;
			var pShiftB = parseInt($("input[name='prod_shiftB']").val()) || 0;
			var totalProd = pShiftA + pShiftB || 0;
			$("input[name='total_prod']").val(totalProd);
			$("input[name='yard']").val(totalProd*15);
			//$("input[name='tp']").val(totalProd);
		});

		$("input[name='thread_weight'],input[name='rate']").bind('keyup blur',function(){
			var total_rate = $("input[name='thread_weight']").val()*$("input[name='rate']").val();
			total_rate = (total_rate/1000);
			$("input[name='thread_price']").val((total_rate).toFixed(2));
			$("input[name='total_price']").val(($("input[name='total_prod']").val()*total_rate).toFixed(2));
			//$("input[name='tp']").val(totalProd);
		});
		//stitch calculation
		$("input[name='stitch'],input[name='stitch_a'],input[name='stitch_b']").bind('change blur keyup',function(){
			var stitches  = parseInt($("input[name='stitch']").val())||0;
			var pShiftA =  parseInt($("input[name='prod_shiftA']").val()) || 0;
			var pShiftB = parseInt($("input[name='prod_shiftB']").val()) || 0;
			$("input[name='stitch_a']").val(pShiftA * stitches)||0;
			$("input[name='stitch_b']").val(pShiftB * stitches)||0;

		});
		//sale calculation
		$("input[name='sale_rate']").on('blur',function(){
			var sale_rate  = parseFloat($(this).val())||0;
			$(this).val(sale_rate);

			var stitch_a = parseInt($("input[name='stitch_a']").val());
			var stitch_b = parseInt($("input[name='stitch_b']").val());
			var total_stitch = stitch_a + stitch_b;
			//alert(total_stitch);
			$("input[name='sale']").val((total_stitch*15*sale_rate/1000).toFixed(2));

		});
		$(".new_machine").click(function(){
			window.location.href = '<?php echo basename($_SERVER['PHP_SELF']); ?>';
		});
//popup
		$("input[name='prod_shiftA1'],input[name='prod_shiftB1']").bind('keyup blur',function(){
			var pShiftA =  parseInt($("input[name='prod_shiftA1']").val()) || 0;
			var pShiftB = parseInt($("input[name='prod_shiftB1']").val()) || 0;
			var totalProd = pShiftA + pShiftB || 0;
			$("input[name='total_prod1']").val(totalProd);
			$("input[name='yard1']").val(totalProd*15);
			//$("input[name='tp']").val(totalProd);
		});

		$("input[name='thread_weight1'],input[name='rate1']").bind('keyup blur',function(){
			var total_rate = $("input[name='thread_weight1']").val()*$("input[name='rate1']").val();
			total_rate = (total_rate/1000).toFixed(2);
			$("input[name='thread_price1']").val(total_rate);
			$("input[name='total_price1']").val(($("input[name='total_prod1']").val()*total_rate).toFixed(2));
			//$("input[name='tp']").val(totalProd);
		});
		//stitch calculation
		$("input[name='stitch1'],input[name='stitch_a1'],input[name='stitch_b1']").bind('change blur keyup',function(){
			var stitches  = parseInt($("input[name='stitch1']").val())||0;
			var pShiftA =  parseInt($("input[name='prod_shiftA1']").val()) || 0;
			var pShiftB = parseInt($("input[name='prod_shiftB1']").val()) || 0;
			$("input[name='stitch_a1']").val(pShiftA * stitches)||0;
			$("input[name='stitch_b1']").val(pShiftB * stitches)||0;

		});
		//sale calculation
		$("input[name='sale_rate1']").on('blur',function(){
			var sale_rate  = parseFloat($(this).val()/1000)||0;
			$(this).val(sale_rate);

			var stitch_a = parseInt($("input[name='stitch_a1']").val());
			var stitch_b = parseInt($("input[name='stitch_b1']").val());
			var total_stitch = stitch_a + stitch_b;
			//alert(total_stitch);
			$("input[name='sale1']").val(total_stitch*15*sale_rate);

		});
//popup

		<?php
			if(isset($message)){
		?>
			displayMessage('<?php echo $message; ?>');
		<?php
			}
		?>
	});
</script>
<script>
	$(document).ready(function(){
		$(window).load(function(){
			$("div.machine_container").each(function(){
				if($(this).find("tr.machine").length == 0){
					$(this).remove();
				}
			});
		});
		$("#printReport").click(function(){
			$(".print_able").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Sitsbook",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			});
		});
	});
</script>
