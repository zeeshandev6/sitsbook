<?php
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('lib/thumbnail.php');
	include('common/classes/departments.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/measure.php');

	//Permission
  if(!in_array('items-managment',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objDepartments   = new Departments();
	$objItems         = new Items();
	$objItemsCategory = new itemCategory();
	$objMeasures      = new Measures();
	$objConfigs       = new Configs();

	$itemDetails   = NULL;
	$sale_scanner  = $objConfigs->get_config('SALE_SCANNER');
	$discount_type = $objConfigs->get_config('DISCOUNT');
	$use_cartons   = $objConfigs->get_config('USE_CARTONS');

	$use_image     = $objConfigs->get_config('USE_PRODUCT_IMAGE');

	$barcode_print_flag = $objConfigs->get_config('BARCODE_PRINT');
	$barcode_print_flag = ($barcode_print_flag == 'Y')?true:false;

	$path          = 'uploads/products/';
	$last_id       = $objItems->getNextAutoIncrement();

	if(isset($_GET['go2'])){
		echo "disabled";
		exit();
		$items_list  = $objItems->getListAll();
		$last_id     = 1;
		while($row = mysql_fetch_assoc($items_list)){
			$q = "UPDATE sale_details SET COST_PRICE = ".$row['PURCHASE_PRICE']." WHERE ITEM_ID = ".$row['ID'];
			mysql_query($q);
			echo mysql_error();
		}
		exit();
	}

	if(isset($_GET['go'])){
		exit();
		$items_list  = $objItems->getListAll();
		$last_id      = 1;
		while($row = mysql_fetch_assoc($items_list)){
			$max_length  = 4;
			$this_length = strlen($last_id);
			$zero_num    = $max_length-$this_length;
			$item_code   = (int)$last_id;
			$zeros       = '';
			for($v=0; $v < $zero_num; $v++){
				$zeros .= '0';
			}
			$item_code = $zeros.$item_code;
			$item_code = '0'.$item_code;

			$query = "UPDATE items SET ITEM_BARCODE = '".$item_code."' WHERE ID = ".$row['ID'];
			$updated = mysql_query($query);
			if($updated){
				$last_id++;
			}
		}
		exit();
	}

	$max_length  = 4;
	$this_length = strlen($last_id+1);
	$zero_num    = $max_length-$this_length;
	$item_code   = (int)$last_id+1;
	$zeros       = '';
	for($v=0; $v < $zero_num; $v++){
		$zeros .= '0';
	}
	$item_code = $zeros.$item_code;
	$bar_used = $objItems->barcodeAvailabilityCheck($item_code,0);
	if($bar_used){
		$item_code = '0'.$item_code;
	}
	if(isset($_POST['remove_image'])){
		$item_id  = (int)mysql_real_escape_string($_POST['remove_image']);
		if($item_id>0){
			$itemDetails = $objItems->getRecordDetails($item_id);
			$image_name_before = $itemDetails['IMAGE'];
			if($image_name_before != ''){
				if(file_exists($path.$image_name_before)){
					unlink($path.$image_name_before);
					unlink($path."thumb_".$image_name_before);
				}
			}
			$objItems->setImage($item_id,'');
		}
		exit();
	}
	if(isset($_POST['addItem'])){
		$objItems->itemCatId       = isset($_POST['itemCategory'])?$_POST['itemCategory']:0;
		$objItems->barcode         = isset($_POST['barcode'])?$_POST['barcode']:"";
		$objItems->name            = mysql_real_escape_string($_POST['itemTitle']);
		$objItems->urdu_name       = isset($_POST['urdu_name'])?mysql_real_escape_string($_POST['urdu_name']):"";
		$objItems->hs_code         = (isset($_POST['hs_code']))?mysql_real_escape_string($_POST['hs_code']):"";
		$objItems->salePrice       = ($_POST['salePrice'] == '')?0:$_POST['salePrice'];
		$objItems->active          = 'Y';
		$objItems->measure	       = isset($_POST['measure'])?mysql_real_escape_string($_POST['measure']):"";
		$objItems->qty_per_carton  = isset($_POST['qty_per_carton'])?mysql_real_escape_string($_POST['qty_per_carton']):"";
		$objItems->rate_carton	   = isset($_POST['rate_carton'])?mysql_real_escape_string($_POST['rate_carton']):"";
		// $objItems->carton_dozen	   = isset($_POST['carton_dozen'])?mysql_real_escape_string($_POST['carton_dozen']):"";
		$objItems->is_carton	   = isset($_POST['is_carton'])?mysql_real_escape_string($_POST['is_carton']):"";
		$objItems->is_dozen	  	   = isset($_POST['is_dozen'])?mysql_real_escape_string($_POST['is_dozen']):"";
		$objItems->dozen_rate	   = isset($_POST['dozen_rate'])?mysql_real_escape_string($_POST['dozen_rate']):"";
		$objItems->minQty	       = isset($_POST['min_qty'])?mysql_real_escape_string($_POST['min_qty']):"";
		$objItems->item_discount   = isset($_POST['item_discount'])?mysql_real_escape_string($_POST['item_discount']):"";
		$objItems->company_name	   = isset($_POST['company_name'])?ucfirst(mysql_real_escape_string($_POST['company_name'])):"";
		$objItems->comment	       = isset($_POST['comment'])?mysql_real_escape_string($_POST['comment']):"";
		$objItems->unit_length     = isset($_POST['unit_length'])?mysql_real_escape_string($_POST['unit_length']):"";
		$objItems->inv_type        = isset($_POST['inv_type'])?$_POST['inv_type']:"N";

		if($objItems->inv_type == 'B'){
			$objItems->barcode = '';
		}

		if($_POST['itemCategory'] == '' || $_POST['itemCategory'] == 0){
			$iid = 0;
			$message = 'Please Select A Category For Item!';
		}else{
			$objItems->save();
			$iid = mysql_insert_id();
			if($objItems->inv_type != 'B'){
				$current_stock = $objItems->getStock($iid);
				$stox = $_POST['units'];
				$price  = $_POST['unit_price'];
				$total_cost = $price*$stox;
				$updated = $objItems->stock_entry($iid,$stox,$total_cost,$price);
			}

			// Image upload handling ///
			if(isset($_FILES['image'])&&$_FILES['image']['error'] == 0){
				$file_extension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
				$file_dimensions = getimagesize($_FILES['image']['tmp_name']); // in bytes
				$imageWidth = $file_dimensions[0];
				$imageHeight = $file_dimensions[1];
				$file_types = array();
				$file_types[] = 'jpg';$file_types[] = 'gif';$file_types[] = 'png';
				if(in_array($file_extension, $file_types)){
					$renaming_mask = md5(date("ymd").time().$iid).".".$file_extension;
					move_uploaded_file($_FILES['image']['tmp_name'],$path.$renaming_mask);
					if($file_extension == 'jpg'){
						make_thumb_jpg($path.$renaming_mask,$path.'thumb_'.$renaming_mask,207);
					}elseif($file_extension == 'png'){
						make_thumb_png($path.$renaming_mask,$path.'thumb_'.$renaming_mask,207);
					}
					$objItems->setImage($iid, $renaming_mask);
				}else{
					$message = 'Image type not supported!';
				}
			}
			// Image upload handling ///
			if($iid > 0){
				echo "<script>window.location.replace(\"".basename($_SERVER['PHP_SELF'])."?id=".$iid."&added\");</script>";
				exit();
			}else{
				$message = "Error! Record Could Not Be Saved!";
			}
		}
	}
	if(isset($_POST['updateItem'])){
		if(!in_array('items-managment-modify',$permissionz) && !$admin){
			echo "Updation not allowed";
			exit();
		}
		$objItems->itemCatId       = isset($_POST['itemCategory'])?$_POST['itemCategory']:0;
		$objItems->barcode         = isset($_POST['barcode'])?$_POST['barcode']:"";
		$objItems->name            = isset($_POST['itemTitle'])?mysql_real_escape_string($_POST['itemTitle']):"";
		$objItems->urdu_name       = isset($_POST['urdu_name'])?mysql_real_escape_string($_POST['urdu_name']):"";
		$objItems->hs_code         = isset($_POST['hs_code'])?mysql_real_escape_string($_POST['hs_code']):"";
		$objItems->salePrice       = isset($_POST['salePrice'])?mysql_real_escape_string($_POST['salePrice']):"";
		$objItems->measure	       = isset($_POST['measure'])?mysql_real_escape_string($_POST['measure']):"";
		$objItems->qty_per_carton  = isset($_POST['qty_per_carton'])?mysql_real_escape_string($_POST['qty_per_carton']):"";
		$objItems->rate_carton	   = isset($_POST['rate_carton'])?mysql_real_escape_string($_POST['rate_carton']):"";
		// $objItems->carton_dozen	   = isset($_POST['carton_dozen'])?mysql_real_escape_string($_POST['carton_dozen']):"";
		$objItems->is_carton	   = isset($_POST['is_carton'])?mysql_real_escape_string($_POST['is_carton']):"";
		$objItems->is_dozen	  	   = isset($_POST['is_dozen'])?mysql_real_escape_string($_POST['is_dozen']):"";
		$objItems->dozen_rate	   = isset($_POST['dozen_rate'])?mysql_real_escape_string($_POST['dozen_rate']):"";
		$objItems->minQty	       = isset($_POST['min_qty'])?mysql_real_escape_string($_POST['min_qty']):"";
		$objItems->item_discount   = isset($_POST['item_discount'])?mysql_real_escape_string($_POST['item_discount']):"";
		$objItems->company_name	   = isset($_POST['company_name'])?ucfirst(mysql_real_escape_string($_POST['company_name'])):"";
		$objItems->comment	       = isset($_POST['comment'])?mysql_real_escape_string($_POST['comment']):"";
		$objItems->unit_length     = isset($_POST['unit_length'])?mysql_real_escape_string($_POST['unit_length']):"";
		$objItems->inv_type        = isset($_POST['inv_type'])?$_POST['inv_type']:"N";

		if($_POST['itemCategory'] == '' || $_POST['itemCategory'] == 0){
			$message = 'Please Select A Category For Item!';
		}else{
			$iid = $_POST['itemID'];
			$updated = $objItems->update($iid);
			$item_used = $objItems->beingUsed($iid);

			if($objItems->inv_type != 'B'){
				$current_stock = $objItems->getStock($iid);
				$stox = $_POST['units'];
				$price  = $_POST['unit_price'];
				$total_cost = $price*$stox;
				$updated = $objItems->stock_entry($iid,$stox,$total_cost,$price);
			}

			// Image upload handling ///
			if(isset($_FILES['image'])&&$_FILES['image']['error'] == 0){
				$image_name_before = $objItems->getImage($iid);
				$file_extension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
				$file_dimensions = getimagesize($_FILES['image']['tmp_name']); // in bytes
				$imageWidth = $file_dimensions[0];
				$imageHeight = $file_dimensions[1];
				$file_types = array();
				$file_types[] = 'jpg';$file_types[] = 'gif';$file_types[] = 'png';
				if(in_array($file_extension, $file_types)){
					if($image_name_before != ''){
						if(file_exists($path.$image_name_before))
						{
							unlink($path.$image_name_before);
							unlink($path."thumb_".$image_name_before);
						}
					}
					$renaming_mask = md5(date("ymd").time().$iid).".".$file_extension;
					move_uploaded_file($_FILES['image']['tmp_name'],$path.$renaming_mask);
					if($file_extension == 'jpg'){
						make_thumb_jpg($path.$renaming_mask,$path.'thumb_'.$renaming_mask,207);
					}elseif($file_extension == 'png'){
						make_thumb_png($path.$renaming_mask,$path.'thumb_'.$renaming_mask,207);
					}
					$objItems->setImage($iid, $renaming_mask);
				}else{
					$message = 'Image type not supported!';
				}
			}
			// Image upload handling ///

			if(!$updated){
				$message = "Error! Record Could Not Be Saved!";
			}else{
				$itemtID = $_POST['itemID'];
				if(!isset($_POST['no_redirect'])){
					echo "<script>window.location.replace(\"".basename($_SERVER['PHP_SELF'])."?id=".$itemtID."&updated\");</script>";
					exit();
				}
			}
		}
	}

	$itemCategoryList = $objItemsCategory->getList();
	$inv_type_text = '';
	$beingUsed 	   = false;
	if(isset($_GET['id'])){
		$item_id = mysql_real_escape_string($_GET['id']);
		$itemDetails = $objItems->getRecordDetails($item_id);
		$beingUsed = $objItems->beingUsed($item_id);
		if($itemDetails['INV_TYPE'] == 'B'){
			$inv_type_text    = "Mobile Item";
			$barcode_selected = 'checked="checked"';
			$normal_selected  = '';
		}else{
			$barcode_selected = '';
			$normal_selected  = 'checked="checked"';
			$inv_type_text = "General Item";
		}
	}else{
		if($mobileAddon == 'Y'){
			$barcode_selected = 'checked="checked"';
			$normal_selected  = '';
		}else{
			$barcode_selected = '';
			$normal_selected  = 'checked="checked"';
		}
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="resource/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
	<link href="resource/css/lightbox.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	input.error{
		border-color: red !important;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("select").selectpicker();
		$("input.item-barcode").barcodeAvailable();
		$(".numOnly").numericOnly();
		$(".numFlotOnly").numericFloatOnly();
		$(".delete_thumb").click(function(){
			var item_id = parseInt($("input[name=itemID]").val())||0;
			if(item_id>0){
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
					$.post("item-details.php", {remove_image : item_id}, function(data){});
				});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
				});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#fade").fadeOut('fast');
				});
			}
		});
		<?php
		if(isset($message)){
			?>
			displayMessage('<?php echo $message; ?>');
			<?php
		}
		?>
		$("input[name=inv_type]").change(function(){
			if($(this).val() == 'B'){
				$("input.item-barcode").val('').prop("readonly",true);
				$(".barcode_code_div").hide();
				$(".stock-entry-fields input").prop("readonly",true).val('');
				$(".stock-entry-fields").hide();
				$("input[name=salePrice]").prop("readonly",true);
				$(".sale_price_div").hide();
			}else{
				$("input[name=salePrice]").prop("readonly",false);
				$("input.item-barcode").prop("readonly",false);
				$(".barcode_code_div").show();
				$(".stock-entry-fields input").prop("readonly",false);
				$(".stock-entry-fields").show();
				$(".sale_price_div").show();
				if($("input.item-barcode").val()==''){
					$("input.item-barcode").val($("input.item-barcode").attr('data-defbar'));
				}
			}
		});
		$.fn.multiplication = function($elm1,$elm2,$product){
			$($product).val($($elm1).val()*$($elm2).val());
		}
		$(".units").keyup(function(){
			$(this).multiplication(".units",".unit_price",".total_cost");
		});
		$(".unit_price").keyup(function(){
			$(this).multiplication(".units",".unit_price",".total_cost");
		});
		$(".itemCode").selectpicker();
		$(".itemCode").change(function(){
			var itemCode = $(this).find("option:selected").val();
			$.post('db/get-item-stock.php',{item_id:itemCode},function(data){
				data = $.parseJSON(data);
				$("input[name='units']").animate({'background-color':'rgba(0,255,0,0.5)'},200,function(){
					$("input[name='units']").animate({'background-color':'rgba(255,255,255,1.0)'},200).val(data['QTY'])
				});
				$("input[name=unit_price]").val(data['PRICE']);
				$("input[name=total_cost]").val(data['PRICE']*data['QTY']);
				if(data['QTY'] > 0){
					$("input:submit").prop('disabled',true);
				}else{
					$("input:submit").prop('disabled',false);
				}
			});
		});
	});
	$(window).load(function(){
		if($("input[name=inv_type]:checked").val() == 'B'){
			$(".stock-entry-fields input").prop("readonly",true).val('');
			$("input[name=salePrice]").prop("readonly",true);
		}else{
			$("input[name=salePrice]").prop("readonly",false);
			$(".stock-entry-fields input").prop("readonly",false);
			if($("input.item-barcode").val()==''){
				$("input.item-barcode").val($("input.item-barcode").attr('data-defbar'));
			}
		}
	});
	</script>

</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Items Management</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<a href="items-management.php?tab=list"><div class="tab">List</div></a>
						<div class="tabSelected">Details</div>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div id="bodyTab1">
						<form method="post" action="" class="the-form form-horizontal" enctype="multipart/form-data">
							<?php
								if(isset($_GET['added'])){
									successMessage('Record saved successfully.');
								}
								if(isset($_GET['updated'])){
									successMessage('Record updated successfully.');
								}
							?>
							<?php if(isset($item_id)&&$item_id>0){ ?>
							<a  class="btn btn-primary pull-left m-10" href="item-barcodes-print.php?id=<?php echo $item_id; ?>" target="_blank" ><i class="fa fa-barcode"></i></a>
							<?php } ?>
							<?php if(isset($item_id)&&$item_id>0){ ?>
							<a  class="btn btn-primary pull-left m-10" href="item-details.php?c&id=<?php echo $item_id; ?>" > <i class="fa fa-copy"></i> </a>
							<?php } ?>
							<div class="clear"></div>
							<hr />
							<div class="clear"></div>

							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
								<div class="popup_content_nostyle">
									<div class="form-group">
										<label class="control-label col-sm-3">Category :</label>
										<div class="col-sm-9">
											<select name="itemCategory" class="form-control catSelector" data-live-search="true" required="required">
												<option value=""></option>
												<?php
												if(mysql_num_rows($itemCategoryList)){
													while($row = mysql_fetch_array($itemCategoryList)){
														$selectedCategory = ( isset($itemDetails) && $row['ITEM_CATG_ID']==$itemDetails['ITEM_CATG_ID'])?"selected=\"selected\"":"";
														?>
														<option value="<?php echo $row['ITEM_CATG_ID']; ?>" <?php echo $selectedCategory ;?> >
															<?php echo $row['NAME']; ?>
														</option>
														<?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<!-- <div class="caption popuporemov" style="width: 100px;">
										<button type="button" onclick="addCategory();"  tabindex="-1" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
									</div> -->

									<div class="form-group">
										<label class="control-label col-sm-3"> Measure :</label>
										<div class="col-sm-9">
											<select type="text" name="measure" class="form-control measureSelector">
												<option value=""></option>
			<?php
												$measureList = $objMeasures->getList();
												if(mysql_num_rows($measureList)){
													while($row = mysql_fetch_array($measureList)){
														$selectedMeasure = ( isset($itemDetails) && $row['ID'] == $itemDetails['MEASURE'])?"selected=\"selected\"":"";
			?>
														<option value="<?php echo $row['ID']; ?>" <?php echo $selectedMeasure; ?> ><?php echo $row['NAME']; ?></option>
			<?php
													}
												}
			?>
											</select>
										</div>
									</div>
									<!-- <div class="caption popuporemov" style="width: 100px;">
										<button type="button" onclick="addMeasure();"  tabindex="-1" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
									</div> -->

									<?php if($sale_scanner=='Y'){ ?>
									<div class="form-group">
										<label class="control-label col-sm-3"> Barcode :</label>
										<div class="col-sm-9">
											<input type="text" name="barcode" value="<?php echo (isset($itemDetails)&&!isset($_GET['c']))?$itemDetails['ITEM_BARCODE']:$item_code; ?>" data-defbar="<?php echo $item_code; ?>" class="form-control item-barcode" />
										</div>
									</div>
									<?php } ?>

									<div class="form-group">
										<label class="control-label col-sm-3"> Item Title :</label>
										<div class="col-sm-9">
											<input type="text" name="itemTitle" required="required" value="<?php echo (isset($itemDetails))?$itemDetails['NAME']:""; ?>" class="form-control getTitle" />
										</div>
									</div>

									<?php if($urdu_account_names=='Y'){ ?>
									<div class="form-group">
										<label class="control-label col-sm-3"> Title in Urdu :</label>
										<div class="col-sm-9">
											<input type="text" name="urdu_name" dir="rtl" value="<?php echo (isset($itemDetails))?$itemDetails['URDU_NAME']:""; ?>" class="form-control" />
										</div>
									</div>
									<?php } ?>

					  				<div class="form-group">
										<label class="control-label col-sm-3"> Pack Types :</label>
										<div class="col-sm-9">
											<input id="cmn-toggle-1" value="Y" class="css-checkbox" name="is_carton" type="checkbox" <?php echo ($itemDetails['IS_CARTON'] == 'Y')?"checked":""; ?> />
											<label for="cmn-toggle-1" class="css-label" style="margin-top:5px;margin-right:-25px;">Cartons &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; </label>
											<input id="cmn-toggle-2" value="Y" class="css-checkbox" name="is_dozen" type="checkbox" <?php echo ($itemDetails['IS_DOZEN'] == 'Y')?"checked":""; ?> />
											<label for="cmn-toggle-2" class="css-label" style="margin-top:5px;margin-right:-25px;">Dozens &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;  </label>
										</div>
									</div>

									<?php if($use_cartons == 'Y'){ ?>
									<div class="form-group">
										<label class="control-label col-sm-3"> Qty Per Carton :</label>
										<div class="col-sm-9">
											<input type="text" name="qty_per_carton" value="<?php echo (isset($itemDetails)&&!isset($_GET['c']))?$itemDetails['QTY_PER_CARTON']:''; ?>" class="form-control" />
										</div>
									</div>
									<?php } ?>

									<?php if($distSalesAddon == 'Y'){ ?>
									<div class="form-group">
										<label class="control-label col-sm-3"> Rate Per Carton :</label>
										<div class="col-sm-9">
											<input type="text" name="rate_carton" value="<?php echo (isset($itemDetails)&&!isset($_GET['c']))?$itemDetails['RATE_CARTON']:''; ?>" class="form-control" />
										</div>
									</div>
									<?php } ?>

									<div class="form-group">
										<label class="control-label col-sm-3"> Rate Per Dozen :</label>
										<div class="col-sm-9">
											<input type="text" name="dozen_rate" value="<?php echo (isset($itemDetails)&&!isset($_GET['c']))?$itemDetails['DOZEN_RATE']:''; ?>" class="form-control" />
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-sm-3"> Unit Sale Price :</label>
										<div class="col-sm-9">
											<input type="text" name="salePrice" value="<?php echo (isset($itemDetails))?$itemDetails['SALE_PRICE']:""; ?>" class="form-control getTitle numFlotOnly" />
										</div>
									</div>

									<?php if($saleTaxModule=='Y'){ ?>
									<div class="form-group">
										<label class="control-label col-sm-3"> HS Code : </label>
										<div class="col-sm-9">
											<input type="text" name="hs_code"  value="<?php echo (isset($itemDetails))?$itemDetails['HS_CODE']:""; ?>" class="form-control getTitle" />
										</div>
									</div>
									<?php } ?>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Min Quantity : </label>
									<div class="col-sm-9">
										<input type="text" name="min_qty" value="<?php echo (isset($itemDetails))?$itemDetails['MIN_QTY']:""; ?>" class="form-control min_qty numOnly" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Item Discount : </label>
									<div class="col-sm-9">
										<input type="text" name="item_discount" value="<?php echo (isset($itemDetails))?$itemDetails['ITEM_DISCOUNT']:""; ?>" class="form-control pull-right" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Company Brand : </label>
									<div class="col-sm-9">
										<input type="text" name="company_name" value="<?php echo (isset($itemDetails))?$itemDetails['COMPANY']:""; ?>" class="form-control" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Comments : </label>
									<div class="col-sm-9">
										<input type="text" name="comment" value="<?php echo (isset($itemDetails))?$itemDetails['COMMENT']:""; ?>" class="form-control" />
									</div>
								</div>

							<?php if($use_image == 'Y'){ ?>
								<div class="form-group">
									<label class="control-label col-sm-3"> Image : </label>
									<div class="col-sm-9">
										<input type="file" class="btn btn-default" name="image" />
									</div>
								</div>
								<?php if($itemDetails!=NULL){ ?>
								<div class="form-group">
									<label class="control-label col-sm-3"></label>
									<div class="col-sm-3">
										<?php if(isset($itemDetails)&&$itemDetails['IMAGE']!=''&& is_file("uploads/products/".$itemDetails['IMAGE'])){ ?>
											<a href="uploads/products/<?php echo $itemDetails['IMAGE']; ?>" target="_blank" data-lightbox="roadtrip">
												<img src='uploads/products/<?php echo $itemDetails['IMAGE']; ?>' class='col-xs-8' />
											</a>
										<?php } ?>
									</div>
									<div class="col-sm-3">
										<?php if(is_file("uploads/products/".$itemDetails['IMAGE'])){ ?>
											<button type="button" class="btn  btn-danger delete_thumb"> <i class="fa fa-times"></i> Delete Thumb </button>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
								<?php } ?>
								<div class="form-group">
									<label class="control-label col-sm-3"> Scan Type : </label>
									<div class="col-sm-9">
										<?php if(!$beingUsed){ ?>
											<?php if($salePurchaseModule=='Y'||$salePanelModule=='Y'){ ?>
												<input type="radio" class="input_size pull-left css-checkbox" name="inv_type" id="check_inv_1" value="N" <?php echo $normal_selected; ?> />
												<label class="css-label-radio" for="check_inv_1">&nbsp;&nbsp; General &nbsp;&nbsp;&nbsp; </label>
											<?php } ?>
											<?php if($mobileAddon=='Y'){ ?>
												<input type="radio" class="input_size pull-left css-checkbox" name="inv_type" id="check_inv_2" value="B" <?php echo $barcode_selected; ?> />
												<label class="css-label-radio" for="check_inv_2">&nbsp;&nbsp;&nbsp; Mobile </label>
											<?php } ?>
											<?php }else{
												echo $inv_type_text;
													}
										?>
									</div>
								</div>
								<hr />
								<div class="form-group">
									<label class="control-label col-sm-3"> Available Stock : </label>
									<div class="col-sm-9">
										<input type="text" name="units" value="<?php echo (isset($itemDetails))?$itemDetails['STOCK_QTY']:""; ?>" class="form-control units numOnly" <?php echo ($beingUsed)?"readonly='readonly'":""; ?> />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Unit Cost : </label>
									<div class="col-sm-9">
										<input type="text" name="unit_price" value="<?php echo (isset($itemDetails))?$itemDetails['PURCHASE_PRICE']:""; ?>" class="form-control unit_price numFlotOnly" <?php echo ($beingUsed)?"readonly='readonly'":""; ?> />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"> Total Cost : </label>
									<div class="col-sm-9">
										<input type="text" name="total_cost" value="<?php echo (isset($itemDetails))?$itemDetails['TOTAL_COST']:""; ?>" class="form-control total_cost numFlotOnly" <?php echo ($beingUsed)?"readonly='readonly'":""; ?> />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-3"></label>
									<div class="col-sm-9">
										<?php
										if(isset($itemDetails)){
											if(in_array('items-managment-modify',$permissionz) || $admin){
											?>
											<input type="hidden" value="<?php echo ((isset($_GET['c'])))?0:$itemDetails['ID']; ?>" name="itemID" />
											<input type="submit" name="<?php echo (isset($_GET['c']))?"addItem":"updateItem"; ?>" value="<?php echo (isset($_GET['c']))?"Save":"Update"; ?>" class="btn btn-primary" />
											<?php
											}
											if($barcode_print_flag){
											?>
											<a class="btn btn-primary" href="item-barcodes-print.php?id=<?php echo $itemDetails['ID']; ?>" target="_blank" ><i class="fa fa-barcode"></i> Print Barcode </a>
											<?php
											}
											?>
										<?php
											}else{
										?>
												<input type="hidden" value="0" name="itemID" />
												<input type="submit" name="addItem" value="Save" class="btn btn-primary" />
												<input type="reset" name="submit" value="Reset" class="btn btn-primary" />
												<?php
											}
											?>
									</div>
								</div>
														</div>
													</form>
													<div class="clear"></div>
											</div> <!-- End #tab1 -->
										</div> <!-- End .content-box-content -->
									</div> <!-- End .content-box -->
								</div><!--body-wrapper-->
								<div id="fade"></div>
								<div id="xfade"></div>
							</body>
							</html>
							<?php include('conn.close.php'); ?>
