<?php
	ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/suppliers.php');
	include('common/classes/customers.php');
	include('common/classes/lot_details.php');
	include('common/classes/lot_costing.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/fabric_contract_details.php');
	include('common/classes/processing_contracts.php');
	include('common/classes/processing_contracts_details.php');
	include('common/classes/packing_contracts.php');
	include('common/classes/packing_contract_details.php');
	include('common/classes/embroidery_contracts.php');
	include('common/classes/embroidery_contract_details.php');
	include('common/classes/ledger_report.php');

	//Permission
  if(!in_array('fabric-contract',$permissionz) && $admin != true){
	  echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
	  echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
	  echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
	  echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
	  echo '</div>';
  	exit();
  }
  //Permission ---END--

	$tab 				 = "form";
	$id 				 = 0;
	$lot_details = NULL;

	if(isset($_GET['tab'])){
		$tab = $_GET['tab'];
	}

	$objAccount         			   	 = new UserAccounts();
	$objCustomers       			   	 = new Customers();
	$objSupplier        			   	 = new Suppliers();
	$objAccounts        			   	 = new UserAccounts();
	$objLotDetails						   	 = new LotDetails();
	$objLotCosting						   	 = new LotCosting();
	$objFabricContract  			   	 = new FabricContracts();
	$objFabricContractDetails    	 = new FabricContractDetails();
	$objProcessingContracts    		 = new ProcessingContracts();
	$objProcessingContractDetails  = new ProcessingContractDetails();
	$objProcessingContracts    		 = new ProcessingContracts();
	$objProcessingContractDetails  = new ProcessingContractDetails();
	$objPackingContracts     	  	 = new PackingContracts();
	$objPackingContractDetails  	 = new PackingContractDetails();
	$objEmbroideryContracts 			 = new EmbroideryContracts();
	$objEmbroideryContractDetails  = new EmbroideryContractDetails();
	$objLedgerReport   				 		 = new ledgerReport();

	if(isset($_GET['lot_no'])){
		$lot_no = (int)mysql_real_escape_string($_GET['lot_no']);
	}

	$objLedgerReport->po_number = $lot_no;
	$objLedgerReport->transactionType = "Dr";

	$total_cost 		 = $objLedgerReport->getVoucherDebitSumByPO();

	$fabric_lot_details  = $objFabricContractDetails->getListByLotNo($lot_no);
	$dyeing_lot_details  = $objProcessingContractDetails->getListByLotNo($lot_no);
	$packing_lot_details = $objPackingContractDetails->getListByLotNo($lot_no);
	$emb_lot_details 		 = $objEmbroideryContractDetails->getListByLotNo($lot_no);

	$grand_amount 									= 0;
	$fabric_detail  							  = array();
	$fabric_detail['THAAN'] 				= 0;
	$fabric_detail['QTY'] 				  = 0;
	$fabric_detail['UNIT_PRICE'] 	  = 0;
	$fabric_detail['TOTAL_AMOUNT']  = 0;
	$dyeing_detail  							  = array();
	$dyeing_detail['THAAN'] 				= 0;
	$dyeing_detail['QTY'] 				  = 0;
	$dyeing_detail['UNIT_PRICE'] 	  = 0;
	$dyeing_detail['TOTAL_AMOUNT']  = 0;
	$packing_detail 							  = array();
	$packing_detail['THAAN'] 				= 0;
	$packing_detail['QTY'] 				  = 0;
	$packing_detail['UNIT_PRICE']   = 0;
	$packing_detail['TOTAL_AMOUNT'] = 0;
	$emb_details 							  		= array();
	$emb_details['THAAN'] 					= 0;
	$emb_details['QTY'] 					  = 0;
	$emb_details['UNIT_PRICE']  	 	= 0;
	$emb_details['TOTAL_AMOUNT']		= 0;

	if(mysql_num_rows($fabric_lot_details)){
		$num_rows = 0;
		while($fabric_row = mysql_fetch_assoc($fabric_lot_details)){
			$fabric_detail['THAAN']   			+= $fabric_row['THAAN'];
			$fabric_detail['QTY']   				+= $fabric_row['QUANTITY'];
			$fabric_detail['UNIT_PRICE'] 		+= $fabric_row['UNIT_PRICE'];
			$fabric_detail['TOTAL_AMOUNT']  += $fabric_row['TOTAL_AMOUNT'];
			$num_rows++;
		}
		if($fabric_detail['QTY']>0){
			$fabric_detail['UNIT_PRICE'] =  round($fabric_detail['TOTAL_AMOUNT']/$fabric_detail['QTY'],2);
		}
	}

	if(mysql_num_rows($dyeing_lot_details)){
		$num_rows = 0;
		while($dyeing_row = mysql_fetch_assoc($dyeing_lot_details)){
			$dyeing_detail['THAAN'] 		+= $dyeing_row['THAAN'];
			$dyeing_detail['QTY'] 			+= $dyeing_row['QUANTITY'];
			$dyeing_detail['UNIT_PRICE'] 	+= $dyeing_row['UNIT_PRICE'];
			$dyeing_detail['TOTAL_AMOUNT']  += $dyeing_row['TOTAL_AMOUNT'];
			$num_rows++;
		}
		if($dyeing_detail['QTY']>0){
			$dyeing_detail['UNIT_PRICE'] =  round($dyeing_detail['TOTAL_AMOUNT']/$dyeing_detail['QTY'],2);
		}
	}

	if(mysql_num_rows($packing_lot_details)){
		$num_rows = 0;
		while($packing_row = mysql_fetch_assoc($packing_lot_details)){
			$packing_detail['THAAN'] 		+= $packing_row['THAAN'];
			$packing_detail['QTY'] 			+= $packing_row['QUANTITY'];
			$packing_detail['UNIT_PRICE']   += $packing_row['RATE'];
			$packing_detail['TOTAL_AMOUNT'] += $packing_row['AMOUNT'];
			$num_rows++;
		}
		if($packing_detail['QTY']>0){
			$packing_detail['UNIT_PRICE'] =  round($packing_detail['TOTAL_AMOUNT']/$packing_detail['QTY'],2);
		}
	}

	if(mysql_num_rows($emb_lot_details)){
		$num_rows = 0;
		while($emb_row = mysql_fetch_assoc($emb_lot_details)){
			$emb_details['THAAN'] 				+= $emb_row['THAAN'];
			$emb_details['QTY'] 					+= $emb_row['QUANTITY'];
			$emb_details['UNIT_PRICE']   	+= $emb_row['RATE'];
			$emb_details['TOTAL_AMOUNT'] 	+= $emb_row['TOTAL_AMOUNT'];
			$num_rows++;
		}
		if($emb_details['QTY']>0){
			$emb_details['UNIT_PRICE'] =  round($emb_details['TOTAL_AMOUNT']/$emb_details['QTY'],2);
		}
	}
	$stage_names = array();
	$stage_names['F'] = 'Fabric Contract';
	$stage_names['P'] = 'Processing Contract';
	$stage_names['E'] = 'Embroidery Contract';
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<!-- jQuery -->
	<style>
		td{
			padding: 5px !important ;
			font-size: 14px !important;
		}
		.content-box-header p{
			padding: 1px 1px !important;
			margin-bottom: 0px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){

		});
	</script>
</head>
<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div id="bodyWrapper">
		<div class = "content-box-top" style="overflow:visible;">
			<div class = "summery_body">
				<div class="content-box-header">
					<p>Lot Ledger Details</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="clear"></div>

				<?php include('lot.tabs.inc.php'); ?>
				<div class="clear"></div>

				<?php
					if($tab == 'form'){
				?>

				<div id = "bodyTab1">
					<div class="title">
						Total Cost : <?php echo number_format($total_cost,2); ?>
					</div>
					<hr />
					<form class="" action="" method="post">
						<div id="form">
							<div class="caption"></div>
							<div class="field"  style="width:650px;">
								<table cellspacing="0" style="width:650px;" class="costing" >
									<thead>
										<tr>
											<th width="25%" style="text-align:center">Process Stage</th>
											<th width="10%" style="text-align:center">Meters</th>
											<th width="10%" style="text-align:center">Cost Rate</th>
											<th width="10%" style="text-align:center">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr id="recordPanel">
											<td style="text-align:center">Grey Fabric</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_qty" value="<?php echo  $fabric_detail['QTY']; ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_cost" value="<?php echo ($fabric_detail['QTY']==0)?"":round($fabric_detail['TOTAL_AMOUNT']/$fabric_detail['QTY'],2); ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $fabric_detail['TOTAL_AMOUNT'] ?>" readonly />
											</td>
										</tr>
										<tr id="recordPanel">
											<td style="text-align:center">Dyed Fabric</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="bgrade_qty" value="<?php echo $dyeing_detail['QTY']; ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_cost" value="<?php echo ($dyeing_detail['QTY']==0)?"":round($dyeing_detail['TOTAL_AMOUNT']/$dyeing_detail['QTY'],2); ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $dyeing_detail['TOTAL_AMOUNT'] ?>" readonly />
											</td>
										</tr>
										<tr id="recordPanel">
											<td style="text-align:center">Packed Fabric</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="bgrade_qty" value="<?php echo $packing_detail['QTY']; ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_cost" value="<?php echo ($packing_detail['QTY']==0)?"":round($packing_detail['TOTAL_AMOUNT']/$packing_detail['QTY'],2); ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $packing_detail['TOTAL_AMOUNT'] ?>" readonly />
											</td>
										</tr>
										<tr id="recordPanel">
											<td style="text-align:center">Emb Fabric</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="bgrade_qty" value="<?php echo $emb_details['QTY']; ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_cost" value="<?php echo ($emb_details['QTY']==0)?"":round($emb_details['TOTAL_AMOUNT']/$emb_details['QTY'],2); ?>" />
											</td>
											<td style="text-align:center">
												<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $emb_details['TOTAL_AMOUNT'] ?>" readonly />
											</td>
										</tr>
									</tbody>
									<?php

									$grand_amount  += $fabric_detail['TOTAL_AMOUNT'];
									$grand_amount  += $dyeing_detail['TOTAL_AMOUNT'];
									$grand_amount  += $packing_detail['TOTAL_AMOUNT'];
									$grand_amount  += $emb_details['TOTAL_AMOUNT'];
									 ?>
									<tfoot>
										<tr>
											<th class="text-right"> <b>Total :</b> </th>
											<th class="text-center"><?php //echo $fabric_detail['QTY']+$dyeing_detail['QTY']+$packing_detail['QTY'] ?></th>
											<th class="text-center">- - -</th>
											<th class="text-center"><?php echo $grand_amount ?></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="clear"></div>

							<?php
								$total_thaan  = $fabric_detail['THAAN']+$dyeing_detail['THAAN']+$packing_detail['THAAN'];
								$total_meters = $fabric_detail['QTY']+$dyeing_detail['QTY']+$packing_detail['QTY'];
							?>


							<div class="caption"></div>
							<div class="field" style="width:650px;">
								<div class="pull-right">
									<div class="caption">Total Thaan : </div>
									<div class="field">
										<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $packing_detail['THAAN']; ?>" readonly />
									</div>
									<div class="clear"></div>

									<div class="caption">Total Meters : </div>
									<div class="field">
										<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $packing_detail['QTY']; ?>" readonly />
									</div>
									<div class="clear"></div>

									<div class="caption">Rate / Meters : </div>
									<div class="field">
										<?php
											if($packing_detail['QTY']==0){
												$rate_meters        = '- - -';
											}else{
												$rate_meters        = number_format(($grand_amount-$emb_details['TOTAL_AMOUNT'])/$packing_detail['QTY'],2);
											}
										?>
										<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $rate_meters ?>" readonly />
									</div>
									<div class="clear"></div>

									<div class="caption">Total Meters Cost : </div>
									<div class="field">
										<?php
											if($packing_detail['QTY']==0){
												$total_meter_cost        = '- - -';
											}else{
												$total_meter_cost        = number_format(($grand_amount-$emb_details['TOTAL_AMOUNT']),2);
											}
										?>
										<input class="form-control text-center" type="text" name="fresh_total" value="<?php echo $total_meter_cost ?>" readonly />
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</form>
				</div> <!--bodyTab1-->
<?php
				}
?>
					<div class="clear mt-20"></div>
				</div>     <!-- End summer -->
			</div>   <!-- End .content-box-top -->
		</div>
		<div id="xfade"></div>
		<div id="fade"></div>
	</body>
	</html>
	<?php ob_end_flush(); ?>
	<?php include("conn.close.php"); ?>
