<?php 
	include("common/connection.php");
	include('common/config.php');
	include('common/classes/accounts.php');

	$objUsers    = new UserAccounts();
	$objAccounts = new ChartOfAccounts();
    $objConfigs  = new Configs();

	$user_id     = $_SESSION['classuseid'];
	$userDetails = $objUsers->getDetails($user_id);
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>User Management</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<style>
			 table th, table td{
			 	font-size: 14px;
			 	text-align:center;
			 }
			 input.error{
			 	border-color: red !important;
			 }
			 input[readonly]{
			 	background-color: #FFF !important;
			 }
		</style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript">
			$(function(){
				$("input").attr("readonly",true);
			});
		</script>
   </head>

   	<body>
   		<div id = "body-wrapper">
      		<div id="sidebar">
				<?php include("common/left_menu.php") ?>
            </div> <!-- End #sidebar -->
      		<div id = "bodyWrapper">
        		<div class = "content-box-top">
            		<div class = "summery_body">
               			<div class = "content-box-header">
                  			<p> <i class="fa fa-user"></i> My Profile</p>
                  			<div class="clear"></div>
               			</div><!-- End .content-box-header -->

                        <div id = "bodyTab1" style="display:block">
                            <div id = "form">
                                <div class = "caption">User Name :</div>
                                <div class = "field">
                                    <input type="text" name="userName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['USER_NAME']:""; ?>" />
                                </div>
                                <div class="clear"></div>
								<hr />

								<div class = "caption">First Name :</div>
                                <div class = "field"> 
                                    <input type="text" name="firstName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['FIRST_NAME']:""; ?>" />
                                </div>
                                <div class="clear"></div>
                                
                                <div class = "caption">Last Name :</div>
                                <div class = "field">
                                    <input type="text" name="lastName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['LAST_NAME']:""; ?>" />
                                </div>
                                <div class="clear"></div>


                                <div class = "caption">Designation :</div>
                                <div class = "field">
                                    <input type="text" name="designation" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['DESIGNATION']:""; ?>" />
                                </div>
                                <div class="clear"></div>
                                
                                <div class = "caption">Contact # :</div>
                                <div class = "field">
                                    <input type="text" name="contact" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['CONTACT_NO']:""; ?>" />
                                </div>
                                <div class="clear"></div>
                                
                                <div class = "caption">Address :</div>
                                <div class = "field" style="width:250px;;">
                                	<textarea name="address" style="height: 100px;" class="form-control"><?php echo (isset($userDetails))?$userDetails['ADDRESS']:""; ?></textarea>
                                </div>
                                <div class="clear"></div>

<?php if(isset($userDetails)&&$userDetails['ID'] > 1){  ?> 
                                <div class = "caption">Use Cash In Hand</div>
                                <div class = "field">
<?php 
		if(isset($userDetails)){
			if($userDetails['CASH_IN_HAND'] == 'Y'){
				$y_checked = 'checked="checked"';
				$n_checked = '';
			}elseif($userDetails['CASH_IN_HAND'] == 'N'){
				$n_checked = 'checked="checked"';
				$y_checked = '';
			}
		}else{
			$y_checked = 'checked="checked"';
			$n_checked = '';
		}
?>
                                	<input type="radio" name="cash_status" value="Y" <?php echo $y_checked; ?> > Yes 
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                	<input type="radio" name="cash_status" value="N" <?php echo $n_checked; ?> > No 
                                </div>
                                <div class="clear"></div>
<?php } ?>
                            </div><!--form-->
                            <div style = "clear:both;bodyWrapper"></div>
                        </div><!--bodyTab1-->
                        
                        <div id="bodyTab2" style="display:none">
                            <table class="table" style="width: 100%;float:left;">
                                <thead>
                                    <tr>
                                    	<th width="5%">Sr.</th>
                                        <th width="35%">Full Name</th>
                                        <th width="35%">User Name</th>
                                        <th width="10%">Cash In Hand</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <div style = "clear:both;"></div>
                        </div><!--bodyTab2-->
            		</div><!--summery_body-->
	         	</div><!--End.content-box-top-->
      		</div><!--bodyWrapper-->
      	</div>  <!--body-wrapper-->
      	<div id="fade"></div>
      	<div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
<?php
	if(isset($message)){
?>
		displayMessage('<?php echo $message; ?>');
<?php
	}
?>
    });
	var centerThisDiv = function(elementSelector){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		var elemHeight = $(elementSelector).height()/2;
		var elemWidth = $(elementSelector).width()/2;
		var posTop = win_hi-elemHeight;
		var posLeft = win_width-elemWidth;
		$(elementSelector).css({
			'position': 'fixed',
			'top': posTop,
			'left': posLeft,
			'margin': '0px'
		});
	}
	var displayMessage= function(message){
		$(".myAlert-bottom span").text(message);
		$(".myAlert-bottom").show();
		setTimeout(function () {
			$(".myAlert-bottom").hide();
		}, 2000);
	}
	
</script>