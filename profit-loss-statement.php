<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/accounts.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/profit-loss.php';
	include 'common/classes/ledger_report.php';

	//Permission
	if(!in_array('profitAndLoss',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts        = new ChartOfAccounts();
	$objJournalVoucher  = new JournalVoucher();
	$objProfitAndLoss   = new ProfitLossAccount();
	$objLedgerReport    = new ledgerReport();
	$objConfigs		    	= new Configs();

	$perdiod_set   = true;
	$pnl_date      = $objConfigs->get_config('PNL_DATE');
	$period_length = $objConfigs->get_config('PNL_PERIOD');

	if((int)$period_length==0){
		$perdiod_set = false;
	}

	if($pnl_date != ''){
		$pnl_date = date('Y-m-d',strtotime($pnl_date));
	}else{
		$perdiod_set = false;
	}

	if(strtotime($pnl_date) > strtotime(date('Y-m-d'))){
		$perdiod_set = false;
	}

	if(isset($_POST['delete_pnl'])){
		$voucher_id = (int)mysql_real_escape_string($_POST['delete_pnl']);

		echo $prev_pnl_date = date('Y-m-d',strtotime($pnl_date));
		echo $prev_pnl_date = date('Y-m-d',strtotime($prev_pnl_date." - $period_length months "));

		$objConfigs->set_config($prev_pnl_date, 'PNL_DATE');
		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		exit();
	}

	if(isset($_GET['history_list'])){
		$voucher_list = $objJournalVoucher->getVoucherListByTypeLimit('PL',1);
		if(mysql_num_rows($voucher_list)){
			while($row = mysql_fetch_assoc($voucher_list)){
				?><tr>
					<td class="text-center"><?php echo $row['VOUCHER_NO']; ?></td>
					<td class="text-center"><?php echo date('d-m-Y',strtotime($row['VOUCHER_DATE'])); ?></td>
					<td class="text-center">
						<a href="voucher-print.php?id=<?php echo $row['VOUCHER_ID']; ?>" class="btn btn-primary btn-sm btn-sm" target="_blank"> <i class="fa fa-print"></i> View </a>
						<?php if($admin){ ?>
							<a do="<?php echo $row['VOUCHER_ID']; ?>" class="btn btn-danger btn-sm" onclick="delete_row(this);"> <i class="fa fa-times"></i> Delete </a>
							<?php } ?>
						</td>
					</tr><?php
				}
			}else{
				?>
				<tr>
					<td colspan="10">0 Records Found.</td>
				</tr>
				<?php
			}
			exit();
		}

		$currentDate  = date('d-m-Y',strtotime($pnl_date." + 1 day"));
		$monthEndDate = date('d-m-Y',strtotime($currentDate." + ".($period_length)." months "));
		$monthEndDate = date('d-m-Y',strtotime($monthEndDate." -1 day "));

		if(strtotime(date('Y-m-d')) > strtotime($monthEndDate)){
			$perdiod_set = true;
		}else{
			$perdiod_set = false;
		}
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>SIT Solutions</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
		<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<link href="resource/css/bootstrap.min.css" rel="stylesheet">
		<link href="resource/css/bootstrap-select.css" rel="stylesheet">
		<style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
		.caption{
			width: 200px !important;
		}
		#history-pnl{
			min-height: 100px;
		}
		</style>
		<!-- jQuery -->
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script src="resource/scripts/bootstrap.min.js"></script>
		<script src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
		<script type = "text/javascript" src = "resource/scripts/configuration.js"></script>
		<script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("a[href='#history-pnl']").on("click",function(){
				$("#history-pnl").addClass("loadingContent");
				$("#history-pnl table").hide();
				$.get('profit-loss-statement.php',{history_list:true},function(html){
					$("#history-pnl").removeClass("loadingContent");
					$("#history-pnl table").fadeIn();
					$("#history-pnl table tbody").html(html);
				});
			});
			$(".printThis").click(function(){
				$(".printTable").printThis({
					debug: false,
					importCSS: false,
					printContainer: true,
					loadCSS: 'resource/css/reports.css',
					pageTitle: "Sitsbook",
					removeInline: false,
					printDelay: 500,
					header: null
				});
			});
		});
		var delete_row = function(elm){
			var id = parseInt($(elm).attr('do'))||0;
			$(elm).prop("disabled",true);
			if(id>0){
				$.post('profit-loss-statement.php',{delete_pnl:id},function(data){
					//window.location.reload();
					$("a[href='#history-pnl']").trigger('click');
				});
			}
		}
		</script>
	</head>

	<body>
		<div id = "body-wrapper">
			<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

			<div id = "bodyWrapper">
				<div class = "content-box-top">
					<div class="summery_body">
						<div class = "content-box-header">
							<p>Profit &amp; Loss Statement</p>
							<div class="clear"></div>
						</div><!-- End .content-box-header -->

						<div class="col-xs-12 mt-25">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#history-pnl">History</a></li>
								<li class="active"><a data-toggle="tab" href="#present-pnl">Present</a></li>
							</ul>
							<div class="tab-content">
								<div id="history-pnl" class="tab-pane fade">
									<table class="table" style="margin: 10px auto !important;float:none !important;">
										<thead>
											<tr>
												<th class="col-xs-1 text-center">JV#</th>
												<th class="col-xs-2 text-center">Voucher Date</th>
												<th class="col-xs-1 text-center">Action</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
								<div id="present-pnl" class="tab-pane fade in active">
									<div id="form" style="margin: 0 auto; width: 900px;">
										<form method="post" action="">
											<div class="caption" style="width:150px">Start Date:</div>
											<div class="field" style="width:150px;padding-top:3px;">
												<label for=""><?php echo $currentDate; ?></label>
											</div>
											<div class="caption" style="margin-left:120px">End Date:</div>
											<div class="field" style="width:150px;padding-top: 3px;">
												<label for=""><?php echo $monthEndDate; ?></label>
											</div>
											<div class="clear"></div>

											<div class="caption"></div>
											<div class="field" style="text-align:left">
												<input type="submit" value="Generate" name="report" class="btn btn-default"/>
											</div>
											<div class="clear"></div>
										</form>
									</div><!--form-->
									<div style="clear:both;height:20px;"></div>

									<?php
									if(isset($_POST['report'])){
										?>
										<div class="content-box-content">
											<div id="form">
												<span style="float:left;">
													<?php if($perdiod_set){ ?>
														<button class="button postButton" style="margin-right: 20px;" onclick="saveProfitAndLoss();">Post</button>
														<?php }else{ ?>
															<button class="button pull-left" style="margin-left: 20px;" disable="disabled">Period Not Valid</button>
															<?php } ?>
														</span>
														<span style="float:right;">
															<button class="button printThis">Print</button>
														</span>
														<div id="bodyTab" class='printTable' style="width: 800px;margin: 0 auto;">
															<div style="text-align:left;margin-bottom:20px;">
																<p class="report_heading">
																	<span style="font-size:26px !important;"><?php echo $companyTitle; ?></span><br />
																	<span class="text-word-space">Profit &amp; Loss Statement</span>
																	<br />
																	<span style="font-size:14px !important;">for the period ended <?php echo date('M-d-Y',strtotime($monthEndDate)); ?></span>
																</p>
															</div>
															<div style="clear:both; height:1;"></div>
															<div id="summary_form">
																<div style="float:right;margin-right:60px;margin-top:10px;font-weight:600">Amount</div>
																<div style="float:right;margin-right:60px;margin-top:10px;font-weight:600">Amount</div>
																<div class="clear"></div>
																<?php
																//Exclude 0402010003 == Qamitty Account
																$revenuesArrayL2 = array();
																$revenuesArrayL3 = array();
																$reve = $objAccounts->getLevelTwoListByGeneral('02');
																if(mysql_num_rows($reve)){
																	while($revenuesRow = mysql_fetch_assoc($reve)){
																		$revenuesArrayL2[] = $revenuesRow['ACC_CODE'];
																	}
																}

																$expenseArrayL2 = array();
																$expenseArrayL3 = array();
																$reve = $objAccounts->getLevelTwoListByGeneral('03');
																if(mysql_num_rows($reve)){
																	while($revenuesRow = mysql_fetch_assoc($reve)){
																		$expenseArrayL2[] = $revenuesRow['ACC_CODE'];
																	}
																}

																$array_exp_first_four = array();
																$array_exp_first_four[] = '0302';
																$array_exp_first_four[] = '0303';
																$array_exp_first_four[] = '0301';
																$array_exp_first_four[] = '0304';

																foreach ($expenseArrayL2 as $key => $value) {
																	if(in_array($value, $array_exp_first_four)){
																		unset($expenseArrayL2[$key]);
																	}
																}

																$expenseArrayL2 = array_merge($array_exp_first_four,$expenseArrayL2);
																?>
																<div class="main_title_1">Revenues</div>
																<div id="form" style="margin-left:20px;border: 1px solid #CCC;padding: 10px;">
																	<?php
																	$mq_four_amount = 0;
																	$grossRevenue 	= 0;
																	foreach($revenuesArrayL2 as $key=>$revCodeL2){
																		$reve = $objAccounts->getLevelThreeListByMain($revCodeL2);
																		/*
																		?>
																		<div class="caption_P" style="float:left;font-size: 16px !important;font-weight:bold;padding-left:0px;"><?php echo $objAccounts->getAccountTitleByCode($revCodeL2)." - ".$revCodeL2; ?></div>
																		<div class="field_column"></div>
																		<div class="clear"></div>
																		<?php
																		*/
																		if(mysql_num_rows($reve)){
																			while($revenuesRow = mysql_fetch_assoc($reve)){
																				$revCode = $revenuesRow['ACC_CODE'];
																				$level_this_pnl = 0;
																				$accCodeList = $objAccounts->getAccountByCatAccCode($revCode);
																				if(mysql_num_rows($accCodeList)){
																					while($revRow = mysql_fetch_assoc($accCodeList)){
																						$profitOrLoss = $objJournalVoucher->getProfitnLoss($revRow['ACC_CODE'],$currentDate,$monthEndDate);

																						if($revRow['ACC_CODE'] == '0201030004'){
																							$mq_four_amount = $profitOrLoss;
																						}
																						if($revRow['ACC_CODE'] == '0401010001' || $revRow['ACC_CODE'] == '0402010003' || $revRow['ACC_CODE'] == '0402010001'){
																							continue;
																						}
																						if($profitOrLoss == 0 || $profitOrLoss == '' ){
																							continue;
																						}
																						$grossRevenue += $profitOrLoss;
																						$level_this_pnl    += $profitOrLoss;
																					}
																					if($level_this_pnl != 0){
																						?>
																						<div class="caption_P" style="float:left;font-size: 14px !important;padding-left:20px;"><?php echo $objAccounts->getAccountTitleByCode($revCode); ?></div>
																						<div class="field_column" style="font-size: 14px !important;"><?php echo ($level_this_pnl < 0)?"(":""; ?><?php echo number_format(str_ireplace('-', '', $level_this_pnl),2); ?><?php echo ($level_this_pnl < 0)?")":""; ?></div>
																						<div class="clear"></div>
																						<?php
																					}
																				}
																			}
																		}
																	}//revenues END
																	?>
																	<div class="caption" style="line-height:0px;font-size:16px !important;font-weight:bold; margin-left:0px;">Revenue Total</div>
																	<div class="field_column" style="margin-right:20px !important;font-size:16px !important;font-weight:bold;"><?php echo number_format($grossRevenue,2); ?></div>
																	<div class="clear"></div>
																</div><!--form of rev-->
																<div class="clear"></div>

																<div class="main_title_1">Expenses</div>
																<div id="form" style="margin-left:20px;border: 1px solid #CCC;padding: 10px;">
																	<?php
																	$totalExpense = 0;
																	foreach($expenseArrayL2 as $key=>$revCodeL2){
																		$reve = $objAccounts->getLevelThreeListByMain($revCodeL2);
																		/*
																		?>
																		<div class="caption_P" style="float:left;font-size: 16px !important;font-weight:bold; padding-left:0px;"><?php echo $objAccounts->getAccountTitleByCode($revCodeL2); ?></div>
																		<div class="field_column"></div>
																		<div class="clear"></div>
																		<?php
																		*/
																		if(mysql_num_rows($reve)){
																			while($revenuesRow = mysql_fetch_assoc($reve)){
																				$revCode = $revenuesRow['ACC_CODE'];
																				$level_this_pnl = 0;
																				$accCodeList = $objAccounts->getAccountByCatAccCode($revCode);
																				if(mysql_num_rows($accCodeList)){
																					while($revRow = mysql_fetch_assoc($accCodeList)){
																						$profitOrLoss = $objJournalVoucher->getProfitnLoss($revRow['ACC_CODE'],$currentDate,$monthEndDate);
																						if($profitOrLoss == 0){
																							continue;
																						}
																						$totalExpense      += $profitOrLoss;
																						$level_this_pnl    += $profitOrLoss;
																					}
																					if($level_this_pnl != 0){
																						?>
																						<div class="caption_P" style="float:left;font-size: 14px !important;padding-left:20px;"><?php echo $objAccounts->getAccountTitleByCode($revCode); ?></div>
																						<div class="field_column" style="font-size: 14px !important;"><?php echo ($level_this_pnl < 0)?"(":""; ?><?php echo number_format(str_ireplace('-', '', $level_this_pnl),2); ?><?php echo ($level_this_pnl < 0)?")":""; ?></div>
																						<div class="clear"></div>
																						<?php
																					}
																				}
																			}
																		}
																	}
																	?>
																	<div class="caption" style="line-height:0px;font-size:16px !important;font-weight:bold; margin-left:0px;">Expense Total</div>
																	<div class="field_column" style="margin-right:20px !important;font-size:16px !important;font-weight:bold;"><?php echo number_format($totalExpense,2); ?></div>
																	<div class="clear"></div>

																</div><!--form of exp-->
																<div class="clear"></div>

																<?php
																if($grossRevenue >= $totalExpense){
																	$netType = "Income";
																	$netTotal = $grossRevenue - $totalExpense;
																	$calculateAvg = true;
																}else{
																	$calculateAvg = false;
																	$netType = "Loss";
																	$netTotal = $totalExpense - $grossRevenue;
																}
																?>
																<input type="hidden" value="<?php echo $monthEndDate; ?>" class="entryDate" />
																<input type="hidden" class="pOrLAmount" value="<?php echo $grossRevenue-$totalExpense; ?>" />

																<div class="main_title_1" style="width:300px; float:left;font-size:18px !important;padding:10px;border: none;">Net <?php echo $netType; ?> Rs. </div>
																<div class="field_column" style="float:right;margin-right:50px !important;margin-top:5px;font-size:18px !important;font-weight:bold; border-bottom:double"><?php echo number_format($netTotal,2); ?></div>
																<div class="clear"></div>

															</div>
															<div style="clear:both; height:20px"></div>
														</div><!--bodyTab2-->
													</div><!--#form-->
												</div><!--summery_body-->
											</div><!-- End .content-box -->
											<?php
										}
										?>
									</div>
								</div>
							</div> <!-- End .content-box-content -->
						</div>
					</div><!--bodyWrapper-->
				</div><!--body-wrapper-->
			</body>
			</html>
			<?php include('conn.close.php'); ?>
			<script type="text/javascript">
			$(document).ready(function(){
				var pnl = parseFloat($(".pOrLAmount").val())||0;
				if(pnl == 0){
					$(".postButton").hide();
				}
			});
			$(window).load(function(){
				$.fn.stDigits = function(){
					return this.each(function(){
						$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
					})
				};
				$("div.field_column").stDigits();
				$(".datepicker").first().setFocusTo(".datepicker:last");
				$(".datepicker").last().setFocusTo("input[type='submit']");
			});

			var saveProfitAndLoss = function(){
				var entryDate = $(".entryDate").val();
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do You Really Want To Post?</p><a class='dodelete button'>Yes</a><a class='nodelete button'>No</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
					$(".postButton").hide();
					$.post('db/profit_and_loss_entry.php',{entryDate:entryDate},function(data){
						data = $.parseJSON(data);
						if(data['OK'] == 'Y'){
							$(".postButton").hide();
						}
						//displayMessage(data['MSG']);
						window.location.href = 'profit-loss-statement.php';
					});
				});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut(function(){
						$(this).remove();
					});
				});
			}
			</script>
