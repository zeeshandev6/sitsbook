<?php
    $out_buffer = ob_start();
    include ('common/connection.php');
    include ('common/config.php');
    include('common/classes/emb_stitch_account.php');

    $objEmbStitchAccount  = new EmbStitchAccount();

    $id              = 0;
    $stitch_account_details = NULL;

    if(isset($_POST['save'])){
      $id                                                  = (int)$_POST['id'];
      $objEmbStitchAccount->name                           = mysql_real_escape_string(ucfirst($_POST['title']));
      if($id == 0){
        $id = $objEmbStitchAccount->save();
      }else{
        $objEmbStitchAccount->update($id);
      }
      exit(header('location: emb-stitch-account-details.php?id='.$id));
      exit();
    }

    //get one record by id
    if(isset($_GET['id'])){
        $id = mysql_real_escape_string($_GET['id']);
        $stitch_account_details = $objEmbStitchAccount->getDetails($id);
    }
?>
    <!DOCTYPE html 
        >

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css"                         />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css"                         />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"                       />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css"                          />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"                          />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css"                       />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"                  />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"                 />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"              />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p ><B>Product Details</B></p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="emb-stitch-account-management.php"><div class="tab">List</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>

                <?php
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type updated.
                        </div>
                    <?php
                    }elseif($_GET['action']=='added'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type added.
                        </div>
                    <?php
                    }
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" name="cust">
                        <input type="hidden" name="measure_type" value="<?php if(isset($_GET['id'])){ echo 'update';} else { echo 'add'; } ?>" />
                        <input type="hidden" name="id" value="<?php if(isset($_GET['id'])){ echo $id;} ?>" />
                        <div id="form">
                            <div class="caption"> Title :</div>
                            <div class="field">
                                <input class="form-control" name="title" value="<?php echo $stitch_account_details['NAME']; ?>" required="required"/>
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input  type="submit"  name="save"         value="<?php echo ($id>0)?"Update":"Save"; ?>" class="button" />
                            </div>
                            <div class="clear"></div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>
    </div><!--bodyWrapper-->
    </body>
    </html>
<?php include('conn.close.php'); ?>
