<?php
	ob_start();
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/item_category.php');
	include('common/classes/items.php');
	include('common/classes/customers.php');
	include('common/classes/suppliers.php');
	include('common/classes/brokers.php');
	include('common/classes/branches.php');
	include('common/classes/branch_stock.php');
	include('common/classes/lot_details.php');
	include('common/classes/j-voucher.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/fabric_contract_details.php');
	include('common/classes/processing_contracts.php');
	include('common/classes/processing_contracts_details.php');

	//Permission
  if(!in_array('processing-contract',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objChartOfAccounts 			 		 = new ChartOfAccounts();
	$objItemCategory        	 		 = new ItemCategory();
	$objItems               	 		 = new Items();
	$objCustomer            	 		 = new Customers();
	$objSupplier        			 		 = new Suppliers();
	$objBrokers    		  			 		 = new Brokers();
	$objUserAccount         	 		 = new UserAccounts();
	$objBranches  	   				 		 = new Branches();
	$objBranchStock    				 		 = new BranchStock();
	$objLotDetails     	 			 		 = new LotDetails();
	$objJournalVoucher 				 		 = new JournalVoucher();
	$objFabricContracts 					 = new FabricContracts();
	$objFabricContractDetails 		 = new FabricContractDetails();
	$objProcessingContracts    		 = new ProcessingContracts();
	$objProcessingContractDetails  = new ProcessingContractDetails();

	$supplier_list 			= $objSupplier->getList();
	$broker_list 				= $objBrokers->getList();

	$fabric_id 				  = 0;
	$fabric_inward_row  = NULL;

	//save and update
	if(isset($_POST['lot_no'])){
		$fabric_id     								  						= (int)(isset($_POST['fabric_id'])?$_POST['fabric_id']:0);

		$objProcessingContracts->user_id    				= (int)$_SESSION['classuseid'];
		$objProcessingContracts->lot_no      				= mysql_real_escape_string($_POST['lot_no']);
		$objProcessingContracts->supplier_id      	= mysql_real_escape_string($_POST['supplier_id']);
		$objProcessingContracts->broker_id      		= (isset($_POST['broker_id']))?mysql_real_escape_string($_POST['broker_id']):0;
		$objProcessingContracts->dyeing_unit_lot_no = mysql_real_escape_string($_POST['dyeing_unit_lot_no']);
		$objProcessingContracts->contract_date      = date('Y-m-d',strtotime($_POST['contract_date']));

		$objProcessingContracts->total_meter  			= mysql_real_escape_string($_POST['total_meter']);
		$objProcessingContracts->difference_percent = mysql_real_escape_string($_POST['difference_percent']);

		if($fabric_id > 0){
			$update = $objProcessingContracts->update($fabric_id);
			$action = "U";
		}else{
			$fabric_id = $objProcessingContracts->save();
			$action = "S";
		}

		echo mysql_error();
		if($fabric_id){
			$arrPro_table = json_decode($_POST['details'], true);
			$rows_deleted = (isset($_POST['deleted_rows']))?$_POST['deleted_rows']:array();

			if(is_array($rows_deleted)){
				foreach($rows_deleted as $key => $id){
					$deleted = $objProcessingContractDetails->delete($id);
				}
			}
			$row_id = 0;
			$fabric_amount = 0;
			$objProcessingContractDetails->fabric_id = $fabric_id;
			foreach($arrPro_table as $row_key => $row){
				$row_id                                  		= (int)$row['row_id'];
				$objProcessingContractDetails->fabric_type 	= mysql_real_escape_string($row['fabric_type']);
				$objProcessingContractDetails->thaan    		= mysql_real_escape_string($row['thaan']);
				$objProcessingContractDetails->quantity   	= mysql_real_escape_string($row['quantity']);
				$objProcessingContractDetails->unit_price   = mysql_real_escape_string($row['unit_price']);
				$objProcessingContractDetails->total_amount = mysql_real_escape_string($row['amount']);
				$objProcessingContractDetails->color   			= mysql_real_escape_string($row['color']);

				if($row_id>0){
					$record_state = $objProcessingContractDetails->update($row_id);
				}else{
					$record_state = $objProcessingContractDetails->save();
				}

				if($record_state){
					$fabric_amount += $objProcessingContractDetails->total_amount;
				}

			}
		}

		if($fabric_id>0){
			$voucher_id 											 = $objProcessingContracts->getVoucherId($fabric_id);

			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($objProcessingContracts->contract_date));
			$objJournalVoucher->voucherType    = 'FV';
			$objJournalVoucher->reference      = 'Dyeing Contract';
			$objJournalVoucher->po_number      = $objProcessingContracts->lot_no;
			$objJournalVoucher->reference_date = date('Y-m-d',strtotime($objProcessingContracts->contract_date));

			if($voucher_id == 0){
				$voucher_id = $objJournalVoucher->saveVoucher();
				$objProcessingContracts->insertVoucherId($fabric_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id > 0){
				$objJournalVoucher->voucherId       = $voucher_id;

				$objJournalVoucher->accountCode     = "0101060003";
				$objJournalVoucher->accountTitle    = mysql_real_escape_string($objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode));
				$objJournalVoucher->narration       = "Fabric Dyeing against lot # ".$objProcessingContracts->lot_no;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount          = $fabric_amount;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $objProcessingContracts->supplier_id;
				$objJournalVoucher->accountTitle    = mysql_real_escape_string($objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode));
				$objJournalVoucher->narration       = "Fabric Dyeing against lot # ".$objProcessingContracts->lot_no;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount          = $fabric_amount;

				$objJournalVoucher->saveVoucherDetail();
			}
		}

		if($fabric_id>0){
			if($action == 'S'){
				exit(header('location: processing-contract-detail.php?id='.$fabric_id.'&action=added'));
			}elseif($action == 'U'){
				exit(header('location: processing-contract-detail.php?id='.$fabric_id.'&action=updated'));
			}
		}
		exit();
	}

	if(isset($_GET['lot_no'])){
		$lot_no = (int)mysql_real_escape_string($_GET['lot_no']);
	}

	if(isset($_GET['id'])){
		$fabric_id = (int)(mysql_real_escape_string($_GET['id']));
		$fabric_inward_row = $objProcessingContracts->getDetail($fabric_id);
		$lot_no 					 = $fabric_inward_row['LOT_NO'];
		$fabric_contract_list= $objProcessingContractDetails->getListById($fabric_id);
	}

	$fabric_array = array();
	$fabric_array['F'] = 'Fabric';
	$fabric_array['B'] = 'B.Grade';
	$fabric_array['C'] = 'C.P';

	$total_meters = $objFabricContractDetails->getTotalMeters($lot_no);

?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style media="screen">
		table#pro_table td,table#pro_table th{
			padding: 5px !important;
			font-size: 14px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/processing.contract.config.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select").selectpicker();
			$(".quickSubmit input[type=text]").addClass("text-center");
			$("table").change(function(){
				$("#view_button").parent().addClass("text-center");
			});
			$(".reload_item").click(function(){
				$.get('processing-contract-detail.php',{},function(data){
					$("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
					$("select.itemSelector1").html($(data).find("select.itemSelector1").html()).selectpicker('refresh');
				});
			});
		});
	</script>
</head>
<body>
	<div id="body-wrapper">
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
			<div class="content-box-top">
				<div class="content-box-header">
					<p>Processing Contract</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
							<a href="lot-details.php?tab=search"><div class="tab">Search</div></a>
							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="clear"></div>

				<?php include('lot.tabs.inc.php'); ?>
				<div class="clear"></div>

				<div class = "content-box-header" style="margin-top: 0px !important;">
					<span id="tabPanel" class="pull-left" style="margin-left: 70px;">
						<div class="tabPanel">
							<a href="processing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="tab">List</div></a>
							<a href="processing-contract-detail.php?lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Form</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('processing-contract-list.php'); ?>&rf=<?php echo base64_encode('processing-contract-detail.php'); ?>&io_status=i&typee=pc&lot_no=<?php echo $lot_no; ?>"><div class="tab">Inward</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('processing-contract-list.php'); ?>&rf=<?php echo base64_encode('processing-contract-detail.php'); ?>&io_status=o&typee=pc&lot_no=<?php echo $lot_no; ?>"><div class="tab">Outward</div></a>
							<a href="lot-processing-report.php?rl=<?php echo base64_encode('processing-contract-list.php'); ?>&rf=<?php echo base64_encode('processing-contract-detail.php'); ?>&io_status=o&typee=pc&lot_no=<?php echo $lot_no; ?>"><div class="tab">Report</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!-- End .content-box-header -->

				<div class="content-box-content">
					<div id="bodyTab1">
						<div id="form">
							<form method="post" action="" name="add-history">
								<div class="caption">Lot No :</div>
								<div class="form-group field" style="width:350px;margin-bottom:0px;">
									<input type="text" class="form-control" name="lot_no" id="pon" value="<?php echo ($fabric_inward_row['LOT_NO']=='')?$lot_no:$fabric_inward_row['LOT_NO']; ?>" style="width: 350px;" required readonly />
									<input type="hidden" name="details" value="" />
									<input type="hidden" name="fabric_id" value="<?php echo $fabric_id ?>" />
								</div>
								<div class="caption">Contract Date :</div>
								<div class="field" style="width:150px">
									<input type="text" name="contract_date" value="<?php echo ($fabric_inward_row==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($fabric_inward_row['CONTRACT_DATE'])); ?>" class="form-control datepicker" required />
								</div>
								<div class="clear"></div>

									<div class="caption">Dyeing Unit :</div>
									<div class="form-group field" style="width:350px;margin-bottom:0px;">
										<select class="selectpicker show-tick form-control" name="supplier_id" data-live-search="true" required >
                      <option value="" ></option>
                      <?php
                      if(mysql_num_rows($supplier_list)){
                        while($supplierDetails = mysql_fetch_assoc($supplier_list)){
                          ?>
                          <option value="<?php echo $supplierDetails['SUPP_ACC_CODE']; ?>" <?php echo ($supplierDetails['SUPP_ACC_CODE']== $fabric_inward_row['SUPPLIER_ID'])?"selected":""; ?> ><?php echo $supplierDetails['SUPP_ACC_TITLE']; ?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
									</div>

									<div class="caption">Dyeing Ut. Lot # </div>
									<div class="field" style="width:150px">
										<input type="text" name="dyeing_unit_lot_no" value="<?php echo $fabric_inward_row['DYEING_UNIT_LOT_NO']; ?>" class="form-control" required />
									</div>
									<div class="clear"></div>

									<div class="caption">Total Meter : </div>
									<div class="field" style="width:150px">
										<input type="text" name="total_meter" value="<?php echo ($fabric_inward_row==NULL)?$total_meters:$fabric_inward_row['TOTAL_METER']; ?>" class="form-control" readonly required />
									</div>

									<div class="caption" style="width:350px"> &#37; Difference : </div>
									<div class="field" style="width:150px">
										<input type="text" name="difference_percent" value="<?php echo $fabric_inward_row['DIFFERENCE_PERCENT']; ?>" class="form-control" readonly  />
									</div>
									<div class="clear"></div>

									<?php /*

									<div class="caption">Broker :</div>
									<div class="form-group field" style="width:350px;margin-bottom:0px;">
										<select class="selectpicker show-tick form-control" name="broker_id" data-live-search="true">
												<option value="" ></option>
												<?php
												if(mysql_num_rows($broker_list)){
														while($brokerArray = mysql_fetch_assoc($broker_list)){
																?>
																<option value="<?php echo $brokerArray['ACCOUNT_CODE']; ?>" <?php echo ($brokerArray['ACCOUNT_CODE']==$fabric_inward_row['BROKER_ID'])?"selected":""; ?> ><?php echo $brokerArray['ACCOUNT_TITLE']; ?></option>
														<?php
														}
												}
												?>
										</select>
									</div>

									<div class="clear"></div>

									*/ ?>

									<div style="margin-top:0px"></div>
								</div> <!-- End form -->
								<div class="clear"></div>
								<div id="form">
									<div class="col-xs-12">
										<table id="pro_table" style="width:100%;">
											<thead>
												<tr>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Fabric Output</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Thaan</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">QtyLength</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Rate</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Amount</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Variety/Color</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr class="inputRow">
													<td>
														<select class="form-control show-tick fabric_type" name="fabric_type">
															<?php foreach($fabric_array as $key => $value){
																?>
																	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
																<?php
															} ?>
														</select>
													</td>
													<td>
														<input type="text" class="thaan form-control text-center"/>
													</td>
													<td>
														<input type="text" class="quantity form-control text-center"/>
													</td>
													<td>
														<input type="text" class="unit_price form-control text-center"/>
													</td>
													<td>
														<input type="text" class="amount form-control text-center" readonly/>
													</td>
													<td>
														<input type="text" class="color form-control text-center"/>
													</td>
													<td>
														<button type="button" class="btn btn-default btn-block enter_new_row" name="enter">Enter</button>
													</td>
												</tr>
											</tbody>
											<tbody>
												<?php
													$total_thaan    = 0;
													$total_quantity = 0;
													$total_amount 	= 0;
													if(isset($fabric_contract_list)&&mysql_num_rows($fabric_contract_list)){
														while($fabric_detail_row = mysql_fetch_assoc($fabric_contract_list)){
															$total_thaan 		+= $fabric_detail_row['THAAN'];
															$total_quantity += $fabric_detail_row['QUANTITY'];
															$total_amount   += $fabric_detail_row['TOTAL_AMOUNT'];
																?>
																<tr class='table_row' data-id="<?php echo $fabric_detail_row['ID']; ?>">
																	<td class='text-center fabric_type' data-value="<?php echo $fabric_detail_row['FABRIC_TYPE']; ?>"><?php echo ($fabric_detail_row['FABRIC_TYPE']=='')?"":$fabric_array[$fabric_detail_row['FABRIC_TYPE']]; ?></td>
																	<td class='text-center thaan'><?php echo $fabric_detail_row['THAAN']; ?></td>
																	<td class='text-center quantity'><?php echo $fabric_detail_row['QUANTITY']; ?></td>
																	<td class='text-center unit_price'><?php echo $fabric_detail_row['UNIT_PRICE']; ?></td>
																	<td class='text-center amount'><?php echo $fabric_detail_row['TOTAL_AMOUNT']; ?></td>
																	<td class='text-center color'><?php echo $fabric_detail_row['COLOR']; ?></td>
																	<td class="text-center">
																		<a id="view_button" onclick='editMe(this);'><i class="fa fa-pencil"></i></a>
																		<a class="pointer" onclick='deleteMe(this);'><i class="fa fa-times"></i></a>
																	</td>
																</tr>
																<?php
														}
													}
												?>
											</tbody>
											<tfoot>
												<tr>
													<td class="text-right" >Total</td>
													<td class="text-center thaan_total"><?php echo $total_thaan; ?></td>
													<td class="text-center quantity_total"><?php echo $total_quantity; ?></td>
													<td class="text-center"> - - - </td>
													<td class="text-center amount_total"><?php echo $total_amount; ?></td>
													<td class="text-center" colspan="2"> - - - </td>
												</tr>
											</tfoot>
										</table>
									</div>
									<div class="clear"></div>
									<div class="caption"></div>
									<div class="field">
										<input type="button" name="save" value="<?php echo ($fabric_id>0)?"Update":"Save"; ?>" class="button" />
										<?php if ($fabric_id>0) { ?>
											<a target="_blank" class="button" href="print-processing-details.php?id=<?php echo $_GET['id']; ?>">Print</a>
											<?php } ?>
											<input type="button" value="New Form" class="button" onclick="window.location.href='processing-contract-detail.php?lot_no=<?php echo $lot_no; ?>'";  />
										</div>
										<div class="clear"></div>
									</form>
								</div> <!-- End form -->
							</div> <!-- End #tab1 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->

					<!-- Delete confirmation popup -->
					<div id="myConfirm" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Confirmation</h4>
								</div>
								<div class="modal-body">
									<p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
									<button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<!-- Loading bar confirmation popup -->
					<div id="myLoading" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Processing...</h4>
								</div>
								<div class="modal-body">
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
										aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
										<span class="sr-only">100% Complete</span>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->


				<div id="selecCustomer" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="background: #72a9d8; color: white;">
								<h4 class="modal-title">Attention!</h4>
							</div>
							<div class="modal-body">
								<p class="text-danger" style="font-weight: bold;">Please Select Customer first.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->



				<!-- Edit popup -->
				<div id="myedit" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="background: #72a9d8; color: white;">
								<h4 class="modal-title">Edit Record</h4>
							</div>
							<div id="form" class="modal-body">
								<form class="editmyform" id="pro_table">
									<input type="hidden" class="form-control" name="rowid" value="" />
									<input type="hidden" class="form-control" name="stock_hand1" value="" />
									<div style="width:450px; margin: auto;">
										<div class="caption"> Item :</div>
										<div class="field">
											<select class="itemSelector1 show-tick form-control"
															data-style="btn-default"
															data-live-search="true" style="border:none">
												 <option selected value=""></option>
<?php
									$itemsCategoryList   = $objItemCategory->getList();
									if(mysql_num_rows($itemsCategoryList)){
											while($ItemCat = mysql_fetch_array($itemsCategoryList)){
		$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
			<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
		if(mysql_num_rows($itemList)){
			while($theItem = mysql_fetch_array($itemList)){
				if($theItem['ACTIVE'] == 'N'){
					continue;
				}
				if($theItem['INV_TYPE'] == 'B'){
					continue;
				}
?>
				<option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
			}
		}
?>
			</optgroup>
<?php
											}
									}
?>
											</select>
										</div>
										<div class="clear"></div>
										<div class="caption">Carton/Cloth :</div>
										<div class="field">
											<input class="form-control" name="carton1" value="" />
										</div>
										<div class="clear"></div>

										<div class="caption">Qty/Length :</div>
										<div class="field">
											<input class="form-control" name="qty_carton1" value="" />
										</div>
										<div class="clear"></div>

										<div class="caption">Qty Receipt :</div>
										<div class="field">
											<input class="form-control" name="qty_receipt1" value="" style="background-color:#F3F3F3;" readonly="readonly" />
										</div>
										<div class="clear"></div>


										<div class="clear" style="height:30px"></div>

										<div class="modal-footer">
											<button type="button" class="btn btn-primary btn-sm" id="addrow" data-dismiss="modal">Update</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										</div>

									</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!--body-wrapper-->
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
