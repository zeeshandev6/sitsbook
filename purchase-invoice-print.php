<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/sale.php';
	include 'common/classes/sale_details.php';
	include 'common/classes/accounts.php';
	include 'common/classes/customers.php';
	include 'common/classes/suppliers.php';
	include 'common/classes/items.php';
	include('common/classes/services.php');
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');
	include('common/classes/purchase_invoice.php');
	include('common/classes/purchase_invoice_details.php');

	//Permission
	if(!in_array('invoice-purchases',$permissionz) && $admin != true){
			echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
			echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
			echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
			echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
			echo '</div>';
			exit();
	}
	//Permission ---END--

	$objCompanyDetails   = new CompanyDetails();
	$objSales            = new Sale();
	$objSaleDetails      = new SaleDetails();
	$objInvoice          = new PurchaseInvoice();
	$objInvoiceDetails   = new PurchaseInvoiceDetails();
	$objChartOfAccounts  = new ChartOfAccounts();
	$objCustomers        = new Customers();
	$objSuppliers 			 = new Suppliers();
	$objItems  	         = new Items();
	$objServices         = new Services();
	$objJournalVoucher   = new JournalVoucher();
	$objConfigs   	     = new Configs();

	$invoice_format			 = $objConfigs->get_config('INVOICE_FORMAT');

	$individual_discount 	= $objConfigs->get_config('SHOW_DISCOUNT');
  $individual_discount 	= ($individual_discount=='Y')?true:false;

    $use_taxes           = $objConfigs->get_config('SHOW_TAX');
    $use_taxes           = ($use_taxes=='Y')?true:false;

	$invoice_format = explode('_', $invoice_format);
	if(isset($_GET['id'])){
		$invoice_id 					 = mysql_real_escape_string($_GET['id']);
		$invoiceDetails 			 = $objInvoice->getDetail($invoice_id);
		$saleDetailList 			 = $objInvoiceDetails->getList($invoice_id);
		$customer_balance      = $objJournalVoucher->getOpeningBalanceOfVoucher($invoiceDetails['CUSTOMER'],$invoiceDetails['VOUCHER_ID'],$invoiceDetails['INVOICE_DATE']);
		$cutomer_balance_array = $objJournalVoucher->getBalanceType($invoiceDetails['CUSTOMER'], $customer_balance);
	}
	///   INVOICE_FORMAT Config Size - Header - Duplicate
	$invoice_num 		 = $invoice_format[1];
	$invoiceStyleCss = 'resource/css/invoiceStyle.css';
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>

    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "<?php echo $invoiceStyleCss; ?>",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			//$(".printThis").click();
			$(".ledger_view").click(function(){
				var gl_date = $(this).attr('data-date');
				var gl_code = $(this).attr('data-code');
				if(gl_code != ''){
					$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
						if(data == ''){
							var win = window.open('invoice-ledger-print.php','_blank');
							if(win){
							    win.focus();
							}else{
							    displayMessage('Please allow popups for This Site');
							}
						}
					});
				}
			});
			//$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($invoice_id)){
		$customer 	   = $objChartOfAccounts->getAccountTitleByCode($invoiceDetails['CUSTOMER']);
		$invoice_date  = date('d-M-Y',strtotime($invoiceDetails['INVOICE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($invoice_date));
		$companyLogo   = $objCompanyDetails->getLogo('1');
		$company  = $objCompanyDetails->getActiveProfile();
?>

<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
        </div><!--headerWrapper-->
    </div><!--header-->
    <div class="clear"></div>
    <div class="printThisDiv" style="position: relative;height:9.7in;">
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint" style="height:10.6in !important;">
            <div class="invoiceHead" style="width: 100%;margin: 0px auto;">
<?php
				if($companyLogo != ''){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
            		<div style="font-size:26px;"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']; ?>
                    <br />
                    Contact : <?php echo $company['CONTACT']; ?>
                    <br />
                    <?php echo ($company['EMAIL']=='')?"":" Email : ".$company['EMAIL']."<br />"; ?>
                </div>
                <div class="clear"></div>
                <div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
	               <?php echo (substr($invoiceDetails['CUSTOMER'], 0,6) == '010101')?"CASH":""; ?> PURCHASE BILL
                </div>

                <div class="clear" style="height: 5px;"></div>
<?php
				if($invoice_num == '1'){
?>
                <div class="infoPanel pull-left" style="height:auto !important;">
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">From : <?php echo $customer; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right" style="width:200px;height:auto !important;">
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Bill No. <?php echo $invoiceDetails['INVOICE_NO']; ?></span>
                	<div class="clear"></div>
	                <span class="pull-left" style="padding:0px 0px;font-size:14px;">Bill Date. <?php echo $invoice_date; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="clear"></div>
                <?php if($invoiceDetails["SUBJECT"] != ''){ ?>
                <div class="infoPanel" style="height:auto !important;">
                	<b>Subject :  </b>
                	<?php echo $invoiceDetails["SUBJECT"]; ?>
                </div>
                <?php } ?>
                <div class="clear mt-10"></div>

<?php
				}

				if($invoice_num == '2'){
?>
				<div class="invoPanel">
					<div class="invoPanel">
						<div class="clear"></div>
	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Name: </span>
	                		<span class="text_line"><?php echo $customer['ACC_TITLE']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Address: </span>
	                		<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">City: </span>
	                		<span class="text_line"><?php echo $customer['CITY']; ?></span>
	                	</span>
	                	<div class="clear"></div>
	                </div>
				</div>
				<div class="clear" style="height:10px;"></div>
<?php
				}
?>
				<div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 100%;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="5%">Sr#</th>
                            <th width="40%">Description</th>
                            <th width="10%">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($saleDetailList)){
							while($row = mysql_fetch_array($saleDetailList)){
?>
								<tr>
									<td><?php echo $counter; ?></td>
									<td ><span style="float:left;margin-left:5px;"><?php echo $row['DESCRIPTION']; ?></span></td>
									<td><span style="float:right;margin-right:5px;"><?php echo number_format($row['AMOUNT'],2); ?></span></td>
								</tr>
<?php
									$totalAmount += $row['AMOUNT'];
									$counter++;
							}
						}
?>
                    </tbody>
                    <?php
                    	$colum_skipper = 0;
                    	$columns       = 2;
                    ?>
                    <tfoot>
                        <tr>
                            <td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Bill Total</span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($totalAmount,2); ?></span></td>
                        </tr>
												<tr>
													<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> Previous Balance </span></td>
													<td style="text-align: right;padding-right: 10px;">
														<span style="float:right;">
															<?php echo number_format(($cutomer_balance_array['BALANCE']),2); ?>
														</span>
													</td>
												</tr>
												<tr>
													<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> Total </span></td>
													<td style="text-align: right;padding-right: 10px;">
														<span style="float:right;">
															<?php echo number_format(($cutomer_balance_array['BALANCE']+$totalAmount),2) ?>
														</span>
													</td>
												</tr>
                    </tfoot>
                </table>
                <div class="clear"></div>

                <?php if($invoiceDetails['NOTES'] != ''){ ?>
                <div class="infoPanel pull-left" style="position:relative;top:-20px;">
                	<span class="pull-left" style="padding-left:0px !important;">
	                	<span class="pull-left" style="padding:0px 0px !important;font-size:14px;border-bottom:1px solid #000;">Notes &amp; Enclosures</span>
	                </span>
	                <div class="clear" style="height:10px;"></div>
	                <span class="pull-left" style="padding-left:0px !important;">
	                	<span class="pull-left" style="padding:0px 0px !important;;font-size:14px;text-align:justify !important;"><?php echo $invoiceDetails['NOTES']; ?></span>
	                </span>
	                <div class="clear"></div>
                </div>
                <?php } ?>
                <div class="clear"></div>

                <div class="invoice_footer" style="position:absolute !important;bottom:5px !important;">
	                <div class="auths pull-left">
	                	Prepared By :
	                	<span class="authorized"></span>
	                </div><!--partyTitle-->
	                <div class="auths pull-right">
	                	Authorized By :
	                </div><!--partyTitle-->
	                <div class="clear" style="height: 10px;"></div>
	                <div class="invoice-developer-info"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
	        	</div>
            </div><!--invoiceBody-->
        </div><!--invoiceLeftPrint-->
        <div class="clear"></div>
    </div><!--invoiceContainer-->
    </div><!--printThisDiv-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
