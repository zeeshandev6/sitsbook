<?php
	include 'common/connection.php';
	include 'common/config.php';
?>
<!DOCTYPE html  
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/listing.css" type="text/css" media = "screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<style>
			a.poplight{
				color:#666;
			}
			h3{
				font-size: 14px;
			}
		</style>
      	<script type = "text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type = "text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
      	<script type = "text/javascript" src="resource/scripts/tab.js"></script>
      	<script type = "text/javascript" src="resource/scripts/configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
        	$(function(){
        		$("button.synchronize").click(function(){
        			$(this).hide();
	        		$(this).attr("disabled",true);
	        		$("img.sync").addClass("fa-pulse");
	        		$(this).fadeIn();
	        		
	        		setTimeout(function(){
	        			$("img.sync").removeClass("fa-pulse");
	        			$("button.synchronize").attr("disabled",false);
	        			displayMessage("Success! Fully Synched.");
	        		},5000);
	        	});
        	});
        </script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p>Sync Online</p>
                  		</div><!-- End .content-box-header -->  
               			<div style=" clear:both;"></div>
<?php
						if(isset($permissionDetails)&&in_array('backup-create',$permissionDetails) || $admin == true){
?>
                        	<button class="backup_panel synchronize">
                        		<img src="resource/images/sync.png" class="sync" />
                            	<h5>Sync Now</h5>
                     		</button>
<?php
						}
?>
        				<div style=" clear:both;"></div>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
        <div id="fade"></div>
        <div id="xfade"></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		<?php if(isset($message)){ ?>displayMessage('<?php echo $message; ?>');<?php } ?>
    });
	
	var centerThisDiv = function(elementSelector){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		var elemHeight = $(elementSelector).height()/2;
		var elemWidth = $(elementSelector).width()/2;
		var posTop = win_hi-elemHeight;
		var posLeft = win_width-elemWidth;
		$(elementSelector).css({
			'position': 'fixed',
			'top': posTop,
			'left': posLeft,
			'margin': '0px'
		});
	}
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete button'>Close</a></div>");
		centerThisDiv("#popUpDel");
		$("#popUpDel").hide();
		$("#xfade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#xfade").fadeOut();
			$("#popUpDel").remove();
		});
	}
	
</script>