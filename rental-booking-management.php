<?php
	include('common/connection.php');
	include 'common/config.php';
	include ('common/classes/rental_booking.php');
	include ('common/classes/ordering.php');

	//Permission
	if(!in_array('rental-booking',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--
	$objRentalBooking = new RentalBooking();
	$objOrdering      = new Ordering();
	//$totalRows = $objRentalBooking->countRows();

	$total = $objConfigs->get_config('PER_PAGE');

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);
	$invoiceSize = $invoice_format[0]; // S  L
	if($invoiceSize == 'L'){
		$invoiceFile = 'rental-invoice.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile = 'rental-invoice.php'; //$invoiceFile = 'purchase-invoice-duplicates.php';
	}elseif($invoiceSize == 'S'){
		//$invoiceFile = 'purchase-bill-small.php';
	}

	if(isset($_GET['search'])){
		$objRentalBooking->title 						= (isset($_GET['title']))?$_GET['title']:"";
		$objRentalBooking->fromBookingDate 	= (isset($_GET['fromBookingDate']))?$_GET['fromBookingDate']:"";
		$objRentalBooking->toBookingDate 		= (isset($_GET['toBookingDate']))?$_GET['toBookingDate']:"";
		$objRentalBooking->fromDeliveryDate = (isset($_GET['fromDeliveryDate']))?$_GET['fromDeliveryDate']:"";
		$objRentalBooking->toDeliveryDate 	= (isset($_GET['toDeliveryDate']))?$_GET['toDeliveryDate']:"";
		$objRentalBooking->booking_status 	= (isset($_GET['booking_status']))?$_GET['booking_status']:"";
		$objRentalBooking->client_name 			= (isset($_GET['client_name']))?$_GET['client_name']:"";
		$objRentalBooking->client_mobile 		= (isset($_GET['client_mobile']))?$_GET['client_mobile']:"";
		$objRentalBooking->order_status 		= (isset($_GET['order_status']))?$_GET['order_status']:"";
	}

	if(!$admin){
		$objRentalBooking->user_id = $user_id;
	}

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	if(!isset($_GET['search'])){
		$objRentalBooking->limit_start = $start;
	}

	$objRentalBooking->limit_end = $total;
	$rentalList = $objRentalBooking->searchRental();
	$found_records = $objRentalBooking->found_records;
	/*if(isset($_POST['save_changes'])){
	$objRentalBooking->delivery_time = date("Y-m-d H:i:s",strtotime($_POST['delivery_time1']));
	$objRentalBooking->return_time = date("Y-m-d H:i:s",strtotime($_POST['return_time1']));
	$rental_id = $_POST['rental_id'];
	$objRentalBooking->updateDatesStatus($rental_id);
	echo "<script> window.location.href = '".$_SERVER['PHP_SELF']."' </script>";
	}*/
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" type="text/css" />
	<style>
	td{
		padding: 10px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$(".selectpicker").selectpicker();
		$("a.pointer").click(function(){
			$(this).deleteMainRow("db/del-rental-booking.php");
		});
	});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("input[name=save_changes]").click(function() {
			var delivery_time = $('input[name=delivery_time1]').val();
			var return_time = $('input[name=return_time1]').val();
			var rental_id = $('input[name=rental_id]').val();

			$.post("db/updateRentalBookingDates.php",{delivery_time:delivery_time,return_time:return_time,rental_id:rental_id},function(data){
				$.get("rental-booking-management.php",function(data_html){
					var content = $(data_html).find(".update_row").html();
					$(".update_row").html(content);
					//$(".update_row").removeClass("update_row");
					hidePopUpBox();
					window.location.href = 'rental-booking-management.php?tab=list';
				});

			});
		});
		/*$(".item_status").click(function(){
		$(".update_row").removeClass("update_row");
		var deliver_date = $(this).attr("deliver-date");
		var return_date = $(this).attr("return-date");
		var rental_id = $(this).attr("rental-id");
		$("input[name=delivery_time1]").val(deliver_date);
		$("input[name=return_time1]").val(return_date);
		$("input[name=rental_id]").val(rental_id);
		$(this).parent().parent().addClass("update_row");
		showPopUpBox();
	});*/


	$(".addDetailRow").click(function(){
		$(this).quickSave();
	});
	$(".save_rental").click(function(){
		saveRentalBooking();
	});
});
</script>

</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Booking Management - <?php echo $found_records; ?> Records</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
						<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
						<a href="rental-booking-details.php"><div class="tab">New</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1" style="display:block;">
					<table width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th style="text-align:center;width:5%;">Booking #</th>
								<th style="text-align:center;width:20%;" >Time info</th>
								<th style="text-align:center;width:20%;" >Client Info</th>
								<th style="text-align:center;width:20%;" >Particulars</th>
								<th style="text-align:center;width:10%;" >Booking Status</th>
								<th style="text-align:center;width:10%;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($rentalList)){
								while($rentalRow = mysql_fetch_array($rentalList)){
									$item_names    = $objRentalBooking->getItemTitlesByBookingId($rentalRow['ID']);
									$service_names = $objRentalBooking->getServiceTitlesByBookingId($rentalRow['ID']);
									$item_names_arr=array();
									$service_arr   =array();
									if(mysql_num_rows($item_names)){
										while($ir=mysql_fetch_assoc($item_names)){
											$item_names_arr[]=$ir['NAME'];
										}
									}
									if(mysql_num_rows($service_names)){
										while($sr=mysql_fetch_assoc($service_names)){
											$service_arr[]=$sr['TITLE'];
										}
									}
									$item_names_arr = array_merge($item_names_arr,$service_arr);
									?>
									<tr data-row-id="<?php echo $rentalRow['ID'] ?>">
										<td style="text-align:center"><?php echo $rentalRow['BOOKING_ORDER_NO']; ?></td>
										<td>
											Booking Time: <?php if($rentalRow['BOOKING_DATE_TIME'] != '0000-00-00'){ echo date('d-m-Y',strtotime($rentalRow['BOOKING_DATE_TIME'])); }else{ echo ""; } ?> <br />
											Delivery Time: <?php if($rentalRow['DELIVERY_DATE_TIME'] != '0000-00-00'){ echo date('d-m-Y',strtotime($rentalRow['DELIVERY_DATE_TIME'])); }else{ echo ""; } ?> <br />
											Return Time: <?php if($rentalRow['RETURNING_DATE_TIME'] != '0000-00-00'){ echo date('d-m-Y',strtotime($rentalRow['RETURNING_DATE_TIME'])); }else{ echo ""; } ?>
										</td>
										<td>Name: <?php echo $rentalRow['CLIENT_NAME']."<br /> Cell: ". $rentalRow['CLIENT_MOBILE']; ?></td>
										<td style="text-align:center">
											<?php echo implode(' , ',$item_names_arr); ?>
										</td>
										<td>
											<select class="selectpicker show-tick form-control booking_status_change" row-id="<?php echo $rentalRow['ID'] ?>" name="booking_status" data-live-search="true">
												<option value="O" <?php echo (isset($rentalRow) && $rentalRow['BOOKING_STATUS']=="O")?"selected":""; ?> >Order</option>
												<option value="P" <?php echo (isset($rentalRow) && $rentalRow['BOOKING_STATUS']=="P")?"selected":""; ?> >Partial Delivered</option>
												<option value="D" <?php echo (isset($rentalRow) && $rentalRow['BOOKING_STATUS']=="D")?"selected":""; ?> >Delivered</option>
												<option value="R" <?php echo (isset($rentalRow) && $rentalRow['BOOKING_STATUS']=="R")?"selected":""; ?> >Return</option>
											</select>
											</td>
											<td style="text-align:center">
												<a id="view_button" href="rental-booking-details.php?id=<?php echo $rentalRow['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
												<a id="view_button" target="_blank" href="rental-invoice.php?id=<?php echo $rentalRow['ID']; ?>" title="Print"><i class="fa fa-print"></i></a>
												<?php if($rentalRow['BOOKING_STATUS']!='R'){ ?>
													<a do="<?php echo $rentalRow['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
													<?php } ?>
											</td>
										</tr>
										<?php
											}
										}else{
											//}elseif(isset($_GET['search'])){
										?>
											<tr>
												<th colspan="100" style="text-align:center;">
													<?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
												</th>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
								<div class="col-xs-12 text-center">
									<?php
									//if(!isset($_GET['search']) && $found_records > $total){
									if($found_records > $total){
										?>
										<nav>
											<ul class="pagination">
												<?php
												$count = $found_records;
												$total_pages = ceil($count/$total);
												$i = 1;
												$thisFileName = $_SERVER['PHP_SELF'];
												if(isset($this_page) && $this_page>0){
													?>
													<li>
														<a href="<?php echo $thisFileName; ?>?page=1">First</a>
													</li>
													<?php
												}
												if(isset($this_page) && $this_page>=1){
													$prev = $this_page;
													?>
													<li>
														<a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
													</li>
													<?php
												}
												$this_page_act = $this_page;
												$this_page_act++;
												while($total_pages>=$i){
													$left = $this_page_act-5;
													$right = $this_page_act+5;
													if($left<=$i && $i<=$right){
														$current_page = ($i == $this_page_act)?"active":"";
														?>
														<li class="<?php echo $current_page; ?>">
															<?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
														</li>
														<?php
													}
													$i++;
												}
												$this_page++;
												if(isset($this_page) && $this_page<$total_pages){
													$next = $this_page;
													echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
												}
												if(isset($this_page) && $this_page<$total_pages){
													?>
													<li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
												</ul>
											</nav>
											<?php
										}
									}
									?>
								</div>
								<div class="clear"></div>
							</div> <!--End bodyTab1-->
							<div style="height:0px;clear:both"></div>
							<div id="bodyTab2" style="display:none;" >
								<div id="form">
									<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
										<div class="title" style="font-size:20px; margin-bottom:20px">Search Booking</div>

										<div class="caption"></div>
										<div class="message red"><?php echo (isset($message))?$message:""; ?></div>
										<div class="clear"></div>

										<div class="caption">Title</div>
										<div class="field">
											<input type="text" value="<?php echo $objRentalBooking->title; ?>" name="title" class="form-control" />
										</div>
										<div class="clear"></div>

										<div class="caption">From Booking Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->fromBookingDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->fromBookingDate)); ?>" name="fromBookingDate" class="form-control datepicker" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Booking Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->toBookingDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->toBookingDate)); ?>" name="toBookingDate" class="form-control datepicker"/>
										</div>
										<div class="clear"></div>

										<div class="caption">From Delivery Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->fromDeliveryDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->fromDeliveryDate)); ?>" name="fromDeliveryDate" class="form-control datepicker" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Delivery Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->toDeliveryDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->toDeliveryDate)); ?>" name="toDeliveryDate" class="form-control datepicker"/>
										</div>
										<div class="clear"></div>

										<div class="caption"> Booking Status:</div>
										<div class="field">
											<select class="selectpicker show-tick form-control" name="booking_status" data-live-search="true">
												<option value="O">Order</option>
												<option value="P">Partial Delivered</option>
												<option value="D">Delivered</option>
												<option value="R">Return</option>
											</select>
										</div>
										<div class="clear"></div>

										<div class="caption">Client Name</div>
										<div class="field">
											<input type="text" value="<?php echo $objRentalBooking->clinet_name; ?>" name="clinet_name" class="form-control" />
										</div>
										<div class="clear"></div>

										<div class="caption"> Ordering Status :</div>
										<div class="field">
											<select class="selectpicker show-tick form-control" name="order_status" data-live-search="true" >
												<option value="" ></option>
												<option value="Y">In Stock</option>
												<option value="N">Out of Stock</option>
											</select>

										</div>
										<div class="clear"></div>
										<div class="caption"></div>
										<div class="field">
											<input type="submit" value="Search" name="search" class="button"/>
										</div>
										<div class="clear"></div>
									</form>
								</div><!--form-->
							</div> <!-- End bodyTab2 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->
				</div><!--body-wrapper-->

				<!-- popup window start -->
				<div id="popUpBox" style="display:none;top:100px !important">
					<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>
					<!---<form action="" method="post" id="myid">-->
					<input type="hidden" name="rental_id" />
					<div class="clear"></div>
					<div class="caption">Delivery Time</div>
					<div class="field" style="width: 250px;">
						<input type="text" name="delivery_time1" value="" class="form-control" />
					</div>

					<div class="caption">Return Time</div>
					<div class="field" style="width: 250px;">
						<input type="text" name="return_time1" value="" class="form-control" />
					</div>

					<div class="field" style="width: 250px;margin-top:20px;">
						<input type="submit" name="save_changes"   value="Update" class="btn btn-primary btn-sm" />
					</div>

				</div>
				<div class="clear"></div>
				<!-- popup window end -->
				</div>
				<div id="xfade"></div>
				<div id="fade"></div>
</body>
</html>
			<?php include('conn.close.php'); ?>
			<script>
			$(document).ready(function() {
				$(".shownCode,.loader").hide();
				$("input.supplierTitle").keyup(function(){
					$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
				});
				$(window).keydown(function(e){
					if(e.keyCode==113){
						e.preventDefault();
						window.location.href = "<?php echo "inventory-details.php"; ?>";
					}
				});
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>tab('2', '1', '2');<?php } ?>

			</script>
