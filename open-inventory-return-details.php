<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/inventory-return.php');
	include('common/classes/inventory-return-details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/tax-rates.php');
	//Permission
	if(!in_array('purchase-return',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--
	$objInventory        = new InventoryReturn();
	$objInventoryDetails = new InventoryReturnDetails();
	$objItems            = new Items();
	$objItemCategory     = new itemCategory();
	$objTaxRates         = new TaxRates();

	$use_income_tax      = $objConfigs->get_config('INCOME_TAX');
	$stock_check      	 = $objConfigs->get_config('STOCK_CHECK');

	$suppliersList   	 		= $objInventory->getSuppliersList();
	$itemsCategoryList   	= $objItemCategory->getList();
	$taxRateList     	 		= $objTaxRates->getList();

	$purchase_id = 0;
	if(isset($_GET['id'])){
		$purchase_id = mysql_real_escape_string($_GET['id']);
		if($purchase_id != '' && $purchase_id != 0){
			$inventory = mysql_fetch_array($objInventory->getRecordDetails($purchase_id));
			$inventoryDetails = $objInventoryDetails->getList($purchase_id);
		}else{
			$purchase_id = 0;
		}
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
        html{

        }
        .ui-tooltip{
            font-size: 12px;
            padding: 5px;
            box-shadow: none;
            border: 1px solid #999;
        }
        div.tabPanel .tab,div.tabPanel .tabSelected{
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
        }
        .input_sized{
            float:left;
            width: 152px;
            padding-left: 5px;
            border: 1px solid #CCC;
            height:30px;
            -webkit-box-shadow:#F4F4F4 0 0 0 2px;
            border:1px solid #DDDDDD;

            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;

            -moz-border-radius-topleft:3px;
            -moz-border-radius-topright:3px;
            -moz-border-radius-bottomleft:3px;
            -moz-border-radius-bottomright:5px;

            -webkit-border-top-left-radius:3px;
            -webkit-border-top-right-radius:3px;
            -webkit-border-bottom-left-radius:3px;
            -webkit-border-bottom-right-radius:3px;

            box-shadow: 0 0 2px #eee;
            transition: box-shadow 300ms;
            -webkit-transition: box-shadow 300ms;
        }
        .input_sized:hover{
            border-color: #9ecaed;
            box-shadow: 0 0 2px #9ecaed;
        }
        td,th{
            padding: 10px 5px !important;
            border:1px solid #CCC !important;
        }
        .quickSubmit input[type='text']{
            text-align: center !important;
        }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/open.inventory.return.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Return</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="inventory-return.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="Inventory-return.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Return</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div id="form" style="width: 1000px;margin: 20px auto;">
			                <div class="myCat" onClick="letMeGo(this);"></div>
														<input type="hidden" value="<?php echo $stock_check; ?>" class="stock_check" />
                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Return Date</div>
                            <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                                <input type="text" name="rDate" id="datepicker" value="<?php echo (isset($inventory))?date("d-m-Y",strtotime($inventory['PURCHASE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
                            </div>
                            <div class="caption" style="width:100px;">Memo #</div>
                            <div class="field" style="width:80px">
                            	<input class="form-control bill_number" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>"  />
                            </div>
														<?php if($purchaseOrdersModule == 'Y'){ ?>
                            <div class="caption" style="width:100px;">PO #</div>
                            <div class="field" style="width:80px;">
                            	<input class="form-control po_number" value="<?php echo (isset($inventory))?$inventory['PO_NUMBER']:''; ?>"  />
                            </div>
														<?php } ?>
														<?php if($saleTaxModule == 'Y'){ ?>
                            <div class="field" style="width:118px;margin-left: 30px;">
                            	<input id="cmn-toggle-unreg" value="Y" class="css-checkbox unregistered_tax" type="checkbox" <?php echo (isset($inventory)&&$inventory['UNREGISTERED_TAX'] == 'N')?"checked":""; ?> />
															<label for="cmn-toggle-unreg" class="css-label" style="margin-top:5px;margin-right: 20px;">Unregistered(FBR)</label>
                            </div>
														<?php } ?>
                            <div class="clear"></div>
                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Account</div>
                            <div class="field" style="width:200px;position:relative;">
                                <select class="supplierSelector form-control "
                                        data-style="btn-default" data-hide-disabled='true'
                                        data-live-search="true" style="border:none" <?php echo ($purchase_id!=0)?"disabled":""; ?> >
                                   <option selected value=""></option>
<?php
                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected":"";
?>
                                   <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
<?php
                            if(mysql_num_rows($cash_in_hand_list)){
                                while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                                    $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                                    if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
                                        continue;
                                    }
                                    $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
?>
                                    <option <?php echo $selected; ?> disabled data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
                            </div>
                            <div class="caption">
                                <input type="checkbox" id="mor1" <?php echo (isset($inventory) && substr($inventory['SUPP_ACC_CODE'],0,6)== '010101')?"checked=\"checked\"":""; ?> onchange="makeItCash(this);" class="trasactionType css-checkbox" value="0101010001" title="Cash In Hand" />
                                <label id="mor1" for="mor1" class="css-label" title="Ctrl+Space"><small>Cash Transaction</small></label>
                            </div>
                            <div class="supplier_name_div" style="display: <?php echo (isset($inventory)&&substr($inventory['SUPP_ACC_CODE'],0,6)=='010101')?"":"none" ?>;">
	                            <div class="field" style="width:195px;margin-left:10px;">
	                            	<input class="form-control supplier_name" placeholder="Name" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>"  />
	                            </div>
                            </div>
                            <div class="clear"></div>
	                        <input type="hidden" class="purchase_id" value="<?php echo $purchase_id; ?>" />
                            <div style="height: 10px;"></div>
                            <table class="prom">
                            <thead>
                                <tr>
                                   <th width="20%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Discount %</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">StockInHand</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="quickSubmit" style="background:none">
                                    <td >
                                        <select class="itemSelector show-tick form-control"
                                                data-style="btn-default"
                                                data-live-search="true" style="border:none">
																						<option selected value=""></option>
		<?php
																			if(mysql_num_rows($itemsCategoryList)){
																					while($ItemCat = mysql_fetch_array($itemsCategoryList)){
												$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
		?>
													<optgroup label="<?php echo $ItemCat['NAME']; ?>">
		<?php
												if(mysql_num_rows($itemList)){
													while($theItem = mysql_fetch_array($itemList)){
														if($theItem['ACTIVE'] == 'N'){
															continue;
														}
														if($theItem['INV_TYPE'] == 'B'){
															continue;
														}
		?>
																												 <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
		<?php
													}
												}
		?>
													</optgroup>
		<?php
																					}
																			}
		?>
                                        </select>
                                    </td>
                                    <td >
                                        <input type="text" class="quantity form-control"/>
                                    </td>
                                    <td >
                                        <input type="text" class="unitPrice form-control"/>
                                    </td>
                                    <td >
                                        <input type="text" class="discount form-control"/>
                                    </td>
                                    <td >
                                        <input type="text"  readonly value="0" class="subAmount form-control"/>
                                    </td>
                                    <td  class="taxTd">
                                    	<input type="text" class="form-control taxRate" value="" />
                                    </td>
                                    <td >
                                        <input type="text"  readonly   value="0" class="taxAmount form-control" />
                                    </td>
                                    <td >
                                        <input type="text"  readonly   value="0" class="totalAmount form-control" />
                                    </td>
                                    <td >
                                        <input type="text"  class="inStock form-control" theStock="0" readonly />
                                    </td>
                                    <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                </tr>
                            </tbody>
                            <tbody>
                            	<!--doNotRemoveThisLine-->
                                    <tr style="display:none;" class="calculations"></tr>
                                <!--doNotRemoveThisLine-->
<?php
						if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
							while($invRow = mysql_fetch_array($inventoryDetails)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
?>
                                <tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td style="text-align:left;"
                                    	class="itemName"
                                        data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
										<?php echo $itemName; ?>
                                    </td>
                                    <td style="text-align:center;" class="quantiity"><?php  echo $invRow['STOCK_QTY'] ?></td>
                                    <td style="text-align:center;" class="unitPrice"><?php  echo $invRow['UNIT_PRICE'] ?></td>
                                    <td style="text-align:center;" class="discount"><?php   echo $invRow['PURCHASE_DISCOUNT'] ?></td>
                                    <td style="text-align:right;"  class="subAmount"><?php  echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td style="text-align:center;" class="taxRate"><?php    echo $invRow['TAX_RATE'] ?></td>
                                    <td style="text-align:right;"  class="taxAmount"><?php  echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td style="text-align:right;"  class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                    <td style="text-align:center;"> - - - </td>
                                    <td style="text-align:center;"></td>
                                </tr>
<?php
							}
						}
?>
                                <tr class="totals">
                                    	<td style="text-align:center;background-color:#EEEEEE;">Total</td>
                                        <td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
                                        <td style="text-align:center;background-color:#EEE;">- - -</td>
                                        <td style="text-align:center;background-color:#f5f5f5;" class="discountAmount"></td>
                                        <td style="text-align:right;background-color:#f5f5f5;" class="amountSub"></td>
                                        <td style="text-align:center;background-color:#EEE;">- - -</td>
                                        <td style="text-align:right;background-color:#f5f5f5;" class="amountTax"></td>
                                        <td style="text-align:right;background-color:#f5f5f5;" class="amountTotal"></td>
                                        <td style="text-align:center;background-color:#EEE;">- - -</td>
                                        <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    </tr>
                            </tbody>
                        </table>
												<div class="clear mt-20" ></div>


<?php
						if($use_income_tax == 'Y'){
?>
								<div class="col-xs-6 pull-right">
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Income Tax Percent</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right income_tax_percent"  value="<?php echo (isset($inventory))?$inventory['INCOME_TAX']:""; ?>" />
									</div>
									<div class="clear"></div>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Income Tax Amount</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right income_tax"  value="" readonly />
									</div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
<?php
						}
?>


                        <div class="underTheTable">
                            <?php if($purchase_id == 0){ ?>
			                 			<div class="button savePurchase pull-right">Save</div>
                            <?php }else{ ?>
                            <a class="button pull-right" target="_blank" href="purchase-return-memo.php?id=<?php echo $inventory['ID']; ?>" ><i class="fa fa-print"></i> Print</a>
                            <?php } ?>
                            <div class="button pull-left" onclick="window.location.href='open-inventory-return-details.php';">New Form</div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                savePurchase();
                return false;
            }
        });
		$('.supplierSelector').selectpicker();
		$('.itemSelector').selectpicker();
		$(".billSelector").selectpicker();
        $('input.bill_number').focus();
		$(document).calculateColumnTotals();
		$("input.bill_number").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.supplierSelector button");
		$("div.supplierSelector button").keyup(function(e){
			var supp = $("select.supplierSelector option:selected").val();
			if(e.keyCode == 13 && supp != ''){
                if($("select.supplierSelector option:selected[value^='010101']").length){
                    $(".supplier_name_div").show();
                    $(".supplier_name_div input").focus();
                }else{
                    $(".supplier_name_div").hide();
                    $('div.itemSelector button').focus();
                }
			}
		});
		$("input.bill_number").setFocusTo("div.supplierSelector button");
		$("input.supplier_name").setFocusTo("div.itemSelector button");
		$('select.itemSelector').change(function(){
			$(this).getItemDetails();
			$("input.quantity").focus();
		});
		$("input.income_tax_percent").on('change keyup',function(e){
			$(this).calculateColumnTotals();
		});

		$('.itemSelector').parent('td').find(".dropdown-toggle").keyup(function(e){
			if(e.keyCode == 13 && $('.itemSelector option:selected').val() != ''){
				if($(".updateMode").length == 0){
					$(this).getItemDetails();
				}
				$("input.quantity").focus();
			}
		});
		$("input.quantity").keyup(function(e){
			stockOs();
		});
		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13){
				$("input.unitPrice").focus();
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".taxRate").focus();
			}
		});
		$(".taxRate").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".addDetailRow").focus();
			}
		});
		$("input.taxRate").change(function(){
			$(this).calculateRowTotal();
			$(".addDetailRow").focus();
		});
		$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
		$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");
		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
		});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".savePurchase").click(function(){
			savePurchase();
		});
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Bill Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Bill Updated Successfully!');
<?php
		}
?>

</script>
