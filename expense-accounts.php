<?php
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/accounts.php');

    $objAccountCodes  = new ChartOfAccounts();

    //expense heads that cannot be edited.
    $skip_expenses = array();
    $skip_expenses[] = '030104';
    $skip_expenses[] = '030105';

    if(isset($_GET['third_level'])){
        $acc  = mysql_real_escape_string($_GET['third_level']);
        $rows = $objAccountCodes->getAccountByCatAccCode($acc);
        $count = 1;
        if(mysql_num_rows($rows)){
            while($row = mysql_fetch_assoc($rows)){
        ?>
            <div class="tb_row nodisplay" data-id="<?php echo $row['ACC_CODE_ID'] ?>">
                <span class="counts col-xs-1 text-center"><?php echo $count; ?></span> <div class="col-xs-7 cont"><input type='text' value="<?php echo $row['ACC_TITLE'] ?>" title="click to edit" data-id="<?php echo $row['ACC_CODE_ID'] ?>" data-text="<?php echo $row['ACC_TITLE'] ?>" onblur="title_edit(this);" data-code="<?php echo $row['ACC_CODE'] ?>" class="input_hide" /></div> <span class="counts col-xs-3 text-center"><?php echo $row['ACC_CODE'] ?></span>
                <?php if( !in_array(substr($row['ACC_CODE'], 0,6), $skip_expenses) ){ ?>
                <span class="power_btn col-xs-1 text-center pull-right" onclick="delete_code(this);" data-id="<?php echo $row['ACC_CODE_ID'] ?>"><i class="fa fa-times"></i></span>
                <?php } ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        <?php
                $count++;
            }
        }
        exit();
    }

    if(isset($_POST['new_expense'])){
        $submain = mysql_real_escape_string($_POST['new_expense']);
        $title =mysql_real_escape_string($_POST['title']);
        if($title == ''){
            exit();
        }
        $objAccountCodes->addSubMainChild($title,$submain);
        echo mysql_insert_id();
        exit();
    }

    $expenses_types   = $objAccountCodes->getLevelThreeListByGeneral('03');
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select").selectpicker();
                $("select[name=expense_type]").change(function(){
                    var value = $("select[name=expense_type] option:selected").val();
                    $.get('expense-accounts.php',{third_level:value},function(data){
                        $("div.rows").html(data);
                        var del = 300;
                        $("div.rows .tb_row").each(function(){
                            $(this).fadeIn(del);
                            del += 300;
                        });
                    });
                });
                $("input[name=save]").click(function(){
                    var title = $("input[name=title]").val();
                    if(title == ''){
                        return;
                    }
                    $("input[name=title]").val('');
                    var acc_code = $("select[name=expense_type] option:selected").val();
                    $.post('expense-accounts.php',{new_expense:acc_code,title:title},function(data){
                        $("select[name=expense_type]").change();
                    });
                });
            });
            var duty = function(){
                $("input[name=save]").click();
                return false;
            };
            var title_edit = function(this_elm){
                var newTitle = $(this_elm).val();
                var oldTitle = $(this_elm).attr('data-text');
                var acc_code_id = $(this_elm).attr('data-id');
                var this_acc_code = $(this_elm).attr('data-code');
                if(oldTitle == newTitle){
                    return;
                }
                $.post('db/saveAccTitle.php',{acc_id:acc_code_id,new_title:newTitle,acc_code:this_acc_code},function(data){
                    if(data != ''){
                        data = $.parseJSON(data);
                        if(data['OK'] == 'Y'){
                            displayMessage(newTitle+' Saved Successfully!');
                            $(this_elm).attr('data-text',newTitle);
                        }else{
                            displayMessage('Error! Cannot Save Account Title.');
                        }
                        $(".miniText").remove();
                    }else{
                        displayMessage('Error! Cannot Save Account Title.');
                        $(".miniText").remove();
                    }
                });
            }
            var delete_code = function(this_elm){
                var clickedDel = $(this_elm);
                var idValue         = $(this_elm).attr("data-id");
                $("#fade").hide();
                $("#popUpDel").remove();
                $("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><a class='dodelete button'>Confirm</a><a class='nodelete button'>Cancel</a></div>");
                $("#popUpDel").centerThisDiv();
                $("#popUpDel").hide();
                $("#fade").fadeIn('slow');
                $("#popUpDel").fadeIn();
                $(".dodelete").click(function(){
                    $.get('db/del-account-code.php',{id:idValue},function(data){
                        if(data != ''){
                            data = $.parseJSON(data);
                            $(".dodelete").hide();
                            $(".nodelete").text('Close');
                            $("#popUpDel .confirm").html(data['MSG']);
                            if(data['OK'] == 'Y'){
                                $(".tb_row[data-id='"+idValue+"']").remove();
                            }
                        }
                    });
                });
                $(".nodelete").click(function(){
                    $("#fade").fadeOut();
                    $("#popUpDel").fadeOut(function(){
                        $("#popUpDel").remove();
                    });
                });
                $(".close_popup").click(function(){
                    $("#fade").fadeOut('fast');
                    $("#popUpDel").fadeOut(function(){
                        $("#popUpDel").remove();
                    });
                });
            }
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p>Expense Accounts  Management</p>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>
                <div id = "bodyTab1">
                    <div id="form" class="pull-left" style="width:40%">
                    <form method = "post" action="" onsubmit="return duty();">
                        <input type="hidden" name="id" value="<?php echo isset($_GET['id'])?$_GET['id']:0; ?>" />
                            <div style="width:450px; float:left">
                                <div class="caption"> Expense Types</div>
                                <div class="field">
                                    <select name="expense_type" class="form-control show-tick">
                                        <option value=""></option>
                                        <?php
                                            if(mysql_num_rows($expenses_types)){
                                                while($row = mysql_fetch_assoc($expenses_types)){
                                                  if(in_array(substr($row['ACC_CODE'], 0,6), $skip_expenses)){
                                                    continue;
                                                  }
                                                    ?>
                                                        <option value="<?php echo $row['ACC_CODE']; ?>" data-subtext="<?php echo $row['ACC_CODE']; ?>"><?php echo $row['ACC_TITLE']; ?></option>
                                                    <?php
                                                }
                                            }
                                         ?>
                                    </select>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input type="text" name="title" value="" class="form-control" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input type="button" name="save" value="Save" class="button" />
                            </div>
                            <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <div id="form" class="rows pull-left" style="width:40%">

                    </div>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>
        </div><!--bodyWrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
