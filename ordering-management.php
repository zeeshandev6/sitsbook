<?php
	include('common/connection.php');
	include 'common/config.php';
	include ('common/classes/accounts.php');
	include('common/classes/j-voucher.php');
	include ('common/classes/ordering.php');
	include ('common/classes/suppliers.php');
	include ('common/classes/items.php');
	include ('common/classes/itemCategory.php');
	include ('common/classes/rental_booking.php');

	//Permission
	if(!in_array('order-detail',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts   = new ChartOfAccounts();
	$objJournalVoucher    = new JournalVoucher();
	$objOrdering          = new Ordering();
	$objSupplier          = new Suppliers();
	$objItems 				    = new Items();
	$objItemCategory      = new itemCategory();
	$objRentalBooking 		= new RentalBooking();

	$itemsCategoryList   = $objItemCategory->getList();

	if(isset($_POST['delete_order'])){
		$order_id   = (int)(mysql_real_escape_string($_POST['delete_order']));
		$voucher_id = $objOrdering->getVoucherId($order_id);
		$voucher_id2 = $objOrdering->getStatusVoucherId($order_id);
		$objOrdering->delete($order_id);
		if($voucher_id>0){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
		}
		if($voucher_id2>0){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
		}

		exit();
	}

	if(isset($_POST['order_status_change'])){
		$status   = mysql_real_escape_string($_POST['order_status_change']);
		$order_id = (int)mysql_real_escape_string($_POST['order_id']);

		$order_detail = $objOrdering->getDetail($order_id);
		$changed 			= $objOrdering->statusChange($order_id,$status);
		$objRentalBooking->setOrderCompleteStatus($order_detail['RBD_ID'],'Y');
		if($changed){
			if($order_detail['ITEM_TYPE']=='I'){
				if($status!='R'&&$order_detail['ORDER_STATUS']=='R'){
					$item_cost = $objItems->getPurchasePrice($order_detail['ITEM_ID']);
					$it_cost   = $order_detail['QUANTITY']*$item_cost;
					$objItems->removeStockValue($order_detail['ITEM_ID'],$order_detail['QUANTITY'],$it_cost);
				}
			}
			if($order_detail['ITEM_TYPE']=='I'){
				if($status=='R'&&$order_detail['ORDER_STATUS']!='R'){
					$item_cost = $objItems->getPurchasePrice($order_detail['ITEM_ID']);
					$it_cost   = $order_detail['QUANTITY']*$item_cost;
					$objItems->addStockValue($order_detail['ITEM_ID'],$order_detail['QUANTITY'],$it_cost);
				}
			}
			if($order_id>0){
				$voucher_id 											   = $order_detail['STATUS_VOUCHER'];

				$objJournalVoucher->jVoucherNum      = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate      = date('Y-m-d');
				$objJournalVoucher->voucherType      = 'SV';
				$objJournalVoucher->reference        = 'Ordering Return';
				$objJournalVoucher->reference_date   = $objJournalVoucher->voucherDate;
				$objJournalVoucher->status	  	     = 'P';

				if($voucher_id==0){
					$voucher_id = $objJournalVoucher->saveVoucher();
					$objOrdering->insertStatusVoucherId($order_id,$voucher_id);
				}else{
					$objJournalVoucher->reverseVoucherDetails($voucher_id);
					$objJournalVoucher->updateVoucherDate($voucher_id);
				}

				if($voucher_id>0&&$status=='R'){
					$objJournalVoucher->voucherId       = $voucher_id;

					$objJournalVoucher->accountCode     = '0101060001';
					$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
					$objJournalVoucher->narration       = "Inventory received against order # ".$order_detail['ORDER_NO'];
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = $order_detail['TOTAL_PRICE'];

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $order_detail['SUPPLIER_ID'];
					$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
					$objJournalVoucher->narration       = "Amount payable against order # ".$order_detail['ORDER_NO'];
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = $order_detail['TOTAL_PRICE'];

					$objJournalVoucher->saveVoucherDetail();
				}else{
					$objJournalVoucher->reverseVoucherDetails($voucher_id);
					$objJournalVoucher->deleteJv($voucher_id);
					$objOrdering->insertStatusVoucherId($order_id,0);
				}
			}
		}
		exit();
	}


	$supplier_list 			= $objSupplier->getList();


	$total = $objConfigs->get_config('PER_PAGE');

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);
	$invoiceSize = $invoice_format[0]; // S  L
	if($invoiceSize == 'L'){
		$invoiceFile = 'rental-invoice.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile = 'rental-invoice.php'; //$invoiceFile = 'purchase-invoice-duplicates.php';
	}elseif($invoiceSize == 'S'){
		//$invoiceFile = 'purchase-bill-small.php';
	}

	if(isset($_GET['search'])){
		$objOrdering->title = (isset($_GET['title']))?$_GET['title']:"";
		$objOrdering->fromOrderDate = (isset($_GET['from-booking-date']))?$_GET['from-booking-date']:"";
		$objOrdering->fromOrderDate = (isset($_GET['to-booking-date']))?$_GET['to-booking-date']:"";
		$objOrdering->supplier_id   = (isset($_GET['supplier']))?$_GET['supplier']:"";
		$objOrdering->item_id 			= (isset($_GET['item_id']))?$_GET['item_id']:"";
	}

	if(!$admin){
		$objOrdering->user_id = $user_id;
	}

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	if(!isset($_GET['search'])){
		$objOrdering->limit_start = $start;
	}
	$objOrdering->limit_end = $total;
	$rentalList = $objOrdering->searchOrdering();
	$found_records = $objOrdering->found_records;
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	td{
		padding: 10px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<!-- jQuery Configuration -->
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("select").selectpicker();
		$("td[data-items-list]").on("mouseover",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data);
		});
		$("td[data-items-list]").on("mouseout",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data.substr(0,40));
		});
		$("select[name=odering_status]").change(function(){
			var status   = $(this).find("option:selected").val();
			var order_id = parseInt($(this).attr("data-row-id"))||0;
			$.post("ordering-management.php",{order_status_change:status,order_id:order_id},function(){});
		});
		$("a.delete_record").click(function(){
			var file = $("input.this_file_name").val();
			var idValue    = $(this).attr("do");
			var currentRow = $(this).parent().parent();
			var billNumbr  = currentRow.find('td').first().text();
			$("#xfade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete button'>Confirm</a><a class='nodelete button'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#xfade").fadeIn();
			$("#popUpDel").centerThisDiv();
			$("#popUpDel .confirm").text("Are Sure you Want To Delete ?");
			$("#popUpDel").slideDown();
			$(".dodelete").click(function(){
				$.post(file, {delete_order : idValue}, function(data){
					$(currentRow).remove();
					$(".nodelete").click();
				});
			});
			$(".nodelete").click(function(){
				$("#popUpDel").slideUp(function(){
					$(this).remove();
				});
				$("#xfade").fadeOut();
			});
		});
	});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Order List - <?php echo $found_records; ?> Records</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
						<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
						<a href="ordering-details.php"><div class="tab">New</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1" style="display:block;">
					<table width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th style="text-align:center;width:5%;">Order#</th>
								<th style="text-align:center;width:5%;">Booking#</th>
								<th style="text-align:center;width:10%;">Supplier</th>
								<th style="text-align:center;width:20%;" >Order/Delivery Time</th>
								<th style="text-align:center;width:5%;">Cost</th>
								<th style="text-align:center;width:10%;" >Ordering Status</th>
								<th style="text-align:center;width:10%;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($rentalList)){
								while($rentalRow = mysql_fetch_array($rentalList)){
									$supplier_name = $objChartOfAccounts->getAccountTitleByCode($rentalRow['SUPPLIER_ID']);
									$booking_number= $objOrdering->getBookingNumberByOrderId($rentalRow['ID']);
									?>
									<tr data-row-id="<?php echo $rentalRow['ID'] ?>" >
										<td style="text-align:center"><?php echo $rentalRow['ORDER_NO']; ?></td>
										<td style="text-align:center"><?php echo $booking_number; ?></td>
										<td><?php echo $supplier_name; ?></td>
										<td>
												Order Date: <?php if($rentalRow['ORDER_DATE'] != '0000-00-00'){ echo date('d-m-Y',strtotime($rentalRow['ORDER_DATE'])); }else{ echo ""; } ?><br/>
												Receiving Date: <?php if($rentalRow['RECEIVING_DATE'] != '0000-00-00'){  echo date('d-m-Y',strtotime($rentalRow['RECEIVING_DATE'])); }else{ echo ""; } ?><br/>
												</td>
												<td><?php echo $rentalRow['TOTAL_PRICE']; ?></td>
												<td class="text-center">
													<select class="form-control show-tick" name="odering_status" data-row-id="<?php echo $rentalRow['ID'] ?>">
														<option value="N" <?php echo $rentalRow['ORDER_STATUS']=='N'?"selected":""; ?>>New</option>
														<option value="R" <?php echo $rentalRow['ORDER_STATUS']=='R'?"selected":""; ?>>Received</option>
													</select>
												</td>
												<td style="text-align:center">
													<a id="view_button" href="ordering-details.php?id=<?php echo $rentalRow['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
												<a do="<?php echo $rentalRow['ID']; ?>" class="pointer delete_record" title="Delete"><i class="fa fa-times"></i></a>
												</td>
											</tr>
													<?php
													$start++;
												}
											}elseif(isset($_GET['search'])){
												?>
												<tr>
													<th colspan="100" style="text-align:center;">
														<?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
													</th>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
									<div class="col-xs-12 text-center">
										<?php
										//if(!isset($_GET['search']) && $found_records > $total){
										if($found_records > $total){
											?>
											<nav>
												<ul class="pagination">
													<?php
													$count = $found_records;
													$total_pages = ceil($count/$total);
													$i = 1;
													$thisFileName = $_SERVER['PHP_SELF'];
													if(isset($this_page) && $this_page>0){
														?>
														<li>
															<a href="<?php echo $thisFileName; ?>?page=1">First</a>
														</li>
														<?php
													}
													if(isset($this_page) && $this_page>=1){
														$prev = $this_page;
														?>
														<li>
															<a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
														</li>
														<?php
													}
													$this_page_act = $this_page;
													$this_page_act++;
													while($total_pages>=$i){
														$left = $this_page_act-5;
														$right = $this_page_act+5;
														if($left<=$i && $i<=$right){
															$current_page = ($i == $this_page_act)?"active":"";
															?>
															<li class="<?php echo $current_page; ?>">
																<?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
															</li>
															<?php
														}
														$i++;
													}
													$this_page++;
													if(isset($this_page) && $this_page<$total_pages){
														$next = $this_page;
														echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
													}
													if(isset($this_page) && $this_page<$total_pages){
														?>
														<li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
													</ul>
												</nav>
												<?php
											}
										}
										?>
									</div>
									<div class="clear"></div>
								</div> <!--End bodyTab1-->
								<div style="height:0px;clear:both"></div>
								<div id="bodyTab2" style="display:none;" >
									<div id="form">
										<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
											<div class="title" style="font-size:20px; margin-bottom:20px">Search Booking</div>

											<div class="caption"></div>
											<div class="message red"><?php echo (isset($message))?$message:""; ?></div>
											<div class="clear"></div>

											<div class="caption">Title</div>
											<div class="field">
												<input type="text" value="<?php echo $objOrdering->title; ?>" name="title" class="form-control" />
											</div>
											<div class="clear"></div>

											<div class="caption">From Booking Date</div>
											<div class="field">
												<input type="text" value="<?php echo ($objOrdering->fromOrderDate == '')?'':date('d-m-Y',strtotime($objOrdering->toOrderDate)); ?>" name="from-booking-date" class="form-control datepicker" />
											</div>
											<div class="clear"></div>

											<div class="caption">To Booking Date</div>
											<div class="field">
												<input type="text" value="<?php echo ($objOrdering->toOrderDate == '')?'':date('d-m-Y',strtotime($objOrdering->toOrderDate)); ?>" name="to-booking-date" class="form-control datepicker"/>
											</div>
											<div class="clear"></div>


											<div class="caption">Supplier</div>
											<div class="field">
												<select class="selectpicker form-control" name='supplier' data-style="btn-default" data-live-search="true" style="border:none">
													<option selected value=""></option>
													<?php

													if(mysql_num_rows($supplier_list)){
														while($supplierDetails = mysql_fetch_assoc($supplier_list)){
															?>
															<option value="<?php echo $supplierDetails['SUPP_ACC_CODE']; ?>"><?php echo $supplierDetails['SUPP_ACC_TITLE']; ?></option>
															<?php
														}
													}
													?>

												</select>
											</div>
											<div class="clear"></div>

											<div class="caption"> Item :</div>
											<div class="field" >
												<select class="itemSelector show-tick form-control" name="item_id"
												data-style="btn-default"
												data-live-search="true" style="border:none">
												<option selected value=""></option>
												<?php
												if(mysql_num_rows($itemsCategoryList)){
													while($ItemCat = mysql_fetch_array($itemsCategoryList)){
														$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
														?>
														<optgroup label="<?php echo $ItemCat['NAME']; ?>">
															<?php
															if(mysql_num_rows($itemList)){
																while($theItem = mysql_fetch_array($itemList)){
																	if($theItem['ACTIVE'] == 'N'){
																		continue;
																	}
																	if($theItem['INV_TYPE'] == 'B'){
																		continue;
																	}
																	?>
																	<option data-type="I" value="<?php echo $theItem['ID']; ?>" ><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
																	<?php
																}
															}
															?>
														</optgroup>
														<?php
													}
												}
												?>
											</select>
										</div>
										<div class="clear"></div>

										<div class="caption"></div>
										<div class="field">
											<input type="submit" value="Search" name="search" class="button"/>
										</div>
										<div class="clear"></div>
									</form>
								</div><!--form-->
							</div> <!-- End bodyTab2 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->
				</div><!--body-wrapper-->
				<div id="xfade"></div>
			</body>
			</html>
			<?php include('conn.close.php'); ?>
			<script>
			$(document).ready(function() {
				$(".shownCode,.loader").hide();
				$("input.supplierTitle").keyup(function(){
					$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
				});
				$(window).keydown(function(e){
					if(e.keyCode==113){
						e.preventDefault();
						window.location.href = "<?php echo "inventory-details.php"; ?>";
					}
				});
			});
			$(function(){
				$("#fromDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
				$("#toDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>tab('2', '1', '2');<?php } ?>

			</script>
