<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_outward.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/measure.php');
	include('common/classes/emb_products.php');
	include('common/classes/machines.php');
	include('common/classes/payrolls.php');
	include('common/classes/design.php');
	include('common/classes/emb_stitch_account.php');
	include('common/classes/customers.php');

	//Permission
	if(!in_array('emb-outward',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts     = new ChartOfAccounts();
	$objOutwards 						= new outward();
	$objLotRegister 				= new EmbLotRegister();
	$objMeasures  					= new Measures();
	$objMachines 						= new machines();
	$objPayrolls   					= new Payrolls();
	$objDesign   						= new design();
	$objEmbStitchAccount   	= new EmbStitchAccount();
	$objEmbProducts 				= new EmbProducts();
	$objCustomers  					= new customers();

	$customersList 		= $objCustomers->getList();
	$productList   		= $objEmbProducts->getList();
	$measureList   		= $objMeasures->getList();
	$machineList   		= $objMachines->getList();

	$third_parties_suppliers = $objChartOfAccounts->getAccountByCatAccCode('040101');

	if(isset($_POST['addOutward'])){
		$objOutwards->outwd_date       	= date('Y-m-d',strtotime($_POST['outwardDate']));
		$objOutwards->billNum          	= mysql_real_escape_string($_POST['billNum']);
		$objOutwards->cloth          		= mysql_real_escape_string($_POST['cloth']);
		$objOutwards->yarn          		= mysql_real_escape_string($_POST['yarn']);
		$objOutwards->transport         = mysql_real_escape_string($_POST['transport']);
		$objOutwards->customerAccCode  	= mysql_real_escape_string($_POST['custAccCode']);
		$objOutwards->customerAccTitle 	= mysql_real_escape_string($objOutwards->getAccountTitle($_POST['custAccCode']));

		$objOutwards->save();

		$outwdId = mysql_insert_id();

		echo "<script>window.location.href =\"".basename($_SERVER['PHP_SELF'])."?wid=".$outwdId."\";</script>";
		exit();
	}
	if(isset($_POST['updateOutward'])){
		$objOutwards->outwd_date       = date('Y-m-d',strtotime($_POST['outwardDate']));
		$objOutwards->billNum          = mysql_real_escape_string($_POST['billNum']);
		$objOutwards->cloth          	 = mysql_real_escape_string($_POST['cloth']);
		$objOutwards->yarn          	 = mysql_real_escape_string($_POST['yarn']);
		$objOutwards->transport         = mysql_real_escape_string($_POST['transport']);
		$objOutwards->customerAccCode  = mysql_real_escape_string($_POST['custAccCode']);
		$objOutwards->customerAccTitle = $objOutwards->getAccountTitle($_POST['custAccCode']);

		$outwdId = (int)$_POST['outwd_id'];
		$objOutwards->update($outwdId);

		echo "<script>window.location.href =\"".basename($_SERVER['PHP_SELF'])."?wid=".$outwdId."&v\";</script>";
		exit();
	}

	$isNotEditable = 0;
	$outward 	   = NULL;

	if(isset($_POST['updateOutwardDetails'])){
		$outwardId 											= (int)mysql_real_escape_string($_POST['outwardId']);

		$objOutwards->product_id 				= mysql_real_escape_string($_POST['productCode']);
		$objOutwards->quality 					= mysql_real_escape_string($_POST['quality']);
		$objOutwards->measure 					= mysql_real_escape_string($_POST['measure']);
		$objOutwards->billing_type 			= mysql_real_escape_string($_POST['billing_type2']);
		$objOutwards->stitches 					= mysql_real_escape_string($_POST['stitches2']);
		$objOutwards->total_laces 			= mysql_real_escape_string($_POST['total_laces']);
		$objOutwards->length 						= mysql_real_escape_string($_POST['length']);
		$objOutwards->embRate 					= mysql_real_escape_string($_POST['embRate']);
		$objOutwards->embAmount 				= mysql_real_escape_string($_POST['embAmount']);
		$objOutwards->stitchRate 				= mysql_real_escape_string($_POST['stitchRate']);
		$objOutwards->stitchAmount 			= mysql_real_escape_string($_POST['stitchAmount']);
		$objOutwards->stitchAccount 		= mysql_real_escape_string($_POST['stitchAccount']);
		$objOutwards->designNum 				= mysql_real_escape_string($_POST['designNum']);
		$objOutwards->machineNum 				= mysql_real_escape_string($_POST['machineNum']);
		$objOutwards->remarks 					= mysql_real_escape_string($_POST['remarks']);

		$objOutwards->updateOutwardDetail($_POST['outwardDetailId']);

		$prefix_url 										= (isset($_GET['w']))?"w&v&":"";
		echo "<script>window.location.href ='".basename($_SERVER['PHP_SELF'])."?".$prefix_url."wid=".$outwardId."';</script>";
		exit();
	}
	if(isset($_GET['wid'])){
		$outwardResult 		= $objOutwards->getSpecificCustomerOutward($_GET['wid']);
		if(mysql_num_rows($outwardResult)){
			$outward 		= mysql_fetch_array($outwardResult);
		}else{
			$outward 		= NULL;
		}
		$outward_get_id     = (int)mysql_real_escape_string($_GET['wid']);
		$outwardDetails 		= $objOutwards->getOutwardDetails($_GET['wid']);
		$invoiceDesignCodes = $objOutwards->getDesignsList($outward_get_id);
		$isNotEditable 			= (int)$outward['VOUCHER_ID'];
	}else{
		$newBillNum = $objOutwards->genLastBillNum();
	}
	if($outward == NULL && isset($_GET['wid'])){
		echo "<script>window.location.href =\"".basename($_SERVER['PHP_SELF'])."\";</script>";
		exit();
	}

	$billing_types = array();
	$billing_types['S'] = 'Stitches';
	$billing_types['Y'] = 'Yards';
	$billing_types['U'] = 'Suites';
	$billing_types['L'] = 'Laces';
	$billing_types['']  = 'Unknown';
?>
<!DOCTYPE html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     							/>
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     							/>
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     							/>
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       							/>
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     							/>
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     							/>
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     							/>
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  					/>
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  								/>
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  							/>
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  									/>
	<link rel="stylesheet" href="resource/css/lightbox.css" type="text/css"  											/>
	<style media="screen">
		table th,table td{
			font-size: 14px !important;
		}
		table td{
			background-color: rgba(0,0,0,0) !important;
		}
		tr.bg-warning{
			background-color: #fcf8e3 !important;
		}
		tr.bg-warning td{
			background-color: rgba(0,0,0,0) !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/emb.outward.config.js"></script>
	<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
	<script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript">
		$(function(){
			$('select').selectpicker();
			$("table").on('click','a.quick_edit',function(e){
				$(this).quickEditRow();
			});
		});
	</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
					<?php include("common/left_menu.php") ?>
		    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Cloth Outward Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
											<a href="emb-outward.php?tab=list"><div class="tab">List</div></a>
											<a href="emb-outward.php?tab=search"><div class="tab">Search</div></a>
											<div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">
                            <div class="caption">Outward Date</div>
                            <div class="field" style="width:150px">
                                <input type="text" name="outwardDate" class="datepicker form-control outwards_date" style="width:145px" value="<?php echo (isset($outward))?date('d-m-Y',strtotime($outward['OUTWD_DATE'])):date('d-m-Y'); ?>" />
                            </div>

                            <div class="caption" style="width:140px">Outward No</div>
                            <div class="field" style="width:150px">
                                <input type="text" name="billNum" class="form-control" value="<?php echo (isset($outward))?$outward['BILL_NO']:"";echo (isset($newBillNum))?$newBillNum:""; ?>" />
                            </div>
                            <div class="clear"></div>

														<div class="caption" style="width:150px">Customer Title</div>
                            <div class="field" style="width:470px;position:relative;">
                                <select class="custCodeSelector show-tick form-control" name="custAccCode" data-live-search="true"  onchange="getCompletedLots();" >
                                   <option selected value="">Select</option>
    <?php
                            if(mysql_num_rows($customersList)){
                                while($account = mysql_fetch_array($customersList)){
                                    $selected = (isset($outward)&&$outward['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";

?>
                                   <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                            </div>

														<div class="col-md-5 col-sm-8" style="padding: 0px !important;">
															<div class="caption" style="width:150px">Third Party :</div>
	                            <div class="field" style="width:240px;position:relative;">
	                                <select class="third_party_code show-tick form-control" name="third_party_code" data-live-search="true">
	                                   <option selected value=""></option>
<?php
	                            if(mysql_num_rows($third_parties_suppliers)){
	                                while($account = mysql_fetch_array($third_parties_suppliers)){
	                                    $selected = (isset($outward)&&$outward['THIRD_PARTY_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
?>
	                                   <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
<?php
	                                }
	                            }
?>
	                                </select>
	                            </div>
														</div>
                        		<div class="clear"></div>

														<div class="caption" style="width:150px">Cloth</div>
                            <div class="field" style="width:470px">
                                <input type="text" name="cloth" class="form-control" value="<?php echo (isset($outward))?$outward['CLOTH']:""; ?>" />
                            </div>
                            <div class="clear"></div>

														<div class="caption" style="width:150px">Yarn</div>
                            <div class="field" style="width:470px">
                                <input type="text" name="yarn" class="form-control" value="<?php echo (isset($outward))?$outward['YARN']:""; ?>" />
                            </div>
                            <div class="clear"></div>

														<div class="caption" style="width:150px">Transport</div>
                            <div class="field" style="width:470px">
                                <input type="text" name="transport" class="form-control" value="<?php echo (isset($outward))?$outward['TRANSPORT']:""; ?>" />
                            </div>
                            <div class="clear"></div>
                    	</form>
											<div class="clear"></div>
											<hr />
											<div class="clear"></div>
											<div class="col-xs-12" style="overflow-x: auto;min-height: 320px !important;height: auto !important;">
											<table class="prom outwardDetailsTable">
													<thead>
															<tr>
																 <th width="8%" style="font-size:11px; text-align:center">Product</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Lot#</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Quality</th>
																 <th width="5%"  style="font-size:11px;text-align:center;">Measure</th>
																 <th width="5%"  style="font-size:11px;text-align:center;">Billing Type</th>
																 <th width="5%"  style="font-size:11px;text-align:center;">Stitches</th>
																 <th width="5%"  style="font-size:11px;text-align:center;">Production</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Thaan</th>
																 <th width="5%"  style="font-size:11px; text-align:center">@Emb</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Rate/1000 Stitches</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
																 <th width="5%" style="font-size:11px; text-align:center;">Commander</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Design#</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Machine</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Action</th>
																 <th width="2%"  style="font-size:11px; text-align:center">
																		<input type="checkbox" onclick="checkLots(this)" class="css-checkbox" id="lot-check-all"  />
																		<label for="lot-check-all" class="css-label"></label>
																 </th>
															</tr>
															<tr class="quickSubmit">
																	<td>
																			<input type="hidden"  value="" class="insert_prod_title" style="width:95%;min-height:20px;text-align:center;" />
																			<select class="product_id show-tick form-control" data-live-search="true" style="border:none">
																				 <option value="">Select</option>
<?php
																	if(mysql_num_rows($productList)){
																			while($account = mysql_fetch_array($productList)){
?>
																				 <option value="<?php echo $account['ID']; ?>" data-subtext="<?php echo $account['PROD_CODE']; ?>" ><?php echo $account['TITLE']; ?></option>
<?php
																			}
																	}
?>
																			</select>
																	</td>
																 <td>
																		<input type="text" value="" class="lotNum form-control" />
																	</td>
																	<td>
																		<select class="qualityDropDown form-control show-tick">
																					<option value="">select</option>
																			</select>
																	</td>
																	<td>
																		<select name="measure" class="measure form-control show-tick" >
<?php
							if(mysql_num_rows($measureList)){
								while($row = mysql_fetch_array($measureList)){
?>
									<option value="<?php  echo $row['ID']; ?>"><?php  echo $row['NAME']; ?></option>
<?php
								}
							}
?>
								</select>
																	</td>
																	<td>
																		<select name="billing_type" class="billing_type form-control show-tick" >
																			<option value="S">Stitches</option>
																			<option value="Y">Yards</option>
																			<option value="U">Suits</option>
																			<option value="L">Laces</option>
																		</select>
																	</td>
																	<td>
																				<input type="text"  value="" class="stitches form-control" />
																	</td>
																	<td>
																				<input type="text"  value="" class="total_laces form-control" />
																	</td>
																	<td>
																				<input type="text"  value="" class="length form-control" />
																	</td>
																	<td>
																		<input type="text" class="embRate form-control" />
																	</td>
																	<td>
																		<input type="text"  value="" class="form-control embAmount" readonly="readonly" />
																	</td>
																	<td>
																		<input type="text" class="stitchRate form-control	" />
																	</td>
																	<td>
																		<input type="text"  value="" class="form-control stitchAmount" readonly="readonly" />
																	</td>
																	<td>
																		<select name="stitchAccount" class="form-control show-tick stitchAccount">
																				<option value="">Select</option>
<?php
							$stitchList = $objPayrolls->getList();
							if(mysql_num_rows($stitchList)){
								while($stitch = mysql_fetch_array($stitchList)){
?>
									<option value="<?php echo $stitch['CUST_ACC_CODE']; ?>"><?php echo $stitch['CUST_ACC_TITLE']; ?></option>
<?php
								}
							}
?>
																			</select>
																	</td>
																	<td>
																		<input type="text" class="designNum form-control" />
																	</td>
																	<td>
																			<select class="machine_id form-control" >
																				<option value="">Select</option>
<?php
							if(mysql_num_rows($machineList)){
								while($machine_row = mysql_fetch_array($machineList)){
?>
																				<option value="<?php echo $machine_row['ID']; ?>"><?php echo $machine_row['MACHINE_NO']."-".$machine_row['NAME']; ?></option>
<?php
								}
							}
?>
								</select>
							</td>
																	<td>
																		<input type="text"  value=""  class="form-control thaanLengthOS" inwardOS="0" readonly="readonly" />
																	</td>
																	<td>
																		<input type="button" class="quick_insert btn btn-default btn-block" value="Enter" />
																	</td>
															</tr>
													</thead>
<?php
if(isset($outward)){
			$details_num_rows = mysql_num_rows($outwardDetails);
			$lengthTotal 				= 0;
			$embRateTotal 			= 0;
			$stitchAmountTotal 	= 0;
			$stitchesTotal 			= 0;
			$lacesTotal 				= 0;
			if($details_num_rows){
?>
					<tbody>
						<tr class="calculations" style="display:none;"></tr>
<?php
											while($row = mysql_fetch_array($outwardDetails)){
													$measurement  = $objMeasures->getName($row['MEASURE_ID']);
													$stitchMan    = $objChartOfAccounts->getAccountTitleByCode($row['STITCH_ACC']);
													$product_name = $objEmbProducts->getTitle($row['PRODUCT_ID']);
?>
															<tr class="alt-row calculations bill-rows" data-lot-id="<?php echo $row['OUTWD_DETL_ID']; ?>">
																	<td class="text-center product_id" data-product-id="<?php echo $row['PRODUCT_ID']; ?>"><?php echo $product_name; ?></td>
																	<td class="text-center lot_no"><?php echo $row['LOT_NO']; ?></td>
																	<td class="text-center quality"><?php echo $row['QUALITY']; ?></td>
																	<td class="text-center measure_id" data-measure-id="<?php echo $row['MEASURE_ID']; ?>"><?php echo $measurement; ?></td>
																	<td class="text-center billing_type" data-billing-type="<?php echo $row['BILLING_TYPE']; ?>"><?php echo $billing_types[$row['BILLING_TYPE']]; ?></td>
																	<td class="text-center stitches"><?php echo $row['STITCHES']; ?></td>
																	<td class="text-center total_laces"><?php echo $row['TOTAL_LACES']; ?></td>
																	<td class="text-center measure_length lengthColumn"><?php echo $row['MEASURE_LENGTH']; ?></td>
																	<td class="text-center emb_rate"><?php echo $row['EMB_RATE']; ?></td>
																	<td class="text-center emb_amount embAmountColumn"><?php echo $row['EMB_AMOUNT']; ?></td>
																	<td class="text-center stitch_rate"><?php echo $row['STITCH_RATE']; ?></td>
																	<td class="text-center stitch_amount stichAmountColumn"><?php echo $row['STITCH_AMOUNT']; ?></td>
																	<td class="text-center stitch_acc" data-stitch-id="<?php echo $row['STITCH_ACC']; ?>"><?php echo $stitchMan; ?></td>
																	<td class="text-center design_code"><?php echo $row['DESIGN_CODE']; ?></td>
																	<td class="text-center machine_id"><?php echo $row['MACHINE_ID']; ?></td>
																	<td>
																		<a class="quick_edit" id='view_button' do="<?php echo $row['OUTWD_DETL_ID']; ?>" title="Modify"><i class="fa fa-pencil"></i></a>
																	</td>
																	<td style="text-align:center">
																			<input type="checkbox" class="lotCheckBox css-checkbox" id="lot-checkv-<?php echo $row['OUTWD_DETL_ID']; ?>" value="<?php echo $row['OUTWD_DETL_ID']; ?>" checked />
																			<label for="lot-checkv-<?php echo $row['OUTWD_DETL_ID']; ?>" class="css-label"></label>
																	</td>
															</tr>
<?php
															$lengthTotal 			 += $row['MEASURE_LENGTH'];
															$embRateTotal 		 += $row['EMB_AMOUNT'];
															$stitchAmountTotal += $row['STITCH_AMOUNT'];
															$stitchesTotal 		 += $row['STITCHES'];
															$lacesTotal 			 += $row['TOTAL_LACES'];
											}
			}
?>
<?php
											$completedLots = $objLotRegister->getListByCustomerIncParials($outward['CUST_ACC_CODE']);
											while($completed_rows = mysql_fetch_array($completedLots)){
												$measurement 	= $objMeasures->getName($completed_rows['MEASURE_ID']);
												$stitchMan 		= $objPayrolls->getTitle($completed_rows['STITCH_ACC']);
												$product_name 	= $objEmbProducts->getTitle($completed_rows['PRODUCT_ID']);

												$partial_row = $objOutwards->getLengthIfPartial($completed_rows['ID']);
												if($partial_row['PARTIAL_LENGTH'] != 0){
													if($partial_row['PARTIAL_LENGTH'] == $completed_rows['MEASURE_LENGTH']){
														continue;
													}
													$completed_rows['STITCHES'] 			-= $partial_row['TOTAL_STITCHES'];
													$completed_rows['MEASURE_LENGTH'] -= $partial_row['PARTIAL_LENGTH'];
													$completed_rows['TOTAL_LACES']    -= $partial_row['TOTAL_PRODUCTION'];

													if($completed_rows['BILLING_TYPE'] == 'S' || $completed_rows['BILLING_TYPE'] == 'L'){
														$completed_rows['STITCH_AMOUNT']   = ( ($completed_rows['STITCHES']*15)/1000 ) * $completed_rows['STITCH_RATE'] * $completed_rows['MEASURE_LENGTH'];
													}
													if($completed_rows['BILLING_TYPE'] == 'L'){
														$completed_rows['EMB_AMOUNT']   = $completed_rows['EMB_RATE'] * $completed_rows['TOTAL_LACES'];
													}
													if($completed_rows['BILLING_TYPE'] == 'Y'){
														$completed_rows['EMB_AMOUNT']   = $completed_rows['EMB_RATE'] * $completed_rows['MEASURE_LENGTH'] * 15;
													}
													if($completed_rows['BILLING_TYPE'] == 'U'){
														$completed_rows['EMB_AMOUNT']   = $completed_rows['EMB_RATE'] * $completed_rows['TOTAL_LACES'];
													}
												}
?>
												<tr class="alt-row calculations lot-register-rows" data-lot-id="<?php echo $completed_rows['ID']; ?>">
														<td class="text-center product_id" data-product-id="<?php echo $completed_rows['PRODUCT_ID']; ?>"><?php echo $product_name; ?></td>
														<td class="text-center lot_no"><?php echo $completed_rows['LOT_NO']; ?></td>
														<td class="text-center quality"><?php echo $completed_rows['QUALITY']; ?></td>
														<td class="text-center measure_id" data-measure-id="<?php echo $completed_rows['MEASURE_ID']; ?>"><?php echo $measurement; ?></td>
														<td class="text-center billing_type" data-billing-type="<?php echo $completed_rows['BILLING_TYPE']; ?>"><?php echo $billing_types[$completed_rows['BILLING_TYPE']]; ?></td>
														<td class="text-center stitches"><?php echo $completed_rows['STITCHES']; ?></td>
														<td class="text-center total_laces"><?php echo $completed_rows['TOTAL_LACES']; ?></td>
														<td class="text-center measure_length lengthColumn"><?php echo $completed_rows['MEASURE_LENGTH']; ?></td>
														<td class="text-center emb_rate"><?php echo $completed_rows['EMB_RATE']; ?></td>
														<td class="text-center emb_amount embAmountColumn"><?php echo $completed_rows['EMB_AMOUNT']; ?></td>
														<td class="text-center stitch_rate"><?php echo $completed_rows['STITCH_RATE']; ?></td>
														<td class="text-center stitch_amount stichAmountColumn"><?php echo $completed_rows['STITCH_AMOUNT']; ?></td>
														<td class="text-center stitch_acc" data-stitch-id="<?php echo $completed_rows['STITCH_ACC']; ?>"><?php echo $stitchMan; ?></td>
														<td class="text-center design_code"><?php echo $completed_rows['DESIGN_CODE']; ?></td>
														<td class="text-center machine_id"><?php echo $completed_rows['MACHINE_ID']; ?></td>
														<td>
															<a class="quick_edit" id='view_button' do="<?php echo $completed_rows['ID']; ?>" title="Modify"><i class="fa fa-pencil"></i></a>
														</td>
														<td style="text-align:center">
																<input type="checkbox" class="lotCheckBox css-checkbox" id="lot-checkv-<?php echo $completed_rows['ID']; ?>" value="<?php echo $completed_rows['ID']; ?>" />
																<label for="lot-checkv-<?php echo $completed_rows['ID']; ?>" class="css-label"></label>
														</td>
												</tr>
<?php
											}
?>
															<tr class="totals">
																	<td style="text-align:center;background-color:#eee;" colspan="5" >Total</td>
																	<td style="text-align:center;background-color:#eee;" class="sumStitchesTotal"><?php echo $stitchesTotal; ?></td>
																	<td style="text-align:center;background-color:#eee;" class="sumLacesTotal"><?php echo $lacesTotal; ?></td>
																	<td style="text-align:center;background-color:#eee;" class="sumLenTotal"><?php echo $lengthTotal; ?></td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#eee;" class="embAmountTotal"><?php echo $embRateTotal; ?></td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#eee;" class="stichAmountTotal"><?php echo $stitchAmountTotal; ?></td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
																	<td style="text-align:center;background-color:#f5f5f5;">- - -</td>
															</tr>
													</tbody>
<?php
}
?>
											</table>
											<div class="clear" style="margin: 20px ;"></div>
											</div>
											<div class="clear"></div>

											<div class="underTheTable">
												<input type="hidden" name="outwd_id" value="<?php echo isset($outward)?$outward['OUTWD_ID']:0; ?>" />
												<?php if(in_array('emb-outward-modify',$permissionz) || $admin == true || $outward == NULL){ ?>
												<div class="btn btn-primary pull-right createVoucher ml-10"><?php echo ($outward==NULL)?"Save":"Update"; ?></div>
												<?php } ?>
												<?php if($outward != NULL){ ?>
												<div class="btn btn-warning pull-right mr-10 toggleLotRows">Show Lots</div>
												<?php } ?>
												<div class="btn btn-default pull-left" onclick="window.location.href = 'emb-outward-details.php';">New Form</div>
											</div>
											<div class="clear"></div>

											<?php
														$design_image_dir = 'uploads/designs/';
														if(isset($invoiceDesignCodes) && mysql_num_rows($invoiceDesignCodes)){
											?>
											            <div class="content-box-content" style="padding:0;">
											            	<div id="bodyTab1">
											            		<div id="form">
											            			<div class="alert alert-success">Designs</div>
											            			<div class="row">
											            				<div class="col-md-12">
											<?php
															while($design_code = mysql_fetch_array($invoiceDesignCodes)){
																$image_name = $objDesign->getImageByCode($design_code['DESIGN_CODE']);
																$image_link = $design_image_dir.$image_name;
											?>
											            					<div class="col-md-3">
											            						<div class="panel panel-info">
												            						<div class="panel-heading"><?php echo $design_code['DESIGN_CODE']; ?></div>
											<?php
																					if($image_name != ''){
											?>
													            						<a href="<?php echo $image_link; ?>" data-lightbox="SameName">
													            							<img class="img-thumbnail" src="<?php echo $image_link; ?>" />
													            						</a>
											<?php
																					}
											?>
												            					</div>
											            					</div>
											<?php
															}
											?>
											            				</div>
											            			</div>
											            		</div>
											            	</div><!--content-box-content--->
											            </div><!--content-box-content--->
											<?php
														}
											?>
                    </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box-top-->
    </div><!--body-wrapper-->

    <div id="fade"></div>
    <div id="xfade"></div>

    <div id="popUpForm3" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
      	<p>New Machine:</p>
        <p class="textBoxGo">
        	<input type="text" value="" name="newMachineTitle" class="form-control newMachineTitle" />
					<i class="fa fa-arrow-right"></i>
        </p>
      </div>
    </div>

    <div id="popUpForm2" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
            	<p>New Stitch Account:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="newStitchTitle" class="form-control newStitchTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(".datepicker").focus();
		$(".datepicker").setFocusTo("div.custCodeSelector button");
		$(".innerTdRed").find('td').css('color','#C30');

		$("select.product_id").change(function(){
			if($("tr.updateMode").length){
				$("input.stitches").focus();
			}else{
				$("input.lotNum").focus();
			}
		});
		$("div.product_id button").keydown(function(e){
			if(e.keyCode==13){
				$("input.stitches").focus();
			}
		});
		$(".lotNum").on('keydown',function(e){
			if(e.keyCode==13){
				$(this).getProductQualities();
			}
		});
		$(".lotNum").on('change',function(e){
			$(this).getProductQualities();
		});
		$("div.measure button").keydown(function(e){
			if(e.keyCode==13){
				$("div.billing_type button").focus();
			}
		});
		$("div.billing_type button").keydown(function(e){
			if(e.keyCode==13){
				$("input.stitches").focus();
			}
		});
		$("input.stitches").keydown(function(e){
			if(e.keyCode==13){
				$("input.total_laces").focus();
			}
		});
		$("input.total_laces").keydown(function(e){
			if(e.keyCode==13){
				$("input.length").focus();
			}
		});

		$("input.length").keydown(function(e){
			if(e.keyCode==13){
				$("input.embRate").focus();
			}
			var claimOrIssue = 'P';
			if($(".claimCheck").is(":checked")){
				claimOrIssue = 'R';
			}
			if(claimOrIssue == 'P' || (claimOrIssue == 'R' && $(".length").val() < 0)){
				//$(".thaanLengthOS").val($(".thaanLengthOS").attr("inwardOS")-$(".length").val());
			}
			if($(".thaanLengthOS").val()<0){
				//$(".length").val('');
				//$(".thaanLengthOS").val($(".thaanLengthOS").attr("inwardOS")-$(".length").val());
			}
		});
		$(".embRate").keydown(function(e){
			if(e.keyCode==13){
				$(".stitchRate").focus();
			}
		});
		$(".stitchRate").keydown(function(e){
			if(e.keyCode==13){
				$("div.stitchAccount button").focus();
			}
		});
		$("div.stitchAccount button").keydown(function(e){
			if(e.keyCode==13){
				$("input.designNum").focus();
			}
		});
		$("input.designNum").keydown(function(e){
			if(e.keyCode==13){
				$("div.machine_id button").focus();
			}
		});
		$("div.machine_id button").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				$(".quick_insert").focus();
			}
		});
		$("a.pointer").click(function(){
			$(this).deleteRow();
		});
		$(".editDetailsFrom").click(function(){
			$(this).editOutwardDetails();
		});
		$(".quick_insert").click(function(){
			$(this).quickSubmit();
		});
		$(".quick_insert").keydown(function(e){
			if(e.keyCode==13){
				$(this).quickSubmit();
			}
		});
		var selectedFlag = false;
		$(".qualityDropDown").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				if(selectedFlag==false){
					selectedFlag = true;
				}else{
					$("div.measure button").focus();
					selectedFlag = false;
				}
			}else{
				selectedFlag = false;
			}
		});
		$(".qualityDropDown").change(function(e){
			$(this).getProductDetail();
		});
		$("datepicker").setFocusTo(".getCustomerTitle");
		$(".getCustomerTitle").keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()=="")
				{
					e.preventDefault();
				}
			}
		});
		$("input.insert_title").keydown(function(e){
				if(e.keyCode==16){
					$("div.listEye").accountList();
				}
			});
			$("input.insert_title").keyup(function(e){
				if(e.keyCode==16){
					$("div#fade").fadeOut('fast');
					$("div.popupList").fadeOut('fast');
				}
			});
			$("div.listEye").click(function(){
				$("div.listEye").accountList();
			});
		$(".productSelector").change(function(){
			$(".get_code_for_prod").val($(this).val());
			$(".lotNum").focus();
		});

		$("div.createVoucher").click(function(){
			$(this).journalizeThis();
		});
		$(".lot-register-rows").each(function(){
			if(!$(this).find("input[type='checkbox']").is(":checked")){
				$(this).hide();
			}
		});
		$("select.billing_type,input.total_laces,input.stitchRate,input.stitches,input.embRate").change(function(){
			embroideryFormulas();
		});
		$(".toggleLotRows").click(function(){
			$(".lot-register-rows").toggle();
		});
<?php
			if(isset($_GET['v'])){
?>
				$(this).journalizeThis();
<?php
			}
?>
    });
</script>
