<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/mobile-purchase.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/suppliers.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');

	//Permission
	if(!in_array('mobile-purchase',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	= new ChartOfAccounts;
	$objSuppliers			= new suppliers;
	$objScanPurchase   	 	= new ScanPurchase;
	$objScanPurchaseDetails = new ScanPurchaseDetails;
	$objItems            	= new Items;
	$objItemCategory        = new itemCategory;
	$objDepartments         = new Departments;
    $objPagination          = new Pagination;

    $total = $objPagination->getPerPage();
    $start = 0;
    $suppliersList = $objSuppliers->getlist();
    $cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');
    if(isset($_GET['search'])){
        $objScanPurchase->fromDate = ($_GET['from_date'] == '')?"":date('Y-m-d',strtotime($_GET['from_date']));
        $objScanPurchase->toDate = ($_GET['to_date'] == '')?"":date('Y-m-d',strtotime($_GET['to_date']));
		$objScanPurchase->supp_acc_code = mysql_real_escape_string($_GET['supplierAccCode']);
		$objScanPurchase->bill_no	    = mysql_real_escape_string($_GET['billNum']);
        $objScanPurchase->supplier_name = mysql_real_escape_string($_GET['supplier_info']);
        $objScanPurchase->imei          = mysql_real_escape_string($_GET['imei']);
    }
    if(isset($_GET['page'])){
        $this_page = $_GET['page'];
        if($this_page>=1){
            $this_page--;
            $start = $this_page * $total;
        }
    }else{
        $start = 0;
        $this_page = 0;
    }
    $list = $objScanPurchase->search($start, $total);
    $found_records = $objScanPurchase->num_record;
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Scan Purchase</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table td{
            padding: 7px !important;
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/scan.purchase.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$('select[name=supplierAccCode]').selectpicker();
    		$("input[name='billNum']").numericOnly();
			$("#datepicker").setFocusTo("input[name='billNum']");
			$("input[name='billNum']").setFocusTo("div.supplierSelector button");
			$("input[name='gpNum']").focus();
			makeItCash(".trasactionType");
			$('[data-toggle="tooltip"]').tooltip();
			$(".pointer").click(function(){
				$(this).deleteRowDefault('db/deleteScanPurchase.php');
			});
            <?php if(isset($_GET['tab']) && $_GET['tab'] == 'search'){ ?>
                tab('2', '1', '2');
            <?php } ?>
    	});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>    
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Scanning - <?php echo $found_records; ?> Records </p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="mobile-purchase-details.php"><div class="tab">New</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1" style="display:block;">
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th style="text-align:center;width:10%;">Bill#</th>
                               <th style="text-align:center;width:10%;">PurchaseDate</th>
                               <th style="text-align:left;width:40%;">Account Title</th>
                               <th style="text-align:center;width:15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
            if(mysql_num_rows($list)){
                while($purchaseRow = mysql_fetch_array($list)){
                    $supplierTitle =     $objAccountCodes->getAccountTitleByCode($purchaseRow['SUPP_ACC_CODE']);
?>
                            <tr>
                                <td style="text-align:center"><?php echo $purchaseRow['BILL_NO']; ?></td>
                                <td style="text-align:center"><?php echo date('d-m-Y',strtotime($purchaseRow['SCAN_DATE'])); ?></td>
                                <td style="text-align:left;"><?php echo $supplierTitle; ?></td>
                                <td style="text-align:center">
                                        <a id="view_button" href="mobile-purchase-details.php?id=<?php echo $purchaseRow['ID']; ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a id="view_button" target="_blank" href="mobile-purchase-invoice.php?id=<?php echo $purchaseRow['ID']; ?>" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a>
                                        <a id="view_button" target="_blank" href="voucher-print.php?id=<?php echo $purchaseRow['VOUCHER_ID']; ?>" title="Print"><i class="fa fa-print"></i> JV</a>
                                        <?php if(in_array('delete-mobile-purchase',$permissionz) || $admin){ ?>
                                        <a do="<?php echo $purchaseRow['ID']; ?>" class="pointer" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a>
                                        <?php } ?>
                                        <a class="button-orange" href="mobile-purchase-return-details.php?id=<?php echo $purchaseRow['ID']; ?>" data-toggle="tooltip" title="Return" ><i class="fa fa-reply"></i></a>
                                </td>
                            </tr>
<?php
                }
            }elseif(isset($_GET['search'])){
?>
                            <tr>
                                <th colspan="100" style="text-align:center;">
                                    <?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
                                </th>
                            </tr>
<?php
            }
?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
<?php
                    if(!isset($_GET['search'])){
?>
                            <nav>
                              <ul class="pagination">
<?php
                        $count = $found_records;
                        $total_pages = ceil($count/$total);
                        $i = 1;
                        $thisFileName = $_SERVER['PHP_SELF'];
                        if(isset($this_page) && $this_page>0){
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=1">First</a>
                            </li>
<?php
                        }
                        if(isset($this_page) && $this_page>=1){
                            $prev = $this_page;
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
                            </li>
<?php
                        }
                        $this_page_act = $this_page;
                        $this_page_act++;
                        while($total_pages>=$i){
                            $left = $this_page_act-5;
                            $right = $this_page_act+5;
                            if($left<=$i && $i<=$right){
                            $current_page = ($i == $this_page_act)?"active":"";
?>
                                <li class="<?php echo $current_page; ?>">
                                    <?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                </li>
<?php
                            }
                            $i++;
                        }
                        $this_page++;
                        if(isset($this_page) && $this_page<$total_pages){
                            $next = $this_page;
                            echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
                        }
                        if(isset($this_page) && $this_page<$total_pages){
?>
                                <li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
                            </ul>
                            </nav>
<?php
                        }
                    }
?>
                        </div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
                <div id="bodyTab2" style="display:none" >
                    <div id="form">
                    <form method="get" action="">
                        <div class="title" style="font-size:20px; margin-bottom:20px">Search receipt inventory</div>
                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>
                        
                        <div class="caption">From Date</div>
                        <div class="field">
                            <input type="text" name="from_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>
                        
                        <div class="caption">To Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo date('d-m-Y'); ?>" name="to_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>
                        
                        <div class="caption">Supplier</div>
                        <div class="field" style="width:305px;">
                            <select class="selectpicker form-control" 
                                    name='supplierAccCode'
                                    data-style="btn-default" 
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($cashAccounts)){
                            while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
                                $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$cash_ina_hand['ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
<?php
                            }
                        }
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                                $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Bill No</div>
                        <div class="field" style="width:305px;">
                            <input type="text" value="" name="billNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Supplier Info</div>
                        <div class="field" style="width:305px;">
                            <input type="text" value="" name="supplier_info" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"> Item Barcode </div>
                        <div class="field">
                            <input type="text" value="" name="imei" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form--> 
                </div> <!-- End bodyTab2 -->        
            </div> <!-- End .content-box-content -->    
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>