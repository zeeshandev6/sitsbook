<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/sale_details.php');
	include('common/classes/accounts.php');
	include('common/classes/j-voucher.php');
	include('common/classes/items.php');

	//Permission
	if(!in_array('purchase',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objInventory  				= new inventory();
	$objInventoryDetails  = new inventory_details();
	$objSaleDetails				= new saleDetails();
	$objJournalVoucher  	= new JournalVoucher();
	$objAccountCodes   		= new ChartOfAccounts();
	$objItems      				= new Items();
	$objConfigs         	= new Configs();

	if(isset($_GET['a'])){
		/*
		//SELECT * FROM `sale_details` WHERE UNIT_PRICE < COST_PRICE GROUP BY ITEM_ID
		$query = "SELECT * FROM items WHERE ID IN (SELECT ITEM_ID FROM purchase_details)";
		$rows  = mysql_query($query);

		echo mysql_num_rows($rows)."<hr />";
		if(mysql_num_rows($rows)){
			while($row = mysql_fetch_assoc($rows)){
				echo $row['NAME']."--".$row['PURCHASE_PRICE'];
				echo "<hr />";
			}
		}
		*/
		/*
		$query1 = "SELECT * FROM `sale_details` WHERE UNIT_PRICE < COST_PRICE GROUP BY ITEM_ID ";
		$rows1  = mysql_query($query1);

		if(mysql_num_rows($rows1)){
			while($row1 = mysql_fetch_assoc($rows1)){
				$query = "SELECT UNIT_PRICE FROM purchase_details WHERE ITEM_ID = ".$row1['ITEM_ID']."  ORDER BY ID DESC LIMIT 1  ";
				$rows  = mysql_query($query);
				$rows  = mysql_fetch_assoc($rows);

				$qx = "UPDATE `sale_details` SET COST_PRICE = ".$rows['UNIT_PRICE']." WHERE ITEM_ID = '".$row1['ITEM_ID']."' ";
				mysql_query($qx);
				$qx2 = "UPDATE `items` SET PURCHASE_PRICE = ".$rows['UNIT_PRICE']." WHERE ID = '".$row1['ITEM_ID']."' ";
				mysql_query($qx2);
				echo $rows['UNIT_PRICE'];
				echo "<hr />";
			}
		}
		*/

		/*
		$query1 = "SELECT * FROM `sale_details` WHERE UNIT_PRICE < COST_PRICE GROUP BY ITEM_ID ";
		$rows1  = mysql_query($query1);

		if(mysql_num_rows($rows1)){
			while($row1 = mysql_fetch_assoc($rows1)){
				$query = "SELECT UNIT_PRICE FROM purchase_details WHERE ITEM_ID = ".$row1['ITEM_ID']."  ORDER BY ID DESC LIMIT 1  ";
				$rows  = mysql_query($query);
				$rows  = mysql_fetch_assoc($rows);

				$qx = "UPDATE `sale_details` SET COST_PRICE = ".$rows['UNIT_PRICE']." WHERE ITEM_ID = '".$row1['ITEM_ID']."' ";
				mysql_query($qx);
				$qx2 = "UPDATE `items` SET PURCHASE_PRICE = ".$rows['UNIT_PRICE']." WHERE ID = '".$row1['ITEM_ID']."' ";
				mysql_query($qx2);

				$objItems->calculateAvgPrice($row1['ITEM_ID']);

				echo $rows['UNIT_PRICE'];
				echo "<hr />";
			}
		}
		*/

		/*
		//0301040001 cost of sales

		$query1 = "SELECT * FROM `sale` ";
		$rows1  = mysql_query($query1);

		if(mysql_num_rows($rows1)){
			while($row1 = mysql_fetch_assoc($rows1)){
				$total_cost = 0;
				$sale_item_list = $objSaleDetails->getList($row1['ID']);
				if(mysql_num_rows($sale_item_list)){
					while($sitem = mysql_fetch_assoc($sale_item_list)){

						$sumup = $sitem['QUANTITY']*$sitem['COST_PRICE'];

						$total_cost += $sumup;
					}
				}
				if($total_cost>0){
					$qe  = "UPDATE voucher_details SET AMOUNT = $total_cost WHERE VOUCHER_ID = ".$row1['VOUCHER_ID']." AND ACCOUNT_CODE = '0301040001' ";
					mysql_query($qe);
				}
			}
		}
		*/

		/*
		$query1 = "SELECT * FROM `sale` ";
		$rows1  = mysql_query($query1);

		if(mysql_num_rows($rows1)){
			while($row1 = mysql_fetch_assoc($rows1)){

			}
		}
		*/
		exit();
	}

	$totalRows 				 = $objInventory->countRows();
	$currency_type     = $objConfigs->get_config('CURRENCY_TYPE');
	//Pagination Settings
    $objPagination = new Pagination;
		$total 				 = $objPagination->getPerPage();
	//Pagination Settings { END }

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
  $invoice_format = explode('_', $invoice_format);
	$invoiceSize 		= $invoice_format[0]; // S  L

  if($invoiceSize == 'L'){
      $invoiceFile = 'purchase-invoice.php';
  }elseif($invoiceSize == 'M'){
      $invoiceFile = 'purchase-invoice.php'; //$invoiceFile = 'purchase-invoice-duplicates.php';
  }elseif($invoiceSize == 'S'){
      $invoiceFile = 'purchase-bill-small.php';
  }

	$suppliersList = $objInventory->getSuppliersList();
  $cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');

	$sms_config 	 = $objConfigs->get_config('SMS');

	if(isset($_GET['search'])){
		$objInventory->fromDate 						= (isset($_GET['from_date']))?$_GET['from_date']:"";
		$objInventory->toDate 							= (isset($_GET['to_date']))?$_GET['to_date']:"";
		$objInventory->billNum 							= (isset($_GET['billNum']))?$_GET['billNum']:"";
		$objInventory->po_number 						= (isset($_GET['po_number']))?$_GET['po_number']:"";
		$objInventory->vendorBillNum 				= (isset($_GET['vendor_bill_no']))?$_GET['vendor_bill_no']:"";
		$objInventory->supplierAccCode 			= (isset($_GET['supplierAccCode']))?$_GET['supplierAccCode']:"";
		$objInventory->with_tax  						= isset($_GET['with_tax'])?mysql_real_escape_string($_GET['with_tax']):"";
		$objInventory->unregistered_tax 		= (isset($_GET['unregistered_tax']))?mysql_real_escape_string($_GET['unregistered_tax']):"";
	}

	if(!$admin){
    $objInventory->user_id = $user_id;
  }

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	if(!isset($_GET['search'])){
		$objInventory->limit_start = $start;
	}
	$objInventory->limit_end = $total;
	$inventoryList = $objInventory->searchInventory();
	$found_records = $objInventory->found_records;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

    <script src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
		<script src="resource/scripts/sideBarFunctions.js"></script>
		<script src="resource/scripts/configuration.js"></script>
    <script src="resource/scripts/inventory.configuration.js"></script>
    <script src="resource/scripts/tab.js"></script>
    <script>
			$(document).ready(function() {
				$(".selectpicker").selectpicker();
	      $("a.pointer").click(function(){
					$(this).delete_main_list_record("db/del-inventory.php");
				});
	      $("td[data-items-list]").on("mouseover",function(){
	          var data = $(this).attr("data-items-list");
	          $(this).text(data);
	      });
	      $("td[data-items-list]").on("mouseout",function(){
	          var data = $(this).attr("data-items-list");
	          $(this).text(data.substr(0,40));
	      });
		  });
	</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Inventory Management - <?php echo $found_records; ?> Records</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="inventory-details.php"><div class="tab">New</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1" style="display:block;">
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
															<?php
																if($purchaseOrdersModule == 'Y'){
															?>
							   								<th style="text-align:center;width:5%;" >PO#</th>
															<?php
																}
															 ?>
                              	<th style="text-align:center;width:5%;" >Bill#</th>
																<?php if($show_vendor_bill_no=='Y'){ ?>
																<th style="text-align:center;width:5%;" >VendorBill#</th>
																<?php } ?>
		                            <th style="text-align:center;width:10%;">PurchaseDate</th>
		                            <th style="text-align:center;width:20%;">Supplier</th>
		                            <th style="text-align:center;width:20%;">Items</th>
		                            <th style="text-align:center;width:5%;" >Qty Receipt</th>
		                            <th style="text-align:center;width:15%;">Bill Amount(<?php echo $currency_type; ?>)</th>
																<?php if($sms_config=='Y'){ ?>
																<th style="text-align:center;width:5%;">SMS</th>
																<?php } ?>
		                            <th style="text-align:center;width:30%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
			if(mysql_num_rows($inventoryList)){
				while($purchaseRow = mysql_fetch_array($inventoryList)){
					$supplierTitle = 	 $objAccountCodes->getAccountTitleByCode($purchaseRow['SUPP_ACC_CODE']);
					$billTotalQuantity = $objInventory->getQuantityPerBill($purchaseRow['ID']);
					$billTotalAmount   = $objInventory->getInventoryAmountSum($purchaseRow['ID']);
					if($purchaseRow['INCOME_TAX']>0){
						$income_tax = ($purchaseRow['INCOME_TAX']*$billTotalAmount)/100;
						$billTotalAmount += $income_tax;
					}
					$itemsArray = 		 $objInventory->getPurchaseDetailArrayItemOnly($purchaseRow['ID']);
					$itemsList = '';
					foreach($itemsArray as $key => $item_id){
						if($key > 0){
							$itemsList .= ', ';
						}
						$itemsList .= $objItems->getItemTitle($item_id);
					}
?>
                        <tr data-row-id="<?php echo $purchaseRow['ID']; ?>" data-main-id="<?php echo $purchaseRow['ID']; ?>">
													<?php
														if($purchaseOrdersModule == 'Y'){
													?>
														<td style="text-align:center"><?php echo $purchaseRow['PO_NUMBER']; ?></td>
													<?php
														}
													?>
														<td style="text-align:center"><?php echo $purchaseRow['BILL_NO']; ?></td>
														<?php if($show_vendor_bill_no=='Y'){ ?>
														<td style="text-align:center"><?php echo $purchaseRow['VENDOR_BILL_NO']; ?></td>
														<?php } ?>
                            <td style="text-align:center"><?php echo date('d-m-Y',strtotime($purchaseRow['PURCHASE_DATE'])); ?></td>
                            <td style="text-align:left"><?php echo $supplierTitle; ?></td>
                            <td style="text-align:left;" class="pl-10" data-items-list="<?php echo $itemsList; ?>"><?php echo substr($itemsList, 0,40); ?></td>
                            <td style="text-align:center"><?php echo number_format($billTotalQuantity,3); ?></td>
                            <td style="text-align:center"><?php echo number_format(round($billTotalAmount),2); ?></td>
														<?php if($sms_config=='Y'){ ?>
														<td style="text-align:center">
															<button type="button" class="btn btn-<?php echo $purchaseRow['SMS_STATUS']=='Y'?"success":"default"; ?> btn-sm" data-id="<?php echo $purchaseRow['ID']; ?>" onclick="send_sms_from_list(this,'inventory-details.php');"><i class="fa fa-<?php echo $purchaseRow['SMS_STATUS']=='Y'?"check":"envelope"; ?>"></i></button>
														</td>
														<?php } ?>
                            <td style="text-align:center">
															<a id="view_button" href="inventory-details.php?id=<?php echo $purchaseRow['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
															<a id="view_button" target="_blank" href="<?php echo $invoiceFile; ?>?id=<?php echo $purchaseRow['ID']; ?>" title="Print"><i class="fa fa-print"></i></a>
															<a id="view_button" target="_blank" href="voucher-print.php?id=<?php echo $purchaseRow['VOUCHER_ID']; ?>" title="Print"> JV</a>
															<?php if(in_array('delete-purchase',$permissionz) || $admin == true){ ?>
                            	<a do="<?php echo $purchaseRow['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
                            	<?php } ?>
                            	<?php if(in_array('purchase-return',$permissionz) || $admin == true){ ?>
                            	<a class="button-orange" href="inventory-return-details.php?pid=<?php echo $purchaseRow['ID']; ?>" title="Return" ><i class="fa fa-reply"></i></a>
                            	<?php } ?>
                        	</td>
                        </tr>
<?php
				}
			}elseif(isset($_GET['search'])){
?>
							<tr>
                            	<th colspan="100" style="text-align:center;">
                                	<?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
                                </th>
                            </tr>
<?php
			}
?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
<?php
					if(!isset($_GET['search']) && $found_records > $total){
?>
	                        <nav>
	                          <ul class="pagination">
<?php
						$count = $found_records;
						$total_pages = ceil($count/$total);
						$i = 1;
						$thisFileName = $_SERVER['PHP_SELF'];
						if(isset($this_page) && $this_page>0){
?>
							<li>
								<a href="<?php echo $thisFileName; ?>?page=1">First</a>
							</li>
<?php
						}
						if(isset($this_page) && $this_page>=1){
							$prev = $this_page;
?>
							<li>
								<a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
							</li>
<?php
						}
						$this_page_act = $this_page;
						$this_page_act++;
						while($total_pages>=$i){
							$left = $this_page_act-5;
							$right = $this_page_act+5;
							if($left<=$i && $i<=$right){
							$current_page = ($i == $this_page_act)?"active":"";
?>
								<li class="<?php echo $current_page; ?>">
                                	<?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                </li>
<?php
							}
							$i++;
						}
						$this_page++;
						if(isset($this_page) && $this_page<$total_pages){
							$next = $this_page;
							echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
						}
						if(isset($this_page) && $this_page<$total_pages){
?>
								<li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
							</ul>
                        	</nav>
<?php
						}
					}
?>
						</div>
                    <div class="clear"></div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
                <div id="bodyTab2" style="display:none;" >
                    <div id="form">
                    <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

                        <div class="caption">From Date</div>
                        <div class="field">
                        	<input type="text" value="<?php echo ($objInventory->fromDate == '')?'':date('d-m-Y',strtotime($objInventory->fromDate)); ?>" name="from_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">To Date</div>
                        <div class="field">
                        	<input type="text" value="<?php echo ($objInventory->toDate == '')?date('d-m-Y'):date('d-m-Y',strtotime($objInventory->toDate)); ?>" name="to_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Supplier</div>
                        <div class="field" style="width:305px;">
                        	<select class="selectpicker form-control"
                            		name='supplierAccCode'
                                    data-style="btn-default"
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($cashAccounts)){
                            while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
                                $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$cash_ina_hand['ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
<?php
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                                $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">System Bill #</div>
                        <div class="field">
                        	<input type="text" value="<?php echo $objInventory->billNum; ?>" name="billNum" class="form-control" />
                        </div>
						<div class="clear"></div>



                        <div class="caption">Vendor Bill #</div>
                        <div class="field">
                        	<input type="text" value="<?php echo $objInventory->vendorBillNum; ?>" name="vendor_bill_no" class="form-control" />
                        </div>
                        <div class="clear"></div>

<?php
											if($purchaseOrdersModule == 'Y'){
?>
												<div class="caption">PO #</div>
                        <div class="field">
                        	<input type="text" value="<?php echo $objInventory->billNum; ?>" name="po_number" class="form-control" />
                        </div>
												<div class="clear"></div>
<?php
											}
?>

												<?php if($saleTaxModule=='Y'){ ?>
												<div class="caption">WH.Income Tax</div>
												<div class="field" style="width:300px;position:relative;">
													<input type="radio" name="with_tax"  value="" id="radio0x" class="css-checkbox" checked  />
													<label for="radio0x" class="css-label-radio radGroup2"> All </label>
													<input type="radio" name="with_tax"  value="Y" id="radio1x" class="css-checkbox" />
													<label for="radio1x" class="css-label-radio radGroup2"> YES </label>
													<input type="radio" name="with_tax"  value="N" id="radio3x" class="css-checkbox" />
													<label for="radio3x" class="css-label-radio radGroup2"> NO </label>
												</div>
												<div class="clear"></div>

												<div class="caption">Status</div>
												<div class="field" style="width:300px;position:relative;">
													<input type="radio" name="unregistered_tax"  value="" id="radio0xyz" class="css-checkbox" checked  />
													<label for="radio0xyz" class="css-label-radio radGroup2"> All </label>
													<input type="radio" name="unregistered_tax"  value="Y" id="radio1xyz" class="css-checkbox" />
													<label for="radio1xyz" class="css-label-radio radGroup2"> Unregistered </label>
													<input type="radio" name="unregistered_tax"  value="N" id="radio1zee" class="css-checkbox" />
													<label for="radio1zee" class="css-label-radio radGroup2"> Registered </label>
												</div>
												<div class="clear"></div>
												<?php } ?>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
	$(document).ready(function() {
		$(".shownCode,.loader").hide();
        $("input.supplierTitle").keyup(function(){
			$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
		});
		$(window).keydown(function(e){
			if(e.keyCode==113){
				e.preventDefault();
				window.location.href = "<?php echo "inventory-details.php"; ?>";
			}
		});
    });
	<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>tab('2', '1', '2');<?php } ?>
</script>
