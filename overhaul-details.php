<?php 
	include('common/connection.php');
	include 'common/config.php';
    include('common/classes/accounts.php');
	include('common/classes/overhaul.php');
	include('common/classes/overhaul_details.php');
	include('common/classes/items.php');
	include('common/classes/measure.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');

	//Permission
	if(!in_array('work-in-process',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

    $objAccountCodes          = new ChartOfAccounts();
	$objOverHaul              = new OverHaul();
	$objOverHaulDetails       = new OverHaulDetails();
	$objItems            	  = new Items();
	$objMeasures 			  = new Measures();
	$objItemCategory     	  = new itemCategory();
	$objDepartments      	  = new Departments();
	
    $suppliers_list = $objAccountCodes->getAccountByCatAccCode('040101');
    $accounts_list  = $objAccountCodes->getLevelFourList();
	$newJobNumber   = $objOverHaul->getJobNumber();
	
    $overHaul = NULL;
	$wid = 0;
	if(isset($_GET['wid'])){
		$wid = mysql_real_escape_string($_GET['wid']);
		if($wid > 0){
			$overHaul     = $objOverHaul->getRecordDetails($wid);
			$overHaulList = $objOverHaulDetails->getList($wid);
		}
	}
?>
<!DOCTYPE html  
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    <style>
        table{
            border: 1px solid #CCC;
        }
        table th{
            padding: 10px !important; 
            border: 1px solid #CCC;
        }
        table td{
            padding: 5px !important; 
            border: 1px solid #CCC;
        }
        .col-md-10{
            float: none;
            margin: 10px auto;
        }
    </style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.ui.timepicker.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/alphanumeric.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/overhaul.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.timepicker.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>    
    </div> <!-- End #sidebar -->
    	<div class="content-box-top">
        	<div class="content-box-header">
            	<p>Overhaul Process Details</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="wip.php?tab=list"><div class="tab">List</div></a>
                        <a href="wip.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected" id="tab1">Details</div>
                    </div>
                </span>
            </div><!--content-box-header-->
            <div class="content-box-content">
            	<div id="form" class="col-md-10">
                    <form method="post" action="" class="speedForm">
                    	<input type="hidden" name="gate_pass_num" value="0" class="form-control" />
                        
                        <div class="caption">Product</div>
                        <div class="field" style="width:300px">
                            <select class="resultingItemSelector show-tick form-control" 
                                    data-style="btn-default" 
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                    $itemsCategoryList = $objItemCategory->getList();
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                            $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
                                <optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
                            if(mysql_num_rows($itemList)){
                                while($theItem = mysql_fetch_array($itemList)){
                                    if($theItem['ACTIVE'] == 'N'){
                                        continue;
                                    }
                                    if($theItem['INV_TYPE'] == 'B'){
                                        continue;
                                    }

                                    $item_selected = ($overHaul['RESULT_ITEM'] == $theItem['ID'])?"selected":"";
?>
                                    <option <?php echo $item_selected; ?> data-subtext="<?=$theItem['ITEM_BARCODE']?>" value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
                                }
                            }
?>
                                </optgroup>
<?php
            }
        }
?>
                            </select>
                        </div>
                        <div class="caption">Job #</div>
                        <div class="field" style="width:150px">
                        	<input type="text" class="form-control" value="<?php echo (isset($overHaul))?$overHaul['ID']:$newJobNumber; ?>" readonly="readonly" />
                        </div>
                        <div class="clear"></div>

						<div class="caption">Quantity</div>
                        <div class="field" style="width:300px">
                        	<input type="text" name="result_qty" class="form-control" value="<?php echo (isset($overHaul))?$overHaul['RESULT_QUANTITY']:""; ?>" />
                        </div>
                        <div class="caption">Started On</div>
                        <div class="field" style="width:150px">
                            <input type="text" name="started_on" class="datepicker form-control" value="<?php echo (isset($overHaul))?date('d-m-Y',strtotime($overHaul['STARTED_ON'])):date("d-m-Y"); ?>" />
                        </div>
                        <div class="clear"></div>
                        <input type="hidden" class="oh_id" name="oh_id" value="<?php echo $wid; ?>" />
                        <div class="clear"></div>
                    </form>
            	</div>
                <div class="clear"></div>
                <hr />
                <div class="col-md-10">
                    <table class="prom">
                        <thead>
                            <tr style="background:#EEE;">
                               <th width="20%" style="font-size:12px;text-align:center">Item</th>
                               <th width="10%" style="font-size:12px;text-align:center">Quality</th>
                               <th width="10%" style="font-size:12px;text-align:center">Unit Cost</th>
                               <th width="10%" style="font-size:12px;text-align:center">Total Cost</th>
                               <th width="10%" style="font-size:12px;text-align:center">StockInHand</th>
                               <th width="10%" style="font-size:12px;text-align:center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="quickSubmit" style="background:none">
                                <td style="padding:0; line-height:0;text-align:left;">
                                    <select class="issueItemSelector show-tick form-control" 
                                        data-style="btn-default" 
                                        data-show-subtext="true"
                                        data-live-search="true" style="border:none">
                                           <option selected value=""></option>
<?php
                    $itemsCategoryList = $objItemCategory->getList();
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                            $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
                                <optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
                            if(mysql_num_rows($itemList)){
                                while($theItem = mysql_fetch_array($itemList)){
                                    if($theItem['ACTIVE'] == 'N'){
                                        continue;
                                    }
                                    if($theItem['INV_TYPE'] == 'B'){
                                        continue;
                                    }
?>
                                    <option data-subtext="<?=$theItem['ITEM_BARCODE']?>" value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
                                }
                            }
?>
                                </optgroup>
<?php
            }
        }
?>
                                    </select>
                                </td>
                                <td><input type="text" class="quantity form-control" /></td>
                                <td><input type="text" class="unitPrice form-control" readonly="readonly" /></td>
                                <td><input type="text" class="gross_cost form-control" readonly="readonly" /></td>
                                <td><input type="text" class="stock_in_hand form-control" readonly="readonly" /></td>
                                <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="button  quick_submit" value="Enter" /></td>
                            </tr>
                        </tbody>
                    <tbody>
                    <!--doNotRemoveThisLine-->
                        <tr style="display:none;" class="calculations"></tr>
                    <!--doNotRemoveThisLine-->
    <?php
                $total_cost = 0;
                $total_quat = 0;
                if(isset($overHaulList) && mysql_num_rows($overHaulList)){
                    while($work_row = mysql_fetch_array($overHaulList)){
                        $item_id     = $work_row['ITEM_ID'];
                        $item_name   =  $objItems->getItemTitle($work_row['ITEM_ID']);
                        $gross_price = $work_row['GROSS_COST'];
                        $unit_price  = $work_row['GROSS_COST']/$work_row['QUANTITY'];
                        $total_quat += $work_row['QUANTITY'];
    ?>
                        <tr data-row-id="0" class="alt-row calculations transactions">
                            <td data-item-id="<?php echo $item_id; ?>" class="itemName" style="text-align:left;"><?php echo $item_name; ?></td>
                            <td class="quantity" style="text-align:center;"><?php echo $work_row['QUANTITY']; ?></td>
                            <td class="unitPriceTd" style="text-align:center;"><?php echo $unit_price; ?></td>
                            <td class="gross_price" style="text-align:center;"><?php echo $gross_price; ?></td>
                            <td style="text-align:center;"> - - - </td>
                            <td style="text-align:center;">
                                <a title="Update" onclick="editThisRow(this);" id="view_button"><i class="fa fa-pencil"></i></a>
                                <a onclick="$(this).parent().parent().remove();" title="Delete" do="" class="pointer"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
    <?php

                        $total_cost += $gross_price;
                    }
                }
    ?>
                        <tr class="totals">
                            <td style="text-align:right;">Total</td>
                            <td style="text-align:center;" class="quantityTotal"><?=$total_quat?></td>
                            <td style="text-align:center;"> - - - </td>
                            <td style="text-align:center;" class="costTotal"><?php echo number_format($total_cost,2); ?></td>
                            <td style="text-align:center;"> - - - </td>
                            <td style="text-align:center;"> - - - </td>
                        </tr>
                    </tbody>
                </table>
                </div>
                <div class="clear"></div>
                <div id="form">
                    <div class="pull-left">
                        
                    </div>
                    <div id="form" style="float:right;margin:10px 85px 10px 10px;">
                        <div class="pull-right">
                            <div class="caption">External Expense</div>
                            <div class="field" style="width:200px">
                                <select class="labour_account form-control" data-live-search="true">
                                    <option value=""></option>
                                    <?php
                                        if(mysql_num_rows($suppliers_list)){
                                            while($supp_row = mysql_fetch_assoc($suppliers_list)){
                                                $ext_selected = (isset($overHaul)&&$overHaul['LABOUR_CODE'] == $supp_row['ACC_CODE'])?"selected":"";
                                                ?><option value="<?php echo $supp_row['ACC_CODE']; ?>" <?php echo $ext_selected; ?> ><?php echo $supp_row['ACC_TITLE']; ?></option><?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="caption">
                                <input type="text" name="labour" class="form-control text-center" value="<?php echo (isset($overHaul))?$overHaul['LABOUR']:""; ?>" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="pull-right">
                            <div class="caption">Internal Expense</div>
                            <div class="field" style="width:200px">
                                <select class="overhead_account form-control" data-live-search="true">
                                    <option value=""></option>
                                    <?php
                                        if(mysql_num_rows($accounts_list)){
                                            while($supp_row = mysql_fetch_assoc($accounts_list)){
                                                $int_selected = (isset($overHaul)&&$overHaul['OVERHEAD_CODE'] == $supp_row['ACC_CODE'])?"selected":"";
                                                ?><option value="<?php echo $supp_row['ACC_CODE']; ?>" <?php echo $int_selected; ?>><?php echo $supp_row['ACC_TITLE']; ?></option><?php
                                            }
                                        }
                                    ?>
                                </select>
                                <div class="clear"></div>
                            </div>
                            <div class="caption">
                                <input type="text" name="overhead" class="form-control text-center" value="<?php echo (isset($overHaul))?$overHaul['OVERHEAD']:""; ?>" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="pull-right mr-10">
                            <div class="caption">Total Cost</div>
                            <div class="field" style="width:150px">
                                <input type="text" class="form-control text-right" name="final_cost" value="<?php echo number_format(($total_cost+$overHaul['OVERHEAD']+$overHaul['LABOUR']),2); ?>" readonlyt />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="underTheTable col-md-10">
                    <?php if($overHaul['STATUS'] != 'C'){ ?>
                        <button class="newForm saveProcess pull-right"><?php echo (isset($overHaul))?"Update":"Save"; ?></button>
                    <?php } ?>
                    <button class="newForm" onclick="window.location.href = 'wip-details.php';">New Form</button>
                </div>
                <div class="clear"></div>
            </div><!--content-box-content-->
        </div><!--content-box-->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="popUpForm3" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
                <p>New Product Title:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="productTitle" class="input_size newProductTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div><!--popUpForm-->
</body>
</html>
<?php include('conn.close.php'); ?>