<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/accounts.php');
	include('common/classes/j-voucher.php');
	include('common/classes/items.php');

	//Permission
	if(!in_array('sale-returns',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objSaleReturn  	= new DistributorSaleReturn();
	$objJournalVoucher  = new JournalVoucher();
	$objAccounts   		= new ChartOfAccounts();
	$objItems      		= new Items();

	$total              = $objConfigs->get_config('PER_PAGE');

	$suppliersList = $objAccounts->getAccountByCatAccCode('010104');
	if(isset($_GET['search'])){
        $objSaleReturn->billNum         = mysql_real_escape_string($_GET['billNum']);
        $objSaleReturn->po_number       = mysql_real_escape_string($_GET['po_number']);
        $objSaleReturn->customerAccCode = mysql_real_escape_string($_GET['supplierAccCode']);
    }
    if(isset($_GET['page'])){
        $this_page = $_GET['page'];
        if($this_page>=1){
            $this_page--;
            $start = $this_page * $total;
        }
    }else{
        $start = 0;
        $this_page = 0;
    }

		//$objSaleReturn->sale_id = '0';

    $inventoryList = $objSaleReturn->search($start, $total);
    $found_records = $objSaleReturn->found_records;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <style>
		html{
        }
        .ui-tooltip{
            font-size: 12px;
            padding: 5px;
            box-shadow: none;
            border: 1px solid #999;
        }
        div.tabPanel .tab,div.tabPanel .tabSelected{
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
        }
        .input_sized{
            float:left;
            width: 152px;
            padding-left: 5px;
            border: 1px solid #CCC;
            height:30px;
            -webkit-box-shadow:#F4F4F4 0 0 0 2px;
            border:1px solid #DDDDDD;

            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;

            -moz-border-radius-topleft:3px;
            -moz-border-radius-topright:3px;
            -moz-border-radius-bottomleft:3px;
            -moz-border-radius-bottomright:5px;

            -webkit-border-top-left-radius:3px;
            -webkit-border-top-right-radius:3px;
            -webkit-border-bottom-left-radius:3px;
            -webkit-border-bottom-right-radius:3px;

            box-shadow: 0 0 2px #eee;
            transition: box-shadow 300ms;
            -webkit-transition: box-shadow 300ms;
        }
        .input_sized:hover{
            border-color: #9ecaed;
            box-shadow: 0 0 2px #9ecaed;
        }
        input[type='text']{

		}
		span.red{
			color:red;
		}
		span.green{
			color:green;
		}
        td{
            padding: 8px !important;
        }
	</style>

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/dist.sale.return.config.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".selectpicker").selectpicker();
            $("a.pointer").click(function(){
				$(this).deleteRowConfirmation("db/del-dist-sale-return.php");
			});
        });
	</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sale Returns - <?php echo $found_records; ?> Records </p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="dist-sale-return-details.php"><div class="tab">New</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1" style="display:block;">
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th width="5%" style="text-align:center">Memo #</th>
                               <th width="10%" style="text-align:center">ReturnDate</th>
                               <th width="20%">Account</th>
                               <th width="25%">Items</th>
                               <th width="10%">Qty Returned</th>
                               <th width="10%" style="text-align:center">Action</th>
                            </tr>
                        </thead>

                        <tbody>
<?php
			if(mysql_num_rows($inventoryList)){
				while($purchaseRow = mysql_fetch_array($inventoryList)){
					$supplierTitle  = $objAccounts->getAccountTitleByCode($purchaseRow['CUST_ACC_CODE']);
					$quantity_row   = $objSaleReturn->getQuantityPerBill($purchaseRow['ID']);
					$itemsArray     = $objSaleReturn->getSaleDetailArrayItemOnly($purchaseRow['ID']);
					$itemsList = '';
                    if($purchaseRow['SALE_ID'] == 0){
                        $urlink = "dist-sale-return-details.php?id=".$purchaseRow['ID'];
                    }else{
                        $urlink = "dist-sale-return-by-bill.php?sid=".$purchaseRow['SALE_ID'];
                    }
                    $billTotalQuantity = 0;
					foreach($itemsArray as $key => $item_row){
						if($key > 0){
							$itemsList .= ', ';
						}
						$itemsList .= $item_row['NAME'];
                        $billTotalQuantity +=  $quantity_row['TOTAL_QUANTITY'];
                        $billTotalQuantity +=  $quantity_row['TOTAL_CARTONS']*$item_row['QTY_PER_CARTON'];
                        $billTotalQuantity +=  $quantity_row['TOTAL_DOZENS']*12;
					}
?>
                            <tr class="alt-row">
                            	<td style="text-align:center"><?php echo $purchaseRow['BILL_NO']; ?></td>
                                <td style="text-align:center"><?php echo date('d-m-Y',strtotime($purchaseRow['SALE_DATE'])); ?></td>
                                <td style="text-align:left"><?php echo $supplierTitle; ?></td>
                                <td style="text-align:left"><?php echo $itemsList; ?></td>
                                <td style="text-align:center"><?php echo $billTotalQuantity; ?></td>
                                <td style="text-align:center">
										<a id="view_button" href="<?php echo $urlink; ?>"><i class="fa fa-eye"></i></a>
										<a id="view_button" target="_blank" href="sale-returns-memo.php?id=<?php echo $purchaseRow['ID']; ?>" ><i class="fa fa-print"></i></a>
                                        <a id="view_button" target="_blank" href="voucher-print.php?id=<?php echo $purchaseRow['VOUCHER_ID']; ?>" title="Print"><i class="fa fa-print"></i> JV</a>
                                    	<a do="<?php echo $purchaseRow['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
                            	</td>
                            </tr>
<?php
				}
			}
?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
<?php
                    if(!isset($_GET['search'])){
?>
                            <nav>
                              <ul class="pagination">
<?php
                        $count = $found_records;
                        $total_pages = ceil($count/$total);
                        $i = 1;
                        $thisFileName = $_SERVER['PHP_SELF'];
                        if(isset($this_page) && $this_page>0){
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=1">First</a>
                            </li>
<?php
                        }
                        if(isset($this_page) && $this_page>=1){
                            $prev = $this_page;
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
                            </li>
<?php
                        }
                        $this_page_act = $this_page;
                        $this_page_act++;
                        while($total_pages>=$i){
                            $left = $this_page_act-5;
                            $right = $this_page_act+5;
                            if($left<=$i && $i<=$right){
                            $current_page = ($i == $this_page_act)?"active":"";
?>
                                <li class="<?php echo $current_page; ?>">
                                    <?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                </li>
<?php
                            }
                            $i++;
                        }
                        $this_page++;
                        if(isset($this_page) && $this_page<$total_pages){
                            $next = $this_page;
                            echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
                        }
                        if(isset($this_page) && $this_page<$total_pages){
?>
                                <li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
                            </ul>
                            </nav>
<?php
                        }
                    }
?>
                    </div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
                <div id="bodyTab2" style="display:none;" >
                    <div id="form">
                    <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

                        <div class="caption">Account</div>
                        <div class="field" style="width:305px;">
                        	<select class="selectpicker form-control"
                            		name='supplierAccCode'
                                    data-style="btn-default"
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                                $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Memo No</div>
                        <div class="field" style="width:305px;">
                        	<input type="text" value="" name="billNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">PO No</div>
                        <div class="field" style="width:305px;">
                        	<input type="text" value="" name="po_number" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
	$(document).ready(function() {
		$(".shownCode,.loader").hide();
        $("input.supplierTitle").keyup(function(){
			$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
		});
		<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>
			$("#bodyTab2 .dropdown-toggle").focus();
			$("#bodyTab2 billNum").setFocusTo("#bodyTab2 input[type='submit']");
			$("#bodyTab2 select").change(function(){
				$("#bodyTab2 input[name='billNum']").focus();
			});
		<?php } ?>
    });

	$(function(){
		$("#fromDatepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			showAnim : 'show',
			changeMonth: true,
			changeYear: true,
			yearRange: '2000:+10'
		});
		$("#toDatepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			showAnim : 'show',
			changeMonth: true,
			changeYear: true,
			yearRange: '2000:+10'
		});
	});
	<?php echo (isset($_POST['searchInventory']))?"tab('1', '1', '2');":""; ?>
	<?php echo (isset($_GET['tab'])&&$_GET['tab'] == 'search')?"tab('2', '1', '2');":""; ?>
</script>
