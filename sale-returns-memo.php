<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/sale_return.php';
	include 'common/classes/sale_return_details.php';
	include 'common/classes/customers.php';
	include 'common/classes/items.php';
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	$objCompanyDetails 	= new CompanyDetails();
	$objSales 					= new SaleReturn();
	$objSaleDetails 		= new SaleReturnDetails();
	$objCustomers   		= new Customers();
	$objItems  	    		= new Items();
	$objJournalVoucher 	= new JournalVoucher();

	if(isset($_GET['id'])){
		$sale_id 								= mysql_real_escape_string($_GET['id']);
		$saleDetails 						= mysql_fetch_array($objSales->getRecordDetails($sale_id));
		$saleDetailList 				= $objSaleDetails->getList($sale_id);
		$customer_balance 			= $objJournalVoucher->getInvoiceBalance($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID']);
		$cutomer_balance_array 	= $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
	}
	$invoice_header_type = 'A';   // A or B
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/invoiceStyle.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/invoiceStyle.css",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			//$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100%;background-repeat: no-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		$sale_date     = date('d-m-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));

		$companyLogo   	 	= $objCompanyDetails->getLogo();
		$company  		 		= $objCompanyDetails->getActiveProfile();
?>

<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
<?php
	if(in_array('general-ledger',$permissionz) || $admin == true){
?>
            <a target="_blank" class="button" href="voucher-print.php?id=<?php echo $saleDetails['VOUCHER_ID']; ?>">Ledger</a>
<?php
	}
?>
        </div><!--headerWrapper-->
    </div><!--header-->
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
        <div class="printThisDiv" style="height: 800px;position: relative;">
            <div class="invoiceHead" style="width: 720px;margin: 0px auto;">
<?php
				if($companyLogo != ''){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
            		<div style="font-size:26px;padding-bottom: 10px;"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']; ?>
                    <br />
                    Contact : <?php echo $company['CONTACT']; ?>
                    <br />
                    Email : <?php echo $company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
	                SALES RETURNS MEMO
                </div>
                <div class="clear" style="height: 5px;"></div>
<?php
				if($invoice_header_type == 'A'){
?>
                <div class="infoPanel pull-left">
                	<div class="infoHead">Customer Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['ADDRESS']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['CITY']; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right">
                	<div class="infoHead">Memo Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Memo No. <?php echo $saleDetails['BILL_NO']; ?></span>
	                <span class="pull-right" style="padding:0px 0px;font-size:14px;">Date. <?php echo $sale_date; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
<?php
				}

				if($invoice_header_type == 'B'){
?>
				<div class="invoPanel">
					<div class="invoPanel">
						<div class="clear"></div>
	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Name: </span>
	                		<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Address: </span>
	                		<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">City: </span>
	                		<span class="text_line"><?php echo $customer['CITY']; ?></span>
	                	</span>
	                	<div class="clear"></div>
	                </div>
				</div>
<?php
				}
?>
				<div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear" style="height: 10px;"></div>
            <div class="invoiceBody" style="width: 720px;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="10%">Sr#</th>
                            <th width="15%">Description</th>
                            <th width="10%">Quantity</th>
                            <th width="10%">Rate/Unit</th>
                            <th width="10%">Disc/Rate</th>
                            <th width="10%">Sub Total</th>
                            <th width="10%">Tax Rate</th>
                            <th width="10%">Tax Amount</th>
                            <th width="15%">Total Amount</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($saleDetailList)){
							while($row = mysql_fetch_array($saleDetailList)){
								$itemName = $objItems->getItemTitle($row['ITEM_ID']);
?>
								<tr>
                                	<td><?php echo $counter; ?></td>
									<td style="font-size:12px;text-align:left;"><?php echo $itemName; ?></td>
									<td><?php echo $row['QUANTITY']; ?></td>
									<td><?php echo $row['UNIT_PRICE']; ?></td>
                                    <td><?php echo $row['SALE_DISCOUNT']; ?></td>
                                    <td><?php echo $row['SUB_AMOUNT']; ?></td>
                                    <td><?php echo $row['TAX_RATE']; ?></td>
                                    <td><?php echo $row['TAX_AMOUNT']; ?></td>
                                    <td><?php echo $row['TOTAL_AMOUNT']; ?></td>
								</tr>
<?php
									$quantity    += $row['QUANTITY'];
									$subAmount 	 += $row['SUB_AMOUNT'];
									$taxAmount   += $row['TAX_AMOUNT'];
									$totalAmount += $row['TOTAL_AMOUNT'];

									$counter++;
							}
						}
?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="text-align:right;" colspan="2">Bill Total</td>
                            <td><?php echo $quantity; ?></td>
                            <td style="text-align:center;" colspan="2"> - - - </td>
                            <td><?php echo $subAmount; ?></td>
                            <td style="text-align:center;"> - - - </td>
                            <td><?php echo $taxAmount; ?></td>
                            <td><?php echo $totalAmount; ?></td>
                        </tr>
                        <tr style="background-color:  #EEE;">
                        	<td colspan="7" style="text-align: right !important;">Balance : </td>
                        	<td colspan="2" style="text-align: right !important;padding-right: 10px;"><?php echo $cutomer_balance_array['BALANCE']." ".$cutomer_balance_array['TYPE']; ?></td>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>
                <div class="clear" style="height: 5px;"></div>
                <div class="invoice_footer">
	                <div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
	                	Prepared By : ___________
	                </div><!--partyTitle-->
	                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
	                	Checked By : ___________
	                </div><!--partyTitle-->
	                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
	                	Authorized By : ___________
	                </div><!--partyTitle-->
	                <div class="clear"></div>
	        	</div>
            </div><!--invoiceBody-->
        </div><!--printThisDiv-->
        </div><!--invoiceLeftPrint-->
    </div><!--invoiceContainer-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
