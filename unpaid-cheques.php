<?php
  include 'msgs.php';
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/cash_management.php';
	include 'common/classes/suppliers.php';
  include 'common/classes/accounts.php';

	//Permission
	if(!in_array('cash-receipts',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

  $objChartOfAccounts      = new ChartOfAccounts();
	$objSuppliers            = new suppliers();
	$objCashManagement       = new CashManagement();

  if(isset($_GET['get_possessions'])){
    $possession = mysql_real_escape_string($_GET['get_possessions']);
    $column_name = 'MEMO';
    $values = $objCashManagement->get_autocomplete_values($possession,$column_name);
    echo json_encode($values);
    exit();
  }

  $cheque_id = 0;
  if(isset($_POST['save'])){
    $cheque_id                          = mysql_real_escape_string($_POST['cheque_id']);
		$objCashManagement->chequeDate      = date('Y-m-d',strtotime($_POST['chequeDate']));
		$objCashManagement->today           = date('Y-m-d',strtotime($_POST['currentDate']));
		$objCashManagement->customer_code   = mysql_real_escape_string($_POST['supplier_code']);
		$objCashManagement->bank_acc_code   = mysql_real_escape_string($_POST['bank_acc_code']);
		$objCashManagement->amount          = mysql_real_escape_string($_POST['amount']);
		$objCashManagement->chequeNum       = mysql_real_escape_string($_POST['chequeNumber']);
		$objCashManagement->bankReceipt     = 0;
		$objCashManagement->memo            = mysql_real_escape_string($_POST['memo']);
    $objCashManagement->status          = mysql_real_escape_string($_POST['cheque_status']);
		$objCashManagement->type            =  'P';  // P = Payment
    if($cheque_id==0){
      if($objCashManagement->chequeDate==$objCashManagement->today){
  			$objCashManagement->status      = 'O';
  		}elseif($objCashManagement->chequeDate>$objCashManagement->today){
  			$objCashManagement->status      = 'P';
  		}
    }
		$objCashManagement->post            = 'N';
		if($objCashManagement->chequeDate<$objCashManagement->today){
			$message = " Cheque's Date is Incorrect! ";
		}else{
      if($cheque_id){
        $recorded = $objCashManagement->update($cheque_id);
      }else{
        $cheque_id = $objCashManagement->save();
        $recorded  = $cheque_id;
      }
			if($recorded){
				echo "<script>window.location.href = 'unpaid-cheques.php?id=$cheque_id';</script>";
			}else{
				echo "<script>alert('Entry Could Not Be Saved!');</script>";
			}
		}
	}
	$message = "";
  $cheque_details = NULL;
  if(isset($_GET['id'])){
    $cheque_id = mysql_real_escape_string($_GET['id']);
    $cheque_details = $objCashManagement->getDetails($cheque_id);
  }
  if(isset($_POST['search'])){
    $fromDate 	= isset($_POST['fromDate'])?date('Y-m-d',strtotime($_POST['fromDate'])):"";
    $toDate 		= isset($_POST['toDate'])?date('Y-m-d',strtotime($_POST['toDate'])):"";
    $accCode 		= (isset($_POST['accCode']))?mysql_real_escape_string($_POST['accCode']):"";
    $status 		= (isset($_POST['status']))?mysql_real_escape_string($_POST['status']):"";
    $type   		= 'P';
    $ChequeList = $objCashManagement->search($fromDate,$toDate,$accCode,$status,$type);
  }else{
    $ChequeList = $objCashManagement->getPaymentChequeListNext();
        $till_next3days = true;
  }
?>
<!DOCTYPE html 
   >
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link type="text/css"  href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
        <link type="text/css"  href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css"  href="resource/css/bootstrap-select.css" rel="stylesheet">
        <link type="text/css"  href="resource/css/easy-autocomplete.css" rel="stylesheet">
        <link type="text/css"  href="resource/css/easy-autocomplete-theme.css" rel="stylesheet">
        <style>
			html{
			}
			.ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized{
				float:left;
				width: 152px;
				padding-left: 5px;
				color:rgb(102, 102, 102) !important;
				border: 1px solid rgb(153, 153, 153);
				height:30px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;

				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;

				-moz-border-radius-topleft:3px;
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;

				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;

				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			}
			.input_sized:hover{
				border: 1px solid rgb(41, 153, 255) !important;
				box-shadow: 0 0 2px #9ecaed;
			}
			input.error{
				border:1px solid red !important;
			}
		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/cash-management.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
        <script type="text/javascript" src="resource/scripts/easy-autocomplete.js"></script>
        <script type="text/javascript">
			$(window).on('load', function () {
				$('select').selectpicker();
				$(".bankSelect").selectpicker();
				$(".chequeStatusSelector").selectpicker();

				$(".the-form").validate({
					rules:{
						supplier_code:"required",
						currentDate:"required",
						bank_acc_code:"required",
						chequeNumber:"required",
						amount:"required",
						chequeDate:"required"
				    },
				    messages:{
				    	supplier_code:"",
				    	currentDate:"",
				    	chequeNumber:"",
				    	bank_acc_code:"",
				    	amount:"",
				    	chequeDate:""
				    }
				});
			});
		</script>
   </head>

<body>
	<div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div id="bodyWrapper">
            <div class="content-box-top" style="overflow:visible;">
                <div class="summery_body">
                    <div class="content-box-header">
                        <p>Cheque Payments</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab3" onClick="tab('3', '1', '3');">List</div>
                                <div class="tab" id="tab2" onClick="tab('2', '1', '3');">Search</div>
                                <a href="unpaid-cheques.php"><div class="tab" id="tab1">Add</div></a>
                            </div>
                        </span>
                        <div class="clear"></div>
                    </div><!-- End .content-box-header -->

                    <div id="bodyTab1">
                        <div id="form">
                            <form method="post" action="" class="the-form">

                            <div class="caption" style="width:190px;">Entry Date:</div>
                            <div class="field" >
                               <input class="form-control datepicker" name="currentDate" type="text" value="<?php echo ($cheque_details==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($cheque_details['TODAY'])); ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Payee: </div>
                            <div class="field" >
                               <select class="form-control" name="supplier_code" data-live-search="true">
                                 <option value=""></option>
 <?php
                                  $accounts_list       = $objChartOfAccounts->getLevelFourList();
                                   if(mysql_num_rows($accounts_list)){
                                       while($account = mysql_fetch_array($accounts_list)){
                                         $acc_selected = ($cheque_details['PARTY_ACC_CODE']==$account['ACC_CODE'])?"selected":"";
 ?>
                                          <option <?php echo $acc_selected; ?> data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" ><?php echo $account['ACC_TITLE']; ?></option>
 <?php
                                       }
                                   }
 ?>
 <?php
                                   $suppliersList       = $objChartOfAccounts->getAccountByCatAccCode('040101');
                                   if(mysql_num_rows($suppliersList)){
                                       while($account = mysql_fetch_array($suppliersList)){
                                         $acc_selected = ($cheque_details['PARTY_ACC_CODE']==$account['ACC_CODE'])?"selected":"";
 ?>
                                          <option <?php echo $acc_selected; ?> data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" ><?php echo $account['ACC_TITLE']; ?></option>
 <?php
                                       }
                                   }
 ?>
                               </select>
                            </div>
                            <div style="clear:both;"></div>
                            <div class="caption" style="width:190px;">Bank:</div>
                            <div class="field">
                              <select class="form-control" name="bank_acc_code" data-live-search="true">
                                <option value=""></option>
<?php
                                  $banksList = $objChartOfAccounts->getAccountByCatAccCode('010102');
                                  if(mysql_num_rows($banksList)){
                                      while($account = mysql_fetch_array($banksList)){
                                        $bnk_selected = ($cheque_details['BANK_ACC_CODE']==$account['ACC_CODE'])?"selected":"";
?>
                                         <option <?php echo $bnk_selected; ?> data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                      }
                                  }
?>
                              </select>
                            </div>
                            <div class="clear"></div>
                            <div class="caption" style="width:190px;">Cheque's Date:</div>
                            <div class="field" >
                               <input class="form-control datepicker" name="chequeDate" type="text" value="<?php echo ($cheque_details==NULL)?'':date('d-m-Y',strtotime($cheque_details['CHEQUE_DATE'])); ?>"  />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Cheque #:</div>
                            <div class="field">
                               <input class="form-control" name="chequeNumber" type="text" value="<?php echo $cheque_details['CHEQUE_NO']; ?>"  />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Amount:</div>
                            <div class="field">
                               <input class="form-control chequeAmount" name="amount" type="text" value="<?php echo $cheque_details['AMOUNT']; ?>"  />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Cheque Status:</div>
                            <div class="field">
                              <select class="form-control" name="cheque_status">
                                <option value="O" <?php echo ($cheque_details['STATUS']=='O')?"selected='selected'":""; ?> >Cheque In Hand</option>
                                <option value="R" <?php echo ($cheque_details['STATUS']=='R')?"selected='selected'":""; ?> >Dishonour</option>
                                <option value="C" <?php echo ($cheque_details['STATUS']=='C')?"selected='selected'":""; ?> >Pass</option>
                                <option value="P" <?php echo ($cheque_details['STATUS']=='P')?"selected='selected'":""; ?> >Post Dated</option>
                                <option value="I" <?php echo ($cheque_details['STATUS']=='I')?"selected='selected'":""; ?> >Issued</option>
                                <option value="D" <?php echo ($cheque_details['STATUS']=='D')?"selected='selected'":""; ?> >Deposite</option>
                                <option value="X" <?php echo ($cheque_details['STATUS']=='X')?"selected='selected'":""; ?> >Cancelled</option>
                              </select>
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Cheque Possession:</div>
                            <div class="field">
                               <input class="form-control" name="memo" type="text" value="<?php echo $cheque_details['MEMO']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;"></div>
                            <div class="field">
                              <input type="hidden" name="cheque_id" value="<?php echo $cheque_id; ?>">
                              <input type="submit" name="save" class="btn btn-primary btn-sm" value="<?php echo $cheque_id>0?"Update":"Save"; ?>" />
                              <?php
                              if($cheque_id>0){
                                ?><a class="btn btn-default btn-sm pull-right" href="?">New Entry</a><?php
                              }
                              ?>
                            </div>
                            <div class="clear"></div>

                            </form>
                        </div><!--#form-->
                    </div><!--bodyTab1-->

                    <div id="bodyTab2">
                        <div id="form">
                            <form method="post" action="">
                            <div class="caption" style="width:190px;">Payee :</div>
                            <div class="field" >
                              <select class="form-control" name="supplier_code" data-live-search="true">
                                <option value=""></option>
<?php
                                  $accounts_list       = $objChartOfAccounts->getLevelFourList();
                                  if(mysql_num_rows($accounts_list)){
                                      while($account = mysql_fetch_array($accounts_list)){
?>
                                         <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                      }
                                  }
?>
                              </select>
                            </div><!--field-->
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">Cheque Status:</div>
                            <div class="field" >
                              <select name="status" class="form-control">
                                <option value="">All</option>
                                <option value="O">Cheque in Hand</option>
                                <option value="R">Dishonour</option>
                                <option value="C">Pass</option>
                                <option value="P">Post Dated</option>
                                <option value="I">Issued</option>
                                <option value="D">Deposite</option>
                              </select>
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">From Date:</div>
                            <div class="field">
                               <input class="form-control datepicker" name="fromDate" type="text" value=""  />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;">To Date:</div>
                            <div class="field">
                               <input class="form-control datepicker" name="toDate"
                               type="text" value="<?php echo date('d-m-Y'); ?>"  />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:190px;"></div>
                               <input type="submit" style="height:30px;margin:5px;" name="search" class="button" value="Search" />
                            <div class="clear"></div>
                        </form>
                        </div><!--#form-->
                    </div><!--bodyTab2-->
                    <div class="clear"></div>
                    <div id="bodyTab3">
	                    <div class="clear"></div>
<?php
					if(isset($ChequeList)){
?>
          <?php
              if(isset($till_next3days)){
                  $till_date = date("d-M-Y",strtotime("+3 days"));
                  echo '<div class="col-md-12">';
                  infoMessage("Showing Records Till : ".$till_date);
                  echo '</div>';
              }
          ?>
                    <div class="col-md-12">
                        <table cellspacing = "0" align = "center" >
                         <thead>
                            <tr>
                               <th width="10%" style="text-align:center" class="bg-color">Entry Date</th>
                               <th width="15%" style="text-align:center" class="bg-color">Payee Title</th>
                               <th width="10%" style="text-align:center" class="bg-color">Bank Name</th>
                               <th width="10%" style="text-align:center" class="bg-color">Cheque#</th>
                               <th width="10%" style="text-align:center" class="bg-color">Due Date</th>
                               <th width="10%" style="text-align:center" class="bg-color">Amount</th>
                               <th width="10%" style="text-align:center" class="bg-color">Chq.Possession</th>
                               <th width="5%"  style="text-align:center" class="bg-color">Status</th>
                               <th width="10%" style="text-align:center" class="bg-color">Status Date</th>
                               <th width="10%"  style="text-align:center" class="bg-color">Action</th>
                            </tr>
                         </thead>

                         <tbody>
                            <tr class="transactions" style="display:none;"></tr>
                        <?php
                        if(mysql_num_rows($ChequeList)){
                        	$totalAmount = 0;
                        	while($row = mysql_fetch_array($ChequeList)){
                        ?>
                            <tr class="transactions">
                                <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['TODAY'])); ?></td>
                                <td style="text-align:center;"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['PARTY_ACC_CODE']); ?></td>
                                <td style="text-align:center;"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['BANK_ACC_CODE']); ?></td>
                                <td style="text-align:center;"><?php echo $row['CHEQUE_NO']; ?></td>
                                <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['CHEQUE_DATE'])); ?></td>
                                <td style="text-align:center;"><?php echo $row['AMOUNT']; ?></td>
                                <td style="text-align:center;"><?php echo $row['MEMO']; ?></td>
                                <td style="text-align:center;" class="statusTd<?php echo $row['ID']; ?>">
                                	<select class="chequeStatusSelector form-control" data-id='<?php echo $row['ID']; ?>'	>
                                    <option value="O" <?php echo ($row['STATUS']=='O')?"selected='selected'":""; ?> >Cheque In Hand</option>
                                    <option value="R" <?php echo ($row['STATUS']=='R')?"selected='selected'":""; ?> >Dishonour</option>
                                    <option value="C" <?php echo ($row['STATUS']=='C')?"selected='selected'":""; ?> >Pass</option>
                                    <option value="P" <?php echo ($row['STATUS']=='P')?"selected='selected'":""; ?> >Post Dated</option>
                                    <option value="I" <?php echo ($row['STATUS']=='I')?"selected='selected'":""; ?> >Issued</option>
                                    <option value="D" <?php echo ($row['STATUS']=='D')?"selected='selected'":""; ?> >Deposite</option>
                                    <option value="X" <?php echo ($cheque_details['STATUS']=='X')?"selected='selected'":""; ?> >Cancelled</option>
                                  </select>
                                </td>
                                <td style="text-align:center;" class="clearanceDate<?php echo $row['ID']; ?>">
                                  <?php echo ($row['STATUS_DATE']=='0000-00-00')?"":date('d-m-Y',strtotime($row['STATUS_DATE'])); ?>
                                </td>
                                <td class="text-center">
                                  <a href="unpaid-cheques.php?id=<?php echo $row['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        <?php
                        		$totalAmount += $row['AMOUNT'];
                        	}
                        }
                        ?>
                            <tr style="background-color:#F8F8F8;height: 30px;">
                                <td colspan="5" style="text-align:right;"> Total: </td>
                                <td class="creditTotal" style="text-align:center;color:#BD0A0D;"><?php echo (isset($totalAmount))?$totalAmount:"0"; ?></td>
                                <td colspan="4" style="text-align:center;"> - - - </td>
                            </tr>
                         </tbody>
                        </table>
                    </div>
<?php
					}
?>
                    </div><!--bodyTab3-->
                </div><!--summery_body-->
                    <div style="clear:both;"></div>
            </div><!-- End .content-box-top -->
        </div><!-- End summer -->
	</div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="light"></div>
</body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
  var bg_colours = {};
  bg_colours['R'] = 'bg-danger';
  bg_colours['O'] = '';
  bg_colours['I'] = 'bg-warning';
  bg_colours['D'] = 'bg-warning';
  bg_colours['C'] = 'bg-success';
  bg_colours['P'] = 'bg-grey';
	$(window).load(function(){
		$("button.dropdown-toggle").first().focus();
		$("input[type='submit']").escFocusTo("select.bankSelect");
		$(".narration").setFocusTo(".receiptNum");
		$(".receiptNum").setFocusTo(".amount");
		$(".narration").escFocusTo("button.dropdown-toggle");
		$(".receiptNum").escFocusTo(".narration");
		$(".amount").escFocusTo(".receiptNum");
		var keyFlag = false;
		$("select.bankSelect").keyup(function(e){
			if(e.keyCode==13){
				if(keyFlag==true){
					$("input[type='submit']").focus();
				}else{
					keyFlag = true;
				}
			}else{
				keyFlag = false;
			}
		});
		$("input[name='bank_acc_code']").setFocusTo(".datepicker");
		$(".datepicker").setFocusTo("input[name='chequeNumber']");
		$(".datepicker").setFocusTo("input[name='chequeNumber']");

		$("input[name='balanceBefore']").focus(function(){
			$("button.dropdown-toggle").last().focus();
		});
		$("input[name='bankBalance']").focus(function(){
			$("input[name='chequeDate']").last().focus();
		});
		$("input.ChequeBoxAll").click(function(){
			$('tbody').find("input.chequeBox").attr('checked',$(this).is(":checked"));

		});
		$("input[name='chequeDate']").setFocusTo("input[name='chequeNumber']");
		$("input[name='chequeNumber']").setFocusTo("input[name='amount']");
		$("input[name='amount']").setFocusTo("input[name='memo']");
		$("input[name='memo']").setFocusTo("input[type='submit']");
		$.fn.postEntry = function(){
			$(this).click(function(){
				var thisPointer = $(this);
				var row_id = $(this).attr('do');
				$.post('db/createCashManagementVoucher.php',{cash_id:row_id,createPaymentVoucher:true},function(data){
					if(data==''){
						thisPointer.hide();
						$(".statusTd"+row_id).children(".chequeStatusSelector").attr("disabled",true);
						$('td.row-action-'+row_id).text('POSTED');
						$(".chequeStatusSelector").selectpicker('refresh');
					}
				});
			});
		}
		$(".chequeAmount").numericInputOnly();
    $("select[name=cheque_status]").change(function(){
      var thisElement = $(this);
      var row_id = $("input[name=cheque_id]").val();
			var status = $(this).val();
      $.post('db/chequeStatus.php',{id:row_id,status:status},function(data){
        $("td.clearanceDate"+row_id).text(data);
        thisElement.prop('disabled',false);
        thisElement.selectpicker('refresh');
        $("td.statusTd"+row_id+" select").selectpicker('val',status);
        displayMessage("Cheque Status Changed Successfully!");
      });
    });
		$(".chequeStatusSelector").change(function(){
			var thisElement = $(this);
			thisElement.attr('disabled',true);
			thisElement.selectpicker('refresh');
			var row_id = $(this).attr('data-id');
			var status = $(this).val();

			var currentRow = $(this).parent('td').parent('tr');

      $.post('db/chequeStatus.php',{id:row_id,status:status},function(data){
        $("td.clearanceDate"+row_id).text(data);
        thisElement.attr('disabled',false);
        thisElement.selectpicker('refresh');
        $(thisElement).parent().parent().parent().removeClass();
        $(thisElement).parent().parent().parent().addClass(bg_colours[status]+" transactions");
      });
		});
    $("select.chequeStatusSelector").each(function(){
      var status = $(this).val();
      $(this).parent().parent().parent().removeClass();
      $(this).parent().parent().parent().addClass(bg_colours[status]+" transactions");
    });
<?php
	if(isset($_GET['saved'])){
?>
			$("#fade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Record Saved Successfully!</p><a class='nodelete button'>Close</a></div>");
			$("#popUpDel").hide();
			$("#popUpDel").centerThisDiv();
			$("#fade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".nodelete").click(function(){
				$("#fade").fadeOut('slow');
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
					window.location.href = 'unpaid-cheques.php';
				});
			});
<?php
	}
?>
		$("button.view_button").postEntry();
    });
</script>
<script><?php if(isset($_POST['search'])){ ?>tab('3', '1', '3');<?php }else{ ?>tab('1', '1', '3');<?php } ?></script>
