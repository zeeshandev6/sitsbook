<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/mobile-sale.php';
	include 'common/classes/mobile-sale-details.php';
	include 'common/classes/mobile-purchase-details.php';
	include 'common/classes/customers.php';
	include 'common/classes/items.php';
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	$objCompanyDetails 			= new CompanyDetails;
	$objScanSale   					= new ScanSale;
	$objScanSaleDetails 		= new ScanSaleDetails;
	$objScanPurchaseDetails = new ScanPurchaseDetails;
	$objCustomers   				= new Customers;
	$objItems  	    			  = new Items;
	$objJournalVoucher 			= new JournalVoucher;
	$objConfigs     				= new Configs();

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	$scan_bill = NULL;
	$scan_bill_details = NULL;

	if(isset($_GET['id'])){
		$id = mysql_real_escape_string($_GET['id']);
		$scan_bill = $objScanSale->getDetail($id);
		$scan_bill_details = $objScanSaleDetails->getList($id);
		$supplier_balance = $objJournalVoucher->getInvoiceBalance($scan_bill['CUST_ACC_CODE'],$scan_bill['VOUCHER_ID']);
		$supplier_balance_array = $objJournalVoucher->getBalanceType($scan_bill['CUST_ACC_CODE'], $supplier_balance);
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/invoiceStyle.css" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "resource/css/invoiceStyle.css",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		//$(".printThis").click();
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100%;background-repeat: no-repeat;">
	<?php
	if(isset($id)){
		$supplier 	   = $objCustomers->getCustomer($scan_bill['CUST_ACC_CODE']);
		$purchase_date     = date('d-m-Y',strtotime($scan_bill['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($purchase_date));
		$companyLogo   = $objCompanyDetails->getLogo($session_branch_id);
		$companyTitle  = $objCompanyDetails->getTitle('1');
		$company  = $objCompanyDetails->getRecordDetails($session_branch_id);
		?>
		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
					<button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="invoiceContainer">
				<div class="invoiceLeftPrint">
					<div class="printThisDiv">
						<div class="invoiceHead" style="width: 720px;margin: 0px auto;">
							<?php
							if($companyLogo != ''){
								?>
								<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
								<?php
							}
							?>
							<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
								<div style="font-size:26px;padding-bottom: 10px;"> <?php echo $company['NAME']; ?> </div>
								<?php echo $company['ADDRESS']; ?>
								<br />
								Contact : <?php echo $company['CONTACT']; ?>
								<br />
								Email : <?php echo $company['EMAIL']; ?>
								<br />
							</div>
							<div class="clear"></div>
							<div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
								<?php echo (substr($scan_bill['CUST_ACC_CODE'], 0,6) == '010101')?"CASH":""; ?> SALES INVOICE
							</div>
							<div class="clear" style="height: 5px;"></div>
							<div class="infoPanel pull-left">
								<div class="infoHead">Customer Details</div>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;">
									<?php
									if(substr($scan_bill['CUST_ACC_CODE'], 0,6) != '010101'){
										?>
										<?php echo $supplier['CUST_ACC_TITLE']; ?>
										<?php
									}else{
										echo $scan_bill['CUSTOMER_NAME'];
									}
									?>
								</span>
								<div class="clear"></div>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $supplier['ADDRESS']; ?></span>
								<div class="clear"></div>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $supplier['CITY']; ?></span>
								<div class="clear"></div>
							</div><!--partyTitle-->
							<div class="infoPanel pull-right">
								<div class="infoHead">Invoice Details</div>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;">Inv No. <?php echo $scan_bill['BILL_NO']; ?></span>
								<div class="clear"></div>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;">Invoice Date. <?php echo $purchase_date; ?> Time : <?php echo date('h:i A',strtotime($scan_bill['RECORD_TIME'])); ?> </span>
								<div class="clear"></div>
							</div><!--partyTitle-->
							<div class="clear"></div>
						</div><!--invoiceHead-->
						<div class="clear" style="height: 10px;"></div>
						<div class="invoiceBody" style="width: 720px;margin: 0px auto;">
							<table>
								<thead>
									<tr>
										<th width="5%">Sr#</th>
										<th width="20%">Brand</th>
										<th width="20%">Item</th>
										<th width="10%">Colour</th>
										<th width="10%">Rate</th>
										<th width="10%">Discount</th>
										<th width="10%">Tax</th>
										<th width="20%">Price</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$image_list = array();
									$totalAmount = 0;
									$counter = 1;
									if(mysql_num_rows($scan_bill_details)){
										while($row = mysql_fetch_array($scan_bill_details)){
											$purchase_detail = $objScanPurchaseDetails->getDetails($row['SP_DETAIL_ID']);
											$itemDetails = $objItems->getRecordDetails($purchase_detail['ITEM_ID']);
											if(!in_array($itemDetails['IMAGE'],$image_list)){
												$image_list[$itemDetails['NAME']] = $itemDetails['IMAGE'];
											}
											?>
											<tr>
												<td><?php echo $counter; ?></td>
												<td><?php echo $itemDetails['COMPANY']; ?></td>
												<td><?php echo $itemDetails['NAME']." ( ".$purchase_detail['BARCODE']." ) "; ?></td>
												<td style="text-align:center;"><?php echo $purchase_detail['COLOUR']; ?></td>
												<td style="padding-right: 10px;"><span class="pull-right"><?php echo $row['PRICE']; ?></span></td>
												<td style="padding-right: 10px;"><span class="pull-right"><?php echo $row['DISCOUNT']; ?></span></td>
												<td style="padding-right: 10px;"><span class="pull-right"><?php echo $row['TAX']; ?></span></td>
												<td style="padding-right: 10px;"><span class="pull-right"> <?php echo $currency_type; ?> <?php echo $row['SALE_PRICE']; ?></span></td>
											</tr>
											<?php
											$totalAmount += $row['SALE_PRICE'];

											$counter++;
										}
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td style="text-align:right !important;" colspan="7">Total Amount</td>
										<td style="padding-right: 10px;"><span class="pull-right"> <?php echo $currency_type; ?> <?php echo number_format($totalAmount,2); ?></span></td>
									</tr>
									<tr style="background-color:  #EEE;">
										<td colspan="4"></td>
										<td style="text-align: right !important;" colspan="3">Balance : </td>
										<td style="text-align: right;padding-right: 10px;">
											<?php
											if(substr($scan_bill['CUST_ACC_CODE'], 0,6) != '010101'){
												?>
												<span class="pull-right">
													<?php echo $currency_type; ?> <?php echo number_format($supplier_balance_array['BALANCE'],2)." ".$supplier_balance_array['TYPE']; ?>
												</span>
												<?php
											}
											?>
										</td>
									</tr>
								</tfoot>
							</table>
							<div class="clear"></div>
							<div class="clear" style="height: 5px;"></div>

							<?php if(count($image_list)){
											$image_path = 'uploads/products/';
											foreach ($image_list as $product_name => $file_name) {
												if(is_file($image_path.$file_name)){
								?>
														<div class="product_thumb">
															<img src="<?php echo $image_path.$file_name; ?>" alt="" />
															<span class="caption_thumbg"><?php echo $product_name; ?></span>
														</div>
							<?php
												}
											}
											?><div class="clear"></div><?php
										}
							?>

							<div class="invoice_footer">
								<div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
									Prepared By : ___________
								</div><!--partyTitle-->
								<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
									Checked By : ___________
								</div><!--partyTitle-->
								<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
									Authorized By : ___________
								</div><!--partyTitle-->
								<div class="clear"></div>
							</div>
						</div><!--invoiceBody-->
					</div><!--printThisDiv-->
				</div><!--invoiceLeftPrint-->
			</div><!--invoiceContainer-->
		</div><!--invoiceBody-->
		<?php
	}
	?>
</body>
</html>
<?php include('conn.close.php'); ?>
