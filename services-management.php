<?php
    $out_buffer = ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/services.php');

    //Permission
    if(!in_array('service-managment',$permissionz) && $admin != true){
      echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
      echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
      echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
      echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
      echo '</div>';
      exit();
    }
    //Permission ---END--

    $objServices    = new Services();

    $mode = "list";

    if(isset($_GET['mode'])){
        $mode = $_GET['mode'];
    }

    $serviceDetails = NULL;

    if(isset($_POST['delete_id'])){
        $return = array('OKAY'=>'N');
        $service_id = mysql_real_escape_string($_POST['delete_id']);
        $service_id = (int)($service_id);
        if($service_id){
            $used = $objServices->checkIfused($service_id);
            if(!$used){
                $deleted = $objServices->delete($service_id);
                if($deleted){
                    $return = array('OKAY'=>'Y');
                }
            }
        }
        echo json_encode($return);
        exit();
    }

    if(isset($_POST['save'])){
        $id                        = (int)(mysql_real_escape_string($_POST['id']));
        $objServices->title        = mysql_real_escape_string(ucfirst($_POST['title']));
        $objServices->price        = mysql_real_escape_string(ucfirst($_POST['price']));
        $objServices->description  = mysql_real_escape_string(ucfirst($_POST['description']));

        if($id > 0){
            $itemid = $objServices->update($id);
            exit(header('location: services-management.php?id='.$id.'&action=updated'));
        }else{
            $idd = $objServices->save();
            exit(header('location: services-management.php?id='.$idd.'&action=added'));
        }
    }

    if(isset($_GET['id'])){
        if(!empty($_GET['id'])){
            $id = mysql_real_escape_string($_GET['id']);
            $serviceDetails = $objServices->getDetail($id);
        }
    }

    if($mode == 'list'){
        $services_list = $objServices->getList();
    }
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".pointer").click(function(){
                    var file    = "services-management.php";
                    var idValue = $(this).attr("do");
                    var clickedDel = $(this);
                    $("#fade").hide();
                    $("#popUpDel").remove();
                    $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
                    $("#popUpDel").hide();
                    $("#popUpDel").centerThisDiv();
                    $("#fade").fadeIn('slow');
                    $("#popUpDel").fadeIn();
                    $(".dodelete").click(function(){
                            $.post(file, {delete_id : idValue}, function(data){
                                data = $.parseJSON(data);
                                $("#popUpDel").children(".confirm").text(data['MSG']);
                                $("#popUpDel").children(".dodelete").hide();
                                $("#popUpDel").children(".nodelete").text("Close");
                                if(data['OKAY'] == 'Y'){
                                    clickedDel.parent('td').parent('tr').remove();
                                }
                                $("#fade").fadeOut();
                                $("#popUpDel").fadeOut(function(){
                                    $(this).remove();
                                });
                            });
                        });
                    $(".nodelete").click(function(){
                        $("#fade").fadeOut();
                        $("#popUpDel").fadeOut();
                    });
                    $(".close_popup").click(function(){
                        $("#popUpDel").slideUp();
                        $("#fade").fadeOut('fast');
                    });
                });
            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div>
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p>Services Management</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="services-management.php?mode=list"><div class="<?php echo ($mode == 'list')?"tabSelected":"tab"; ?>">List</div></a>
                                <a href="services-management.php?mode=new"><div class="<?php echo ($mode == 'new')?"tabSelected":"tab"; ?>"><?php echo ($serviceDetails == 'NULL')?"New":"Details"; ?></div></a>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>

<?php
            if($mode == 'list'){
?>
                <div id="bodyTab1">
                    <div class="col-xs-12 mt-20">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="col-xs-1 text-center" >Sr.</th>
                                    <th class="col-xs-2 text-center" >Title</th>
                                    <th class="col-xs-2 text-center" >Price</th>
                                    <th class="col-xs-5 text-center" >Description</th>
                                    <th class="col-xs-2 text-center" >Action</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                            $counter = 1;
                            if(isset($services_list) && mysql_num_rows($services_list)){
                                while($service_row = mysql_fetch_assoc($services_list)){
?>
                                <tr>
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-center"><?php echo $service_row['TITLE']; ?></td>
                                    <td class="text-center"><?php echo $service_row['PRICE']; ?></td>
                                    <td class="text-left"><?php echo $service_row['DESCRIPTION']; ?></td>
                                    <td class="text-center">
                                        <a href="services-management.php?mode=new&id=<?php echo $service_row['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                        <a class="pointer ml-5" do="<?php echo $service_row['ID']; ?>"> <i class="fa fa-times"></i> </a>
                                    </td>
                                </tr>
<?php
                                    $counter++;
                                }
                            }else{
                                ?>
                                <tr>
                                    <td class="text-center" colspan="6"> No Records Found! </td>
                                </tr>
                                <?php
                            }
?>
                            </tbody>
                        </table>
                    </div>
                </div>
<?php
            }
?>

<?php
            if($mode == 'new'){
?>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Currency Type Updated.");}
                    elseif($_GET['action']=='added'){ successMessage("Currency Type Added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
                }
                ?>

                <div id = "bodyTab1">
                    <form method="post" action="services-management.php?mode=new" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo (int)$serviceDetails['ID']; ?>" />
                        <div class="col-xs-12 mt-20">
                          <div class="form-group">
  			    								<label class="control-label col-sm-2">Title</label>
  			    								<div class="col-sm-10">
  			                      <input class="form-control" name="title" value="<?php echo $serviceDetails['TITLE'] ?>" required="required" autofocus/>
  			    								</div>
  			    							</div>

                          <div class="form-group">
  			    								<label class="control-label col-sm-2">Rate </label>
  			    								<div class="col-sm-10">
  			                      <input class="form-control" type="text" name="price" value="<?php echo $serviceDetails['PRICE'] ?>" />
  			    								</div>
  			    							</div>

                          <div class="form-group">
  			    								<label class="control-label col-sm-2"> Description </label>
  			    								<div class="col-sm-10">
  			                      <textarea class="form-control" style="min-height:100px;" name="description"><?php echo $serviceDetails['DESCRIPTION'] ?></textarea>
  			    								</div>
  			    							</div>

                          <div class="form-group">
  			    								<label class="control-label col-sm-2"></label>
  			    								<div class="col-sm-10">
                              <input type="submit" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="btn btn-primary" />
                              <input type="button" value="New Form" onclick="window.location.href='services-management.php?mode=new'"  class="btn btn-default pull-right" />
  			    								</div>
  			    							</div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
<?php
            }
?>
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style="clear:both; height:20px"></div>
        </div><!--bodyWrapper-->
        <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
