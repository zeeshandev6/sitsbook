<?php
	session_start();
?>

<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
        }
        .ui-tooltip{
            font-size: 12px;
            padding: 5px;
            box-shadow: none;
            border: 1px solid #999;
        }
        div.tabPanel .tab,div.tabPanel .tabSelected{
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
        }
        .input_sized{
            float:left;
            width: 152px;
            padding-left: 5px;
            border: 1px solid #CCC;
            height:30px;
            -webkit-box-shadow:#F4F4F4 0 0 0 2px;
            border:1px solid #DDDDDD;
            
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
            
            -moz-border-radius-topleft:3px; 
            -moz-border-radius-topright:3px;
            -moz-border-radius-bottomleft:3px;
            -moz-border-radius-bottomright:5px;
            
            -webkit-border-top-left-radius:3px;
            -webkit-border-top-right-radius:3px;
            -webkit-border-bottom-left-radius:3px;
            -webkit-border-bottom-right-radius:3px;
            
            box-shadow: 0 0 2px #eee;
            transition: box-shadow 300ms;
            -webkit-transition: box-shadow 300ms;
        }
        .input_sized:hover{
            border-color: #9ecaed;
            box-shadow: 0 0 2px #9ecaed;
        }
        input[type='text']{
			
		}
		span.red{
			color:red;
		}
		span.green{
			color:green;
		}
	</style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <!-- jQuery Configuration -->
    <script type="text/javascript" src="resource/scripts/inventory.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script>
		$(document).ready(function() {
			
			//setInterval(function(){
				//$(".teminate").click();	
			//},1500);
			
			$(".teminate").click(function(){
				var counter = parseInt($(this).attr('data-counter')) || 0;
				$('.badge').text(counter);
				$('title').text(counter);
				$(".iFrame").prop('src','create-voucher.php?jid='+counter);
				counter++;
				$(this).attr('data-counter',counter);
			});
        });
	</script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
      
	<body>
		<div class="container" style="background-color: #FFF;">
			<div class="col-md-12">
				<div class="panel panel-primary" style="margin: 30px auto;">
					<div class="panel-heading">
						<button class="btn btn-default teminate" data-counter='468'>
							Update <span class="badge"></span>
						</button>
					</div>
					<div class="clearfix"></div>
				</div>
				<iframe class="iFrame panel panel-danger" style="height: 800px;width:100%;"></iframe>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</body>
</html>
<?php include('conn.close.php'); ?>