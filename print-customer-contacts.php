<?php
    include 'common/connection.php';
    include 'common/config.php';
    include 'common/classes/customer_contacts.php';

    //Permission
    if(!in_array('chart-of-accounts',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objCustomerContacts = new CustomerContacts();

    $customer_contacts_list = $objCustomerContacts->getList();
?>
<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/daily-report.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
            table tbody tr:hover{
                background-color: #EEE;
            }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#print_it").click(function(){
                    $(".printable").printThis({
                          debug: false,
                          importCSS: false,
                          printContainer: true,
                          loadCSS: 'resource/css/reports.css',
                          pageTitle: "Smart IT Solutions",
                          removeInline: false,
                          printDelay: 500,
                          header: null
                    });
                });
            });
        </script>
   </head>
   <body>
        <div id="body-wrapper">
            <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
        <div id="bodyWrapper">
            <div class = "content-box-top">
                <div class = "summery_body">
                    <div class = "content-box-header">
                        <p>Customer Contact Details</p>
                        <a style="margin: 8px 10px;" class="btn btn-sm btn-primary pull-right" id="print_it"><i class="fa fa-print"></i></a>
                        <div class="clear"></div>
                    </div><!-- End .content-box-header -->
                    <div style="height:50px;"></div>
                    <div id="bodyTab1" class="printable">
                        <div class="pageHeader">
                            <p style="font-size:18px;padding:0;text-align:center;">Customer Details</p>
                            <p style="float:right; margin-left:0px; font-size:12px" class="repoGen">Report generated on : <?php echo date('d-m-Y'); ?></p>
                        </div>
                        <div class="clear"></div>
                        <table class="table table-striped table-hover tableBreak" width="90%" cellspacing="0" align="left" style="margin:0px auto; margin-bottom:20px">
                            <thead class="tHeader">
                                <tr>
                                    <th style="width:20%;text-align:center;padding-left:10px !important; ">Customer Name</th>
                                    <th style="width:15%;text-align:center;padding-right:10px !important;">Mobile</th>
                                    <th style="width:65%;text-align:center;padding-left:10px !important; ">Address</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                if(mysql_num_rows($customer_contacts_list)){
                    while($row = mysql_fetch_array($customer_contacts_list)){
?>
                                <tr>
                                    <td class="acc_code " style="text-align:right;"><?php echo $row['CUSTOMER_NAME']; ?></td>
                                    <td class="acc_code " style="text-align:left;"><?php echo $row['CUSTOMER_MOBILE']; ?></td>
                                    <td class="acc_name " align="left" style="position: relative;">
                                        <?php echo $row['CUSTOMER_ADDRESS']; ?>
                                    </td>
                                </tr>
<?php
                    }
                }
?>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div><!--End bodyTab1-->
                </div><!-- End summer -->
            </div><!-- End .content-box -->
        </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
