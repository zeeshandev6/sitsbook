<?php
	ob_start();
	include ('common/connection.php');
	include ('common/config.php');
	include ('common/classes/accounts.php');
	include ('common/classes/demand_list.php');
	include ('common/classes/demand_list_detail.php');
	include ('common/classes/items.php');
	include ('common/classes/itemCategory.php');

	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}else{
		$mode = 'list';
	}

	//Permission
	if(!in_array('purchase-ordering',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts  = new ChartOfAccounts();
	$objDemandList       = new DemandList();
	$objDemandListDetail = new DemandListDetail();
	$objItems            = new Items();
	$objItemCategory     = new ItemCategory();

	$total         			 = $objConfigs->get_config('PER_PAGE');

	if(isset($_POST['delete_id'])){
		$id = (int)(mysql_real_escape_string($_POST['delete_id']));
		if($id > 0){
			$demand_detail = $objDemandList->getDetail($id);
			$objDemandList->delete($id);
			$objDemandListDetail->deleteList($id);
			echo "Y";
		}
		exit();
	}

	if(isset($_POST['json_data'])){
		$dl_id = (int)(mysql_real_escape_string($_POST['dl_id']));

		$objDemandList->po_number     		= mysql_real_escape_string($_POST['po_number']);
		$objDemandList->supplier_acc_code   = mysql_real_escape_string($_POST['supplier_acc_code']);
		$objDemandList->entry_date    		= date('Y-m-d',strtotime($_POST['entry_date']));
		$objDemandList->required_date 		= date('Y-m-d',strtotime($_POST['required_date']));

		if($dl_id > 0){
			$dem_detail = $objDemandList->getDetail($dl_id);
			$objDemandList->update($dl_id);
			$action = 'update';
		}else{
			$dl_id = $objDemandList->save();
			$action = 'save';
		}
		$dl_id = (int)$dl_id;
		if($dl_id > 0){
			$objDemandListDetail->dl_id = $dl_id;
			$prev_rows_ids     			= $objDemandListDetail->getIdArrayByDemand($dl_id);
			$tansactions       			= json_decode($_POST['json_data']);

			//save and update records - only
			$newTransactionIds = array();
			foreach($tansactions as $t=>$tansaction){
				$newTransactionIds[]  			  = (int)$tansaction->row_id;

				$objDemandListDetail->item_id     = $tansaction->item_id;
				$objDemandListDetail->quantity    = $tansaction->item_qty;
				$objDemandListDetail->unit_rate   = $tansaction->unit_rate;

				if((int)($tansaction->row_id) > 0){
					$objDemandListDetail->update(((int)$tansaction->row_id));
				}else{
					$objDemandListDetail->save();
				}
			}
			//delete records - only
			foreach ($prev_rows_ids as $key => $prev_id) {
				if(!in_array($prev_id,$newTransactionIds)){
					$objDemandListDetail->delete($prev_id);
				}
			}

			echo $action;
		}
		exit();
	}


	$suppliers_list = $objChartOfAccounts->getAccountByCatAccCode('040101');

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$demand_details = NULL;

	$objDemandList->po_number 		  = (isset($_GET['po_number']))?mysql_real_escape_string($_GET['po_number']):"";
	$objDemandList->from_date 		  = (isset($_GET['from_date']))?$_GET['from_date']:"";
	$objDemandList->to_date 		  = (isset($_GET['to_date']))?$_GET['to_date']:"";
	$objDemandList->supplier_acc_code = (isset($_GET['supplier_acc_code']))?mysql_real_escape_string($_GET['supplier_acc_code']):"";

	$demand_list              = $objDemandList->search($start,$total);

	$found_records            = $objDemandList->found_records;
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		table.table th{
			font-family: 'Trebuchet MS';
			font-weight: bold !important;
			font-size:   16px !important;
			height:      auto !important;
			padding:     5px !important;
		}
		table.table td{
			padding: 10px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/demand.list.config.js"></script>
	<script type="text/javascript" src="resource/scripts/notifyMe.browser.js"></script>
	<script>
		$(function(){
			$("select").selectpicker();
		});
	</script>
</head>

<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div id="bodyWrapper">
		<div class = "content-box-top" style="overflow:visible;">
			<div class = "summery_body">
				<div class = "content-box-header">
					<p> <i class="fa fa-print"></i> Purchase Ordering</p>
					<span id="tabPanel">
						<div class="tabPanel">
						<a href="<?php echo $fileName; ?>?mode=list" class="<?php echo ($mode=='list')?"tabSelected":"tab"; ?>" id="tab1">List</a>
						<a href="<?php echo $fileName; ?>?mode=search" class="<?php echo ($mode=='search')?"tabSelected":"tab"; ?>" id="tab1">Search</a>
						<a href="<?php echo $fileName; ?>?mode=form" class="<?php echo ($mode=='form')?"tabSelected":"tab"; ?>" id="tab1"><?php echo ($mode=='form')?"Detail":"New"; ?></a>
						</div>
					</span>
					<div style = "clear:both;"></div>
				</div><!-- End .content-box-header -->
				<div style = "clear:both; height:20px"></div>
				<?php
				if($mode == 'form'){
					$id = 0;
					if(isset($_GET['id'])){
						$id = (int)($_GET['id']);
						$demand_details   = $objDemandList->getDetail($id);
						$demand_item_list = $objDemandListDetail->getListByDemand($id);
					}
					$itemsCategoryList = $objItemCategory->getList();
					?>
					<div id = "bodyTab1">
						<div id="form" style="width:100% !important;margin:0px !important;">
							<div class="col-xs-10 col-xs-offset-1">
								<input type="hidden" name="dl_id" value="<?php echo $id; ?>" />

								<div class="caption" style="width: 80px;"> PO # :</div>
								<div class="field" style="width:150px;">
									<input class="form-control" name="po_number" value="<?php echo ($demand_details!=NULL)?$demand_details['PO_NUMBER']:''; ?>" required="required"/>
								</div>
								<div class="caption" style="width: 250px;"> Entry Date :</div>
								<div class="field" style="width:150px;">
									<input class="form-control datepicker" name="entry_date" value="<?php echo (isset($demand_details['ENTRY_DATE']))?date('d-m-Y',strtotime($demand_details['ENTRY_DATE'])):""; ?>" />
								</div>
								<div class="clear"></div>

								<div class="caption" style="width: 80px;">Supplier</div>
								<div class="field">
									<select name="supplier_acc_code" class="form-control" data-live-search="true" >
										<option value=""></option>
										<?php
											if(mysql_num_rows($suppliers_list)){
												while($supplier = mysql_fetch_assoc($suppliers_list)){
													$supplier_selected = (isset($demand_details)&&$demand_details['SUPPLIER_ACC_CODE']==$supplier['ACC_CODE'])?"selected":"";
													?><option <?php echo $supplier_selected; ?> value="<?php echo $supplier['ACC_CODE'] ?>"><?php echo $supplier['ACC_TITLE'] ?></option><?php
												}
											}
										?>
									</select>
								</div>

								<div class="caption"> Required Date :</div>
								<div class="field mb-20" style="width:150px;">
									<input class="form-control datepicker" name="required_date" value="<?php echo (isset($demand_details['REQUIRED_DATE']))?date('d-m-Y',strtotime($demand_details['REQUIRED_DATE'])):""; ?>" />
								</div>
								<div class="clear"></div>

								<table class="table">
									<thead>
										<tr>
											<th class="col-xs-5 text-center"> Item</th>
											<th class="col-xs-2 text-center"> Quantity</th>
											<th class="col-xs-2 text-center"> Unit Rate</th>
											<th class="col-xs-1 text-center"> Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(isset($demand_item_list)){
											while($item_row = mysql_fetch_assoc($demand_item_list)){
												?>
												<tr class="item_row" data-row-id="<?php echo $item_row['ID'] ?>">
													<td class="item_select" data-item-id="<?php echo $item_row['ITEM_ID'] ?>"></td>
													<td class="item_qty">
														<input type="text" class="form-control text-center" value="<?php echo $item_row['QUANTITY'] ?>" />
													</td>
													<td class="unit_rate">
														<input type="text" class="form-control text-center" value="<?php echo $item_row['UNIT_RATE'] ?>" />
													</td>
													<td class="item_act text-center">
														<button class="btn btn-default btn-xs" onclick="add_row(this);"> <i class="fa fa-plus"></i> </button>
														<button class="btn btn-danger btn-xs"  onclick="delete_row(this);"> <i class="fa fa-times"></i> </button>
													</td>
												</tr>
												<?php
											}
										}else{
											?>
											<tr class="item_row">
												<td class="item_select" data-item-id=""></td>
												<td class="item_qty">
													<input type="text" class="form-control text-center" value="" />
												</td>
												<td class="unit_rate">
													<input type="text" class="form-control text-center" value="" />
												</td>
												<td class="item_act text-center">
													<button class="btn btn-default btn-xs" onclick="add_row(this);"> <i class="fa fa-plus"></i> </button>
													<button class="btn btn-danger btn-xs"  onclick="delete_row(this);"> <i class="fa fa-times"></i> </button>
												</td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
								<div class="clear"></div>

								<div class="pull-left mt-20">
									<?php if($id > 0){ ?>
										<?php $po_base64 = base64_encode($demand_details['PO_NUMBER']);  ?>
										<?php if(in_array('demand-modify',$permissionz) || $admin == true){ ?>
										<a href="<?php echo $fileName; ?>?mode=form" target="_blank" class="btn btn-default">New Form</a>
										<?php } ?>
									<?php } ?>
									</div>

									<div class="pull-right pt-20">
										<?php if($demand_details != NULL){ ?>
											<a href="print-demand-list.php?id=<?php echo $demand_details['ID']; ?>" target="_blank" class="btn btn-info mr-20">Print</a>
											<?php } ?>
											<?php
													if(in_array('demand-modify',$permissionz) || $admin == true){
											?>
												<button type="button" class="btn btn-info save_demand_list"><?php echo ($id > 0)?"Update":"Save"; ?></button>
											<?php
													}
											?>
											</div>
										</div>
									</div>
									<div class="hide">
										<select class="item_list_def show-tick form-control" data-style="btn btn-default" data-live-search="true" name="item_id">
											<option selected value=""></option>
											<?php
											if(mysql_num_rows($itemsCategoryList)){
												while($ItemCat = mysql_fetch_array($itemsCategoryList)){
													$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
													?>
													<optgroup label="<?php echo $ItemCat['NAME']; ?>">
														<?php
														if(mysql_num_rows($itemList)){
															while($theItem = mysql_fetch_array($itemList)){
																if($theItem['ACTIVE'] == 'N'){
																	continue;
																}
																if($theItem['INV_TYPE'] == 'B'){
																	continue;
																}
																?>
																<option data-subtext="<?=$theItem['ITEM_BARCODE']?>" value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
																<?php
															}
														}
														?>
													</optgroup>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<?php
							}
							if($mode == 'search'){
								?>
								<div id="bodyTab1">
									<div id="form">
										<form method="get" action="">
											<div class="caption">From Date :</div>
											<div class="field">
												<input type="text" class="form-control datepicker" name="from_date" />
											</div>
											<div class="clear"></div>

											<div class="caption">To Date :</div>
											<div class="field">
												<input type="text" class="form-control datepicker" name="to_date" />
											</div>
											<div class="clear"></div>

											<div class="caption">Supplier</div>
											<div class="field">
												<select name="supplier_acc_code" class="form-control" data-live-search="true" >
													<option value=""></option>
													<?php
														if(mysql_num_rows($suppliers_list)){
															while($supplier = mysql_fetch_assoc($suppliers_list)){
																?><option value="<?php echo $supplier['ACC_CODE'] ?>"><?php echo $supplier['ACC_TITLE'] ?></option><?php
															}
														}
													?>
												</select>
											</div>
											<div class="clear"></div>

											<div class="caption">PO #</div>
											<div class="field">
												<input type="text" class="form-control" name="po_number" />
											</div>
											<div class="clear"></div>

											<div class="caption"></div>
											<div class="field">
												<input type="submit" name="search" value="Search" class="btn btn-info" />
											</div>
											<div class="clear"></div>
										</form>
									</div>
								</div>
								<?php
							}
							if($mode == 'list'){
								?>
								<div id = "bodyTab1">
									<table style="width:100%" class="table-hover table">
										<thead>
											<tr>
												<th width="5%" style="text-align:center">  Sr No.</th>
												<th width="15%" style="text-align:center"> Supplier </th>
												<th width="5%" style="text-align:center">  PO # </th>
												<th width="10%" style="text-align:center"> EntryDate </th>
												<th width="10%" style="text-align:center"> Expected End Date </th>
												<th width="10%" style="text-align:center"> Action </th>
											</tr>
										</thead>
										<tbody>
											<?php
											if(mysql_num_rows($demand_list)){
												$counter = 1;
												while($demand = mysql_fetch_assoc($demand_list)){
													?>
													<tr id="recordPanel">
														<td class="text-center"><?php echo $counter; ?></td>
														<td class="text-center"><?php echo $objChartOfAccounts->getAccountTitleByCode($demand['SUPPLIER_ACC_CODE']); ?></td>
														<td class="text-center"><?php echo $demand['PO_NUMBER']; ?></td>
														<td class="text-center"><?php echo date("d-m-Y",strtotime($demand['ENTRY_DATE'])); ?></td>
														<td class="text-center"><?php echo date("d-m-Y",strtotime($demand['REQUIRED_DATE'])); ?></td>
														<td class="text-center">
															<a id="view_button" href="<?php echo $fileName ?>?mode=form&id=<?php echo $demand['ID']; ?>"><i class="fa fa-pencil"></i></a>
															<a id="view_button" href="print-demand-list.php?id=<?php echo $demand['ID']; ?>" target="_blank"><i class="fa fa-print"></i></a>
															<?php if($admin){ ?>
																<a class="pointer" onclick="delete_demand_list(this);"  value="<?php echo $demand['ID']; ?>"><i class="fa fa-times"></i></a>
																<?php } ?>
															</td>
														</tr>
														<?php
														$counter++;
													}
												}
												else{
													?>
													<tr id="recordPanel">
														<td style="text-align:center" colspan="6"> No Record Found!</td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
										<div class="col-xs-12 text-center">
											<?php
											//if(!isset($_GET['search'])){
											if(true){
												$get_url = "";
												foreach($_GET as $key => $value){
													$get_url .= ($key == 'page')?"":"&".$key."=".$value;
												}
												?>
												<nav>
													<ul class="pagination">
														<?php
														$count = $found_records;
														$total_pages = ceil($count/$total);
														$i = 1;
														$thisFileName = $_SERVER['PHP_SELF'];
														if(isset($this_page) && $this_page>0){
															?>
															<li>
																<a href="<?php echo $thisFileName; ?>?page=1<?php echo $get_url; ?>">First</a>
															</li>
															<?php
														}
														if(isset($this_page) && $this_page>=1){
															$prev = $this_page;
															?>
															<li>
																<a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?><?php echo $get_url; ?>">Prev</a>
															</li>
															<?php
														}
														$this_page_act = $this_page;
														$this_page_act++;
														while($total_pages>=$i){
															$left = $this_page_act-5;
															$right = $this_page_act+5;
															if($left<=$i && $i<=$right){
																$current_page = ($i == $this_page_act)?"active":"";
																?>
																<li class="<?php echo $current_page; ?>">
																	<?php echo "<a href=\"".$thisFileName."?".$get_url."&page=".$i."\">".$i."</a>"; ?>
																</li>
																<?php
															}
															$i++;
														}
														$this_page++;
														if(isset($this_page) && $this_page<$total_pages){
															$next = $this_page;
															echo " <li><a href='".$thisFileName."?".$get_url."&page=".++$next."'>Next</a></li>";
														}
														if(isset($this_page) && $this_page<$total_pages){
															?>
															<li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?><?php echo $get_url; ?>">Last</a></li>
														</ul>
													</nav>
													<?php
												}
											}
											?>
										</div>
									</div> <!--bodyTab1-->
									<?php
								}
								?>
								<div class="clear" style="height:30px"></div>
							</div><!-- End summer -->
						</div><!-- End .content-box-top-->
					</div><!--bodyWrapper-->
					<div id="confirmation" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Confirmation</h4>
								</div>
								<div class="modal-body">
									<p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
									<button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

					<div id="information" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Information</h4>
								</div>
								<div class="modal-body">
									<p class="text-primary" style="font-weight: bold;"></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' name="cancel">Close</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</body>
				</html>
				<?php if(isset($ob_var)){ ob_end_flush($ob_var); } ?>
				<?php include("conn.close.php"); ?>
