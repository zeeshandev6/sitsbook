<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include 'common/config.php';
	include ('common/classes/items.php');
	include ('common/classes/services.php');
	include ('common/classes/ordering.php');
	include ('common/classes/rental_booking.php');

	//Permission
	if(!in_array('rental-booking',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objItems 			  = new Items();
	$objServices 			= new Services();
	$objRentalBooking = new RentalBooking();
	$objOrdering      = new Ordering();

	if(isset($_POST['deliver'])){
		$rbd_id = (int)mysql_real_escape_string($_POST['rbd_id']);

		$objRentalBooking->received_by 			 = mysql_real_escape_string($_POST['received_by']);
		$objRentalBooking->received_by_date  = date('Y-m-d H:i:s',strtotime($_POST['received_date']));
		$objRentalBooking->received_by_notes = mysql_real_escape_string($_POST['received_by_notes']);
		$objRentalBooking->deliver_status 	 = "Y";

		$objRentalBooking->deliverOrderUpdate($rbd_id);

		header("location:rental-booking-deliveries.php");
		exit();
	}

	$rbd_id = 0;
	if(isset($_GET['id'])){
		$rbd_id = (int)mysql_real_escape_string($_GET['id']);
		$booking_detail_detail = $objRentalBooking->getDetailDetail($rbd_id);
		$booking_detail        = $objRentalBooking->getDetail($booking_detail_detail['RENTAL_BOOKING_ID']);
	}else{
		$rentalList 		  = $objRentalBooking->getUndeliveredCompletedOrders();
	}
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" type="text/css" />
	<style>
	td{
		padding: 10px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".selectpicker").selectpicker();
			$("input[name=received_date]").datetimepicker({
				format: "dd-mm-yyyy HH:ii P",
				weekStart: 1,
        todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
        showMeridian: 1
			});
		});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p> Booking Delivery Management </p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<?php if(isset($rentalList)){ ?>
				<div id="bodyTab1" style="display:block;">
					<table width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th style="text-align:center;width:5%;">Booking #</th>
								<th style="text-align:center;width:20%;">Date</th>
								<th style="text-align:center;width:20%;">ClientInfo</th>
								<th style="text-align:center;width:20%;">Item</th>
								<th style="text-align:center;width:10%;">Quantity</th>
								<th style="text-align:center;width:10%;">Action</th>
							</tr>
						</thead>
						<tbody>
									<?php
									$counter = 1;
									if(mysql_num_rows($rentalList)){
										while($rentalRow = mysql_fetch_array($rentalList)){
											if($rentalRow['ITEM_ID']>0){
												$item_name 		 = $objItems->getItemTitle($rentalRow['ITEM_ID']);
											}else{
												$item_name 		 = $objServices->getTitle($rentalRow['SERVICE_ID']);
											}
											$bookingDetail = $objRentalBooking->getDetail($rentalRow['RENTAL_BOOKING_ID']);
											?>
											<tr>
												<td class="text-center"><?php echo $bookingDetail['BOOKING_ORDER_NO']; ?></td>
												<td class="text-left">
													<b>Booking Date : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['BOOKING_DATE_TIME'])); ?>
													<br />
													<b>Delivery Time : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['DELIVERY_DATE_TIME'])); ?>
													<br />
													<b>Return Time : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['RETURNING_DATE_TIME'])); ?>
												</td>
												<td class="text-left">
														<b>Name : </b>	<?php echo $bookingDetail['CLIENT_NAME']; ?>
														<br />
														<b>Mobile : </b>	<?php echo $bookingDetail['CLIENT_MOBILE']; ?>
														<br />
														<b>Total Price : </b>	<?php echo $bookingDetail['TOTAL_PRICE']; ?>
														<br />
												</td>
												<td class="text-center"><?php echo $item_name; ?></td>
												<td class="text-center"><?php echo $rentalRow['QUANTITY']; ?></td>
												<td class="text-center">
													<a href="rental-booking-deliveries.php?id=<?php echo $rentalRow['ID']; ?>" id="view_button"> <i class="fa fa-send"></i> </a>
												</td>
											</tr>
											<?php
										}
									}
									?>
									</tbody>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab1-->
							<?php }

							if(isset($booking_detail)){ ?>
								<div id="bodyTab1" >
									<div id="form">
										<form method="post" action="">
											<div class="title" style="font-size:20px; margin-bottom:20px">
												<a href="rental-booking-deliveries.php" class="btn btn-default"> <i class="fa fa-list"></i> Delivery List</a>
											</div>
											<div class="caption" style="font-size: 18px !important;">Customer Details</div>
											<div class="field" style="padding-top:  5px !important;">
												<label for="" style="color:#000;" > Client Name :     <span style="font-weight: normal !important;"><?php echo $booking_detail['CLIENT_NAME']; ?></span> </label>
												<label for="" style="color:#000;" > Client Mobile :   <span style="font-weight: normal !important;"><?php echo $booking_detail['CLIENT_MOBILE']; ?></span></label>
												<label for="" style="color:#000;" > Client Phone :    <span style="font-weight: normal !important;"><?php echo $booking_detail['CLIENT_PHONE']; ?></span></label>
												<label for="" style="color:#000;" > Client Address :  <span style="font-weight: normal !important;"> <?php echo $booking_detail['ADDRESS']; ?></span></label>
											</div>
											<div class="clear"></div>

											<input type="hidden" name="rbd_id" value="<?php echo $rbd_id ?>" />
											<div class="caption">Received By :</div>
											<div class="field">
												<input type="text" value="<?php echo $booking_detail_detail['RECEIVED_BY']; ?>" name="received_by" class="form-control" autofocus />
											</div>
											<div class="clear"></div>

											<div class="caption">Receiving Date :</div>
											<div class="field">
												<input type="text" value="<?php echo ($booking_detail_detail['RECEIVED_BY_DATE']=='0000-00-00 00:00:00')?"":date('d-m-Y h:i A',strtotime($booking_detail_detail['RECEIVED_BY_DATE'])); ?>" name="received_date" class="form-control" />
											</div>
											<div class="clear"></div>

											<div class="caption"> Notes :</div>
											<div class="field">
												<textarea name="received_by_notes" rows="8" cols="40" class="form-control"><?php echo $booking_detail_detail['RECEIVED_BY_NOTES']; ?></textarea>
											</div>
											<div class="clear"></div>

											<div class="caption"></div>
											<div class="field">
												<input type="submit" value="Save" name="deliver" class="btn btn-primary"/>
											</div>
											<div class="clear"></div>
										</form>
									</div><!--form-->
								</div> <!-- End bodyTab2 -->
							<?php } ?>
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->
				</div><!--body-wrapper-->
				<div id="xfade"></div>
				<div id="fade"></div>
</body>
</html>
			<?php include('conn.close.php'); ?>
			<script>
			$(document).ready(function() {
				$(".shownCode,.loader").hide();
				$("input.supplierTitle").keyup(function(){
					$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
				});
				$(window).keydown(function(e){
					if(e.keyCode==113){
						e.preventDefault();
						window.location.href = "<?php echo "inventory-details.php"; ?>";
					}
				});
			});
			$(function(){
				$("#fromDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
				$("#toDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>tab('2', '1', '2');<?php } ?>

			</script>
