<?php
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/emb_products.php');

    $objEmbProducts  = new EmbProducts();

    if(isset($_POST['delete_id'])){
        $delete_id =  mysql_real_escape_string($_POST['delete_id']);
        $deleted = $objEmbProducts->delete($delete_id);
        exit();
    }
    $mode = "";
    if(!isset($_GET['search'])){
        $path = "emb-product-management.php?title=&status=&search=Search";
        if(isset($_GET['tab'])){
            $path .= "&tab=search";
        }
        echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        exit();
    }
    if(isset($_GET['search'])){
        $end = 20;
        $objEmbProducts->title    = mysql_real_escape_string($_GET['title']);
        $objEmbProducts->status   = mysql_real_escape_string($_GET['status']);
        $myPage = 1;
        $start  = 0;
        if(isset($_GET['page'])){
            $page   = $_GET['page'];
            $myPage = $page;
            $start  = ((int)$page * $end) - $end;
        }
        $machine_list = $objEmbProducts->search($start,$end);
        $row          = mysql_num_rows($machine_list);
        $queryCount   = $objEmbProducts->totalMatchRecords;
        $pageCount    = ceil($queryCount/$end);
    }else{
        $machine_list = $objEmbProducts->getList();
        $row          = mysql_num_rows($machine_list);
    }
?>
<!DOCTYPE html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
        <link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
        <link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
        <link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
        <link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
        <link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
        <link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
        <style media="screen">
          table th{
            font-weight: bold !important;
          }
          table th,table td{
            padding: 10px !important;
          }
        </style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
            //delete product
            function deleteMachine(x){
                $("#myConfirm").modal('show');
                $(document).on('click', 'button#delete', function(){
                    var delete_id = $(x).attr("value");
                    var del = $.post("emb-product-management.php", {delete_id:delete_id}, function(data){});
                    if(del){
                        $(x).parent().parent().remove();
                    }
                });
                $(document).on('click', 'button#cancell', function(){
                    x = null;
                });
            }
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class="content-box-header">
                    <p ><B>Embroidery Products Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span> </p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                                <a href="emb-product-details.php"><div class="tab">New</div></a>
                            </div>
                        </span>
                    <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>
                <div id = "bodyTab1">
                    <table style="width:100%" class="table-hover"  >
                        <thead>
                        <tr>
                            <th width="10%" style="text-align:center">Sr No.</th>
                            <th width="10%" style="text-align:center">Code</th>
                            <th width="20%" style="text-align:center">Title</th>
                            <th width="50%" style="text-align:center">Description</th>
                            <th width="10%" style="text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($row>0){
                            $counter = 1;
                            while($machine_row = mysql_fetch_assoc($machine_list)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $machine_row['PROD_CODE']; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $machine_row['TITLE']; ?></td>
                                    <td class="measure_type-list"><?php echo $machine_row['DESCRIPTION']; ?></td>
                                    <td style="text-align:center">
                                        <a href="emb-product-details.php?id=<?php echo $machine_row['ID']; ?>">
                                            <input type="button" value="View" id="view_button" title="View/Update" />
                                        </a>
                                        <a onClick="deleteMachine(this);" id="del" value="<?php echo $machine_row['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times" ></i></a>
                                    </td>
                                </tr>
                            <?php
                                $counter++;
                            }
                        }
                        else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php
                    if(isset($queryCount)){
                        if($queryCount>$pageCount){
                            ?>
                    <table align="center" border="0" id="navbar">
                        <tr>
                            <td style="text-align: center">
                                        <nav>
                                            <ul class="pagination">
                                                <?php
                                                if($myPage > 1)
                                                {
                                                    echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=1'>Fst</a></li>";
                                                    echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage-1) ."'>Prv</a></li>";
                                                }

                                                for($x=$myPage-4; $x<$myPage; $x++)
                                                {
                                                    if($x > 0){
                                                        echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $x ."'>". $x ."</a></li>";
                                                    }
                                                }

                                                print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                                                for($y=$myPage+1; $y<$myPage+5; $y++)
                                                {
                                                    if($y <= $pageCount){
                                                        echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $y ."'>". $y ."</a></li>";
                                                    }
                                                }

                                                if($myPage < $pageCount)
                                                {
                                                    echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage+1) ."'>Nxt</a></li>";
                                                    echo "<li><a href='emb-product-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $pageCount ."'>Lst</a></li>";
                                                }

                                                ?>
                                            </ul>
                                        </nav>
                            </td>
                        </tr>
                    </table>
                    <?php
                  }
                }
                ?>
                </div> <!--bodyTab1-->
                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Measure Type :</div>

                            <div class="caption">Measure Type Title :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="title" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Status :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="status" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption"></div>
                            <div class = "field">
                                <input type="submit" name="search" value="Search" class="button" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab1-->
                <div class="clear"></div>
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
    </div><!--bodyWrapper-->
    <!-- Delete confirmation popup -->
    <div id="myConfirm" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </body>
    </html>
<?php include('conn.close.php'); ?>
