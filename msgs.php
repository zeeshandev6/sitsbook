<?php
function linkBootstrap(){
  echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
	echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
	echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
}
function errorMessage($msg){
    ?>
    <div id="msg" class="alert alert-danger" style="margin: 20px;">
        <button type="button" class="close">&times;</button>
        <span style="font-weight: bold;">Error! </span><?php echo $msg; ?>
    </div>
    <?php
}

function warningMessage($msg){
    ?>
    <div id="msg" class="alert alert-warning" style="margin: 20px;">
        <button type="button" class="close">&times;</button>
        <span style="font-weight: bold;">Warning! </span><?php echo $msg; ?>
    </div>
<?php
}

function successMessage($msg){
    ?>
    <div id="msg" class="alert alert-success" style="margin: 20px;">
        <button type="button" class="close">&times;</button>
        <span style="font-weight: bold;">Success! </span><?php echo $msg; ?>
    </div>
<?php
}


function infoMessage($msg){
    ?>
    <div id="msg" class="alert alert-info" style="margin: 20px;">
        <button type="button" class="close">&times;</button>
        <span style="font-weight: bold;">Note! </span><?php echo $msg;  ?>
    </div>
<?php
}

?>
