<?php
	$out_buffer = ob_start();
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/items.php';
	include 'common/classes/itemCategory.php';
	include 'common/classes/godown.php';
	include 'common/classes/godown-details.php';

	$objItems  		  = new Items();
	$objItemCategories= new itemCategory();
	$objGodown  	  = new Godown();
	$objGodownDetails = new GodownDetails();

	$godown = NULL;
	$id = 0;

	if(isset($_GET['mode'])){
		$mode = mysql_real_escape_string($_GET['mode']);
		if($mode == 'list'){
			$mode_id = 1;
		}elseif($mode == 'add'){
			$mode_id = 2;
		}elseif($mode == 'shift'){
			$mode_id = 3;
		}
	}else{
		exit(header('location:godown-management.php?mode=list'));
	}

	if(isset($_POST['shift'])){
		$item_id 	 = mysql_real_escape_string($_POST['item_id']);
		$from_godown = mysql_real_escape_string($_POST['from_godown']);
		$to_godown   = mysql_real_escape_string($_POST['to_godown']);
		$item_qty    = mysql_real_escape_string($_POST['quantity']);



		$item_from_godown = $objGodownDetails->getDetails($item_id,$from_godown);
		$item_to_godown   = $objGodownDetails->getDetails($item_id,$to_godown);
		$to_stock_added   = false; // stock not yet shifted to "to godown"
		$from_stock_removed = false;

		if(($item_from_godown||$from_godown == 0)&&$item_qty!=0){
			if($item_to_godown==NULL&&$to_godown>0){
				$objGodownDetails->godown_id = $to_godown;
				$objGodownDetails->item_id   = $item_id;
				$objGodownDetails->quantity  = $item_qty;

				$to_stock_added = $objGodownDetails->save();
			}elseif($item_to_godown!=NULL){
				$to_stock_added = $objGodownDetails->addStock($item_id, $to_godown, $item_qty);
			}elseif($to_godown == 0){
				$to_stock_added = $objItems->addStock($item_id, $item_qty);
			}

			if($to_stock_added){
				if($from_godown == 0){
					$from_stock_removed = $objItems->removeStock($item_id, $item_qty);
				}
				if($from_godown > 0){
					$from_stock_removed = $objGodownDetails->removeStock($item_id, $from_godown, $item_qty);
				}
			}

			if($from_stock_removed&&$to_stock_added){
				exit(header('location:godown-management.php?mode=shift&shift-success'));
			}else{
				exit(header('location:godown-management.php?mode=shift&shift-error'));
			}

		}
	}


	if(isset($_POST['save'])){
		$objGodown->title       = mysql_real_escape_string($_POST['title']);
		$objGodown->location    = mysql_real_escape_string($_POST['location']);
		$objGodown->description = mysql_real_escape_string($_POST['description']);

		$id = mysql_real_escape_string($_POST['id']);
		if($id > 0){
			$updated = $objGodown->update($id);
			if($updated){
				$message = "Record Updated Successfully!";
			}else{
				$error_message = "Error! updating Record.";
			}
		}else{
			$saved   = $objGodown->save();
			if($saved){
				$message = "New Godown (".$objGodown->title.") Added Successfully!";
			}else{
				$error_message = "Error! Saving Record.";
			}
		}
	}

	if(isset($_GET['shift-success'])){
		$shift_message = "Stock Shifted Successfully!";
	}

	if(isset($_GET['shift-error'])){
		$shift_message_error = "Error! Cannot Shift Stock";
	}

	if(isset($_GET['id'])){
		$id = mysql_real_escape_string($_GET['id']);
		$godown = $objGodown->getDetails($id);
	}

	$godownList 	  = $objGodown->getList();
	$godown_array     = array();
?>
<!DOCTYPE html 
   >
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="resource/css/bootstrap-select.css" rel="stylesheet">

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/godown.config.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
        		tab($("input.mode_id").val(), '1', '3');
        		$("select").selectpicker();
        		$("select[name=item_id],select[name=from_godown],select[name=to_godown]").change(function(){
        			getItemDetailsForGodown();
        		});
        		$("input[name=quantity]").keyup(function(){
        			shift_quantity_from_to_godown();
        		});
        	});
        </script>
   </head>
<body>
	<div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div id = "bodyWrapper">
            <div class = "content-box-top" style="overflow:visible;">
                <div class="summery_body">
                    <div class = "content-box-header">
                        <p >Godown Management</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                            	<input type="hidden" value="<?php echo $mode_id; ?>" class="mode_id" />
                                <div class="tabSelected" id="tab1" onClick="tab('1', '1', '3');">List</div>
                                <div class="tab" id="tab2" onClick="tab('2', '1', '3');">Add</div>
                                <div class="tab" id="tab3" onClick="tab('3', '1', '3');">Stock Shifting</div>
                            </div>
                        </span>
                        <div class="clear"></div>
                    </div><!-- End .content-box-header -->
                    <div class="clear"></div>
                    <div id="bodyTab1">
                    	<table cellspacing="0" align="center">
                         <thead>
                            <tr>
                               <th width="20%" style="text-align:center">Godown Name</th>
                               <th width="20%" style="text-align:center">Location</th>
                               <th width="20%" style="text-align:center">Description</th>
                               <th width="10%" style="text-align:center">Total Stock</th>
                               <th width="5%" style="text-align:center">Action</th>
                            </tr>
                         </thead>

                         <tbody>
                            <tr class="transactions" style="display:none;"></tr>
<?php
				$totalStock = 0;
                if(mysql_num_rows($godownList)){
                	while($row = mysql_fetch_array($godownList)){
                		$godown_array[] = $row;
                		$stock = $objGodownDetails->getGodownStock($row['ID']);
?>
                            <tr>
                                <td style="text-align:center;"><?php echo $row['TITLE']; ?></td>
                                <td style="text-align:center;"><?php echo $row['LOCATION']; ?></td>
                                <td style="text-align:center;"><?php echo $row['DESCRIPTION']; ?></td>
                                <td style="text-align:center;"><?php echo $stock; ?></td>
                                <td style="text-align:center;">
                                	<a class="button btn-xs" href="godown-management.php?id=<?php echo $row['ID']; ?>&mode=add">View</a>
                                </td>
                            </tr>
<?php
						$totalStock += $stock;
                	}
                }
?>
                         </tbody>
                         <tfoot>
                         	<tr>
                         		<td class="text-right" colspan="3">Total</td>
                         		<td class="text-center"><?php echo $totalStock; ?></td>
                         		<td class="text-center"> - - - </td>
                         	</tr>
                         </tfoot>
                        </table>
                    </div><!--bodyTab1-->
                    <div id="bodyTab2" style="display: none;">
                    	<div id="form">
                    		<?php if(isset($message)){ ?>
                    			<div class="alert alert-danger alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								  <?php echo $message; ?>
								</div>
                    		<?php } ?>
                    		<?php if(isset($error_message)){ ?>
                    			<div class="alert alert-success alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								  <?php echo $error_message; ?>
								</div>
                    		<?php } ?>
                            <form method="post" action="" class="the-form">
                            <div class = "caption" style="width:100px;">Title : </div>
                            <div class = "field">
                               <input class="form-control" name="title" type="text" value="<?php echo $godown['TITLE']; ?>" style="width:200px;" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption" style="width:100px;">Location : </div>
                            <div class = "field">
                               <input class="form-control" name="location" type="text" value="<?php echo $godown['LOCATION']; ?>" style="width:200px;" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:100px;">Description : </div>
                            <div class="field">
                               <input class="form-control" name="description" type="text" value="<?php echo $godown['DESCRIPTION']; ?>" style="width:200px;" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption" style="width:100px;"></div>
                            <div class="field">
                            	<input type="hidden" name="id" value="<?php echo $id; ?>" />
                            	<input type="submit" name="save" class="button" value="<?php echo ($id > 0)?"Update":"Save"; ?>" />
                            	<?php if($id>0){ ?>
                            		<a class="button" href="godown-management.php?mode=add">New</a>
                            	<?php } ?>
                            </div>
                            <div style = "clear:both; height:10px;"></div>
                            </form>
                        </div><!--#form-->
                    </div><!--bodyTab2-->
                    <div id="bodyTab3" style="display: none;">
                    	<form method="post" action="">
                    	<div id="form">
                    		<?php if(isset($shift_message)){ ?>
                    			<div class="alert alert-success alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								  <?php echo $shift_message; ?>
								</div>
                    		<?php } ?>
                    		<?php if(isset($shift_message_error)){ ?>
                    			<div class="alert alert-danger alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								  <?php echo $shift_message_error; ?>
								</div>
                    		<?php } ?>
                    		<div class="caption" style="width:100px;">Item</div>
                            <div class="field">
                               <select class="form-control" name="item_id">
                               		<option value=""></option>
<?php
							$itemsList = $objItems->getActiveList();
							if($itemsList){
								while($item = mysql_fetch_assoc($itemsList)){
									$category_name = $objItemCategories->getTitle($item['ITEM_CATG_ID']);
									if($item['INV_TYPE'] == 'B'){
										continue;
									}
?>
									<option value="<?php echo $item['ID']; ?>" data-subtext="<?php echo $category_name; ?>"><?php echo $item['NAME']; ?></option>
<?php
								}
							}
?>
                               </select>
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:100px;">From Godown</div>
                            <div class="field">
                               <select class="form-control" name="from_godown">
                               	<option value="0">Main Godown</option>
<?php
							foreach($godown_array as $key => $godown_row){
?>
									<option value="<?php echo $godown_row['ID']; ?>" data-subtext="<?php echo $category_name; ?>"><?php echo $godown_row['TITLE']; ?></option>
<?php
							}
?>
                               </select>
                            </div>
                            <div class="caption">
                            	<input type="text" class="form-control from_godown_qty" readonly="readonly" style="width: 100px;" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:100px;">To Godown</div>
                            <div class="field">
                               <select class="form-control" name="to_godown">
                               		<option value="0">Main Godown</option>
<?php
							foreach($godown_array as $key => $godown_row){
?>
									<option value="<?php echo $godown_row['ID']; ?>" data-subtext="<?php echo $category_name; ?>"><?php echo $godown_row['TITLE']; ?></option>
<?php
							}
?>
                               </select>
                            </div>
                            <div class="caption">
                            	<input type="text" class="form-control to_godown_qty" readonly="readonly" style="width: 100px;" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption" style="width:100px;">Quantity</div>
                            <div class="field">
                            	<input type="text" class="form-control" name="quantity" value="0" />
                            </div>
                            <div class="clear"></div>


                            <div class="caption" style="width:100px;"></div>
                            <div class="field">
                            	<input type="submit" name="shift" class="button" value="Shift" />
                            </div>
                            <div class="clear"></div>

                    	</div><!--#form-->
                    	</form><!--form-->
                    </div><!--bodyTab3-->
                </div><!--summery_body-->
				<div style="clear:both;"></div>
            </div><!-- End .content-box-top -->
        </div><!-- End summer -->
	</div><!--body-wrapper-->
    <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
