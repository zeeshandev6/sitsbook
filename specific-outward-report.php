<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_outward.php');
	include('common/classes/machines.php');
	include('common/classes/measure.php');
	include('common/classes/emb_stitch_account.php');
	include('common/classes/machine_sales.php');
	include('common/classes/emb_products.php');
	include('common/classes/emb_inward.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/company_details.php');

	$objChartOfAccounts    = new ChartOfAccounts();
	$objOutwards           = new outward();
	$objInward             = new EmbroideryInward();
	$objEmbProducts        = new EmbProducts();
	$objMachine            = new machines();
	$objMeasure            = new Measures();
	$objStitch             = new EmbStitchAccount();
	$objMachineSales       = new machineSales();
	$objLotRegister        = new EmbLotRegister();
	$objCompanyDetails     = new CompanyDetails();

	$firstDayOfThisMonth = date('01-m-Y');

	$company_details  = $objCompanyDetails->getActiveProfile();

	$machineList   = $objMachine->getList();
  $customersList = $objChartOfAccounts->getAccountByCatAccCode('010104');

	if(isset($_POST['search'])){
		$objOutwards->fromDate 				= ($_POST['fromDate']=="")?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$objOutwards->toDate 					= ($_POST['toDate']=="")?"":date('Y-m-d',strtotime($_POST['toDate']));
		$objOutwards->customerAccCode = mysql_real_escape_string($_POST['custAccCode']);
		$objOutwards->billNum 				= mysql_real_escape_string($_POST['billNum']);
		$objOutwards->lotNum 					= mysql_real_escape_string($_POST['lotNum']);
		$objOutwards->designNum 			= mysql_real_escape_string($_POST['designNum']);
		if($objOutwards->customerAccCode!==''){
			if($objOutwards->fromDate==''){
				$thisYear = date('Y');
				$firstJanuary = '01-01';
				$firstJanuaryThisYear = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
				$objOutwards->fromDate = $firstJanuaryThisYear;
			}
			$report = $objOutwards->getSpecificOutwardIncBill();

			$objLotRegister->fromDate        = $objOutwards->fromDate;
			$objLotRegister->toDate          = $objOutwards->toDate;
			$objLotRegister->customerAccCode = $objOutwards->customerAccCode;
			$objLotRegister->lotNum          = $objOutwards->lotNum;
			$objLotRegister->designNum       = $objOutwards->designNum;
			$objLotRegister->lotStatus       = 'R';

			$claimReport = $objLotRegister->report();
		}else{
			$message = 'Party not Selected!';
		}
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
    <script type="text/javascript">
        $(window).on('load', function () {
            $('.selectpicker').selectpicker();
					$(".printThis").click(function(){
						var MaxHeight = 1400;
						var RunningHeight = 0;
						var PageNo = 1;
						var MaxHeight_after = 0;
						//Sum Table Rows (tr) height Count Number Of pages
						$('table.tableBreak>tbody>tr').each(function(){
							if(PageNo == 1){
								MaxHeight_after = 1300;
							}else{
								MaxHeight_after = 1400;
							}
							if (RunningHeight + $(this).height() > MaxHeight_after) {
								RunningHeight = 0;
								PageNo += 1;
							}
							RunningHeight += $(this).height();
							//store page number in attribute of tr
							$(this).attr("data-page-no", PageNo);
						});
						//Store Table thead/tfoot html to a variable
						var tableHeader = $(".tHeader").html();
						var tableFooter = $(".tableFooter").html();
						var repoDate    = $(".repoDate").text();
						//remove previous thead/tfoot/ReportDate
						$(".tHeader").remove();
						$(".repoDate").remove();
						$(".tableFooter").remove();
						//Append .tablePage Div containing Tables with data.
						for(i = 1; i <= PageNo; i++){
							$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
							//get trs by pagenumber stored in attribute
							var rows = $('table tr[data-page-no="' + i + '"]');
							$('#Table' + i).find("thead").append(tableHeader);
							$('#Table' + i).find("tbody").append(rows);

						}
						$(".newTable").last().append(tableFooter);
						$('table.tableBreak').remove();
						$(".printAble").printThis({
					      debug: false,
						  	importCSS: false,
						  	printContainer: true,
						  	loadCSS: 'resource/css/reports.css',
					      pageTitle: "Emque Embroidery",
					      removeInline: false,
					      printDelay: 400,
					      header: null
					  });
					});
        });
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Outward Reports</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="emb-outward-report.php"><div class="tab">Generic</div></a>
                        <a href="specific-outward-report.php"><div class="tabSelected">Specific</div></a>
                        <a href="cloth-stock-report.php"><div class="tab">Cloth Register</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="">
                        <div class="caption"></div><div class="redColor"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>
                        <div class="caption">Customer Title</div>
                        <div class="field" style="width:295px;">
                        	<select class="selectpicker show-tick form-control"  name='custAccCode'  data-live-search="true">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
?>
                               <option value="<?php echo $account['ACC_CODE']; ?>"><?php echo $account['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Start Date:</div>
                        <div class="field">
                            <input type="text" value="" name="fromDate" class="form-control datepicker" style="width:145px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">End Date:</div>
                        <div class="field">
                            <input type="text" value="<?php echo date('d-m-Y'); ?>" name="toDate" class="form-control datepicker" style="width:145px;" />
                        </div>
                        <div class="clear"></div>

												<div class="caption">GP No</div>
                        <div class="field">
                        	<input type="text" value="" name="billNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Lot No</div>
                        <div class="field">
                        	<input type="text" value="" name="lotNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Design</div>
                        <div class="field">
                        	<input type="text" value="" name="designNum" class="form-control" />
                        </div>
                        <div class="clear"></div>
                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button" id="hide"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
				<?php
          if(isset($_POST['search'])&&!isset($message)){
                ?>
<?php
		$reportRows = mysql_num_rows($report);
		if($reportRows){
			$prevCustomer = "";
			$grandLength = 0;
			$grandAmount = 0;
?>

        	<div class="content-box-content">
            	<span style="float:right;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printAble" style="width:900px;margin: 0 auto;">
                	<div style="text-align:center;margin-bottom:0px;" class="pageHeader">
											<p style="font-size: 18px;padding:0;text-align:left;margin:0px !important;"><b><?php echo $company_details['NAME']; ?></b></p>
                      <p style="font-size: 18px;padding:0;text-align:left;margin:0px !important;">Cloth Outward Register <?php echo $objChartOfAccounts->getAccountTitleByCode($objOutwards->customerAccCode); ?> ( <?php echo $objOutwards->customerAccCode; ?> )  </p>
                      <div class="clear"></div>
                      <p style="font-weight:bold;float:left;margin:0px !important;">From Period <?php echo ($objOutwards->fromDate=="")?"":date('d-m-Y',strtotime($objOutwards->fromDate)); ?> to Period <?php echo date('d-m-Y',strtotime($objOutwards->toDate)); ?></p>
                      <span style="float:right;margin:10px; font-size:14px;margin:0px !important;" class="repoDate">Report generated on: <?php echo date('d-m-Y'); ?></span>
                      <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                        <table style="margin:0; width:100%" class="tableBreak">
                            <thead class="tHeader">
                                 <tr style="background:#EEE;">
                                   <th width="5%"  class="text-center">Bill#</th>
	                               	 <th width="5%"  class="text-center">Lot#</th>
                                   <th width="8%"  class="text-center">OutdDate</th>
																	 <th width="8%"  class="text-center">3rdParty</th>
                                   <th width="15%" class="text-center">Product</th>
                                   <th width="5%"  class="text-center">Measure</th>
                                   <th width="15%" class="text-center">Quality</th>
                                   <th width="5%"  class="text-center">Design</th>
                                   <th width="5%"  class="text-center">Length</th>
                                   <th width="5%"  class="text-center">Emb@</th>
                                   <th width="5%"  class="text-center">Stitch@</th>
                                   <th width="10%" class="text-center">Amount</th>
                                   <th width="5%"  class="text-center">StockO/s</th>
                            	</tr>
                            </thead>
                            <tbody>
<?php
					$prevBillNo = '';
					while($detailRow = mysql_fetch_array($report)){
							$measureMent = $objMeasure->getName($detailRow['MEASURE_ID']);
							$stockInward = $objInward->getInwardLotStockInHandWhloleLot($detailRow['CUST_ACC_CODE'],$detailRow['LOT_NO']);
							$outwardStockDelivered = $objOutwards->getOutwardLotQtyDeliveredWholeLotPrevId($detailRow['CUST_ACC_CODE'],$detailRow['LOT_NO'],$detailRow['OUTWD_DATE'],$detailRow['OUTWD_DETL_ID']);
							$outwardStockOs = $stockInward - $outwardStockDelivered;
							if($prevBillNo != $detailRow['BILL_NO']){
								$thisBillNo = $detailRow['BILL_NO'];
							}else{
								$thisBillNo = '';
							}
?>
								<tr id="recordPanel" class="alt-row">
									<td class="text-center" ><?php echo $thisBillNo; ?></td>
									<td class="text-center" ><?php echo $detailRow['LOT_NO']; ?></td>
									<td class="text-center" ><?php echo date('d-m-Y',strtotime($detailRow['OUTWD_DATE'])); ?></td>
									<td ><?php echo $objChartOfAccounts->getAccountTitleByCode($detailRow['THIRD_PARTY_CODE']) ?></td>
									<td class="text-left" 	><?php echo $objEmbProducts->getTitle($detailRow['PRODUCT_ID']); ?></td>
									<td class="text-center" ><?php echo $measureMent; ?></td>
									<td class="text-center" ><?php echo $detailRow['QUALITY']; ?></td>
									<td class="text-center" ><?php echo $detailRow['DESIGN_CODE']; ?></td>
									<td class="text-center mLength"><?php echo $detailRow['MEASURE_LENGTH']; ?></td>
									<td class="text-center" ><?php echo $detailRow['EMB_RATE']; ?></td>
									<td class="text-center" ><?php echo $detailRow['STITCH_RATE']; ?></td>
									<td class="text-center eAmount"><?php echo $detailRow['EMB_AMOUNT']; ?></td>
									<td class="text-center" ><?php echo $outwardStockOs; ?></td>
								</tr>
<?php
						$grandLength += $detailRow['MEASURE_LENGTH'];
						$grandAmount += $detailRow['EMB_AMOUNT'];
						$prevBillNo = $detailRow['BILL_NO'];
						}
?>

<?php
						if(mysql_num_rows($claimReport)){
							while($claimRow = mysql_fetch_array($claimReport)){
								$gotBillNumber = $objLotRegister->getBillNumberFromOutwardClaim($claimRow['ID']);
								$mesureName = $objMeasure->getName($claimRow['MEASURE_ID']);
?>
								<tr id="recordPanel" class="alt-row hasTdReds">
									<td class="text-center" ><?php echo $gotBillNumber; ?></td>
									<td class="text-center" ><?php echo $claimRow['LOT_NO']; ?></td>
									<td class="text-center" ><?php echo date('d-m-Y',strtotime($claimRow['LOT_DATE'])); ?></td>
									<td class="text-left" ><?php echo $objEmbProducts->getTitle($claimRow['PRODUCT_ID']); ?></td>
									<td class="text-center" ><?php echo $mesureName; ?></td>
									<td class="text-center" ><?php echo $claimRow['QUALITY']; ?></td>
									<td class="text-center" ><?php echo $claimRow['DESIGN_CODE']; ?></td>
									<td class="text-center mLength" ><?php echo $claimRow['MEASURE_LENGTH']; ?></td>
									<td class="text-center" ><?php echo $claimRow['EMB_RATE']; ?></td>
									<td class="text-center" ><?php echo $claimRow['STITCH_RATE']; ?></td>
									<td class="text-center eAmount" ><?php echo $claimRow['EMB_AMOUNT']; ?></td>
									<td class="text-center" > - - - </td>
              	</tr>
<?php
								if($claimRow['EMB_RATE'] < 0 && $claimRow['MEASURE_LENGTH'] > 0){
									$grandAmount += $claimRow['EMB_AMOUNT'];
								}elseif($claimRow['MEASURE_LENGTH'] < 0 && $claimRow['EMB_RATE'] > 0){
									$grandLength += $claimRow['MEASURE_LENGTH'];
									$grandAmount += $claimRow['EMB_AMOUNT'];
								}elseif($claimRow['EMB_RATE'] < 0 ){
									$grandAmount += $claimRow['EMB_AMOUNT'];
								}
							}
						}
?>

                            </tbody>
                            <tfoot class="tableFooter">
                                <tr>
                                    <td style="text-align:right;" colspan="7">Total</td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo $grandLength; ?></td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo $grandAmount; ?></td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                </tr>
                            </tfoot>
						</table>
<?php
		}
?>
                    <div class="clear;" style="height:20px"></div>
                </div> <!--End bodyTab-->
<?php
	}
?>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script type="text/javascript">
	$(window).load(function(){
		$(".hasTdReds").find('td').css('color','red');
		$(".dropdown-toggle").focus();
<?php
			if(isset($_GET['tab'])&&$_GET['tab']=='general'){
?>

<?php
			}
			if(isset($_GET['tab'])&&$_GET['tab']=='specific'){
?>

<?php
			}
		?>
	});
</script>
