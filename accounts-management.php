<?php
	$out_buffer = ob_start();
	include("common/connection.php");
	include("common/config.php");
	include("common/classes/accounts.php");
	include('common/classes/payrolls.php');
	include('common/classes/banks.php');

	if(!in_array('coa-managment',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}

	$objChartOfAccounts   = new ChartOfAccounts();
	$objPayrolls 	 				= new Payrolls();
	$objBanks         		= new Banks();

	if(isset($_POST['addAccount'])){
			$general 		= mysql_real_escape_string($_POST['general']);
			$main 			= mysql_real_escape_string($_POST['main']);
			$submain 		= mysql_real_escape_string($_POST['subMain']);
			$title 			= mysql_real_escape_string($_POST['title']);

			$this_len = 0;
			if($general==00 && $title !== ""){
				//$objChartOfAccounts->addGeneral($title);
				$message = 'Chart Of Accounts Is Limited To 5 General Categories.';
			}elseif($main==00 && $title !== ""){
				$objChartOfAccounts->addMain($title,$general);
				$this_len = 2;
			}elseif($submain==00 && $title !== ""){
				$objChartOfAccounts->addSubMain($title,$main);
				$this_len = 4;
			}elseif($title !== "" && $submain !== 00){
				$objChartOfAccounts->addSubMainChild($title,$submain);
				$this_len = 6;
			}

			$newAccountId 			= mysql_insert_id();
			$accountDetailArray = mysql_fetch_array($objChartOfAccounts->getAccountCodeById($newAccountId));

			if(strlen($accountDetailArray['ACC_CODE'])>6){
				if(substr($accountDetailArray['ACC_CODE'],0,6)=='010104'){  //customers
					$customer_id = $objChartOfAccounts->creatCustomer($accountDetailArray['ACC_CODE'],$accountDetailArray['ACC_TITLE']);
					//header('location:customer-detail.php?cid='.$customer_id);
					exit();
				}
				if(substr($accountDetailArray['ACC_CODE'],0,6)=='040101'){  //Suppliers
					$supplier_id = $objChartOfAccounts->creatSupplier($accountDetailArray['ACC_CODE'],$accountDetailArray['ACC_TITLE']);
					//header('location:supplier-details.php?cid='.$supplier_id);
					exit();
				}
				if(substr($accountDetailArray['ACC_CODE'],0,6)=='010102'){  //Banks
					$objBanks->account_title = $accountDetailArray['ACC_TITLE'];
					$objBanks->account_code  = $accountDetailArray['ACC_CODE'];
					$bank_id 								 = $objBanks->save();
					//header('location:add-bank-detail.php?id='.$bank_id);
					exit();
				}
				if(substr($accountDetailArray['ACC_CODE'],0,6)=='030105'){  //Payrolls
					$objPayrolls->account_code  = $accountDetailArray['ACC_CODE'];
					$objPayrolls->account_title = $accountDetailArray['ACC_TITLE'];
					$payroll_id 								= $objPayrolls->save();
					//header('location:payroll-details.php?cid='.$payroll_id);
					exit();
				}
			}
			echo substr($accountDetailArray['ACC_CODE'],0,$this_len);
		exit();
	}

	if(isset($_GET['hrchi'])){
		$heirarchi = $_GET['hrchi'];
		if(strlen($heirarchi)>6){
			unset($heirarchi);
			$hrchi_general = '';
			$hrchi_main = '';
			$hrchi_sub_main = '';
		}else{
			if(strlen($heirarchi)==6){
				$hrchi_general = substr($heirarchi,0,2);
				$hrchi_main = substr($heirarchi,0,4);
				$hrchi_sub_main = substr($heirarchi,0,6);
			}
			if(strlen($heirarchi)==4){
				$hrchi_general = substr($heirarchi,0,2);
				$hrchi_main = substr($heirarchi,0,4);
				$hrchi_sub_main = '';
			}
			if(strlen($heirarchi)==2){
				$hrchi_general = substr($heirarchi,0,2);
				$hrchi_main = '';
				$hrchi_sub_main = '';
			}
		}
	}

	$accountList 				 = $objChartOfAccounts->getList();
	$account_codes_array = array();
	if(mysql_num_rows($accountList)){
		while($account_row = mysql_fetch_assoc($accountList)){
			$account_codes_array[] = $account_row;
		}
	}
	$array_exculde = '["01","0101","010101","0101010001","010102","010103","010104","010105","0101050001","010106","0101060001","0101060002","0101060003","0101060004","010107","010108","0101080001","0101080002","010109","010110","010111","010111001","010111002","010112","010112001","0102","010201","010202","010203","010204","010205","02","0201","020101","0201010001","0201010002","0201010003","020102","0201020001","020201","0202010001","03","0301","030101","0301010001","0301010003","030102","0301020001","030103","030104","0301040001","0301040002","030105","04","0401","040101","040102","040103","040104","0401040002","040105","040106","040107","040107","0402","040201","040202","05","0501","050101","050102","0501010001","0501020001"]';
	$array_exculde = json_decode($array_exculde);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>SIT Solutions</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
		<style>
			table td,
			table th {
				font-size: 1em !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script>
			$(document).ready(function(){

				$("body").on("click","*[data-target='#popup-form-modal']",function(){
					setTimeout(function(){
						$("select").selectpicker();
						$("#popup-form-modal input[type=text]").focus();
						$('form').ajaxForm({
								beforeSubmit: function(){
									$("input[type=submit]").prop("disabled",true);
								},
							success: function(code){
									if($.trim(code) == ''){
										displayMessage('Error! Cannot create account.');
									}else{
										displayMessage('Success! New Account has been created.');
									}
									$("input[type=submit]").prop("disabled",false);
									$.get("?",{},function(html){
										$("table tbody").replaceWith($(html).find("table tbody"));
										events();
									});
									return;
								}
						});
					},300);
				});
				$("table").on("click","a[data-target='#popup-form-modal']",function(){
					var herchi = $(this).attr("data-herchi");
					$("#popup-form-modal select").unbind('change');

					$("#popup-form-modal select[name='general'] option[value='']").prop("selected",true).parent().trigger('change');
					$("#popup-form-modal select[name='main'] 	option[value='']").prop("selected",true).parent().trigger('change');
					$("#popup-form-modal select[name='subMain'] option[value='']").prop("selected",true).parent().trigger('change');

					$("#popup-form-modal select[name='general'] option[value='"+herchi.substring(0,2)+"']").prop("selected",true).parent().trigger('change');
					$("#popup-form-modal select[name='main'] 	option[value='"+herchi.substring(0,4)+"']").prop("selected",true).parent().trigger('change');
					$("#popup-form-modal select[name='subMain'] option[value='"+herchi.substring(0,6)+"']").prop("selected",true).parent().trigger('change');
					$("#popup-form-modal select").selectpicker('refresh');

					$("#popup-form-modal select[name='general']").bind('change',function(){
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"'] option").prop("disabled",true)
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"'] option[value^="+$(this).selectpicker('val')+"]").prop("disabled",false);
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"']").selectpicker('refresh');
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"']").trigger('change');
					});
					$("#popup-form-modal select[name='main']").bind('change',function(){
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"'] option").prop("disabled",true)
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"'] option[value^="+$(this).selectpicker('val')+"]").prop("disabled",false);
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"']").selectpicker('refresh');
						$("#popup-form-modal select[name='"+$(this).attr("data-child")+"']").trigger('change');
					});
				});
				$("table").on("click",".miniEdit",function(){
					if($(".miniText").length){
						$(".miniText").remove();
					};
					miniEdit($(this));
				});
		    });
			var drop_down_change = function(elm){
				setTimeout(function(){
					$("#popup-form-modal select[name='"+$(elm).attr("data-child")+"'] option").prop("disabled",true)
					$("#popup-form-modal select[name='"+$(elm).attr("data-child")+"'] option[value^="+$(elm).selectpicker('val')+"]").prop("disabled",false);
					$("#popup-form-modal select[name='"+$(elm).attr("data-child")+"']").selectpicker('refresh');
					$("#popup-form-modal select[name='"+$(elm).attr("data-child")+"']").trigger('change');
				},400);
			}
		</script>
	</head>

	<body>
		<div id="body-wrapper">
			<div id="sidebar">
				<?php include("common/left_menu.php") ?>
			</div>
			<!-- End #sidebar -->
			<div id="bodyWrapper">
				<div class="content-box-top">
					<div class="summery_body">
						<div class="content-box-header">
							<p>Accounts Management</p>
							<button type="button" data-toggle="modal" data-target='#popup-form-modal' class="btn btn-default btn-sm pull-right mt-5 mr-5">Create</button>
							<div class="clear"></div>
						</div>
						<!-- End .content-box-header -->
						<div id="bodyTab1" class="table-responsive">
							<div class="clear"></div>
							<table class="table table-striped table-hover" width="90%" cellspacing="0" align="left" style="margin:0px auto; margin-bottom:20px">
								<thead>
									<tr>
										<th class="text-center col-xs-2">Account Code</th>
										<th class="text-center col-xs-8">Account Name</th>
										<th class="text-center col-xs-2">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
					foreach($account_codes_array as $key => $row){
						$inherit = $objChartOfAccounts->checkInherited($row['ACC_CODE']);

						switch (strlen($row['ACC_CODE'])) {
							case 4:
								$parent_code = substr($row['ACC_CODE'],0,2);
								break;
							case 6:
								$parent_code = substr($row['ACC_CODE'],0,4);
								break;
							case 9:
								$parent_code = substr($row['ACC_CODE'],0,6);
								break;
							default:
								$parent_code = '';
								break;
						}
?>
										<tr <?php echo (strlen($row[ 'ACC_CODE'])>6)?"class='".substr($row['ACC_CODE'],0,6)."'":""; ?> style="display:
											<?php echo (strlen($row['ACC_CODE']) < 7)?"":"none"; ?>;" data-parent="
											<?php echo $parent_code; ?>" >
											<td class="acc_code text-right">
												<?php echo $row['ACC_CODE']; ?>
											</td>
											<td class="acc_name text-left" align="left" style="position: relative;">
												<a id="view_button" data-title="<?php echo $row['ACC_TITLE'] ?>" data-id='<?php echo $row['ACC_CODE_ID'] ?>' class="miniEdit fa fa-pencil">
												</a>
												<span class="theTitleSpan"><?php echo $row['ACC_TITLE']; ?></span>
												<?php
										if(strlen($row['ACC_CODE'])==6 && $inherit){
?>
													<a id='view_button' class="rowToggler" data-code='<?php echo $row['ACC_CODE']; ?>'>
														<i class="fa fa-caret-down"></i>
													</a>
													<?php
										}
?>
											</td>
											<td class="delete_code_check text-center">
												<?php
								if(strlen($row['ACC_CODE'])<7){
?>
													<a data-herchi="<?php echo $row['ACC_CODE']; ?>" data-toggle="modal" data-target="#popup-form-modal" id="view_button" style="padding:3px 9px;"><i class="fa fa-plus"></i> Add</a>
													<?php
								}
								if(!in_array($row['ACC_CODE'], $array_exculde) && !$inherit && substr($row['ACC_CODE'],0,6) != '010101'&&substr($row['ACC_CODE'],0,6) != '040105'&&substr($row['ACC_CODE'],0,6) != '040102'){
?>
														<a class="pointer" do='<?php echo $row['ACC_CODE']; ?>'><i class="fa fa-times"></i></a>
														<?php
								}
?>
											</td>
										</tr>
										<?php
					}
?>
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
						<!--End bodyTab1-->
						<div class="clear"></div>
					</div>
					<!-- End summer -->
				</div>
				<!-- End .content-box -->
				<div id="popup-form-modal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Account Details</h4>
							</div>
							<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" class="form-horizontal">
								<div class="modal-body">
										<div class="form-group">
											<label class="control-label col-sm-2">General</label>
											<div class="col-sm-10">
												<select name="general" class="form-control show-tick" data-child="main" onchange="drop_down_change(this);" data-hide-disabled="true">
													<option value="00"></option>
													<?php
														foreach($account_codes_array as $key => $rowTwo){
															if(strlen($rowTwo['ACC_CODE'])==2){
																?>
																<option value="<?php echo $rowTwo['ACC_CODE']; ?>" data-subtext="<?php echo $rowTwo['ACC_CODE']; ?>" ><?php echo $rowTwo['ACC_TITLE']; ?></option>
																<?php
															}
														}
													?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2"> Main </label>
											<div class="col-sm-10">
												<select name="main" class="form-control show-tick" data-child="subMain" onchange="drop_down_change(this);" data-hide-disabled="true">
														<option value="00"></option>
														<?php
															foreach ($account_codes_array as $key => $rowTwo) {
																if(strlen($rowTwo['ACC_CODE'])==4){
																	?>
																	<option value="<?php echo $rowTwo['ACC_CODE']; ?>" data-subtext="<?php echo $rowTwo['ACC_CODE']; ?>" ><?php echo $rowTwo['ACC_TITLE']; ?></option>
																	<?php
																}
															}
														?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2">Sub Main</label>
											<div class="col-sm-10">
												<select name="subMain" class="form-control show-tick" data-hide-disabled="true">
														<option value="00"></option>
														<?php
															foreach ($account_codes_array as $key => $rowTwo) {
																if(strlen($rowTwo['ACC_CODE'])==6){
																	?>
																	<option value="<?php echo $rowTwo['ACC_CODE']; ?>" data-subtext="<?php echo $rowTwo['ACC_CODE']; ?>" ><?php echo $rowTwo['ACC_TITLE']; ?></option>
																	<?php
																}
															}
														?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2">Title </label>
											<div class="col-sm-10">
												<input name="title" id="charges" type="text" class="form-control" required />
											</div>
										</div>
									<div class="clear"></div>
								</div>
								<div class="modal-footer">
									<input name="addAccount" type="submit" value="Save" class="btn btn-primary" />
									<input type="button" value="Close" data-dismiss="modal" class="btn btn-default" />
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
			</div>
			<!--body-wrapper-->
			<div id="xfade"></div>
	</body>
	<script>
		$(window).load(function() {
			events();
		});
		var events = function(){
			$("#xfade").click(function(){
				$(this).fadeOut();$("#popUpDel").remove();
			});
				$(".rowToggler").click(function(){
					var thisSwitch = $(this);
					var subCode = $(this).attr('data-code');
					$("."+subCode).fadeToggle(function(){
						if($("."+subCode).is(":visible")){
							thisSwitch.children('i').css('transform','rotate(180deg)');
						}else{
							thisSwitch.children('i').css('transform','rotate(0deg)');
						}
					});
				});
				$("a.pointer").click(function(){
					var acc_code = $(this).attr("do");
					var thisRow = $(this).parent().parent();
					if(acc_code==""){
						return false;
					}
					$("#xfade").fadeOut();
					$("#popUpDel").remove();
					var appendable_td = $(this).parent(".delete_code_check");
					$("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><button class='dodelete btn btn-danger '>Confirm</button><button class='nodelete btn btn-default'>Cancel</button></div>");
					var win_hi = $(window).height()/2;
					var win_width = $(window).width()/2;
					win_hi = win_hi-$("#popUpDel").height()/2;
					win_width = win_width-$("#popUpDel").width()/2;
					$("#popUpDel").css({
						'position': 'fixed',
						'top': win_hi,
						'left': win_width
					});
					$("#popUpDel").hide();
					$("#xfade").fadeIn('slow');
					$("#popUpDel").fadeIn();
					$(".dodelete").click(function(){
							$("#popUpDel").show();
							$.get("db/del-account-code.php", {acc_code : acc_code}, function(data){
								if(data != ''){
									data = $.parseJSON(data);
									if(data['OK'] == 'Y'){
										thisRow.remove();

									}
									displayMessage(data['MSG']);
									$("#xfade").fadeOut();
									$("#popUpDel").fadeOut(function(){
										$("#popUpDel").remove();
									});
								}
							});
						});
					$(".nodelete").click(function(){
						$("#xfade").fadeOut();
						$("#popUpDel").fadeOut();
					});
					$(".close_popup").click(function(){
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut('fast');
					});
				});
				$(".code_like").keyup(function(){
					var search_term = $(this).val();
					if(search_term!==""){
						$("#clear_filter").css('opacity','1.0');
						$("#clear_filter").click(function(){
							$(".acc_code").parent("tr").fadeIn();
							$("#clear_filter").css('opacity','0.6');
							$("#clear_filter").css({'cursor':'default'});
							$(".code_like").val("");
							$(".name_like").val("");
							});
					}else{
						$("#clear_filter").css({'cursor':'default'});
						$("#clear_filter").css('opacity','0.6');
					}
					$(".acc_code").each(function(){
						if($(this).text().search(new RegExp(search_term,"i"))<0){
							$(this).parent("tr").fadeOut();
						}else{
							$(this).parent("tr").fadeIn();
						}
					});
				});
				$(".name_like").keyup(function(){
					var search_term = $(this).val();
					if(search_term!==""){
						$("#clear_filter").css('opacity','1.0');
						$("#clear_filter").click(function(){
							$(".acc_code").parent("tr").fadeIn();
							$("#clear_filter").css('opacity','0.6');
							$("#clear_filter").css({'cursor':'default'});
							$(".code_like").val("");
							$(".name_like").val("");
						});
					}else{
						$("#clear_filter").css({'cursor':'default'});
						$("#clear_filter").css('opacity','0.6');
					}
					$(".acc_name").each(function(){
						var term_length = search_term.length;
						if($(this).text().substr(0,term_length).search(new RegExp(search_term,"i"))<0){
							$(this).parent("tr").fadeOut();
						}else{
							$(this).parent("tr").fadeIn();
						}
					});
				});
		}
	</script>
	</html>
	<?php include('conn.close.php'); ?>
