<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/measure.php');
	include('common/classes/emb_products.php');
	include('common/classes/machines.php');
	include('common/classes/emb_stitch_account.php');
	include('common/classes/customers.php');
	include('common/classes/emb_inward.php');
	include('common/classes/accounts.php');
	include('common/classes/j-voucher.php');

	//Permission
	if(!in_array('emb-lot-register',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objLotRegister 			= new EmbLotRegister();
	$objMeasures  				= new Measures();
	$objMachines 					= new Machines();
	$objEmbStitchAccount	= new EmbStitchAccount();
	$objProducts 					= new EmbProducts();
	$objCustomers 				= new customers();
	$objInward    				= new EmbroideryInward();
	$objJournalVoucher 		= new JournalVoucher();
	$objChartOfAccounts 	= new ChartOfAccounts();

	$total 		     = $objConfigs->get_config('PER_PAGE');
	$customersList = $objCustomers->getList();
	$machineList   = $objMachines->getList();

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$objLotRegister->fromDate = (isset($_GET['from_date'])&&$_GET['from_date']!="")?date('Y-m-d',strtotime($_GET['from_date'])):"";
	$objLotRegister->toDate   = (isset($_GET['to_date'])&&$_GET['to_date']!="")?date('Y-m-d',strtotime($_GET['to_date'])):"";

	$lotStatus = '';
	if(isset($_GET['lotStatus'])){
		if($_GET['lotStatus'] == 'A'){
			$lotStatus = '';
		}else{
			$lotStatus = mysql_real_escape_string($_GET['lotStatus']);
		}
	}

	$objLotRegister->lotStatus 			 = $lotStatus;

	$objLotRegister->customerAccCode = isset($_GET['customer'])?mysql_real_escape_string($_GET['customer']):"";
	$objLotRegister->designNum 			 = isset($_GET['design'])?mysql_real_escape_string($_GET['design']):"";
	$objLotRegister->lotNum    			 = isset($_GET['lot_number'])?mysql_real_escape_string($_GET['lot_number']):"";
	$objLotRegister->machineNum    	 = isset($_GET['machine_id'])?mysql_real_escape_string($_GET['machine_id']):"";

	$lot_register_list 		 = $objLotRegister->search($start,$total);

	$found_records = $objLotRegister->found_records;

	$counter_start = $start+1;

	$billing_types = array();
	$billing_types['S'] = 'Stitches';
	$billing_types['Y'] = 'Yards';
	$billing_types['U'] = 'Suites';
	$billing_types['L'] = 'Laces';
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<style media="screen">
			table.input_table th,table.input_table td{
				padding: 10px !important;
			}
			tr.quickSubmit td{
				padding: 1px !important;
			}
			div.red{
				color: red;
			}
			span.red{
				color:red;
			}
			span.green{
				color:green;
			}
			tr{
				transition: all 300ms ease-in;
				-webkit-transition: all 300ms ease-in;
				-moz-transition: all 300ms ease-in;
			}
			tr td{
				font-size:12px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.lot.register.config.js"></script>
    <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function (){
				$('select').selectpicker();
				$("ul.selectpicker:first li a").click(function(){
					var accountTitle = $(this).children("span.text").text();
					accountsCode = accountTitle.substr(-9,9);
					accountTitle = accountTitle.replace(accountTitle.substr(-9,9),'');
					if(accountTitle!==""){
						$("input[name='custAccCode']").val(accountsCode);
					}else{
						$("input[name='custAccCode']").val("");
					}
				});
				$("button.dropdown-toggle").focus();
				<?php if(isset($_GET['wid'])){  ?> $("button.dropdown-toggle").last().focus(); <?php } ?>
      });
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Lot Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="emb-lot-register.php"><div class="tab">New</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
            	<div id="bodyTab1">
<?php
			$searchAmountTotal = 0;
?>
					<table width="100%" cellspacing="0" class="input_table" >
<?php
					if(mysql_num_rows($lot_register_list)){
?>
                        <thead>
                            <tr>
                               	<th class="text-center">Lot Date</th>
                                <th class="text-center">Customer</th>
																<th class="text-center">Lot#</th>
                               	<th class="text-center">Product</th>
                                <th class="text-center">Quality</th>
                                <th class="text-center">Measure</th>
																<th class="text-center">Billing Type</th>
                                <th class="text-center">Thaan</th>
                                <th class="text-center">Design#</th>
                                <th class="text-center">Machine</th>
                                <th class="text-center">Lot Status</th>
                               	<th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
						$status_color     = "btn-primary";
						while($detailRow = mysql_fetch_array($lot_register_list)){
							$measurement 	= $objMeasures->getName($detailRow['MEASURE_ID']);
							$stitchMan 		= $objEmbStitchAccount->getName($detailRow['STITCH_ACC']);
							$customerTitle 	= $objChartOfAccounts->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);
							$machine_number = $objMachines->getRecordDetailsValue($detailRow['MACHINE_ID'],'MACHINE_NO');
							$machine_name  	= $objMachines->getName($detailRow['MACHINE_ID']);

							if($detailRow['LOT_STATUS'] == 'P'){
								$status = 'PROCESSING';
								$status_color     = "btn-primary";
							}elseif($detailRow['LOT_STATUS'] == 'C'){
								$status = 'COMPLETED';
								$status_color     = "btn-info";
							}elseif($detailRow['LOT_STATUS'] == 'O'){
								$status = 'DELIVERED';
								$status_color     = "btn-success";
							}elseif($detailRow['LOT_STATUS'] == 'F'){
								$status = 'FROZEN';
								$status_color     = "btn-primary";
							}elseif($detailRow['LOT_STATUS'] == 'R'){
								$status = 'CLAIMED';
								$status_color     = "btn-warning";
							}else{
								$status = '';
							}
?>
                            <tr class="<?php echo ($detailRow['LOT_STATUS'] == 'R')?"claimRow":""; ?>"  >
                                <td class="text-center"><?php echo date('d-m-Y',strtotime($detailRow['LOT_DATE'])) ?></td>
                                <td class="text-left"><?php echo $customerTitle; ?></td>
																<td class="text-center"><?php echo $detailRow['LOT_NO'] ?></td>
                                <td class="text-left"><?php echo $objProducts->getTitle($detailRow['PRODUCT_ID']); ?></td>
                                <td class="text-left"><?php echo $detailRow['QUALITY'] ?></td>
                                <td class="text-center"><?php echo $measurement; ?></td>
																<td class="text-center"><?php echo isset($billing_types[$detailRow['BILLING_TYPE']])?$billing_types[$detailRow['BILLING_TYPE']]:""; ?></td>
                                <td class="text-center"><?php echo $detailRow['MEASURE_LENGTH'] ?></td>
                                <td class="text-center"><?php echo $detailRow['DESIGN_CODE'] ?></td>
                                <td class="text-center"><?php echo $machine_number."-".$machine_name; ?></td>
                                <td class="text-center"><span class="badge <?php echo $status_color; ?>"><?php echo $status; ?></span></td>
                                <td class="text-center">
<?php
					$claim_found_in_outward = $objLotRegister->checkIfClaimIsTransferred($detailRow['ID']);
					if($detailRow['LOT_STATUS'] != 'O'){
						if($detailRow['LOT_STATUS'] != 'R' || ($claim_found_in_outward && $detailRow['LOT_STATUS'] == 'R')){
?>
						<?php if(in_array('emb-lot-register-modify',$permissionz) || $admin == true){ ?>
                                	<a type="button" id="view_button" class="editLot" value="view" do='<?php echo $detailRow['ID']; ?>'><i class="fa fa-pencil"></i></a>
						<?php } ?>
<?php
						}
					if($detailRow['LOT_STATUS'] != 'R'){
?>
                                    <button class="<?php echo ($detailRow['LOT_STATUS'] == 'P')?"button-green":"button-orange"; ?>" value="view" do='<?php echo $detailRow['ID']; ?>' onClick='lotProceed(this);' ><i class="fa fa-arrow-<?php echo ($detailRow['LOT_STATUS'] == 'P')?"right":"left"; ?>"></i></button>
<?php
					}
						if(in_array('emb-lot-register-delete',$permissionz) || $admin == true){
							if( $detailRow['LOT_STATUS'] != 'R' || ($claim_found_in_outward && $detailRow['LOT_STATUS'] == 'R')){
?>
                                    <a class="pointer" do="<?php echo $detailRow['ID']; ?>"><i class="fa fa-times"></i></a>
<?php
							}
						}
					}
?>
                            	</td>
                            </tr>
<?php
								$counter_start++;
						}
					}
?>
                        </tbody>
                    </table>
									<div class="col-xs-12 text-center">
										<?php
										if($found_records > $total){
											$get_url = "";
											foreach($_GET as $key => $value){
												$get_url .= ($key == 'page')?"":"&".$key."=".$value;
											}
											?>
											<nav>
												<ul class="pagination">
													<?php
													$count = $found_records;
													$total_pages = ceil($count/$total);
													$i = 1;
													$thisFileName = basename($_SERVER['PHP_SELF']);
													if(isset($this_page) && $this_page>0){
														?>
														<li>
															<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
														</li>
														<?php
													}
													if(isset($this_page) && $this_page>=1){
														$prev = $this_page;
														?>
														<li>
															<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
														</li>
														<?php
													}
													$this_page_act = $this_page;
													$this_page_act++;
													while($total_pages>=$i){
														$left = $this_page_act-5;
														$right = $this_page_act+5;
														if($left<=$i && $i<=$right){
															$current_page = ($i == $this_page_act)?"active":"";
															?>
															<li class="<?php echo $current_page; ?>">
																<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
															</li>
															<?php
														}
														$i++;
													}
													$this_page++;
													if(isset($this_page) && $this_page<$total_pages){
														$next = $this_page;
														?>
														<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
														<?php
													}
													if(isset($this_page) && $this_page<$total_pages){
														?>
														<li>
															<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
														</li>
													</ul>
												</nav>
												<?php
											}
										}
										?>
									</div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>

                <div id="bodyTab2" style="display:none;" >
                    <div id="form">
                    <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<?php
						if(isset($xPage)){
?>
	                    <input name="page" value="<?php echo $xPage; ?>" type="hidden" />
<?php
						}
?>
                        <div class="caption"></div>


                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

                        <div class="caption">From Date :</div>
                        <div class="field">
                        	<input name="from_date" class="form-control datepicker" style="width:145px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">To Date :</div>
                        <div class="field">
                        	<input name="to_date" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker"  style="width:145px;" />
                        </div>
                        <div class="clear"></div>

						<div class="caption">Lot Number :</div>
                        <div class="field">
							<input style="width: 150px;" class="form-control" value="" name="lot_number" />
						</div>
                        <div class="clear"></div>

						<div class="caption">Lot Status :</div>
                        <div class="field" style="width: 450px;">
							<input id="cmn-toggle-all" name="lotStatus" value="A" class="css-checkbox" type="radio" checked />
							<label for="cmn-toggle-all" class="css-label-radio"> All </label>
							<br>
							<input id="cmn-toggle-under-process" name="lotStatus" value="P" class="css-checkbox" type="radio" />
							<label for="cmn-toggle-under-process" class="css-label-radio"> Under Process </label>
							<br>
							<input id="cmn-toggle-complete" name="lotStatus" value="C" class="css-checkbox" type="radio" />
							<label for="cmn-toggle-complete" class="css-label-radio"> Completed </label>
							<br>
							<input id="cmn-toggle-delived" name="lotStatus" value="O" class="css-checkbox" type="radio" />
							<label for="cmn-toggle-delived" class="css-label-radio"> Delivered </label>
							<br>
							<input id="cmn-toggle-claimed" name="lotStatus" value="R" class="css-checkbox" type="radio" />
							<label for="cmn-toggle-claimed" class="css-label-radio"> Claimed </label>
                        </div>
                        <div class="clear"></div>

						<div class="caption">Design :</div>
                        <div class="field">
                        	<input name="design" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Customer :</div>
                        <div class="field">
                        	<select name="customer" class="form-control show-tick" data-live-search="true" >
                            	<option value=""></option>
<?php
							$customers = $objCustomers->getList();
							if(mysql_num_rows($customers)){
								while($customer = mysql_fetch_array($customers)){
?>
								<option value="<?php echo $customer['CUST_ACC_CODE']; ?>" data-subtext="<?php echo $customer['CUST_ACC_CODE']; ?>"><?php echo $customer['CUST_ACC_TITLE']; ?></option>
<?php
								}
							}
?>
                            </select>
                        </div>
                        <div class="clear"></div>

												<div class="caption">Machine :</div>
												<div class="field">
													<select name="machine_id" class="machine_id form-control" data-live-search="true" >
														<option value=""></option>
													<?php
														if(mysql_num_rows($machineList)){
															while($machine_row = mysql_fetch_array($machineList)){
													?>
														<option value="<?php echo $machine_row['ID']; ?>"><?php echo $machine_row['MACHINE_NO']."-".$machine_row['NAME']; ?></option>
													<?php
															}
														}
													?>
													</select>
												</div>
												<div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm"></div>
    <div class="invoicePopup"></div>
</body>
</html>
<script>
	$(document).ready(function() {
		$(".claimRow").find('td').css('color','#C30');
		$("a.pointer").click(function(){
			var idValue = $(this).attr("do");
			var hideTr = $(this).parent().parent();
			var itemTitle = $(this).parent().siblings('td').first().next().text();
			$("#xfade").fadeOut();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Are You Sure?</p><a class='btn btn-danger dodelete'>Confirm</a><a class='btn btn-info nodelete'>Cancel</a></div>");
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$("#popUpDel").height()/2;
			win_width = win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel").hide();
			$("#xfade").fadeIn('slow');
			$("#popUpDel").children("p.confirm").text(" Do you want to delete ? ");
			$("#popUpDel").children("a.dodelete").text("Delete");
			$("#popUpDel").fadeIn();
			$(".dodelete").click(function(){
				$.post("db/del-lot-register.php", {lid : idValue}, function(data){
					if(data == true){
						$("#popUpDel").children("a.dodelete").hide();
						hideTr.slideUp();
						var modSerial = hideTr.nextAll("tr");
						$("#popUpDel").children("a.nodelete").text("Close");
						$("#popUpDel").children("p.confirm").text("Record Deleted!");
					}else if(data == false){
						$("#popUpDel").children("a.dodelete").hide();
						$("#popUpDel").children("a.nodelete").text("Close");
						$("#popUpDel").children("p.confirm").text("Unable to Delete!");
					}
				});
			});
			$(".nodelete").click(function(){
				$("#xfade").fadeOut();
				$("#popUpDel").fadeOut();
				});
			$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#xfade").fadeOut('fast');
			});
		});

		$("input[type='text']").keyup(function(e){
			if(e.keyCode==27){
				$(this).blur();
			}
		});
	});

	$(".button-green").attr('title','Process');
	$(".button-orange").attr('title','Un-Process');
	$(".editLot").click(function(){
		$(this).editLotDetails();
	});
<?php
	if(isset($_GET['tab'])&&$_GET['tab'] == 'search'){
?>
		tab('2', '1', '2');
<?php
	}
?>
</script>
