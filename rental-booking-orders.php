<?php
	include('common/connection.php');
	include 'common/config.php';
	include ('common/classes/items.php');
	include ('common/classes/services.php');
	include ('common/classes/ordering.php');
	include ('common/classes/rental_booking.php');

	//Permission
	if(!in_array('rental-booking',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objItems 			  = new Items();
	$objServices 			= new Services();
	$objRentalBooking = new RentalBooking();
	$objOrdering      = new Ordering();

	$rentalList 		  = $objRentalBooking->getOrderPendingList();

?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" type="text/css" />
	<style>
	td{
		padding: 10px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$(".selectpicker").selectpicker();
		$("a.pointer").click(function(){
			$(this).deleteMainRow("db/del-rental-booking.php");
		});
	});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("input[name=save_changes]").click(function() {
				var delivery_time = $('input[name=delivery_time1]').val();
				var return_time = $('input[name=return_time1]').val();
				var rental_id = $('input[name=rental_id]').val();

				$.post("db/updateRentalBookingDates.php",{delivery_time:delivery_time,return_time:return_time,rental_id:rental_id},function(data){
					$.get("rental-booking-management.php",function(data_html){
						var content = $(data_html).find(".update_row").html();
						$(".update_row").html(content);
						//$(".update_row").removeClass("update_row");
						hidePopUpBox();
						window.location.href = 'rental-booking-management.php?tab=list';
					});

				});
			});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".save_rental").click(function(){
			saveRentalBooking();
		});
	});
</script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p> Booking Orders List </p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1" style="display:block;">
					<table width="100%" cellspacing="0" >
						<thead>
							<tr>
								<th style="text-align:center;width:5%;">Booking #</th>
								<th style="text-align:center;width:20%;">Date</th>
								<th style="text-align:center;width:20%;">ClientInfo</th>
								<th style="text-align:center;width:20%;">Item</th>
								<th style="text-align:center;width:10%;">Quantity</th>
								<th style="text-align:center;width:10%;">Action</th>
							</tr>
						</thead>
						<tbody>
									<?php
									$counter = 1;
									if(mysql_num_rows($rentalList)){
										while($rentalRow = mysql_fetch_array($rentalList)){
											if($rentalRow['ITEM_ID']>0){
												$item_name 		 = $objItems->getItemTitle($rentalRow['ITEM_ID']);
											}else{
												$item_name 		 = $objServices->getTitle($rentalRow['SERVICE_ID']);
											}
											$bookingDetail = $objRentalBooking->getDetail($rentalRow['RENTAL_BOOKING_ID']);
											?>
											<tr>
												<td class="text-center"><?php echo $bookingDetail['BOOKING_ORDER_NO']; ?></td>
												<td class="text-left">
													<b>Booking Date : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['BOOKING_DATE_TIME'])); ?>
													<br />
													<b>Delivery Time : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['DELIVERY_DATE_TIME'])); ?>
													<br />
													<b>Return Time : </b>	<?php echo date("d-m-Y",strtotime($bookingDetail['RETURNING_DATE_TIME'])); ?>
												</td>
												<td class="text-left">
														<b>Name : </b>	<?php echo $bookingDetail['CLIENT_NAME']; ?>
														<br />
														<b>Mobile : </b>	<?php echo $bookingDetail['CLIENT_MOBILE']; ?>
														<br />
														<b>Total Price : </b>	<?php echo $bookingDetail['TOTAL_PRICE']; ?>
														<br />
												</td>
												<td class="text-center"><?php echo $item_name; ?></td>
												<td class="text-center"><?php echo $rentalRow['QUANTITY']; ?></td>
												<td class="text-center">
													<a href="ordering-details.php?rbdid=<?php echo $rentalRow['ID']; ?>" target="_blank" id="view_button"> <i class="fa fa-pencil"></i> </a>
												</td>
											</tr>
											<?php
										}
									}
									?>
									</tbody>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab1-->
							<div style="height:0px;clear:both"></div>
							<div id="bodyTab2" style="display:none;" >
								<div id="form">
									<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
										<div class="title" style="font-size:20px; margin-bottom:20px">Search Booking</div>

										<div class="caption"></div>
										<div class="message red"><?php echo (isset($message))?$message:""; ?></div>
										<div class="clear"></div>

										<div class="caption">Title</div>
										<div class="field">
											<input type="text" value="<?php echo $objRentalBooking->title; ?>" name="title" class="form-control" />
										</div>
										<div class="clear"></div>

										<div class="caption">From Booking Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->fromBookingDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->fromBookingDate)); ?>" name="fromBookingDate" class="form-control datepicker" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Booking Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->toBookingDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->toBookingDate)); ?>" name="toBookingDate" class="form-control datepicker"/>
										</div>
										<div class="clear"></div>

										<div class="caption">From Delivery Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->fromDeliveryDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->fromDeliveryDate)); ?>" name="fromDeliveryDate" class="form-control datepicker" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Delivery Date</div>
										<div class="field">
											<input type="text" value="<?php echo ($objRentalBooking->toDeliveryDate == '')?'':date('d-m-Y',strtotime($objRentalBooking->toDeliveryDate)); ?>" name="toDeliveryDate" class="form-control datepicker"/>
										</div>
										<div class="clear"></div>

										<div class="caption"> Booking Status:</div>
										<div class="field">
											<select class="selectpicker show-tick form-control" name="booking_status" data-live-search="true">
												<option value=""></option>
												<option value="N">New</option>
												<option value="D">Delivered</option>
												<option value="R">Return</option>
											</select>
										</div>
										<div class="clear"></div>

										<div class="caption">Client Name</div>
										<div class="field">
											<input type="text" value="<?php echo $objRentalBooking->clinet_name; ?>" name="clinet_name" class="form-control" />
										</div>
										<div class="clear"></div>

										<div class="caption"> Ordering Status :</div>
										<div class="field">
											<select class="selectpicker show-tick form-control" name="order_status" data-live-search="true" >
												<option value="" ></option>
												<option value="Y">In Stock</option>
												<option value="N">Out of Stock</option>
											</select>

										</div>
										<div class="clear"></div>
										<div class="caption"></div>
										<div class="field">
											<input type="submit" value="Search" name="search" class="button"/>
										</div>
										<div class="clear"></div>
									</form>
								</div><!--form-->
							</div> <!-- End bodyTab2 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->
				</div><!--body-wrapper-->

				<!-- popup window start -->
				<div id="popUpBox" style="display:none;top:100px !important">
					<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>
					<!---<form action="" method="post" id="myid">-->
					<input type="hidden" name="rental_id" />
					<div class="clear"></div>
					<div class="caption">Delivery Time</div>
					<div class="field" style="width: 250px;">
						<input type="text" name="delivery_time1" value="" class="form-control" />
					</div>

					<div class="caption">Return Time</div>
					<div class="field" style="width: 250px;">
						<input type="text" name="return_time1" value="" class="form-control" />
					</div>

					<div class="field" style="width: 250px;margin-top:20px;">
						<input type="submit" name="save_changes"   value="Update" class="btn btn-primary btn-sm" />
					</div>

				</div>
				<div class="clear"></div>
				<!-- popup window end -->
				</div>
				<div id="xfade"></div>
				<div id="fade"></div>
</body>
</html>
			<?php include('conn.close.php'); ?>
			<script>
			$(document).ready(function() {
				$(".shownCode,.loader").hide();
				$("input.supplierTitle").keyup(function(){
					$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
				});
				$(window).keydown(function(e){
					if(e.keyCode==113){
						e.preventDefault();
						window.location.href = "<?php echo "inventory-details.php"; ?>";
					}
				});
			});
			$(function(){
				$("#fromDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
				$("#toDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){  ?>tab('2', '1', '2');<?php } ?>

			</script>
