<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/wip.php');
	include('common/classes/items.php');
    include('common/classes/itemCategory.php');
	include('common/classes/measure.php');

	//Permission
	if(!in_array('work-in-process',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objWorkInProcess = new WorkInProgress();
	$objItems 		  = new Items();
    $objItemCategory  = new itemCategory();
	$objMeasures      = new Measures();

    //Pagination Settings
	$objPagination = new Pagination();
	$total = $objPagination->getPerPage();
	//Pagination Settings { END }

	$today = date('Y-m-d');

    $objWorkInProcess->from_date    = isset($_GET['from_date'])?$_GET['from_date']:date('Y-m-01');
    $objWorkInProcess->to_date      = isset($_GET['to_date'])?$_GET['to_date']:$today;
    $objWorkInProcess->process_id   = isset($_GET['process_id'])?$_GET['process_id']:"";
    $objWorkInProcess->status       = isset($_GET['satus'])?$_GET['satus']:"";

    if(isset($_GET['page'])){
        $this_page = $_GET['page'];
        if($this_page>=1){
            $this_page--;
            $start = $this_page * $total;
        }
    }else{
        $start = 0;
        $this_page = 0;
    }

	$workInProcessList = $objWorkInProcess->search($start,$total);
    $found_records     = $objWorkInProcess->found_records;

    $itemsCategoryList   = $objItemCategory->getList();
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <!-- jQuery Configuration -->
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script>
		$(document).ready(function(){
			$("select").selectpicker();
            $.fn.deleteRowDefaultWip = function(file){
                var idValue = $(this).attr("do");
                var clickedDel = $(this);
                job = $(this).parent().siblings().first().text();
                $("#fade").hide();
                $("#popUpDel").remove();
                $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete Job # "+job+"?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
                $("#popUpDel").hide();
                $("#popUpDel").centerThisDiv();
                $("#fade").fadeIn('slow');
                $("#popUpDel").fadeIn();
                $(".dodelete").click(function(){
                        $.post(file, {id : idValue}, function(data){
                            data = $.parseJSON(data);
                            $("#popUpDel").children(".confirm").text(data['MSG']);
                            $("#popUpDel").children(".dodelete").hide();
                            $("#popUpDel").children(".nodelete").text("Close");
                            if(data['OKAY'] == 'Y'){
                                clickedDel.parent('td').parent('tr').remove();
                            }
                        });
                    });
                $(".nodelete").click(function(){
                    $("#fade").fadeOut();
                    $("#popUpDel").fadeOut();
                });
                $(".close_popup").click(function(){
                    $("#popUpDel").slideUp();
                    $("#fade").fadeOut('fast');
                });
            };
			$(".pointer").click(function(){
				$(this).deleteRowDefaultWip('db/delWorkInProcess.php');
			});
			$("select.statusSelector").change(function(){
				var status = $(this).find("option:selected").val();
				var wip_id = parseInt($(this).attr('wip-id'))||0;
                var $current_row = $(this).parent().parent();
                $("select.statusSelector").prop('disabled',true).selectpicker('refresh');
				$.post('db/workInProcessStatus.php',{wip_id:wip_id,status:status},function(data){
					data = $.parseJSON(data);
                    $("select.statusSelector").prop('disabled',false).selectpicker('refresh');
                    if(status == 'P'){
                        $current_row.find(".pointer").show();
                    }else{
                        $current_row.find(".pointer").hide();
                    }
				});
			});
		});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Work In Process</p>
                <span id="tabPanel">
                    <div class="tabPanel">
						<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="wip-details.php"><div class="tab">Add</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab2" style="display:none;">
                    <div id="form">
                        <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                            <div class="caption">From Date </div>
                            <div class="field">
                                <input type="text" class="form-control datepicker" name="from_date" style="width:150px;" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">To Date </div>
                            <div class="field">
                                <input type="text" class="form-control datepicker" name="to_date" style="width:150px;" value="<?php echo date('d-m-Y'); ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Job No</div>
                            <div class="field">
                                <input type="text" value="" name="job_number" class="form-control" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Process Item</div>
                            <div class="field">
                                    <select class="itemSelector show-tick form-control" name="main_item_id" data-style="btn-default" data-live-search="true" style="border:none">
                                        <option selected value=""></option>
            <?php
                                if(mysql_num_rows($itemsCategoryList)){
                                    while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                                        $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
            ?>
                                            <optgroup label="<?php echo $ItemCat['NAME']; ?>">
            <?php
                                        if(mysql_num_rows($itemList)){
                                            while($theItem = mysql_fetch_array($itemList)){
                                                if($theItem['ACTIVE'] == 'N'){
                                                    continue;
                                                }
                                                if($theItem['INV_TYPE'] == 'B'){
                                                    continue;
                                                }
            ?>
                                                <option data-type="I" value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
            <?php
                                            }
                                        }
            ?>
                                            </optgroup>
            <?php
                                    }
                                }
            ?>
                                    </select>
                            </div>
                            <div class="clear"></div>


                            <div class="caption"></div>
                            <div class="field">
                                <input type="submit" value="Search" name="search" class="button"/>
                            </div>
                            <div class="clear"></div>
                        </form>
                    </div>
                </div>
                <div id="bodyTab1">
                    <table width="100%" cellspacing="0">
                        <thead>
                            <tr>
                               <th width="10%" style="text-align:center">Job #</th>
                               <th width="15%" style="text-align:center">Started On</th>
                               <th width="15%" style="text-align:center">Completed On</th>
                               <th width="5%" style="text-align:center">Status</th>
                               <th width="10%" style="text-align:center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
				if(mysql_num_rows($workInProcessList)){
					while($row = mysql_fetch_array($workInProcessList)){
?>
                            <tr id="recordPanel">
                            		<td style="text-align:center"><?php echo $row['ID']; ?></td>
                                <td style="text-align:center"><?php echo date('d-m-Y',strtotime($row['STARTED_ON'])); ?></td>
                                <td style="text-align:center"><?php echo ($row['COMPLETED_ON'] =='0000-00-00')?"PROCESSING":date('d-m-Y',strtotime($row['COMPLETED_ON'])); ?></td>
                                <td style="text-align:center">
                                	<select class="statusSelector form-control" wip-id='<?php echo $row['ID']; ?>'>
                                    	<option value="C" <?php echo ($row['STATUS'] == 'C')?'selected="selected"':""; ?> >Completed</option>
                                        <option value="P" <?php echo ($row['STATUS'] == 'P')?'selected="selected"':""; ?> >Processing</option>
                                    </select>
                                </td>
                                <td style="text-align:center">
                                  <a href="wip-details.php?wid=<?php echo $row['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                  <a class="pointer" do="<?php echo $row['ID']; ?>" style="display:<?php echo ($row['STATUS'] == 'C')?"none":""; ?>;" > <i class="fa fa-times"></i> </a>
                            	</td>
                            </tr>
<?php
					}
				}else{
?>
							<tr>
                            	<th colspan="100" style="text-align:center;">
                                	<?php echo (isset($_POST['search']))?"0 Records Found!!":"No Records Today." ?>
                                </th>
                            </tr>
<?php
				}
?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
<?php
                    if($found_records > $total){
                        $get_url = "";
                        foreach($_GET as $key => $value){
                            $get_url .= ($key == 'page')?"":"&".$key."=".$value;
                        }
?>
                            <nav>
                              <ul class="pagination">
<?php
                        $count = $found_records;
                        $total_pages = ceil($count/$total);
                        $i = 1;
                        $thisFileName = basename($_SERVER['PHP_SELF']);
                        if(isset($this_page) && $this_page>0){
?>
                            <li>
                                <?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
                            </li>
<?php
                        }
                        if(isset($this_page) && $this_page>=1){
                            $prev = $this_page;
?>
                            <li>
                                <?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
                            </li>
<?php
                        }
                        $this_page_act = $this_page;
                        $this_page_act++;
                        while($total_pages>=$i){
                            $left = $this_page_act-5;
                            $right = $this_page_act+5;
                            if($left<=$i && $i<=$right){
                            $current_page = ($i == $this_page_act)?"active":"";
?>
                                <li class="<?php echo $current_page; ?>">
                                    <?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
                                </li>
<?php
                            }
                            $i++;
                        }
                        $this_page++;
                        if(isset($this_page) && $this_page<$total_pages){
                            $next = $this_page;
                            ?>
                            <li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
                            <?php
                        }
                        if(isset($this_page) && $this_page<$total_pages){
?>
                                <li>
                                    <?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
                                </li>
                            </ul>
                            </nav>
<?php
                        }
                    }
?>
                    </div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($_GET['tab']) && $_GET['tab'] =='search'){
		echo "tab('2', '1', '2');";
	}
?>
</script>
