<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/trial-balance.php';
	include 'common/classes/customers.php';

	//Permission
	if(!in_array('trial-balance',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objTrialBalance = new TrialBalance;
	$objCustomers    = new Customers;
	$city = '';
	$posted_code = '';
	$cities 		 = $objCustomers->get_cities_array();
	if(isset($_GET['search'])){
		$fromDate = ($_GET['fromDate']=="")?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$toDate   = ($_GET['toDate']=="")?"":date('Y-m-d',strtotime($_GET['toDate']));
		if($fromDate==''){
			$fromDate = date('01-m-Y');
		}
		$thisMonthStart = date('01-m-Y',strtotime($fromDate));
		$prevMonthEndDate = date('Y-m-d',strtotime($thisMonthStart."-1 day"));

		$accCode  =  mysql_real_escape_string($_GET['AccCode']);
		$posted_code = $accCode;
		if($posted_code == '010104'){
			$city  =  mysql_real_escape_string($_GET['city']);
		}
		if($accCode == ''){
			$accountsList = $objTrialBalance->getFourthLevelAccounts();
		}else{
			$accountsList = $objTrialBalance->getChildAccountsOf($accCode);
		}
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>Trial Balance</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
			html{
			}
			.ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized{
				float:left;
				width: 152px;
				padding-left: 5px;
				border: 1px solid #CCC;
				height:30px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;
				border:1px solid #DDDDDD;

				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;

				-moz-border-radius-topleft:3px;
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;

				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;

				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			}
			.input_sized:hover{
				border-color: #9ecaed;
				box-shadow: 0 0 2px #9ecaed;
			}
		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
	        $(document).ready(function(){
<?php
			if($posted_code != '010104'){
?>
				$(".cities").hide();
<?php 		} ?>
				$.fn.stDigits = function(){
					return this.each(function(){
						$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
					});
				};
				$("tr.theCommaRow").each(function(index, element){
		            $(this).find("td").first().next().nextAll().stDigits();
		        });
		    });
			$(window).on('load', function(){
				$(".loadingContent").removeClass('loadingContent');
				$(".printTable").show();
				$(".citySelector").selectpicker();
				$(".accountSelector").selectpicker();
				$(".accountSelector").change(function(){
					var code_selected = $("select.accountSelector option:selected").val();
					if(code_selected == '010104'){
						$(".cities").show();
					}else{
						$(".cities").hide();
					}
				});
				$(".printThis").click(function(){
					var MaxHeight = 730;
					var RunningHeight = 0;
					var PageNo = 1;
					var MaxHeight_after = 0;

                    var odr = 0;
                    var ocr = 0;
                    var pdr = 0;
                    var pcr = 0;
                    var cdr = 0;
                    var ccr = 0;

                    //Sum Table Rows (tr) height Count Number Of pages
                    $('table.tableBreak>tbody>tr').each(function(){

                        odr = parseFloat($(this).find(".drOpening").text())||0;
                        ocr = parseFloat($(this).find(".crOpening").text())||0;
                        pdr = parseFloat($(this).find(".debits").text())||0;
                        pcr = parseFloat($(this).find(".credits").text())||0;
                        cdr = parseFloat($(this).find(".drClosing").text())||0;
                        ccr = parseFloat($(this).find(".crClosing").text())||0;

                        if( odr == 0 && ocr == 0 && pdr == 0 && pcr == 0 && cdr == 0 && ccr == 0){
                            if($(this).find(".drOpening").length){
                                $(this).remove();
                                return;
                            }
                        }
                    });
                    $("tr.ac_title_row").each(function(){
                        if($(this).next("tr.theCommaRow").length == 0){
                        $(this).remove();
                        }
                    });

					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if(PageNo == 1){
							MaxHeight = 730;
						}else{
							MaxHeight = 820;
						}
						if (RunningHeight + $(this).height() > MaxHeight) {
							RunningHeight = 0;
							PageNo += 1;
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var tableFooter = $(".tableFooter").html();
					var repoDate    = $(".repoDate").text();
					//remove previous thead/tfoot/ReportDate
					$(".tHeader").remove();
					$(".repoDate").remove();
					$(".tableFooter").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						if(i == 1){
							MaxHeight = 730;
						}else{
							MaxHeight = 820;
						}
						$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);

					}
					$(".newTable").last().append(tableFooter);
					$('table.tableBreak').remove();
					$(".printTable").printThis({
					  debug: false,
					  importCSS: false,
					  printContainer: true,
					  loadCSS: 'resource/css/reports.css',
					  pageTitle: "DN Enterprise",
					  removeInline: false,
					  printDelay: 500,
					  header: null
				  });
				});
			});
		</script>
      	<script type = "text/javascript" src = "resource/scripts/reports.configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p style="font-size:18px;">Trial Balance</p>
                        <div class="clear"></div>
                  		</div><!-- End .content-box-header -->

                    	<div id = "bodyTab1">
                            <div class="clear"></div>
                            <div id="form">
                                <form method="method" action="">

                                    <div class="caption">Start Period:</div>
                                    <div class="field" style="width:200px">
                                        <input type="text" value="<?php echo (isset($fromDate))?date('d-m-Y',strtotime($fromDate)):''; ?>" name="fromDate" class="form-control datepicker" style="width:145px" />
                                    </div>

                                    <div class="caption">End Period:</div>
                                    <div class="field" style="width:200px">
                                        <input type="text" name="toDate" value="<?php echo (isset($toDate))?date('d-m-Y',strtotime($toDate)):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">AccCode:</div>
                                    <div class="field" style="width:300px">
                                    	<select name="AccCode" class="accountSelector show-tick form-control" data-live-search="true">
                                        <option value=""></option>
<?php
									$thirdLevelAccountList = $objTrialBalance->getThirdLevelAccounts();
									if(mysql_num_rows($thirdLevelAccountList)){
										while($thirdRow = mysql_fetch_array($thirdLevelAccountList)){
											$selected = (isset($accCode)&&$thirdRow['ACC_CODE']==$accCode)?"selected='selected'":"";
?>
                                        	<option value="<?php echo $thirdRow['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $thirdRow['ACC_TITLE']; ?></option>
<?php
										}
									}
?>
										</select>
                                    </div>
                                    <div class="cities">
	                                    <div class="caption" style="width:50px;">City</div>
	                                    <div class="field" style="width:155px;">
	                                    	<select name="city" class="citySelector show-tick form-control">
	                                    		<option value="">All Cities</option>
<?php
										foreach($cities as $key => $value){
											$select_city = ($city == $value)?"selected='selected'":"";
?>
												<option value="<?php echo $value;?>" <?php echo $select_city?>><?php echo $value;?></option>
<?php
										}
?>
	                                    	</select>
	                                    </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="caption"></div>
                                    <div class="field" style="text-align:left">
                                        <input type="submit" value="Search" name="search" class="button"/>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->
            		</div> <!-- End .content-box-content -->
					<?php
                    	if(isset($_GET['search'])){
                    ?>
        			<div class="content-box-content loadingContent" style="min-height: 300px;">
								<span style="float:right;"><button class="button printThis">Print</button></span>
	          		<div id="bodyTab" class="printTable" style="display: none;">
	                      <div style="text-align:center;margin-bottom:0px;" class="pageHeader">
	                          <p style="font-size:18px;padding:0;text-align:left;font-weight:normal;"><?php echo $companyTitle; ?> <?php echo ($city !='')?" - ".$city:"";?></p>
	                          <p style="text-align: left;">Trial Balance <?php echo ($accCode != '')?$objTrialBalance->getAccountTitle($accCode):""; ?>
	                          	From <u><?php echo date('d-m-Y',strtotime($fromDate)); ?></u> To <u><?php echo date('d-m-Y',strtotime($toDate)); ?> </u>  </p>
	                          <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
	                      </div>
	                      <table class="tableBreak">
	                          <thead class="tHeader">
	                              <tr>
	                                 <th style="text-align:center;border:1px solid #ddd;wdth:20%;" rowspan="2">Code</th>
	                                 <th style="text-align:center;border:1px solid #ddd;wdth:20%;" rowspan="2">A/c Title</th>
	                                 <th style="text-align:center;border:1px solid #ddd;wdth:20%" colspan="2">Openning</th>
	                                 <th style="text-align:center;border:1px solid #ddd;wdth:20%" colspan="2">Period</th>
	                                 <th style="text-align:center;border:1px solid #ddd;wdth:20%" colspan="2">Closing</th>
	                              </tr>
	                              <tr>
	                                 <th style="text-align:center;border:1px solid #ddd;">Dr</th>
	                                 <th style="text-align:center;border:1px solid #ddd;">Cr</th>
	                                 <th style="text-align:center;border:1px solid #ddd;">Dr</th>
	                                 <th style="text-align:center;border:1px solid #ddd;">Cr</th>
	                                 <th style="text-align:center;border:1px solid #ddd;">Dr</th>
	                                 <th style="text-align:center;border:1px solid #ddd;">Cr</th>
	                              </tr>
	                          </thead>

	                          <tbody>
	<?php
							$cC = 0;
							$cD = 0;
							$cs = 0;
							$ds = 0;
							$oC = 0;
							$oD = 0;
							if(mysql_num_rows($accountsList)){
								$lastThirdCode = '';
								while($row = mysql_fetch_array($accountsList)){
									$cust_detail = $objCustomers->getCustomer($row['ACC_CODE']);

									if($posted_code == '010104'&&$city!=''){
										if($city != $cust_detail['CITY']){
											continue;
										}
									}

									$openingBalance = $objTrialBalance->getTrialOpeningBalance($fromDate,$row['ACC_CODE']);
									$debits         = $objTrialBalance->getAccountDebitsSum($row['ACC_CODE'],$fromDate,$toDate);
									$credits        = $objTrialBalance->getAccountCreditsSum($row['ACC_CODE'],$fromDate,$toDate);
									$closingBalance = $objTrialBalance->getLedgerOpeningBalance($toDate,$row['ACC_CODE']);

									$periodBalance  = $closingBalance - $openingBalance;

									$openingBalance = round($openingBalance,2);
									$debits         = round($debits,2);
									$credits        = round($credits,2);
									$periodBalance  = round($periodBalance,2);
									$closingBalance = round($closingBalance,2);

									$account_type = substr($row['ACC_CODE'],0,2);

									//Check Opening Balance
									if($openingBalance >= 0 && ($account_type == '01' || $account_type == '03')){
										$openingDebit = $openingBalance;
										$openingCredit= '';
									}
									if($openingBalance < 0 && ($account_type == '01' || $account_type == '03')){
										$openingDebit  = '';
										$openingCredit = $openingBalance;
									}

									if($openingBalance < 0 && ($account_type == '02' || $account_type == '04' || $account_type == '05')){
										$openingDebit = $openingBalance;
										$openingCredit= '';
									}
									if($openingBalance >= 0 && ($account_type == '02' || $account_type == '04' || $account_type == '05')){
										$openingCredit = $openingBalance;
										$openingDebit  = '';
									}

									//check Closing Balance
									if($closingBalance >= 0 && ($account_type == '01' || $account_type == '03')){
										$closingDebit = $closingBalance;
										$closingCredit= '';
									}
									if($closingBalance < 0 && ($account_type == '01' || $account_type == '03')){
										$closingDebit  = '';
										$closingCredit = $closingBalance;
									}

									if($closingBalance < 0 && ($account_type == '02' || $account_type == '04' || $account_type == '05')){
										$closingDebit = $closingBalance;
										$closingCredit= '';
									}
									if($closingBalance >= 0 && ($account_type == '02' || $account_type == '04' || $account_type == '05')){
										$closingCredit = $closingBalance;
										$closingDebit  = '';
									}

									if(isset($lastThirdCode)&& $lastThirdCode !== substr($row['ACC_CODE'],0,6)){
	?>
	                                          <tr style="background-color: #6CC;" class="ac_title_row">
	                                          	<th style="text-align:left;background-color: #FFF;color:#000;font-weight: bold;"><?php echo substr($row['ACC_CODE'],0,6); ?></th>
	                                              <th style="text-align:left;background-color: #FFF;color:#000;font-weight: bold;font-size:12px !important;"><?php echo $objTrialBalance->getAccountTitle(substr($row['ACC_CODE'],0,6)); ?></th>
	                                              <th style="text-align:left;background-color: #FFF;color:#000;font-weight: bold;" colspan="7"></th>
	                                          </tr>
	<?php
									}
	?>
	                              <tr class="lol redRowBreak theCommaRow">
	                                 <td><?php echo $row['ACC_CODE']; ?></td>
	                                 <td style="font-size:12px !important;"><a target="_blank" href="ledger-details.php?accountCode=<?php echo $row['ACC_CODE']; ?>&fromDate=&toDate=<?=date('d-m-Y'); ?>&generalReport=Search"><?php echo $row['ACC_TITLE']; ?></a></td>
	                                 <td style="text-align:right" class="drOpening">
							   		<?php echo ($openingDebit == 0)?"&nbsp;":number_format(str_ireplace('-','',$openingDebit),2); ?>
	                                 </td>
	                                 <td style="text-align:right" class="crOpening">
	                                 		<?php echo ($openingCredit == 0)?"&nbsp;":number_format(str_ireplace('-','',$openingCredit),2); ?>
	                                 </td>
	                                 <td style="text-align:right" class="debits"><?php echo number_format($debits,2); ?></td>
	                                 <td style="text-align:right" class="credits"><?php echo number_format($credits,2); ?></td>
	                                 <td style="text-align:right" class="drClosing">
	                                 		<?php echo ($closingDebit == 0)?"&nbsp;":number_format(str_ireplace('-','',$closingDebit),2); ?>
	                                 </td>
	                                 <td style="text-align:right" class="crClosing">
	                                 		<?php echo ($closingCredit == 0)?"&nbsp;":number_format(str_ireplace('-','',$closingCredit),2); ?>
	                                 </td>
	                              </tr>
	<?php
									$lastThirdCode = substr($row['ACC_CODE'],0,6);

									$oD += str_ireplace('-','',$openingDebit);
									$oC += str_ireplace('-','',$openingCredit);
									$ds += str_ireplace('-','',$debits);
									$cs += str_ireplace('-','',$credits);
									$cD += str_ireplace('-','',$closingDebit);
									$cC += str_ireplace('-','',$closingCredit);
								}
							}
	?>
	                          </tbody>
	                          <tfoot class="tableFooter">
	                          	<tr style="background:none" class="theCommaRow">
	                                 <td colspan="2" style="text-align:right;font-weight:bold">Report Totals:</td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($oD,2); ?></td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($oC,2); ?></td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($ds,2); ?></td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($cs,2); ?></td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($cD,2); ?></td>
	                                 <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"><?php echo number_format($cC,2); ?></td>
	                              </tr>
	                          </tfoot>

	                      </table>
	                      <div style="clear:both;"></div>
	                  </div><!--bodyTab2-->
                        <?php
                            }
                        ?>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>
