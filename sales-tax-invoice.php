<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/sale.php';
	include 'common/classes/sale_details.php';
	include 'common/classes/customers.php';
	include 'common/classes/items.php';
	include('common/classes/services.php');
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	$objCompanyDetails 	   = new CompanyDetails();
	$objSales              = new Sale();
	$objSaleDetails        = new SaleDetails();
	$objCustomers          = new Customers();
	$objItems  	           = new Items();
	$objServices           = new Services();
	$objJournalVoucher     = new JournalVoucher();
	$objConfigs   	       = new Configs();

	//INVOICE_FORMAT Config Size - Header - Duplicate
	$use_cartons           = $objConfigs->get_config('USE_CARTONS');
	$use_cartons           = ($use_cartons == 'Y')?true:false;
	$invoice_format        = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format        = explode('_', $invoice_format);
	$invoice_num 		   		 = $invoice_format[1];
	$invoiceStyleCss       = 'resource/css/invoiceStyle.css';

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;

	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
	$use_taxes           = ($use_taxes=='Y')?true:false;

	$cutomer_balance_array = array();
	$cutomer_balance_array['BALANCE'] = 0;
	$cutomer_balance_array['TYPE']    = '';

	$companyLogo   = $objCompanyDetails->getLogo();
	$company  		 = $objCompanyDetails->getActiveProfile();

	if(isset($_GET['id'])){
		$sale_id 		       = mysql_real_escape_string($_GET['id']);
		$saleDetails 	       = mysql_fetch_array($objSales->getRecordDetails($sale_id));
		$saleDetailList        = $objSaleDetails->getList($sale_id);

		if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010101'){
			$customer_balance      = $objJournalVoucher->getOpeningBalanceOfVoucher($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID'],$saleDetails['SALE_DATE']);
			$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
		}
	}
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/towords.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "<?php echo $invoiceStyleCss; ?>",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$(".printThis").click();
		$("<th class='text-center' width='5%'>HS Code</th>").insertAfter($("table thead tr th").first());
		$(".item_td").each(function(){
			if($(this).next(".hs_code_td").length==0){
				$("<td class='text-center'></td>").insertAfter($(this));
			}
		});
		$(".ledger_view").click(function(){
			var gl_date = $(this).attr('data-date');
			var gl_code = $(this).attr('data-code');
			if(gl_code != ''){
				$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
					if(data == ''){
						var win = window.open('invoice-ledger-print.php','_blank');
						if(win){
							win.focus();
						}else{
							displayMessage('Please allow popups for This Site');
						}
					}
				});
			}
		});
		//$(".printThis").click();
		var amount = parseFloat($("input.total_amount").val())||0;
		amount = toWords(amount);
		amount = amount.slice(0,1).toUpperCase() + amount.slice(1) + "only";
		$(".amount_words").text(amount);
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
	<?php
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $saleDetails['CUSTOMER_NAME'];
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$bill_discount = $saleDetails['SALE_DISCOUNT'];
		?>

		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
					<button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print View </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="clear"></div>
			<div class="printThisDiv" style="position: relative;height:9.7in;">
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint" style="height: 10.4in !important;">
						<div class="invoiceHead" style="width: 100%;margin: 0px auto;">
							<?php
							if($companyLogo != ''){
								?>
								<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
								<?php
							}
							?>
							<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
								<div style="font-size:26px;"> <?php echo $company['NAME']; ?> </div>
								<?php echo $company['ADDRESS']; ?>
								<?php
								/*
								<br />
								<?php echo ($company['CONTACT']=='')?"":"Contact : ".$company['CONTACT']; ?> &nbsp;&nbsp;&nbsp;   <?php echo ($company['EMAIL']=='')?"":"Email : ".$company['EMAIL']; ?>
								*/
								 ?>
								<br />
								Sales Tax # : <?php echo $company['TAX_REG_NO']; ?>
								<br />
								NTN #  : <?php echo $company['NTN_REG_NO']; ?>
								 <br />
							</div>
							<div class="clear"></div>
							<div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
								SALES TAX INVOICE
							</div>

							<div class="clear" style="height: 5px;"></div>
							<?php
							if($invoice_num == '1'){

								$customer['CUST_ACC_TITLE'] = explode(',', $customer['CUST_ACC_TITLE']);
								$customer['CUST_ACC_TITLE'] = implode('<br />', $customer['CUST_ACC_TITLE']);
								?>
								<div class="infoPanel pull-left" style="height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">To : <?php echo $customer['CUST_ACC_TITLE']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['ADDRESS']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['CITY']; ?></span>
									<div class="clear"></div>
									<?php if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010101'){ ?>
										<span class="pull-left" style="padding:0px 0px;font-size:12px;"><?php echo " Sales Tax # ".$customer['TAX_REG_NO']; ?></span>
										<div class="clear"></div>
										<span class="pull-left" style="padding:0px 0px;font-size:12px;"><?php echo " NTN #".$customer['NTN_REG_NO']; ?></span>
										<div class="clear"></div>
									<?php } ?>
								</div><!--partyTitle-->
								<div class="infoPanel pull-right" style="width:200px;height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Serial No. <?php echo $saleDetails['TAX_BILL_NO']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Date. <?php echo $sale_date; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="clear"></div>
								<?php
							}

							if($invoice_num == '2'){
								?>
								<div class="invoPanel">
									<div class="invoPanel">
										<div class="clear"></div>
										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Name: </span>
											<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Address: </span>
											<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">City: </span>
											<span class="text_line"><?php echo $customer['CITY']; ?></span>
										</span>
										<div class="clear"></div>
									</div>
								</div>
								<div class="clear" style="height:10px;"></div>
								<?php
							}
							?>
								<div class="clear"></div>
							</div><!--invoiceHead-->
							<div class="clear"></div>
							<div class="invoiceBody" style="width: 100%;margin: 20px auto 0px auto;">
								<table>
									<thead>
										<tr>
											<th width="20%">Yarn Description</th>
											<?php if($use_cartons){ ?>
												<th width="5%">Cartons</th>
												<th width="5%">PerCarton</th>
												<?php } ?>
												<th width="10%">Quantity In Bags</th>
												<th width="10%">Rate/Bag</th>
												<th width="10%">VALUE EXCLUDING S.TAX (Rs.)</th>
												<?php if($individual_discount){ ?>
													<th width="10%">Dis/Rate</th>
													<?php } ?>
													<?php if($use_taxes){ ?>
														<th width="10%">Sales Tax Rate <b>%</b></th>
														<th width="10%">Sales Tax Payable</th>
														<?php } ?>
														<th width="10%">VALUE INCLUDING S.TAX (Rs.)</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$quantity    = 0;
													$subAmount   = 0;
													$taxAmount   = 0;
													$totalAmount = 0;
													$counter = 1;
													$itemDetail = NULL;
													if(mysql_num_rows($saleDetailList)){
														while($row = mysql_fetch_array($saleDetailList)){
															if($row['ITEM_ID'] != 0){
																$itemDetail = $objItems->getRecordDetails($row['ITEM_ID']);
																$itemName   = $itemDetail['NAME'];
															}else{
																$itemName = $objServices->getTitle($row['SERVICE_ID']);
															}
?>
															<tr>
																<td style="font-size:12px;" class="item_td"><span style="float:left;margin-left:5px;"><?php echo $itemName; ?></span></td>
																<?php if($itemDetail['HS_CODE']!=''){ ?>
																<td class="hs_code_td"><?php echo $itemDetail['HS_CODE']; ?></td>
																<?php } ?>
																<?php if($use_cartons){ ?>
																	<td><?php echo $row['CARTONS']; ?></td>
																	<td><?php echo $row['PER_CARTON']; ?></td>
																	<?php } ?>
																	<td><?php echo ($row['SERVICE_ID'] > 0 && $row['QUANTITY'] == 0)?"1":$row['QUANTITY']; ?></td>
																	<td><?php echo $row['UNIT_PRICE']; ?></td>
																	<td><?php echo number_format($row['UNIT_PRICE']*$row['QUANTITY'],2); ?></td>
																	<?php if($individual_discount){ ?>
																		<td><?php echo $row['SALE_DISCOUNT']; ?> <?php echo ($saleDetails['DISCOUNT_TYPE'] == 'P')?"%":""; ?></td>
																		<?php } ?>
																		<?php if($use_taxes){ ?>
																			<td><?php echo $row['TAX_RATE']; ?></td>
																			<td><?php echo number_format(round($row['TAX_AMOUNT']),2); ?></td>
																			<?php } ?>
																			<td><span style="float:right;margin-right:5px;"><?php echo number_format(round($row['TOTAL_AMOUNT']),2); ?></span></td>
																		</tr>
																		<?php
																		$quantity    += $row['QUANTITY'];
																		$subAmount 	 += $row['SUB_AMOUNT'];
																		$taxAmount   += $row['TAX_AMOUNT'];
																		$totalAmount += $row['TOTAL_AMOUNT'];
																		$counter++;
																	}
																}
																?>
															</tbody>
															<?php
															$colum_skipper = 0;
															$columns       = 3;
															$colum_skipper += (!$individual_discount)?0:1;
															$colum_skipper += (!$use_cartons)?0:2;
															$colum_skipper += (!$use_taxes)?0:1;
															$columns += $colum_skipper;
															?>
															<tfoot>
																<tr>
																	<td colspan="2" class="text-right"><b>Total :</b></td>
																	<?php if($use_cartons){ ?>
																	<td class="text-center"><b>- - -</b></td>
																	<td class="text-center"><b>- - -</b></td>
																	<?php } ?>
																	<td class="text-center"><b><?php echo number_format($quantity,2); ?></b></td>
																	<td class="text-center"><b>- - -</b></td>
																	<td class="text-center"><b><?php echo number_format(round($subAmount),2); ?></b></td>
																	<?php if($individual_discount){ ?>
																	<td class="text-center"><b>- - -</b></td>
																	<?php } ?>
																	<td class="text-center"><b>- - -</b></td>
																	<td class="text-center"><b><?php echo number_format(round($taxAmount),2); ?></b></td>
																	<td class="text-center"><span style="float:right;margin-right:5px;"><b><?php echo number_format(round($totalAmount),2); ?></b></span></td>
																</tr>
															</tfoot>
														</table>
														<input type="hidden" class="total_amount" value="<?php echo $totalAmount; ?>" />
														<div class="clear"></div>

														<div class="infoPanel pull-left" style="position:relative;top:10px;min-height: auto !important;height: auto !important;width: 95% !important;">
															<b>RUPEES</b>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<b class="amount_words"></b>
														</div>
														<div class="clear"></div>

														<?php if($saleDetails['NOTES'] != ''){ ?>
															<div class="infoPanel pull-left" style="position:relative;top:10px;">
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;font-size:14px;border-bottom:1px solid #000;">Notes</span>
																</span>
																<div class="clear" style="height:10px;"></div>
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;;font-size:14px;text-align:justify !important;"><?php echo $saleDetails['NOTES']; ?></span>
																</span>
																<div class="clear"></div>
															</div>
															<?php } ?>
															<div class="clear"></div>
															<div class="auths pull-right absoute_bottom_right">
																Authorized By
															</div><!--partyTitle-->
															<div class="clear"></div>
														</div><!--invoiceBody-->
													</div><!--invoiceLeftPrint-->
													<div class="clear"></div>
												</div><!--invoiceContainer-->
											</div><!--printThisDiv-->
										</div><!--invoiceBody-->
										<?php
									}
									?>
								</body>
								</html>
								<?php include('conn.close.php'); ?>
