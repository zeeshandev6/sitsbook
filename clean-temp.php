<?php
	$out_buffer = ob_start();
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
   <body>
      	<div id="body-wrapper">
         	<div id="bodyWrapper">
            	<div class = "content-box-top" style="padding-bottom: 30px;">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p>Cleaning.....</p>
                  		</div><!-- End .content-box-header -->
                        <div id="form">
														<?php
															echo shell_exec('DEL "%systemroot%\temp\" /Q /S');
															echo shell_exec('DEL "%USERPROFILE%\Local Settings\Temporary Internet Files" /Q /S /F');
															echo shell_exec('DEL "%USERPROFILE%\Local Settings\Temp" /Q /S /F');
															echo shell_exec('DEL "%USERPROFILE%\Local Settings\Historique" /Q /S /F');
															echo shell_exec('DEL "%USERPROFILE%\RECENT" /Q /S /F');
															echo shell_exec('DEL "%USERPROFILE%\cookies" /Q /S /F');
														?>
                        </div><!--form-->
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
        <div id="xfade"></div>
	</body>
</html>
<?php include('conn.close.php'); ?>
