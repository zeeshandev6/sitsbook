<?php
	include('common/connection.php');
  include('common/config.php');
  include('common/classes/accounts.php');
	include('common/classes/emb_inward.php');
	include('common/classes/measure.php');
	include('common/classes/emb_products.php');
	include('common/classes/company_details.php');

  $objChartOfAccounts  = new ChartOfAccounts();
	$objEmbroideryInward = new EmbroideryInward();
	$objMeasures         = new Measures();
	$objEmbProducts 		 = new EmbProducts();
	$objCompanyDetails 	 = new CompanyDetails();

	if(isset($_POST['search'])){
    $objEmbroideryInward->account_code  = mysql_real_escape_string($_POST['cust_acc_code']);
    $objEmbroideryInward->from_date     = ($_POST['from_date']=="")?"":date('Y-m-d',strtotime($_POST['from_date']));
    $objEmbroideryInward->to_date       = ($_POST['to_date']=="")?"":date('Y-m-d',strtotime($_POST['to_date']));
    $objEmbroideryInward->gate_pass_num = "";
    $objEmbroideryInward->lot_num       = mysql_real_escape_string($_POST['lotNum']);
		if($objEmbroideryInward->from_date == ''){
			$objEmbroideryInward->from_date = date('01-01-Y');
		}
    $emb_inward_report = $objEmbroideryInward->getsSpecificInward();

		$company_details        = $objCompanyDetails->getActiveProfile();
	}
  $customersList = $objEmbroideryInward->getCustomerslist();
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
		<link rel="stylesheet" href="resource/css/reports-horizontal.css" type="text/css"  		     		 />
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
		<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
		<!-- jQuery -->
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
  	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Embroidery Inward Report</p>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <form method="post" action="">
                            <div class="caption"></div>
                            <div class="redColor"><?php echo (isset($message))?$message:""; ?></div>
                            <div class="clear"></div>
                            <div class="caption">Customer</div>
                            <div class="field" style="width:300px;">
                                <select class="selectpicker show-tick form-control" data-live-search="true" name="cust_acc_code">
                                <option selected value=""></option>
<?php
                            if(mysql_num_rows($customersList)){
                                while($account = mysql_fetch_array($customersList)){
?>
                                <option value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Lot No</div>
                            <div class="field">
                                <input type="text" value="" name="lotNum" class="form-control" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">From Date</div>
                            <div class="field">
                                <input type="text" value="<?php echo (isset($objEmbroideryInward->from_date))?date('d-m-Y',strtotime($objEmbroideryInward->from_date)):""; ?>" name="from_date" class="form-control datepicker" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">To Date</div>
                            <div class="field">
                                <input type="text" value="<?php echo (isset($objEmbroideryInward->to_date))?date('d-m-Y',strtotime($objEmbroideryInward->to_date)):date('d-m-Y'); ?>" name="to_date" class="form-control datepicker" />
                            </div>
                            <div class="clear"></div>
                                <input type="hidden" name="custAccCode" value="" />
                            <div class="caption"></div>
                            <div class="field">
                                <input type="submit" value="Search" name="search" class="button"/>
                            </div>
                            <div class="clear"></div>
                        </form>
										</div><!--form-->
                	<div class="clear"></div>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
            <div class="content-box-content" id="PrintMe">
			<?php
               	if(isset($emb_inward_report)){
            ?>
            	<span style="float:right;"><button class="button printThis">Print</button></span>
            	<div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    <div class="pageHeader">

											<div class="logo_aside">
												<h1 class="h1"><?php echo $company_details['NAME']; ?></h1>
				                <p class="slogan">
				                  Cloth Inward Report
				                </p>
				                <p class="underSlogan">
				                  <?php echo ($objEmbroideryInward->from_date=="")?"From Begining ":"From Period ".date('d-m-Y',strtotime($objEmbroideryInward->from_date)); ?>  <?php echo ($objEmbroideryInward->to_date=="")?" To Period ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objEmbroideryInward->to_date)); ?>
				                </p>
											</div>
											<div class="clear"></div>
											<p class="repoDate" style="text-align:left !important;">Report Generated On : <?php echo date('d-m-Y'); ?></p>
                    </div>
                    <div class="clear"></div>
                    <table class='tableBreak'>
                        <thead class="tHeader">
                            <tr style="background:#EEE;">
                               <th width="8%"  class="text-center">InwDate</th>
                               <th width="15%" class="text-center">Customer</th>
                               <th width="6%"  class="text-center">Lot No</th>
															 <th width="10%"  class="text-center">Product</th>
                               <th width="10%"  class="text-center">Fab.Quality</th>
                               <th width="9%"  class="text-center">Colour</th>
                               <th width="5%"  class="text-center">Than</th>
                               <th width="5%"  class="text-center">M/Length</th>
                               <th width="7%"  class="text-center">Measure</th>
                               <th width="7%" class="text-center">TotalThan</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
						$total_thaan = 0;
						$total_amount = 0;
						if(mysql_num_rows($emb_inward_report)){
							$lastCustCode = "";

							while($row = mysql_fetch_array($emb_inward_report)){
								$detailsQuery = $objEmbroideryInward->getDetailsById($row['INWARD_ID']);
										$measurement = $objMeasures->getName($row['MEASURE_ID']);
?>
                            <tr id="recordPanel" class="alt-row">
                                <td class="text-center "><?php echo date('d-m-Y',strtotime($row['INWD_DATE'])); ?></td>
                                <td class="text-left pl-10"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['ACCOUNT_CODE']); ?></td>
                                <td class="text-center "><?php echo $row['LOT_NO']; ?></td>
																<td class="text-left pl-10"><?php echo $objEmbProducts->getTitle($row['PRODUCT_ID']); ?></td>
                                <td class="text-left pl-10"><?php echo $row['QUALITY']; ?></td>
                                <td class="text-left pl-10"><?php echo $row['COLOR']; ?></td>
                                <td class="text-center thanColumn"><?php echo $row['THAN']; ?></td>
                                <td class="text-center "><?php echo $row['TOTAL_LENGTH']/$row['THAN']; ?></td>
                                <td class="text-center "><?php echo $measurement; ?></td>
                                <td class="text-right pr-10 thanLengthColumn"><?php echo $row['TOTAL_LENGTH']; ?></td>
                            </tr>
<?php
										$lastCustCode = $row['ACCOUNT_CODE'];
										$total_thaan  += $row['THAN'];
										$total_amount += $row['TOTAL_LENGTH'];
?>
<?php
							}
						}
?>
                        </tbody>
                        <tfoot class='tableFooter'>
                            <tr>
                                <td class="text-right" colspan="6"> <b>Total:</b> </td>
                                <td class="text-center" ><?php echo $total_thaan; ?></td>
                                <td class="text-center" >- - -</td>
                                <td class="text-center" >- - -</td>
                                <td class="text-right pr-10" ><?php echo $total_amount; ?></td>
                            </tr>
                        </tfoot>
                    </table><!--end table 1-->
                </div> <!--End bodyTab-->
                <div class="clear"></div>
<?php
				}
?>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script>
	$(window).load(function(){
		$(".printThis").click(function(){
			if($("div.tablePage").length==0){
				var MaxHeight = 580;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if (RunningHeight + $(this).height() > MaxHeight) {
						RunningHeight = 0;
						PageNo += 1;
					}
					if(PageNo > 1){
						MaxHeight = 640;
					}
					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate    = $(".repoDate").text();
				//remove previous thead/tfoot/ReportDate
				$(".tHeader").remove();
				$(".repoDate").remove();
				$(".tableFooter").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					$('table.tableBreak').parent().append("<div class='clear'></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					//$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$("div.tablePage").each(function(){
					//$("div.pageHeader").first().clone().insertBefore($(this).find("table.newTable"));
					$(this).find("table.newTable thead").html(tableHeader);
				});
				//$("div.pageHeader").first().remove();
				$('table.tableBreak').remove();
			}
			$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				   loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Emque Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
		});
	});
</script>
