<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/banks.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/sale_orders.php');
  include('common/classes/sale_order_details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/sms_templates.php');
	include('common/classes/sitsbook_sms_api.php');

	//Permission
	if(!in_array('mobile-sales',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts     = new ChartOfAccounts();
	$objBanks 							= new Banks();
	$objCustomers						= new Customers();
	$objScanPurchaseDetails	= new ScanPurchaseDetails();
	$objScanSale   	 	    	= new ScanSale();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objSmsTemplates       	= new SmsTemplates();
	$objSitsbookSmsApi     	= new SitsbookSmsApi();
	$objSaleOrders         	= new SaleOrders();
  $objSaleOrderDetails   	= new SaleOrderDetails();
	$objItems            		= new Items();
	$objItemCategory        = new itemCategory();
	$objDepartments         = new Departments();
	$objConfigs 						= new Configs();

	$sms_config    					= $objConfigs->get_config('SMS');
	if(isset($_POST['send_sms_by_id'])){
		if($sms_config == 'N'){
			exit();
		}
		$sale_id 			= (int)mysql_real_escape_string($_POST['send_sms_by_id']);
		$sale_details = $objScanSale->getDetail($sale_id);

		if(substr($sale_details['CUST_ACC_CODE'],0,6) == '010104'){
			$mobile_number = $objCustomers->getMobileIfActive($sale_details['CUST_ACC_CODE']);
		}
		if($mobile_number==''){
			echo "Error! Mobile number not available.";
			exit();
		}
		$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
		$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');
		$sale_date 		     = date("d-m-Y",strtotime($sale_details['SALE_DATE']));
		$account_code      = $sale_details['CUST_ACC_CODE'];
		$amount 					 = $objScanSale->getCustomerAmountFromVoucher($sale_id);
		$account_title 		 = $objChartOfAccounts->getAccountTitleByCode($sale_details['CUST_ACC_CODE']);
		$sms_template      = $objSmsTemplates->get_template(6);
		$sms_body          = $sms_template['SMS_BODY'];

		$sms_body          = str_replace("[DATE]",$sale_date,$sms_body);
		$sms_body          = str_replace("[AMOUNT]",$amount,$sms_body);
		$sms_body          = str_replace("[ACCOUNT_TITLE]",$account_title,$sms_body);
		$sms_body          = str_replace("[SALE_TYPE]",'A/c',$sms_body);
		$no_of_sms         = 0;
		$acc_id  	         = 0;
		$sent_status       = $objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
    if(stripos($sent_status,'Successfully')!==false){
			$objScanSale->setSmsStatus($sale_id,'Y');
		}
    echo $sent_status;
		exit();
	}

	if(isset($_POST['get_bank_rate'])){
		$bank_acc_code = mysql_real_escape_string($_POST['get_bank_rate']);
		$bank_details = $objBanks->getDetailByAccCode($bank_acc_code);
		echo json_encode($bank_details);
		exit();
	}
	$show_tax 		  = $objConfigs->get_config('SHOW_TAX');
	$show_discount 	= $objConfigs->get_config('SHOW_DISCOUNT');
	$invoice_msg    = $objConfigs->get_config('INVOICE_MSG');
	$user_name      = $objConfigs->get_config("USER_NAME");
	$sms_config 		= $objConfigs->get_config('SMS');
	if(isset($_POST['mobile_number'])){
		if($sms_config == 'N'){
			echo $sms_config;
			exit();
		}

		$mobile_number = $_POST['mobile_number'];
		$sale_id       = $_POST['sale_id'];

		$saleDetails   = $objScanSale->getDetail($sale_id);
		$saleItems     = $objScanSaleDetails->getList($sale_id);

		$sms_search_vars 	 = array();
		$sms_search_vars[] = '[INVOICE]';
		$sms_search_vars[] = '[DATE]';
		$sms_search_vars[] = '[AMOUNT]';

		$sms_replace_vars   = array();
		$sms_replace_vars[] = $saleDetails['BILL_NO'];
		$sms_replace_vars[] = date('d-m-Y',strtotime($saleDetails['SALE_DATE']));
		$sms_replace_vars[] = $saleDetails['TOTAL_AMOUNT'];

		$invoice_msg = str_replace($sms_search_vars, $sms_replace_vars, $invoice_msg);

		$options = array('location' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/soap-server.php',
		'uri' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/');
		$api = new SoapClient(NULL, $options);
		try{
			echo $api->process($user_name,"N","",$mobile_number,$invoice_msg,0,0);
		}catch(Exception $e){
			echo "N";
		}
		exit();
	}

	$scan_bill         = NULL;
	$scan_bill_details = NULL;
	$newBillNum        = $objScanSale->genBillNumber();

	$currency_type     = $objConfigs->get_config('CURRENCY_TYPE');

	if(isset($_GET['id'])){
		$id 	   		   		 = mysql_real_escape_string($_GET['id']);
		$scan_bill 		     = $objScanSale->getDetail($id);
		$scan_bill_details = $objScanSaleDetails->getList($id);
	}
	$customersList 	 = $objCustomers->getList();
	$item_categories = $objItems->getCategoryListFull();

	$invoice_type		 = $objConfigs->get_config("SCAN_INVOICE_FORMAT");

	if($invoice_type == 'L_1'){
		$invoice_url = 'mobile-sale-invoice.php?';
	}
	if($invoice_type == 'S_1'){
		$invoice_url = 'mobile-sales-invoice-small.php?';
	}
	if($invoice_type == 'M_1'){
		$invoice_url = 'mobile-sales-invoice-duplicates.php?';
	}
	$order_no 				= 0;
	$selected_account = '';
	if(isset($_GET['customer_code'])){
		$selected_account = $_GET['customer_code']!=''?mysql_real_escape_string($_GET['customer_code']):"";
	}
	if(isset($_GET['saleorder'])){
    $order_no 					= (int)(mysql_real_escape_string($_GET['saleorder']));
    $sale_order_rows  	= $objSaleOrderDetails->getListByOrderNumber($order_no);
  }
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mobile Sales</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	th,td{
		text-align: center;
		font-size:  14px !important;
	}
	.barcode_me.recorrect{
		background-color: rgba(255, 112, 0, 0.5);
		color: #FFF;
	}
	.existed{
		background-color: rgba(255,0,0,0.1);
		color: #000;
		font-weight: bold;
	}
	tfoot tr td{
		border: 1px solid #E5E5E5 !important;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/scan.sale.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
		var show_discount,show_tax;
		$(document).ready(function(){
			show_discount = '<?php echo $show_discount; ?>';
			show_tax 	  = '<?php echo $show_tax; ?>';
			$("input[name='billNum']").numericOnly();
			$("#datepicker").setFocusTo("input[name='billNum']");
			$("input[name='billNum']").setFocusTo("div.supplierSelector button");
			$("input[name='gpNum']").focus();
			$(document).keydown(function(e){
				if(e.keyCode == 32 && e.ctrlKey){
					$(".trasactionType").click();
					$(".supplier_name").focus();
				}
			});
			if($("tr[data-cust-name]").length){
				$("input.supplier_name").val($("tr[data-cust-name]").attr('data-cust-name'));
			}
			$('.supplierSelector').selectpicker();
			$(".barcodes_table *").on('change remove',function(){
				$("#reset_bt").show();
			});
			$("#reset_bt").click(function(){
				$("#xfade").show();
			});
			$("input[readonly]").prop("tabindex",'-1');
			$("table.barcodes_table td.discount_td input").blur();
			<?php
			if(isset($_GET['print'])){
				?>
				var src = $("#printstart").attr("href");
				var win = window.open(src);
				if(win){
					win.focus();
				}else{
					alert('Please allow popups for this site');
				}

				<?php
			}
			?>
			$("input[name=radiog_dark]").change(function(){
				if($("input[name=radiog_dark]:checked").val() == 'B'){
					get_bank_rate();
				}
			});
			$("select.supplierSelector").change(function(){
				if($("input[name=radiog_dark]:checked").val() == 'B'){
					get_bank_rate();
				}
			});
			$(window).keydown(function(e){
				if(e.altKey == true&&e.keyCode == 73){
					e.preventDefault();
					$("#form").click();
					$("div.itemSelector button").click();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 65){
					e.preventDefault();
					$("#form").click();
					$("div.supplierSelector button").click();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 66){
					e.preventDefault();
					$("#form").click();
					$("input.barcode_put").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 67){
					e.preventDefault();
					$("#form").click();
					$("input.supplier_name").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 77){
					e.preventDefault();
					$("#form").click();
					$("input.customer_mobile").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 78){
					e.preventDefault();
					$("#form").click();
					$("textarea.inv_notes").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 68){
					e.preventDefault();
					$("#form").click();
					$("input.whole_discount").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 79){
					e.preventDefault();
					$("#form").click();
					$("input.inv_charges").focus();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 83){
					e.preventDefault();
					$("#form").click();
					$(".save_scan").click();
					return false;
				}
				if(e.altKey == true&&e.keyCode == 80){
					e.preventDefault();
					$("#form").click();
					$(".save_scan_and_print").click();
					return false;
				}
			});
		});
	</script>
</head>

<body class="body_menu_close">
	<input type="hidden" class="discount_type" value="<?php echo $discountType; ?>" />
	<input type="hidden" class="sale_order_no" value="<?php echo $order_no; ?>" />
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top" style="left: 40px;">
			<div class="content-box-header">
				<p>Sales Panel</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<a href="mobile-sale.php?tab=list"><div class="tab">List</div></a>
						<a href="mobile-sale.php?tab=search"><div class="tab">Search</div></a>
						<div class="tabSelected">Details</div>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content" style="padding: 5px;" >
				<div id="bodyTab1">
					<div class="clear"></div>
					<div id="form" style="float:none;margin:10px 15px;">
						<div class="myCat"></div>
						<div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Sale Date</div>
						<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
							<input type="text" name="rDate" id="datepicker" value="<?php echo $scan_bill['SALE_DATE']!=''?date("d-m-Y",strtotime($scan_bill['SALE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
						</div>
						<div class="caption" style="width:50px;margin-left:0px;">Bill#</div>
						<div class="field" style="width:100px">
							<input type="text" name="billNum" value="<?php echo ($scan_bill != NULL)?$scan_bill['BILL_NO']:$newBillNum; ?>" class="form-control text-center" style="width:100px" <?php echo ($scan_bill == NULL)?"":"readonly"; ?> />
						</div>
						<div class="clear"></div>

						<div class="caption" style="width:80px">Account</div>
						<div class="field" style="width:270px;position:relative;">
							<a href="#" onclick="add_supplier();" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
							<select class="supplierSelector form-control show-tick"
							data-style="btn-default" data-width="230"
							data-live-search="true" data-hide-disabled='true' style="border:none;" >
							<option selected value=""></option>
							<?php
							if(mysql_num_rows($customersList)){
								$disabled = '';
								while($account = mysql_fetch_array($customersList)){
									$selected = ((isset($scan_bill)&&$scan_bill['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])||($selected_account==$account['CUST_ACC_CODE']))?"selected":"";
									if($scan_bill != NULL){
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":"";
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
									}else{
										if($selected_account==''){
											$disabled = "disabled";
										}
									}
									?>
									<option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" ><?php echo $account['CUST_ACC_TITLE']; ?></option>
									<?php
								}
							}
							?>
							<?php
							if(mysql_num_rows($cash_in_hand_list)){
								$disabled = '';
								while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
									$cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
									if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
										continue;
									}
									if($scan_bill != NULL){
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
									}else{
										if($selected_account!=''){
											$disabled = "disabled";
										}
									}
									if($scan_bill != NULL){
										$scan_bill['CUST_ACC_CODE'];
										$selected = ($scan_bill['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected=\"selected\"":"";
									}else{
										$selected = ($cash_in_hand_acc_code==$cash_rows['CASH_ACC_CODE'])?"selected":"";
									}
									if($selected_account != ''){
										$selected = '';
									}
									?>
									<option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
									<?php
								}
							}
							?>
							<?php
							if(mysql_num_rows($cash_in_bank_list)&&$admin){
								$disabled = '';
								while($cash_rows = mysql_fetch_array($cash_in_bank_list)){
									if($scan_bill != NULL){
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
										$disabled = (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":$disabled;
									}else{
										$disabled = "disabled";
									}
									$selected = (isset($scan_bill)&&$scan_bill['CUST_ACC_CODE']==$cash_rows['ACC_CODE'])?"selected='selected'":"";
									?>
									<option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['ACC_CODE']; ?>" value="<?php echo $cash_rows['ACC_CODE']; ?>"><?php echo $cash_rows['ACC_TITLE']; ?></option>
									<?php
								}
							}
							?>
						</select>
						<input type="hidden" class="bills-cash-account" value="<?php echo $scan_bill['CUST_ACC_CODE']; ?>" />
						<input type="hidden" class="sale_id" value="<?php echo $scan_bill['ID']; ?>" />
						<input type="hidden" class="discount_type" value="<?php echo $discountType; ?>" />
						<input type="hidden" class="selected_account" value="<?php echo $selected_account; ?>" />
					</div>

					<div class="caption" style="width: 550px;">
						<div class="pull-left" style="padding-top:5px;">
							<input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="A" id="radio1" <?php echo ((substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010104')||$selected_account!='')?"checked":''; ?> class="css-checkbox" />
							<label for="radio1" class="css-label-radio radGroup1">Account</label>
							<input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="C" id="radio2" <?php echo ((substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010101')||($scan_bill == NULL&&$selected_account==''))?"checked":''; ?> class="css-checkbox" />
							<label for="radio2" class="css-label-radio radGroup1">Cash</label>
							<input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="B" id="radio3" <?php echo (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010102')?"checked=\"checked\"":''; ?> class="css-checkbox" />
							<label for="radio3" class="css-label-radio radGroup1">Card</label>
						</div>
						<div class="pull-left" style="margin-left:10px;">
							<input type="text" name="supplier_name"  value="<?php echo $scan_bill['CUSTOMER_NAME']; ?>" class="form-control supplier_name" style="width:190px;text-align:center;display:<?php echo (substr($scan_bill['CUST_ACC_CODE'], 0,6)=='010104')?"none":""; ?>;" placeholder="Customer Name" />
						</div>
						<div class="pull-left merch_rate_div m-5">
							<input id="cmn-toggle-card-charges" value="BD" class="css-checkbox card_charges_applies" type="checkbox" <?php echo ($scan_bill['MERCH_RATE'] == 'Y')?"checked":""; ?> />
							<label for="cmn-toggle-card-charges" class="css-label" style="margin-right:-20px;"></label>
						</div>
						<?php if($objConfigs->get_config('SMS') == 'Y'){ ?>
							<div class="pull-left" style="margin-left:10px;width:125px;">
								<input type="text" class="customer_mobile form-control text-center" value="<?php echo $scan_bill['MOBILE']; ?>" placeholder="Mobile Number" maxlength="11" />
							</div>
							<?php } ?>
						</div>
						<div class="clear"></div>

						<div style="height: 10px;"></div>
						<div class="panel panel-default" style="padding:0em;border:none;box-shadow:none;">
							<div id="form" class="scanner_div" style="text-align: center;margin:0px;margin-top:10px;;">
								<input type="text" class="barcode_put form-control text-center" value="" placeholder="Scan Barcode" style="height:50px;font-size:26px;">
							</div> <!-- End form -->
							<div class="clear"></div>
						</div>
						<table style="width: 100%;" class="barcodes_table">
							<thead>
								<tr>
									<th width="5%">Sr.</th>
									<th width="15%">Barcode</th>
									<th width="10%">Company</th>
									<th width="15%">Item</th>
									<th width="5%">Stock</th>
									<th width="15%">Price</th>
									<?php if($show_discount=='Y'){ ?>
									<th width="7%">Discount</th>
									<?php } ?>
									<?php if($show_tax=='Y'){ ?>
									<th width="7%">Tax</th>
									<?php } ?>
									<th width="15%">Sale Price(<?php echo $currency_type; ?>)</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($scan_bill_details != NULL && mysql_num_rows($scan_bill_details)){
									while($row = mysql_fetch_array($scan_bill_details)){
										$sp_detail    	  = $objScanPurchaseDetails->getDetails($row['SP_DETAIL_ID']);
										$item_was_retuend = $objScanSaleDetails->checkForReturnedAgainstCurrentBill($row['ID']);
										$item_details 	  = $objItems->getRecordDetails($sp_detail['ITEM_ID']);
										$status = '';

										if($item_was_retuend){
											$sp_detail['STOCK_STATUS'] = "A";
										}

										$status = ($sp_detail['STOCK_STATUS'] == 'S')?"Sold":$status;
										$status = ($sp_detail['STOCK_STATUS'] == 'R')?"Returned":$status;
										$status = ($sp_detail['STOCK_STATUS'] == 'A')?"(InStock)":$status;
										?>
										<tr class="barcode_me dynamic" data-sp-id="<?php echo $row['SP_DETAIL_ID']; ?>" row-id="<?php echo $row['ID']; ?>">
											<td class="sr_td text-center"></td>
											<td><?php echo $sp_detail['BARCODE']; ?></td>
											<td><?php echo $item_details['COMPANY']; ?></td>
											<td><?php echo $item_details['NAME']." - ".$sp_detail['COLOUR']; ?></td>
											<td></td>
											<td type="text" class="sale_td"><input value="<?php echo $row['PRICE']; ?>" class="form-control text-center" /></td>
											<?php if($show_discount=='Y'){ ?>
											<td type="text" class="discount_td"><input value="<?php echo $row['DISCOUNT']; ?>" class="form-control text-center" /></td>
											<?php } ?>
											<?php if($show_tax=='Y'){ ?>
											<td type="text" class="tax_td"><input value="<?php echo $row['TAX']; ?>" class="form-control text-center" /></td>
											<?php } ?>
											<td type="text" class="price_td"><input value="<?php echo $row['SALE_PRICE']; ?>" class="form-control text-center" readonly /></td>
											<td>
												<?php if($sp_detail['STOCK_STATUS']!='A'){ ?>
													<a class="pointer"><i class="fa fa-times"></i></a>
													<?php }else{
														echo $status;
													} ?>
												</td>
											</tr>
											<?php
										}
									}
?>
<?php
												if(isset($sale_order_rows)&&mysql_num_rows($sale_order_rows)){
													$included_spds  		= array();
													while($order_row 		= mysql_fetch_assoc($sale_order_rows)){
														$sp_detail    	  = $objScanPurchaseDetails->getDetailsBySaleOrderItem($order_row['PRODUCT_ID'],$included_spds);
														if($sp_detail['ID']!=''){
															$included_spds[]  = $sp_detail['ID'];
														}
														$item_details 	  = $objItems->getRecordDetails($order_row['PRODUCT_ID']);
														$status 					= '';
														$status 					= ($sp_detail['STOCK_STATUS'] == 'S')?"Sold":$status;
														$status 					= ($sp_detail['STOCK_STATUS'] == 'R')?"Returned":$status;
														$status 					= ($sp_detail['STOCK_STATUS'] == 'A')?"(InStock)":$status;
?>
											<tr class="barcode_me dynamic" data-sp-id="<?php echo $sp_detail['ID']; ?>" row-id="0" data-cust-name="<?php echo $order_row['CUSTOMER_NAME']; ?>">
												<td class="sr_td text-center"></td>
												<td><?php echo $sp_detail['BARCODE']; ?></td>
												<td><?php echo $item_details['COMPANY']; ?></td>
												<td class="<?php echo ($sp_detail==NULL)?"bg-danger":""; ?>"><?php echo $item_details['NAME']." - ".$sp_detail['COLOUR']; ?></td>
												<td></td>
												<td type="text" class="sale_td"><input class="form-control text-center" value="<?php echo $order_row['AMOUNT']; ?>" /></td>
												<?php if($show_discount=='Y'){ ?>
												<td type="text" class="discount_td"><input value="" class="form-control text-center" /></td>
												<?php } ?>
												<?php if($show_tax=='Y'){ ?>
												<td type="text" class="tax_td"><input value="" class="form-control text-center" /></td>
												<?php } ?>
												<td type="text" class="price_td"><input value="" class="form-control text-center" readonly /></td>
												<td class="<?php echo ($sp_detail==NULL)?"bg-danger":""; ?>">
													<?php echo ($sp_detail==NULL)?"NiS":""; ?>
													<?php if($sp_detail['STOCK_STATUS']!='A'){ ?>

														<?php }else{
															echo $status;
														} ?>
													</td>
												</tr>
<?php
													}
												}
?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="4">Total</td>
										<td></td>
										<td><input tabindex="-1" class="form-control total_sale_price text-center" readonly="readonly" /></td>
										<?php if($show_discount=='Y'){ ?>
										<td><input tabindex="-1" class="form-control total_discount text-center" readonly="readonly" /></td>
										<?php } ?>
										<?php if($show_tax=='Y'){ ?>
										<td><input tabindex="-1" class="form-control total_tax text-center" readonly="readonly" /></td>
										<?php } ?>
										<td><input class="form-control total_amount text-center" readonly="readonly" /></td>
										<td></td>
									</tr>
								</tfoot>
							</table>
							<div class="clear" style="height: 20px;"></div>
							<div class="pull-right" style="padding: 0em 1em;width:auto;">
								<div class="pull-right">

									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
									<div class="caption" style="width: 150px;margin-left:0px;">
											<input type="text" class="form-control text-right delivery_charges"  value="<?php echo $scan_bill['DELIVERY_CHARGES']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
									<div class="caption" style="width: 150px;margin-left:0px;">
										<input type="text" class="form-control text-right grand_total" readonly="readonly" />
									</div>
									<div class="clear"></div>

									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Tendered :</div>
									<div class="caption" style="width: 150px;margin-left:0px;">
										<input type="text" class="form-control text-right received_cash" value="<?php echo $scan_bill['RECEIVED_CASH']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="return-to-customer">
										<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Change Given :</div>
										<div class="caption" style="width: 150px;margin-left:0px;">
											<input type="text" class="form-control text-right change_return" value="<?php echo $scan_bill['CHANGE_RETURNED']; ?>" readonly />
										</div>
										<div class="clear"></div>
									</div>

									<div class="remains-to-customer" style="display:<?php echo ((substr($scan_bill['CUST_ACC_CODE'], 0,6)!='010101')||$scan_bill==NULL)?"":"none"; ?>;">
										<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Receivable :</div>
										<div class="caption" style="width: 150px;margin-left:0px;">
											<input type="text" class="form-control text-right remaining_amount" value="<?php echo $scan_bill['REMAINING_AMOUNT']; ?>" readonly />
										</div>
										<div class="clear"></div>
									</div>

									<div class="recovered-from-customer">
										<div class="caption" style="padding: 5px;width: 180px;height: 30px;">
											Balance Recovered :
											<input id="cmn-toggle-recovered" value="X" class="css-checkbox recovered_balance" onclick="return recover_balance(this);" type="checkbox" <?php echo ($scan_bill['RECOVERED_BALANCE'] == 'Y')?"checked":""; ?> />
											<label for="cmn-toggle-recovered" class="css-label" style="margin-right:-20px;"></label>
										</div>
										<div class="caption" style="width: 150px;margin-left:0px;">
											<input type="text" class="form-control text-right recovery_amount" value="<?php echo $scan_bill['RECOVERY_AMOUNT']; ?>" readonly />
										</div>
										<div class="clear"></div>
									</div>

									<div class="balance-of-customer">
										<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Balance Receivable :</div>
										<div class="caption" style="width: 150px;margin-left:0px;">
											<input type="text" class="form-control customer-balance text-right" readonly />
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
							<div class="clear" style="height: 20px;"></div>

							<input type="hidden" class="scan_sale_id" value="<?php echo ($scan_bill==NULL)?"0":$id; ?>" />
							<div class="button pull-right" onclick="window.location.href='mobile-sale-details.php';">New Form</div>
							<?php if(($scan_bill != NULL && in_array('modify-mobile-sales',$permissionz)) || $admin == true || $scan_bill == NULL){ ?>
								<button class="button pull-right save_scan_and_print mr-10"><?php echo ($scan_bill==NULL)?"Save":"Update"; ?>  &amp; Print</button>
								<button class="button pull-right save_scan mr-10"><?php echo ($scan_bill==NULL)?"Save":"Update"; ?></button>
								<?php } ?>
								<?php
								if($scan_bill!=NULL){
									?>
									<?php if($sms_config == 'Y' && ((int)($scan_bill['ID'])) > 0){ ?>
									<button class="button pull-left" onclick="send_sms_form(this,<?php echo $scan_bill['ID'] ?>);"> <i class="fa fa-envelope"></i> Send SMS</button>
									<?php } ?>
									<a class="button pull-right mr-10" id="printstart" target="_blank" href="<?php echo $invoice_url; ?>&id=<?php echo $id; ?>"><i class="fa fa-print"></i> Print</a>
									<a class="button button-green pull-right mr-10" id="printstart" target="_blank" href="open-mobile-sale-return.php?sale=<?php echo $id; ?>"><i class="fa fa-reply"></i> Return</a>
									<?php
									}
									?>
								</div>
							</div> <!-- End #tab1 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->
				</div><!--body-wrapper-->
				<div id="fade"></div>
				<div id="xfade"></div>
			</body>
			</html>
			<?php include('conn.close.php'); ?>
