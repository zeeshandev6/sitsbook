<?php
	include 'common/classes/dashboard.php';
	include 'common/classes/items.php';
	include 'common/classes/accounts.php';
	include 'common/classes/cash_management.php';
	include 'common/classes/mobile-purchase-details.php';
	include 'common/classes/godown-details.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/events.php';


	if(!$admin){
		exit();
	}

	$objDashboard             = new Dashboard();
	$objItems                 = new Items();
	$objAccounts              = new ChartOfAccounts();
	$objJournalVoucher        = new JournalVoucher();
	$objUserAccounts          = new UserAccounts();
	$objGodownDetails         = new GodownDetails();
	$objCashManagement        = new CashManagement();
	$objMobilePurchaseDetails = new ScanPurchaseDetails();
	$objEvents                = new Events();
	$objConfigs               = new Configs();


	$version_type = $objConfigs->get_config('ONLINE');
	$demand_list  = $objConfigs->get_config('DASHBOARD_DEMAND_LIST');

	$today = date('Y-m-d');

	$exp_date = strtotime(EXP);
	$now      = strtotime($today);

	$desktop = ($version_type == 'Y')?false:true;

	$objEvents->status 		 = "N";
	$events 							 = $objEvents->getList($today);

	$salesAccCode          = '0201010001';
	$receivableCode        = '010104';

	$purchaseAccCode       = '0101060001';
	$payablesCode          = '040101';

	$saleReturnsAccCode    = '0201010003';
	$purchaseReturnAccCode = '0101060002';

	$sales              = $objDashboard->getSales($today,$salesAccCode);

	$totalReceivables   = $objDashboard->getThirdLevelTotal('', $today,$receivableCode,'<=');
	$currentReceivables = $objDashboard->sumAccountsDebits($today,$receivableCode,'=');
	$currentReceived    = $objDashboard->sumAccountsCredits($today,$receivableCode,'=');

	$currentReceivables-= $currentReceived;

	$overDueReceivables = $totalReceivables - $currentReceivables;

	if($currentReceivables < 0){
		$overDueReceivables+= $currentReceivables;
		$currentReceivables = 0;
	}

	$perCentageCurrentReceivables = $objDashboard->calculatePerCentage($totalReceivables,$currentReceivables);
	$perCentageOverDueReceivables = $objDashboard->calculatePerCentage($totalReceivables,$overDueReceivables);

	$totalPayables   = $objDashboard->getThirdLevelTotal('', $today,$payablesCode,'<=');
	$currentPayables = $objDashboard->sumAccountsCredits($today,$payablesCode,'=');
	$currentPaid     = $objDashboard->sumAccountsDebits($today,$payablesCode,'=');

	$currentPayables		-= $currentPaid;

	$overDuePayables 		 = $totalPayables - $currentPayables;

	if($currentPayables < 0){
		$overDuePayables   += $currentPayables;
		$currentPayables = 0;
	}

	$perCentageCurrentPayables = $objDashboard->calculatePerCentage($totalPayables,$currentPayables);
	$perCentageOverDuePayables = $objDashboard->calculatePerCentage($totalPayables,$overDuePayables);

	$assets 	 	 	= $objAccounts->getLevelThreeListByGeneral('01');
	$liabilities 	= $objAccounts->getLevelThreeListByGeneral('04');
	$equity 	 		= $objAccounts->getLevelThreeListByGeneral('05');

	$revenues    = $objAccounts->getLevelThreeListByGeneral('02');
	$expenses    = $objAccounts->getLevelThreeListByGeneral('03');

	$rev_array = array();
	if(mysql_num_rows($revenues)){
		while($revenue = mysql_fetch_array($revenues)){
			$account_code = $revenue['ACC_CODE'];
			$accCodeList = $objAccounts->getAccountByCatAccCode($account_code);
			if(mysql_num_rows($accCodeList)){
				while($revRow = mysql_fetch_array($accCodeList)){
					$rev_array[] = $revRow['ACC_CODE'];
				}
			}
		}
	}
	$exp_array = array();
	if(mysql_num_rows($expenses)){
		while($revenue = mysql_fetch_array($expenses)){
			$account_code = $revenue['ACC_CODE'];
			$accCodeList = $objAccounts->getAccountByCatAccCode($account_code);
			if(mysql_num_rows($accCodeList)){
				while($revRow = mysql_fetch_array($accCodeList)){
					$exp_array[] = $revRow['ACC_CODE'];
				}
			}
		}
	}

	$equity_array = array();
	$eq_fourth = $objAccounts->getAccountByCatAccCode("050101");
	if(mysql_num_rows($eq_fourth)){
		while($eqRow = mysql_fetch_array($eq_fourth)){
			$equity_array[] = $eqRow['ACC_CODE'];
		}
	}

	$trends_chart= $objDashboard->trends_chart($today, $rev_array, $exp_array,$equity_array);

	$expenses_summary_chartdata_3 = $objDashboard->getExpensesSummaryChartMonthly($exp_array,date('m'));

	$expenses_summary_chartdata_2 = $objDashboard->getExpensesSummaryChartMonthly($exp_array,date('m',strtotime(" -1 month")));

	$expenses_summary_chartdata_1 = $objDashboard->getExpensesSummaryChartMonthly($exp_array,date('m',strtotime(" -2 month")));

	$cashChart = $objDashboard->cashChartWeekly(8);

	if($demand_list=='Y'){
		$stocks = $objItems->getDashboardStockPosition();
	}

	$paymentCheques = $objCashManagement->getPaymentChequeListToday();
	$receiptCheques = $objCashManagement->getReceiptChequeListToday();

	//$ledgerChange = $objJournalVoucher->getAccountCodeEntriesAsPerDate($today);

	$color_array = array();
	$color_array['030101'] = '#AA4643';
	$color_array['030102'] = '#327f46';
	$color_array['030103'] = '#3399CC';
	$color_array['030104'] = '#4572A7';
	$color_array['030105'] = '#1f887d';
	$color_array['030106'] = '#104C4C';
	$color_array['030107'] = '#88CCCC';
	$color_array['030108'] = '#228E8E';
	$color_array['030109'] = '#CCFFFF';
	$color_array['030110'] = '#00CCCC';
	$color_array['030110'] = '#3399CC';
	$color_array['X'] 		 = '#3399CC';
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Dashboard</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-table.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/dashboard.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery.dataTables.min.css" type="text/css" media="screen" />

	<style>
	table.table-transform th{
		background-color: #FFF;
		border-right: none;
	}
	html{
		margin-bottom: 0px;
	}
	.fixed-table-pagination{
		overflow: hidden;
		text-align: center;
		background-color: #EEE;
	}
	.pull-left.pagination-detail{
		float: none;
		width: 100%;
	}
	.fixed-table-pagination .pull-right.pagination{
		margin:  0px;
		padding: 0px;
		float: none !important;
	}
	.fixed-table-pagination .pull-right.pagination .pagination{
		margin: 0px;
		padding: 0px;
	}
	.fixed-table-pagination .pagination a{
		padding: 6px 5px;
	}
	.th-inner{
		font-weight: bold;
		background-color:  white;
	}
	.chart-wrapper{
		float: left;
		padding-bottom: 40%;
		position: relative;
		width: 200px;
	}
	.highchart-container-pie-small,
	.highchart-container-pie-small2,
	.highchart-container-pie-small3{
		margin-top: -70px;
	}
	</style>

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-table.js"></script>
	<script type="text/javascript" src="resource/scripts/highcharts.js"></script>
	<script type="text/javascript" src="resource/scripts/highcharttables.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="resource/scripts/chart.js"></script>
	<script type="text/javascript">
	$(function(){
		$(window).on('load',function(){
			$('table.hightCharts').highchartTable();
			$('table.hightChartsSized').each(function(){
				if($(this).find("tbody td").length == 0){
					$(this).parent().remove();
				}
			});
			if($('table.hightChartsSized').length == 0){
				$("#pie_legend").css({"margin-top":"0px"});
			}
			$('table.hightChartsSized').highchartTable();
			$(".data-tables-tables").dataTable({
				"pagingType": "simple",
				"searching": false,
				"lengthChange": false,
				"length": 10
			});

			$(".table-transform2,.table-transform3,.table-transform4,.table-transform5,.table-transform6").dataTable({
				"pagingType": "simple",
				"searching": false,
				"lengthChange": false,
				"length": 10
			});
			setTimeout(function(){
				if($(".pie_chart_panel svg").length==0){
					$(".pie_chart_panel").remove();
				}
			},100);
		});
	});
	</script>
</head>

<body style="margin-bottom: 0px !important;">
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div style="height:70px;"></div>
	<?php
	//Permission
	if((in_array('dashboard',$permissionz) || $admin == true) ){
		?>
		<div class="content-box-top">
			<div class="all_content">
				<div class="content-box-header">
					<p> <i class="fa fa-dashboard"></i> Dashboard</p>
					<div class="clear"></div>
				</div> <!-- End .content-box-header -->
				<div class="content-box-content">

						<div class="col-md-6">

						</div>

						<?php if($financial_dashboard == 'Y'){ ?>

						<div class="col-md-12">
							<div class="alert alert-success col-md-3 text-center">
								<b>Today's Revenue <br /> </b> Rs.<?php echo number_format($sales['TODAY'],2); ?>
							</div>
							<div class="col-md-1"></div>
							<div class="alert alert-warning col-md-4 text-center">
								<b>Yearly Revenue <br /> </b> Rs.<?php echo number_format($sales['YEARLY'],2); ?>
							</div>
							<div class="col-md-1"></div>
							<div class="alert alert-info col-md-3 text-center">
								<b>Monthly Revenue <br /> </b> Rs.<?php echo number_format($sales['MONTHLY'],2); ?>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>

						<div class="col-xs-12">
							<div class="col-xs-12 panel panel-default">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="col-xs-12" style="height:10px;"></div>
									<div class="col-xs-12 p-0 col-sm-6 pull-left">
										<h4>Receivables</h4>
									</div>
									<div class="col-xs-12 p-0 col-sm-6 pull-right text-right">
										<button class="btn btn-primary btn-sm" type="button">
											Total <span class="badge"><?php echo number_format($totalReceivables,2); ?></span>
										</button>
									</div>
									<div class="col-md-12">
										<div class="progress" style="margin-bottom:0px;">
											<div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?php echo number_format($perCentageCurrentReceivables,2); ?>%"></div>
											<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: <?php echo number_format($perCentageOverDueReceivables,2); ?>%"></div>
										</div>
									</div>
									<div class="clear" style="height:10px;"></div>
									<div class="col-xs-12 ">
										<div class="col-xs-12  text-left p-0" style="font-size:14px;">
											<b>CURRENT</b>
											Rs. <?php echo number_format($currentReceivables,2); ?>
										</div>
										<div class="col-xs-12 text-left p-0" style="font-size:14px;">
											<b>OVERDUE</b>
											Rs. <?php echo number_format($overDueReceivables,2); ?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="col-xs-12" style="height:10px;"></div>
									<div class="col-xs-6 p-0 pull-left">
										<h4>Payables</h4>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right text-right">
										<button class="btn btn-primary btn-sm" type="button">
											Total <span class="badge"><?php echo number_format($totalPayables,2); ?></span>
										</button>
									</div>
									<div class="col-md-12">
										<div class="progress" style="margin-bottom:0px;">
											<div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?php echo number_format($perCentageCurrentReceivables,2); ?>%"></div>
											<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: <?php echo number_format($perCentageOverDueReceivables,2); ?>%"></div>
										</div>
									</div>
									<div class="clear" style="height:10px;"></div>
									<div class="col-xs-12">
										<div class="col-xs-12  text-left" style="font-size:14px;">
											<b>CURRENT</b>
											Rs. <?php echo number_format($perCentageCurrentPayables,2); ?>
										</div>
										<div class="col-xs-12 text-left" style="font-size:14px;">
											<b>OVERDUE</b>
											Rs. <?php echo number_format($perCentageOverDuePayables,2); ?>
										</div>
									</div>
								</div>
								<div class="clear" style="height: 10px;"></div>
							</div>
						</div>
					</div>
					<div class="clear"></div>

		<div class="col-xs-12">
			<div style="height: 10px;"></div>
			<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
				<?php if(isset($stocks)){ ?>
				<div class="col-xs-12" style="padding:0px;">
					<h4>Stocks Position</h4>
					<table class="data-tables-tables">
						<thead>
							<tr>
								<th style="width:60%;">Item</th>
								<th style="width:40%;" class="text-center">Stock</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($stocks)){
								while($stockRow = mysql_fetch_array($stocks)){
									$stockRow['STOCK_QTY'] += $objGodownDetails->getStockSum($stockRow['ID']);
									if($stockRow['INV_TYPE']=='B'){
										$stockRow['STOCK_QTY'] += $objMobilePurchaseDetails->get_item_stock($stockRow['ID']);
									}
									if($stockRow['STOCK_QTY'] == 0){
										continue;
									}
									?>
									<tr>
										<td><?php echo $stockRow['NAME']; ?></td>
										<td class="text-center"><?php echo $stockRow['STOCK_QTY']; ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div class="clear" style="height:10px;"></div>
				</div>
				<?php } ?>
				<div class="col-xs-12" style="padding:0px;">
					<h4> Financial Position</h4>
					<table class="table-transform2">
						<thead>
							<tr>
								<th style="width:60%;">Assets</th>
								<th style="width:40%;">Balances</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($assets)){
								while($asset = mysql_fetch_array($assets)){
									$account_code = $asset['ACC_CODE'];
									$assetsTotal  = 0;
									$thisAssetCodelist = $objAccounts->getAccountByCatAccCode($account_code);

									if(mysql_num_rows($thisAssetCodelist)){
										while($currentAssetRow = mysql_fetch_array($thisAssetCodelist)){
											$currentAssetRowClose = $objDashboard->getLedgerOpeningBalance($today,$currentAssetRow['ACC_CODE'],'<=');
											$assetsTotal += $currentAssetRowClose;
										}
									}
									if($assetsTotal == 0){
										continue;
									}
									?>
									<tr>
										<td> <a target="_blank" href="trial-balance.php?fromDate=<?php echo date('d-m-Y') ?>&toDate=<?php echo date('d-m-Y') ?>&AccCode=<?php echo $account_code; ?>&city=&search=Search"> <?php echo $objAccounts->getAccountTitleByCode($account_code); ?> </a> </td>
										<td><?php echo number_format($assetsTotal,2); ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div style="height:10px;"></div>
					<table class="table-transform3">
						<thead>
							<tr>
								<th style="width:60%;">Liabilities</th>
								<th style="width:40%;">Balances</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($liabilities)){
								while($liability = mysql_fetch_array($liabilities)){
									$account_code = $liability['ACC_CODE'];
									$liabilityTotal  = 0;
									$thisAssetCodelist = $objAccounts->getAccountByCatAccCode($account_code);

									if(mysql_num_rows($thisAssetCodelist)){
										while($currentAssetRow = mysql_fetch_array($thisAssetCodelist)){
											$currentAssetRowClose = $objDashboard->getLedgerOpeningBalance($today,$currentAssetRow['ACC_CODE'],'<=');
											$liabilityTotal += $currentAssetRowClose;
										}
									}
									if($liabilityTotal == 0){
										continue;
									}
									?>
									<tr>
										<td> <a target="_blank" href="trial-balance.php?fromDate=<?php echo date('d-m-Y') ?>&toDate=<?php echo date('d-m-Y') ?>&AccCode=<?php echo $account_code; ?>&city=&search=Search"> <?php echo $objAccounts->getAccountTitleByCode($account_code); ?> </a> </td>
										<td><?php echo number_format($liabilityTotal,2); ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div style="height:10px;"></div>
					<table class="table-transform3">
						<thead>
							<tr>
								<th style="width:60%;">Equity</th>
								<th style="width:40%;">Balances</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($equity)){
								while($equityRow = mysql_fetch_array($equity)){
									$account_code = $equityRow['ACC_CODE'];
									$equityTotal  = 0;
									$thisAssetCodelist = $objAccounts->getAccountByCatAccCode($account_code);

									if(mysql_num_rows($thisAssetCodelist)){
										while($currentAssetRow = mysql_fetch_array($thisAssetCodelist)){
											$currentAssetRowClose = $objDashboard->getLedgerOpeningBalance($today,$currentAssetRow['ACC_CODE'],'<=');
											$equityTotal += $currentAssetRowClose;
										}
									}
									if($equityTotal == 0){
										continue;
									}
									?>
									<tr>
										<td><a target="_blank" href="trial-balance.php?fromDate=<?php echo date('d-m-Y') ?>&toDate=<?php echo date('d-m-Y') ?>&AccCode=<?php echo $account_code; ?>&city=&search=Search"><?php echo $objAccounts->getAccountTitleByCode($account_code); ?></a></td>
										<td><?php echo number_format($equityTotal,2); ?></td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
					<div class="clear" style="height:10px;"></div>
				</div>
			</div>

			<!--Col MD 8s -->
			<?php
			if(mysql_num_rows($paymentCheques)){
				?>
				<div class="col-md-8 col-xs-12 panel panel-default">
					<h4>Payment Cheques</h4>
					<div class="col-md-12">
						<table class="table-transform4">
							<thead>
								<tr>
									<th>Date</th>
									<th>Payee</th>
									<th>Bank</th>
									<th>Cheque#</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php
								while($paymentCheque = mysql_fetch_array($paymentCheques)){
									?>
									<tr>
										<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($paymentCheque['TODAY'])); ?></td>
										<td style="text-align:center;"><?php echo $objAccounts->getAccountTitleByCode($paymentCheque['PARTY_ACC_CODE']); ?></td>
										<td style="text-align:center;"><?php echo $objAccounts->getAccountTitleByCode($paymentCheque['BANK_ACC_CODE']); ?></td>
										<td style="text-align:center;"><?php echo $paymentCheque['CHEQUE_NO']; ?></td>
										<td style="text-align:center;"><?php echo number_format($paymentCheque['AMOUNT'],2); ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div style="height: 30px;"></div>
					</div>
				</div>
				<?php
			}
			?>
			<?php
			if(mysql_num_rows($receiptCheques)){
				?>
				<div class="col-md-8 col-xs-12 panel panel-default">
					<h4>Receipt Cheques</h4>
					<div class="col-md-12">
						<table class="table-transform5">
							<thead>
								<tr>
									<th>Date</th>
									<th>Payee</th>
									<th>Bank</th>
									<th>Cheque#</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php
								while($receiptCheque = mysql_fetch_array($receiptCheques)){
									?>
									<tr>
										<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($receiptCheque['TODAY'])); ?></td>
										<td style="text-align:center;"><?php echo $objAccounts->getAccountTitleByCode($receiptCheque['PARTY_ACC_CODE']); ?></td>
										<td style="text-align:center;"><?php echo $receiptCheque['BANK_ACC_CODE']; ?></td>
										<td style="text-align:center;"><?php echo $receiptCheque['CHEQUE_NO']; ?></td>
										<td style="text-align:center;"><?php echo number_format($receiptCheque['AMOUNT'],2); ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div style="height: 30px;"></div>
					</div>
				</div>
				<?php
			}
			?>
			<?php
			if(mysql_num_rows($events)){
				?>
				<div class="col-md-8 col-xs-12 panel panel-default">
					<h4>Today's Events</h4>
					<hr />
					<div id="events" style="margin-bottom:10px;">
						<?php
						while($event = mysql_fetch_assoc($events)){
							?>
							<div class="event col-md-12" data-remove="<?php echo $event['ID']; ?>">
								<p class="pull-left col-xs-12">
									<span><?php echo $event['TITLE']; ?></span>
									<button class="button btn-xs  pull-right ml-5" onclick="event_status(<?php echo $event['ID']; ?>,'Y');"> <i class="fa fa-check"></i> </button>
									<small class="pull-right mr-10 is_time"><?php echo date('d-m-Y h:i A',strtotime($event['EVENT_DATE'])); ?></small>
								</p>
								<div class="clear"></div>
								<small class="col-xs-12 is_desc"><?php echo $event['DESCRIPTION']; ?></small>
							</div>
							<?php
						}
						?>
						<div class="clear"></div>
					</div>
				</div>
				<?php       } ?>

				<div class="col-md-8 col-xs-12 panel panel-default" style="padding:0px;">
					<div class="panel-heading">Cash Flow</div>
					<div class="cash-chart highchart-container">
						<table class="hightCharts" data-graph-container=".highchart-container" data-graph-type="line" style="display:none;">
							<thead>
								<tr>
									<th>Week</th>
									<th>Cash</th>
								</tr>
							</thead>
							<tbody>
								<?php


								if($cashChart != NULL){
									$week = 1;
									foreach($cashChart as $k => $cashWeek){
										$k = date('d-M',strtotime($k));
										?>
										<tr>
											<td><?php echo $k; ?></td>
											<td><?php echo number_format($cashWeek,2,'.',''); ?></td>
										</tr>
										<?php
										$week++;
									}
								}
								?>
							</tbody>
						</table>
						<div class="clear"></div>
					</div><!--cash-chart-->
				</div>

				<div class="col-md-8 col-xs-12 panel panel-default" style="padding:0px;">
					<div class="panel-heading">Income And Expense Trend</div>
					<div class="cash-chart highchart-container2">
						<table class="hightCharts" data-graph-container=".highchart-container2"  data-graph-type="column" style="display:none;">
							<thead>
								<tr>
									<th>Month</th>
									<th>Income</th>
									<th>Expense</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($trends_chart as $key=>$value){
									?>
									<tr>
										<td><?php echo $key; ?></td>
										<td><?php echo number_format($value['REVENUE'],2,'.',''); ?></td>
										<td><?php echo number_format($value['EXPENSE'],2,'.',''); ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div class="clear"></div>
					</div><!--cash-chart-->
				</div>


				<div class="col-md-8 col-xs-12 panel panel-default" style="padding:0px;">
					<div class="panel-heading">Returns on Equity</div>
					<div class="cash-chart highchart-container3">
						<table class="hightCharts" data-graph-container=".highchart-container3"  data-graph-type="column" style="display:none;">
							<thead>
								<tr>
									<th>Month</th>
									<th>Return</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($trends_chart as $key=>$value){
									?>
									<tr>
										<td ><?php echo $key; ?></td>
										<td data-graph-item-color="#<?php echo ($value['RETURN'] < 0)?"AA4643":"89A54E"; ?>"><?php echo number_format($value['RETURN'],2,'.','')." % "; ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div class="clear"></div>
					</div><!--cash-chart-->
				</div>

				<div class="col-md-8 col-xs-12 panel panel-default pie_chart_panel" style="padding:0px;position:relative;">
					<div class="panel-heading">Expense Summary</div>
					<div class="clear" style="height:20px;"></div>
					<div>
						<div class="col-md-12 always_on_top">
							<p>Displays values on each point of the graph. Could be defined for all the chart on the table tag or just for one serie on the th tag. The th tag value overrides the table tag value.</p>
						</div>
						<div class="panel-body highchart-container-pie-small col-md-4 col-sm-12 col-xs-12">
							<table class="hightChartsSized" data-graph-container=".highchart-container-pie-small" data-graph-type="pie" style="display:none;">
								<thead>
									<tr>
										<th></th>
										<th>Expenses As on : <?php echo date('M-Y',strtotime(" -2 month")) ?></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count_color = 0;
									foreach ($expenses_summary_chartdata_1 as $key => $value) {
										if($value == 0){
											continue;
										}
										$acc_title = $objAccounts->getAccountTitleByCode($key);
										?>
										<tr>
											<td><?php echo $acc_title; ?></td>
											<td data-graph-name="<?php echo $acc_title; ?>" data-graph-item-color="<?php echo isset($color_array[$key])?$color_array[$key]:$color_array['X']; ?>"><?php echo $value; ?></td>
										</tr>
										<?php
										$count_color++;
									}
									?>
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
						<div class="panel-body highchart-container-pie-small2 col-md-4 col-sm-12 col-xs-12">
							<table class="hightChartsSized" data-graph-container=".highchart-container-pie-small2" data-graph-type="pie" style="display:none;">
								<thead>
									<tr>
										<th></th>
										<th>Expenses As on : <?php echo date('M-Y',strtotime(" -1 month")); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count_color = 0;
									foreach ($expenses_summary_chartdata_2 as $key => $value) {
										if($value == 0){
											continue;
										}
										$acc_title = $objAccounts->getAccountTitleByCode($key);
										?>
										<tr>
											<td><?php echo $acc_title; ?></td>
											<td data-graph-name="<?php echo $acc_title; ?>" data-graph-item-color="<?php echo isset($color_array[$key])?$color_array[$key]:$color_array['X']; ?>"><?php echo $value; ?></td>
										</tr>
										<?php
										$count_color++;
									}
									?>
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
						<div class="panel-body highchart-container-pie-small3 col-md-4 col-sm-12 col-xs-12">
							<table class="hightChartsSized" data-graph-container=".highchart-container-pie-small3" data-graph-type="pie" style="display:none;">
								<thead>
									<tr>
										<th></th>
										<th>Expenses As on : <?php echo date('M-Y'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count_color = 0;
									foreach ($expenses_summary_chartdata_3 as $key => $value) {
										if($value == 0){
											continue;
										}
										?>
										<tr>
											<td><?php echo $objAccounts->getAccountTitleByCode($key); ?></td>
											<td data-graph-name="Invoices" data-graph-item-color="<?php echo isset($color_array[$key])?$color_array[$key]:$color_array['X']; ?>"><?php echo $value; ?></td>
										</tr>
										<?php
										$count_color++;
									}
									?>
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<ul id="pie_legend" class="col-md-12">
							<?php
							foreach ($color_array as $key => $value){
								$acc_title = $objAccounts->getAccountTitleByCode($key);
								if($acc_title == ''){
									continue;
								}
								?>
								<li class="col-md-4"><span class="bullet" style="background-color:<?php echo $value; ?>;"></span> <?php echo $acc_title; ?></li>
								<?php
							}
							?>
						</ul>
						<div class="clear"></div>
					</div><!--cash-chart-->
				</div>
				<div class="clear"></div><!--Col MD 8 End  -->
				<!--Col MD 12 -->

				<?php } ?>
				<?php
				//if(mysql_num_rows($ledgerChange)){
				if(false){
					?>
					<div class="col-md-12 panel panel-default">
						<h4>Current Ledger Entries</h4>
						<hr />
						<div class="col-md-12">
							<table class="table-transform6">
								<thead>
									<tr>
										<th>Account Title</th>
										<th>Debit</th>
										<th>Credit</th>
										<th>Closing Balance</th>
									</tr>
								</thead>
								<tbody>
									<?php
									while($ledgerDetail = mysql_fetch_array($ledgerChange)){
										$account_code = $ledgerDetail['ACCOUNT_CODE'];
										$debits       = $objJournalVoucher->getTotalDebits($account_code, $today);
										$credits      = $objJournalVoucher->getTotalCredits($account_code, $today);
										$lastBalance  = $objJournalVoucher->getDayEndBalance($today, $account_code);
										?>
										<tr>
											<td><?php echo $ledgerDetail['ACCOUNT_TITLE']; ?></td>
											<td><?php echo number_format($debits,2); ?></td>
											<td><?php echo number_format($credits,2); ?></td>
											<td><?php echo number_format($lastBalance,2); ?></td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
							<div class="clearfix" style="height: 20px;"></div>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div><!--content-box-content-->
	</div><!--all_content-->
</div>
<?php
}
?>
<div class="clear"></div>
<div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
