<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/suppliers.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('purchase-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     = new ChartOfAccounts();
	$objInventory        = new inventory();
	$objInventoryDetails = new inventory_details();
	$objSuppliers 			 = new suppliers();
	$objItems            = new Items();
	$objItemCategory     = new itemCategory();
	$objTaxRates         = new TaxRates();
	$objDepartments      = new Departments();
	$objConfigs 		 		 = new Configs();

	$suppliersList   = $objInventory->getSuppliersList();
	$cashAccounts    = $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList     = $objTaxRates->getList();

	$currency_type 		 = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   		 = $objConfigs->get_config('USE_CARTONS');
	$use_income_tax    = $objConfigs->get_config('INCOME_TAX');


	if(isset($_GET['search'])){
		$objInventory->fromDate 		= ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objInventory->toDate   		= ($_GET['toDate']=='')?"":date('Y-m-d',strtotime($_GET['toDate']));

		if(isset($_GET['user_id'])){
			$objInventory->user_id 			= ($_SESSION['classuseid'] == 1)?$_GET['user_id']:$_GET['user_id'];
		}else{
			$objInventory->user_id 			= mysql_real_escape_string($_SESSION['classuseid']);
		}

		$objInventory->item     				= isset($_GET['item'])?mysql_real_escape_string($_GET['item']):"";
		$objInventory->supplierAccCode  = isset($_GET['supplier'])?mysql_real_escape_string($_GET['supplier']):"";
		$objInventory->cat              = isset($_GET['item_category'])?mysql_real_escape_string($_GET['item_category']):"";
		$objInventory->unregistered_tax = isset($_GET['unregistered_tax'])?mysql_real_escape_string($_GET['unregistered_tax']):"";
		$objInventory->with_tax  				= isset($_GET['with_tax'])?mysql_real_escape_string($_GET['with_tax']):"";
		$supplierTypeReport             = ($objInventory->supplierAccCode == '')?false:true;
		$itemTypeReport                 = ($objInventory->item == '')?false:true;

		$u_full_name = $objAccounts->getFullName($objInventory->user_id);
		//both selected
		if(($objInventory->supplierAccCode != '') && ($objInventory->item != '')){
			$supplierTypeReport = true;
		}

		$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objInventory->supplierAccCode);
		$theItemTitle      = $objItems->getItemTitle($objInventory->item);

		if($objInventory->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objInventory->fromDate = $startThisMonth;
		}
		$purchaseReport = $objInventory->purchaseReport('P');
	}
	if(!isset($_GET['csv'])){
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				if($("div.tablePage").length==0){
					var MaxHeight = 480;
					var RunningHeight = 0;
					var PageNo = 1;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var tableFooter = $(".tableFooter").html();
					var repoDate = $(".repoDate").text();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					$(".tableFooter").remove();
					$(".repoDate").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					$(".newTable").last().append(tableFooter);
					$('table.tableBreak').remove();

          $("div.tablePage").each(function(i,e){
              $(this).prepend($(".pageHeader").first().clone());
          });
          $(".pageHeader").first().remove();
				}
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Sit Solution",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});

			if($(".carton_th").length){
				var colspan = $(".tHeader .carton_th").prevAll().length;
			}else{
				var colspan = $(".tHeader th.qty_th").prevAll().length;
			}
			//alert(colspan);
			$(".total_tf").attr('colspan',colspan);

			$('select').selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Export Purchase Data </p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="get" action="">
                        <div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

												<!--HIDDEN-->
												<div class="hide">

                        <div class="caption">Account Title</div>
                        <div class="field" style="width:300px;position:relative;">
                                <select class="supplierSelector form-control "
                                		name="supplier"
                                        data-style="btn btn-default"
                                        data-live-search="true" style="border:none" >
                                        <option selected value=""></option>
<?php
                        if(mysql_num_rows($cashAccounts)){
                            while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
?>
                               <option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
<?php
                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Category </div>
                        <div class="field" style="width:300px;">
                        <select class="itemSelector show-tick form-control"
                                data-style="btn btn-default"
                                data-live-search="true" style="border:none" name="item_category">
                           <option selected value=""></option>
<?php
                    $itemsCategoryList  = $objItemCategory->getList();
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
?>
                                <option value="<?php echo $ItemCat['ITEM_CATG_ID']; ?>" ><?php echo $ItemCat['NAME']; ?></option>
<?php
                        }
                    }
?>
                        </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Item </div>
                        <div class="field" style="width:300px;">
                        <select class="itemSelector show-tick form-control"
                                data-style="btn btn-default"
                                data-live-search="true" style="border:none" name="item">
                           <option selected value=""></option>
<?php
                    $itemsCategoryList  = $objItemCategory->getList();
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
							$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
								<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
							if(mysql_num_rows($itemList)){
								while($theItem = mysql_fetch_array($itemList)){
									if($theItem['ACTIVE'] == 'N'){
										continue;
									}
									if($theItem['INV_TYPE'] == 'B'){
										continue;
									}
?>
                           			<option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
								}
							}
?>
								</optgroup>
<?php
                        }
                    }
?>
                        </select>
                        </div>
                        <div class="clear"></div>
<?php
					if($_SESSION['classuseid'] == 1){
?>
                        <div class="caption">Salesman</div>
                        <div class="field" style="width:300px;position:relative;">
                        	<select name="user_id" class="form-control" data-style="btn btn-default">
                        		<option value=""></option>
<?php
								$userList = $objAccounts->getActiveList();
								if(mysql_num_rows($userList)){
									while($the_user = mysql_fetch_array($userList)){
?>
                        				<option value="<?php echo $the_user['ID']; ?>"><?php echo $the_user['FIRST_NAME']." ".$the_user['LAST_NAME']; ?></option>
<?php
									}
								}
?>
                        	</select>
                        </div>
                        <div class="clear"></div>
<?php
					}
?>
                        <div class="caption">Report Type</div>
                        <div class="field" style="width:300px;position:relative;">
                        	<select id="repoType" class="form-control" name="repoType">
                            	<option value="P" selected>Purchases</option>
                                <option value="R">Purchases Return</option>
                            </select>
                        </div><!--field-->
                        <div class="clear"></div>

												<?php if($use_income_tax == 'Y'){ ?>
													<div class="caption">WH.Income Tax</div>
													<div class="field" style="width:300px;position:relative;">
														<input type="radio" name="with_tax"  value="" id="radio0x" class="css-checkbox"  checked />
														<label for="radio0x" class="css-label-radio radGroup2"> All </label>
														<input type="radio" name="with_tax"  value="Y" id="radio1x" class="css-checkbox"  />
														<label for="radio1x" class="css-label-radio radGroup2"> YES </label>
														<input type="radio" name="with_tax"  value="N" id="radio3x" class="css-checkbox" />
														<label for="radio3x" class="css-label-radio radGroup2"> NO </label>
													</div>
													<div class="clear"></div>

													<div class="caption">Status</div>
													<div class="field" style="width:300px;position:relative;">
														<input type="radio" name="unregistered_tax"  value="" id="radio0xyz" class="css-checkbox"   />
														<label for="radio0xyz" class="css-label-radio radGroup2"> All </label>
														<input type="radio" name="unregistered_tax"  value="Y" id="radio1xyz" class="css-checkbox" />
														<label for="radio1xyz" class="css-label-radio radGroup2"> Unregistered </label>
														<input type="radio" name="unregistered_tax"  value="N" id="radio1zee" class="css-checkbox" checked />
														<label for="radio1zee" class="css-label-radio radGroup2"> Registered </label>
													</div>
													<div class="clear"></div>
												<?php } ?>

											</div> <!--HIDDEN-->

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />
<?php
					if(isset($purchaseReport)){
						$url = 'tax-purchase-report.php?csv';
						foreach($_GET as $name => $value){
							$url .= "&".$name."=".$value;
						}
?>
                    <div id="form">
											<!--<span class="pull-right m-10"><button class="button printThis"> <i class="fa fa-print"></i> Print</button></span>-->
											<span class="pull-right m-10"><a href="<?php echo $url; ?>" target="_blank"><button class="button"> <i class="fa fa-download"></i> Export CSV</button></a></span>
                    </div>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Purchase Report </p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            	<?php echo ($objInventory->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objInventory->fromDate)); ?>
                                <?php echo ($objInventory->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objInventory->toDate)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						if(mysql_num_rows($purchaseReport)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="5%" >Sr.</th>
																	 <th width="5%" >Supplier NTN</th>
                                   <th width="5%" >Supplier CNIC</th>
																	 <th width="10%" >Supplier Name</th>
																	 <th width="5%" >Supplier Type</th>
																	 <th width="5%" >Supplier Province</th>
																	 <th width="5%" >Document Type</th>
                                   <th width="5%" >Document Number</th>
                                   <th width="5%" >Document Date</th>
                                   <th width="5%" >HS Code</th>
                                   <th width="5%" >Purchase Type</th>
                                   <th width="5%" >Rate</th>
                                   <th width="5%" >Description</th>
                                   <th width="5%" class="qty_th">Quantity / Electricity Units</th>
                                   <th width="5%" >UOM</th>
																	 <th width="5%" >Value of Purchases Excluding Sales Tax</th>
																	 <th width="5%" >Sales Tax/ FED in ST Mode</th>
																	 <th width="5%" >Input Credit not allowed</th>
																	 <th width="5%" >Extra Tax</th>
																	 <th width="5%" >ST Withheld as WH Agent</th>
																	 <th width="5%" >FED Charged</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_qty 		= 0;
								$total_crtns 	= 0;
								$total_price 	= 0;
								$total_tax 		= 0;
								$total_amount = 0;
								$extra_tax_total 		= 0;
								$sales_tax_wh_total = 0;
								$counter  		= 1;

								while($detailRow = mysql_fetch_array($purchaseReport)){
									$u_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
									$supplier_details = $objSuppliers->getSupplier($detailRow['SUPP_ACC_CODE']);

									$itemDetail    = $objItems->getRecordDetails($detailRow['ITEM_ID']);
									$discount_type = (isset($detailRow['DISCOUNT_TYPE']) && $detailRow['DISCOUNT_TYPE'] == 'P')?"%":"";

									$extra_tax     = ($detailRow['TOTAL_AMOUNT']*$detailRow['INCOME_TAX'])/100;
									$sales_tax_wh  = ($detailRow['TAX_AMOUNT']*20)/100;
?>
                              <tr id="recordPanel" class="alt-row">
	                                <td><?php echo $counter; ?></td>
																	<td><?php echo $supplier_details['NTN_REG_NO'] ?></td>
																	<td><?php echo $supplier_details['CNIC_NO'] ?></td>
																	<td><?php echo $supplier_details['SUPP_ACC_TITLE']; ?></td>
																	<td><?php echo "Registered"; ?></td>
																	<td><?php echo $supplier_details['CITY']; ?></td>
																	<td><?php echo "PI"; ?></td>
																	<td><?php echo $detailRow['VENDOR_BILL_NO']; ?></td>
																	<td><?php echo date('d/m/Y',strtotime($detailRow['PURCHASE_DATE'])); ?></td>
																	<td><?php echo $itemDetail['HS_CODE']; ?></td>
																	<td><?php echo ""; ?></td>
																	<td><?php echo $detailRow['UNIT_PRICE']; ?></td>
																	<td><?php echo "52-b-Cotton Yarn / Thread"; ?></td>
																	<td><?php echo $detailRow['STOCK_QTY']; ?></td>
																	<td><?php echo "Bags"; ?></td>
                                  <td><?php echo number_format($detailRow['SUB_AMOUNT'],2); ?></td>
                                  <td><?php echo number_format($detailRow['TAX_AMOUNT'],2); ?></td>
																	<td><?php echo ""; ?></td>
																	<td><?php echo number_format($extra_tax,0); ?></td>
																	<td><?php echo number_format($sales_tax_wh,0); ?></td>
																	<td><?php echo ""; ?></td>
                              </tr>
<?php
								if($use_cartons=='Y'){
									$total_crtns  += $detailRow['CARTONS'];
								}
								$total_qty    			+= $detailRow['STOCK_QTY'];
								$total_price  			+= $detailRow['SUB_AMOUNT'];
								$total_tax    			+= $detailRow['TAX_AMOUNT'];
								$total_amount 			+= $detailRow['TOTAL_AMOUNT'];
								$extra_tax_total 		+= $extra_tax;
								$sales_tax_wh_total += $sales_tax_wh;

								$counter++;
								}
?>

                            </tbody>
<?php
						}//end if
						if(mysql_num_rows($purchaseReport)){
							$total_price = number_format($total_price,2);
							$total_tax = number_format($total_tax,2);
							$total_amount = number_format($total_amount,2);
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;" colspan="" class="total_tf">Total:</td>
																<td style="text-align:center;"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
                                <td style="text-align:center;"> - - - </td>
                                <td style="text-align:center;"> <?php echo (isset($total_price))?$total_price:"0"; ?> </td>
																<td style="text-align:center;"> <?php echo (isset($total_tax))?$total_tax:"0"; ?> </td>
                                <td style="text-align:center;"> - - - </td>
                                <td style="text-align:right;"> <?php echo (isset($extra_tax_total))?round($extra_tax_total):"0"; ?> </td>
																<td style="text-align:right;"> <?php echo (isset($sales_tax_wh_total))?round($sales_tax_wh_total):"0"; ?> </td>
																<td style="text-align:center;"> - - - </td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
<?php }else{
	 // output headers so that the file is downloaded rather than displayed
	 header('Content-Type: text/csv; charset=utf-8');
	 header('Content-Disposition: attachment; filename=purchase('.date('d-M-Y').').csv');

	 // create a file pointer connected to the output stream
	 $output = fopen('php://output', 'w');

	 $array_col_titles   = array();
	 $array_col_titles[] = "Sr.";
	 $array_col_titles[] = "Supplier NTN";
	 $array_col_titles[] = "Supplier CNIC";
	 $array_col_titles[] = "Supplier Name";
	 $array_col_titles[] = "Supplier Type";
	 $array_col_titles[] = "Supplier Province";
	 $array_col_titles[] = "Document Type";
	 $array_col_titles[] = "Document Number";
	 $array_col_titles[] = "Document Date";
	 $array_col_titles[] = "HS Code";
	 $array_col_titles[] = "Purchase Type";
	 $array_col_titles[] = "Rate";
	 $array_col_titles[] = "Description";
	 $array_col_titles[] = "Quantity / Electricity Units";
	 $array_col_titles[] = "UOM";
	 $array_col_titles[] = "Value of Purchases Excluding Sales Tax";
	 $array_col_titles[] = "Sales Tax/ FED in ST Mode";
	 $array_col_titles[] = "Input Credit not allowed";
	 $array_col_titles[] = "Extra Tax";
	 $array_col_titles[] = "ST Withheld as WH Agent";
	 $array_col_titles[] = "FED Charged";

	  // output the column headings
	  fputcsv($output, $array_col_titles);

	  if(isset($purchaseReport)){
			$total_qty 					= 0;
			$total_crtns 				= 0;
			$total_price 				= 0;
			$total_tax 					= 0;
			$total_amount 			= 0;
			$extra_tax_total 		= 0;
			$sales_tax_wh_total = 0;
			$counter  				  = 1;
	  	while($detailRow = mysql_fetch_array($purchaseReport)){
	  		$u_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
	  		$supplier_details = $objSuppliers->getSupplier($detailRow['SUPP_ACC_CODE']);

	  		$itemDetail    = $objItems->getRecordDetails($detailRow['ITEM_ID']);
	  		$discount_type = (isset($detailRow['DISCOUNT_TYPE']) && $detailRow['DISCOUNT_TYPE'] == 'P')?"%":"";

	  		$extra_tax     = ($detailRow['TOTAL_AMOUNT']*$detailRow['INCOME_TAX'])/100;
	  		$sales_tax_wh  = ($detailRow['TAX_AMOUNT']*20)/100;
				$row 					 = array();

				$row[] = $counter;
				$row[] = $supplier_details['NTN_REG_NO'];
				$row[] = $supplier_details['CNIC_NO'];
				$row[] = $supplier_details['SUPP_ACC_TITLE'];
				$row[] = "Registered";
				$row[] = $supplier_details['CITY'];
				$row[] = "PI";
				$row[] = $detailRow['VENDOR_BILL_NO'];
				$row[] = date('d/m/Y',strtotime($detailRow['PURCHASE_DATE']));
				$row[] = $itemDetail['HS_CODE'];
				$row[] = "";
				$row[] = $detailRow['UNIT_PRICE'];
				$row[] = "52-b-Cotton Yarn / Thread";
				$row[] = $detailRow['STOCK_QTY'];
				$row[] = "Bags";
				$row[] = number_format($detailRow['SUB_AMOUNT'],2);
				$row[] = number_format($detailRow['TAX_AMOUNT'],2);
				$row[] = "";
				$row[] = number_format($extra_tax,0);
				$row[] = number_format($sales_tax_wh,0);
				$row[] = "";

	  		fputcsv($output, $row);

				$total_qty    			+= $detailRow['STOCK_QTY'];
				$total_price  			+= $detailRow['SUB_AMOUNT'];
				$total_tax    			+= $detailRow['TAX_AMOUNT'];
				$total_amount 			+= $detailRow['TOTAL_AMOUNT'];
				$extra_tax_total  	+= $extra_tax;
				$sales_tax_wh_total += $sales_tax_wh;

				$counter++;
	  	}

			$row 					 = array();

			$row[] = "Total";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			$row[] = "";
			//---------------------------
			$row[] = $total_qty;
			$row[] = "";
			$row[] = $total_price;
			$row[] = round($total_tax);
			$row[] = "";
			$row[] = round($extra_tax_total);
			$row[] = round($sales_tax_wh_total);
			$row[] = "";

			fputcsv($output, $row);
	  }
 }
?>
