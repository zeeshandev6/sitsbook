<?php
    ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/accounts.php');
    include ('common/classes/processing_report.php');
    include ('common/classes/processing_report_details.php');
    include ('common/classes/suppliers.php');

    //Permission
    if(!in_array('processing-report-form',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource-sp/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource-sp/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission
    $mode = "";

    $objChartOfAccounts         = new ChartOfAccounts();
    $objProcessingReport        = new ProcessingReport();
    $objProcessingReportDetails = new ProcessingReportDetails();
    $objSupplier                = new suppliers();

    $end                = $objConfigs->get_config("PER_PAGE");
    $suppliers_list     = $objSupplier->getList();

    if(!isset($_GET['search'])){
        $path = "processing-report-list.php?fromdate=&todate=&supplier_id=&lot_no=&search";
        if(isset($_GET['tab'])){
            $path .= "&tab=".$_GET['tab'];
        }
        echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        exit();
    }
?>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style media="screen">
          tbody td{
            padding:  5px !important;
          }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
                $('#pon, #sc_no').blur(function(e){
                    var keycode     = e.keyCode || e.which;
                    var report      = 'processingreport';
                    var pon         = $('#pon').val();

                    $('span#cname').html("Loading...");
                    $.ajax({
                        url:    'db/show-customer-name-by-pon.php',
                        type:   'post',
                        data:   {'pon': pon, 'reporting':report},
                        success: function(data) {
                            var id_name     = $.parseJSON(data);
                            var id          = id_name['sc_id'];
                            var name        = id_name['cu_name'];
                            var contract    = id_name['contractOption'];
                            var mgr         = id_name['mgr'];

                            if((name!=null && id!=null)){
                                $('input[name=pon]').val(id);
                                $('span#cname').html(name);
                            }else{
                                $('input#scid').val('');
                                $('p#cname').html('Nothing Found.');
                            }
                        }
                    });

                });
            });
            $(document).ready(function(){
                $('input[name=fromdate], input[name=todate]').datepicker({
                    dateFormat:'dd-mm-yy',
                    showAnim: 'show',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '2000:+10'
                });
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
<?php
            $myPage = 1;
            $start = 0;
            if(isset($_GET['page'])){
                $page = $_GET['page'];
                $myPage = $page;
                $start = ((int)$page * $end) - $end;
            }

            $objProcessingReport->user_id        = $_SESSION['classuseid'];

            $objProcessingReport->fromdate       = $_GET['fromdate']!=''?date("Y-m-d",strtotime($_GET['fromdate'])):"";
            $objProcessingReport->todate         = $_GET['todate']!=''?date("Y-m-d",strtotime($_GET['todate'])):"";
            $objProcessingReport->supplier_id    = mysql_real_escape_string($_GET['supplier_id']);
            $objProcessingReport->lot_no         = mysql_real_escape_string($_GET['lot_no']);

            $objProcessingReport->start     = $start;
            $objProcessingReport->per_page  = $end;

            $perocessingReportsList         = $objProcessingReport->search();

            $queryCount = $objProcessingReport->totalMatchRecords;
            $pageCount = ceil($queryCount/$end);

            $counter_start = $start;
            $counter_start++;
?>
        <div id="bodyWrapper">
        	<div class = "content-box" style="overflow:visible;">
            	<div class = "summery_body">
                    <div class = "content-box-header">
                        <p style="font-size:15px;"><b>Processing Reports List</b><span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span> </p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab1" onClick="tab('1','1','2');">List</div>
                                <div class="tab" id="tab2" onClick="tab('2','1','2');">Search</div>
                                <a href="processing-report-detail.php"><div class="tab">New</div></a>
                            </div>
                        </span>
               		</div><!-- End .content-box-header -->
                    <div style = "clear:both; height:10px"></div>

                    <div id = "bodyTab1">
                        <table cellspacing="0" style="width:100%" >
                            <thead>
								<tr>
                                    <th width="5%" style="text-align:center;" rowspan="2">Sr.</th>
                                    <th width="10%" style="text-align:center;" rowspan="2">Report Date</th>
                                    <th width="10%" style="text-align:center;" rowspan="2">Lot #</th>
                                    <th width="10%" style="text-align:center;" rowspan="2">Processing Unit</th>
                                    <th width="5%"  style="text-align:center;" rowspan="3">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php
                            if(mysql_num_rows($perocessingReportsList)){
                                while($main_record = mysql_fetch_assoc($perocessingReportsList)){
                                    $supplier_name = $objChartOfAccounts->getAccountTitleByCode($main_record['SUPPLIER_ID']);
?>
                                    <tr id="recordPanel">
                                      <td style="text-align:center"><?php echo $counter_start; ?></td>
                                      <td style="text-align:center"><?php echo date('d-m-Y', strtotime($main_record['REPORT_DATE'])); ?></td>
                                      <td style="text-align:center"><?php echo $main_record['LOT_NO']; ?></td>
                                      <td style="text-align:center"><?php echo $supplier_name; ?></td>
                                      <td style="text-align:center">
                                        <a href="processing-report-detail.php?id=<?php echo $main_record['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                        <a data-id="<?php echo $main_record['ID']; ?>" class="pointer ml-5"><i class="fa fa-times"></i></a>
                                      </td>
                                    </tr>
                                <?php
                                    $counter_start++;
                                }
                            }else{
                                echo "<tr><td colspan='6' style='text-align: center;'>No Record Found.</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                        <table align="center" border="0" id="navbar">
                            <?php
                            if(isset($queryCount)){
                                if($queryCount>$pageCount){
                                    ?>
                                    <tr>
                                        <td style="text-align: center">
                                            <nav>
                                                <ul class="pagination">
                                                    <?php
                                                    if($myPage > 1){
                                                        echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=1'>Fst</a></li>";
                                                        echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=". ($myPage-1) ."'>Prv</a></li>";
                                                    }

                                                    for($x=$myPage-8; $x<$myPage; $x++){
                                                        if($x > 0){
                                                            echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=". $x ."'>". $x ."</a></li>";
                                                        }
                                                    }

                                                    print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                                                    for($y=$myPage+1; $y<$myPage+9; $y++){
                                                        if($y <= $pageCount){
                                                            echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=". $y ."'>". $y ."</a></li>";
                                                        }
                                                    }

                                                    if($myPage < $pageCount){
                                                        echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=". ($myPage+1) ."'>Nxt</a></li>";
                                                        echo "<li><a href='processing-report-list.php?fromdate=".$_GET['fromdate']."&todate=".$_GET['todate']."&supplier_id=".$_GET['supplier_id']."&lot_no=".$_GET['lot_no']."&search&page=". $pageCount ."'>Lst</a></li>";
                                                    }
                                                    ?>
                                                </ul>
                                            </nav>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                        </table>
                        <div style = "clear:both;"></div>
               		</div> <!--bodyTab1-->
                    <div id = "bodyTab2" style=" display:none">
						              <form method="get" action="" >
                        	<div id="form">
                                <div class="caption"> From Date :</div>
                                <div class="field">
                                    <input type="hidden" name="srch" />
                                    <input type="text" class="form-control datepicker" name="fromdate" style="width: 144px;" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> To Date :</div>
                                <div class="field">
                                    <input type="text" class="form-control datepicker" name="todate" style="width: 144px;" />
                                </div>
                                <div class="clear"></div>


                                <div class="caption">Processing Unit :</div>
                                <div class="field" style="width:350px;">
                                    <select class="selectpicker show-tick form-control" name="supplier_id" data-live-search="true" >
                                        <option value=""></option>
                                        <?php
                            						if(mysql_num_rows($suppliers_list)){
                            							while($account = mysql_fetch_array($suppliers_list)){
                            								$selected = ($objProcessingReport->supplier_id==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
                            								?>
                            								<option <?php echo $selected; ?> data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
                            								<?php
                            							}
                            						}
                            						?>
                                    </select>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Lot # :</div>
                                <div class="field">
                                	<input type="text" class="form-control" name="lot_no" value="" />
                                </div>
                                <div class="clear"></div>

                                <div class = "caption"></div>
                                <div class = "field">
                                   <input type="submit" name="search" value="Search" class="button" />
                                </div>
                                <div class="clear"></div>
                            </div>
	                      	<div style="height:30px"></div>
                        </form>
               		</div><!--bodyTab1-->
            	</div><!-- End summer -->
         	</div><!-- End .content-box-top -->
            <div style="height:30px"></div>
        </div><!-- End .Wrap Container -->
    </body>
</html>
<?php ob_end_flush();  ?>
