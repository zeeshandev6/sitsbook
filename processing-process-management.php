<?php
    ob_start();

    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/processing_process.php');
    include ('common/classes/pagination.php');
    include ('common/classes/notifications.php');
    include ('common/classes/alerts.php');

    //Permission
    if(!in_array('processing-process',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource-sp/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource-sp/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objCurrecyTypes    = new ProcessingProcess();
    $objNotifications   = new Notifications();
    $objAlerts          = new Alerts();

    if(isset($_POST['delete_id'])){
        $ret = array();
        $ret['OK'] = 'N';
        $ret['MSG'] = 'Record Cant Be Deleted!';

        $delete_id = mysql_real_escape_string($_POST['delete_id']);
        if($delete_id > 0){
            $used_records   = $objCurrecyTypes->in_use($delete_id);
            $process_detail = $objCurrecyTypes->getRecordDetailsByID($delete_id);
            if($used_records == 0){
                $deleted = $objCurrecyTypes->delete($delete_id);
                if($deleted){
                    $ret['OK'] = 'Y';
                    $ret['MSG'] = 'Record deleted Successfully!';

                    //notification
                    $alert_details  = $objAlerts->getRecordDetailsByID(67);

                    $alert_details['NOTIFICATION'] = str_replace('[TITLE]', $process_detail['TITLE'], $alert_details['NOTIFICATION']);
                    $alert_details['NOTIFICATION'] = str_replace('[OPERATOR]',$sessionUserDetails['FIRST_NAME']." ".$sessionUserDetails['LAST_NAME'] , $alert_details['NOTIFICATION']);

                    foreach ($admins_for_notifications as $key => $admin_notify_id){
                        $objNotifications->notification_date   = date('Y-m-d H:i:s');
                        $objNotifications->notification_status = ($alert_details['SHOW_NOTIFICATION']=='Y')?"N":"Y";
                        $objNotifications->from_account        = $user_id;
                        $objNotifications->to_account          = $admin_notify_id;
                        $objNotifications->sc_id               = isset($sc_id)?$sc_id:0;
                        $objNotifications->notification_type   = $alert_details['ALERT'];
                        $objNotifications->title               = $alert_details['NOTIFICATION'];
                        $objNotifications->save();
                    }
                }
            }
        }
        echo json_encode($ret);
        exit();
    }

?>
    <!DOCTYPE html   >
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/product.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('input[name=fromdate], input[name=todate]').datepicker({
                    dateFormat:'dd-mm-yy',
                    showAnim: 'show',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '2000:+10'
                });
                $.fn.centerThisDiv = function(){
                    var win_hi = $(window).height()/2;
                    var win_width = $(window).width()/2;
                    win_hi = win_hi-$(this).height()/2;
                    win_width = win_width-$(this).width()/2;
                    $(this).css({
                        'position': 'fixed',
                        'top': win_hi,
                        'left': win_width
                    });
                };
                $("a.pointer").click(function(){
                    var row_id   = parseInt($(this).attr('data-id'))||0;
                    var this_elm = $(this);
                    $("#fade").hide();
                    $("#popUpDel").remove();
                    $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
                    $("#popUpDel").hide();
                    $("#popUpDel").centerThisDiv();
                    $("#fade").fadeIn('slow');
                    $("#popUpDel").fadeIn();
                    $(".dodelete").click(function(){
                            if(row_id > 0){
                                $.post('processing-process-management.php',{delete_id:row_id},function(data){
                                    if(data != ''){
                                        data = $.parseJSON(data);
                                        if(data['OK'] == 'Y'){
                                            $(this_elm).parent().parent().remove();
                                            $("#popUpDel .confirm").text(data['MSG']);
                                            $(".dodelete").hide();
                                            $(".nodelete").text('Close');
                                        }else{
                                            $("#popUpDel .confirm").text(data['MSG']);
                                            $(".dodelete").hide();
                                            $(".nodelete").text('Close');
                                        }
                                    }
                                });
                            }
                        });
                    $(".nodelete").click(function(){
                        $("#fade").fadeOut();
                        $("#popUpDel").fadeOut();
                    });
                    $(".close_popup").click(function(){
                        $("#popUpDel").slideUp();
                        $("#fade").fadeOut('fast');
                    });
                });
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div>
<?php
    if(isset($_SESSION['classuseid'])){

        $mode = "";
        if(!isset($_GET['search']))
        {
            $path = "processing-process-management.php?title=&search=Search";
            if(isset($_GET['tab'])){
                $path .= "&tab=search";
            }
            echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        }

        if(isset($_GET['search'])){
            $objpage = new Paginations();
            $end = $objpage->getList();

            $objCurrecyTypes->name           = $_GET['title'];
            $myPage = 1;
            $start = 0;
            if(isset($_GET['page']))
            {
                $page = $_GET['page'];
                $myPage = $page;
                $start = ((int)$page * $end) - $end;
            }
            $itemCategoryList = $objCurrecyTypes->searchItemCategory($start,$end);
            $row          = mysql_num_rows($itemCategoryList);

            $queryCount = $objCurrecyTypes->totalMatchRecords;
            $pageCount = ceil($queryCount/$end);
        }else{
            $itemCategoryList = $objCurrecyTypes->getList();
            $row          = mysql_num_rows($itemCategoryList);
        }
?>
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p style="font-size:15px;"><B>Process Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span> </p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                                <div class="tab" id="tab2" onclick="tab('2','1','2');">Search</div>
                                <a href="add-processing-process.php"><div class="tab">New</div></a>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <div id = "bodyTab1 panel panel-default">
                    <table style="width:100%;margin:0px auto;" class="table-hover">
                        <thead>
                        <tr>
                            <th width="5%" style="text-align:center">Sr.</th>
                            <th width="80%" style="text-align:center">Title</th>
                            <th width="10%" style="text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($row>0){
                            $counter = 1;
                            while($itemCategoryDetails = mysql_fetch_assoc($itemCategoryList)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-left" style="padding-left:10px;"><?php echo $itemCategoryDetails['TITLE']; ?></td>
                                    <td style="text-align:center">
                                        <a id="view_button" href="add-processing-process.php?id=<?php echo $itemCategoryDetails['ID']; ?>"><i class="fa fa-pencil"></i></a>
                                        <a data-id="<?php echo $itemCategoryDetails['ID']; ?>" class="pointer" ><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            <?php
                                $counter++;
                            }
                        }else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div> <!--bodyTab1-->

                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Product :</div>

                            <div class="caption">Item Category Title :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="title" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption"></div>
                            <div class = "field">
                                <input type="submit" name="search" value="Search" class="button" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab1-->
                <div class="clear" style="height:30px"></div>
                <!-- Delete confirmation popup -->
                <div id="myConfirm" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
    </div><!--bodyWrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <?php
    }else{//End isset session
        exit(header('location: login.php'));
    }

    ?>
    </body>
</html>
<?php ob_end_flush(); ?>
<?php include("common/close.con.php"); ?>
