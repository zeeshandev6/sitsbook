<?php
    ob_start();

    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/accounts.php');
    include ('common/classes/brokers.php');

    $objBroker        = new brokers();
    $objAccountCodes  = new ChartOfAccounts();

    if(isset($_POST['delete_id'])){
      $broker_id              = mysql_real_escape_string($_POST['delete_id']);
      $broker_details         = $objBroker->getRecordDetailsByID($broker_id);
      $objAccountCodes->deleteAccCode($broker_details['ACCOUNT_CODE']);
      $objBroker->delete($broker_id);
      exit();
    }

    $start = 0;
    $end  = $objConfigs->get_config('PER_PAGE');

    if(isset($_GET['search'])){

      $objBroker->first_name  = $_GET['first_name'];
      $objBroker->last_name   = $_GET['last_name'];
      $objBroker->phones      = $_GET['phone'];
      $objBroker->mobiles     = $_GET['mobile'];
      $objBroker->city        = $_GET['city'];

      $myPage = 1;
      if(isset($_GET['page'])){
        $page = $_GET['page'];
        $myPage = $page;
        $start = ((int)$page * $end) - $end;
      }

      $brokerList = $objBroker->searchBrokers($start,$end);
      $row          = mysql_num_rows($brokerList);

      $queryCount = $objBroker->totalMatchRecords;
      $pageCount = ceil($queryCount/$end);
    }else{
      $brokerList = $objBroker->getList();
      $row          = mysql_num_rows($brokerList);
    }
    $counter_start = $start;
    $counter_start++;
?>
<!DOCTYPE html 
>



<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Admin Panel</title>
  <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
  <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <!-- jQuery -->
  <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
  <script src="resource/scripts/jquery-ui.min.js"></script>
  <script src="resource/scripts/bootstrap.min.js"></script>
  <script src="resource/scripts/bootstrap-select.js"></script>
  <script src="resource/scripts/tab.js"></script>
  <script type="text/javascript" src="resource/scripts/broker.js"></script>
  <script type="text/javascript" src="resource/scripts/configuration.js"></script>
  <script type="text/javascript">
      $(document).ready(function(){
        $("select").selectpicker();
      });
  </script>
</head>

<body>
  <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
  <div id="bodyWrapper">
    <div class = "content-box-top" style="overflow:visible;">
      <div class = "summery_body">
        <div class = "content-box-header">
          <p style="font-size:15px;"><B>Broker Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span> </p>
            <span id="tabPanel">
              <div class="tabPanel">
                <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                <div class="tab" id="tab2" onclick="tab('2','1','2');">Search</div>
                <a href="add-broker-details.php"><div class="tab">New</div></a>
              </div>
            </span>
            <div style = "clear:both;"></div>
          </div><!-- End .content-box-header -->
          <div style = "clear:both; height:20px"></div>

          <div id="bodyTab1">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th rowspan="2" width="5%" style="text-align:center">Sr.</th>
                  <th rowspan="2" width="10%" style="text-align:center">Account Code</th>
                  <th rowspan="2" width="20%" style="text-align:center">Title</th>
                  <th width="10" style="text-align:center">Mobile</th>
                  <th width="10"style="text-align:center">Email</th>
                  <th rowspan="2" width="10%" style="text-align:center">Action</th>
                </tr>
              </thead>
              <tbody>

                <?php
                if($row>0){
                  while($brokerDetails = mysql_fetch_assoc($brokerList)){
                    ?>
                    <tr id="recordPanel">
                      <td class="broker-list text-center"><?php echo $counter_start; ?></td>
                      <td class="broker-list text-center"><?php echo $brokerDetails['ACCOUNT_CODE']; ?></td>
                      <td class="broker-list text-left"><?php echo $brokerDetails['ACCOUNT_TITLE']; ?></td>
                      <td class="broker-list"><?php echo $brokerDetails['MOBILES']; ?></td>
                      <td class="broker-list"><?php echo $brokerDetails['EMAILS']; ?></td>
                      <td style="text-align:center">
                        <a href="add-broker-details.php?bid=<?php echo $brokerDetails['ID']; ?>" id="view_button"> <i class="fa fa-pencil"></i></a>
                        <a onClick="deleteBroker(this);" id="del" value="<?php echo $brokerDetails['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times" ></i></a>
                      </td>
                    </tr>
                    <?php
                    $counter_start++;
                  }
                }
                else{
                  ?>
                  <tr id="recordPanel">
                    <td style="text-align:center" colspan="7"> No Record Found!</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
            <table align="center" border="0" id="navbar">
              <tr>
                <td style="text-align: center">
                  <?php
                  if(isset($queryCount)){
                    if($queryCount>$pageCount){
                      ?>
                      <nav>
                        <ul class="pagination">
                          <?php
                          if($myPage > 1)
                          {
                            echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=1'>Fst</a></li>";
                            echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=". ($myPage-1) ."'>Prv</a></li>";
                          }

                          for($x=$myPage-4; $x<$myPage; $x++)
                          {
                            if($x > 0){
                              echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=". $x ."'>". $x ."</a></li>";
                            }
                          }

                          print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                          for($y=$myPage+1; $y<$myPage+5; $y++)
                          {
                            if($y <= $pageCount){
                              echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=". $y ."'>". $y ."</a></li>";
                            }
                          }

                          if($myPage < $pageCount)
                          {
                            echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=". ($myPage+1) ."'>Nxt</a></li>";
                            echo "<li><a href='brokers-management.php?first_name=".$_GET['first_name']."&last_name=".$_GET['last_name']."&phone=".$_GET['phone']."&mobile=".$_GET['mobile']."&city=".$_GET['city']."&search=Search&page=". $pageCount ."'>Lst</a></li>";
                          }

                          ?>
                        </ul>
                      </nav>

                      <?php }
                    }
                    ?>
                  </td>
                </tr>
              </table>
            </div> <!--bodyTab1-->

            <div id = "bodyTab2" style=" display:none">
              <form method="get" action="" class="form-horizontal">
                <div class="col-xs-12">
                  <div class="form-group">
    								<label class="control-label col-sm-2">First Name</label>
    								<div class="col-sm-10">
                      <input type="text" class="form-control" name="first_name" />
    								</div>
    							</div>

                  <div class="form-group">
    								<label class="control-label col-sm-2">Last Name</label>
    								<div class="col-sm-10">
                      <input type="text" class="form-control" name="last_name" />
    								</div>
    							</div>

                  <div class="form-group">
    								<label class="control-label col-sm-2">Phone</label>
    								<div class="col-sm-10">
                      <input type="text" class="form-control" name="phone" />
    								</div>
    							</div>

                  <div class="form-group">
    								<label class="control-label col-sm-2">Mobile</label>
    								<div class="col-sm-10">
                      <input type="text" class="form-control" name="mobile" maxlength="11" />
    								</div>
    							</div>

                  <div class="form-group">
    								<label class="control-label col-sm-2">City</label>
    								<div class="col-sm-10">
                      <input type="text" class="form-control" name="city" />
    								</div>
    							</div>

                  <div class="form-group">
    								<label class="control-label col-sm-2"></label>
    								<div class="col-sm-10">
                      <input type="submit" name="search" value="Search" class="button" />
    								</div>
    							</div>
                </div>
              </form>
            </div> <!--bodyTab1-->
            <div class="clear" style="height:30px"></div>
          </div><!-- End summer -->
        </div><!-- End .content-box-top-->

        <!-- Delete confirmation popup -->
        <div id="myConfirm" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Confirmation</h4>
              </div>
              <div class="modal-body">
                <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      </div><!--bodyWrapper-->
      <script type="text/javascript">
      $(function(){
        <?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?>
          $("#tab2").click();
        <?php } ?>
      });
      </script>
    </body>
    </html>
    <?php ob_end_flush(); ?>
    <?php include("conn.close.php"); ?>
