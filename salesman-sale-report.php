<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     = new ChartOfAccounts();
	$objSales 			 		 = new Sale();
	$objSaleDetails      = new SaleDetails();
	$objSaleReturnDetails= new SaleReturnDetails();
	$objItems            = new Items();
	$objServices         = new Services();
	$objItemCategory     = new itemCategory();
	$objTaxRates         = new TaxRates();
	$objDepartments      = new Departments();
	$objConfigs 		 		 = new Configs();

	$suppliersList   = $objAccountCodes->getAccountByCatAccCode('010104');
	$cashAccounts  	 = $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList     = $objTaxRates->getList();

	$brands_list     = $objItems->getBrandNameList();


	$currency_type 			 = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   			 = $objConfigs->get_config('USE_CARTONS');

	if(isset($_GET['search'])){

		$objSales->fromDate 		= ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));

		if(isset($_GET['user_id'])){
			$objSales->user_id 			= ($_SESSION['classuseid'] == 1)?$_GET['user_id']:$_GET['user_id'];
		}else{
			$objSales->user_id 			= $_SESSION['classuseid'];
		}

		$supplierTypeReport   =  ($objSales->supplierAccCode == '')?false:true;
		$itemTypeReport       =  ($objSales->item == '')?false:true;

		//both selected
		if(($objSales->supplierAccCode != '') && ($objSales->item != '')){
			$supplierTypeReport = true;
		}

		$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objSales->supplierAccCode);
		$theItemTitle      = $objItems->getItemTitle($objSales->item);

		if($supplierTypeReport){
			$titleRepo = $supplierHeadTitle;
		}elseif($itemTypeReport){
			$titleRepo = $theItemTitle;
		}else{
			$titleRepo = '';
		}
		if($objSales->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objSales->fromDate = $startThisMonth;
		}

		$purchaseReport = $objSales->salesmanReport();
	}
?>
<!DOCTYPE html>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$(".printThis").click(function(){
			var MaxHeight = 450;
			var RunningHeight = 0;
			var PageNo = 1;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if (RunningHeight + $(this).height() > MaxHeight){
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate = $(".repoDate").text();
			//remove previous thead/tfoot
			$(".tHeader").remove();
			$(".tableFooter").remove();
			$(".repoDate").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();

			$("div.tablePage").each(function(i,e){
				$(this).prepend($(".pageHeader").first().clone());
			});
			$(".pageHeader").first().remove();
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports-horizontal.css',
				pageTitle: "Sit Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$('select').selectpicker();
		if($(".carton_th").length){
			var colspan = $(".carton_th").prevAll().length;
		}else{
			var colspan = $(".tHeader th").eq(4).prevAll().length;
		}
		$(".total_tf").attr('colspan',colspan);
	});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sales Reports</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="">
								<div class="caption">Sale Date </div>
								<div class="field" style="width:300px;">
									<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
								</div><!--field-->
								<div class="clear"></div>

								<div class="caption">Salesman</div>
								<div class="field" style="width:300px;position:relative;">
									<select name="user_id" class="form-control" data-style="btn btn-default">
										<option></option>
										<?php
										$userList = $objAccounts->getActiveList();
										if(mysql_num_rows($userList)){
											while($the_user = mysql_fetch_array($userList)){
												?>
												<option value="<?php echo $the_user['ID']; ?>"><?php echo $the_user['FIRST_NAME']." ".$the_user['LAST_NAME']; ?></option>
												<?php
											}
										}
										?>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption"></div>
								<div class="field">
									<input type="submit" value="Search" name="search" class="button"/>
								</div>
								<div class="clear"></div>
			</form>
		</div><!--form-->

		<?php
		if(isset($purchaseReport)){
			?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							Salesman Issue Report
							<?php
							if($objSales->user_id != ''){
								$ozer_name = $objAccounts->getFullName($objSales->user_id);
								?>
								- <?php echo $ozer_name['FIRST_NAME']." ".$ozer_name['LAST_NAME']; ?>
								<?php
							}else{
								?>
								<?php echo ($titleRepo == '')?"":" - ".$titleRepo; ?>
								<?php
							}
							?>
						</p>
						<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
						<div class="clear"></div>
					</div>
					<?php
					$prevBillNo = '';
					if(mysql_num_rows($purchaseReport)){
						?>
						<table class="tableBreak">
							<thead class="tHeader">
								<tr style="background:#EEE;">
										<th width="20%" style="font-size:12px;text-align:center">Item</th>
										<th width="10%"  style="font-size:12px;text-align:center">Qty</th>
								</tr>
								</thead>
								<tbody>
									<?php
									$total_crtn   = 0;
									$total_qty    = 0;
									$total_price  = 0;
									$total_tax    = 0;
									$total_amount = 0;
									$total_cost   = 0;
									$total_pnl    = 0;
									while($detailRow = mysql_fetch_array($purchaseReport)){
										$supplierTitle = $objAccountCodes->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);
										if($detailRow['SERVICE_ID'] > 0){
											$item_id  = $detailRow['SERVICE_ID'];
											$itemName = $objServices->getTitle($item_id);
											$item_type= 'S';
										}else{
											$item_id  = $detailRow['ITEM_ID'];
											$itemName = $objItems->getRecordDetails($detailRow['ITEM_ID']);
											$itemName = substr($itemName['NAME'],0,25)."(".$itemName['ITEM_BARCODE'].")";
											$item_type= 'I';
										}
										$user_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
										$user_full_name = $user_full_name['FIRST_NAME']." ".$user_full_name['LAST_NAME'];
										$discount_type = ($detailRow['DISCOUNT_TYPE'] == 'P')?"%":$currency_type;

										if($detailRow['BILL_NO'] !== $prevBillNo){
											$thisBillNo = $detailRow['BILL_NO'];
										}else{
											$thisBillNo = '';
										}
										?>
										<tr id="recordPanel" class="alt-row">
												<td class="text-left"><?php echo $itemName; ?></td>
												<td class="text-center"><?php echo $detailRow['TOTAL_QTY']; ?></td>
											</tr>
											<?php
											$total_crtn  += $detailRow['CARTONS'];
											$total_qty   += $detailRow['TOTAL_QTY'];
											$total_price += $detailRow['SUB_AMOUNT'];
											$total_tax += $detailRow['TAX_AMOUNT'];
											$total_amount   += $detailRow['TOTAL_AMOUNT'];
											$prevBillNo = $detailRow['BILL_NO'];
										}
										?>

									</tbody>
									<?php
								}//end if
								if(mysql_num_rows($purchaseReport)){
									$total_price  = number_format($total_price,2);
									$total_tax    = number_format($total_tax,2);
									$total_amount = number_format($total_amount,2);
									$total_cost   = number_format($total_cost,2);

									$columnSkip = 3;
									if($itemTypeReport || $supplierTypeReport){
										$columnSkip = 4;
									}
									?>
										<tfoot class="tableFooter">
											<tr>
												<td style="text-align:right;" colspan="" >Total:</td>
												<td class="text-center"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
											</tr>
										</tfoot>
										<?php
									}
									?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					} //end if is generic report
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
if(isset($reportType)&&$reportType=='generic'){
	?>
	tab('1', '1', '2')
	<?php
}
?>
<?php
if(isset($reportType)&&$reportType=='specific'){
	?>
	tab('2', '1', '2')
	<?php
}
?>
</script>
