<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/mobile-purchase.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/items.php');
	include('common/classes/suppliers.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('available-stock-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes        = new ChartOfAccounts();
	$objScanPurchase        = new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objItems               = new Items();
	$objItemCategory        = new itemCategory();
	$objSuppliers		    		= new suppliers();
	$objConfigs 		    		= new Configs();

	$suppliersList   = $objSuppliers->getList();
	$item_categories = $objItemCategory->getList();
	$cashAccounts    = $objAccountCodes->getAccountByCatAccCode('010101');

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	$titleRepo = '';

	$objScanPurchase->fromDate 			= "";
	$objScanPurchase->toDate   			= "";
	$objScanPurchase->item_id     	= "";
	$objScanPurchase->supp_acc_code = "";
	$objScanPurchase->state 	    	= "A";
	$objScanPurchase->categories    = isset($_GET['cats'])?mysql_real_escape_string($_GET['cats']):"";

	$current_user  	= $user_id;
	$purchaseReport = $objScanPurchase->availableStockReport();
	$users_list 		= $objAccounts->getActiveList();
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
		$(window).on('load',function(){
			$("input.column_configure").change(function(){
				var conf,lng=2;
				$("td[data-type='alone']").hide();
				$("input.column_configure").each(function(){
					conf =  $(this).val();
					$("*[data-type='"+conf+"']").hide();
				});
				$("input.column_configure:checked").each(function(){
					conf =  $(this).val();
					$("*[data-type='"+conf+"']").show();
					if(conf!='stock'&&conf!='price'){
						lng++;
					}else{
						$("td[data-type='alone']").show();
					}
				});
				$("td[data-type='alone']").attr('colspan',lng);
			});
			$(".printThis").click(function(){
				$("div#form").hide();
				var MaxHeight = 884;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){

					if(PageNo == 1){
						MaxHeight = 750;
					}

					if(PageNo != 1){
						MaxHeight = 820;
					}

					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){

					if(i == 1){
						MaxHeight = 750;
					}

					if(i != 1){
						MaxHeight = 820;
					}

					$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
					debug: false,
					importCSS: false,
					printContainer: true,
					loadCSS: 'resource/css/reports.css',
					pageTitle: "Sitsbook.com",
					removeInline: false,
					printDelay: 500,
					header: null
				});
			});
			$('select').selectpicker();
		});
		var categorize = function(){
			var categories;
			if($("select[name=categories] option:selected").length){
				categories = $("select[name=categories]").selectpicker('val');
				window.location.href = 'available-stock-report.php?cats='+categories;
			}else{
				window.location.href = 'available-stock-report.php';
			}
		};
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Available Stock</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div class="" id="form">


					<div class="pull-left">
						<select class="form-control" name="categories" multiple data-live-search="true" data-width="350px">
								<option value=""></option>
							<?php
									if(mysql_num_rows($item_categories)){
										while($cat_row = mysql_fetch_assoc($item_categories)){
							?>
								<option value="<?php echo $cat_row['ITEM_CATG_ID']; ?>"><?php echo $cat_row['NAME']; ?></option>
							<?php
										}
									}
							?>
						</select>
					</div>
					<div class="pull-left">
						<button type="button" onclick="categorize();" class="btn btn-default">Go</button>
					</div>
					<div class="clear"></div>

					<div class="caption text-left">
						<input id="cmn-toggle-thumb" value="thumb" class="css-checkbox column_configure" type="checkbox" checked />
						<label for="cmn-toggle-thumb" class="css-label" style="margin-top:5px;margin-right:-25px;">Thumnail</label>
					</div>
					<div class="clear"></div>

					<div class="caption text-left">
						<input id="cmn-toggle-comp" value="company" class="css-checkbox column_configure" type="checkbox" checked />
						<label for="cmn-toggle-comp" class="css-label" style="margin-top:5px;margin-right:-25px;">Company Brand</label>
					</div>
					<div class="clear"></div>

					<div class="caption text-left">
						<input id="cmn-toggle-stock" value="stock" class="css-checkbox column_configure" type="checkbox" checked />
						<label for="cmn-toggle-stock" class="css-label" style="margin-top:5px;margin-right:-25px;">Stock</label>
					</div>
					<div class="clear"></div>

					<div class="caption text-left">
						<input id="cmn-toggle-price" value="price" class="css-checkbox column_configure" type="checkbox" checked />
						<label for="cmn-toggle-price" class="css-label" style="margin-top:5px;margin-right:-25px;">Cost</label>
					</div>
					<div class="clear"></div>

					<div class="" style="margin-top: 40px"></div>
				</div>
				<div id="bodyTab1">
					<?php
					if(true){
						?>

						<span style="float:right;"><button class="button printThis"> <i class="fa fa-print"></i> Print</button></span>
						<div class="clear"></div>
						<div id="bodyTab" class="printTable" style="margin: 0 auto;">
							<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
								<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Available Stock</p>
								<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
							</div>
							<?php
							$prevBillNo = '';
							$counter = 1;
							if(mysql_num_rows($purchaseReport)){
								?>
								<table class="tableBreak">
									<thead class="tHeader">
										<tr style="background:#EEE;">
											<th width="5%" style="font-size:12px !important ;text-align:center" data-type="counter">Sr.</th>
											<th width="10%" style="font-size:12px !important ;text-align:center" data-type="thumb">Thumbnail</th>
											<th width="25%" style="font-size:12px !important ;text-align:center" data-type="company">Company Brand</th>
											<th width="25%" style="font-size:12px !important ;text-align:center" data-type="item">Item</th>
											<th width="10%" style="font-size:12px !important ;text-align:center" data-type="stock">Stock</th>
											<th width="10%" style="font-size:12px !important ;text-align:center" data-type="price">Cost(<?php echo $currency_type; ?>)</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$total_amount = 0;
										$total_stock  = 0;
										$prev_bill_no = 0;
										$path = "uploads/products/";
										while($row = mysql_fetch_array($purchaseReport)){
											$supplierTitle = $objAccountCodes->getAccountTitleByCode($row['SUPP_ACC_CODE']);
											$item = $objItems->getRecordDetails($row['ITEM_ID']);
											if($row['STOCK_STATUS'] == 'S'){
												$row['STOCK_STATUS'] = 'Sold';
											}elseif($row['STOCK_STATUS'] == 'A'){
												$row['STOCK_STATUS'] = 'In Stock';
											}elseif($row['STOCK_STATUS'] == 'R'){
												$row['STOCK_STATUS'] = 'Returned';
											}
											$u_full_name = $objAccounts->getFullName($row['USER_ID']);
											?>
											<tr id="recordPanel" class="alt-row">
												<td style="text-align:center;font-size:18px !important;" data-type="counter"><?php echo $counter; ?></td>
												<td style="text-align:center;font-size:18px !important;" data-type="thumb">
													<?php if(is_file($path."thumb_".$item['IMAGE'])){ ?>
														<img src="<?php echo $path."thumb_".$item['IMAGE']; ?>" style="height:120px;" alt="" />
														<?php } ?>
													</td>
													<td style="text-align:center;font-size:18px !important;" data-type="company"><?php echo $item['COMPANY']; ?></td>
													<td style="text-align:left;font-size:18px !important;" data-type="name"><?php echo $item['NAME']; ?><?php echo ($row['COLOUR']=='')?"":"-".$row['COLOUR']; ?></td>
													<td style="text-align:center;font-size:18px !important;" data-type="stock"><?php echo $row['QTY']; ?></td>
													<td style="text-align:center;font-size:18px !important;" data-type="price"><?php  echo number_format($row['TOTAL_PRICE'],2); ?></td>
												</tr>
												<?php
												$total_amount += $row['TOTAL_PRICE'];
												$total_stock  += $row['QTY'];
												$prev_bill_no  = $row['BILL_NO'];
												$counter++;
											}
											?>

										</tbody>
										<?php
									}//end if
									if(mysql_num_rows($purchaseReport)){
										?>
										<tfoot class="tableFooter">
											<tr>
												<td style="text-align:right;font-size:18px !important;"  colspan="4" data-type="alone">Total:</td>
												<td style="text-align:center;font-size:18px !important;" data-type="stock"> <?php echo number_format($total_stock,0); ?> </td>
												<td style="text-align:center;font-size:18px !important;" data-type="price"> <?php echo number_format($total_amount,2); ?> </td>
											</tr>
										</tfoot>
										<?php
									}
									?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
							<?php
						} //end if is generic report
						?>
					</div> <!-- End bodyTab1 -->
				</div> <!-- End .content-box-content -->
			</div><!--content-box-->
		</div><!--body-wrapper-->
	</body>
	</html>
	<?php include('conn.close.php'); ?>
	<script>
	<?php
	if(isset($reportType)&&$reportType=='generic'){
		?>
		tab('1', '1', '2')
		<?php
	}
	?>
	<?php
	if(isset($reportType)&&$reportType=='specific'){
		?>
		tab('2', '1', '2')
		<?php
	}
	?>
	</script>
