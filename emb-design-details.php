<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/design.php');

	$ObjDesign = new Design();

	if(!is_dir('uploads')){
		mkdir('uploads');
	}

	if(!is_dir('uploads/designs/')){
		mkdir('uploads/designs/');
	}

	$path = "uploads/designs/";

	if(!is_dir($path)){
		mkdir($path,0777);
	}

	if(isset($_GET['did'])){
		$did = $_GET['did'];
		$designRow = mysql_fetch_array($ObjDesign->getRecordDetails($did));
	}
	if(isset($_POST['update'])){
		$ObjDesign->title = mysql_real_escape_string($_POST['title']);
		$ObjDesign->code  = mysql_real_escape_string($_POST['code']);
		$ObjDesign->description = mysql_real_escape_string($_POST['description']);
		$id = $_POST['did'];
		$getdata = mysql_fetch_array($ObjDesign->getRecordDetails($id));
		$image_name_before = $getdata['IMAGE'];
		if(!$_FILES['image']['error'] > 0){
			if($image_name_before != ''){
				if(file_exists($path.$image_name_before))
				{
					unlink($path.$image_name_before);
				}
			}
			$renaming_mask = md5(date("ymd").time()).".".pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
			move_uploaded_file($_FILES['image']['tmp_name'],$path.$renaming_mask);
			$ObjDesign->image = $renaming_mask;
		}else{
			$ObjDesign->image = $image_name_before;
		}
		$ObjDesign->update($id);
		$page = (isset($_GET['page']))?$_GET['page']:"1";
		echo "<script>window.location.replace(\"emb-design-details.php?did=".$id."&page=".$page."&updated\");</script>";
		exit();
	}

	if(isset($_POST['adddesign'])){
		$ObjDesign->title = mysql_real_escape_string($_POST['title']);
		$ObjDesign->code  = mysql_real_escape_string($_POST['code']);
		$ObjDesign->description = mysql_real_escape_string($_POST['description']);
		$renaming_mask = md5(date("ymd").time()).".".pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
		$success = move_uploaded_file($_FILES['image']['tmp_name'],$path.$renaming_mask);
		$ObjDesign->image = $renaming_mask;
		$inserted = $ObjDesign->addDesign();
		$insertedId = mysql_insert_id();
		echo "<script>window.location.replace(\"emb-design-details.php?did=".$insertedId."&added\");</script>";
		exit();
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Sitsbook - Design Management</title>
	<link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<style media="screen">
		table th{
		font-weight: bold !important;
		}
		table th,table td{
		padding: 10px !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
            $("input[name='code']").setFocusTo("input[name='title']");
			$("input[name='title']").setFocusTo("textarea[name='description']");
			$("textarea[name='description']").setFocusTo("input[type='file']");
			$("input[name='code']").focus();
        });
	</script>
    <style>
		#form #view_button{
			float: right;
			margin: 10px 50px 10px 0px;
			padding: 5px;
			width: 10%;
		}
	</style>

</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
	            <p> <i class="fa fa-image"></i> Designs Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "?page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                    	<a href="emb-design-list.php<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="emb-design-list.php?goSearch"><div class="tab">Search</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1">
                    <div id="form">
<?php

?>
                    <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">



											<div class="form-group">
												<label class="control-label col-sm-2">Design Code</label>
												<div class="col-sm-10">
													<input type="text" name="code" class="form-control"  value="<?php echo (isset($designRow))?$designRow['DESIGN_CODE']:""; ?>" required />
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">Design Name</label>
												<div class="col-sm-10">
													<input type="text" name="title" class="form-control" value="<?php echo (isset($designRow))?$designRow['TITLE']:""; ?>"  required />
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">Description</label>
												<div class="col-sm-10">
													<textarea name="description" style="height:100px;" class="form-control"><?php echo (isset($designRow))?$designRow['DESCRIPTION']:""; ?></textarea>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2"><?php echo (isset($designRow))?"Update Image":"Upload Image"; ?></label>
												<div class="col-sm-10">
													<input type="file" name="image"  class="btn btn-default" />
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2"></label>
												<div class="col-sm-4	 thumbnail">
													<?php if(isset($designRow)){ ?>
		                        <img src="uploads/designs/<?php echo (isset($designRow))?$designRow['IMAGE']:""; ?>" />
													<?php } ?>
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2"></label>
												<div class="col-sm-10">
<?php
						if(isset($did)){
?>
													<input type="hidden" value="<?php echo $did; ?>" name="did" />
                          <input type="submit" value="Update" name="update" class="btn btn-primary" />
                          <input type="reset" onclick="window.location.replace('emb-design-list.php');" value="Cancel"  class="btn btn-default pull-right" />
<?php
						}else{
?>
                          <input type="submit" value="Save" name="adddesign" class="btn btn-primary" />
                          <input type="reset" value="Reset"  class="btn btn-default pull-right" />
<?php
						}
?>
												</div>
											</div>
                    </form>
                    </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
