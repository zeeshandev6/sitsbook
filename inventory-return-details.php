<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/inventory-return.php');
	include('common/classes/inventory-return-details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/accounts.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('purchase-return',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts 										= new ChartOfAccounts();
	$objInventory 									= new inventory();
	$objInventoryDetails 						= new inventory_details();
	$objInventoryReturn 						= new InventoryReturn();
	$objInventoryReturnDetails 	    = new InventoryReturnDetails();
	$objItems 											= new Items();
	$objItemCategory 								= new itemCategory();
	$objTaxRates 										= new TaxRates();
	$objDepartments 								= new Departments();

	$suppliersList   = $objInventoryReturn->getSuppliersList();
	$departmentList  = $objDepartments->getList();
	$taxRateList     = $objTaxRates->getList();

	$purchase_id 				= 0;
	$purchase_return_id = 0;

	if(isset($_GET['pid'])){
		$purchase_id = mysql_real_escape_string($_GET['pid']);
		if($purchase_id != '' && $purchase_id > 0){
			$inventory 				= $objInventory->getDetail($purchase_id);
			$inventoryDetails = $objInventoryDetails->getList($purchase_id);
		}
	}else{
		header('location:inventory.php');
		eixt();
	}
	$items_array = array();
	$ref_url 		 = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'inventory.php';
?>
<!DOCTYPE html>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" type="text/css" href="resource/css/reset.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/style.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/invalid.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/form.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/tabs.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/reports.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/font-awesome.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/bootstrap.min.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/bootstrap-select.css"   />
    <link rel="stylesheet" type="text/css" href="resource/css/jquery-ui/jquery-ui.min.css"  />
    <style>
    	.wel_made td{
				padding: 7px !important;
    	}
			.wel_made th{
				padding: 5px !important;
				font-weight: bold !important;
    	}
			.totals td{
				font-weight: bold !important;
			}
			.entry_table th{
				padding: 5px !important;
				font-weight: bold !important;
			}
			.entry_table td{
				padding: 7px !important;
			}
    	.panel{
    		padding: 0px;
    	}
    	hr{
    		margin: 10px;
    	}
      input.taxRate{
          text-align: center;
      }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/inventory.return.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Return Panel</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="inventory.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="inventory.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Return</div>
                        <a href="inventory-details.php"><div class="tab">Purchase</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div class="col-sm-12">
                    		<div class="title">Purchase Details </div>
                            <div class="panel panel-default">
                            	<div class="panel-heading">
                            		<div class="caption_simple pull-left">Purchase Date :  <?php echo date("d-m-Y",strtotime($inventory['PURCHASE_DATE'])); ?></div>
                                    <input type="hidden" value="<?php echo $inventory['PO_NUMBER']; ?>" class="po_number" />
                                    <input type="hidden" value="<?php echo $inventory['DISCOUNT_TYPE']; ?>" class="discount_type" />
                            		<?php
                            				if(substr($inventory['SUPP_ACC_CODE'], 0,6) == '010101'){
                            					$customer_name = $inventory['SUPPLIER_NAME'];
																		}else{
																			$customer_name = $objAccounts->getAccountTitleByCode($inventory['SUPP_ACC_CODE']);
																		}
																		if(substr($inventory['SUPP_ACC_CODE'], 0,6) == '010101'){
                            		?>
                            		<div class="caption_simple pull-left">Cash Purchase</div>
                            		<?php
                            				}
                            		?>
                            		<div class="caption_simple pull-left">Supplier :  	   <?php echo $customer_name; ?></div>
                            		<div class="caption_simple pull-left">Bill Number :    <?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?></div>
																<div class="caption_simple pull-left">Discount Type :  <?php echo ($inventory['DISCOUNT_TYPE'] == 'R')?"Amount":"Percentage"; ?></div>
	                            	<div class="clear"></div>
                            	</div>
		                        <input type="hidden" class="purchase_id" value="<?php echo $purchase_id; ?>" />
		                        <input type="hidden" class="purchase_return_id" value="<?php echo $purchase_return_id; ?>" />
<?php
						if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
							$qty = 0;
							$stotal = 0;
							$ttotal = 0;
							$atotal = 0;
?>
                          <table class="wel_made purch_table">
                            <thead>
                                <tr>
                                   <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
																	 <?php if($medicalStoreAddon=='Y'){ ?>
																	 <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Batch#</th>
																	 <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">ExpiryDate</th>
																	 <?php } ?>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Discount</th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<tr style="display:none;" class="calculations"></tr>
<?php
							while($invRow = mysql_fetch_array($inventoryDetails)){
								$itemName                        = $objItems->getItemTitle($invRow['ITEM_ID']);
								$items_array[$invRow['ITEM_ID']] = $objItems->getRecordDetails($invRow['ITEM_ID']);
?>
                                <tr class="alt-row bill-rows" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td class="itemName text-left" data-item='<?php echo $invRow['ITEM_ID']; ?>' data-batch-no="<?php echo $invRow['BATCH_NO'] ?>">
																			<?php echo $itemName; ?> ( <?php echo $items_array[$invRow['ITEM_ID']]['ITEM_BARCODE'] ?> )
                                    </td>
																		<?php if($medicalStoreAddon=='Y'){ ?>
                                    <td class="text-center batch_no_td"><?php echo $invRow['BATCH_NO'] ?></td>
																		<td class="text-center expiry_date_td"><?php echo date('d-m-Y',strtotime($invRow['EXPIRY_DATE'])); ?></td>
																		<?php } ?>
																		<td class="text-center bill_table_td quantity" data-batch-no="<?php echo $invRow['BATCH_NO']; ?>" data-expiry-date="<?php echo $invRow['EXPIRY_DATE']; ?>" ><?php echo $invRow['STOCK_QTY'] ?></td>
                                    <td class="text-center" data-unitprice-id="<?php echo $invRow['ITEM_ID']; ?>"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td class="text-center" ><?php echo $invRow['PURCHASE_DISCOUNT'] ?></td>
                                    <td class="text-center" ><?php echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td class="text-center" ><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td class="text-center" ><?php echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td class="text-center" ><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                </tr>
<?php
								$qty    += $invRow['STOCK_QTY'];
								$stotal += $invRow['SUB_AMOUNT'];
								$ttotal += $invRow['TAX_AMOUNT'];
								$atotal += $invRow['TOTAL_AMOUNT'];
							}
?>
                                <tr class="totals">
                                	<td class="text-center">Total</td>
																	<?php if($medicalStoreAddon=='Y'){ ?>
																	<td class="text-center" colspan="2"> - - - </td>
																	<?php } ?>
                                  <td class="text-center"><?php echo $qty; ?></td>
                                  <td class="text-center"> - - - </td>
                                  <td class="text-center"> - - - </td>
                                  <td class="text-center"><?php echo number_format($stotal,2); ?></td>
                                  <td class="text-center"> - - - </td>
                                  <td class="text-center"><?php echo number_format($ttotal,2); ?></td>
                                  <td class="text-center"><?php echo number_format($atotal,2); ?></td>
                                </tr>
                            </tbody>
                        	</table>
                        </div>
<?php
						}
?>
						<div class="title">Purchase Return Panel </div>
						<div class="panel panel-default">
									<div class="panel-heading">
										<div class="caption_simple">
											Return Date
											<input type="text" name="rDate" value="<?php echo date('d-m-Y'); ?>" class="datepicker input_ghost" style="width:150px;padding-left: 40px;margin-left: -30px;" />
										</div>
										<div class="clear"></div>
									</div>
		              <table class="entry_table">
		                  <thead>
		                      <tr>
														<th width="10%" class="text-center">Item</th>
														<?php if($medicalStoreAddon=='Y'){ ?>
														<th width="5%" 	class="text-center">Batch#</th>
														<th width="8%" 	class="text-center">ExpiryDate</th>
														<?php } ?>
														<th width="8%" 	class="text-center">Quantity</th>
														<th width="8%" 	class="text-center">Unit Price</th>
														<th width="5%" 	class="text-center">Discount</th>
														<th width="8%" 	class="text-center">Sub Total</th>
														<th width="5%" 	class="text-center"> Tax @ </th>
														<th width="8%" 	class="text-center">Tax Amount</th>
														<th width="8%" 	class="text-center">Total Amount</th>
														<th width="5%" 	class="text-center">Stock</th>
														<th width="8%" 	class="text-center">Action</th>
		                      </tr>
		                  </thead>

		                  <tbody>
		                      <tr class="quickSubmit" style="background:none">
		                          <td style="padding:0; line-height:0">
		                              <select class="itemSelector show-tick form-control"
		                                      data-style="btn-default"
		                                      data-live-search="true" style="border:none">
		                                 <option selected value=""></option>
		<?php
								foreach ($items_array as $key => $value){
									$cat = $objItemCategory->getTitle($key)
		?>
		                                 			<option value="<?php echo $value['ID']; ?>" data-subtext="<?php echo $cat; ?>" ><?php echo $value['ITEM_BARCODE']; ?>-<?php echo $value['NAME']; ?></option>
		<?php
								}
		?>
		                              </select>
		                          </td>
															<?php if($medicalStoreAddon=='Y'){ ?>
															<td style="padding:0; line-height:0">
		                              <select class="form-control batch_no" name="batch_no">
																		<option value="">Select</option>
		                              </select>
		                          </td>
															<td style="padding:0; line-height:0">
																	<select class="form-control expiry_date" name="expiry_date">
																		<option value="">Select</option>
		                              </select>
		                          </td>
															<?php } ?>
		                          <td style="padding:0; line-height:0">
		                              <input type="text" class="quantity form-control text-center"/>
		                          </td>
		                          <td style="padding:0; line-height:0">
		                              <input type="text" class="unitPrice form-control text-center"/>
		                          </td>
		                          <td style="padding:0; line-height:0">
		                              <input type="text" class="discount form-control text-center" />
		                          </td>
		                          <td style="padding:0; line-height:0;">
		                              <input type="text"  readonly  value="0" class="subAmount form-control text-center" />
		                          </td>
		                          <td style="padding:0; line-height:0;" class="taxTd">
		                          	<input class="taxRate form-control text-center" value=""  />
		                          </td>
		                          <td style="padding:0; line-height:0;">
		                              <input type="text"  readonly value="0" class="taxAmount form-control text-center" />
		                          </td>
		                          <td style="padding:0; line-height:0;">
		                              <input type="text"  readonly value="0" class="totalAmount form-control text-center" />
		                          </td>
		                          <td style="padding:0; line-height:0">
		                              <input type="text" class="inStock form-control" theStock="0" readonly />
		                              <input type="hidden" class="qty_limit" /><input type="hidden" class="qty_returned text-center" />
		                          </td>
		                          <td style="text-align:center;background-color:#f5f5f5;">
																<input type="button" class="addDetailRow" value="Enter" id="clear_filter" />
															</td>
		                      </tr>
		               	</tbody>
	                  <tbody>
	                  	<!--doNotRemoveThisLine-->
	                    <tr style="display:none;" class="calculations"></tr>
	                    <!--doNotRemoveThisLine-->
	<?php
		if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
			while($invRow = mysql_fetch_array($inventoryDetails)){
				$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
	?>
	                      <tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
													<td style="text-align:left;" class="itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
													<?php if($medicalStoreAddon=='Y'){ ?>
													<td style="text-align:center;"><?php echo $invRow['BATCH_NO']; ?></td>
													<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($invRow['EXPIRY_DATE'])); ?></td>
													<?php } ?>
													<td style="text-align:center;"><?php echo $invRow['STOCK_QTY'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['UNIT_PRICE'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['PURCHASE_DISCOUNT'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['SUB_AMOUNT'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['TAX_RATE'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['TAX_AMOUNT'] ?></td>
													<td style="text-align:center;"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
													<td class="text-center"> - - - </td>
	                      </tr>
	<?php
			}
		}
	?>
	                      <tr class="totals">
													<td class="text-center">Total</td>
													<?php if($medicalStoreAddon=='Y'){ ?>
													<td class="text-center" colspan="2">- - -</td>
													<?php } ?>
													<td class="qtyTotal text-center"></td>
													<td class="text-center">- - -</td>
													<td class="discountAmount text-center"></td>
													<td class="amountSub text-center"></td>
													<td class="text-center">- - -</td>
													<td class="amountTax text-center"></td>
													<td class="amountTotal text-center"></td>
													<td class="text-center">- - -</td>
													<td class="text-center">- - -</td>
	                      </tr>
	                  </tbody>
		              </table>
                </div>
                <div class="underTheTable">
              <div class="pull-right">
                      <div class="button savePurchase">Save</div>
                  </div>
              </div><!--underTheTable-->
              <div class="clear"></div>
              <div style="margin-top:10px"></div>
              <div class="title">Recent Purchase Return </div>
						<div class="recent_returns"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
    $(window).keyup(function(e){
	    if(e.altKey == true&&e.keyCode == 83){
        e.preventDefault();
        $("#form").click();
        savePurchase();
        return false;
	    }
    });
		$('select').selectpicker();
		$("input.bill_number").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$(document).calculateColumnTotals();

		$(window).load(function(){
			$("div.itemSelector button").focus();
			$("div.recent_returns").on('click','button.delete_purchase_return',function(){
				$(this).deletePanel("db/del-inventory-return.php");
			});
			$("#form").change(function(){
				get_recent_purchase_returns("div.recent_returns");
			});
			$("#datepicker").setFocusTo("div.supplierSelector button");
			$("div.supplierSelector button").keyup(function(e){
				var supp = $("select.supplierSelector option:selected").val();
				if(e.keyCode == 13 && supp != ''){
					$(".bill_number").focus();
				}
			});
			$("input.bill_number").setFocusTo("div.itemSelector button");
			$('div.itemSelector button').keyup(function(e){
				if(e.keyCode ==13 && $("select.itemSelector option:selected").val() != ''){
					$(this).getItemReturnDetails();
					$("input.quantity").focus();
				}
			});
			$('select.itemSelector').change(function(){
				if($("select.itemSelector option:selected").val() != ''){
					$(this).getItemReturnDetails();
					if($("select.batch_no").length>0){
						$("select.batch_no").populateBatchNumberList();
						$("div.batch_no button").focus();
					}else{
						$("input.quantity").focus();
					}
				}
			});
			$("div.batch_no button").keydown(function(e){
				if(e.keyCode==13){
					$("select.expiry_date").populateExpiryDateList();
					$("div.expiry_date button").focus();
				}
			});
			$("div.expiry_date button").keydown(function(e){
				if(e.keyCode==13){
					$("input.quantity").focus();
				}
			});
			$("select.expiry_date,select.batch_no").change(function(){
				$(this).getItemReturnDetails();
			});
			$("input.quantity").on('keyup blur',function(e){
				stockOs();
			});
			$("input.quantity").keydown(function(e){
				if(e.keyCode == 13){
					$("input.unitPrice").focus();
				}
			});
			$("input.unitPrice").keydown(function(e){
				if(e.keyCode == 13){
					$("input.discount").focus();
				}
			});
			$("input.discount").keydown(function(e){
				if(e.keyCode == 13){
					$(this).calculateRowTotal();
					$(".taxRate").focus();
				}
			});
			$("input.taxRate").keydown(function(e){
				if(e.keyCode == 13){
						$(this).calculateRowTotal();
						$(".addDetailRow").focus();
				}
			});
			$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
			$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");
			$(".addDetailRow").keydown(function(e){
				if(e.keyCode == 13){
					$(this).quickSave();
				}
			});
			$(".addDetailRow").click(function(){
				$(this).quickSave();
			});
			$(".savePurchase").click(function(){
				savePurchase();
			});
			get_recent_purchase_returns("div.recent_returns");
		});
  });
<?php if(isset($_GET['saved'])){ ?>displayMessage('Bill Saved Successfully!');<?php } ?>
<?php if(isset($_GET['updated'])){ ?>displayMessage('Bill Updated Successfully!');<?php } ?>
</script>
<?php include('conn.close.php'); ?>
