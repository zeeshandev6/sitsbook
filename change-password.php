<?php
	include("common/connection.php");
	include("common/config.php");

	$sessionUserDetails = $objAccounts->getDetails($user_id);

	$permissionz = $objAccounts->getPermissions($user_id);
	$permissionz = explode('|',$permissionz);

	if(isset($_POST['change'])){
		if($admin){
			$changePwOfUser = mysql_real_escape_string($_POST['user_acc_id']);
		}else{
			$changePwOfUser = $sessionUserDetails['ID'];
		}
		if(!$admin){
			$oldPassword = $_POST['oldPassword'];
			$oldMatched = $objAccounts->checkPassword($oldPassword,$changePwOfUser);
		}else{
			$oldMatched = true;
		}
		$password    = mysql_real_escape_string($_POST['password']);
		$rePassword    = mysql_real_escape_string($_POST['rePassword']);


		if($oldMatched){
			if($password != ''){
				if(strlen($password) >= 5){
					if($password == $rePassword){
						$passwordChanged = $objAccounts->changePassword($password,$changePwOfUser);
						if($passwordChanged){
							$message = "Password Changed Successfully!";
						}else{
							$message = "Password Not Changed!";
						}
					}else{
						$message = "New Passwords Do Not Match!";
					}
				}else{
					$message = "Password Must Contain at Least 5 Characters!";
				}
			}else{
				$message = "New Password is Required!";
			}
		}else{
			$message = "Existing Password Does Not Match!";
		}
	}
	$thisFile = basename($_SERVER['PHP_SELF']);
	if(isset($_GET['id'])){
		$user_id = mysql_real_escape_string($_GET['id']);
		$userDetails = $objAccounts->getDetails($user_id);
	}
	$userList = $objAccounts->getFullList();
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>Change Password</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	    <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
        <script type="text/javascript">
			$(document).ready(function(){
                $("select[name='user_acc_id']").selectpicker();
                $(".input_size").css({"width":"180px"});
                $(".the-form").validate({
					rules:{
						oldPassword: "required",
						password: "required",
						rePassword: { required:true,
									  equalTo:"input[name='password']",
									  minlength:5}
				    },messages:{
				    	oldPassword: {required:"Cannot be empty!"},
				    	password: {minlength:"At Least Five Charactors!",
				    				 required:"Required!"},
				    	rePassword: {minlength:"At Least Five Charactors!",
				    				 required:"Required!",
				    				 equalTo:"New Passwords do not Match!"}
				    }
				});
            });
        </script>
   </head>

   	<body>
   		<div id = "body-wrapper">
      		<div id="sidebar">
				<?php include("common/left_menu.php") ?>
            </div> <!-- End #sidebar -->

      		<div id = "bodyWrapper">
        		<div class = "content-box-top">
            		<div class = "summery_body">
               			<div class = "content-box-header">
                  			<p >Change Password</p>
                        	<div style = "clear:both;"></div>
               			</div><!-- End .content-box-header -->
                        <div style = "height:30px"></div>

                        <div id = "bodyTab1" style="display:block">
                        <form method="post" action="" class="the-form">
                            <div id = "form">
<?php
								if(!$admin){
?>
                                <div class = "caption" >User Name:</div>
                                <div class="field" >
                                	<input type="text" class="form-control" value="<?php echo (isset($sessionUserDetails))?$sessionUserDetails['USER_NAME']:""; ?>" readonly />
                                </div>
                                <div class="clear"></div>

<?php
								}
								if($admin){
?>
                                <div class = "caption">Select User:</div>
                                <div class="field" >
                                    <select name="user_acc_id" class="form-control show-tick">
<?php
									if(mysql_num_rows($userList)){
										while($userRow = mysql_fetch_array($userList)){
											$userSelected = (isset($user_id)&&$user_id==$userRow['ID'])?"selected":"";
?>
										<option value="<?php echo $userRow['ID']; ?>" <?php echo $userSelected; ?> ><?php echo $userRow['FIRST_NAME']." ".$userRow['LAST_NAME']; ?> - ( <?php echo $userRow['USER_NAME']; ?> )</option>
<?php
										}
									}
?>
                                    </select>
                                </div>
                                <div class="clear"></div>
<?php
								}

								if(!$admin){
?>
								<div class = "caption">Existing Password:</div>
                                <div class="field" style="width: 250px;" >
                                    <input type="password" name="oldPassword" class="form-control" value=""  />
                                </div>
                                <div class="clear"></div>
<?php
								}
?>

                                <div class = "caption"> New Password:</div>
                                <div class="field" style="width: 250px;" >
                                    <input type="password" name="password" minlength="5" class="form-control" value=""  />
                                </div>
                                <div class="clear"></div>

                                <div class = "caption">Confirm Password:</div>
                                <div class="field" style="width: 250px;" >
                                    <input type="password" name="rePassword" minlength="5" class="form-control" value="" />
                                </div>
                                <div class="clear"></div>

                                <div class = "caption"></div>
                                <div class="field" >
                                	<input type="hidden" name="user_id" value="<?php echo (isset($userDetails))?$userDetails['ID']:0; ?>" />
                                    <button type="submit" class="button" name="change"><i class="fa fa-check"></i> <?php echo (isset($userDetails))?"Change":"Change"; ?></button>
<?php
								if($admin){
?>
                                    <button type="button" class="button" onclick="window.location.href = 'users-management.php';"> <i class="fa fa-plus"></i> New</button>
<?php
								}
?>
                                </div><!--field-->
                                <div class="clear"></div>
                            </div><!--form-->
                            <div style = "clear:both;"></div>
                        </form>
                        </div><!--bodyTab1-->
            		</div><!--summery_body-->
	         	</div><!--End.content-box-top-->
      		</div><!--bodyWrapper-->
      	</div>  <!--body-wrapper-->
      	<div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
<?php
	if(isset($message)){
?>
		displayMessage('<?php echo $message; ?>');
<?php
	}
?>
    });
</script>
