<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/back-up.php');

	//Permission
	if(!in_array('backup',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objBackup = new Backup();

	if(!is_dir('backups')){
		mkdir('backups',0777);
	}

	if(isset($_GET['fresh'])){
		exit();
	}
	if(isset($_GET['create'])){
		$floating = $objBackup->getFloatingVoucherList();
		if($floating){
				$sql_file = $objBackup->makeBackUp();
				if($sql_file){
					readfile($sql_file);
				}else{
					$message = "Error Accured While Creating Backup!";
				}
		}else{
			$message = "Floating entries entries Exist therefore Can not create backup at this time.";
		}
	}
	if(isset($_POST['submit'])){
		if(!$_FILES['sqlFile']['error'] > 0){
			if(pathinfo($_FILES['sqlFile']['name'],PATHINFO_EXTENSION)=='sql'){
				$uploaded = $objBackup->restoreFile($_FILES['sqlFile']['tmp_name']);
				if($uploaded==0){
					$message = 'Database uploaded Successfully!';
				}else{
					$message = 'Database uploaded with Errors! '."\n".$objBackup->query;
				}
			}else{
				$message = 'Invalid File!';
			}
		}else{
			$message =  'File Not Selected!';
		}
	}
?>
<!DOCTYPE html 
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/listing.css" type="text/css" media = "screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<style>
			a.poplight{
				color:#666;
			}
			h3{
				font-size: 14px;
			}
		</style>
      	<script type = "text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type = "text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type = "text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type = "text/javascript" src="resource/scripts/tab.js"></script>
      	<script type = "text/javascript" src="resource/scripts/configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>

   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id="bodyWrapper">
          	<div class="content-box-top">
           		<div class="summery_body">
              	<div class = "content-box-header">
									<p >Database Back Up</p>
              	</div><!-- End .content-box-header -->
           			<div class="clear"></div>

								<div class="col-xs-12 mt-10">
									<div class="panel1 col-xs-12 col-sm-12 col-md-4 col-lg-3 createBackup">
										<img src="resource/images/download.png" class="panelIcon" />
										<h3>Create Backup</h3>
									</div>
									<div class="panel1 col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/upload.png" class="panelIcon" />
										<form method="post" action="back-up.php" enctype="multipart/form-data" class="uploadSql">
											<input type="file" name="sqlFile" class="fileParent" style="opacity:0.0;width: 100%;height: 100%;" />
											<input type="submit" class="button submitSql" name="submit" value="Upload" style="opacity:0.0;width:1px;height:0;position: absolute;top:0px;" />
										</form>
										<h3>Upload Backup File</h3>
									</div>
									<div class="clear"></div>
								</div>
           		</div><!--summery_body-->
          	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
        <div id="fade"></div>
        <div id="xfade"></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$(".createBackup").click(function(){ window.open('back-up.php?create','_blank');});
		$("input[name='sqlFile']").change(function(){
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Upload Database Backup?</p><a class='dodelete button'>Upload</a><a class='nodelete button'>Cancel</a></div>");
			centerThisDiv("#popUpDel");
			$("#popUpDel").hide();
			$("#xfade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".nodelete").click(function(){
				$("#xfade").fadeOut();
				$("#popUpDel").remove();
			});
			$(".dodelete").click(function(){
				$("#popUpDel").hide();
				$(".submitSql").click();
			});
		});
		//				 //
		//Message  Starts//
		//				 //
<?php
		if(isset($message)){
?>
		displayMessage('<?php echo $message; ?>');
<?php
		}
?>
		//				 //
		//Message  Ends//
		//				 //
    });

	var centerThisDiv = function(elementSelector){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		var elemHeight = $(elementSelector).height()/2;
		var elemWidth = $(elementSelector).width()/2;
		var posTop = win_hi-elemHeight;
		var posLeft = win_width-elemWidth;
		$(elementSelector).css({
			'position': 'fixed',
			'top': posTop,
			'left': posLeft,
			'margin': '0px'
		});
	}
	var displayMessage= function(message){
		$(".myAlert-bottom span").text(message);
		$(".myAlert-bottom").show();
		setTimeout(function () {
			$(".myAlert-bottom").hide();
		}, 2000);
	}
</script>
