<?php
	include("common/connection.php");
    include("common/classes/items.php");
    include("common/classes/company_details.php");

    $objItems = new Items;
    $objCompanyInfo = new CompanyDetails;

    $companyName = $objCompanyInfo->getTitle(1);
    $companyName = substr($companyName, 0,21);
	if(isset($_GET['id'])){
        $item_details = $objItems->getRecordDetails($_GET['id']);
        $number = isset($_GET['number'])?$_GET['number']:0;
        $skips  = isset($_GET['skip'])?$_GET['skip']:0;
        $item_details['NAME'] = substr($item_details['NAME'], 0,21);
?>
<!DOCTYPE html 
>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="resource/css/labels.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script>
    	$(document).ready(function(){
    		$(".startPrint").click(function(){
    			window.print();
    		});
    	});
    </script>
</head>
<body style="background-image:none !important;background-color:#FFF !important;">
<?php
    if(!isset($_GET['submit'])){
    ?>
    <div class="col-md-8">
        <div class="panel panel-default" style="padding:1em;">
            <form>
                <label><?php echo $item_details['NAME']; ?> &nbsp;&nbsp;&nbsp;&nbsp;  <?php echo $item_details['ITEM_BARCODE']; ?> <img src="lib/barcode.php?size=50&text=<?php echo $item_details['ITEM_BARCODE']; ?>" />  </label>
                <div class="clearfix"></div>
                <input type="hidden"  name="id" value="<?php echo $_GET['id'] ?>" class="form-control" />
                <div class="col-md-8">
                    <div class="caption">Number of barcodes</div>
                    <input type="text"  name="number" value="32" class="form-control" style="width:150px;"  />
                </div>
                <div class="clearfix"></div>
                <div class="col-md-8">
                    <div class="caption"  style="width:100px;">Skip</div>
                    <input type="text"  name="skip" value="0" class="form-control" style="width:150px;" />
                </div>
                <div class="clearfix"></div>
                <div style="height:20px;"></div>
                <div class="col-md-8">
                    <input type="submit" name="submit" value="Generate" class="button" />
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
<?php 
    }else{
    ?>
    <button class="button startPrint dont-print"><i class="fa fa-print"></i> Print </button>
	<div class="barcode-print">
        <?php
            for ($i=1; $i <= $skips; $i++){
            ?>
                    <div id="lable"></div>
        <?php
            }
            ?>
		<?php
            for ($i=1; $i <= $number; $i++){
            ?>
                <div id="lable">
                    <div class="line"><?php echo $companyName; ?></div>
                    <div class="line"><?php echo $item_details['NAME']; ?> </div>
                        <div class="left">
                            <div class="rotate">
                                Rs.<?php echo (int)$item_details['SALE_PRICE']; ?>
                            </div>
                        </div>
                    <div class="right">
                     <div class="barcode">
                         <img src="lib/barcode.php?size=30&text=<?php echo $item_details['ITEM_BARCODE']; ?>" />
                        </div>
                        <div class="barcodeNo pull-left" style="font-size:10px;margin-top:4px;">Rs.<?php echo $item_details['SALE_PRICE']; ?>/=</div>
                        <div class="barcodeNo pull-right"><?php echo $item_details['ITEM_BARCODE']; ?></div>
                    </div>
                    <div class="clear"></div>
                </div><!--lable-->
        <?php
            }
            ?>
        <div class="clear"></div>
	</div><!--barcode-print-->
    <?php
        }
        ?>
        <script type="'text/javascript'">
            $(document).ready(function(){
                $(".startPrint").click(function(){
                    window.print();
                });
            });
        </script>
</body>
</html>
<?php include('conn.close.php'); ?>
<?php
	}
?>