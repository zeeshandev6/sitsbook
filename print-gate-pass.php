<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/gate_pass.php');
	include('common/classes/gate_pass_details.php');
	include('common/classes/company_details.php');

	$objCompanyDetails 	   = new CompanyDetails();
	$objGatePass           = new GatePass();
	$objGatePassDetails    = new GatePassDetails();
	$objConfigs   	       = new Configs();

	//INVOICE_FORMAT Config Size - Header - Duplicate
	$use_cartons           = $objConfigs->get_config('USE_CARTONS');
	$use_cartons           = ($use_cartons == 'Y')?true:false;
	$invoice_format        = $objConfigs->get_config('INVOICE_FORMAT');
	$show_header 					 = $objConfigs->get_config('SHOW_INVOICE_HEADER');
	$invoice_format        = explode('_', $invoice_format);
	$invoice_num 		   		 = $invoice_format[1];
	$invoiceStyleCss       = 'resource/css/invoiceStyle.css';

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;

	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
	$use_taxes           = ($use_taxes=='Y')?true:false;

	$cutomer_balance_array 						= array();
	$cutomer_balance_array['BALANCE'] = 0;
	$cutomer_balance_array['TYPE']    = '';

	$merge_item_by_category = false;
	if($fabricProcessAddon=='Y'&&!isset($_GET['office'])){
		$merge_item_by_category = true;
	}

	if(isset($_GET['id'])){
		$sale_id 		       = mysql_real_escape_string($_GET['id']);
		$saleDetails 	     = $objGatePass->getDetail($sale_id);
		$particular_list   = $objGatePassDetails->getList($sale_id);
		$companyLogo   		 = $objCompanyDetails->getLogo();
		$company  		 		 = $objCompanyDetails->getActiveProfile();
	}
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>

	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "<?php echo $invoiceStyleCss; ?>",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$(".ledger_view").click(function(){
			var gl_date = $(this).attr('data-date');
			var gl_code = $(this).attr('data-code');
			if(gl_code != ''){
				$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
					if(data == ''){
						var win = window.open('invoice-ledger-print.php','_blank');
						if(win){
							win.focus();
						}else{
							displayMessage('Please allow popups for This Site');
						}
					}
				});
			}
		});
		//$(".printThis").click();
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		$entry_date     = date('d-M-Y',strtotime($saleDetails['ENTRY_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($entry_date));
?>
		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
					<button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print View </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="clear"></div>
			<div class="printThisDiv" style="position: relative;height: auto;">
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint" style="height:auto !important;min-height: 4in !important;">
						<div class="invoiceHead" style="width: 100%;margin: 0px auto;">
							<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
							<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
								<div style="font-size:26px;line-height: 40px;"> <?php echo $company['NAME']; ?> </div>
								<?php echo $company['ADDRESS']; ?>
								<br />
								Contact : <?php echo $company['CONTACT']; ?>
								<br />
								Email : <?php echo $company['EMAIL']; ?>
								<br />
							</div>
							<div class="clear"></div>

							<div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
								GATE PASS
							</div>
							<div class="clear" style="height: 5px;"></div>

							<div class="infoPanelWide pull-left" style="min-height:auto !important;">
								<span class="pull-left" style="padding:0px 0px;font-size:14px;padding: 10px;"> <b>Lot Number &nbsp;&nbsp; </b> <span class="pull-right"><?php echo $saleDetails['LOT_NO']; ?></span></span>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;padding: 10px;"> <b>Gate Pass Number &nbsp;&nbsp; </b> <span class="pull-right"><?php echo $saleDetails['GP_NO']; ?></span></span>
								<span class="pull-left" style="padding:0px 0px;font-size:14px;padding: 10px;"> <b>Date : &nbsp;&nbsp;&nbsp; </b> <span class="pull-right"><?php echo $entry_date; ?></span></span>
								<div class="clear"></div>

								<span class="pull-left" style="padding:0px 0px;font-size:14px;padding: 10px;"> <b>To Address :</b> <?php echo $saleDetails['TO_ADDRESS']; ?> </span>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
							</div><!--invoiceHead-->
							<div class="clear"></div>
							<div class="invoiceBody" style="width: 100%;margin: 0px auto;">
								<table>
									<thead>
													<tr>
														<th width="80%">Design</th>
														<th width="20%">Quantity</th>
													</tr>
												</thead>
												<tbody>
<?php
													$quantity      = 0;
													$counter       = 1;
													if(mysql_num_rows($particular_list)){
														while($row = mysql_fetch_array($particular_list)){
?>
																		<tr>
																			<td class="text-left"><?php echo $row['PARTICULARS']; ?></td>
																			<td class="text-center"><?php echo $row['QUANTITY']; ?></td>
																		</tr>
<?php
																		$quantity    += $row['QUANTITY'];
																		$counter++;
														}
													}
?>
															</tbody>
															<tfoot>
																<tr>
																	<th class="text-right">Total</th>
																	<th class="text-center"><?php echo $quantity; ?></th>
																</tr>
															</tfoot>
														</table>
														<div class="clear" style="height: 150px;"></div>

															<div class="invoice_footer <?php echo $show_header=='N'?"opacity_zero":""; ?>" style="position:absolute !important;bottom:5px !important;">
																<div class="auths pull-left">
																	Prepared By
																	<span class="authorized"></span>
																</div><!--partyTitle-->
																<div class="auths pull-right">
																	Authorized By
																</div><!--partyTitle-->
																<div class="clear" style="height: 10px;"></div>
																<div class="invoice-developer-info text-right"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
															</div>
														</div><!--invoiceBody-->
													</div><!--invoiceLeftPrint-->
													<div class="clear"></div>
												</div><!--invoiceContainer-->
											</div><!--printThisDiv-->
										</div><!--invoiceBody-->
										<?php
									}
									?>
								</body>
								</html>
								<?php include('conn.close.php'); ?>
