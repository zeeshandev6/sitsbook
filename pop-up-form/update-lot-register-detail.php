<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_lot_register.php');
	include('../common/classes/emb_inward.php');
	include('../common/classes/measure.php');
	include('../common/classes/emb_products.php');
	include('../common/classes/machines.php');
	include('../common/classes/customers.php');
	include('../common/classes/emb_stitch_account.php');
	include('../common/classes/payrolls.php');

	$objLotRegisterDetails = new EmbLotRegister();
	$objInward  					 = new EmbroideryInward();
	$objMeasures 					 = new Measures();
	$objProducts 					 = new EmbProducts();
	$objMachines 					 = new Machines();
	$objCustomers 				 = new customers();
	$objPayrolls 				   = new Payrolls();

	$oDetail_id 		= mysql_real_escape_string($_POST['oDetail_id']);
	$row 						= $objLotRegisterDetails->getDetail($oDetail_id);

	$measureList 				= $objMeasures->getList();
	$productList 				= $objProducts->getList();
	$machineList 				= $objMachines->getList();
	$customerList 			= $objCustomers->getList();
	$stitchMen   				= $objPayrolls->getList();

	$qualityList 				= $objLotRegisterDetails->getLotQualityList($row['CUST_ACC_CODE'],$row['LOT_NO']);

	$totalLotsIssued 						= $objLotRegisterDetails->getLotQuantityIssued($row['CUST_ACC_CODE'],$row['LOT_NO']);
	$inwardProductTotallength 	= $objInward->getInwardLotStockInHandWhloleLotByDate($row['CUST_ACC_CODE'],$row['LOT_NO'],$row['LOT_DATE']);

	$lengthOs = ($inwardProductTotallength - $totalLotsIssued);

?>
		<div class="popup_title">
			<p class="pull-left" style="margin-top: 10px;">Edit Lot Details</p>
			<button class="btn btn-default btn-sm pull-right closePopUpForm mt-10 text-danger"> <i class="fa fa-times"></i> </button>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
        <div id="form">
                <input type="hidden" value="<?php echo $row['ID'] ?>" name="lot_detail_id"/>
                <div class="caption" style="width:120px;">Lot Date:</div>
                <div class="field" style="width:305px;">
                	<input type="text" name="lot_date" class="form-control datepicker" style="width:145px;" value="<?php echo date('d-m-Y',strtotime($row['LOT_DATE'])); ?>" />
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Customer:</div>
                <div class="field" style="width:305px;">
                    <select name="cutomerCode" class="cutomerCode show-tick form-control" >
<?php
						if(mysql_num_rows($customerList)){
							while($customer = mysql_fetch_array($customerList)){
								$selected = ($row['CUST_ACC_CODE']==$customer['CUST_ACC_CODE'])?"selected='selected'":"";
?>
								<option value="<?php  echo $customer['CUST_ACC_CODE']; ?>"
                                		data-subtext='<?php  echo $customer['CUST_ACC_CODE']; ?>'
                                		<?php echo $selected; ?>
                                	    data-subtext='<?php  echo $customer['CUST_ACC_CODE']; ?>' ><?php  echo $customer['CUST_ACC_TITLE']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Product:</div>
                <div class="field" style="width:305px;">
                    <select name="productCode" class="productCode show-tick form-control" >
<?php
						if(mysql_num_rows($productList)){
							while($product = mysql_fetch_array($productList)){
								$selected = ($row['PRODUCT_ID']==$product['ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $product['ID']; ?>" <?php echo $selected; ?> ><?php  echo $product['TITLE']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Lot No:</div>
                <div class="field" style="width:80px;">
                    <input type="text" name="lotNum"  style="width:80px;" class="form-control lotNumber" value="<?php echo $row['LOT_NO'] ?>"  />
                </div>
								<div class="caption" style="width:90px;margin: 10px 5px 0px 20px;"></div>
                <div class="field" style="width:120px;padding-left:10px;padding-top:8px;font-weight:normal;">
									<input type="checkbox" id="woo" value="WOOO" class="claimCheck css-checkbox" <?php echo ($row['LOT_STATUS']=='R')?"checked":""; ?> />
									<label for="woo" class="css-label">Claim</label>
								</div>
								<div class="clear"></div>

                <div class="caption" style="width:120px;">Quality :</div>
                <div class="field" style="width:100px;font-weight:normal;">
                	<select name="quality" class="quality form-control show-tick" >
<?php
						if(mysql_num_rows($qualityList)){
							while($quality = mysql_fetch_array($qualityList)){
								$selectedQuality = (trim($row['QUALITY']) == trim($quality['QUALITY']))?"selected":"";
?>
								<option value="<?php echo $quality['QUALITY']; ?>" <?php echo $selectedQuality; ?> ><?php echo $quality['QUALITY']; ?></option>
<?php
							}
						}
?>
                    </select>
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Measure :</div>
                <div class="field" style="width:120px;">
                	<select name="measure" class="measure form-control show-tick">
<?php
						if(mysql_num_rows($measureList)){
							while($measure = mysql_fetch_array($measureList)){
								$selectMeasure = ($measure['ID']==$row['MEASURE_ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $measure['ID']; ?>" <?php echo $selectMeasure; ?> ><?php  echo $measure['NAME']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>

								<div class="caption" style="width:120px;">Billing Type:</div>
                <div class="field" style="width:100px;">
										<select name="billing_type2" class="billing_type2 form-control show-tick"  >
											<option value="S" <?php echo ($row['BILLING_TYPE'] == 'S')?"selected":""; ?> >Stitches</option>
											<option value="Y" <?php echo ($row['BILLING_TYPE'] == 'Y')?"selected":""; ?> >Yards</option>
											<option value="U" <?php echo ($row['BILLING_TYPE'] == 'U')?"selected":""; ?> >Suits</option>
											<option value="L" <?php echo ($row['BILLING_TYPE'] == 'L')?"selected":""; ?> >Laces</option>
										</select>
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Stitch/Yrd:</div>
                <div class="field" style="width:120px;">
                	<input type="text" name="stitches2" class="form-control stitches2"  value="<?php echo $row['STITCHES']; ?>" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Thaan:</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="length" style="width:100px;"  class="form-control"  value="<?php echo $row['MEASURE_LENGTH'] ?>" />
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Thaan Os:</div>
                <div class="field" style="width:120px;">
                	<input type="text" name="length_os" class="form-control length_os" data-lot-os='<?php echo $lengthOs+$row['MEASURE_LENGTH']; ?>' value="<?php echo $lengthOs; ?>" readonly="readonly" />
                </div>
                <div class="clear"></div>

								<div class="caption" style="width:120px;">Production :</div>
                <div class="field" style="width:100px;">
                	<input type="text" name="total_laces" class="form-control"  value="<?php echo $row['TOTAL_LACES']; ?>" />
                </div>
								<div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Waste:</div>
                <div class="field" style="width:120px;">
                	<input type="text" name="gpNum" class="form-control"  value="<?php echo $row['GP_NO'] ?>" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Emb @:</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="embRate" class="form-control"  style="width:80px;" value="<?php echo $row['EMB_RATE'] ?>" />
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Amount:</div>
                <div class="field" style="width:120px;">
                <input type="text" name="embAmount" class="form-control" value="<?php echo $row['EMB_AMOUNT'] ?>" readonly="readonly" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Stitch @:</div>
                <div class="field" style="width:100px;">
                    <input type="text" style="width:80px;" name="stitchRate"  class="form-control"  value="<?php echo $row['STITCH_RATE'] ?>" />
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Amount:</div>
                <div class="field" style="width:120px;">
                <input type="text" name="stitchAmount" class="form-control"  value="<?php echo $row['STITCH_AMOUNT'] ?>"  readonly="readonly" />
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Commander :</div>
                <div class="field" style="width:315px;">
                	<select name="stitchAccount" class="stitchAccount form-control show-tick">
                    	<option value="0"></option>
<?php
						if(mysql_num_rows($stitchMen)){
							while($stitch = mysql_fetch_array($stitchMen)){
								$stitchSelected = ($stitch['CUST_ACC_CODE']==$row['STITCH_ACC'])?"selected='selected'":"";
?>
								<option value="<?php  echo $stitch['CUST_ACC_CODE']; ?>" <?php echo $stitchSelected; ?>><?php  echo $stitch['CUST_ACC_TITLE']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Design Number:</div>
                <div class="field" style="width:315px;">
                    <input type="text" name="designNum" class="form-control"  value="<?php echo $row['DESIGN_CODE'] ?>" />
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Machine Number:</div>
                <div class="field" style="width:315px;">
                	<select name="machineNum" class="machineNum form-control show-tick">
<?php
						if(mysql_num_rows($machineList)){
							while($machine = mysql_fetch_array($machineList)){
								$machineSelected = ($machine['ID']==$row['MACHINE_ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $machine['ID']; ?>" <?php echo $machineSelected; ?>> <?php  echo $machine['MACHINE_NO']; ?> - <?php  echo $machine['NAME']; ?></option>
<?php
							}
						}
?>
									</select>
                </div>
                <div class="clear"></div>

                <div class="col-xs-12 mb-20 mt-20">
									<input type="submit" value="Update" name="update" class="btn btn-primary pull-right ml-10"/>
									<input type="button" value="Close"  class="btn  btn-default pull-right closePopUpForm" />
	                <div class="clear"></div>
                </div>
								<div class="clear"></div>
		</div> <!-- End form -->
