		<div class="popup_title">Modify Voucher Details</div>
        <div class="clear"></div>
        <div id="form">
        		<input type='hidden' name='jv_detail_id' />
                <div class="caption">Accounts:</div>
                <div class="field" style="width:250px;">
                    <select class="accCodePopup show-tick form-control" data-style="btn-default" data-live-search="true" name="accCode" style="border:none"></select>
                </div>
                <div class="clear"></div>
                
                <div class="caption">Narration:</div>
                <div class="field" style="width:250px;">
                    <input type="text" name="narration" class="input_size" style="width: 300px;"  value="" />
                </div>
                <div class="clear"></div>
                
                <div class="caption">Narration:</div>
                <div class="field" style="width:250px;">
                    <input type="radio" name="type" value="Dr" /> Debit 
                    <input type="radio" name="type" value="Cr" /> Credit 
                </div>
                <div class="clear"></div>
                <div class="caption">Amount Rs.</div>
                <div class="field" style="width:200px;">
                     <input type="text" name="amount" class="input_size" style="width: 140px;"  value="" />
                </div>
                <div class="clear"></div>
                

                <div class="caption"></div>
                <div class="field" style="width:250px;">
                    <input type="submit" value="Update" name="updateDetail" class="button" onClick="jvFunctions.updateJvDetails();" />
                    <input type="button" value="Cancel"  class="button" onClick="jvFunctions.closePopUp();"/>
                </div>
                <div class="clear"></div>
		</div> <!-- End form -->       