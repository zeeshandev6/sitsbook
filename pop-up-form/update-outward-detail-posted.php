<?PHP
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/emb_inward.php');
	include('../common/classes/measure.php');
	include('../common/classes/emb_products.php');
	include('../common/classes/machines.php');
	include '../common/classes/emb_stitch_account.php';
	include('../common/classes/payrolls.php');

	$objOutward 	= new outward();
	$objInward  	= new EmbroideryInward();
	$objMeasure 	= new Measures();
	$objProducts 	= new EmbProducts();
	$objMachines 	= new Machines();
	$objPayrolls 	= new Payrolls();

	$oDetail_id 		= (int)mysql_real_escape_string($_POST['oDetail_id']);
	$outwardDetails = $objOutward->getSpecificOutwardDetail($oDetail_id);
	$measureList 		= $objMeasure->getList();
	$productList 		= $objProducts->getList();
	$machineList 		= $objMachines->getList();
	$stitchMen   		= $objPayrolls->getList();

	$row = mysql_fetch_array($outwardDetails);

	$forQuality 		  		= mysql_fetch_array($objOutward->getSpecificCustomerOutward($row['OUTWD_ID']));
	$lotForQuality 		  	= mysql_fetch_array($objOutward->getSpecificOutwardDetail($row['OUTWD_DETL_ID']));
	$forQuality['LOT_NO'] = $lotForQuality['LOT_NO'];
	$qualityList   		  	= $objOutward->getLotQualityList($forQuality['CUST_ACC_CODE'],$forQuality['LOT_NO']);
?>
		<div class="popup_title">
			<p class="pull-left" style="margin-top: 10px;">Modify Outward Details</p>
			<button class="btn btn-default btn-sm pull-right closePopUpForm mt-10 text-danger"> <i class="fa fa-times"></i> </button>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
        <div id="form">
            <form method="post" action="">
                <input type="hidden" value="<?PHP echo $row['OUTWD_ID'] ?>" name="outwardId"/>
                <input type="hidden" value="<?PHP echo $row['OUTWD_DETL_ID'] ?>" name="outwardDetailId"/>
                <div class="caption" style="width:120px;">Code/Product:</div>
                <div class="field" style="width:305px;">
                    <select name="productCode" class="productCode show-tick form-control" >
<?php
						if(mysql_num_rows($productList)){
							while($product = mysql_fetch_array($productList)){
								$selected = ($row['PRODUCT_ID']==$product['ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $product['ID']; ?>"
                                		data-subtext='<?php  echo $product['PROD_CODE']; ?>'
                                		<?php echo $selected; ?>
                                	    data-subtext='<?php  echo $product['PROD_CODE']; ?>' ><?php  echo $product['TITLE']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>
<?php
				$outwardDeliveredTilNow   = (float)$objOutward->getOutwardLotQtyDelivered($forQuality['CUST_ACC_CODE'],$forQuality['LOT_NO'],$row['PRODUCT_ID'],$forQuality['OUTWD_DATE']);
				$inwardProductTotallength = (float)$objInward->getInwardLotStockInHand($forQuality['CUST_ACC_CODE'],$forQuality['LOT_NO'],$row['PRODUCT_ID']);

				$inwardStock = $inwardProductTotallength - $outwardDeliveredTilNow;
?>
                <div class="caption" style="width:120px;">Lot No:</div>
                <div class="field" style="width:305px;">
                    <input type="text" name="lotNum"  style="width:80px;" class="form-control lotNumber"  value="<?PHP echo $row['LOT_NO'] ?>" readonly="readonly" />
                </div>
								<div class="clear"></div>

                <div class="caption" style="width:120px;">Quality :</div>
                <div class="field" style="width:100px;font-weight:normal;">
                	<select name="quality" class="quality form-control show-tick" >
<?php
						if(mysql_num_rows($qualityList)){
							while($quality = mysql_fetch_array($qualityList)){
								$selectedQuality = (trim($row['QUALITY']) == trim($quality['QUALITY']))?"selected":"";
?>
								<option value="<?php echo $quality['QUALITY']; ?>" <?php echo $selectedQuality; ?> ><?php echo $quality['QUALITY']; ?></option>
<?php
							}
						}
?>
                    </select>
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Measure :</div>
                <div class="field" style="width:120px;">
                	<select name="measure" class="measure form-control show-tick">
<?php
						if(mysql_num_rows($measureList)){
							while($measure = mysql_fetch_array($measureList)){
								$selectMeasure = ($measure['ID']==$row['MEASURE_ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $measure['ID']; ?>" <?php echo $selectMeasure; ?> ><?php  echo $measure['NAME']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="clear"></div>

								<div class="caption" style="width:120px;">Billing Type:</div>
                <div class="field" style="width:100px;">
										<select name="billing_type2" class="billing_type2 form-control show-tick"  >
											<option value="S" <?php echo ($row['BILLING_TYPE'] == 'S')?"selected":""; ?> >Stitches</option>
											<option value="Y" <?php echo ($row['BILLING_TYPE'] == 'Y')?"selected":""; ?> >Yards</option>
											<option value="U" <?php echo ($row['BILLING_TYPE'] == 'U')?"selected":""; ?> >Suits</option>
											<option value="L" <?php echo ($row['BILLING_TYPE'] == 'L')?"selected":""; ?> >Laces</option>
										</select>
                </div>
                <div class="caption" style="width:90px;margin: 10px 5px 0px 0px;">Stitches:</div>
                <div class="field" style="width:120px;">
                	<input type="text" name="stitches2" class="form-control stitches2"  value="<?php echo $row['STITCHES']; ?>" />
                </div>
                <div class="clear"></div>

								<div class="caption" style="width:120px;">Production :</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="total_laces" style="width:80px;" class="form-control"  value="<?PHP echo $row['TOTAL_LACES'] ?>" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Thaan :</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="length" style="width:80px;" class="form-control"  value="<?PHP echo $row['MEASURE_LENGTH'] ?>" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Emb @:</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="embRate" class="form-control" style="width:80px;" value="<?PHP echo $row['EMB_RATE'] ?>" />
                </div>
                <div class="caption" style="width:85px;margin: 10px 5px 0px 0px;">Amount:</div>
                <div class="field" style="width:100px;">
                <input type="text" name="embAmount" class="form-control" value="<?PHP echo $row['EMB_AMOUNT'] ?>" readonly="readonly" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="width:120px;">Stitch @:</div>
                <div class="field" style="width:100px;">
                    <input type="text" style="width:80px;" name="stitchRate" class="form-control"  value="<?PHP echo $row['STITCH_RATE'] ?>" />
                </div>
                <div class="caption" style="width:85px;margin: 10px 5px 0px 0px;">Amount:</div>
                <div class="field" style="width:100px;">
                <input type="text" name="stitchAmount" class="form-control"  value="<?PHP echo $row['STITCH_AMOUNT'] ?>"  readonly="readonly" />
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Commander :</div>
                <div class="field" style="width:100px;">
                	<select name="stitchAccount" class="stitchAccount form-control show-tick">
                    	<option value="0"></option>
<?php
						if(mysql_num_rows($stitchMen)){
							while($stitch = mysql_fetch_array($stitchMen)){
								$stitchSelected = ($stitch['CUST_ACC_CODE']==$row['STITCH_ACC'])?"selected='selected'":"";
?>
								<option value="<?php  echo $stitch['CUST_ACC_CODE']; ?>" <?php echo $stitchSelected; ?>><?php  echo $stitch['CUST_ACC_TITLE']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="caption" style="width:85px;margin: 10px 5px 0px 0px;">Design #:</div>
                <div class="field" style="width:100px;">
                    <input type="text" name="designNum" class="form-control" style="width:100px;"  value="<?PHP echo $row['DESIGN_CODE'] ?>" />
                </div>
                <div class='clear'></div>
                <div class="caption" style="width:120px;">Machine:</div>
                <div class="field" style="width:100px;">
                	<select name="machineNum" class="machineNum form-control show-tick">
<?php
						if(mysql_num_rows($machineList)){
							while($machine = mysql_fetch_array($machineList)){
								$machineSelected = ($machine['ID']==$row['MACHINE_ID'])?"selected='selected'":"";
?>
								<option value="<?php  echo $machine['ID']; ?>" <?php echo $machineSelected; ?> ><?php  echo $machine['MACHINE_NO']; ?> - <?php  echo $machine['NAME']; ?></option>
<?php
							}
						}
?>
					</select>
                </div>
                <div class="caption" style="width:85px;margin: 10px 5px 0px 0px;">GatePass#:</div>
                <div class="field" style="width:100px;">
                	<input type="text" name="gpNum" class="form-control" style="width: 100px;"  value="<?PHP echo $row['GP_NO'] ?>" />
                </div>
                <div class="clear"></div>
                <div class="caption" style="width:120px;">Remarks:</div>
                <div class="field" style="width:288px;">
                	<textarea name="remarks" class="form-control" style="min-height:80px;" placeholder="e.g Lot# 1 Emb @ Diff Rs. 5 , Diff Amount : 420"><?PHP echo $row['REMARKS'] ?></textarea>
                </div>

								<div class="col-xs-12 mb-20 mt-20">
									<input type="submit" value="Update" name="updateOutwardDetails" class="btn btn-primary pull-right ml-10"/>
									<input type="button" value="Close"  class="btn  btn-default pull-right closePopUpForm" />
	                <div class="clear"></div>
                </div>
								<div class="clear"></div>
                <div class="clear"></div>
            </form>
		</div> <!-- End form -->
