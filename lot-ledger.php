<?php
	ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/suppliers.php');
	include('common/classes/customers.php');
	include('common/classes/lot_details.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/fabric_contract_details.php');
	include('common/classes/ledger_report.php');
	//Permission
	if(!in_array('fabric-contract',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--
	$tab 		 = "form";
	$id  		 = 0;
	$lot_details = NULL;
	if(isset($_GET['tab'])){
		$tab = $_GET['tab'];
	}
	$objAccount         			 		 = new UserAccounts();
	$objCustomers       			 		 = new Customers();
	$objSupplier        			 		 = new Suppliers();
	$objAccounts        			 		 = new UserAccounts();
	$objLotDetails					 	 		 = new LotDetails();
	$objFabricContract  			 		 = new FabricContracts();
	$objFabricContractDetails   	 = new FabricContractDetails();
	$objLedgerReport   				 		 = new ledgerReport();
	if(isset($_GET['lot_no'])){
		$lot_no = (int)mysql_real_escape_string($_GET['lot_no']);
	}
	$objLedgerReport->po_number 			= $lot_no;
	$objLedgerReport->transactionType = "Dr";
	$ledger_list 											= $objLedgerReport->getVoucherDetailListByPO();

	$stage_names 	  	= array();
	$stage_names['F'] = 'Fabric Contract';
	$stage_names['P'] = 'Processing Contract';
	$stage_names['E'] = 'Embroidery Contract';
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<!-- jQuery -->
	<style>
		td{
			padding: 10px !important ;
		}
		.content-box-header p{
			padding: 1px 1px !important;
			margin-bottom: 0px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select").selectpicker();
			$("input.datepicker_style").datepicker({
				dateFormat: 'dd-mm-yy',
				showAnim : 'show',
				changeMonth: true,
				changeYear: true,
				yearRange: '2000:+10'
			});
			$("select.lot_status_at_list").change(function(){
				var id  = $(this).attr('data-id');
				var val = $(this).find("option:selected").val();
				$.post('lot-details.php',{lot_status_change:id,status:val},function(){});
			});
			$(".delete_contract").click(function(){
				var id = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.post("<?php echo basename($_SERVER['PHP_SELF']); ?>", {delete_contract : id}, function(data){
							data = $.parseJSON(data);
							$("#popUpDel").children(".confirm").text(data['MSG']);
							$("#popUpDel").children(".dodelete").hide();
							$("#popUpDel").children(".nodelete").text("Close");
							if(data['OKAY'] == 'Y'){
								clickedDel.parent('td').parent('tr').remove();
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
				});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#fade").fadeOut('fast');
				});
			});
		});
	</script>
</head>
<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div id="bodyWrapper">
		<div class = "content-box-top" style="overflow:visible;">
			<div class = "summery_body">
				<div class="content-box-header">
					<p>Lot Ledger Details</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="clear"></div>

				<?php include('lot.tabs.inc.php'); ?>
				<div class="clear"></div>

				<?php
					if($tab == 'form'){
				?>

				<div id = "bodyTab1">
					<table cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th width="5%" style="text-align:center">JV#</th>
								<th width="10%" style="text-align:center">Date</th>
								<th width="10%" style="text-align:center">ACC/Code</th>
								<th width="10%" style="text-align:center">ACC/Title</th>
								<th width="25%" style="text-align:center">Description</th>
								<th width="10%" style="text-align:center">Cost Pool</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$total_debit  = 0;
								$total_credit = 0;
								if(mysql_num_rows($ledger_list)){
									while($row = mysql_fetch_assoc($ledger_list)){
										?>
										<tr id="recordPanel">
											<td style="text-align:center"><?php echo $row['VOUCHER_NO'] ?></td>
											<td style="text-align:center"><?php echo date("d-m-Y",strtotime($row['VOUCHER_DATE'])) ?></td>
											<td style="text-align:center"><?php echo $row['ACCOUNT_CODE'] ?></td>
											<td style="text-align:center"><?php echo $row['ACCOUNT_TITLE'] ?></td>
											<td style="text-align:left"><?php 	echo $row['NARRATION'] ?></td>
											<td style="text-align:center"><?php echo $row['TRANSACTION_TYPE']=='Dr'?$row['AMOUNT']:""; ?></td>
										</tr>
										<?php
										$total_debit  += $row['TRANSACTION_TYPE']=='Dr'?$row['AMOUNT']:0;
										$total_credit += $row['TRANSACTION_TYPE']=='Cr'?$row['AMOUNT']:0;
									}
								}
							?>
							</tbody>
							<tfoot>
								<tr>
									<th class="text-right" colspan="5"> <b>Total :</b> </th>
									<th class="text-center"><b><?php echo number_format($total_debit,2); ?></b></th>
								</tr>
							</tfoot>
						</table>
					</div> <!--bodyTab1-->
<?php
				}
?>
					<div class="clear mt-20"></div>
				</div>     <!-- End summer -->
			</div>   <!-- End .content-box-top -->
		</div>
		<div id="xfade"></div>
		<div id="fade"></div>
	</body>
	</html>
	<?php ob_end_flush(); ?>
	<?php include("conn.close.php"); ?>
