<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/ledger_report.php';
	include 'common/classes/accounts.php';
	include 'common/classes/dashboard.php';
	
	//Permission
	if(!in_array('general-ledger',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts     = new ChartOfAccounts;
	$objLedgerReport = new ledgerReport;
	$objConfigs      = new Configs;
	$objDashboard    = new Dashboard;

	$accountsList = $objLedgerReport->getAccountsList();
	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
    $invoice_format = explode('_', $invoice_format);
	$invoiceSize = $invoice_format[0]; // S  L

	if(isset($_GET['expense'])){
		$month = $_GET['month'];
		$year  = $_GET['year'];
		$month_start_date = date($year.'-'.$month.'-01');
		$month_end_date   = date('Y-m-t',strtotime($year.'-'.$month.'-01'));

		$expenses    = $objAccounts->getLevelThreeListByGeneral('03');

		$exp_array = array();
		$exp_array_values = array();
		$level_four = array();
		if(mysql_num_rows($expenses)){
			while($revenue = mysql_fetch_array($expenses)){
				$account_code = $revenue['ACC_CODE'];
				$accCodeList = $objAccounts->getAccountByCatAccCode($account_code);
				if(mysql_num_rows($accCodeList)){
					while($revRow = mysql_fetch_array($accCodeList)){
						$exp_array[] = $revRow['ACC_CODE'];
						$exp_array_values[$revRow['ACC_CODE']] = 0;
					}
				}
			}
		}
		foreach($exp_array as $key=>$exp_acc){
			$expense = $objDashboard->getAccountDebitsSum($exp_acc, $month_start_date, $month_end_date, '>=', '<=');
			$exp_array_values[$exp_acc] += $expense;
		}
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="resource/css/bootstrap-select.css" rel="stylesheet">
        <style>
			html{
			}
			.ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized{
				float:left;
				width: 152px;
				padding-left: 5px;
				border: 1px solid #CCC;
				height:30px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;
				border:1px solid #DDDDDD;
				
				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;
				
				-moz-border-radius-topleft:3px; 
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;
				
				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;
				
				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			}
			.input_sized:hover{
				border-color: #9ecaed;
				box-shadow: 0 0 2px #9ecaed;
			}
		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/highcharts.js"></script>
    	<script type="text/javascript" src="resource/scripts/highcharttables.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
			$(window).on('load', function () {
				$('.selectpicker').selectpicker();
				$(".printThis").click(function(){
					var pageHeaderHeight = $(".pageHeader").height();
					var MaxHeight = 770;
					var RunningHeight = 0;
					var PageNo = 1;
					var MaxHeight_after = 0;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if(PageNo == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						if($(this).height() > 29){
							
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var repoDate = $(".repoGen").text();
					$(".repoGen").remove();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						if(i == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span><div class='clear'></div><div class=\"dev-info\"> Designed &amp; Developed By <b>SIT SOLUTIONS</b> , 041-8722200, www.sitsol.net </div></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					
					$('table.tableBreak').remove();
					$(".printTable").printThis({
					  debug: false,
					  importCSS: false,
					  printContainer: true,
					  loadCSS: 'resource/css/reports.css',
					  pageTitle: "SIT Solution",
					  removeInline: false,
					  printDelay: 500,
					  header: null
				  });
				});
				$('table.hightCharts').highchartTable();
				setTimeout(function(){
					$(window).resize();
					$(".highcharts-legend-item").click();
					$(".highcharts-legend-item").click();
				},300);
			});
		</script>
      	<script type = "text/javascript" src = "resource/scripts/reports.configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
   
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >Monthly Expense Report</p>
                            <span id = "tabPanel">
                            	<div class = "tabPanel">
                                	<!--<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Ledger Detail</div>-->
                                    <div class="tabSelected" id="tab1" onClick="tab('1', '1', '1');">Search</div>
                                </div>
                            </span>
                            <div class="clear"></div>
                  		</div><!-- End .content-box-header -->  
                  
                    	<div id = "bodyTab1">
                            <div class="clear"></div>
                            <div id="form" style="margin: 0 auto; width: 900px;">
                                <form method="get" action="">
<?php
						if(isset($message)){
?>
                                    <p style="text-align:center;color:red;"><?php echo $message; ?></p>
<?php
						}
?>
                                    <div class="caption" style="line-height:30px;">Select Date :</div>
                                    <div class="field" style="width:80px;position:relative;">
                                        <select class="selectpicker show-tick form-control" name="month" data-style="btn-default" data-live-search="true" style="border:none">
                                        <?php
                                        	for($i=1; $i <= 12; $i++){
                                        		?>
                                        		<option value="<?php echo $i; ?>"><?php echo date('M',strtotime(date('d-'.$i."-Y"))); ?></option>
                                        	<?php
                                        	}
                                        ?>
                                        </select>
                                    </div>
                                    <div class="field" style="width:80px;position:relative;margin-left:20px;">
                                    	<select class="selectpicker show-tick form-control" name="year" data-style="btn-default" data-live-search="true" style="border:none">
                                        <?php
                                        	for($i=date('Y',strtotime("-20 years")); $i <= date('Y',strtotime("+10 years")); $i++){
                                        		$selected = ($i == date('Y'))?"selected":"";
                                        		?>
                                        		<option <?php echo $selected; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        	<?php
                                        	}
                                        ?>
                                        </select>
                                    </div>
                                    <div class="field" style="width:80px;position:relative;margin-left:20px;">
                                    	<input type="submit" value="Submit" name="expense" class="btn btn-default"/>
                                    </div>
                                    <div class="clear"></div>     
                                </form>
                            </div><!--form--> 
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->        
            		</div> <!-- End .content-box-content -->
					<?php 
                    	if(isset($exp_array_values)){
                    ?>
        			<div class="content-box-content">
        				<div id="form">
                    	<span style="float:right;"><button class="button printThis">Print</button></span>
                		<div id="bodyTab" class='printTable'   style="margin: 0 auto;">
                            <div style="text-align:left;margin-bottom:20px;" class="pageHeader">
                                <p style="font-size:22px;padding:0;text-align:center;font-weight:bold;">Expense Report</p>
                                <p style="float:left; margin-left:0px; font-size:14px" class="repoGen">
                                		Report generated on : <?php echo date('d-m-Y'); ?>
                                </p>
                            </div>
                            <div style="width:100%;margin:0px auto;padding:30px;">
                            	<div class="highchart-container2">
	                            	<table class="hightCharts" data-graph-container=".highchart-container2"  data-graph-type="column" style="display:none;">
	                            		<thead>
	                            			<tr>
		                                		<th style="text-align:left;">Title</th>
		                                		<th style="text-align:left;">Amount</th>
	                            			</tr>
	                            		</thead>
	                            		<tbody>
<?php
								foreach ($exp_array_values as $key => $value){
									$acc_title = $objAccounts->getAccountTitleByCode($key);
									if($value == 0){
										continue;
									}
?>
									<tr>
										<td style="text-align:right;"><?php echo $acc_title; ?></td>
										<td style="text-align:right;"><?php echo $value; ?></td>
									</tr>
<?php
								}
?>
	                            		</tbody>
			                        </table>
			                    </div>
                            </div>
                            <div style="clear:both; height:30px;"></div>
                            <table style="width:100%;margin:10px auto;" cellspacing="0" class="tableBreak">
                                <thead class="tHeader">
                                    <tr>
                                       <th width="10%" style="text-align:center;border:1px solid #ddd;" >Account Code</th>
                                       <th width="70%" colspan="2" style="text-align:center;border:1px solid #ddd;" >Account Title</th>
                                       <th width="20%" style="text-align:center;border:1px solid #ddd;"  >Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
								foreach ($exp_array_values as $key => $value){
									$acc_title = $objAccounts->getAccountTitleByCode($key);
									if($value == 0){
										continue;
									}
?>
                                	<tr>
                                		<td style="text-align:center;"><?php echo $key; ?></td>
                                		<td style="text-align:center;width:15%;"><?php echo $objAccounts->getAccountTitleByCode(substr($key,0,6)); ?></td>
                                		<td style="text-align:left;width:60%;"><?php echo $acc_title; ?></td>
                                        <td style="text-align:right;"><?php echo $value; ?></td>
                                    </tr>
<?php
								}
?>
                                </tbody>
                            </table>
                    	</div><!--bodyTab2-->
						<?php
								}//second numrows end
                        ?>
               		</div><!--summery_body-->
               		</div>
            	</div><!-- End .content-box -->
        	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>