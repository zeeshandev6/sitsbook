<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/wip.php');
	include('common/classes/wip_issue_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('wip-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	 		= new ChartOfAccounts();
	$objWorkInProcess	 	 			= new WorkInProgress();
	$objWorkInProcessDetails 	= new WorkInProcessConsumeDetails();
	$objItems            	 		= new Items();
	$objItemCategory     	 		= new itemCategory();
	$objDepartments      	 		= new Departments();

	$suppliersList   = $objAccountCodes->getAccountByCatAccCode('010104');

	if(isset($_POST['search'])){
		$objWorkInProcess->fromDate 		= ($_POST['fromDate']=='')?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$objWorkInProcess->toDate   		= ($_POST['toDate']=='')?"":date('Y-m-d',strtotime($_POST['toDate']));
		$objWorkInProcess->result_item     	= $_POST['item'];
		$objWorkInProcess->status  			= $_POST['status'];

		if($objWorkInProcess->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objWorkInProcess->fromDate = $startThisMonth;
		}

		$workReport = $objWorkInProcess->search();

		if($objWorkInProcess->result_item != ''){
			$reportType = 'I';
			$itemReportName = $objItems->getItemTitle($objWorkInProcess->result_item);
		}else{
			$reportType = 'G';
		}

	}


?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 884;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Sitsbook.com",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('.itemSelector').selectpicker();
			$('.supplierSelector').selectpicker();
			$("#repoType").selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Inventory Sales</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="">
                        <div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="" class="input_size datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="input_size datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">Product</div>
                        <div class="field" style="width:300px;position:relative;">
                                <select class="itemSelector show-tick form-control"
                                        data-style="btn-default"
                                        data-live-search="true" style="border:none" name="item">
                                   <option selected value=""></option>
<?php
															$itemsCategoryList   = $objItemCategory->getList();
                              if(mysql_num_rows($itemsCategoryList)){
                                  while($ItemCat = mysql_fetch_array($itemsCategoryList)){
																		$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
																			<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
																		if(mysql_num_rows($itemList)){
																			while($theItem = mysql_fetch_array($itemList)){
																				if($theItem['ACTIVE'] == 'N'){
																					continue;
																				}
																				if($theItem['INV_TYPE'] == 'B'){
																					continue;
																				}

																				$item_selected = (isset($workInProcess) && $workInProcess['RESULT_ITEM']==$theItem['ID'])?"selected":"";
?>
                                                 <option data-type="I" <?php echo $item_selected; ?> value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
																			}
																		}
?>
									</optgroup>
<?php
                                  }
                              }
?>
                                </select>
                        </div>
                        <div class="clear"></div>

						<div class="caption">Status</div>
                        <div class="field">
                        	<select id="repoType" name="status">
                            	<option value=""  selected >All</option>
                            	<option value="C">Completed</option>
                                <option value="P">Processing</option>
                            </select>
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Generate" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->

<?php
					if(isset($workReport)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">Work In Process Report <?php echo ($reportType == 'I')?" - ".$itemReportName:""; ?></p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            	<?php echo ($objWorkInProcess->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objWorkInProcess->fromDate)); ?>
                                <?php echo ($objWorkInProcess->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objWorkInProcess->toDate)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						$prevBillNo = '';
						if(mysql_num_rows($workReport)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="5%" style="font-size:12px;text-align:center">Job#</th>
                                   <th width="10%" style="font-size:12px;text-align:center">Status</th>
<?php
								if($reportType == 'G'){
?>
									<th width="10%" style="font-size:12px;text-align:center">Product</th>
<?php
								}
?>
                                   <th width="10%" style="font-size:12px;text-align:center">Quantity</th>
                                   <th width="10%" style="font-size:12px;text-align:center">Product Cost</th>
                                   <th width="10%" style="font-size:12px;text-align:center">Start Date</th>
                                   <th width="10%" style="font-size:12px;text-align:center">Completion Date</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_qty = 0;
								$total_price = 0;
								while($detailRow = mysql_fetch_array($workReport)){
									$status = ($detailRow['STATUS'] == 'C')?"Completed":"Processing";
?>
                                <tr id="recordPanel" class="alt-row">
	                                <td style="text-align:center;"><?php echo $detailRow['ID']; ?></td>
                                    <td style="text-align:center;"><?php echo $status; ?></td>
<?php
							if($reportType == 'G'){
								$itemTitle = $objItems->getItemTitle($detailRow['RESULT_ITEM']);
?>
									<td style="text-align:center;"><?php echo $itemTitle; ?></td>
<?php
							}
?>
									<td style="text-align:center;"><?php echo $detailRow['RESULT_QUANTITY']; ?></td>
                                    <td style="text-align:center;"><?php echo $detailRow['TOTAL_COST']; ?></td>
                                    <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($detailRow['STARTED_ON'])); ?></td>
                                    <td style="text-align:center;"><?php echo $detailRow['COMPLETED_ON'] == '0000-00-00'?'':date('d-m-Y',strtotime($detailRow['COMPLETED_ON'])); ?></td>
                                </tr>
<?php
								$total_qty   += $detailRow['RESULT_QUANTITY'];
								$total_price += $detailRow['TOTAL_COST'];
								}
?>
                            </tbody>
<?php
						}//end if
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;" colspan="<?php echo ($reportType == 'I')?"2":"3"; ?>">Total:</td>
                                <td style="text-align:center;"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
                                <td style="text-align:center;"> <?php echo (isset($total_price))?$total_price:"0"; ?> </td>
                                <td style="text-align:center;"> - - - </td>
                                <td style="text-align:center;"> - - - </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>

<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
