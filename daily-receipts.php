<?php
	include("common/connection.php");
	include 'common/config.php';
	include("common/classes/cash-receipts.php");
	include("common/classes/accounts-code-management.php");
	include("common/classes/j-voucher.php");
	include("common/classes/accounts.php");

	$objDailyExpenditure 	= new CashReceipts;
	$objAccCode 			= new AccountCodeManagement;
	$objJournalVoucher 		= new JournalVoucher;
	$objAccounts  			= new ChartOfAccounts;

	$mainList  		= $objAccounts->getGeneralList();

	if(isset($_POST['Save'])){
		$objDailyExpenditure->expenseDate= date('Y-m-d');
		$objDailyExpenditure->category 	 = $_POST['category'];
		$expenseAccTitle = $objAccounts->getAccountTitleByCode($_POST['category']);

		$objDailyExpenditure->account_type = isset($_POST['account_type'])?$_POST['account_type']:"";

		if($objDailyExpenditure->account_type == 'C'){
			$objDailyExpenditure->account_code = '0101010001';
		}else{
			$objDailyExpenditure->account_code = $_POST['onThisAccount'];
		}

		$otherAccountCode  = $objDailyExpenditure->account_code;
		$otherAccountTitle = $objAccounts->getAccountTitleByCode($objDailyExpenditure->account_code);

		$objDailyExpenditure->amount 	 = $_POST['amount'];
		$objDailyExpenditure->detail 	 = $_POST['detail'];

		if($objDailyExpenditure->account_type == '' || $objDailyExpenditure->amount <= 0){
			echo "<script>window.location.href = 'daily-receipts.php?blank';</script>";
			exit();
		}

		if($_POST['expense_id'] == 0){
			$expense_id = $objDailyExpenditure->save();

			$voucher_id = $objJournalVoucher->ReceiptVoucher(0,$objDailyExpenditure->expenseDate,$objDailyExpenditure->amount,$objDailyExpenditure->category,$expenseAccTitle,$otherAccountCode,$otherAccountTitle);
			$objDailyExpenditure->insertVoucherId($voucher_id,$expense_id);
			echo "<script>window.location.href = 'daily-receipts.php';</script>";
			exit();
		}

		if($_POST['expense_id'] > 0){
			$expense_id = mysql_real_escape_string($_POST['expense_id']);
			$objDailyExpenditure->update($expense_id);
			$voucher_id = $objDailyExpenditure->getVoucherId($expense_id);
			$objJournalVoucher->ReceiptVoucher($voucher_id,$objDailyExpenditure->expenseDate,$objDailyExpenditure->amount,$objDailyExpenditure->category,$expenseAccTitle,$otherAccountCode,$otherAccountTitle);

			echo "<script>window.location.href = 'daily-receipts.php?id=".$expense_id."';</script>";
			exit();
		}
	}
	if(isset($_GET['id'])){
		$expense_id = mysql_real_escape_string($_GET['id']);
		$expense_details = $objDailyExpenditure->getDetails($expense_id);
	}

	if(isset($_GET['blank'])){
		$message = "Account Not Selected";
	}

	$getExpenseList = $objDailyExpenditure->expenseList();
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>Daily Receipts</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<style>
			html{
			}
			ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized, input.other{
				float:left;
				width: 300px;
				padding-left: 5px;
				color:rgb(102, 102, 102) !important;
				border: 1px solid rgb(153, 153, 153);
				height:0px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;

				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;

				-moz-border-radius-topleft:3px;
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;

				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;

				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			 }
			 .input_sized:hover{
				border: 1px solid rgb(41, 153, 255) !important;
				box-shadow: 0 0 2px #9ecaed;
			 }
			 table th, table td{
				 text-align:center
			 }

		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	    <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script>
			$(document).ready(function(){
				$("select[name='category'],select[name='accType'],select[name='onThisAccount']").selectpicker();
				$("div.accountsList,div.onAccountsList").hide();
				$("select[name='accType']").change(function(){
					var generalType = $(this).find("option:selected").val();
					if($(this).find("option:selected").val() != ''){
						var generalType = $(this).find("option:selected").val();
						$("select[name='category'] option").prop("disabled",true);
						$("select[name='category'] option[value^='"+generalType+"']").prop("disabled",false).siblings();
						$("div.accountsList").slideDown();
					}else{
						$("div.accountsList").slideUp();
					}
					$("select[name='category']").selectpicker('refresh');
				});
				//check account Type
					var accType = $("input[name='account_type']:checked").val();
					if(accType == 'A'){
						$("div.onAccountsList").slideDown();
					}else{
						$("div.onAccountsList").slideUp();
					}

					var generalType = $(this).find("option:selected").val();
					if(generalType != ''){
						$("select[name='category'] option").prop("disabled",true);
						$("select[name='category'] option[value^='"+generalType+"']").prop("disabled",false).siblings();
						$("div.accountsList").slideDown();
					}
				//check account Type END
				$("input[name='account_type']").change(function(){
					var accType = $("input[name='account_type']:checked").val();
					if(accType == 'A'){
						$("div.onAccountsList").slideDown();
					}else{
						$("div.onAccountsList").slideUp();
					}
				});

<?php
			if(isset($message)){
?>
				displayMessage('<?php echo $message; ?>');
<?php
			}
?>

            });
		</script>
   </head>

   	<body>
   		<div id = "body-wrapper">
      		<div id="sidebar">
				<?php include("common/left_menu.php") ?>
            </div> <!-- End #sidebar -->


      		<div id = "bodyWrapper">
        		<div class = "content-box-top">
            		<div class = "summery_body">
               			<div class = "content-box-header">
                  			<p >Daily Receipts</p>
                            <span id="tabPanel">
                                <div class="tabPanel">
                                    <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">Receipts</div>
                                    <div class="tab" id="tab2" onclick="tab('2','1','2');">List</div>
                                </div>
                            </span>
                        	<div style = "clear:both;"></div>
               			</div><!-- End .content-box-header -->
                        <div style = "height:30px"></div>

                        <div id = "bodyTab1" style="display:block">
                        <form method="post" action="">
                            <div id = "form">
                                <div class = "caption">Entry Date:</div>
                                <div class = "field">
                                    <input type="text" class="input_size" value="<?php echo (isset($expense_details))?date('d-m-Y',strtotime($expense_details['EXPENSE_DATE'])):date("d-m-Y") ?>" readonly="readonly" style="width:90px;"/>
                                </div>
                                <div style="clear:both;"></div>

                                <div class = "caption">General Title:</div>
                                <div class = "field">
                                    <select name="accType" class="form-control show-tick" data-style="btn-default" data-live-search="true"  >
                                    	<option value=""></option>
<?php
			$selected = '';
			if (mysql_num_rows($mainList)){
				while($codeList = mysql_fetch_array($mainList)){
					$selected = (isset($expense_details) && substr($expense_details['EXPENSE_CODE'],0,2) == $codeList['ACC_CODE'])?"selected='selected'":"";
?>
                                        <option value="<?php echo $codeList['ACC_CODE']; ?>" <?php echo $selected; ?> >
											<?php echo $codeList['ACC_TITLE'];  ?>
                                        </option>
<?php
				}
			}
?>
                                    </select>
                                </div>
                                <div style="clear:both;"></div>

                                <div class="accountsList">
                                <div class = "caption">Account Title:</div>
                                <div class = "field">
                                    <select name="category" class="form-control show-tick" data-style="btn-default" data-live-search="true"  data-hide-disabled="true" >
                                    	<option></option>
<?php
	$selected = '';
	$getCodeList 	= $objAccounts->getLevelFourList();
	if ($getCodeList != NULL && mysql_num_rows($getCodeList)){
		while($codeList = mysql_fetch_array($getCodeList)){
			$selected = (isset($expense_details) && $expense_details['EXPENSE_CODE'] == $codeList['ACC_CODE'])?"selected='selected'":"";
?>

                                        <option value="<?php echo $codeList['ACC_CODE']; ?>" <?php echo $selected; ?> >
											<?php echo $codeList['ACC_TITLE'];  ?>
                                        </option>
<?php
		}
	}
?>
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                </div>

                                <div class = "caption">Amount:</div>
                                <div class = "field">
                                    <input type="text" class="input_size" value="<?php echo (isset($expense_details))?$expense_details['AMOUNT']:""; ?>" name="amount"/>
                                </div>
                                <div style="clear:both;"></div>

                                <div class = "caption">A/c Type:</div>
                                <div class = "field">
                                    <input type="radio" name="account_type" checked="checked" value="C"
										<?php echo (isset($expense_details) && $expense_details['ACCOUNT_TYPE'] == 'C')?"checked='checked'":""; ?> /> Cash
                                    <input type="radio" name="account_type" value="A"
										<?php echo (isset($expense_details) && $expense_details['ACCOUNT_TYPE'] == 'A')?"checked='checked'":""; ?> /> Account
                                </div>
                                <div style="clear:both;"></div>

                                <div class="onAccountsList">
                                <div class = "caption">Account Title:</div>
                                <div class = "field">
                                    <select name="onThisAccount" class="form-control show-tick" data-style="btn-default" data-live-search="true"   data-hide-disabled="true">
                                    	<option></option>
<?php
	$selected = '';
	$getCodeList 	= $objAccounts->getLevelFourList();
	if ($getCodeList != NULL && mysql_num_rows($getCodeList)){
		while($codeList = mysql_fetch_array($getCodeList)){
			$selected = (isset($expense_details)&&$expense_details['ACCOUNT_CODE'] == $codeList['ACC_CODE'])?'selected="selected"':"";
			if(substr($codeList['ACC_CODE'],0,6) != '010102' && substr($codeList['ACC_CODE'],0,6) != '010104'){
				continue;
			}
?>
                                        <option value="<?php echo $codeList['ACC_CODE']; ?>" <?php echo $selected; ?> >
											<?php echo $codeList['ACC_TITLE'];  ?>
                                        </option>
<?php
		}
	}
?>
                                    </select>
                                </div>
                                <div style="clear:both;"></div>
                                </div>

                                <div class = "caption">Detail:</div>
                                <div class = "field">
                                    <textarea style="width:293px" name="detail"><?php echo (isset($expense_details))?$expense_details['DETAIL']:""; ?></textarea>
                                </div>
                                <div style="clear:both;"></div>

                                <div class = "caption"></div>
                                <div class = "field">
                                	<input type="hidden" name="expense_id" value="<?php echo isset($expense_details)?$expense_details['EXPENDITURE_ID']:0; ?>" />
                                   <input type="submit" value="Save" name="Save" class="enter_button" />
                                   <input type="button" value="New" class="enter_button" onclick="window.location.href = 'daily-receipts.php';" />
                                </div>
                                <div style = "clear:both;"></div>
                            </div><!--form-->
                            <div style = "clear:both;bodyWrapper"></div>
                        </form>
                        </div><!--bodyTab1-->

                        <div id = "bodyTab2" style="display:none">
                            <table style="width:90%">
                                <thead>
                                    <tr>
                                        <th width="5%">Sr#</th>
                                        <th>Date</th>
                                        <th>Account Title</th>
                                        <th>Amount</th>
                                        <th>Detail</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
	if ($getExpenseList != NULL && mysql_num_rows($getExpenseList)){
		$counter = 1;
		$expense_total = 0;
		while($expenseList = mysql_fetch_array($getExpenseList)){
			$category = $objAccCode->getCategory($expenseList['EXPENSE_CODE']);
?>

                                    <tr>
                                        <td><?php echo $counter ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($expenseList['EXPENSE_DATE'])); ?></td>
                                        <td><?php echo $category; ?></td>
                                        <td><?php echo $expenseList['AMOUNT']; ?></td>
                                        <td style="text-align:center"><?php echo $expenseList['DETAIL']; ?></td>
                                        <td>
                                        <a href="daily-receipts.php?id=<?php echo $expenseList['EXPENDITURE_ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                        <a class="pointer" onclick="deleteRow(<?php echo $expenseList['EXPENDITURE_ID']; ?>,'db/del-expo-receipt.php',this);"><i class="fa fa-times"></i></a>
                                        </td>

                                    </tr>
<?php
		$counter++;
		$expense_total += $expenseList['AMOUNT'];
		}
	}
?>

                                </tbody>
                                <tfoot>
                                	<tr>
                                    	<td colspan="3">Total</td>
                                        <td><?php echo (isset($expense_total))?$expense_total:0; ?></td>
                                        <td colspan="2"> - - - </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div style = "clear:both;"></div>
                        </div><!--bodyTab2-->
            		</div><!--summery_body-->
	         	</div><!--End.content-box-top-->
      		</div><!--bodyWrapper-->
      	</div>  <!--body-wrapper-->
        <div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>
