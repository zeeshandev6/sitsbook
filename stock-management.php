<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/branches.php');
	include('common/classes/branch_stock.php');
	include('common/classes/stock_transfer.php');
	include('common/classes/stock_transfer_detail.php');

	//Permission
	if(!in_array('stock-transfer',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objItems                  = new Items();
	$objItemCategory           = new itemCategory();
	$objBranches  	           = new Branches();
	$objBranchStock            = new BranchStock();
	$objStockTransfer          = new StockTransfer();
	$objStockTransferDetail    = new StockTransferDetail();

	//Pagination Settings
	$objPagination  = new Pagination();
	$total          = $objPagination->getPerPage();
	//Pagination Settings { END }

	if(isset($_GET['tab'])){
		$mode = $_GET['tab'];
	}else{
		$mode = 'form';
	}


	$transfer_id       = 0;
	$transfer_details  = NULL;
	$itemsCategoryList = $objItemCategory->getList();
	$branchesList      = $objBranches->getList();
	$brach_array       = array();
	if(mysql_num_rows($branchesList)){
		while($brnch = mysql_fetch_assoc($branchesList)){
			$brach_array[] = $brnch;
		}
	}
	if(isset($_GET['id'])){
		$transfer_id           = (int)(mysql_real_escape_string($_GET['id']));
		$transfer_details      = $objStockTransfer->getDetails($transfer_id);
		$transfer_details_list = $objStockTransferDetail->getList($transfer_id);
	}
	if($mode=='list'){
		$start = 0;
		$this_page = 0;
		if(isset($_GET['page'])){
			$this_page = $_GET['page'];
			if($this_page>=1){
				$this_page--;
				$start = $this_page * $total;
			}
		}
		$transfer_list = $objStockTransfer->search($start, $total);
		$found_records = $objStockTransfer->found_records;
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		div.tabPanel .tab,div.tabPanel .tabSelected{
			-webkit-box-sizing: content-box !important;
			-moz-box-sizing: content-box !important;
			box-sizing: content-box !important;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
		td,th{
			padding: 10px 5px !important;
			border:1px solid #CCC !important;
		}
		.caption{
			padding: 5px !important;
		}
		td.itemName{
			padding-left: 10px !important;
		}
		td.quantity{
			position: relative;
		}
		input.error{
			border-color: red !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/stock.transfer.config.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Stock Transfer Management</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
						?>
						<a href="stock-management.php?tab=list<?php echo $page; ?>"><div class="<?php echo $mode=='list'?"tabSelected":"tab"; ?>">List</div></a>
						<a href="stock-management.php?tab=search<?php echo $page; ?>"><div class="<?php echo $mode=='search'?"tabSelected":"tab"; ?>">Search</div></a>
						<a href="stock-management.php?tab=form<?php echo $page; ?>"><div class="<?php echo $mode=='form'?"tabSelected":"tab"; ?>">Details</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content" style="padding: 5px;">
				<?php
				if($mode == 'list'){
					?>
					<div id="bodyTab1">
						<div id="form" style="margin: 20px auto;">
							<table>
								<thead>
									<tr>
										<th class="col-xs-1 text-center">Sr.</th>
										<th class="col-xs-2 text-center">Entry Date</th>
										<th class="col-xs-6 text-center"></th>
										<th class="col-xs-2 text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$counter = $start + 1;
									if(mysql_num_rows($transfer_list)){
										while($transfer_record = mysql_fetch_assoc($transfer_list)){
											?>
											<tr>
												<td class="text-center"><?php echo $counter; ?></td>
												<td class="text-center"><?php echo date("d-m-Y",strtotime($transfer_record['ENTRY_DATE'])); ?></td>
												<td class="text-center">---</td>
												<td class="text-center">
													<a id="view_button" href="stock-management.php?tab=form&page=<?php echo $this_page; ?>&id=<?php echo $transfer_record['ID']; ?>"><i class="fa fa-pencil"></i></a>
													<a class="pointer ml-5" do="<?php echo $transfer_record['ID']; ?>" href="#"> <i class="fa fa-times"></i> </a>
												</td>
											</tr>
											<?php
											$counter++;
										}
									}
									?>
								</tbody>
							</table>
							<div class="col-xs-12 text-center">
								<?php
								if($found_records > $total){
									$get_url = "";
									foreach($_GET as $key => $value){
										$get_url .= ($key == 'page')?"":"&".$key."=".$value;
									}
									?>
									<nav>
										<ul class="pagination">
											<?php
											$count = $found_records;
											$total_pages = ceil($count/$total);
											$i = 1;
											$thisFileName = basename($_SERVER['PHP_SELF']);
											if(isset($this_page) && $this_page>0){
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
												</li>
												<?php
											}
											if(isset($this_page) && $this_page>=1){
												$prev = $this_page;
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
												</li>
												<?php
											}
											$this_page_act = $this_page;
											$this_page_act++;
											while($total_pages>=$i){
												$left = $this_page_act-5;
												$right = $this_page_act+5;
												if($left<=$i && $i<=$right){
													$current_page = ($i == $this_page_act)?"active":"";
													?>
													<li class="<?php echo $current_page; ?>">
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
													</li>
													<?php
												}
												$i++;
											}
											$this_page++;
											if(isset($this_page) && $this_page<$total_pages){
												$next = $this_page;
												?>
												<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
												<?php
											}
											if(isset($this_page) && $this_page<$total_pages){
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
												</li>
											</ul>
										</nav>
										<?php
									}
								}
								?>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				<?php
				if($mode == 'search'){
					?>
					<div id="bodyTab1">
						<div id="form" style="margin: 20px auto;">
							<form method="get" action="">
								<input type="hidden" value="list" name="tab" />
								<div class="caption" style="width:120px;margin-left:0px;padding: 0px;">From Date</div>
								<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
									<input type="text" name="from_date" id="datepicker" value="" class="form-control datepicker" style="width:150px" />
								</div>
								<div class="clear" ></div>

								<div class="caption" style="width:120px;margin-left:0px;padding: 0px;">To Date</div>
								<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
									<input type="text" name="to_date" id="datepicker" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
								</div>
								<div class="clear" ></div>

								<div class="caption" style="width:120px;margin-left:0px;padding: 0px;"></div>
								<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
									<input type="submit" name="search" value="Search" class="button"  />
								</div>
								<div class="clear" ></div>
							</form>
						</div>
					</div>
					<?php
				}
				?>
				<?php
				if($mode == 'form'){
					?>
					<div id="bodyTab1">
						<div id="form" style="margin: 20px auto;">
							<input type="hidden" class="transfer_id" value="<?php echo $transfer_id; ?>" />
							<div class="caption" style="width:120px;margin-left:0px;padding: 0px;">Transfer Date</div>
							<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
								<input type="text" name="transfer_date" id="datepicker" value="<?php echo ($transfer_details != NULL)?date("d-m-Y",strtotime($transfer_details['ENTRY_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
							</div>
							<div class="clear" style="height: 30px;"></div>
							<table class="prom">
								<thead>
									<tr>
										<th width="15%" style="font-size:14px;font-weight:normal;text-align:center">Item</th>
										<th width="15%" style="font-size:14px;font-weight:normal;text-align:center">From</th>
										<th width="15%" style="font-size:14px;font-weight:normal;text-align:center">To</th>
										<th width="10%" style="font-size:14px;font-weight:normal;text-align:center">Quantity</th>
										<th width="10%" style="font-size:14px;font-weight:normal;text-align:center">Unit Cost</th>
										<th width="10%" style="font-size:14px;font-weight:normal;text-align:center">Total Cost</th>
										<th width="10%" style="font-size:14px;font-weight:normal;text-align:center">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr class="quickSubmit" style="background:none">
										<td>
											<select class="itemSelector show-tick form-control"
											data-style="btn btn-default"
											data-live-search="true" style="border:none">
											<option selected value=""></option>
											<?php
											if(mysql_num_rows($itemsCategoryList)){
												while($ItemCat = mysql_fetch_array($itemsCategoryList)){
													$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
													?>
													<optgroup label="<?php echo $ItemCat['NAME']; ?>">
														<?php
														if(mysql_num_rows($itemList)){
															while($theItem = mysql_fetch_array($itemList)){
																if($theItem['ACTIVE'] == 'N'){
																	continue;
																}
																if($theItem['INV_TYPE'] == 'B'){
																	continue;
																}
																?>
																<option data-subtext="<?=$theItem['ITEM_BARCODE']?>" value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
																<?php
															}
														}
														?>
													</optgroup>
													<?php
												}
											}
											?>
										</select>
									</td>
									<td>
										<select class="from_branch form-control show-tick" data-style="btn-default">
											<option value="0">Main Branch</option>
											<?php
											foreach ($brach_array as $key => $row){
												?><option value="<?php echo $row['ID']; ?>" data-subtext="<?php echo ($row['BRANCH_TYPE']=='G')?"Godown":"Branch"; ?>"><?php echo $row['TITLE']; ?></option><?php
											}
											?>
										</select>
									</td>
									<td>
										<select class="to_branch form-control show-tick">
											<option value="0">Main Branch</option>
											<?php
											foreach ($brach_array as $key => $row){
												?><option value="<?php echo $row['ID']; ?>" data-subtext="<?php echo ($row['BRANCH_TYPE']=='G')?"Godown":"Branch"; ?>"><?php echo $row['TITLE']; ?></option><?php
											}
											?>
										</select>
									</td>
									<td>
										<input type="text"  class="quantity text-center form-control"/>
									</td>
									<td>
										<input type="text"  class="unit_cost text-center form-control"/>
									</td>
									<td>
										<input type="text"  class="total_cost text-center form-control"/>
									</td>
									<td><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
								</tr>
								<tr class="tr-td-0-pad">
									<td width="15%" class="text-default text-center">Stock Quantity</td>
									<td width="15%" class="text-info text-center" >
										<input type="text" class="form-control from_stock text-center" data-stock="0" value=""  readonly />
									</td>
									<td width="15%" class="text-info text-center" >
										<input type="text" class="form-control to_stock text-center" data-stock="0" value="" readonly />
									</td>
									<td colspan="4" class="text-center">- - -</td>
								</tr>
							</tbody>
							<tbody>
								<!--doNotRemoveThisLine-->
								<tr style="display:none;" class="calculations"></tr>
								<!--doNotRemoveThisLine-->
								<?php
								if(isset($transfer_details_list) && mysql_num_rows($transfer_details_list)){
									while($invRow = mysql_fetch_array($transfer_details_list)){
										$itemName     = $objItems->getItemTitle($invRow['ITEM_ID']);
										$from_branchn = ($invRow['FROM_BRANCH'] == 0)?"Main Branch":$objBranches->getTitle($invRow['FROM_BRANCH']);
										$to_branchn   = ($invRow['TO_BRANCH'] == 0)?"Main Branch":$objBranches->getTitle($invRow['TO_BRANCH']);
										?>
										<tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
											<td style="text-align:left;"   class="itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
											<td style="text-align:center;" class="from_branch" data-id="<?php echo $invRow['FROM_BRANCH'] ?>"><?php echo $from_branchn; ?></td>
											<td style="text-align:center;" class="to_branch"   data-id="<?php echo $invRow['TO_BRANCH'] ?>"><?php echo $to_branchn; ?></td>
											<td style="text-align:center;" class="quantity"    ><?php echo $invRow['QUANTITY'] ?></td>
											<td style="text-align:center;" class="unit_cost"   ><?php echo $invRow['UNIT_COST'] ?></td>
											<td style="text-align:center;" class="total_cost"  ><?php echo $invRow['TOTAL_COST'] ?></td>
											<td style="text-align:center;">
												<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
												<a class="pointer" onclick="shorDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>
											</td>
										</tr>
										<?php
									}
								}
								?>
								<tr class="totals">
									<td style="text-align:center;background-color:#EEEEEE;">Total</td>
									<td style="text-align:center;background-color:#EEE;">- - -</td>
									<td style="text-align:center;background-color:#EEE;">- - -</td>
									<td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
									<td style="text-align:center;background-color:#EEE;">- - -</td>
									<td style="text-align:center;background-color:#f5f5f5;" class="costTotal"></td>
									<td style="text-align:center;background-color:#EEE;">- - -</td>
								</tr>
							</tbody>
						</table>
						<div style="height:10px;"></div>
						<div class="panel panel-default pull-left" style="width:550px;">
							<div class="panel-heading">Notes : </div>
							<textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $transfer_details['NOTES']; ?></textarea>
						</div>
						<div class="pull-right" style="padding: 0em 1em;width:450px;">
							<div class="pull-right">

								<div class="clear"></div>
							</div>
						</div>
						<div class="clear"></div>
						<div style="height:10px;"></div>
						<div class="underTheTable col-md-4 pull-right" style="text-align: center;">
							<div class="button saveTransferRecord"><?php echo ($transfer_id > 0)?"Update":"Save"; ?></div>
							<div class="button pull-right" onclick="window.location.href='stock-management.php';">New Form</div>
						</div><!--underTheTable-->
						<div class="clear"></div>
						<div style="margin-top:10px"></div>
					</div> <!-- End form -->
				</div> <!-- End #tab1 -->
				<?php
			} //end if == form
			?>
		</div> <!-- End .content-box-content -->
	</div> <!-- End .content-box -->
</div><!--body-wrapper-->
<div id="xfade" onclick="hidePopUpBox();" ></div>
<div id="fade"></div>
<div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
<div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
$(document).ready(function() {
	$('select').selectpicker();
	$(document).calculateColumnTotals();
	$("a[do]").click(function(){
		$(this).deleteRowConfirmation('db/deleteTransferRecord.php');
	});
	$("input[name='billNum']").numericOnly();
	$("input[name='gpNum']").numericOnly();
	$("input.quantity").numericOnly();
	$("input.unitPrice").numericFloatOnly();
	$("input.discount").numericFloatOnly();
	$("#datepicker").setFocusTo("input[name='billNum']");
	$("input[name='gpNum']").setFocusTo("input[name='billNum']");
	$("input[name='billNum']").setFocusTo(".dropdown-toggle:first");
	$("input[name='gpNum']").focus();

	$(document).calculateColumnTotals();

	$(".supplierSelector").change(function(){
		$("div.itemSelector button").focus();
	});
	$('div.itemSelector button').keyup(function(e){
		if(e.keyCode == 13 && $('select.itemSelector option:selected').val() != ''){
			if($(".updateMode").length == 0){
				$("select.from_branch").change();
			}
			$('div.from_branch button').focus();
		}
	});
	$('div.from_branch button').keyup(function(e){
		if(e.keyCode == 13 && $('select.from_branch option:selected').val() != ''){
			$('div.to_branch button').focus();
			$("select.to_branch").change();
		}
	});
	$('div.to_branch button').keyup(function(e){
		if(e.keyCode == 13 && $('select.from_branch option:selected').val() != ''){
			$("input.quantity").focus();
			$("select.to_branch").change();
		}
	});
	$("select.from_branch").change(function(){
		var branch_id = parseInt($("select.from_branch option:selected").val())||0;
		var item_id   = parseInt($("select.itemSelector option:selected").val())||0;
		get_branch_stock(branch_id,item_id,".from_stock");
	});
	$("select.to_branch").change(function(){
		var branch_id = parseInt($("select.to_branch option:selected").val())||0;
		var item_id   = parseInt($("select.itemSelector option:selected").val())||0;
		get_branch_stock(branch_id,item_id,".to_stock");
	});
	$(document).keydown(function(e){
		if(e.keyCode == 32 && e.ctrlKey){
			$(".trasactionType").click();
			$(".customer_mobile").focus();
		}
	});
	$("input.quantity").setFocusTo("input.unit_cost");
	$("input.quantity").keyup(function(){
		stockOs();
		$(this).multiplyTwoFeilds("input.unit_cost","input.total_cost");
	});
	$("input.unit_cost").keyup(function(e){
		$(this).multiplyTwoFeilds("input.quantity","input.total_cost");
	});
	$("input.unit_cost").keydown(function(e){
		if(e.keyCode == 13){
			$(".addDetailRow").focus();
		}
	});
	$(".addDetailRow").click(function(){
		$(this).quickSave();
	});
	$(".saveTransferRecord").click(function(){
		saveTransferRecord();
	});
	$(window).keydown(function(e){
		if(e.altKey == true&&e.keyCode == 73){
			e.preventDefault();
			$("#form").click();
			$("div.itemSelector button").click();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 83){
			e.preventDefault();
			$("#form").click();
			saveTransferRecord();
			return false;
		}
	});

});
<?php
if(isset($_GET['saved'])){
	?>
	displayMessage('Saved! Stock transfered.');
	<?php
}
?>
<?php
if(isset($_GET['updated'])){
	?>
	displayMessage('Updated! Stock transfered.');
	<?php
}
?>

</script>
