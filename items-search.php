<?php
  $out_buffer = ob_start();
	include('common/connection.php');
	include 'common/config.php';
  include 'common/classes/md.php';
	include('common/classes/items.php');
	include('common/classes/measure.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/godown-details.php');
  include 'common/classes/mobile-purchase-details.php';

	$objItems                  = new Items();
	$objItemsCategory          = new itemCategory();
	$objDepartments            = new Departments();
	$objMeasures               = new Measures();
  $objMobilePurchaseDetails  = new ScanPurchaseDetails();
	$objGodownDetails          = new GodownDetails();
  $objConfigs                = new Configs();

	//Pagination Settings
	$objPagination  = new Pagination;
	$items_per_page = $objPagination->getPerPage();
	//Pagination Settings { END }
    if(isset($_POST['search'])){
        $objItems->name = mysql_real_escape_string($_POST['item_name']);
        $objItems->company_name = mysql_real_escape_string($_POST['company_name']);
        $objItems->itemCatId    = mysql_real_escape_string($_POST['itemCatId']);
        $objItems->barcode      = mysql_real_escape_string($_POST['barcode']);
        $objItems->measure      = mysql_real_escape_string($_POST['measure']);
        $objItems->inv_type     = mysql_real_escape_string($_POST['inv_type']);

        $objItems->from_sale_price = mysql_real_escape_string($_POST['from_sale_price']);
        $objItems->to_sale_price   = mysql_real_escape_string($_POST['to_sale_price']);

        $report                 = (isset($_POST['report']))?mysql_real_escape_string($_POST['report']):"";

        $itemsList = $objItems->search();

        if($report==''){
          if(in_array('stock-full',$permissionz)){
            $full_print = true;
          }elseif(in_array('stock-limited',$permissionz)){
            $full_print = false;
          }else{
            $noprint = true;
          }
        }elseif($report != 'F'){
          $full_print = false;
        }elseif($report == 'F'){
          $full_print = true;
        }else{
            $noprint = true;
        }

        if($admin){
            unset($noprint);
            $full_print = ($report == 'F')?true:false;
        }

        if(isset($noprint)){
          echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        	echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        	echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        	echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        	echo '</div>';
          exit();
        }

    }
    $measureList = $objMeasures->getList();
    $itemCategoryList = $objItemsCategory->getList();

    $barcode_print_flag = $objConfigs->get_config('BARCODE_PRINT');
    $barcode_print_flag = ($barcode_print_flag == 'Y')?true:false;
?>
<!DOCTYPE html 
>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" />
    <style>
        .caption{
            padding: 5px;
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>

    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
            $("select").selectpicker();
            $(".printThis").click(function(){
                $(".printDiv").printThis({
                      debug: false,
                      importCSS: false,
                      printContainer: true,
                      loadCSS: 'resource/css/reports.css',
                      pageTitle: "Sitsbook.net",
                      removeInline: false,
                      printDelay: 500,
                      header: null
                });
            });
        });
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p> <i class="fa fa-search"></i> Search Inventory Items</p>
                <?php if($admin){ ?>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="items-management.php"><div class="tab">List</div></a>
                        <a href="items-search.php"><div class="tabSelected">Search</div></a>
                        <a href="item-details.php"><div class="tab">Add New</div></a>
                    </div>
                </span>
                <?php } ?>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1">
                    <div id="form">
                        <form action="" method="post">
                        <div class="caption">Item Category</div>
                        <div class="field" style="width:250px;">
                            <select name="itemCatId" class="form-control catSelector" data-live-search="true" style="width:250px; height:30px">
                                <option value=""></option>
<?php
                        if(mysql_num_rows($itemCategoryList)){
                            while($row = mysql_fetch_array($itemCategoryList)){
                                $selectedCategory = ( isset($itemDetails) && $row['ITEM_CATG_ID']==$itemDetails['ITEM_CATG_ID'])?"selected=\"selected\"":"";
?>
                                <option value="<?php echo $row['ITEM_CATG_ID']; ?>" <?php echo $selectedCategory ;?> >
                                    <?php echo $row['NAME']; ?>
                                </option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Company</div>
                        <div class="field"><input type="text" class="form-control" style="width:250px;" name="company_name" /></div>
                        <div class="clear"></div>

                        <div class="caption">Item Name</div>
                        <div class="field"><input type="text" class="form-control" style="width:250px;" name="item_name" /></div>
                        <div class="clear"></div>

                        <div class="caption">Barcode</div>
                        <div class="field"><input type="text" class="form-control" style="width:250px;" name="barcode" /></div>
                        <div class="clear"></div>

                        <div class="caption">From Sale Price :</div>
                        <div class="field"><input type="text" class="form-control" style="width:250px;" name="from_sale_price" /></div>
                        <div class="clear"></div>

                        <div class="caption">To Sale Price :</div>
                        <div class="field"><input type="text" class="form-control" style="width:250px;" name="to_sale_price" /></div>
                        <div class="clear"></div>

                        <div class="caption">Measure</div>
                        <div class="field" style="width:250px;">
                            <select type="text" name="measure" class="form-control measureSelector" style="width:250px; height:30px">
                                <option value=""></option>
<?php
                        if(mysql_num_rows($measureList)){
                            while($row = mysql_fetch_array($measureList)){
                                $selectedMeasure = ( isset($itemDetails) && $row['ID'] == $itemDetails['MEASURE'])?"selected=\"selected\"":"";
?>
                                <option value="<?php echo $row['ID']; ?>" <?php echo $selectedMeasure; ?> >
                                    <?php echo $row['NAME']; ?>
                                </option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Type</div>
                        <div class="field" style="width:250px;">
                            <select name="inv_type" class="form-control">
                                <option value="">All</option>
                                <option value="B">Mobile</option>
                                <option value="N">Non Mobile</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <?php if($admin){ ?>
                        <div class="caption">Report</div>
                        <div class="field" style="width:250px;">
                            <select name="report" class="form-control">
                                <option value="F">Full</option>
                                <option value="S">Stock Only</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <?php } ?>

                        <div class="caption"></div>
                        <div class="field" style="width:250px;">
                            <input type="submit" class="button" value="Search" name="search" />
                        </div>
                        <div class="clear"></div>
                        </form>
                    </div><!--form-->
<?php
    if(isset($itemsList)&&mysql_num_rows($itemsList)){
?>
                <div class="button printThis pull-right"><i class="fa fa-print"></i> Print </div>
                <div class="clear"></div>
                <div id="form" class="printDiv">
                    <div style="text-align:left;margin-bottom:20px;" class="pageHeader">
                      <p style="font-size:18px;padding:0;text-align:left;">Stock Position</p>
                      <p style="float:left; margin-left:0px; font-size:14px" class="repoGen">
                        Report generated on : <?php echo date('d-m-Y'); ?>
                      </p>
                    </div>
                    <table cellspacing="0" class="tableBreak" >
                        <thead class="tHeader">
                            <tr>
                               <th width="5%" rowspan="2" style="text-align:center">Sr#</th>
                               <th width="5%" rowspan="2" style="text-align:center;">Barcode</th>
                               <th width="10%" rowspan="2" style="text-align:center;">Category</th>
                               <th width="15%" rowspan="2" style="text-align:center;">Item</th>
                               <th colspan="3" width="20%" style="text-align:center;">Stock</th>
                               <th width="5%" rowspan="2" style="text-align:center;">Min/Qty</th>
                               <?php if($full_print){ ?>
                               <th width="5%" rowspan="2" style="text-align:center;">Cost</th>
                               <th width="5%" rowspan="2" style="text-align:center;">Disc</th>
                               <th width="5%" rowspan="2" style="text-align:center;">Price</th>
                               <th width="5%" rowspan="2" style="text-align:center;">Total Cost</th>
                               <?php if($mobileAddon == 'Y'){ ?>
                               <th width="5%" rowspan="2" style="text-align:center;">IMEI</th>
                               <?php } ?>
                               <?php } ?>
                               <th width="5%" rowspan="2" style="text-align:center;">Active</th>
                               <th width="5%" class="noPrint" rowspan="2" style="text-align:center;">Action</th>
                            </tr>
                            <tr>
                                <th style="text-align:center;">Main</th>
                                <th style="text-align:center;">Godown</th>
                                <th style="text-align:center;">Total</th>
                            </tr>
                        </thead>
                        <tbody>
<?php

        $counter = 1;
        $main = 0;
        $godown = 0;
        $total_cost = 0;
        while($row = mysql_fetch_array($itemsList)){
            if(isset($items_exclude) && !in_array($row['ID'],$items_exclude)){
                continue;
            }
            $active = ($row['ACTIVE']=='Y')?"Yes":"No";
            $measureName  = $objMeasures->getName($row['MEASURE']);
            $godown_stock_array = $objGodownDetails->getItemStockAllGodownArray($row['ID']);
            $category = $objItemsCategory->getTitle($row['ITEM_CATG_ID']);
            $godownStocks = implode(' + ', $godown_stock_array);
            $godownStocks = ($godownStocks=='')?"0":$godownStocks;
            if($row['INV_TYPE']=='B'){
                $row['STOCK_QTY'] = $objMobilePurchaseDetails->get_item_stock($row['ID']);
            }
?>
            <tr id="recordPanel" data-cat-id="<?php echo $row['ITEM_CATG_ID']; ?>">
                <td style="text-align:center"><?php echo $counter; ?></td>
                <td style="text-align:center;"><?=$row['ITEM_BARCODE'];?></td>
                <td style="text-align:left"><?php echo $category; ?></td>
                <td style="text-align:left !important;padding-left:5px;"><?php echo $row['NAME']; ?></td>
                <td style="text-align:center"><?php echo $row['STOCK_QTY']; ?></td>
                <td style="text-align:center"><?php echo $godownStocks; ?></td>
                <td style="text-align:center"><?php echo $row['STOCK_QTY']+array_sum($godown_stock_array)." (".$measureName.") "; ?></td>
                <td style="text-align:center" data-min-qty="<?php echo $row['MIN_QTY']; ?>" data-item-id="<?php echo $row['ID']; ?>"><?php echo $row['MIN_QTY']; ?></td>
                <?php if($full_print){ ?>
                <td style="text-align:center"><?php echo $row['PURCHASE_PRICE']; ?></td>
                <td style="text-align:center"><?php echo $row['ITEM_DISCOUNT']; ?></td>
                <td style="text-align:center" data-price-type="s" data-item-id="<?php echo $row['ID']; ?>"><?php echo $row['SALE_PRICE']; ?></td>
                <td style="text-align:center"><?php echo $row['TOTAL_COST']; ?></td>
                <?php if($mobileAddon == 'Y'){ ?>
                <td style="text-align:center"><?php echo ($row['INV_TYPE'] == 'B')?"Yes":'No'; ?></td>
                <?php } ?>
                <?php } ?>
                <td style="text-align:center"><?php echo $active; ?></td>
                <td style="text-align:center" class="noPrint">
                    <a target="_blank" class="button btn-group btn-xs" href="item-details.php?id=<?php echo $row['ID']; ?>"><i class="fa fa-pencil"></i></a>
                </td>
            </tr>
<?php
        $counter++;
        $main += $row['STOCK_QTY'];
        $godown += $godownStocks;
        $total_cost += $row['TOTAL_COST'];
        }
?>
                        </tbody>
                        <tfoot class="tableFooter">
                            <tr>
                                <td colspan="3" style="text-align:right;" >Total</td>
                                <td style="text-align:center;"><?php echo $main; ?></td>
                                <td style="text-align:center;"><?php echo $godown; ?></td>
                                <td style="text-align:center;"><?php echo $main+$godown; ?></td>
                                <?php if($full_print){ ?>
                                <td colspan="4" style="text-align:center;"> - - - </td>
                                <td style="text-align:center;"><?php echo number_format($total_cost,2); ?></td>
                                <?php }else{ ?>
                                <td  style="text-align:center;"> - - - </td>
                                <?php } ?>
                                <td colspan="3" style="text-align:center;"> - - - </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
<?php
    }elseif(isset($itemsList)){
?>
            <h3 class="text-center">No Items Found</h3>
<?php
    }
?>
            </div> <!--End bodyTab1-->
            <div style="height:0px;clear:both"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script><?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?> tab('2', '1', '2'); <?php } ?></script>
