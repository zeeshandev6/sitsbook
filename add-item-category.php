<?php
    $out_buffer = ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/item_category.php');

    $objItemCategory  = new ItemCategory();
?>
    <!DOCTYPE html 
        >

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();


            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

<?php
        //get one record by id
        if(isset($_GET['id'])){
            if(!empty($_GET['id'])){
                $id = $_GET['id'];
                $array = $objItemCategory->getRecordDetailsByID($id);
                $id             =   $array['ITEM_CATG_ID'];
                $name           =   $array['NAME'];
                $description    =   $array['DETAIL'];
            }
        }

        if(isset($_POST['save'])){
            $id     = $_POST['id'];
            $objItemCategory->name          =   ucfirst($_POST['name']);
            $objItemCategory->description   =   mysql_real_escape_string(ucfirst($_POST['description']));

            if($id){
                $itemid = $objItemCategory->update($id);
                exit(header('location: add-item-category.php?id='.$id.'&action=updated'));
            }else{
                $idd = $objItemCategory->save();
                exit(header('location: add-item-category.php?id='.$idd.'&action=added'));
            }
        }

    ?>


    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p ><B>ItemCategory Details</B></p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="item-category-management.php"><div class="tab">List</div></a>
                                <a href="item-category-management.php"><div class="tab">Search</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Item Category updated.");}
                    elseif($_GET['action']=='added'){ successMessage("Item Category added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" class="form-horizontal">
                        <div class="col-xs-12">
                          <input type="hidden" name="id" value="<?php echo isset($_GET['id'])?$_GET['id']:0; ?>" />

                          <div class="form-group">
                            <label class="control-label col-sm-2">Category Title :</label>
                            <div class="col-sm-10">
                              <input class="form-control" name="name" value="<?php if(isset($name)){ echo $name;} ?>" required="required"/>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2">Description :</label>
                            <div class="col-sm-10">
                              <textarea style="height:60px;" class="form-control" name="description"><?php if(isset($description)){ echo $description;}  ?></textarea>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <input type="submit" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="btn btn-primary" />
                              <input type="button" value="New Form" class="btn btn-default pull-right" onclick="window.location.href='add-item-category.php'";  />
                            </div>
                          </div>
                        </div>
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>

        </div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
