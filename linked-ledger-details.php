<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/ledger_report.php';
	include 'common/classes/accounts.php';
	include 'common/classes/linked_accounts.php';


	//Permission
	if(!in_array('general-ledger',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts 	 	= new ChartOfAccounts();
	$objLedgerReport 	= new ledgerReport();
	$objLinkedAccounts  = new LinkedAccounts();
	$objConfigs      	= new Configs();

	$accountsList = $objLinkedAccounts->getAccountsList();

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
    $invoice_format = explode('_', $invoice_format);
	$invoiceSize = $invoice_format[0]; // S  L

	if($invoiceSize == 'L'){
        $invoiceFile = 'sales-invoice.php';
    }elseif($invoiceSize == 'M'){
        $invoiceFile = 'sales-invoice-duplicates.php';
    }elseif($invoiceSize == 'S'){
        $invoiceFile = 'sales-invoice-small.php';
    }

	if(isset($_GET['accountCode'])){
		$postedAccCode = $_GET['accountCode'];
		$postedAccTitle = $objAccounts->getAccountTitleByCode($postedAccCode);
		$_GET['fromDate'] =  isset($_GET['fromDate'])?$_GET['fromDate']:'';
		$postedfromDate = ($_GET['fromDate']=="")?"":date('Y-m-d',strtotime($_GET['fromDate']));
		if($postedfromDate==''){
			$thisYear = date('Y');
			$firstJanuary = '01-01';
			$firstJanuaryThisYear = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
			$postedfromDate = $firstJanuaryThisYear;
		}
		$postedtoDate = ($_GET['toDate']=="")?"":date('Y-m-d',strtotime($_GET['toDate']));
		if($postedAccCode != ''){
			$postedAccCodesArray     = $objLinkedAccounts->getCodesByCode($postedAccCode);
			$postedAccCodes     	 = implode(",", $postedAccCodesArray);
			$voucherDetailsList 	 = $objLedgerReport->getVoucherDetailListByDateRangeMultiCodes($postedfromDate,$postedtoDate,$postedAccCodes);

			$dateForBalance 		 = date('Y-m-d',strtotime($postedfromDate." - 1 days"));

			$ledgerOpeningBalance 	 = $objLedgerReport->getLedgerOpeningBalance($dateForBalance,$postedAccCodesArray['ACCOUNT_ONE']);
			$account_type 			 = substr($postedAccCodesArray['ACCOUNT_ONE'],0,2);
			if($ledgerOpeningBalance >= 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'Dr';
			}elseif($ledgerOpeningBalance < 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance >= 0){
				$ledgerOpeningBalanceType = 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance < 0){
				$ledgerOpeningBalanceType = 'Dr';
			}
			$one_ledgerOpeningBalanceSigned = $ledgerOpeningBalance;
			$one_ledgerOpeningBalance 		= str_replace('-','',$ledgerOpeningBalance);
			$one_ledgerOpeningBalanceType   = strtoupper($ledgerOpeningBalanceType);

			$ledgerOpeningBalance 	 = $objLedgerReport->getLedgerOpeningBalance($dateForBalance,$postedAccCodesArray['ACCOUNT_TWO']);
			$account_type 			 = substr($postedAccCodesArray['ACCOUNT_TWO'],0,2);
			if($ledgerOpeningBalance >= 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'Dr';
			}elseif($ledgerOpeningBalance < 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType = 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance >= 0){
				$ledgerOpeningBalanceType = 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance < 0){
				$ledgerOpeningBalanceType = 'Dr';
			}
			$two_ledgerOpeningBalanceSigned = $ledgerOpeningBalance;
			$two_ledgerOpeningBalance 		= str_replace('-','',$ledgerOpeningBalance);
			$two_ledgerOpeningBalanceType   = strtoupper($ledgerOpeningBalanceType);

			$ledgerOpeningBalanceSigned = $one_ledgerOpeningBalanceSigned - $two_ledgerOpeningBalanceSigned;
			$ledgerOpeningBalance 		= str_replace('-','',$ledgerOpeningBalanceSigned);
			if($ledgerOpeningBalanceSigned < 0){
				$ledgerOpeningBalanceType   = "CR";
			}else{
				$ledgerOpeningBalanceType   = "DR";
			}
		}else{
			$message = "Account was not selected !";
		}
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="resource/css/bootstrap-select.css" rel="stylesheet">
        <style>
			html{
			}
			.ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized{
				float:left;
				width: 152px;
				padding-left: 5px;
				border: 1px solid #CCC;
				height:30px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;
				border:1px solid #DDDDDD;

				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;

				-moz-border-radius-topleft:3px;
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;

				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;

				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			}
			.input_sized:hover{
				border-color: #9ecaed;
				box-shadow: 0 0 2px #9ecaed;
			}
		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
			$(function(){
				$( document ).tooltip({position:{'my':'left center','at':'right center'}});
			});
			$(window).on('load', function () {
				$('.selectpicker').selectpicker();
				$(".printThis").click(function(){
					var pageHeaderHeight = $(".pageHeader").height();
					var MaxHeight = 770;
					var RunningHeight = 0;
					var PageNo = 1;
					var MaxHeight_after = 0;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if(PageNo == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						if($(this).height() > 29){

						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var repoDate = $(".repoGen").text();
					$(".repoGen").remove();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						if(i == 1){
							MaxHeight = 770;
						}else{
							MaxHeight = 850;
						}
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span><div class='clear'></div><div class=\"dev-info\"> Designed &amp; Developed By <b>SIT SOLUTIONS</b> , 041-8722200, www.sitsol.net </div></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}

					$('table.tableBreak').remove();
					$(".printTable").printThis({
					  debug: false,
					  importCSS: false,
					  printContainer: true,
					  loadCSS: 'resource/css/reports.css',
					  pageTitle: "SIT Solution",
					  removeInline: false,
					  printDelay: 500,
					  header: null
				  });
				});
			});
		</script>
      	<script type = "text/javascript" src = "resource/scripts/reports.configuration.js"></script>
        <script type = "text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >General Ledger Reporting</p>
                     		<div class="clear"></div>
                  		</div><!-- End .content-box-header -->
                    	<div id = "bodyTab1">
                            <div class="clear"></div>
                            <div id="form" style="margin: 0 auto; width: 900px;">
                                <form method="get" action="">
                                    <div class="title" style="margin-bottom:20px">Search Ledger:</div>
<?php
						if(isset($message)){
?>
                                    <p style="text-align:center;color:red;"><?php echo $message; ?></p>
<?php
						}
?>
                                    <div class="caption">Account Title :</div>
                                    <div class="field" style="width:250px;position:relative;">
                                        <select class="selectpicker show-tick form-control" name="accountCode" data-style="btn-default" data-live-search="true" style="border:none">
                                               <option selected value=""></option>
<?php
                                        if(mysql_num_rows($accountsList)){
                                            while($account = mysql_fetch_array($accountsList)){
												$selected      = (isset($postedAccCode) && $postedAccCode == $account['ACCOUNT_ONE'])?"selected='selected'":"";
?>
                                               <option value="<?php echo $account['ACCOUNT_ONE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                            }
                                        }
?>
                                        </select>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="caption">Start Date:</div>
                                    <div class="field" style="width:150px">
                                        <input type="text" name="fromDate" class="form-control datepicker" style="width:145px" />
                                    </div>

                                    <div class="caption" style="margin-left:120px">End Date:</div>
                                    <div class="field" style="width:150px">
                                        <input type="text" name="toDate" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" style="width:145px" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption"></div>
                                    <div class="field" style="text-align:left">
                                        <input type="submit" value="Search" name="generalReport" class="button"/>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->
            		</div> <!-- End .content-box-content -->
<?php
	if(isset($voucherDetailsList)){
?>
        			<div class="content-box-content">
                    	<span style="float:right;"><button class="button printThis">Print</button></span>
                		<div id="bodyTab" class='printTable'   style="margin: 0 auto;">
                            <div style="text-align:left;margin-bottom:20px;" class="pageHeader">
                                <p style="font-size:18px;padding:0;text-align:center;"><?php echo $postedAccTitle; ?> (<?php echo $postedAccCode; ?>) </p>
                                <p style="font-size:12px;padding:0;text-align:center;">
                                		General Ledger Details
                                        From : <u> <?php echo date('d-m-Y',strtotime($postedfromDate)); ?></u> to : <u> <?php echo date('d-m-Y',strtotime($postedtoDate)); ?></u>
                                </p>
                                <p style="float:left; margin-left:0px; font-size:14px" class="repoGen">
                                		Report generated on : <?php echo date('d-m-Y'); ?>
                                </p>
                            </div>
                            <div style="clear:both; height:1;"></div>
                            <table style="margin:0 10px; width:98%;" cellspacing="0" class="tableBreak">
                                <thead class="tHeader">
                                    <tr>
                                       <th width="8%"  colspan="2" style="text-align:center;border:1px solid #ddd;">Ref</th>
                                       <th width="10%" style="border:1px solid #ddd;">EntryDate</th>
                                       <th width="45%" style="border:1px solid #ddd;">Description</th>
                                       <th width="10%" style="text-align:center;border:1px solid #ddd;">Debit</th>
                                       <th width="10%" style="text-align:center;border:1px solid #ddd;">Credit</th>
                                       <th width="15%" style="text-align:center;border:1px solid #ddd;">Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
						$account_type 			 = substr($postedAccCodesArray['ACCOUNT_ONE'], 0,2);
						$dateForBalance 		 = date('Y-m-d',strtotime($postedfromDate." - 1 days"));
						$closing_balance     = $ledgerOpeningBalanceSigned;
						if(mysql_num_rows($voucherDetailsList)){
							$debitTotal 		= 0;
							$creditTotal 		= 0;
							$balance_column = 0;
?>
                                	<tr>
                                    	<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
                                        <td style="text-align:right;border-right:1px solid #E5E5E5;border-left:none">Opening Balance:</td>
                                        <td colspan="2" style="border-right:none;border-left:none"></td>
                                        <td style="border-left:none;border-right:none;text-align:right"><?php echo $ledgerOpeningBalance." ".$ledgerOpeningBalanceType ?></td>
                                    </tr>
<?php
							while($row = mysql_fetch_array($voucherDetailsList)){
								if($objLedgerReport->is_reversal_vouhcer($row['VOUCHER_ID']) > 0){
									continue;
								}
								if($row['REVERSAL_ID']>0){
									continue;
								}
                if($row['TRANSACTION_TYPE']=='Dr'){
                    $debitTotal      += $row['AMOUNT'];
                    $closing_balance += $row['AMOUNT'];
                }elseif($row['TRANSACTION_TYPE']=='Cr'){
                    $creditTotal     += $row['AMOUNT'];
                    $closing_balance -= $row['AMOUNT'];
                }
                if($closing_balance < 0){
                    $closing_type = "CR";
                }else{
                    $closing_type = "DR";
                }
?>
                                    <tr>
                                       <td style="text-align:center"><?php echo $row['VOUCHER_TYPE']; ?>#</td>
                                       <td style="text-align:center"><?php echo $row['VOUCHER_NO']; ?></td>
                                       <td align="left"><?php echo date('d-m-Y',strtotime($row['VOUCHER_DATE'])); ?></td>
                                       <td align="left"><?php echo $row['NARRATION']; ?></td>
                                       <td style="text-align:right" class="debitColumn"><?php echo ($row['TRANSACTION_TYPE']=='Dr')?$row['AMOUNT']:""; ?></td>
                                       <td style="text-align:right;"  class="creditColumn"><?php echo ($row['TRANSACTION_TYPE']=='Cr')?$row['AMOUNT']:""; ?></td>
                                       <?php
                                            $display_closing_balance = str_replace('-','',(string)$closing_balance);
                                            $display_closing_balance = (float)$display_closing_balance;
                                       ?>
                                       <td style="text-align: right;"> <?php echo number_format($display_closing_balance,2)." ".$closing_type; ?></td>
                                    </tr>
<?php
							}
						}
								if(mysql_num_rows($voucherDetailsList)){
?>
                                    <tr style="background:none">
                                    	<td colspan="4" style="text-align:right;border:1px solid #e5e5e5;">Period Total:</td>
                                        <td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($debitTotal,2); ?></td>
                                        <td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($creditTotal,2); ?></td>
                                        <td style="border-left:none;text-align:right">

                                        </td>
                                    </tr>
                                    <tr>
                                    	<td colspan="4" style="text-align:right;">Closing Balance:</td>
                                        <td colspan="2" style=""></td>
                                        <td style="text-align:right" class="closingBalance">
                                        	<?php
                                        		if($closing_balance < 0){
                                        			$closing_type = "CR";
                                        		}else{
                                        			$closing_type = "DR";
                                        		}
                                        		$closing_balance = str_replace('-','',$closing_balance);
                                        	?>
                                        	<?php echo number_format($closing_balance,2)." ".$closing_type; ?>
                                        </td>
                                    </tr>
<?php
								}
?>
                                </tbody>
                            </table>
                    	</div><!--bodyTab2-->
<?php
                           	}
?>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
        	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(window).load(function(){
		$("button.dropdown-toggle").focus();
		$("ul.selectpicker li a").click(function(){
			var accountTitle = $(this).children("span.text").text();
			if(accountTitle!==""){
				$("select.selectpicker option").each(function(index, element){
					if($(this).text()==accountTitle){
						var accountsCode = $(this).val();
						$("input.insertAccTitle").val(accountTitle);
						$("input.insertAccCode").val(accountsCode);
					}
				});
			}else{
				$("input.insertAccTitle").val("");
				$("input.insertAccCode").val("");
			}
		});
	});
</script>
