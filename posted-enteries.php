<?php
include 'common/connection.php';
include 'common/config.php';
include 'common/classes/posted-enteries.php';
include 'common/classes/j-voucher.php';
include 'msgs.php';

$objpostedEnteries = new postedEnteries();
$objJournalVoucher = new JournalVoucher();
$today = date('Y-m-d');

if(isset($_GET['delete_voucher'])){
	$voucher_id = (int)$_GET['delete_voucher'];
	$objJournalVoucher->reverseVoucherDetails($voucher_id);
	$objJournalVoucher->deleteJv($voucher_id);
	exit();
}
if(isset($_GET['postVoucher'])){
	$voucherId = (int)$_GET['voucherId'];
	$status 	 = 'P';
	$objpostedEnteries->updateVoucherStatus($voucherId,$status);
	echo "<script>window.location.href = 'posted-enteries.php'</script>";
}
$toDate = date('d-m-Y');
if(isset($_GET['search'])){
	$fromJv = $_GET['fromJv'];
	$toJv = $_GET['toJv'];
	$fromDate = ($_GET['fromDate']=="")?"":date('Y-m-d',strtotime($_GET['fromDate']));
	if($fromDate == ''){
		$fromDate = date('Y-01-01');
	}
	$v_type = mysql_real_escape_string($_GET['v_type']);

	$toDate = ($_GET['toDate']=="")?"":date('Y-m-d',strtotime($_GET['toDate']));
	$postedList = $objpostedEnteries->getVoucherListByDateRange($fromDate,$toDate,$fromJv,$toJv,$v_type);
}
$reversable_voucher_types   = array();
$reversable_voucher_types[] = 'JV';
$reversable_voucher_types[] = 'CB';
$reversable_voucher_types[] = 'CR';
$reversable_voucher_types[] = 'CP';
$reversable_voucher_types[] = 'BR';
$reversable_voucher_types[] = 'BP';

$drCrDiffer = 0;
?>
<!DOCTYPE html 
>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link href="resource/css/bootstrap.min.css" rel="stylesheet">
	<link href="resource/css/bootstrap-select.css" rel="stylesheet">
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.form-controld{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.form-controld:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	.content-box-content{
		transition: height 200ms;
	}
	table th,table td{
		font-size: 14px !important;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load', function(){
		$('.selectpicker').selectpicker();
	});
	</script>
</head>
<body>
	<div id = "body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div id = "bodyWrapper">
			<div class = "content-box-top" style="overflow:visible;">
				<div class = "summery_body">
					<div class = "content-box-header">
						<p style="font-size:18px;"> <i class="fa fa-search text-info"></i> Search Vouchers</p>
						<div class="clear"></div>
					</div><!--.content-box-header-->
					<div id = "bodyTab1">
						<div id = "form">
							<div class="clear"></div>

							<form method="get" action="">
								<div class="caption">Voucher Type:</div>
								<div class="field" style="width:160px;position:relative;">
									<select name="v_type">
										<option value="">All</option>
										<option value="JV">Journal Voucher</option>
										<option value="CB">Cash Book</option>
										<option value="CR">Cash Receipt</option>
										<option value="CP">Cash Payment</option>
										<option value="BR">Bank Receipt</option>
										<option value="BP">Bank Payment</option>
										<option value="PV">Purchase Voucher</option>
										<option value="PR">Purchase Return Voucher</option>
										<option value="SV">Sale Voucher</option>
										<option value="SR">Sale Return Voucher</option>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption">From JV# :</div>
								<div class="field" style="width:160px;position:relative;">
									<input type="text" name="fromJv" class="form-control" style="width:145px;" value="" />
								</div>
								<div class="caption" style="width: 100px;">To JV# :</div>
								<div class="field" style="width:100px;position:relative;">
									<input type="text" name="toJv" class="form-control" style="width:145px;" value="" />
								</div>
								<div class="clear"></div>
								<div class="caption">From Date :</div>
								<div class="field" style="width:160px;position:relative;">
									<input type="text" name="fromDate" class="form-control datepicker" style="width:145px;" value="<?php echo (isset($fromDate))?date('d-m-Y',strtotime($fromDate)):date('01-01-Y'); ?>" />
								</div>
								<div class="caption" style="width:100px;">To Date :</div>
								<div class="field" style="width:150px;position:relative;">
									<input type="text" name="toDate" class="form-control datepicker" style="width:145px;" value="<?php echo date('d-m-Y',strtotime($toDate)); ?>" />
								</div>
								<div class="field" style="margin-left: 20px;">
									<button type="submit" name="search" value="Search" class="btn btn-default"><i class="fa fa-search"></i> Search</button>
								</div>
							</form>
							<div class="clear"></div>
							<hr />
							<?php
							if(isset($postedList) && mysql_num_rows($postedList)){
								while($postedRow = mysql_fetch_array($postedList)){
									$vId = $postedRow['VOUCHER_ID'];
									$voucherDetails = $objpostedEnteries->getVoucherDetailList($vId);
									?>
									<div class="voucherBox" style="position: relative;">
										<div class="voucherHeaders" class="height: 50px;">
											<?php
											//if(!$objpostedEnteries->voucherFromRegisters($postedRow['VOUCHER_ID'])){
											if(false){
												?>
												<div class="caption" style="width: 20px;">
													<input type="checkbox" class="superBox" value="<?php echo $postedRow['VOUCHER_ID']; ?>" />
												</div>
												<?php
											}
											?>
											<div class="caption" style="width: 150px;">
												Voucher Type : <b class="text-primary"><?php echo $postedRow['VOUCHER_TYPE']; ?></b>
											</div>
											<div class="caption text-center" style="width: 200px;">
												Voucher # : <b class="text-primary"><?php echo $postedRow['VOUCHER_NO']; ?></b>
											</div>
											<div class="caption" style="width: 190px;">
												Voucher Date : <b class="text-primary"><?php echo date('d-m-Y',strtotime($postedRow['VOUCHER_DATE'])) ?></b>
											</div>
											<div class="caption" style="width: 170px;">
												<b class="text-success"><?php echo ($postedRow['PO_NO']=='')?"":" PO # : ".$postedRow['PO_NO']; ?></b>
											</div>
											<div class="pull-right alternative text-right">
											<?php
												if(in_array('search-vouchers-reversal',$permissionz)||$admin){
													if(in_array($postedRow['VOUCHER_TYPE'],$reversable_voucher_types)&&$postedRow['REVERSAL_ID']==0){
														?>
														<button class="btn btn-default btn-sm reversal" data-vid="<?php echo $postedRow['VOUCHER_ID']; ?>"> <i class="fa fa-reply"></i> Reverse </button>
														|
														<?php
													}
												}
												if($postedRow['VOUCHER_TYPE'] == 'CB'){
													$voucher_form_url = 'cash-book-voucher.php';
												}else{
													$voucher_form_url = 'create-voucher.php';
												}
												?>
												<?php /* <a href="#" onclick="del_voucher(<?php echo $postedRow['VOUCHER_ID']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-times"></i></a> */ ?>
												<a class="btn btn-primary btn-sm btn-sm printStart" target="_blank" href="voucher-print.php?id=<?php echo $postedRow['VOUCHER_ID']; ?>"><i class="fa fa-print"></i> Print</a>
											<?php
											if(in_array('search-vouchers-modify',$permissionz)||$admin){
												if(in_array($postedRow['VOUCHER_TYPE'],$reversable_voucher_types)&&$postedRow['REVERSAL_ID']==0){
											?>
												<a class="btn btn-primary btn-sm btn-sm" target="_blank" href="<?php echo $voucher_form_url; ?>?jid=<?php echo $postedRow['VOUCHER_ID']; ?>"><i class="fa fa-pencil"></i></a>
											<?php
												}
											}
											?>
											</div>
											<div class="clear"></div>
										</div><!--voucherHeaders-->

										<div class="clear"></div>
										<table cellspacing = "0" align = "center" style="width:100%;" class="removeableTable" data-id='<?php echo $postedRow['VOUCHER_ID']; ?>'>

											<?php
											if(mysql_num_rows($voucherDetails)){
												$debitTotal = 0;
												$creditTotal = 0;
												?>
												<thead class="tHeader">
													<tr>
														<th width = "8%" class="text-center">A/C Code</th>
														<th width = "20%" class="text-center">A/C Title</th>
														<th width = "40%" class="text-center">Narrtion</th>
														<th width = "8%" class="text-center">Debit</th>
														<th width = "8%" class="text-center">Credit</th>
													</tr>
												</thead>
												<tbody>
													<tr class="transactions" style="display:none;"></tr>
													<?php
													while($row = mysql_fetch_array($voucherDetails)){
														?>
														<tr class="transactions">
															<td class="text-center"><?php echo $row['ACCOUNT_CODE']; ?></td>
															<td class="text-left"><?php echo substr($row['ACCOUNT_TITLE'],0,40); ?></td>
															<td style="text-align:left;font-size: 10px !important;"><?php echo $row['NARRATION']; ?></td>
															<td style="text-align:right" class="debitColumn"><?php echo ($row['TRANSACTION_TYPE']=='Dr')?number_format($row['AMOUNT'],2):""; ?></td>
															<td style="text-align:right" class="creditColumn"><?php echo ($row['TRANSACTION_TYPE']=='Cr')?number_format($row['AMOUNT'],2):""; ?></td>
														</tr>
														<?php
														$debitTotal += ($row['TRANSACTION_TYPE']=='Dr')?$row['AMOUNT']:0;
														$creditTotal += ($row['TRANSACTION_TYPE']=='Cr')?$row['AMOUNT']:0;
													}
													?>
													<tr style="background-color:#F8F8F8;height: 30px;">
														<td colspan="3" style="text-align:center;"> Total </td>
														<td class="debitTotal" style="text-align:right;color:#042377;" title="Debit"><?php echo (isset($debitTotal))?number_format($debitTotal,2):"0"; ?></td>
														<td class="creditTotal" style="text-align:right;color:#BD0A0D;" title="Credit"><?php echo (isset($creditTotal))?number_format($creditTotal,2):"0"; ?></td>
														<?php
														if(isset($creditTotal)&&isset($debitTotal)){
															$drCrDiffer = $debitTotal - $creditTotal;
														}else{
															$drCrDiffer = 0;
														}
														?>
													</tr>
													<?php
												}else{
													?>
													<tr>
														<td style="text-align: center;" colspan="10">
															<?php errorMessage('Voucher is blank, No transactions.'); ?>
														</td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
										<div class="clear"></div>

										<?php
											if($drCrDiffer!=0){
												$drCrDiffer = str_ireplace('-','',$drCrDiffer);
												warningMessage('Voucher not balanced. Difference : '.$drCrDiffer);
											}
										?>

									</div><!--voucherBox--->
									<div style="height:30px;"></div>
									<?php
								}

							}
							?>
							<div class="clear"></div>
						</div><!--form-->
						<div class="clear"></div>
					</div>
				</div>     <!-- End summer -->
			</div>   <!-- End .content-box-top -->
		</div>  <!--body-wrapper-->
		<div id="fade"></div>
		<div id="xfade"></div>
		<input type="hidden" value="<?php echo date('d-m-Y'); ?>" class="repoGen" />
	</div>
</body>
</html>
<script type="text/javascript">
$(function(){
	$(".editVoucher").hide();
	$(".showWhenPicked").hide();
});
$(window).load(function(){
	$("select[name='v_type']").selectpicker();
	$(".checkAll").click(function(){
		$(".voucher").prop('checked',true);
	});
	$("form").submit(function(e){
		$("#xfade").fadeIn();
	});
	$(".uncheckAll").click(function(){
		$(".voucher").prop('checked',false);
	});
	$(".postEm").click(function(){
		var voucherList = '';
		if($(".voucher").length){
			$(".voucher").each(function(index, element) {
				if($(this).prop('checked')){
					var voucher_id = $(this).attr('data-id');
					voucherList += voucher_id+",";
				}
			});
			if(voucherList !== ''){
				$.post('db/voucherPost.php',{v:voucherList},function(data){
					window.location.reload();
				});
			}
		}
	});
	$(".reversal").click(function(){
		var id = parseInt($(this).attr("data-vid"))||0;
		if(id == 0){
			return false;
		}
		var this_button = $(this);
		$(this_button).hide();
		$.post("db/reverse-voucher.php",{id:id},function(data){
			data = $.parseJSON(data);
			if(data['DONE']=='N'){
				$(this_button).show();
			}
		});
	});
	//functions for calculations
	$.fn.sumColumn = function(showTotal){
		var totalThaans = 0;
		$(this).each(function() {
			var rowVal = parseFloat($(this).text())||0;
			totalThaans += rowVal;
			$(showTotal).text(totalThaans);
		});
	};

	$.fn.showDifferenceBetween = function(Elm1,Elm2){
		if($(Elm1).text()!==""&&$(Elm2).text()){
			var debitColumn = parseFloat($(Elm1).text()|| 0);
			var creditColumn = parseFloat($(Elm2).text()|| 0);
			if(debitColumn>creditColumn){
				$(this).text(debitColumn-creditColumn);
				var difference = debitColumn-creditColumn;
			}else{
				$(this).text(creditColumn-debitColumn);
				var difference = debitColumn-creditColumn;
			}
			var tdBgColor = $("td.drCrDiffer").css('background-color');
			var tdForeColor = $("td.drCrDiffer").css('color');
			if(difference<0||difference>0){
				$(".editVoucher").click(function(){
					window.location.href = 'create-voucher.php?jid='+$(".voucherId").val();
				});
				$("input[type='submit']").hide();
				$("td.drCrDiffer").animate({'background-color':'#e77711','color':'#FFF'});
				$(".editVoucher").show();
			}else{
				$("input[type='submit']").show();
				$("td.drCrDiffer").css({'background-color':'#e77711','color':'#FFF'});
			}
		}
	};
});
var deleteVouchers = function(){
	var id_list = '';
	$(".superBox:checked").each(function(index, element) {
		if(index > 0){
			id_list += ',';
		}
		id_list += $(this).val();
	});
	var clickedDel = $(this);
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><a class='dodelete button'>Confirm</a><a class='nodelete button'>Cancel</a></div>");
	$("#popUpDel").centerThisDiv();
	$("#popUpDel").hide();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
		$.post('db/del-voucher.php',{v:id_list},function(data){
			data = $.parseJSON(data);
			$(".confirm").html(data['MSG']);
			$(".dodelete").hide();
			$(".nodelete").text('Close');
			if(data['DONE'] == 'Y'){
				$(".superBox:checked").parent().parent().parent('.voucherBox').remove();
			}
		});
	});
	$(".nodelete").click(function(){
		$("#fade").fadeOut();
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
	$(".close_popup").click(function(){
		$("#fade").fadeOut('fast');
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
}

var printStart = function(thisElement){
	var voucher_id = parseInt($(thisElement).attr('data-id'))||0;
	window.location.href = 'voucher-print.php?id='+voucher_id;
	return false;

	var printBox = $(thisElement).parent().parent().parent();
	var currentTable = printBox.find('table.removeableTable');
	$(thisElement).siblings().remove();
	$(".voucherHeaders").height(40);
	var pageHeaderHeight = ($(".voucherHeaders").length)?$(".voucherHeaders").height():0;
	var MaxHeight = 920;
	var RunningHeight = 0;
	var PageNo = 1;
	var MaxHeight_after = 0;
	//Sum Table Rows (tr) height Count Number Of pages
	printBox.find('table>tbody>tr').each(function(){
		if (RunningHeight + $(this).height() > MaxHeight){
			RunningHeight = 0;
			PageNo += 1;
		}
		if($(this).height() > 29){

		}
		RunningHeight += $(this).height();
		//store page number in attribute of tr
		$(this).attr("data-page-no", PageNo);
	});
	//Store Table thead/tfoot html to a variable
	var voucherHead = printBox.find(".voucherHeaders").html();
	var tableHeader = printBox.find(".tHeader").html();
	var repoDate = $(".repoGen").val();
	$(".repoGen").remove();
	//remove previous thead/tfoot
	printBox.find(".tHeader").remove();
	printBox.find(".voucherHeaders").remove();
	//Append .tablePage Div containing Tables with data.
	for(i = 1; i <= PageNo; i++){
		printBox.append("<div class='tablePage'><div class='voucherHeaders'>"+voucherHead+"</div><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><span class=\"repoGen\">"+repoDate+"</span><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
		//get trs by pagenumber stored in attribute
		var rows = $('table tr[data-page-no="' + i + '"]');
		$('#Table' + i).find("thead").append(tableHeader);
		$('#Table' + i).find("tbody").append(rows);
	}
	printBox.find(".tablePage").find(".voucherHeaders").height(30);
	currentTable.remove();

	$(printBox).printThis({
		debug: false,
		importCSS: false,
		printContainer: true,
		loadCSS: 'resource/css/reports.css',
		pageTitle: "Sitsbook.com",
		removeInline: false,
		printDelay: 500,
		header: null
	});
}
var del_voucher = function(voucher_id){
	$.get("posted-enteries.php",{delete_voucher:voucher_id},function(data){
		window.location.reload();
	});
}
</script>
<?php include('conn.close.php'); ?>
