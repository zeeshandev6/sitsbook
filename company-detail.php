<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/company_details.php');
	include('lib/thumbnail.php');

	$objCompanyDetails = new CompanyDetails();

	$path = 'uploads/logo/';
	$message = '';

  if(isset($_POST['delete_profile'])){
      $id = (int)(mysql_real_escape_string($_POST['delete_profile']));
      $deleted = $objCompanyDetails->delete($id);
			echo ($deleted)?"Y":"N";
      exit();
  }
  if(isset($_POST['active'])){
      $id = (int)(mysql_real_escape_string($_POST['id']));
      $status = mysql_real_escape_string($_POST['active']);
      $objCompanyDetails->setAllInActive();
      $objCompanyDetails->setActive($id,$status);
      exit();
  }
	if(isset($_POST['save'])){
		$company_id = ($_POST['id'] >= 0)?$_POST['id']:0;
		$objCompanyDetails->name    			 = mysql_real_escape_string($_POST['name']);
		$objCompanyDetails->urdu_name    	 = mysql_real_escape_string($_POST['urdu_name']);
		$objCompanyDetails->urdu_address 	 = mysql_real_escape_string($_POST['urdu_address']);
		$objCompanyDetails->name    			 = mysql_real_escape_string($_POST['name']);
		$objCompanyDetails->address 			 = mysql_real_escape_string($_POST['address']);
		$objCompanyDetails->contact 			 = mysql_real_escape_string($_POST['contact']);
		$objCompanyDetails->email   			 = mysql_real_escape_string($_POST['email']);
		$objCompanyDetails->slogan  			 = mysql_real_escape_string($_POST['slogan']);
		$objCompanyDetails->tax_reg_no 		 = mysql_real_escape_string($_POST['tax_reg_no']);
		$objCompanyDetails->ntn_reg_no 		 = mysql_real_escape_string($_POST['ntn_reg_no']);

		if($company_id > 0){
			$inserted = $objCompanyDetails->update($company_id);
			if(!$inserted){
				$company_id = 0;
			}
		}else{
			$inserted = $objCompanyDetails->save();
			$company_id = mysql_insert_id();
		}

		$image_name_before = $objCompanyDetails->getLogoById($company_id);

		if($_FILES['image']['error'] == 0){
			$file_extension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
			$file_dimensions = getimagesize($_FILES['image']['tmp_name']); // in bytes
			$imageWidth = $file_dimensions[0];
			$imageHeight = $file_dimensions[1];
			$file_types = array();
			$file_types[] = 'jpg';$file_types[] = 'gif';$file_types[] = 'png';
			if(in_array($file_extension, $file_types)){
				if($company_id){
					if($image_name_before != ''){
						if(file_exists($path.$image_name_before))
						{
							unlink($path.$image_name_before);
						}
					}
					$renaming_mask = md5(date("ymd").time()).".".$file_extension;
					move_uploaded_file($_FILES['image']['tmp_name'],$path.$renaming_mask);
					chmod($path.$renaming_mask,0644);
					if($file_extension == 'jpg'){
						make_thumb_jpg($path.$renaming_mask,$path.$renaming_mask,207);
					}elseif($file_extension == 'png'){
						make_thumb_png($path.$renaming_mask,$path.$renaming_mask,207);
					}
					$objCompanyDetails->setLogo($company_id, $renaming_mask);
				}
			}else{
				$message = 'Image type not supported!';
			}
		}else{

		}
	}

  $companyDetail   = NULL;
  if(isset($_GET['id'])){
      $id = (int)(mysql_real_escape_string($_GET['id']));
      $companyDetail = $objCompanyDetails->getRecordDetails($id);
  }
  $profile_list    = $objCompanyDetails->getList();
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
            #form{
                position: relative;
            }
            #form .seprator{
                position: absolute;
                right: 0px;
                top: 5px;
                height: 100%;
                width: 1px;
                background: #06C;
                opacity: 0.4;
            }
            .caption{
                width: 70px !important;
            }
            .field{
                width: 450px !important;
            }
            .panel2{
                float:left;
                width: 200px;
                border: 1px solid #666;
                box-shadow:0px 0px 5px 0px #999;
                height:auto !important;
                text-align:center;
                margin: 5px;
                margin-right: 0px;
                position:relative;
            }
            .panel2 h3{
                font-size: 16px;
                padding: 10px 5px;
                background-color: #06C;
                color: #FFF;
                font-weight: bold;
                -moz-transition: all 200ms ease-in;
                -webkit-transition: all 200ms ease-in;
                transition: all 200ms ease-in;
            }
            .panel2:hover h3{
                transform: translateY(-45px);
                -moz-transform: translateY(-45px);
                -webkit-transform: translateY(-45px);
            }
            .panel2 .panelIcon{
                max-width: 100%;
                width: auto !important;
                height: auto !important;
            }
            .panel2 .actions{
                position: absolute;
                width:100%;
                min-height: 40px;
                bottom: 0px;
                left:0px;
                text-align:center;
                -moz-transition: all 200ms ease-in;
                -webkit-transition: all 200ms ease-in;
                transition: all 200ms ease-in;
                background: #FFF;
                padding: 10px 0px;
                visibility: hidden;
                opacity:0.0;
            }
            .panel2:hover .actions{
                visibility: visible;
                opacity:1.0;
            }
            .panel2 .css-label{
                position:absolute;
                top: 5px;
                right: 5px;
            }
            .logo_viewer{
                position:relative;
            }
        </style>
      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".view_profile").click(function(){
                    window.location.href = "company-detail.php?id="+$(this).attr('data-id');
                });
                $("input.profile_active").change(function(){
                    var id     = parseInt($(this).attr("data-id"))||0;
                    var status = 'N';
                    if($(this).is(":checked")){
                        status = 'Y';
                        $("input.profile_active").prop('checked',false);
                        $(this).prop('checked',true);
                    }
                    $.post('company-detail.php',{id:id,active:status},function(data){

                    });
                });
								$(".del_profile").click(function(){
									var id = parseInt($(this).attr('data-id'))||0;
									if(id>0){
										var clickedDel = $(this);
										$("#fade").hide();
										$("#popUpDel").remove();
										$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
										$("#popUpDel").hide();
										$("#popUpDel").centerThisDiv();
										$("#fade").fadeIn('slow');
										$("#popUpDel").fadeIn();
										$(".dodelete").click(function(){
												$.post("company-detail.php", {delete_profile : id}, function(data){
													if(data == 'Y'){
														$("#panel-block-"+id).remove();
														$("#fade").fadeOut();
														$("#popUpDel").fadeOut(function(){
															$("#popUpDel").remove();
														});
													}
												});
											});
										$(".nodelete").click(function(){
											$("#fade").fadeOut();
											$("#popUpDel").fadeOut(function(){
												$("#popUpDel").remove();
											});
										});
										$(".close_popup").click(function(){
											$("#popUpDel").fadeOut(function(){
												$("#popUpDel").remove();
											});
											$("#fade").fadeOut('fast');
										});
									}
								});
            });
        </script>
   </head>
   <body>
        <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Company Profile</p>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
	               	<div class="clear"></div>
               		<div id = "bodyTab1">
                  		<div class="col-sm-12 col-md-6 mt-30">
<?php
						if($message != ''){
?>
							<div class="alert alert-danger" role="alert"><?php echo $message; ?></div>
<?php
						}
?>
                     	<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
												<div class="form-group">
											    <label class="control-label col-sm-3">Title :</label>
											    <div class="col-sm-8">
											      <input name="name" id="title" value="<?php echo (isset($companyDetail))?$companyDetail['NAME']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Title (Urdu) :</label>
											    <div class="col-sm-8">
											      <input name="urdu_name" dir="rtl" id="urdu_name" value="<?php echo (isset($companyDetail))?$companyDetail['URDU_NAME']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Slogan :</label>
											    <div class="col-sm-8">
														<input name="slogan" id="slogan" value="<?php echo (isset($companyDetail))?$companyDetail['SLOGAN']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">ST Reg :</label>
											    <div class="col-sm-8">
														<input name="tax_reg_no" value="<?php echo (isset($companyDetail))?$companyDetail['TAX_REG_NO']:""; ?>" type="text" class="form-control" />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">NTN # :</label>
											    <div class="col-sm-8">
														<input name="ntn_reg_no" value="<?php echo (isset($companyDetail))?$companyDetail['NTN_REG_NO']:""; ?>" type="text" class="form-control" />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Address :</label>
											    <div class="col-sm-8">
														<input name="address" value="<?php echo (isset($companyDetail))?$companyDetail['ADDRESS']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Address (Urdu) :</label>
											    <div class="col-sm-8">
														<input name="urdu_address" dir="rtl" value="<?php echo (isset($companyDetail))?$companyDetail['URDU_ADDRESS']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Contact :</label>
											    <div class="col-sm-8">
														<input name="contact" value="<?php echo (isset($companyDetail))?$companyDetail['CONTACT']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Email :</label>
											    <div class="col-sm-8">
														<input name="email" value="<?php echo (isset($companyDetail))?$companyDetail['EMAIL']:""; ?>" type="text" class="form-control"  />
											    </div>
											  </div>
												<div class="form-group">
											    <label class="control-label col-sm-3">Logo :</label>
											    <div class="col-sm-8">
														<input name="image" value="<?php echo (isset($companyDetail))?$companyDetail['LOGO']:""; ?>" type="file" accept="image/*" class="pull-left btn btn-default" />
											    </div>
											  </div>
<?php
						if(isset($companyDetail)){
							if($companyDetail['LOGO'] == ''){
								$companyDetail['LOGO'] = 'default.png';
							}
?>
												<div class="form-group">
													<label class="control-label col-sm-3">Logo :</label>
													<div class="col-sm-8">
														<img class="thumbnail" src="<?php echo $path.$companyDetail['LOGO']; ?>" >
													</div>
												</div>
<?php
						}

?>
												<div class="form-group">
													<label class="control-label col-sm-3"></label>
													<div class="col-sm-8">
														<input type="hidden" name="id" value="<?php echo (isset($companyDetail['ID'])&&$companyDetail['ID']!=NULL)?$companyDetail['ID']:0; ?>"  />
														<input name="save" type="submit" value="<?php echo isset($companyDetail)?"Update":"Save"; ?>" class="btn btn-primary" />
														<?php if((int)($companyDetail['ID'])>0){ ?>
														<a href="company-detail.php" class="btn btn-default pull-right">New Profile</a>
														<?php } ?>
													</div>
												</div>
                     	</form>
                  		</div><!--form-->

                      <div class="col-sm-12 col-md-6  mt-30">
                          <?php
                              if(mysql_num_rows($profile_list)){
                                  while($row = mysql_fetch_assoc($profile_list)){
                               ?>
                                  <div class="panel2" id="panel-block-<?php echo $row['ID']; ?>">
                                      <img src="uploads/logo/<?php echo $row['LOGO'] ?>" class="panelIcon" />
                                      <h3><?php echo $row['NAME'] ?></h3>
                                      <input id="cmn-toggle-out<?php echo $row['ID']; ?>" data-id="<?php echo $row['ID']; ?>" <?php echo ($row['PROFILE_ACTIVE']=='Y')?"checked":""; ?> class="css-checkbox profile_active" type="checkbox">
                                      <label for="cmn-toggle-out<?php echo $row['ID']; ?>" class="css-label"></label>
                                      <div class="actions">
                                          <button type="button" data-id="<?php echo $row['ID'] ?>" class="btn btn-primary btn-sm btn-sm view_profile"> <i class="fa fa-pencil"></i> Edit </button>
                                          <button type="button" data-id="<?php echo $row['ID'] ?>" class="btn btn-danger btn-sm del_profile"> <i class="fa fa-times"></i></button>
                                      </div>
                                  </div>
                               <?php
                                  }
                              }
                          ?>
                      </div>
                  		<div class="clear"></div>
               		</div><!--End bodyTab1-->
            	</div><!-- End summery_body -->
         	</div><!-- End content-box -->
      	</div><!--End body-wrapper-->
        <div id="xfade"></div>
				<div id="fade"></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
