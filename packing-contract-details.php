<?php
	ob_start();
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/item_category.php');
	include('common/classes/items.php');
	include('common/classes/customers.php');
	include('common/classes/suppliers.php');
	include('common/classes/brokers.php');
	include('common/classes/branches.php');
	include('common/classes/branch_stock.php');
	include('common/classes/lot_details.php');
	include('common/classes/j-voucher.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/packing_contracts.php');
	include('common/classes/packing_contract_details.php');

	//Permission
  if(!in_array('packing-contract',$permissionz) && $admin != true){
	  echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
	  echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
	  echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
	  echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
	  echo '</div>';
	  exit();
  }
  //Permission ---END--

	$objChartOfAccounts  		= new ChartOfAccounts();
	$objItemCategory        	= new ItemCategory();
	$objItems               	= new Items();
	$objCustomer            	= new Customers();
	$objSupplier        		= new Suppliers();
	$objBrokers    		  		= new Brokers();
	$objUserAccount         	= new UserAccounts();
	$objBranches  	   			= new Branches();
	$objBranchStock    			= new BranchStock();
	$objLotDetails     	 		= new LotDetails();
	$objJournalVoucher 			= new JournalVoucher();
	$objFabricContracts 		= new FabricContracts();
	$objPackingContracts     	= new PackingContracts();
	$objPackingContractDetails  = new PackingContractDetails();

	$accountsList 	= $objJournalVoucher->getAccountsList();
	$supplier_list 	= $objSupplier->getList();
	$broker_list 		= $objBrokers->getList();

	$fabric_id 				  		 = 0;
	$fabric_inward_row  		 = NULL;
	$fabric_contract_details = NULL;

	//save and update
	if(isset($_POST['lot_no'])){
		$fabric_id     								  					= (int)(isset($_POST['fabric_id'])?$_POST['fabric_id']:0);

		$objPackingContracts->user_id    				= (int)$_SESSION['classuseid'];
		$objPackingContracts->lot_no      				= mysql_real_escape_string($_POST['lot_no']);
		$objPackingContracts->supplier_id      	 	 	= mysql_real_escape_string($_POST['supplier_id']);
		$objPackingContracts->broker_id      		  	= mysql_real_escape_string($_POST['broker_id']);
		$objPackingContracts->contract_date       		= date('Y-m-d',strtotime($_POST['contract_date']));
		$objPackingContracts->delivery_date       		= (isset($_POST['delivery_date']))?date('Y-m-d',strtotime($_POST['delivery_date'])):"0000-00-00";
		$objPackingContracts->broker_commission   		= mysql_real_escape_string($_POST['broker_commission']);
		$objPackingContracts->measure  					= mysql_real_escape_string($_POST['measure']);
		$objPackingContracts->rate  					= mysql_real_escape_string($_POST['rate']);
		$objPackingContracts->payment_schedule    		= mysql_real_escape_string($_POST['payment_schedule']);
		$objPackingContracts->dyeing_unit_lot_no  		= mysql_real_escape_string($_POST['dyeing_unit_lot_no']);

		if($fabric_id > 0){
			$update = $objPackingContracts->update($fabric_id);
			$action = "U";
		}else{
			$fabric_id = $objPackingContracts->save();
			$action = "S";
		}
		if($fabric_id){
			$arrPro_table = json_decode($_POST['details'], true);
			$rows_deleted = (isset($_POST['deleted_rows']))?$_POST['deleted_rows']:array();

			if(is_array($rows_deleted)){
				foreach($rows_deleted as $key => $id){
					$deleted = $objPackingContractDetails->delete($id);
				}
			}
			$row_id = 0;
			$fabric_amount = 0;

			$objPackingContractDetails->fabric_id = $fabric_id;
			foreach($arrPro_table as $row_key => $row){
				$row_id                                   	= (int)$row['row_id'];
				$objPackingContractDetails->construction  	= mysql_real_escape_string($row['fabric_type']);
				$objPackingContractDetails->variety    		= mysql_real_escape_string($row['variety']);
				$objPackingContractDetails->thaan   	  	= mysql_real_escape_string($row['thaan']);
				$objPackingContractDetails->quantity   		= mysql_real_escape_string($row['quantity']);
				$objPackingContractDetails->rate   			= mysql_real_escape_string($row['rate']);
				$objPackingContractDetails->amount   		= mysql_real_escape_string($row['amount']);

				if($row_id>0){
					$record_state = $objPackingContractDetails->update($row_id);
				}else{
					$record_state = $objPackingContractDetails->save();
				}
			}
		}

		if($fabric_id>0){
			if($action == 'S'){
				exit(header('location: packing-contract-details.php?id='.$fabric_id.'&action=added'));
			}elseif($action == 'U'){
				exit(header('location: packing-contract-details.php?id='.$fabric_id.'&action=updated'));
			}
		}
		exit();
	}

	if(isset($_GET['lot_no'])){
		$lot_no = (int)mysql_real_escape_string($_GET['lot_no']);
	}

	if(isset($_GET['id'])){
		$fabric_id 				   = (int)(mysql_real_escape_string($_GET['id']));
		$fabric_inward_row 		   = $objPackingContracts->getDetail($fabric_id);
		$lot_no    						   = $fabric_inward_row['LOT_NO'];

		$fabric_contract_list    = $objPackingContractDetails->getListById($fabric_id);
	}
	$fabric_contract_query   = $objFabricContracts->getListNoVoucherByLotNo($lot_no);
	if(mysql_num_rows($fabric_contract_query)){
		$fabric_contract_details = mysql_fetch_assoc($fabric_contract_query);
	}
	$fabric_array 	   = array();
	$fabric_array['F'] = 'Fabric';
	$fabric_array['B'] = 'B.Grade';
	$fabric_array['C'] = 'C.P';
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style media="screen">
		table#pro_table td,table#pro_table th{
			padding: 5px !important;
			font-size: 14px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/packing.contract.config.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select").selectpicker();
			$(".quickSubmit input[type=text]").addClass("text-center");
			$("table").change(function(){
				$("#view_button").parent().addClass("text-center");
			});
			$(".reload_item").click(function(){
				$.get('packing-contract-list.php',{},function(data){
					$("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
					$("select.itemSelector1").html($(data).find("select.itemSelector1").html()).selectpicker('refresh');
				});
			});
			$("input.thaan,input.rate").on('change keyup',function(e){
				var qty  = parseFloat($("input.thaan").val())||0;
				var rate = parseFloat($("input.rate").val())||0;
				$("input.amount").val(qty*rate);
			});
		});
	</script>
</head>
<body>
	<div id="body-wrapper">
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
			<div class="content-box-top">
				<div class="content-box-header">
					<p>Packing Contract</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
							<a href="lot-details.php?tab=search"><div class="tab">Search</div></a>
							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="clear"></div>

				<?php include('lot.tabs.inc.php'); ?>
				<div class="clear"></div>

				<div class = "content-box-header" style="margin-top: 0px !important;">
					<span id="tabPanel" class="pull-left" style="margin-left: 70px;">
						<div class="tabPanel">
							<a href="packing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="tab">List</div></a>
							<a href="packing-contract-details.php?lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Form</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('packing-contract-list.php'); ?>&rf=<?php echo base64_encode('packing-contract-details.php'); ?>&io_status=i&typee=pk&lot_no=<?php echo $lot_no; ?>"><div class="tab">Inward</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('packing-contract-list.php'); ?>&rf=<?php echo base64_encode('packing-contract-details.php'); ?>&io_status=o&typee=pk&lot_no=<?php echo $lot_no; ?>"><div class="tab">Outward</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!-- End .content-box-header -->

				<div class="content-box-content">
					<div id="bodyTab1">
						<div id="form">
							<form method="post" action="" name="add-history">
								<div class="caption">Lot No :</div>
								<div class="form-group field" style="width:350px;margin-bottom:0px;">
									<input type="text" class="form-control" name="lot_no" id="pon" value="<?php echo ($fabric_inward_row['LOT_NO']=='')?$lot_no:$fabric_inward_row['LOT_NO']; ?>" style="width: 350px;" required readonly />
									<input type="hidden" name="details" value="" />
									<input type="hidden" name="fabric_id" value="<?php echo $fabric_id ?>" />
								</div>
								<div class="caption">Packing Date :</div>
								<div class="field" style="width:150px">
									<input type="text" name="contract_date" value="<?php echo ($fabric_inward_row==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($fabric_inward_row['CONTRACT_DATE'])); ?>" class="form-control datepicker" required />
								</div>
								<div class="clear"></div>

								<div class="caption">Supplier :</div>
								<div class="form-group field" style="width:350px;margin-bottom:0px;">
									<select class="supplier_id show-tick form-control" name="supplier_id" data-style="btn-default" data-live-search="true" style="border:none" required>
										 <option selected value=""></option>
											<?php
												if(mysql_num_rows($accountsList)){
													while($account = mysql_fetch_array($accountsList)){
														$supplier_selected = ($fabric_inward_row['SUPPLIER_ID']==$account['ACC_CODE'])?"selected":"";
												?>
												<option <?php echo $supplier_selected; ?> value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
											<?php
													}
												}
											?>
									</select>
								</div>
								<div class="clear"></div>

								<div class="hide">
									<div class="caption">Delivery Date :</div>
									<div class="field" style="width:150px">
										<input type="text" name="delivery_date" value="<?php echo ($fabric_inward_row==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($fabric_inward_row['DELIVERY_DATE'])); ?>" class="form-control datepicker" />
									</div>
									<div class="clear"></div>
								</div>

								<div class="hide">
									<div class="caption">Broker :</div>
									<div class="form-group field" style="width:350px;margin-bottom:0px;">
										<select class="selectpicker show-tick form-control" name="broker_id" data-live-search="true">
												<option value="" ></option>
												<?php
												if(mysql_num_rows($broker_list)){
														while($brokerArray = mysql_fetch_assoc($broker_list)){
																?>
																<option value="<?php echo $brokerArray['ACCOUNT_CODE']; ?>" <?php echo ($brokerArray['ACCOUNT_CODE']==$fabric_inward_row['BROKER_ID'])?"selected":""; ?> ><?php echo $brokerArray['ACCOUNT_TITLE']; ?></option>
														<?php
														}
												}
												?>
										</select>
									</div>
									<div class="caption">Commission :</div>
									<div class="field" style="width:150px">
										<input type="text" name="broker_commission" value="<?php echo $fabric_inward_row['BROKER_COMMISSION']; ?>" class="form-control" />
									</div>
									<div class="clear"></div>
								</div>

								<div class="hide">
									<div class="caption">Measure :</div>
									<div class="field" style="width:150px">
										<input type="text" name="measure" value="<?php echo $fabric_inward_row['MEASURE']; ?>" class="form-control" />
									</div>
									<div class="caption" style="width:50px">Rate :</div>
									<div class="field" style="width:120px">
										<input type="text" name="rate" value="<?php echo $fabric_inward_row['RATE']; ?>" class="form-control" />
									</div>
									<div class="clear"></div>
								</div>

								<div class="hide">
									<div class="caption">Payment Schedule :</div>
									<div class="field" style="width:680px">
										<input type="text" name="payment_schedule" value="<?php echo $fabric_inward_row['PAYMENT_SCHEDULE']; ?>" class="form-control" />
									</div>
									<div class="clear"></div>
								</div>

								<div class="caption">Dyeing Lot # :</div>
								<div class="field" style="width:150px">
									<input type="text" name="dyeing_unit_lot_no" value="<?php echo ($fabric_inward_row==NULL)?$fabric_contract_details['DYEING_UNIT_LOT_NO']:$fabric_inward_row['DYEING_UNIT_LOT_NO']; ?>" class="form-control" />
								</div>
								<div class="clear"></div>

									<div style="margin-top:0px"></div>
								</div> <!-- End form -->
								<div class="clear"></div>
								<div id="form">
									<div class="col-xs-12">
										<table id="pro_table" style="width:100%;">
											<thead>
												<tr>
													<th width="30%" style="font-size:12px;font-weight:normal;text-align:center">Fabrci Output</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Variety</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Thaan</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Meters</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Rate/Mtr</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Amount</th>
													<th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr class="inputRow" style="background:none">
													<td>
														<select class="form-control show-tick fabric_type" name="fabric_type">
															<?php foreach($fabric_array as $key => $value){
																?>
																	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
																<?php
															} ?>
														</select>
													</td>
													<td>
														<select class="variety show-tick form-control"
																		data-style="btn-default"
																		data-live-search="true" style="border:none">
															<option selected value=""></option>
			<?php
												$itemsCategoryList   = $objItemCategory->getList();
												if(mysql_num_rows($itemsCategoryList)){
														while($ItemCat = mysql_fetch_array($itemsCategoryList)){
					$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
			?>
						<optgroup label="<?php echo $ItemCat['NAME']; ?>">
			<?php
					if(mysql_num_rows($itemList)){
						while($theItem = mysql_fetch_array($itemList)){
							if($theItem['ACTIVE'] == 'N'){
								continue;
							}
							if($theItem['INV_TYPE'] == 'B'){
								continue;
							}
			?>
							<option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo $theItem['NAME']; ?></option>
			<?php
						}
					}
			?>
						</optgroup>
			<?php
														}
												}
			?>
														</select>
													</td>
													<td>
														<input type="text" class="thaan form-control text-center"/>
														<input type="hidden" class="qty_per_thaan" value="0" />
													</td>
													<td>
														<input type="text" class="quantity form-control text-center"/>
													</td>
													<td>
														<input type="text" class="rate form-control text-center"/>
													</td>
													<td>
														<input type="text" class="amount form-control text-center"/>
													</td>
													<td>
														<button type="button" class="btn btn-default btn-block enter_new_row" name="enter">Enter</button>
													</td>
												</tr>
											</tbody>
											<tbody>
												<?php
													$thaan_total    = 0;
													$total_quantity = 0;
													$amount_total   = 0;
													if(isset($fabric_contract_list)&&mysql_num_rows($fabric_contract_list)){
														while($fabric_detail_row = mysql_fetch_assoc($fabric_contract_list)){

															$thaan_total    += $fabric_detail_row['THAAN'];
															$total_quantity += $fabric_detail_row['QUANTITY'];
															$amount_total   += $fabric_detail_row['AMOUNT'];
																?>
																<tr class='table_row' data-id="<?php echo $fabric_detail_row['ID']; ?>">
																	<td class='text-left 	fabric_type' data-value="<?php echo $fabric_detail_row['CONSTRUCTION']; ?>"><?php echo $fabric_array[$fabric_detail_row['CONSTRUCTION']]; ?></td>
																	<td class='text-left 	variety' data-id="<?php echo $fabric_detail_row['VARIETY']; ?>"><?php echo $objItems->getItemTitle($fabric_detail_row['VARIETY']); ?></td>
																	<td class='text-center  thaan'><?php echo $fabric_detail_row['THAAN']; ?></td>
																	<td class='text-center  quantity'><?php echo $fabric_detail_row['QUANTITY']; ?></td>
																	<td class='text-center  rate'><?php echo $fabric_detail_row['RATE']; ?></td>
																	<td class='text-center  amount'><?php echo $fabric_detail_row['AMOUNT']; ?></td>
																	<td class="text-center">
																		<a id="view_button" onclick='editMe(this);'><i class="fa fa-pencil"></i></a>
																		<a class="pointer" onclick='deleteMe(this);'><i class="fa fa-times"></i></a>
																	</td>
																</tr>
																<?php
														}
													}
												?>
											</tbody>
											<tfoot>
												<tr>
													<td class="text-right" colspan="2">Total</td>
													<td class="text-center thaan_total"><?php echo $thaan_total; ?></td>
													<td class="text-center quantity_total"><?php echo $total_quantity; ?></td>
													<td class="text-center" > - - - </td>
													<td class="text-center amount_total"><?php echo $amount_total; ?></td>
													<td class="text-center" > - - - </td>
												</tr>
											</tfoot>
										</table>
									</div>
									<div class="clear"></div>
									<div class="caption"></div>
									<div class="field">
										<input type="button" name="save" value="<?php echo ($fabric_id>0)?"Update":"Save"; ?>" class="button" />
										<input type="button" value="New Form" class="button" onclick="window.location.href='packing-contract-details.php?lot_no=<?php echo $lot_no; ?>'";  />
									</div>
									<div class="clear"></div>
									</form>
								</div> <!-- End form -->
							</div> <!-- End #tab1 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->

					<!-- Delete confirmation popup -->
					<div id="myConfirm" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Confirmation</h4>
								</div>
								<div class="modal-body">
									<p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
									<button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<!-- Loading bar confirmation popup -->
					<div id="myLoading" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Processing...</h4>
								</div>
								<div class="modal-body">
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
										aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
										<span class="sr-only">100% Complete</span>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<div id="selecCustomer" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="background: #72a9d8; color: white;">
								<h4 class="modal-title">Attention!</h4>
							</div>
							<div class="modal-body">
								<p class="text-danger" style="font-weight: bold;">Please Select Customer first.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!--body-wrapper-->
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
