<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/trial-balance.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('balance-sheet',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts       = new ChartOfAccounts();
	$objBalanceSheet   = new TrialBalance();
	$objCompanyDetails = new CompanyDetails();

	$companyTitle = $objCompanyDetails->getTitle('1');

	if(isset($_POST['report'])){
		$CurrentYearLastDate   = ($_POST['endDate']=="")?"":date('Y-m-d',strtotime($_POST['endDate']));
		$CurrentYearStartDate  = ($CurrentYearLastDate=="")?"":date('Y-01-01',strtotime($CurrentYearLastDate));
		$PrevioudYearStartDate = date('Y-m-d',strtotime($CurrentYearStartDate."-1 year"));
		$PrevioudYearEndDate = date('Y-m-d',strtotime($CurrentYearStartDate."-1 day"));

		$thisMonthEndDate = date('Y-m-01',strtotime($CurrentYearLastDate."+ 1 month"));
		$thisMonthEndDate = date('Y-m-d',strtotime($thisMonthEndDate."-1 day"));
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
      	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
      	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
      	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
      	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="resource/css/print-reports.css" type="text/css" media="screen" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
      	<script type="text/javascript" src = "resource/scripts/reports.configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script>
			$(window).load(function(){
				setTimeout(function(){
					$("#validation_message").remove();
				},1000);
				$.fn.stDigits = function(){
					return this.each(function(){
						$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
					})
				};

				$("div.current_year").stDigits();
				$(".printThis").click(function(){
					$(".printTable").printThis({
						  debug: false,
						  importCSS: false,
						  printContainer: true,
						  loadCSS: 'resource/css/print-reports.css',
						  pageTitle: "Smart IT Solutions",
						  removeInline: false,
						  printDelay: 500,
						  header: null
					});
				});
			});
		</script>
		<style type="text/css">
			.button{
				border-radius: 3px;
			}
		</style>
   </head>

   <body style="background-image:none !important;background-color:#FFF !important;">
      	<div id = "body-wrapper">
         	<div id = "bodyWrapper">
            	<div class = "content-box-top" style="left:0px;right:0px;position:relative;width:98%;margin:0px auto;">
               		<div class="summery_body">
                    	<div id = "bodyTab1">
                            <div class="clear"></div>
                            <div id="form" style="margin: 0 auto; width: 900px;">
                                <form method="post" action="">
                                    <div class="caption" style="font-size:16px !important;;margin-left:120px;padding: 10px 0px 20px 20px;">Report Date:</div>
                                    <div class="field" style="width:150px;">
                                        <input type="text" name="endDate" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                    </div>
                                    <div class="field" style="margin-left:10px;"><input type="submit" value="Generate" name="report" class="btn btn-default" /></div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div style="clear:both;height:20px;"></div>
                		</div> <!-- End bodyTab1 -->
            		</div> <!-- End .content-box-content -->
					<?php
                    	if(isset($_POST['report'])){
                    ?>
        			<div class="content-box-content">
                    	<span style="float:right;margin-right:50px;"><button class="btn btn-primary btn-sm printThis">Print</button></span>
                		<div id="bodyTab" class='printTable'   style="width: 800px;margin: 0 auto;">
                            <div style="text-align:left;margin-bottom:20px;">
                            	<p class="report_heading">
                                	<?php echo $companyTitle; ?><br />
                                    Balance Sheet as on <?php echo date('d-M-Y',strtotime($CurrentYearLastDate)); ?>
                                </p>
                            </div>
                            <div style="clear:both; height:1;"></div>
<?php

							$currentAssets    =  array();

							$currentAssetResource = $objAccounts->getLevelThreeListByMain('0101');
							if(mysql_num_rows($currentAssetResource)){
								while($currentAssetRow = mysql_fetch_assoc($currentAssetResource)){
									$currentAssets[] = $currentAssetRow['ACC_CODE'];
								}
							}

							$fixedAssets 		  =  array();

							$fixedAssetResource = $objAccounts->getLevelThreeListByMain('0102');
							if(mysql_num_rows($fixedAssetResource)){
								while($fixedAssetRow = mysql_fetch_assoc($fixedAssetResource)){
									$fixedAssets[] = $fixedAssetRow['ACC_CODE'];
								}
							}

							$liabilities 		  =  array();

							$liabilitieResource = $objAccounts->getLevelThreeListByMain('0401');
							if(mysql_num_rows($liabilitieResource)){
								while($liabRow = mysql_fetch_assoc($liabilitieResource)){
									$liabilities[] = $liabRow['ACC_CODE'];
								}
							}

							$liabilitieResource = $objAccounts->getLevelThreeListByMain('0402');
							if(mysql_num_rows($liabilitieResource)){
								while($liabRow = mysql_fetch_assoc($liabilitieResource)){
									$liabilities[] = $liabRow['ACC_CODE'];
								}
							}

							$equity				  = array();

							$equityResource = $objAccounts->getLevelThreeListByMain('0501');
							if(mysql_num_rows($equityResource)){
								while($equityRow = mysql_fetch_assoc($equityResource)){
									$equity[] = $equityRow['ACC_CODE'];
								}
							}
?>


                            <div id="summary_form">
                                <div class="main_title_1">Assets</div>
                            	<div style="page-break-inside: avoid !important;">
                                	<div class="main_title_middle pull-left">Current Assets</div>
                                	<div class="main_title_amuont pull-right">Amount Rs.</div>
                                	<div class="clear"></div>
                                	<div id="form" style="margin-left:20px">

<?php
							$assetsTotal  = 0;
							$currentAssetsTotal = 0;

							$purchase_row = array();
							$purchase_row['ACC_CODE'] = '';
							$purchase_row['ACC_TITLE'] = '';
							$purchase_row['ACC_AMOUNT'] = '';

							foreach($currentAssets as $key => $account_code){
								$thisCurrentAssetCodelist = $objBalanceSheet->getChildAccountsOf($account_code);
								$this_third_total = 0;
								if(mysql_num_rows($thisCurrentAssetCodelist)){
									$this_this_title = $objAccounts->getAccountTitleByCode($account_code);
									while($currentAssetRow = mysql_fetch_assoc($thisCurrentAssetCodelist)){
										$currentAssetRowClose = $objBalanceSheet->getLedgerOpeningBalance($CurrentYearLastDate,$currentAssetRow['ACC_CODE']);
										$row_account_title = $objAccounts->getAccountTitleByCode($currentAssetRow['ACC_CODE']);
										if($currentAssetRow['ACC_CODE'] == '0101060001'){
											$purchase_row['ACC_CODE']   = $currentAssetRow['ACC_CODE'];
											$purchase_row['ACC_TITLE']  = $row_account_title;
											$purchase_row['ACC_AMOUNT'] = $currentAssetRowClose;
											continue;
										}
										if($currentAssetRow['ACC_CODE'] == '0101060002'){
											$purchase_row['ACC_AMOUNT'] += $currentAssetRowClose;
											$row_account_title    = $purchase_row['ACC_TITLE'];
											$currentAssetRowClose = $purchase_row['ACC_AMOUNT'];
										}
										if($currentAssetRowClose == 0){
											continue;
										}
										$assetsTotal += $currentAssetRowClose;
										$currentAssetsTotal += $currentAssetRowClose;
										$this_third_total += $currentAssetRowClose;
									}
									if($this_third_total != 0){
										$this_third_total_print = number_format($this_third_total,2);
										if($this_third_total < 0){
											$this_third_total_print = "(".str_ireplace('-', '', number_format($this_third_total,2)).")";
										}

									?>
										<div class="rep_row">
											<div class="bs_caption"><?php echo $this_this_title; ?></div>
                                            <div class="current_year"><?php echo $this_third_total_print; ?></div>
		                                    <div class="clear"></div>
		                                </div>
<?php
									}
								}
							}
							if($currentAssetsTotal != 0){
?>
								<div style="height:10px;clear:both"></div>
								<div class="rep_row">
									<div class="bs_caption"><b>Current Assets Total</b></div>
                                    <div class="current_year"><b><?php echo number_format($currentAssetsTotal,2); ?></b></div>
                                    <div class="clear"></div>
                                </div>
<?php
							}
?>
						  			</div>
						  	</div><!--/Avoid-->
					      	<div class="clear"></div>
					      	<?php //exit(); ?>
                          	<div style="page-break-inside:avoid !important;page-break-after:auto !important;">
                                <div class="main_title_middle pull-left">Fixed Assets</div>
                                <div class="clear"></div>
                                <div id="form" style="margin-left:20px">
<?php
							$fixedAssetsTotal = 0;

							foreach($fixedAssets as $key => $account_code){
								$thisFixedAssetCodelist = $objBalanceSheet->getChildAccountsOf($account_code);
								$this_third_total = 0;
								if(mysql_num_rows($thisFixedAssetCodelist)){
									while($currentAssetRow = mysql_fetch_assoc($thisFixedAssetCodelist)){
										$fixedAssetRowClose = $objBalanceSheet->getLedgerOpeningBalance($CurrentYearLastDate,$currentAssetRow['ACC_CODE']);
										if($fixedAssetRowClose == 0){
											continue;
										}
										$assetsTotal += $fixedAssetRowClose;
										$fixedAssetsTotal += $fixedAssetRowClose;
										$this_third_total += $fixedAssetRowClose;
									}
								}
								if($this_third_total != 0){
									$this_third_total_print = number_format($this_third_total,2);
									if($this_third_total < 0){
										$this_third_total_print = "(".str_ireplace('-', '', number_format($this_third_total,2)).")";
									}
?>
								<div class="rep_row">
									<div class="bs_caption"><?php echo $objAccounts->getAccountTitleByCode($account_code); ?></div>
                                    <div class="current_year"><?php echo $this_third_total_print; ?></div>
                                    <div class="clear"></div>
                                </div>
<?php
								}
							}
							if($fixedAssetsTotal != 0){
?>
								<div style="height:10px;clear:both"></div>
								<div class="bs_caption"><b>Fixed Assets Total</b></div>
                                <div class="current_year"><b><?php echo number_format($fixedAssetsTotal,2); ?></b></div>
                                <div class="clear"></div>
<?php
							}
?>
								<div class="clear"></div>
	                            <div style="margin-top: 10px;">
	                                <div class="bs_caption" style="font-size: 16px;font-weight:bold;">Assets Total</div>
	                                <div class="current_year" style="border-bottom: 4px double #999;"><?php echo number_format($assetsTotal,2); ?></div>
	                                <div class="clear"></div>
	                            </div>
	                            </div><!--form-->
                            	<div class="clear" style="height: 5px;"></div>
                            </div>
                            <div class="main_title_1">Liabilities &amp; Owner's Equity</div>
                            <div class="clear"></div>
                            <div style="page-break-inside:avoid !important;page-break-after:auto !important;">
                                <div class="main_title_middle pull-left">Current Liabilities</div>
                                <div class="main_title_amuont pull-right">Amount Rs.</div>
                                <div class="clear"></div>
                                <div id="form" style="margin-left:20px">
<?php
                                $liabilitiesTotal = 0;

                                foreach($liabilities as $key => $account_code){
                                    $thisFixedAssetCodelist = $objBalanceSheet->getChildAccountsOf($account_code);
                                    $this_third_total = 0;
                                    if(mysql_num_rows($thisFixedAssetCodelist)){
                                        while($liabilitiesRow = mysql_fetch_assoc($thisFixedAssetCodelist)){
                                            $liabilitiesRowClose = $objBalanceSheet->getLedgerOpeningBalance($CurrentYearLastDate,$liabilitiesRow['ACC_CODE']);
                                            if($liabilitiesRowClose == 0){
                                                continue;
                                            }
                                            $liabilitiesTotal += $liabilitiesRowClose;
                                            $fixedAssetsTotal += $liabilitiesRowClose;
                                            $this_third_total += $liabilitiesRowClose;
                                        }
	                                    if($this_third_total != 0){
											$this_third_total_print = number_format($this_third_total,2);
											if($this_third_total < 0){
												$this_third_total_print = "(".str_ireplace('-', '', number_format($this_third_total,2)).")";
											}
?>
											<div class="rep_row">
		                                        <div class="bs_caption"><?php echo $objAccounts->getAccountTitleByCode($account_code); ?></div>
		                                        <div class="current_year"><?php echo $this_third_total_print; ?></div>
		                                        <div class="clear"></div>
		                                    </div>
<?php
										}
                                    }
                                }
                                if($liabilitiesTotal != 0){
?>
											<div style="height:10px;clear:both"></div>
											<div class="rep_row">
												<div class="bs_caption"><b>Current Liabilities Total</b></div>
												<div class="current_year"><b><?php echo number_format($liabilitiesTotal,2); ?></b></div>
												<div class="clear"></div>
											</div>
<?php
                                }
?>
							</div>
							<div class="clear"></div>
							<div class="main_title_middle pull-left">Owner's Capital</div>
                            <div class="main_title_amuont pull-right"></div>
                            <div class="clear"></div>
							<div id="form" style="margin-left:20px">
<?php
							$equityTotal = 0;

							foreach($equity as $key => $account_code){
								$thisEquityCodelist = $objBalanceSheet->getChildAccountsOf($account_code);
								$this_third_total = 0;
								if(mysql_num_rows($thisEquityCodelist)){
									while($equityRow = mysql_fetch_assoc($thisEquityCodelist)){
										$equityRowClose = $objBalanceSheet->getLedgerOpeningBalance($CurrentYearLastDate,$equityRow['ACC_CODE']);

										if($equityRowClose == 0){
											continue;
										}
										$equityTotal += $equityRowClose;
										$fixedAssetsTotal += $equityRowClose;
										$this_third_total += $equityRowClose;
									}
								}
								if($this_third_total != 0){
									$this_third_total_print = number_format($this_third_total,2);
									if($this_third_total < 0){
										$this_third_total_print = "(".str_ireplace('-', '', number_format($this_third_total,2)).")";
									}
?>
									<div class="rep_row">
                                        <div class="bs_caption"><?php echo $objAccounts->getAccountTitleByCode($account_code); ?></div>
                                        <div class="current_year"><?php echo $this_third_total_print; ?></div>
                                        <div class="clear"></div>
                                    </div>
<?php
								}
							}
?>
								<div class="clear"></div>
<?php
							if($equityTotal != 0){
?>
									<div style="height:10px;clear:both"></div>
									<div class="bs_caption"><b>Equity Total</b></div>
                                    <div class="current_year"><b><?php echo number_format($equityTotal,2); ?></b></div>
                                    <div class="clear"></div>
<?php
							}
?>
											<div style="margin-top: 10px;">
			                                    <div class="bs_caption" style="font-size: 16px;font-weight:bold;">Liabilities &amp; Equity Total</div>
			                                    <div class="current_year" style="border-bottom: 4px double #999;">
													<?php $liabilitiesGrand = $equityTotal + $liabilitiesTotal; ?>
													<?php echo number_format($liabilitiesGrand,2); ?>
			                                    </div>
			                                    <div class="clear"></div>
			                                </div>
								</div><!--form-->
                            	<div class="clear" style="height: 5px;"></div>
                            	</div>
                            	<div class="clear"></div>
							</div>
	                        <!--/Avoid-->
                            <div style="clear:both; height:10px"></div>
                    	</div><!--bodyTab2-->
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
<?php
						}
?>
        	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
        </div>
   </body>
</html>
<?php include('conn.close.php'); ?>
