<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/dist_sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sale-returns',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes	 	 		 = new ChartOfAccounts();
	$objDistributorSaleReturn        = new DistributorSaleReturn();
	$objDistributorSaleReturnDetails = new DistributorSaleReturnDetails();
	$objItems            			 = new Items();
	$objItemCategory 		 		 = new itemCategory();
	$objTaxRates         			 = new TaxRates();
	$objDepartments      			 = new Departments();

    $suppliersList   		= $objAccountCodes->getAccountByCatAccCode('010104');
	$itemsCategoryList   	= $objItemCategory->getList();
	$taxRateList     		= $objTaxRates->getList();

	$sale_id = 0;
	if(isset($_GET['id'])){
		$sale_id = mysql_real_escape_string($_GET['id']);
		if($sale_id != '' && $sale_id != 0){
			$inventory = mysql_fetch_array($objDistributorSaleReturn->getRecordDetails($sale_id));
			$inventoryDetails = $objDistributorSaleReturnDetails->getList($sale_id);
		}else{
			$sale_id = 0;
		}
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
        html{
        }
        .ui-tooltip{
            font-size: 12px;
            padding: 5px;
            box-shadow: none;
            border: 1px solid #999;
        }
        div.tabPanel .tab,div.tabPanel .tabSelected{
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
        }
        .input_sized{
            float:left;
            width: 152px;
            padding-left: 5px;
            border: 1px solid #CCC;
            height:30px;
            -webkit-box-shadow:#F4F4F4 0 0 0 2px;
            border:1px solid #DDDDDD;

            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;

            -moz-border-radius-topleft:3px;
            -moz-border-radius-topright:3px;
            -moz-border-radius-bottomleft:3px;
            -moz-border-radius-bottomright:5px;

            -webkit-border-top-left-radius:3px;
            -webkit-border-top-right-radius:3px;
            -webkit-border-bottom-left-radius:3px;
            -webkit-border-bottom-right-radius:3px;

            box-shadow: 0 0 2px #eee;
            transition: box-shadow 300ms;
            -webkit-transition: box-shadow 300ms;
        }
        .input_sized:hover{
            border-color: #9ecaed;
            box-shadow: 0 0 2px #9ecaed;
        }
        td,th{
            padding: 10px 5px !important;
            border:1px solid #CCC !important;
        }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/dist.sale.return.config.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
					<?php include("common/left_menu.php") ?>
    		</div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sale Return</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="dist-sale-return.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="dist-sale-return.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div id="form" style="margin: 20px auto;">
                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Return Date</div>
                            <div class="field" style="width:200px;margin-left:0px;padding: 0px;">
                                <input type="text" name="rDate" id="datepicker" value="<?php echo (isset($inventory))?date("d-m-Y",strtotime($inventory['SALE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:200px" />
                            </div>
                            <div class="caption" style="width:100px;">Memo #</div>
                            <div class="field" style="width:118px">
                            	<input type="text" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>" class="form-control bill_num" />
                            </div>
                            <div class="caption" style="width:100px;">PO #</div>
                            <div class="field" style="width:118px">
                            	<input type="text" value="<?php echo (isset($inventory))?$inventory['PO_NUMBER']:''; ?>" class="form-control po_number" />
                            </div>
                            <div class="clear"></div>

<?php
                                      $sales_man_arr = array();
				                      $users_list = $objAccounts->getActiveList();
				                      if(mysql_num_rows($users_list)){
				                        while($user = mysql_fetch_assoc($users_list)){
				                          if($user['DESIGNATION_TYPE'] == ''){
				                            //$user['DESIGNATION_TYPE'] = 'S';
				                            continue;
				                          }
				                          if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
				                            $sales_man_arr[$user['DESIGNATION_TYPE']] = array();
				                          }
				                          $sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
				                        }
				                      }
?>

														<div class="caption" style="width:80px">OrderTaker</div>
			                      <div class="field" style="width:200px;position:relative;">
			                        <select class="order_taker form-control" name="order_taker">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['O'])){
			                            foreach ($sales_man_arr['O'] as $key => $user){
			                                $user_selected = (isset($inventory)&&$inventory['ORDER_TAKER']==$user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>

			                      <div class="caption" style="width:100px">Salesman</div>
			                      <div class="field" style="width:270px;position:relative;">
			                        <select class="user_id" name="salesman">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['S'])){
			                            foreach ($sales_man_arr['S'] as $key => $user){
			                                $user_selected = (isset($inventory)&&$inventory['USER_ID']==$user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>
			                      <div class="clear"></div>

                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Account</div>
                            <div class="field" style="width:200px;position:relative;">
                                <select class="supplierSelector form-control " data-style="btn-default"data-live-search="true" data-hide-disabled="true" style="border:none" <?php echo ($sale_id!=0)?"disabled":""; ?> >
                                   <option selected value=""></option>
<?php

                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    print_r($account);
                                    $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                }
                            }
                            if(mysql_num_rows($cash_in_hand_list)){
                                while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                                    $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                                    if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
                                        continue;
                                    }
                                    $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                    <option <?php echo $selected; ?> disabled data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
                            </div>
                            <div class="caption" style="padding: 10px;width: 80px;">
                                <input type="checkbox" id="mor1" <?php echo (isset($inventory) && substr($inventory['CUST_ACC_CODE'],0,6)== '010101')?"checked=\"checked\"":""; ?> onchange="makeItCash(this);" class="trasactionType css-checkbox" value="0101010001" title="Cash In Hand" />
                                <label id="mor1" for="mor1" class="css-label" title="Ctrl+Space"><small>Cash</small></label>
                            </div>
                            <div class="supplier_name_div" style="display: none; margin-left: 0px; float:left;">
                                <div class="field" style="width:195px;margin-left:10px;">
                                    <input class="form-control customer_name" placeholder="Name" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>"  />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <hr />
                            <input type="hidden" class="sale_id" value="<?php echo $sale_id; ?>" />
                            <table class="prom">
                            <thead>
                                <tr>
                                   <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Cartons</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Carton Rate</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Dozen</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Doz.Rate</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Qty(Peices)</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Discount %</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                   <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">StockInHand</th>
                                   <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="quickSubmit" style="background:none">
                                    <td>
                                        <select class="itemSelector show-tick form-control" data-style="btn-default" data-live-search="true">
                                            <option selected value=""></option>
<?php
                                                if(mysql_num_rows($itemsCategoryList)){
                                                    while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                                                        $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
                                                            <optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
                                                        if(mysql_num_rows($itemList)){
                                                            while($theItem = mysql_fetch_array($itemList)){
                                                                if($theItem['ACTIVE'] == 'N'){
                                                                    continue;
                                                                }
                                                                if($theItem['INV_TYPE'] == 'B'){
                                                                    continue;
                                                                }
?>
                                                            <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
											}
										}
?>
											</optgroup>
<?php
																			}
																	}
?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="cartons form-control" />
                                        <input type="hidden" class="qty_carton" value="" />
                                    </td>
                                    <td>
                                        <input type="text" class="rate_carton form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" class="dozen form-control" />
                                        <input type="hidden" class="dozen_qty" value="12" />
                                    </td>
                                    <td>
                                        <input type="text" class="dozen_price form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" class="quantity form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" class="unitPrice form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" class="discount form-control"/>
                                    </td>
                                    <td>
                                        <input type="text"  readonly value="0" class="subAmount form-control"/>
                                    </td>
                                    <td class="taxTd">
                                        <input class="taxRate form-control" value="" />
                                    </td>
                                    <td>
                                        <input type="text"  readonly value="0" class="taxAmount form-control"/>
                                    </td>
                                    <td>
                                        <input type="text"  readonly  value="0" class="totalAmount form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" class="inStock form-control" theStock="0" readonly />
                                    </td>
                                    <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                </tr>
                            </tbody>
                            <tbody>
                            	<!--doNotRemoveThisLine-->
                                    <tr style="display:none;" class="calculations"></tr>
                                <!--doNotRemoveThisLine-->
<?php
						//if(false){
						if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
							while($invRow = mysql_fetch_array($inventoryDetails)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
?>
                                <tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td class="text-left itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
																				<?php echo $itemName; ?>
                                    </td>
                                    <td class="text-center cartons"><?php echo $invRow['CARTONS'] ?></td>
                                    <td class="text-center rate_carton"><?php echo $invRow['RATE_CARTON'] ?></td>
                                    <td class="text-center dozen"><?php echo $invRow['DOZENS'] ?></td>
                                    <td class="text-center dozen_price"><?php echo $invRow['DOZEN_PRICE'] ?></td>
                                    <td class="text-center quantiity"><?php echo $invRow['QUANTITY'] ?></td>
                                    <td class="text-center unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td class="text-center discount"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                    <td class="text-center subAmount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td class="text-center taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td class="text-center taxAmount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td class="text-center totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"></td>
                                </tr>
<?php
							}
						}
?>
                                <tr class="totals">
                                    <td style="text-align:center;background-color:#EEEEEE;">Total</td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="total_cartons"></td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="total_dozens"></td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="discountAmount"></td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="amountSub"></td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="amountTax"></td>
                                    <td style="text-align:center;background-color:#f5f5f5;" class="amountTotal"></td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                    <td style="text-align:center;background-color:#EEE;">- - -</td>
                                </tr>
                            </tbody>
                        </table>
												<div class="clear m-10"></div>
                        <div class="underTheTable">
                            <?php if($sale_id == 0){ ?>
			                     <div class="button saveSale pull-right ml-30">Save</div>
                            <?php } ?>
                            <div class="button pull-right" onclick="window.location.href='dist-sale-return-details.php';">New Form</div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
        $("input.bill_num").change(function(){
            var billNum = parseInt($(this).val())||0;
            displayMessage("Please Wait...");
            $.get('dist-sale-details.php',{'bill_num_param':billNum},function(data){
                $("select[name=order_taker]").selectpicker('val',$(data).find("select[name=order_taker] option:selected").val());
                $("select[name=salesman]").selectpicker('val',$(data).find("select[name=salesman] option:selected").val());
                $("select.supplierSelector").selectpicker('val',$(data).find("select.supplierSelector option:selected").val());
                if($("div#popUpDel").is(":visible")){
                    $("div#popUpDel .nodelete").trigger("click");
                }
            });
        });
		$('select').selectpicker();
		$(document).calculateColumnTotals();
		//Numericalize
		$("input.bill_num").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.supplierSelector button");

		$("div.supplierSelector button").keyup(function(e){
            var supp = $("select.supplierSelector option:selected").val();
            if(e.keyCode == 13 && supp != ''){
                if($("select.supplierSelector option:selected[value^='010101']").length){
                    console.log('e');
                    $(".supplier_name_div").show();
                    $(".supplier_name_div input").focus();
                }else{
                    $(".supplier_name_div").hide();
                    $('div.itemSelector button').focus();
                }
            }
        });

		$("input.bill_num").setFocusTo("div.supplierSelector button");
		$("input.customer_name").setFocusTo("div.itemSelector button");
		$("div.itemSelector button").keyup(function(e){
			var item = $('.itemSelector option:selected').val();
			if(e.keyCode == 13 && item != ''){
				if($(".updateMode").length == 0){
					$(this).getItemDetails();
				}
				$("input.cartons").focus();
			}
		});

		$("input.cartons").focus();

		$("input.cartons").keydown(function(e){
			if(e.keyCode == 13){
				$("input.rate_carton").focus();
			}
		});

		$("input.rate_carton").keydown(function(e){
			if(e.keyCode == 13){
				$("input.dozen").focus();
			}
        });

        $("input.dozen").keydown(function(e){
			if(e.keyCode == 13){
				$("input.dozen_price").focus();
			}
        });

        $("input.dozen_price").keydown(function(e){
			if(e.keyCode == 13){
				$("input.quantity").focus();
			}
        });

		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13){
				$("input.unitPrice").focus();
			}
		}).keyup(function(e){
			stockOs();
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});

		$("input.taxRate").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".addDetailRow").focus();
			}
		});

		//$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
		//$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");

		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
		});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".saveSale").click(function(){
			saveSale();
		});
        $("input.bill_num").focus();
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Memo Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Memo Updated Successfully!');
<?php
		}
?>

</script>
