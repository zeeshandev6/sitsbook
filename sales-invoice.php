<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/j-voucher.php');
	include('common/classes/company_details.php');

	$objChartOfAccounts    = new ChartOfAccounts();
	$objCompanyDetails 	   = new CompanyDetails();
	$objSales              = new Sale();
	$objSaleDetails        = new SaleDetails();
	$objSaleReturnDetails  = new SaleReturnDetails();
	$objCustomers          = new Customers();
	$objItems  	           = new Items();
	$objServices           = new Services();
	$objJournalVoucher     = new JournalVoucher();
	$objConfigs   	       = new Configs();

	//INVOICE_FORMAT Config Size - Header - Duplicate
	$use_cartons           = $objConfigs->get_config('USE_CARTONS');
	$use_cartons           = ($use_cartons == 'Y')?true:false;
	$invoice_format        = $objConfigs->get_config('INVOICE_FORMAT');
	$show_header 		   		 = $objConfigs->get_config('SHOW_INVOICE_HEADER');
	$notes_enclosures 	   = $objConfigs->get_config('USE_NOTES_ENCLOSURES');

	$invoice_format        = explode('_', $invoice_format);
	$invoice_num 		   		 = $invoice_format[1];
	$invoiceStyleCss       = 'resource/css/invoiceStyle.css';

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;

	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
	$use_taxes           = ($use_taxes=='Y')?true:false;

	$cutomer_balance_array 						= array();
	$cutomer_balance_array['BALANCE'] = 0;
	$cutomer_balance_array['TYPE']    = '';

	$merge_item_by_category 	= false;
	if($fabricProcessAddon=='Y'&&!isset($_GET['office'])){
		$merge_item_by_category = true;
	}

	if(isset($_GET['id'])){
		$sale_id 		     		= mysql_real_escape_string($_GET['id']);
		$saleDetails 	     	= mysql_fetch_array($objSales->getRecordDetails($sale_id));
		if($saleDetails['COMPANY_ID']>0){
			$companyLogo     	= $objCompanyDetails->getLogo($saleDetails['COMPANY_ID']);
			$company  		 		= $objCompanyDetails->getRecordDetails($saleDetails['COMPANY_ID']);
		}else{
			$companyLogo   	 	= $objCompanyDetails->getLogo();
			$company  		 		= $objCompanyDetails->getActiveProfile();
			$objSales->insertCompanyId($sale_id,$company['ID']);
		}
		if($merge_item_by_category){
			$saleDetailList        = $objSaleDetails->getListMergeByCategory($sale_id);
		}else{
			$saleDetailList        = $objSaleDetails->getList($sale_id);
		}

		if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010101'){
			$customer_balance      = $objJournalVoucher->getOpeningBalanceOfVoucher($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID'],$saleDetails['SALE_DATE']);
			$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
		}

		$replaced_items = $objSaleReturnDetails->getReturnReplaceItems($sale_id);
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>

	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "<?php echo $invoiceStyleCss; ?>",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$(".ledger_view").click(function(){
			var gl_date = $(this).attr('data-date');
			var gl_code = $(this).attr('data-code');
			if(gl_code != ''){
				$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
					if(data == ''){
						var win = window.open('invoice-ledger-print.php','_blank');
						if(win){
							win.focus();
						}else{
							displayMessage('Please allow popups for This Site');
						}
					}
				});
			}
		});
		$(".printThis").click();
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $saleDetails['CUSTOMER_NAME'];
		}elseif(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010102'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $objChartOfAccounts->getAccountTitleByCode($saleDetails['CUST_ACC_CODE']);
			$customer['CUST_ACC_TITLE'] .= " - ".$saleDetails['CUSTOMER_NAME'];
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$bill_discount = $saleDetails['SALE_DISCOUNT'];
?>
		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
					<button class="btn btn-default btn-sm printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print View </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="clear"></div>
			<div class="printThisDiv" style="position: relative;height:9.7in;">
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint" style="height:10.6in !important;">
						<div class="invoiceHead" style="width: 100%;margin: 0px auto;">
							<div class="<?php echo $show_header=='N'?"opacity_zero":""; ?>">
								<?php
								if($companyLogo != ''){
									?>
									<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
									<?php
								}
								?>
								<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
									<div style="font-size:26px;line-height: 40px;"> <?php echo $company['NAME']; ?> </div>
									<?php echo $company['ADDRESS']; ?>
									<br />
									Contact : <?php echo $company['CONTACT']; ?>
									<br />
									Email : <?php echo $company['EMAIL']; ?>
									<br />
								</div>
							</div>
							<div class="clear"></div>
							<div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
								<?php echo (substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101')?"CASH":""; ?> SALES INVOICE
							</div>
							<div class="clear" style="height: 5px;"></div>

							<?php
							if($invoice_num == '1'){

								$customer['CUST_ACC_TITLE'] = explode(',', $customer['CUST_ACC_TITLE']);
								$customer['CUST_ACC_TITLE'] = implode('<br />', $customer['CUST_ACC_TITLE']);
								?>
								<div class="infoPanel pull-left" style="height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">To : <?php echo $customer['CUST_ACC_TITLE']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo isset($customer['ADDRESS'])?$customer['ADDRESS']:""; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo isset($customer['CITY'])?$customer['CITY']:""; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="infoPanel pull-right" style="width:200px;height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Inv No. <?php echo $saleDetails['BILL_NO']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Date. <?php echo $sale_date; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="clear"></div>
								<?php
							}

							if($invoice_num == '2'){
								?>
								<div class="invoPanel">
									<div class="invoPanel">
										<div class="clear"></div>
										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Name: </span>
											<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Address: </span>
											<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">City: </span>
											<span class="text_line"><?php echo $customer['CITY']; ?></span>
										</span>
										<div class="clear"></div>
									</div>
								</div>
								<div class="clear" style="height:10px;"></div>
								<?php
							}
							?>

							<?php if(isset($saleDetails['SUBJECT']) && $saleDetails['SUBJECT'] != ''){ ?>
								<div class="infoPanelWide pull-left" style="min-height:auto !important;">
									<span class="pull-left">
										<span class="pull-left" style="padding:0px 0px !important;font-size:14px;">Subject : </span>
										<span class="pull-left" style="padding:0px 0px;font-size:14px;border-bottom:1px solid #000;"><?php echo $saleDetails['SUBJECT']; ?></span>
									</span>
									<div class="clear" style="height:10px;"></div>
								</div>
								<div class="clear" style="height:10px;"></div>
								<?php } ?>
								<div class="clear"></div>
							</div><!--invoiceHead-->
							<div class="clear"></div>
							<div class="invoiceBody" style="width: 100%;margin: 0px auto;">
								<table>
									<thead>
										<tr>
											<th width="5%">Sr#</th>
											<th width="40%">Description</th>
											<?php if($use_cartons){ ?>
												<th width="5%"><?php echo ($fabricProcessAddon=='Y')?"Thaan":"Cartons"; ?></th>
												<?php if($fabricProcessAddon!='Y'){ ?>
												<th width="5%">PerCarton</th>
												<?php } ?>
												<?php } ?>
												<th width="10%"><?php echo ($fabricProcessAddon=='Y')?"Length":"Qty"; ?></th>
												<th width="10%">Rate/Unit</th>
												<?php if($individual_discount){ ?>
													<th width="10%">Dis/Rate</th>
													<?php } ?>
													<?php if($use_taxes){ ?>
														<th width="10%">Tax %</th>
														<?php } ?>
														<th width="10%">Amount</th>
													</tr>
												</thead>
												<tbody>
<?php
													$total_cartons = 0;
													$quantity      = 0;
													$subAmount     = 0;
													$taxAmount     = 0;
													$totalAmount   = 0;
													$counter       = 1;
													if(mysql_num_rows($saleDetailList)){
														while($row = mysql_fetch_array($saleDetailList)){
															if($merge_item_by_category){
																$itemName = $row['CATEGORY_NAME'];
																$row['QUANTITY'] = $row['QTY'];
																$row['TOTAL_AMOUNT'] = $row['AMNT'];
																$row['CARTONS'] = $row['CARTON'];
																$row['PER_CARTON'] = '- - -';
																$row['UNIT_PRICE']   = $objSaleDetails->getAverageCostByItemCategory($row['ITEM_CATG_ID'],$row['SALE_ID']);
															}elseif($row['ITEM_ID'] != 0){
																$item_detail = $objItems->getRecordDetails($row['ITEM_ID']);
																$itemName    = $item_detail['ITEM_BARCODE']."-".$item_detail['NAME'];
															}else{
																$itemName = $objServices->getTitle($row['SERVICE_ID']);
															}
?>
															<tr>
																<td><?php echo $counter; ?></td>
																<td style="font-size:12px;">
																	<span style="float:left;margin-left:5px;"><?php echo $itemName; ?></span>
																	<span style="float:left;margin-left:5px;"><?php echo ($row['ITEM_DESCRIPTION']!='')?"(".$row['ITEM_DESCRIPTION'].")":""; ?></span>
																</td>
																<?php if($use_cartons){ ?>
																	<td><?php echo $row['CARTONS']; ?></td>
																		<?php if($fabricProcessAddon!='Y'){ ?>
																	<td><?php echo $row['PER_CARTON']; ?></td>
																		<?php } ?>
																	<?php } ?>
																	<td><?php echo ($row['SERVICE_ID'] > 0 && $row['QUANTITY'] == 0)?"1":$row['QUANTITY']; ?></td>
																	<td><?php echo number_format($row['UNIT_PRICE'],2); ?></td>
																	<?php if($individual_discount){ ?>
																		<td><?php echo $row['SALE_DISCOUNT']; ?> <?php echo ($saleDetails['DISCOUNT_TYPE'] == 'P')?"%":""; ?></td>
																		<?php } ?>
																		<?php if($use_taxes){ ?>
																			<td><?php echo $row['TAX_RATE']; ?></td>
																			<?php } ?>
																			<td><span style="float:right;margin-right:5px;"><?php echo number_format($row['TOTAL_AMOUNT'],2); ?></span></td>
																		</tr>
																		<?php
																		$total_cartons += $row['CARTONS'];
																		$quantity    += $row['QUANTITY'];
																		$subAmount 	 += $row['QUANTITY']*$row['UNIT_PRICE'];
																		$taxAmount   += $row['TAX_AMOUNT'];
																		$totalAmount += $row['TOTAL_AMOUNT'];
																		$counter++;
																	}
																}
																$return_total = 0;
																if(mysql_num_rows($replaced_items)){
																	while($returns = mysql_fetch_assoc($replaced_items)){
																		$item_name = $objItems->getItemTitle($returns['ITEM_ID']);
																?>
																<tr>
																	<td class="text-center bg-danger">R</td>
																	<td class="text-left bg-danger"><span style="float:left;margin-left:5px;"><?php echo $item_name; ?></span></td>
																<?php if($use_cartons){ ?>
																	<td class="text-center">- - -</td>
																	<?php if($fabricProcessAddon!='Y'){ ?>
																	<td class="text-center">- - -</td>
																	<?php } ?>
																<?php } ?>
																	<td class="text-center bg-danger"><?php echo $returns['QUANTITY']; ?></td>
																	<td class="text-center bg-danger"><?php echo $returns['UNIT_PRICE']; ?></td>
																	<?php if($individual_discount){ ?>
																	<td class="text-center bg-danger"><?php echo $returns['SALE_DISCOUNT']; ?></td>
																	<?php } ?>
																	<?php if($use_taxes){ ?>
																	<td class="text-center bg-danger"><?php echo $returns['TAX_RATE']; ?></td>
																	<?php } ?>
																	<td class="text-center bg-danger"><span style="float:right;margin-right:5px;"><?php echo $returns['TOTAL_AMOUNT']; ?></span></td>
																</tr>
																<?php
																	$return_total += $returns['TOTAL_AMOUNT'];
																	$quantity     -= $returns['QUANTITY'];
																	}
																}
																?>
															</tbody>
<?php
															$colum_skipper  = 0;
															$columns        = 4;
															$colum_skipper += ($use_cartons)?1:0;
															$colum_skipper += ($individual_discount)?1:0;
															$colum_skipper += ($use_taxes)?1:0;
															$colum_skipper += ($fabricProcessAddon=='Y')?0:1;
															$columns 	   += $colum_skipper;
															$columnx 	    = $columns;

															$columnx 	   -= 2;
															$columnx       -= ($use_cartons)?2:1;
															$columnx       -= ($fabricProcessAddon!='Y')?1:0;

															$columns       -= ($fabricProcessAddon!='Y'&&!$use_cartons)?1:0;
?>
															<tfoot>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="2"><span style="float:right;margin-right:15px;">Total </span></td>
																	<?php if($use_cartons){ ?>
																		<td><span style="float:center;margin-right:5px;"><?php echo $total_cartons; ?></span></td>
																		<?php if($fabricProcessAddon!='Y'){ ?>
																		<td class="text-center">- - -</td>
																		<?php } ?>
																	<?php } ?>
																	<td><span style="float:center;margin-right:5px;"><?php echo $quantity; ?></span></td>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columnx ; ?>"><span style="float:right;margin-right:15px;">Gross Sales</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($subAmount,2); ?></span></td>
																</tr>
																<?php if(mysql_num_rows($replaced_items)){ ?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Returns</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($return_total,2); ?></span></td>
																</tr>
																<?php } ?>
																<?php $totalAmount -= $saleDetails['DISCOUNT']; ?>
																<?php if($saleDetails['SALE_DISCOUNT'] > 0){ ?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Trade off</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['SALE_DISCOUNT'],2); ?></span></td>
																</tr>
																<?php } ?>
																<?php if($taxAmount > 0){ ?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Tax Amount</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($taxAmount,2); ?></span></td>
																</tr>
																<?php } ?>
																<?php $totalAmount += $saleDetails['CHARGES']; ?>
																<?php if($saleDetails['CHARGES'] > 0){ ?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Other Charges</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['CHARGES'],2); ?></span></td>
																</tr>
																<?php } ?>
																<tr class="bold_table_tr">
																	<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> Invoice Total </span></td>
																	<td style="text-align: right;padding-right: 10px;">
																		<span style="float:right;">
																			<?php
																				$totalAmount = (float)(str_ireplace('-','',$totalAmount));
																				$totalAmount -= $return_total;
																				echo number_format(($totalAmount),2)
																			 ?>
																		</span>
																	</td>
																</tr>
																<?php if($saleDetails['RECEIVED_CASH'] > 0){ ?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="<?php echo $columns; ?>"><span style="float:right;margin-right:15px;">Cash Received</span></td>
																	<td><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['RECEIVED_CASH'],2); ?></span></td>
																</tr>
																<?php } ?>
																<?php $adjustment = $totalAmount - $saleDetails['RECEIVED_CASH']; ?>
																<?php if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010104'){ ?>
																<tr class="bold_table_tr">
																	<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> <?php echo (substr($saleDetails['CUST_ACC_CODE'],0,6)=='010101')?"Change":"Balance Adjustment"; ?> </span></td>
																	<td style="text-align: right;padding-right: 10px;">
																		<span style="float:right;">
																			<?php
																				$adjustment = (float)(str_ireplace('-','',$adjustment));
																				echo number_format(($adjustment),2)
																			 ?>
																		</span>
																	</td>
																</tr>
																<?php } ?>
																<?php if(substr($saleDetails['CUST_ACC_CODE'],0,6) == '010104'){ ?>
																<tr>
																	<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> Last Balance </span></td>
																	<td style="text-align: right;padding-right: 10px;">
																		<span style="float:right;">
																			<?php echo number_format(($cutomer_balance_array['BALANCE']),2); ?>
																		</span>
																	</td>
																</tr>
																<tr>
																	<td colspan="<?php echo $columns; ?>" style="text-align: right;border:none !important;"><span style="float:right;margin-right:15px;"> Total </span></td>
																	<td style="text-align: right;padding-right: 10px;">
																		<span style="float:right;">
																			<?php echo number_format((($cutomer_balance_array['BALANCE']+$totalAmount)-$saleDetails['RECEIVED_CASH']),2) ?>
																		</span>
																	</td>
																</tr>
																<?php }else{
																	$cutomer_balance_array['BALANCE'] = 0;
																} ?>
															</tfoot>
														</table>
														<div class="clear"></div>

														<?php if($saleDetails['NOTES'] != '' && $notes_enclosures == 'Y'){ ?>
															<div class="infoPanel pull-left" style="position:relative;top:0px;">
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;font-size:14px;border-bottom:1px solid #000;">Notes &amp; Enclosures</span>
																</span>
																<div class="clear" style="height:10px;"></div>
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;;font-size:14px;text-align:justify !important;"><?php echo $saleDetails['NOTES']; ?></span>
																</span>
																<div class="clear"></div>
															</div>
															<?php } ?>
															<div class="clear"></div>

															<div class="invoice_footer <?php echo $show_header=='N'?"opacity_zero":""; ?>" style="position:absolute !important;bottom:5px !important;">
																<div class="auths pull-left">
																	Prepared By
																	<span class="authorized"></span>
																</div><!--partyTitle-->
																<div class="auths pull-right">
																	Authorized By
																</div><!--partyTitle-->
																<div class="clear" style="height: 10px;"></div>
																<div class="invoice-developer-info text-right"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
															</div>
														</div><!--invoiceBody-->
													</div><!--invoiceLeftPrint-->
													<div class="clear"></div>
												</div><!--invoiceContainer-->
											</div><!--printThisDiv-->
										</div><!--invoiceBody-->
										<?php
									}
									?>
								</body>
								</html>
								<?php include('conn.close.php'); ?>
