<?php
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/currency_types.php');

    $objCurrecyTypes    = new CurrecyTypes();

    if(isset($_POST['delete_id'])){
        $ret = array();
        $ret['OK'] = 'N';
        $ret['MSG'] = 'Record Cant Be Deleted!';

        $delete_id = mysql_real_escape_string($_POST['delete_id']);
        if($delete_id > 0){
            $used_records   = $objCurrecyTypes->in_use($delete_id);
            $process_detail = $objCurrecyTypes->getRecordDetailsByID($delete_id);
            if($used_records == 0){
                $deleted = $objCurrecyTypes->delete($delete_id);
                if($deleted){
                    $ret['OK'] = 'Y';
                    $ret['MSG'] = 'Record deleted Successfully!';
                }
            }
        }
        echo json_encode($ret);
        exit();
    }

?>

    <!DOCTYPE html 
        >

    

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $.fn.centerThisDiv = function(){
                    var win_hi = $(window).height()/2;
                    var win_width = $(window).width()/2;
                    win_hi = win_hi-$(this).height()/2;
                    win_width = win_width-$(this).width()/2;
                    $(this).css({
                        'position': 'fixed',
                        'top': win_hi,
                        'left': win_width
                    });
                };
                $("a.pointer").click(function(){
                    var row_id   = parseInt($(this).attr('data-id'))||0;
                    var this_elm = $(this);
                    $("#fade").hide();
                    $("#popUpDel").remove();
                    $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
                    $("#popUpDel").hide();
                    $("#popUpDel").centerThisDiv();
                    $("#fade").fadeIn('slow');
                    $("#popUpDel").fadeIn();
                    $(".dodelete").click(function(){
                            if(row_id > 0){
                                $.post('currency-type-management.php',{delete_id:row_id},function(data){
                                    if(data != ''){
                                        data = $.parseJSON(data);
                                        if(data['OK'] == 'Y'){
                                            $(this_elm).parent().parent().remove();
                                            $("#popUpDel .confirm").text(data['MSG']);
                                            $(".dodelete").hide();
                                            $(".nodelete").text('Close');
                                        }else{
                                            $("#popUpDel .confirm").text(data['MSG']);
                                            $(".dodelete").hide();
                                            $(".nodelete").text('Close');
                                        }
                                    }
                                });
                            }
                        });
                    $(".nodelete").click(function(){
                        $("#fade").fadeOut();
                        $("#popUpDel").fadeOut();
                    });
                    $(".close_popup").click(function(){
                        $("#popUpDel").slideUp();
                        $("#fade").fadeOut('fast');
                    });
                });
            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

    <?php

        $mode = "";
        if(!isset($_GET['search']))
        {
            $path = "currency-type-management.php?title=&search=Search";
            if(isset($_GET['tab'])){
                $path .= "&tab=search";
            }
            echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        }
        //$mode = $_GET['search'];

        if(isset($_GET['search'])){
            $end = 10;

            $objCurrecyTypes->name           = $_GET['title'];
            $myPage = 1;
            $start = 0;
            if(isset($_GET['page']))
            {
                $page = $_GET['page'];
                $myPage = $page;
                $start = ((int)$page * $end) - $end;
            }
            $itemCategoryList = $objCurrecyTypes->searchItemCategory($start,$end);
            $row          = mysql_num_rows($itemCategoryList);

            $queryCount = $objCurrecyTypes->totalMatchRecords;
            $pageCount = ceil($queryCount/$end);
        }else{
            $itemCategoryList = $objCurrecyTypes->getList();
            $row          = mysql_num_rows($itemCategoryList);
        }


    ?>
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class="content-box-header">
                  <p>Currency Type Management</p>
                  <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1">List</div>
                        <a href="add-currency-type.php"><div class="tab">New</div></a>
                    </div>
                  </span>
                  <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>
                <div id = "bodyTab1">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th width="5%" style="text-align:center">Symbol</th>
                            <th width="85%" style="text-align:center">Currency</th>
                            <th width="10%" style="text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($row>0){

                            while($itemCategoryDetails = mysql_fetch_assoc($itemCategoryList)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="text-center"><button class="btn btn-default"><?php echo $itemCategoryDetails['SYMBOL']; ?></button></td>
                                    <td class="text-left" style="padding-left:10px;"><?php echo $itemCategoryDetails['TITLE']; ?></td>
                                    <td style="text-align:center">
                                        <a id="view_button" href="add-currency-type.php?id=<?php echo $itemCategoryDetails['ID']; ?>"><i class="fa fa-pencil"></i></a>
                                        <a data-id="<?php echo $itemCategoryDetails['ID']; ?>" class="pointer" ><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <table align="center" border="0" id="navbar">
                        <tr>
                            <td style="text-align: center">
                                <?php
                                if(isset($queryCount)){
                                    if($queryCount>$pageCount){
                                        ?>
                                        <nav>
                                            <ul class="pagination">
                                                <?php
                                                if($myPage > 1)
                                                {
                                                    echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=1'>Fst</a></li>";
                                                    echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=". ($myPage-1) ."'>Prv</a></li>";
                                                }

                                                for($x=$myPage-4; $x<$myPage; $x++)
                                                {
                                                    if($x > 0){
                                                        echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=". $x ."'>". $x ."</a></li>";
                                                    }
                                                }

                                                print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                                                for($y=$myPage+1; $y<$myPage+5; $y++)
                                                {
                                                    if($y <= $pageCount){
                                                        echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=". $y ."'>". $y ."</a></li>";
                                                    }
                                                }

                                                if($myPage < $pageCount)
                                                {
                                                    echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=". ($myPage+1) ."'>Nxt</a></li>";
                                                    echo "<li><a href='currency-type-management.php?title=".$_GET['title']."&search=Search&page=". $pageCount ."'>Lst</a></li>";
                                                }

                                                ?>
                                            </ul>
                                        </nav>

                                    <?php }
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </div> <!--bodyTab1-->

                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Product :</div>

                            <div class="caption">Item Category Title :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="title" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption"></div>
                            <div class = "field">
                                <input type="submit" name="search" value="Search" class="button" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab1-->
                <div class="clear" style="height:30px"></div>



                      <!-- Delete confirmation popup -->
                <div id="myConfirm" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
    </div><!--bodyWrapper---->
    <div id="fade"></div>
    <div id="xfade"></div>
    </body>
</html>
<?php include('conn.close.php'); ?>
