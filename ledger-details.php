<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_billing.php');
	include('common/classes/ledger_report.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('general-ledger',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts       = new ChartOfAccounts();
	$objJournalVoucher = new JournalVoucher();
	$objLedgerReport   = new ledgerReport();
	$objConfigs        = new Configs();
	$objEmbBilling     = new EmbBilling();
	$objCompanyDetails = new CompanyDetails();

	$company_details   = $objCompanyDetails->getActiveProfile();

/*
	$objJournalVoucher->voucherId				= 0;
	$objJournalVoucher->voucherDate 		= date('Y-m-d');
	$objJournalVoucher->voucherType 		= 'JV';
	$objJournalVoucher->type_num    		= 0;
	$objJournalVoucher->jVoucherNum 		= $objJournalVoucher->genJvNumber();
	$objJournalVoucher->reference 			= "Opening Balance";
	$objJournalVoucher->reference_date  = date('Y-m-d');

	exit();
	$objJournalVoucher->voucherId = $objJournalVoucher->saveVoucher();

	if($objJournalVoucher->voucherId==0){
		exit();
	}

	echo "<pre>";

	$file = fopen('temp/warisha-tb.csv','r');
	$query = '';
	$success = false;
	while(!feof($file)){
		$line = fgets($file);
		if(stripos($line,',')==FALSE){
			continue;
		}
		$line_array = explode(',',$line);
		if(isset($line_array[4])){
			unset($line_array[4]);
		}
		if(stripos($line_array[0],'10-101')!==FALSE){
			$line_array[0] = '050101'; //owner's equity
		}
		if(stripos($line_array[0],'11-101')!==FALSE){
			$line_array[0] = '050201'; //Reserve surplus
		}
		if(stripos($line_array[0],'12-101')!==FALSE){
			$line_array[0] = '040201'; //Other Liabilities
		}
		if(stripos($line_array[0],'12-102')!==FALSE){
			$line_array[0] = '040202'; //Salaries Payable
		}
		if(stripos($line_array[0],'15-101')!==FALSE){
			$line_array[0] = '040101'; //Store Creditors
		}
		if(stripos($line_array[0],'15-102')!==FALSE){
			$line_array[0] = '040102'; //Emb Creditors
		}
		if(stripos($line_array[0],'15-103')!==FALSE){
			$line_array[0] = '040103'; //Conversion Creditors
		}
		if(stripos($line_array[0],'15-104')!==FALSE){
			$line_array[0] = '040104'; //Other Creditors
		}
		if(stripos($line_array[0],'15-107')!==FALSE){
			$line_array[0] = '040106'; //Import Creditors
		}
		if(stripos($line_array[0],'16-101')!==FALSE){
			$line_array[0] = '010201'; //CAPITAL W.I.P
		}
		if(stripos($line_array[0],'16-102')!==FALSE){
			$line_array[0] = '010202'; //Office Equipments
		}
		if(stripos($line_array[0],'16-105')!==FALSE){
			$line_array[0] = '010203'; //Machines Imported
		}
		if(stripos($line_array[0],'22-101')!==FALSE){
			$line_array[0] = '010301'; //Stock In Process - Emb
		}
		if(stripos($line_array[0],'22-102')!==FALSE){
			$line_array[0] = '010302'; //Stock In Process - Spares
		}
		if(stripos($line_array[0],'25-101')!==FALSE){
			$line_array[0] = '010401'; //Conversion Debtors
		}
		if(stripos($line_array[0],'25-103')!==FALSE){
			$line_array[0] = '010402'; //Embroidery Debtors
		}
		if(stripos($line_array[0],'25-104')!==FALSE){
			$line_array[0] = '010403'; //Other Receivables
		}
		if(stripos($line_array[0],'25-105')!==FALSE){
			$line_array[0] = '010101'; //Cash & Banks
		}
		if(stripos($line_array[0],'25-106')!==FALSE){
			$line_array[0] = '010404'; //Spangle Debtors
		}
		if(stripos($line_array[0],'26-101')!==FALSE){
			$line_array[0] = '010501'; //Advances to Staff
		}
		if(stripos($line_array[0],'26-102')!==FALSE){
			$line_array[0] = '010502'; //Advance Taxes
		}
		if(stripos($line_array[0],'26-103')!==FALSE){
			$line_array[0] = '010503'; //Other Advances
		}
		if(stripos($line_array[0],'26-104')!==FALSE){
			$line_array[0] = '010504'; //Deposits
		}
		if(stripos($line_array[0],'26-105')!==FALSE){
			$line_array[0] = '010505'; //Drawing A/c
		}
		if(stripos($line_array[0],'30-101')!==FALSE){
			$line_array[0] = '020101'; //Revenue Emb
		}
		if(stripos($line_array[0],'30-104')!==FALSE){
			$line_array[0] = '020102'; //Other Income
		}
		if(stripos($line_array[0],'34-101')!==FALSE){
			$line_array[0] = '030101'; //Direct Expenses (Purchases)
		}
		if(stripos($line_array[0],'34-104')!==FALSE){
			$line_array[0] = '030102'; //Cost of Sales
		}
		if(stripos($line_array[0],'34-105')!==FALSE){
			$line_array[0] = '030103'; //Manufacturing & Processing Expenses
		}
		if(stripos($line_array[0],'34-106')!==FALSE){
			$line_array[0] = '030104'; // Admin & Operating Expenses
		}
		if(stripos($line_array[0],'34-107')!==FALSE){
			$line_array[0] = '030105'; // SELLING Expenses
		}
		if(stripos($line_array[0],'34-108')!==FALSE){
			$line_array[0] = '030106'; // BANK Charges & Others
		}
		if(stripos($line_array[0],'34-109')!==FALSE){
			$line_array[0] = '030107'; // Other Expenses
		}
		if(stripos($line_array[0],'50-101')!==FALSE){
			$line_array[0] = '030201'; // Other A/C
		}
		if(stripos($line_array[0],'50-102')!==FALSE){
			$line_array[0] = '030202'; // Other A/C 2
		}

		print_r($line_array);
		$account_title = mysql_real_escape_string($line_array[1]);
		$objAccounts->addSubMainChild($account_title,$line_array[0]);
		$account_code = $objAccounts->new_account_code;

		$objJournalVoucher->accountCode  = $account_code;
		$objJournalVoucher->accountTitle = $account_title;
		$objJournalVoucher->narration   = "OPENING BALANCE AS ON 23-06-2016";
		$objJournalVoucher->narration 	  = mysql_real_escape_string($objJournalVoucher->narration);
		$objJournalVoucher->instrumentNo = 0;
		$debit       = (float)str_ireplace(',','',$line_array[2]);
		$credit      = (float)str_ireplace(',','',$line_array[3]);

		if($debit > 0 && $credit == ''){
			$objJournalVoucher->transactionType = 'Dr';
			$objJournalVoucher->amount = $debit;
		}elseif($credit > 0 && $debit == ''){
			$objJournalVoucher->transactionType = 'Cr';
			$objJournalVoucher->amount = $credit;
		}
		$objJournalVoucher->saveVoucherDetail();
	}
	echo "</pre>";
  exit();
		*/

	if(isset($_GET['calc'])){
		exit();
		//$account_codes = $objJournalVoucher->get_accounts();
		//if(mysql_num_rows($account_codes)){
			//while($account = mysql_fetch_array($account_codes)){
				//$acc_code = $account['ACCOUNT_CODE'];
				$acc_code = '0301040001';
				$transactions = $objJournalVoucher->get_transactioins($acc_code);
				if(mysql_num_rows($transactions)){
					$balance = 0;
					while($row = mysql_fetch_array($transactions)){
						$amount 		  = $row['AMOUNT'];
						$account_type 	  = substr($row['ACCOUNT_CODE'],0,2);
						$transaction_type = $row['TRANSACTION_TYPE'];
						if($transaction_type == 'Dr' && ($account_type == '01' || $account_type == '03')){
							$balance += $amount;
						}elseif($transaction_type  == 'Cr' && ($account_type == '01' || $account_type == '03')){
							$balance -= $amount;
						}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $transaction_type  == 'Cr'){
							$balance += $amount;
						}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $transaction_type  == 'Dr'){
							$balance -= $amount;
						}
						$objJournalVoucher->setBalance($row['VOUCH_DETAIL_ID'],$balance);
					}
				}
				echo $balance;
				echo "<hr />";
			//}
		//}
		exit();
	}

	$accountsList   = $objAccounts->getLevelFourList();
	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);
	$invoiceSize    = $invoice_format[0]; // S  L

	if($invoiceSize == 'L'){
		$invoiceFile = 'sales-invoice.php';
		$pInvoiceFile = 'purchase-invoice.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile = 'sales-invoice-duplicates.php';
		$pInvoiceFile = 'purchase-invoice.php';
	}elseif($invoiceSize == 'S'){
		$invoiceFile = 'sales-invoice-small.php';
		$pInvoiceFile = 'purchase-bill-small.php';
	}

	if(isset($_GET['accountCode'])){
		$postedAccCode 							= $_GET['accountCode'];
		$postedAccTitle 						= $objAccounts->getAccountTitleByCode($postedAccCode);
		$_GET['fromDate'] 					=  isset($_GET['fromDate'])?$_GET['fromDate']:'';
		$postedfromDate 						= ($_GET['fromDate']=="")?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objLedgerReport->po_number = isset($_GET['po_no'])?$_GET['po_no']:"";
		if($postedfromDate==''){
			$thisYear 							= date('Y');
			$firstJanuary 					= '01-01';
			$firstJanuaryThisYear   = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
			$postedfromDate 				= $firstJanuaryThisYear;
		}
		$postedtoDate = ($_GET['toDate']=="")?"":date('Y-m-d',strtotime($_GET['toDate']));
		if($postedAccCode != ''){
			$voucherDetailsList     			= $objLedgerReport->getVoucherDetailListByDateRange($postedfromDate,$postedtoDate,$postedAccCode);
			$dateForBalance 							= date('Y-m-d',strtotime($postedfromDate." - 1 days"));
			$ledgerOpeningBalance   			= $objLedgerReport->getLedgerOpeningBalance($dateForBalance,$postedAccCode);
			$account_type 								= substr($postedAccCode,0,2);
			if($ledgerOpeningBalance >= 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType 	= 'Dr';
			}elseif($ledgerOpeningBalance < 0 && ($account_type == '01' || $account_type == '03')){
				$ledgerOpeningBalanceType 	= 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance >= 0){
				$ledgerOpeningBalanceType 	= 'Cr';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance < 0){
				$ledgerOpeningBalanceType 	= 'Dr';
			}
			$ledgerOpeningBalanceSigned 	= (float)$ledgerOpeningBalance;
			$ledgerOpeningBalance 				= (float)str_replace('-','',$ledgerOpeningBalance);
			$ledgerOpeningBalanceType   	= strtoupper($ledgerOpeningBalanceType);
		}else{
			$message = "Account was not selected !";
		}
	}
?>
<?php if(!isset($_GET['csv'])){ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript" src="resource/scripts/reports.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript">
		$(window).on('load',function(){
			$('.selectpicker').selectpicker();
			$(".printThis").click(function(){
				$(".printTable").printThis({
					debug: false,
					importCSS: false,
					printContainer: true,
					loadCSS: 'resource/css/reports.css',
					pageTitle: "Sitsbook.net",
					removeInline: false,
					printDelay: 500,
					header: null
				});
			});
		});
	</script>
</head>
<body>
	<div 	 id="body-wrapper">
		<div id="sidebar"><?php include("common/left_menu.php") ?></div>
		<div id="bodyWrapper">
			<div class="content-box-top">
				<div class="summery_body">
					<div class="content-box-header">
						<p >General Ledger Reporting</p>
						<div class="clear"></div>
					</div><!-- End .content-box-header -->
					<div id="bodyTab1">
						<div class="clear"></div>
						<div id="form" style="margin: 0 auto; width: 900px;">
							<form method="get" action="">
								<?php
								if(isset($message)){
									?>
									<p style="text-align:center;color:red;"><?php echo $message; ?></p>
									<?php
								}
								?>
								<div class="caption">Account Title :</div>
								<div class="field" style="width:250px;position:relative;">
									<select class="selectpicker show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none" name="accountCode">
										<option selected value=""></option>
										<?php
										if(mysql_num_rows($accountsList)){
											while($account = mysql_fetch_array($accountsList)){
												$selected = (isset($postedAccCode) && $postedAccCode == $account['ACC_CODE'])?"selected='selected'":"";
												?>
												<option value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_CODE']; ?> - <?php echo $account['ACC_TITLE']; ?></option>
												<?php
											}
										}
										?>
									</select>
								</div>

								<div class="caption"> PO #:</div>
								<div class="field" style="width:150px">
									<input type="text" name="po_no" class="form-control" style="width:145px" />
								</div>

								<div class="clear"></div>

								<div class="caption">Start Date:</div>
								<div class="field" style="width:150px">
									<input type="text" name="fromDate" class="form-control datepicker" style="width:145px" />
								</div>

								<div class="caption" style="margin-left:120px">End Date:</div>
								<div class="field" style="width:150px">
									<input type="text" name="toDate" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" style="width:145px" />
								</div>
								<div class="clear"></div>

								<div class="caption"></div>
								<div class="field" style="text-align:left">
									<input type="submit" value="Generate" name="generalReport" class="button"/>
								</div>
								<div class="clear"></div>
							</form>
						</div><!--form-->
						<div class="clear"></div>
					</div> <!-- End bodyTab1 -->
				</div> <!-- End .content-box-content -->

				<div class="col-xs-12">
				<?php
				if(isset($voucherDetailsList)){
					?>
					<div class="content-box-content">
						<span style="float:right;">
							<button class="button printThis">Print</button>
						</span>
						<?php
							$export_url = '';
							foreach ($_GET as $key => $value) {
								$export_url .= $key."=".$value."&";
							}
						 ?>
						<a target="_blank" href="ledger-details.php?<?php echo $export_url; ?>&csv" class="button pull-left">Export CSV</a>
						<div id="bodyTab" class='printTable' style="margin: 0 auto;">
							<div style="text-align:left;margin-bottom:20px;" class="pageHeader">
								<p style="font-size:18px;padding:0;margin: 0px !important;text-align:center;"><?php echo $company_details['NAME']; ?></p>
								<p style="font-size:18px;padding:0;margin: 0px !important;text-align:center;">Account :<?php echo $postedAccTitle; ?> (<?php echo $postedAccCode; ?>) </p>
								<p style="font-size:12px;padding:0;text-align:center;">
									General Ledger Details
									From : <u> <?php echo date('d-m-Y',strtotime($postedfromDate)); ?></u> to : <u> <?php echo date('d-m-Y',strtotime($postedtoDate)); ?></u>
								</p>
								<p style="float:left; margin-left:0px; font-size:14px" class="repoGen">
									Report generated on : <?php echo date('d-m-Y'); ?>
								</p>
							</div>
							<div style="clear:both; height:1;"></div>
							<table style="margin:0 10px; width:98%;" cellspacing="0" class="tableBreak">

								<thead class="tHeader">
									<tr>
										<th width="8%"  colspan="2" style="text-align:center;border:1px solid #ddd;" >Ref</th>
										<th width="10%" style="border:1px solid #ddd;"  >EntryDate</th>
										<th width="45%" style="border:1px solid #ddd;" >Description</th>
										<th width="10%" style="text-align:center;border:1px solid #ddd;">Debit</th>
										<th width="10%" style="text-align:center;border:1px solid #ddd;">Credit</th>
										<th width="15%" style="text-align:center;border:1px solid #ddd;">Balance</th>
									</tr>
								</thead>

								<tbody>
									<?php
									if(mysql_num_rows($voucherDetailsList)){
										$debitTotal = 0;
										$creditTotal = 0;
										$balance_column = 0;
										?>
										<tr>
											<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
											<td style="text-align:right;border-right:1px solid #E5E5E5;border-left:none">Opening Balance:</td>
											<td colspan="2" style="border-right:none;border-left:none"></td>
											<td style="border-left:none;border-right:none;text-align:right">
												<?php echo $ledgerOpeningBalance; ?>
												<?php echo $ledgerOpeningBalanceType; ?>
											</td>
										</tr>
<?php
										$closingBalance     = $ledgerOpeningBalance;
										$closingBalanceType = $ledgerOpeningBalanceType;
										while($row = mysql_fetch_array($voucherDetailsList)){
											$ledgerOpeningBalanceSigned = (float)($ledgerOpeningBalanceSigned);
											if($objLedgerReport->is_reversal_vouhcer($row['VOUCHER_ID']) > 0){
												continue;
											}
											$row['TRANSACTION_TYPE'] = strtoupper($row['TRANSACTION_TYPE']);
											$row['NARRATION'] = substr($row['NARRATION'],0,100);
											$voucherType = $row['VOUCHER_TYPE'];
											if($row['VOUCHER_TYPE'] == 'SV'){
												$invoice_id = (int)($objLedgerReport->getInvoiceId($row['VOUCHER_ID']));
												if($invoice_id){
													$row['NARRATION'] = '<a class="text-primary" target="_blank" href="new-invoice-print.php?id='.$invoice_id.'">'.$row['NARRATION'].'</a>';
												}else{
													$sale_id = (int)($objLedgerReport->getSaleId($row['VOUCHER_ID']));
													if($sale_id){
														$row['NARRATION'] = '<a class="text-primary" target="_blank" href="'.$invoiceFile.'?id='.$sale_id.'">'.$row['NARRATION'].'</a>';
													}else{
														$emb_bill_id = $objEmbBilling->getRecordIdByVoucherId($row['VOUCHER_ID']);
														if($emb_bill_id){
															$row['NARRATION'] = '<a class="text-primary" target="_blank" href="emb-billing-print.php?id='.$emb_bill_id.'">'.$row['NARRATION'].'</a>';
														}
													}
												}
											}
											if($row['VOUCHER_TYPE'] == 'PV'){
												$purchase_id = (int)($objLedgerReport->getPurchaseId($row['VOUCHER_ID']));
												if($purchase_id){
													$row['NARRATION'] = '<a class="text-warning" target="_blank" href="'.$pInvoiceFile.'?id='.$purchase_id.'">'.$row['NARRATION'].'</a>';
												}
											}
											if(($account_type == '01'||$account_type == '03')&&$row['TRANSACTION_TYPE']=='DR'){
												$ledgerOpeningBalanceSigned += (float)$row['AMOUNT'];
											}elseif(($account_type == '01'||$account_type == '03')&&$row['TRANSACTION_TYPE']=='CR'){
												$ledgerOpeningBalanceSigned -= (float)$row['AMOUNT'];
											}
											if(($account_type == '02'||$account_type == '04'||$account_type == '05')&&$row['TRANSACTION_TYPE']=='DR'){
												$ledgerOpeningBalanceSigned -= (float)$row['AMOUNT'];
											}elseif(($account_type == '02'||$account_type == '04'||$account_type == '05')&&$row['TRANSACTION_TYPE']=='CR'){
												$ledgerOpeningBalanceSigned += (float)$row['AMOUNT'];
											}

											if($account_type == '01'||$account_type == '03'){
												if($ledgerOpeningBalanceSigned >= 0){
													$closingBalanceType = 'DR';
												}else{
													$closingBalanceType = 'CR';
												}
											}elseif($account_type == '02'||$account_type == '04'||$account_type == '05'){
												if($ledgerOpeningBalanceSigned >= 0){
													$closingBalanceType = 'CR';
												}else{
													$closingBalanceType = 'DR';
												}
											}
											$print_ledgerOpeningBalanceSigned = (float)(str_replace("-",'',$ledgerOpeningBalanceSigned));
											?>
											<tr>
												<td style="text-align:center"><?php echo $voucherType; ?>#</td>
												<td style="text-align:center"> <a href="voucher-print.php?id=<?php echo $row['VOUCHER_ID']; ?>" target="_blank"><?php echo $row['VOUCHER_NO']; ?></a></td>
												<td align="left"><?php echo date('d-m-Y',strtotime($row['VOUCHER_DATE'])); ?></td>
												<td align="left"><?php echo $row['NARRATION']; ?></td>
												<td style="text-align:right" class="debitColumn"><?php echo ($row['TRANSACTION_TYPE']=='DR')?$row['AMOUNT']:""; ?></td>
												<td style="text-align:right;"  class="creditColumn"><?php echo ($row['TRANSACTION_TYPE']=='CR')?$row['AMOUNT']:""; ?></td>
												<td style="text-align: right;"><?php echo number_format(str_replace('-','',$row['BALANCE']),2)." ".$closingBalanceType; ?></td>
											</tr>
											<?php
											$debitTotal  += ($row['TRANSACTION_TYPE']=='DR')?$row['AMOUNT']:0;
											$creditTotal += ($row['TRANSACTION_TYPE']=='CR')?$row['AMOUNT']:0;
										}
									}else{

										?>
										<tr>
											<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
											<td colspan="3" style="text-align:right;border-right:1px solid #666;;border-left:none">Opening Balance:</td>
											<td style="border-left:none;border-right:none;text-align:right">
												<?php echo number_format($ledgerOpeningBalance,2); ?>
												<?php echo $ledgerOpeningBalanceType; ?>
											</td>
										</tr>
										<tr>
											<th colspan="10" style="text-align:center;">Found 0 Enteries</th>
										</tr>
										<tr>
											<td colspan="3" style="border-right:none;border-left:none;background-color: #f8f8f8;font-weight:normal;"></td>
											<td colspan="3" style="text-align:right;border-right:1px solid #666;;border-left:none">Closing Balance:</td>
											<td style="border-left:none;border-right:none;text-align:right">
												<?php echo number_format($ledgerOpeningBalance,2); ?>
												<?php echo $ledgerOpeningBalanceType; ?>
											</td>
										</tr>
<?php
									}
									if(mysql_num_rows($voucherDetailsList)){//second numrows
?>
										<tr style="background:none">
											<td colspan="4" style="text-align:right;border:1px solid #e5e5e5;">Period Total:</td>
											<td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($debitTotal,2); ?></td>
											<td style="border:1px solid #e5e5e5;text-align:right"><?php echo number_format($creditTotal,2); ?></td>
											<td style="border-left:none;text-align:right">

											</td>
										</tr>
										<tr>
											<td colspan="4" style="text-align:right;">Closing Balance:</td>
											<td colspan="2" style=""></td>
											<td style="text-align:right" class="closingBalance">
												<?php $closingBalance = str_replace('-','',$ledgerOpeningBalanceSigned) ?>
												<?php echo number_format($closingBalance,2); ?>
												<?php echo strtoupper($closingBalanceType); ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div><!--bodyTab2-->
							<?php
						}//second numrows end
					}
					?>
					</div>
				</div><!--summery_body-->
			</div><!-- End .content-box -->
		</div><!--bodyWrapper-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
$(window).load(function(){
	$("").focus();
});
</script>
<?php }else{
				// output headers so that the file is downloaded rather than displayed
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=sale('.date('d-M-Y').').csv');

				// create a file pointer connected to the output stream
				$output = fopen('php://output', 'w');

				$array_col_titles   = array();
				$array_col_titles[] =  "Ref#";
				$array_col_titles[] =  "Entry Date";
				$array_col_titles[] =  "Description";
				$array_col_titles[] =  "Debit";
				$array_col_titles[] =  "Credit";
				$array_col_titles[] =  "Balance";


				$closingBalance     = $ledgerOpeningBalance;
				$closingBalanceType = $ledgerOpeningBalanceType;

				 // output the column headings
				 fputcsv($output, $array_col_titles);

				 $csv_row 					= array();
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "Opening Balance";
				 $csv_row[] = $ledgerOpeningBalance;

				 fputcsv($output, $csv_row);

				 $debit_total  = 0;
				 $credit_total = 0;
				 if(mysql_num_rows($voucherDetailsList)){
					 while($row = mysql_fetch_array($voucherDetailsList)){
						 $ledgerOpeningBalanceSigned = (float)($ledgerOpeningBalanceSigned);
						 if($objLedgerReport->is_reversal_vouhcer($row['VOUCHER_ID']) > 0){
							 continue;
						 }
						 $row['TRANSACTION_TYPE'] = strtoupper($row['TRANSACTION_TYPE']);
						 $row['NARRATION'] = substr($row['NARRATION'],0,100);
						 $voucherType = $row['VOUCHER_TYPE'];
						 if(($account_type == '01'||$account_type == '03')&&$row['TRANSACTION_TYPE']=='DR'){
							 $ledgerOpeningBalanceSigned += (float)$row['AMOUNT'];
						 }elseif(($account_type == '01'||$account_type == '03')&&$row['TRANSACTION_TYPE']=='CR'){
							 $ledgerOpeningBalanceSigned -= (float)$row['AMOUNT'];
						 }
						 if(($account_type == '02'||$account_type == '04'||$account_type == '05')&&$row['TRANSACTION_TYPE']=='DR'){
							 $ledgerOpeningBalanceSigned -= (float)$row['AMOUNT'];
						 }elseif(($account_type == '02'||$account_type == '04'||$account_type == '05')&&$row['TRANSACTION_TYPE']=='CR'){
							 $ledgerOpeningBalanceSigned += (float)$row['AMOUNT'];
						 }

						 if($account_type == '01'||$account_type == '03'){
							 if($ledgerOpeningBalanceSigned >= 0){
								 $closingBalanceType = 'DR';
							 }else{
								 $closingBalanceType = 'CR';
							 }
						 }elseif($account_type == '02'||$account_type == '04'||$account_type == '05'){
							 if($ledgerOpeningBalanceSigned >= 0){
								 $closingBalanceType = 'CR';
							 }else{
								 $closingBalanceType = 'DR';
							 }
						 }
						 $print_ledgerOpeningBalanceSigned = (float)(str_replace("-",'',$ledgerOpeningBalanceSigned));

						 $csv_row 	= array();
						 $csv_row[] = $voucherType." ".$row['VOUCHER_NO'];
						 $csv_row[] = date("d-m-Y",strtotime($row['VOUCHER_DATE']));
						 $csv_row[] = $row['NARRATION'];
						 $csv_row[] = $row['TRANSACTION_TYPE']=='DR'?number_format($row['AMOUNT'],2,'.',''):"";
						 $csv_row[] = $row['TRANSACTION_TYPE']=='CR'?number_format($row['AMOUNT'],2,'.',''):"";
						 $csv_row[] = number_format(str_replace('-','',$row['BALANCE']),2,'.','')." ".$closingBalanceType;
						 fputcsv($output, $csv_row);

						 $debit_total  += $row['TRANSACTION_TYPE']=='DR'?$row['AMOUNT']:0;
						 $credit_total += $row['TRANSACTION_TYPE']=='CR'?$row['AMOUNT']:0;
					 }
				 }

				 $csv_row 	= array();
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "Period Total";
				 $csv_row[] = "";
				 $csv_row[] = number_format($debit_total,2,'.','');
				 $csv_row[] = number_format($credit_total,2,'.','');
				 fputcsv($output, $csv_row);

				 $closingBalance = str_replace('-','',$ledgerOpeningBalanceSigned);
				 $closingBalance = number_format($closingBalance,2);
				 $closingBalanceType = strtoupper($closingBalanceType);

				 $csv_row 	= array();
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "";
				 $csv_row[] = "Closing Balance";
				 $csv_row[] = $closingBalance." ".$closingBalanceType;
				 fputcsv($output, $csv_row);

	 }
?>
