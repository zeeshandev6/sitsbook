<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/inventory-return.php');
	include('common/classes/inventory-return-details.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('open-invoices',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes            = new ChartOfAccounts();
	$objInventory               = new inventory();
	$objInventoryDetails        = new inventory_details();
	$objInventoryReturn         = new InventoryReturn();
	$objInventoryReturnDetails  = new InventoryReturnDetails();
	$objSale			  		= new Sale();
	$objSaleDetails		  		= new SaleDetails();
	$objSaleReturn        		= new SaleReturn();
	$objSaleReturnDetails 		= new SaleReturnDetails();
	$objItems             		= new Items();
	$objItemCategory      		= new itemCategory();
	$objTaxRates          		= new TaxRates();
	$objDepartments       		= new Departments();
	$objConfigs 		  		= new Configs();

	$suppliersList        = $objInventory->getSuppliersList();
	$cashAccounts         = $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList          = $objTaxRates->getList();

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   = $objConfigs->get_config('USE_CARTONS');

	$titleRepo     = '';
	$report_date   = '';

	$report_array = array();

	if(isset($_GET['search'])){
		$report_date 		= ($_GET['report_date']=='')?"":date('Y-m-d',strtotime($_GET['report_date']));

		$purchaseReport      = $objInventory->purchaseTaxItemsList($report_date);
		$purchaseReturnReport= $objInventoryReturn->purchaseTaxItemsList($report_date);
		$saleTaxReport 	     = $objSale->saleTaxItemsList($report_date);
		$saleReturnReport 	 = $objSaleReturn->saleTaxItemsList($report_date);

		if(mysql_num_rows($purchaseReport)){
			while($row = mysql_fetch_assoc($purchaseReport)){
				if(!isset($report_array[$row['ITEM_ID']])){
					$report_array[$row['ITEM_ID']] = array();
				}
				if(!isset($report_array[$row['ITEM_ID']]['QTY'])){
					$report_array[$row['ITEM_ID']]['QTY'] = 0;
				}
				$report_array[$row['ITEM_ID']]['QTY'] += $row['TOTAL_QTY'];
				$report_array[$row['ITEM_ID']]['RATE'] = $row['AVG_COST'];
			}
		}

		if(mysql_num_rows($purchaseReturnReport)){
			while($row = mysql_fetch_assoc($purchaseReturnReport)){
				if(!isset($report_array[$row['ITEM_ID']])){
					$report_array[$row['ITEM_ID']] = array();
				}
				if(!isset($report_array[$row['ITEM_ID']]['QTY'])){
					$report_array[$row['ITEM_ID']]['QTY'] = 0;
				}
				$report_array[$row['ITEM_ID']]['QTY'] -= $row['TOTAL_QTY'];
			}
		}

		if(mysql_num_rows($saleTaxReport)){
			while($row = mysql_fetch_assoc($saleTaxReport)){
				if(!isset($report_array[$row['ITEM_ID']])){
					$report_array[$row['ITEM_ID']] = array();
				}
				if(!isset($report_array[$row['ITEM_ID']]['QTY'])){
					$report_array[$row['ITEM_ID']]['QTY'] = 0;
				}
				$report_array[$row['ITEM_ID']]['QTY'] -= $row['TOTAL_QTY'];
			}
		}

		if(mysql_num_rows($saleReturnReport)){
			while($row = mysql_fetch_assoc($saleReturnReport)){
				if(!isset($report_array[$row['ITEM_ID']])){
					$report_array[$row['ITEM_ID']] = array();
				}
				if(!isset($report_array[$row['ITEM_ID']]['QTY'])){
					$report_array[$row['ITEM_ID']]['QTY'] = 0;
				}
				$report_array[$row['ITEM_ID']]['QTY'] += $row['TOTAL_QTY'];
			}
		}

	}
?>
<!DOCTYPE html>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 450;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}
					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();

                $("div.tablePage").each(function(i,e){
                    $(this).prepend($(".pageHeader").first().clone());
                });
                $(".pageHeader").first().remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Sit Solution",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('.itemSelector').selectpicker();
			$('.supplierSelector').selectpicker();
			$("select[name=user_id]").selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>FBR Stock Position</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="get" action="">
                        <div class="caption">Report Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="report_date" value="<?php echo ($report_date=="")?"":date('d-m-Y',strtotime($report_date)); ?>" class="form-control datepicker" style="width:145px;" required />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />

<?php
					if(isset($purchaseReport)&&isset($saleTaxReport)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">FBR Stock Report </p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            	<?php echo ($report_date=="")?"":"Report Date : ".date('d-m-Y',strtotime($report_date)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="25%" style="font-size:12px !important ;text-align:center">Item</th>
                                   <th width="5%" style="font-size:12px !important ;text-align:center">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_qty 		= 0;
								foreach($report_array as $item_id => $row){
									$itemDetail = $objItems->getRecordDetails($item_id);
?>
                                <tr id="recordPanel" class="alt-row">
                                    <td style="text-align:left;font-size:10px   !important;"><?php echo substr($itemDetail['NAME'],0,25)."(".$itemDetail['ITEM_BARCODE'].")"; ?></td>
                                    <td style="text-align:center;font-size:10px !important;"><?php echo $row['QTY']; ?></td>
                                </tr>
<?php
									$total_qty += $row['QTY'];
								}
?>
                            </tbody>
<?php
						if(mysql_num_rows($purchaseReport)){
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;">Total:</td>
                                <td style="text-align:center;"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
