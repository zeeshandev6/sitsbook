<?php
    include('common/connection.php');
    include 'common/config.php';
    include('common/classes/accounts.php');
    include('common/classes/j-voucher.php');
    include('common/classes/items.php');
    include('common/classes/itemCategory.php');
    include('common/classes/customers.php');
    include('common/classes/rental_booking.php');
    include('common/classes/rental_booking_receipts.php');

    //Permission
    if(!in_array('rental-booking',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objJournalVoucher                = new JournalVoucher();
    $objAccountCodes                  = new ChartOfAccounts();
    $objItems                         = new Items();
    $objItemCategory                  = new ItemCategory();
    $objCustomers                     = new Customers();
    $objRentalBooking                 = new RentalBooking();
    $objRentalBookingReceipts         = new RentalBookingReceipts();
    $objConfigs                       = new Configs();

    $total = $objConfigs->get_config('PER_PAGE');
    $dir   = "uploads/podocs";

    if(isset($_POST['delete_id'])){
      $receipt_id = (int)mysql_real_escape_string($_POST['delete_id']);
      if($receipt_id>0){
        $voucher_id = $objRentalBookingReceipts->getVoucherId($receipt_id);
        $objRentalBookingReceipts->delete($receipt_id);
        if($voucher_id>0){
          $objJournalVoucher->reverseVoucherDetails($voucher_id);
          $objJournalVoucher->deleteJv($voucher_id);
        }
      }
      exit();
    }

    if(isset($_POST['save'])){
        $voucher_id = 0;
        $rb_id      = (int)mysql_real_escape_string($_POST['rb_id']);
        $receipt_id = (int)mysql_real_escape_string($_POST['receipt_id']);

        $objRentalBookingReceipts->rb_id        = $rb_id;
        $objRentalBookingReceipts->receipt_date = date('Y-m-d',strtotime($_POST['receipt_date']));
        $objRentalBookingReceipts->narration    = mysql_real_escape_string($_POST['narration']);
        $objRentalBookingReceipts->amount       = (float)mysql_real_escape_string($_POST['amount']);

        if($receipt_id == 0){
            $receipt_id = $objRentalBookingReceipts->save();
        }else{
            $objRentalBookingReceipts->update($receipt_id);
            $voucher_id = $objRentalBookingReceipts->getVoucherId($receipt_id);
        }
        if($receipt_id>0){
          $objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
      		$objJournalVoucher->voucherDate = $objRentalBookingReceipts->receipt_date;
      		$objJournalVoucher->reference   = '';
      		$objJournalVoucher->voucherType = 'SV';
          if($voucher_id==0){
            $voucher_id = $objJournalVoucher->saveVoucher();
          }else{
            $objJournalVoucher->reverseVoucherDetails($voucher_id);
            $objJournalVoucher->updateVoucherDate($voucher_id);
          }

          $objJournalVoucher->voucherId = $voucher_id;

          $objJournalVoucher->accountCode     = '0101010001';
					$objJournalVoucher->accountTitle    = $objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode);
					$objJournalVoucher->narration       = $objRentalBookingReceipts->narration;
					$objJournalVoucher->transactionType = "Dr";
					$objJournalVoucher->amount          = $objRentalBookingReceipts->amount;

					$objJournalVoucher->saveVoucherDetail();

          $objJournalVoucher->accountCode     = '010111001';
					$objJournalVoucher->accountTitle    = $objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode);
					$objJournalVoucher->transactionType = "Cr";
					$objJournalVoucher->amount          = $objRentalBookingReceipts->amount;

					$objJournalVoucher->saveVoucherDetail();

          $objRentalBookingReceipts->insertVoucherId($receipt_id,$voucher_id);
        }
        header("location:rental-booking-receipts.php?rbid=".$rb_id);
        exit();
    }

    $customersList     = $objCustomers->getList();
    $accountsList      = $objJournalVoucher->getAccountsList();
    $rb_id             = 0;
    $receipt_id        = 0;
    $rentalBookingDetails    = NULL;
    $receipt_details   = NULL;

    if(isset($_GET['rbid'])){
        $rb_id     = (int)mysql_real_escape_string($_GET['rbid']);
    }
    if(isset($_GET['id'])){
        $receipt_id      = (int)mysql_real_escape_string($_GET['id']);
        $receipt_details = $objRentalBookingReceipts->getDetails($receipt_id);
    }
    $rentalBookingDetails     = $objRentalBooking->getDetail($rb_id);
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <style media="screen">
      #bodyTab1{
        <?php if($receipt_id>0){echo "display: none;";} ?>
      }
      #bodyTab2{
        <?php if($receipt_id==0){echo "display: none;";} ?>
      }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" >
        $(document).ready(function() {
            $("select").selectpicker();
            $("*[data-toggle=tooltip]").tooltip();
            $("a.pointer").click(function(){
                //$(this).deleteMainRow("db/del-quotation.php");
            });
            $("ul#quot_pagination a").each(function(i,e){
                $(this).attr("href",$(this).attr("href")+"&mode=list");
            });
            $(".pointer").click(function(){
              var id = $(this).attr("do");
        			var clickedDel = $(this);
        			$("#fade").hide();
        			$("#popUpDel").remove();
        			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
        			$("#popUpDel").hide();
        			$("#popUpDel").centerThisDiv();
        			$("#fade").fadeIn('slow');
        			$("#popUpDel").fadeIn();
        			$(".dodelete").click(function(){
                  $(this).hide();
                  $(".nodelete").text("Please Wait....");
        					$.post("rental-booking-receipts.php", {delete_id : id}, function(data){
                    $("#fade").fadeOut();
            				$("#popUpDel").fadeOut(function(){
                      $(this).remove();
                    });
                    clickedDel.parent('td').parent('tr').remove();
        					});
        				});
        			$(".nodelete").click(function(){
        				$("#fade").fadeOut();
        				$("#popUpDel").fadeOut();
        			});
        			$(".close_popup").click(function(){
        				$("#popUpDel").slideUp();
        				$("#fade").fadeOut('fast');
        			});
            });
        });
        var show_form = function(elm){
          $("#bodyTab1").hide();
          $("#bodyTab2").show();
          $(".tabSelected.tab-small").addClass('tab');
          $(".tabSelected.tab-small").removeClass('tabSelected');
          $(elm).removeClass('tab');
          $(elm).addClass('tabSelected');
        };
        var show_list = function(elm){
          $("#bodyTab2").hide();
          $("#bodyTab1").show();
          $(".tabSelected.tab-small").addClass('tab');
          $(".tabSelected.tab-small").removeClass('tabSelected');
          $(elm).removeClass('tab');
          $(elm).addClass('tabSelected');
        };
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Rental Booking Details</p>
								<span id="tabPanel">
										<div class="tabPanel">
												<a href="rental-booking-management.php?tab=list"><div class="tab">List</div></a>
												<a href="rental-booking-management.php?tab=search"><div class="tab">Search</div></a>
												<a href="rental-booking-details.php"><div class="tabSelected">Detail</div></a>
										</div>
								</span>
								<div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-header">
                <span id="tabPanel" class="pull-left ml-10">
                    <div class="tabPanel">
                        <a href="rental-booking-details.php?id=<?php echo $rb_id; ?>" class="tab"><i class="fa fa-cube"></i> Booking</a>
                        <a href="rental-booking-receipts.php?rbid=<?php echo $rb_id; ?>" class="tabSelected"><i class="fa fa-random"></i> Receipts</a>
                        <a href="<?php echo ($rb_id>0)?"rental-booking-ledger.php?rbid=".$rb_id:"#"; ?>" class="tab"><i class="fa fa-file-o"></i> Ledger</a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>

            <div class="content-box-header" style="background: none;;">
                <span id="tabPanel" class="pull-left" style="margin-left: 70px;">
                    <div class="tabPanel">
                        <div class="<?php echo $receipt_id>0?"tabSelected":"tab"; ?> tab-small" id="entry-form" onclick="show_form(this);"><i class="fa fa-file-text text-primary"></i></div>
                        <div class="<?php echo $receipt_id==0?"tabSelected":"tab"; ?> tab-small" id="receipts-list" onclick="show_list(this);"><i class="fa fa-list text-primary"></i></div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-content">
                <div id="bodyTab1">
                  <table class="table">
                    <thead>
                      <th class="text-center col-xs-1" style="font-weight:bold;">Sr.</th>
                      <th class="text-center col-xs-2" style="font-weight:bold;">ReceiptDate</th>
                      <th class="text-center col-xs-4" style="font-weight:bold;">Narration</th>
                      <th class="text-center col-xs-1" style="font-weight:bold;">Amount</th>
                      <th class="text-center col-xs-2" style="font-weight:bold;">Action</th>
                    </thead>
                    <tbody>
                    <?php
                      $counter = 1;
                      $list = $objRentalBookingReceipts->getListByRbId($rb_id);
                      if(mysql_num_rows($list)){
                        while($row = mysql_fetch_assoc($list)){
                          ?>
                          <tr>
                            <td class="text-center"><?php echo $counter; ?></td>
                            <td class="text-center"><?php echo date('d-m-Y',strtotime($row['RECEIPT_DATE'])); ?></td>
                            <td class="text-center"><?php echo $row['NARRATION']; ?></td>
                            <td class="text-center"><?php echo $row['AMOUNT']; ?></td>
                            <td class="text-center">
                              <a href="rental-booking-receipts.php?rbid=<?php echo $row['RB_ID'] ?>&id=<?php echo $row['ID'] ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                              <a do="<?php echo $row['ID'] ?>" class="pointer"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                          <?php
                          $counter++;
                        }
                      }
                    ?>
                    </tbody>
                  </table>
                </div>
                <div id="bodyTab2">
                    <form class="" action="" method="post">
                    <div id="form">
                        <div class="caption">Receipt Date </div>
                        <div class="field">
                            <input type="text" class="form-control datepicker" name="receipt_date" style="width:150px;" value="<?php echo ($receipt_details==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($receipt_details['RECEIPT_DATE'])) ?>" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th width="40%" class="text-center">Narration</th>
                               <th width="10%" class="text-center">Cost</th>
                               <th width="5%"  class="text-center">Action</th>
                            </tr>
                            <tr>
                                <th><input type="text" class="form-control narration" required name="narration"        value="<?php echo ($receipt_details['NARRATION']!='')?$receipt_details['NARRATION']:"Cash received for Order Number : ".$rentalBookingDetails['BOOKING_ORDER_NO']; ?>" /></th>
                                <th><input type="text" class="form-control amount" required   name="amount"           value="<?php echo $receipt_details['AMOUNT'] ?>" /></th>
                                <th class="text-center"><input type="submit" name="save" class="button" value="<?php echo ($receipt_id > 0)?"Update":"Save"; ?>" /></th>
                            </tr>
                        </thead>
                    </table>
                    <input type="hidden" name="receipt_id" value="<?php echo (int)$receipt_id; ?>" />
                    <input type="hidden" class="rb_id" name="rb_id" value="<?php echo (int)$rb_id; ?>" />
                    </form>
                    <div class="clear"></div>
                    <?php if($rb_id>0){ ?>
                    <div class="col-xs-12" style="height: 50px;padding-top:20px;">
                        <a href="rental-booking-receipts.php?rbid=<?php echo $rb_id; ?>" class="pull-left mr-10"><div class="button">New Form</div></a>
                        <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <div class="clear"></div>
                </div> <!--End bodyTab1-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script>
    $(function(){
        $(".shownCode,.loader").hide();
        $("input.insert_row").on('keydown',function(e){
            if(e.keyCode == 13){
                $(this).quickSave();
            }
        });
        $("button.saveExpense").click(function(){
            saveProjectExpense();
        });
        $("div.account_code button").keydown(function(e){
            if(e.keyCode == '13'){
                if($("select.account_code option:selected").val() != ''){
                    $("input.narration").focus();
                }
            }
        });
        $("input.narration").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.amount").focus();
            }
        });
        $("input.amount").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.charged").focus();
            }
        });
        $("input.charged").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.insert_row").focus();
            }
        });
        $(window).keydown(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                $("button.saveExpense").click();
                return false;
            }
        });
        $("#fromDatepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            showAnim : 'show',
            changeMonth: true,
            changeYear: true,
            yearRange: '2000:+10'
        });
        $("#toDatepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            showAnim : 'show',
            changeMonth: true,
            changeYear: true,
            yearRange: '2000:+10'
        });
    });
</script>
