<?php
	include('common/connection.php');
	include('common/classes/posted-enteries.php');
	include('common/classes/company_details.php');
	include('common/classes/settings.php');

	$objpostedEnteries = new postedEnteries();
	$objCompanyDetails = new CompanyDetails();
	$objConfigs 	   	 = new Configs();

	$currency_type     = $objConfigs->get_config('CURRENCY_TYPE');

	if(isset($_GET['id'])){
		$voucher_id 		 = mysql_real_escape_string($_GET['id']);
		$voucher    		 = mysql_fetch_array($objpostedEnteries->getRecordDetailsVoucher($voucher_id));
		$voucher_details = $objpostedEnteries->getVoucherDetailList($voucher_id);
	}
	if(isset($_GET['acc'])){
		$account_code 		= mysql_real_escape_string($_GET['acc']);
		$voucher    			= mysql_fetch_array($objpostedEnteries->getRecordDetailsVoucher($voucher_id));
		$voucher_details 	= $objpostedEnteries->getVoucherDetailList($voucher_id);
	}
	$companyTitle = $objCompanyDetails->getTitle('1');
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript" src="resource/scripts/towords.js"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			var amount = parseFloat($("td.debitTotal").text().replace(',',''))||0;
			amount     = toWords(amount);
			$("td.towords").text(" Rupees "+amount);
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/print-reports.css",
			      pageTitle: "Software Powered By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			$(".printThis").click();
			hide_transaction_type();
		});
		var hide_transaction_type = function(){
			//var transtaction_skip = $("input.transaction-skip").val();
			//$("*[data-type='"+transtaction_skip+"']").remove();
		};
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100%;background-repeat: no-repeat;">
<?php
	if(isset($voucher_id)){
?>
<div class="panel panel-primary col-md-8" style="padding: 10px;margin: 50px auto;float: none;">
	<div class="panel-heading">
		<button class="btn btn-default printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
		<div class="clear"></div>
    </div><!--header-->
    <div style="height: 20px;"></div>

	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
        <div class="printThisDiv">
            <div class="invoiceHead" style="width: 720px;margin: 0px auto;">
            	<div class="partyTitle" style="text-align:left;">
                	<div style="font-size:18px;"> <?php echo $companyTitle; ?> </div>
                </div>
                <div class="partyTitle" style="text-align:left;font-size:16px;font-weight: bold;padding: 5px 0px;margin: 0px auto;">
<?php
					$voucher_type = 'Journal Voucher';
					switch($voucher['VOUCHER_TYPE']){
						case 'JV':
							$voucher_type = 'Journal Voucher';
							$vocuher_type_number = "";
							$transtaction_skip = '';
						break;
						case 'CR':
							$voucher_type = 'Cash Receipt Voucher';
							$vocuher_type_number = "CR# ".$voucher['TYPE_NO'];
							$transtaction_skip = 'Dr';
						break;
						case 'CP':
							$voucher_type = 'Cash Payment Voucher';
							$vocuher_type_number = "CP# ".$voucher['TYPE_NO'];
							$transtaction_skip = 'Cr';
						break;
						case 'BR':
							$voucher_type = 'Bank Receipt Voucher';
							$vocuher_type_number = "BR# ".$voucher['TYPE_NO'];
							$transtaction_skip = 'Dr';
						break;
						case 'BP':
							$voucher_type = 'Bank Payment Voucher';
							$vocuher_type_number = "BP# ".$voucher['TYPE_NO'];
							$transtaction_skip = 'Cr';
						break;
						default:
							$voucher_type = 'Journal Voucher';
							$vocuher_type_number = "";
							$transtaction_skip = '';
					}
					echo $voucher_type;
?>
                </div>
                <input type="hidden" class="transaction-skip" value="<?php echo $transtaction_skip; ?>"></input>
                <div class="clear" style="height: 5px;"></div>
                <div class="partyTitle" style="font-size: 16px;">
                	<span class="pull-right" style="padding:0px 0px;font-size:14px;"><?php echo $vocuher_type_number; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Voucher No. <?php echo $voucher['VOUCHER_NO']; ?></span>
                    <span class="pull-right" style="padding:0px 0px;font-size:14px;">Date. <?php echo date('d-m-Y',strtotime($voucher['VOUCHER_DATE'])); ?></span>
                </div><!--partyTitle-->
                <div class="clear" style="height: 5px;"></div>
                <div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 720px;margin: 0px auto;">
                <table cellspacing = "0" align = "center" style="width:100%;" class="removeableTable tableBreak">
<?php
                    if(mysql_num_rows($voucher_details)){
						$debitTotal = 0;
						$creditTotal = 0;
?>
														 <thead class="tHeader">
	                             <tr>
	                                <th width="10%" class="text-center" >A/C Code</th>
	                                <th width="20%" class="text-center" >Account Title</th>
	                                <th width="30%" class="text-center" >Narrtion</th>
	                                <th width="10%" class="text-center" data-type='Dr'>Debit(<?php echo $currency_type; ?>)</th>
	                                <th width="10%" class="text-center" data-type='Cr'>Credit(<?php echo $currency_type; ?>)</th>
	                             </tr>
                             </thead>
                             <tbody>
                                <tr class="transactions" style="display:none;"></tr>
<?php
                        while($row = mysql_fetch_array($voucher_details)){
                        	if($row['CASH_BOOK_FLAG']=='Y'){
                        		continue;
                        	}
?>
                            <tr class="transactions">
                                <td class="text-center" ><?php echo $row['ACCOUNT_CODE']; ?></td>
                                <td class="text-left" ><?php echo substr($row['ACCOUNT_TITLE'],0,40); ?></td>
                                <td class="text-left" ><?php echo $row['NARRATION']; ?></td>
                                <td class="text-center debitColumn"  data-type='Dr'><?php echo ($row['TRANSACTION_TYPE']=='Dr')?number_format($row['AMOUNT'],2):""; ?></td>
                                <td class="text-center creditColumn"  data-type='Cr'><?php echo ($row['TRANSACTION_TYPE']=='Cr')?number_format($row['AMOUNT'],2):""; ?></td>
                            </tr>
<?php
													$debitTotal += ($row['TRANSACTION_TYPE']=='Dr')?$row['AMOUNT']:0;
													$creditTotal += ($row['TRANSACTION_TYPE']=='Cr')?$row['AMOUNT']:0;
                        }
												$debitTotal = round($debitTotal,2);
												$creditTotal = round($creditTotal,2);
?>
							<tr style="background-color:#F8F8F8;height: 30px;">
                                <td colspan="3" style="text-align:left;font-weight: bold;" class="towords">  </td>
                                <td class="debitTotal" style="text-align:center;color:#042377;" data-type='Dr' title="Debit"><?php echo (isset($debitTotal))?number_format($debitTotal,2):"0"; ?></td>
                                <td class="creditTotal" style="text-align:center;color:#BD0A0D;" data-type='Cr' title="Credit"><?php echo (isset($creditTotal))?number_format($creditTotal,2):"0"; ?></td>
<?php
			if(isset($creditTotal)&&isset($debitTotal)){
				$drCrDiffer = $debitTotal - $creditTotal;
			}else{
				$drCrDiffer = 0;
			}
?>
                            </tr>
<?php
                    }else{
?>
							<tr><td style="text-align: center;" colspan="10"> Blank Voucher </td></tr>
<?php
					}
?>
                         </tbody>
                      </table>
                <div class="clear"></div>
								<p class="towords"></p>
                <div class="clear" style="height: 5px;"></div>
                <div class="voucherEndFooter">
	                <div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
	                	Prepared By : ___________
	                </div><!--partyTitle-->
	                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
	                	Authorized By : ___________
	                </div><!--partyTitle-->
									<?php if($voucher['VOUCHER_TYPE'] == 'CP'||$voucher['VOUCHER_TYPE'] == 'BP'||$voucher['VOUCHER_TYPE'] == 'JV'){ ?>
										<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
		                	Received By : ________________
		                </div><!--partyTitle-->
									<?php } ?>
	                <div class="clear"></div>
	            </div>
                <div class="clear"></div>
            </div><!--invoiceBody-->
        </div><!--printThisDiv-->
        </div><!--invoiceLeftPrint-->
    </div><!--invoiceContainer-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
