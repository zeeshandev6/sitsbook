<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/machine_production.php');
	include('common/classes/machine_production_details.php');
	include('common/classes/machines.php');
	include('common/classes/customers.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_inward.php');
	include('common/classes/emb_products.php');

	//Permission
	if(!in_array('machine-production-report-full',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objMachineProduction 				= new MachineProduction();
	$objMachineProductionDetails  = new MachineProductionDetails();
	$objMachines  								= new machines();
	$objCustomers 								= new customers();
	$objChartOfAccounts  					= new ChartOfAccounts();
	$objEmbroideryInward   				= new EmbroideryInward();
	$objEmbProducts  							= new EmbProducts();

	$productList = $objEmbroideryInward->getProductList();

	$machine_production_id 				= 0;
	$machine_production_detail_id = 0;
	$report_date 									= date('Y-m-d');

	if(isset($_GET['action'])){
		$message = 'Record '.$_GET['action'].' Successfully';
	}

	$customerList = $objCustomers->getList();

	$report_main 		= NULL;
	$report_details = NULL;

	$report = false;
	if(isset($_GET['id'])){
		$machine_production_id 			= (int)mysql_real_escape_string($_GET['id']);
		$report_main 								= $objMachineProduction->getDetails($machine_production_id);
		$report_details   					= $objMachineProductionDetails->getList($machine_production_id);
		$report = true;
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" rel="stylesheet">
		<style media="screen">
			div.new_item_div table{
				width: 1790px !important;
			}
			div.content-box-content {
				overflow: auto !important;
				width: 100%;
			}
			div.new_item_div{
				width: 100%;
			}
			div#bodyTab1,div#form{
				width: 1490px !important;
			}
			div.new_item_div table input[type=text]{
				padding: 2px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/machine.production.config.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("select").selectpicker();
				$("input.add_detail_row").click(function(){
					$(this).saveOnEnter();
				});
				$(document).calculateColumnTotals();
				//save function
				$(document).on('click', 'button[name=add_production]', function(){
					$("button[name=add_production]").prop('disabled',true);
					var jsonString,arrReturn 	= {};
					var deleted_rows 					= {};
					var production_id  			 	= parseInt($("input[name=production_id]").val())||0;
					var reportDate 						= $("input[name='report_date']").val();
					var machine								= $("select.machine_select option:selected").val();
					var party 								= $("select.party_acc_code option:selected").val();
					var machine_rpm						= $("select.machine_select option:selected").attr('data-rpm');
					var expense								= $("input[name='expense']").val();
					var stitch_sh_a						= parseInt($("input[name='stitch_shift_a']").val())||0;
					var stitch_sh_b	 					= parseInt($("input[name='stitch_shift_b']").val())||0;
					var commander_shift_a	    = $("input[name='commander_aa']").val();
					var commander_shift_b	    = $("input[name='commander_bb']").val();
					var fault_note						= $("input.comment").val();
					var niddle_setting				= $("select.niddle_setting option:selected").val();
					var saleRate							= $("input.saleRate").val();

					$('tr.calculations').each(function(x){
						arrReturn[x] = {
								row_id: 				$(this).attr('data-row-id'),
								party_acc_code: $(this).find('td.party_code').attr('data-acc-code'),
								farbric_type:  	$(this).find('td.ft').html(),
								thread_type: 		$(this).find('td.tt').html(),
								design_no: 			$(this).find('td.dn').html(),
								item: 					$(this).find('td.it').attr("data-id"),
								prod_shift_a: 	$(this).find('td.pda').html(),
								prod_shift_b: 	$(this).find('td.pdb').html(),
								total_pord: 		$(this).find('td.tp').html(),
								yard: 					$(this).find('td.yr').html(),
								thread_weight: 	$(this).find('td.tw').html(),
								rate: 					$(this).find('td.ra').html(),
								thread_price: 	$(this).find('td.thp').html(),
								total_price: 		$(this).find('td.thc').html(),
								stitch: 				$(this).find('td.st').html(),
								stitch_a: 			$(this).find('td.sta').html(),
								stitch_b: 			$(this).find('td.stb').html(),
								sale_rate: 			$(this).find('td.sr').html(),
								sale: 					$(this).find('td.sal').html()
						};
					});
					$("input.delete_rows").each(function(i,e){
						deleted_rows[i] = parseInt($(this).val())||0;
					});
					jsonString   = JSON.stringify(arrReturn);
					deleted_rows = JSON.stringify(deleted_rows);
					$.post("db/add_production_details.php",{    add_production:true,
																											production_id:production_id,
																											report_date:reportDate,
																											machine_id:machine,
																											machine_rpm:machine_rpm,
																											party_acc_code:party,
																											expense:expense,
																											comment:fault_note,
																											stitch_shift_a:stitch_sh_a,
																											stitch_shift_b:stitch_sh_b,
																											commander_shift_a:commander_shift_a,
																											commander_shift_b:commander_shift_b,
																											niddle_setting:niddle_setting,
																											saleRate:saleRate,
																											jsonString:jsonString,
																											deleted_rows:deleted_rows
						},function(data){
							$("button[name=add_production]").prop('disabled',false);
							data = parseInt(data)||0;
							if(data > 0){
								window.location.href = '<?php echo basename($_SERVER['PHP_SELF']); ?>?id='+data;
							}else{
								displayMessage('Error! Record could not be saved.');
							}
						});
				});
				//save close

				//numeric only
				$('input.prod_shift_a').numericOnly();
				$('input.prod_shift_b').numericOnly();
				$('input.thread_weight').numericOnly();
				$('input.rate').numericOnly();
				$('input.stitch').numericOnly();
				$('input.sale_rate').numericOnly();
				$('input.stitch_shift_aa').numericOnly();
				$('input.stitch_shift_bb').numericOnly();
				//setFocus

				$("div.party_code button").keydown(function(e){
					if(e.keyCode == 13){
						$("input.farbric_type").focus();
					}
				});
				$("input.farbric_type").setFocusTo("input.thread_type");
				$("input.thread_type").setFocusTo("input.design_no");
				$("input.design_no").setFocusTo("div.product_id button");
				$("select.product_id").change(function(){
					$("input.prod_shift_a").focus();
				});
				$("input.prod_shift_a").setFocusTo("input.prod_shift_b");
				$("input.prod_shift_b").setFocusTo("input.thread_weight");
				$("input.thread_weight").setFocusTo("input.rate");
				$("input.rate").setFocusTo("input.stitch");
				$("input.stitch").setFocusTo("input.sale_rate");
				$("input.sale_rate").setFocusTo("input.add_detail_row");
			});
		</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Daily Machine Production Report</p>
								<span id="tabPanel">
									<div class="tabPanel">
										<a href="machine-production-report-list.php"><div class="tab">List</div></a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>"><div class="tabSelected">Report</div></a>
									</div>
								</span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">

					<!--<form method="post" action="" class="main_form">-->
	                	<div id="form">
	                		<div class="col-md-12">

		                		<div class="caption" style="width:100px;">Report Date</div>
		                		<div class="field" style="width:150px;">
		                			<input class="form-control datepicker" name="report_date" value="<?php echo ($report)?date('d-m-Y',strtotime($report_main['REPORT_DATE'])):date('d-m-Y',strtotime($report_date)); ?>"  />
		                		</div>

		                		<div class="caption" style="width: 80px;">Machine</div>
		                		<div class="field">
		                			<select class="machine_select form-control show-tick" data-live-search="true" data-style="btn-left btn-default" name="machine_id" data-width="150">
		                				<option value=""></option>
	<?php
									$machines = $objMachines->getList();
									if(mysql_num_rows($machines)){
										while($machine = mysql_fetch_array($machines)){
											$mc_selected = '';
											if($report){
												$mc_selected = $report_main['MACHINE_ID'] == $machine['ID']?"selected":"";
											}
	?>
		                				<option value="<?php echo $machine['ID']; ?>" data-rpm="<?php echo $machine['MACHINE_RPM']; ?>" data-rate="<?php echo $machine['RATE']; ?>" <?php echo $mc_selected; ?> ><?php echo $machine['NAME']; ?></option>
	<?php
										}
									}
	?>
		                			</select>
													<input type="text" class="form-control pull-right mr-30 text-center btn-right" style="width: 70px;" name="machine_rpm" value="<?php echo $report_main['MACHINE_RPM']; ?>" readonly>
		                		</div>
												<div class="clear"></div>

		                		<div class="caption" style="width:100px;">Expense</div>
		                		<div class="field" style="width: 145px">
		                			<input type="text" class="form-control" name="expense" value="<?php echo ($report)?$report_main['EXPENSE']:""; ?>" />
		                		</div>

												<div class="caption" style="width: 85px;">Fault Note</div>
		                		<div class="field" style="width: 540px;">
		                			<input type="text" class="form-control comment" name="comment" value="<?php echo ($report)?$report_main['COMMENT']:""; ?>" />
		                		</div>
												<div class="clear"></div>

		                		<div class="caption" style="width: 100px;">Stitches Shift A</div>
		                		<div class="field" style="width: 145px">
		                			<input type="text" class="form-control stitch_shift_aa" name="stitch_shift_a" value="<?php echo $report_main['STITCH_SHIFT_A'] ?>" />
		                		</div>

		                		<div class="caption" style="width: 85px;">St. Shift B</div>
		                		<div class="field" style="width: 150px">
		                			<input type="text" class="form-control stitch_shift_bb" name="stitch_shift_b" value="<?php echo $report_main['STITCH_SHIFT_B'] ?>" />
		                		</div>

												<div class="caption" style="width: 100px;">Niddle Setting</div>
		                		<div class="field" style="width: 80px">
		                			<select name="niddle_setting" class="form-control niddle_setting">
		                				<option value="4/4"  <?php echo $report_main['NIDDLE_SETTING']=="4/4"?"selected":""; ?> >4/4</option>
		                				<option value="8/4"  <?php echo $report_main['NIDDLE_SETTING']=="8/4"?"selected":""; ?> >8/4</option>
		                				<option value="12/4" <?php echo $report_main['NIDDLE_SETTING']=="12/4"?"selected":""; ?> >12/4</option>
														<option value="16/4" <?php echo $report_main['NIDDLE_SETTING']=="16/4"?"selected":""; ?> >16/4</option>
														<option value="20/4" <?php echo $report_main['NIDDLE_SETTING']=="20/4"?"selected":""; ?> >20/4</option>
														<option value="24/4" <?php echo $report_main['NIDDLE_SETTING']=="24/4"?"selected":""; ?> >24/4</option>
														<option value="28/4" <?php echo $report_main['NIDDLE_SETTING']=="28/4"?"selected":""; ?> >28/4</option>
		                				<option value="32/4" <?php echo $report_main['NIDDLE_SETTING']=="32/4"?"selected":""; ?> >32/4</option>
		                			</select>
		                		</div>

												<div class="caption" style="width: 50px;">Rate</div>
		                		<div class="field" style="width: 100px;">
		                			<input type="text" class="form-control saleRate" name="saleRate" value="<?php echo $report_main['SALE_RATE']; ?>" />
		                		</div>
												<div class="clear"></div>

												<div class="caption" style="width: 100px;">Commander</div>
		                		<div class="field" style="width: 145px">
		                			<input type="text" class="form-control commander_aa" name="commander_aa" value="<?php echo $report_main['COMMANDER_SHIFT_A'] ?>" />
		                		</div>

		                		<div class="caption" style="width: 85px;"> Commander</div>
		                		<div class="field" style="width: 150px">
		                			<input type="text" class="form-control commander_bb" name="commander_bb" value="<?php echo $report_main['COMMANDER_SHIFT_B'] ?>" />
		                		</div>
												<div class="clear"></div>

		                		<input type="hidden" name="production_id" value="<?php echo (!isset($_GET['copy']))?$machine_production_id:0; ?>" />
		                		<div class="clear"></div>

		                	</div>
		                	<div class="clear"></div>
	                	</div>
	                	<div class="clear"></div>
                		<hr />
                	<div class="new_item_div">
                			<table>
                        <thead>
                            <tr>
															 <th class="text-center width-5">Party</th>
                               <th class="text-center width-5">Fabric Type</th>
                               <th class="text-center width-5">Thread Type</th>
                               <th class="text-center width-5">Design#</th>
                               <th class="text-center width-5">Product</th>
                               <th class="text-center width-5">ShiftA Prod.</th>
                               <th class="text-center width-5">ShiftB Prod.</th>
                               <th class="text-center width-3">TP</th>
                               <th class="text-center width-3">Yard</th>
                               <th class="text-center width-5">Thread Weight(KG)</th>
                               <th class="text-center width-5">Rate/KG</th>
                               <th class="text-center width-5">Thread Cost/KG</th>
                               <th class="text-center width-5">Thread Cost/TP</th>
                               <th class="text-center width-5">Stitch/Thaan</th>
                               <th class="text-center width-5">ShiftA St.</th>
                               <th class="text-center width-5">ShiftB St.</th>
                               <th class="text-center width-5">Rate</th>
                               <th class="text-center width-5">Sale/1000 Stitches</th>
                               <th class="text-center width-5">Action</th>
                            </tr>
                        </thead>
                        <tbody class="appendtbody">
                            <tr class="quickSubmit" style="background:none" id="recordPanel">
																<td id="party_code">
																	<select name="party_code" class="form-control party_code" data-live-search="true">
																		<option value=""></option>
					<?php
												if(mysql_num_rows($customerList)){
													while($customer = mysql_fetch_array($customerList)){
					?>
																		<option value="<?php echo $customer['CUST_ACC_CODE']; ?>" data-subtext="<?php echo $customer['CUST_ACC_CODE']; ?>" ><?php echo $customer['CUST_ACC_TITLE']; ?></option>
					<?php
													}
												}
					?>
																	</select>
                                </td>
                                <td id="farbric_type">
                                    <input type="text" class="text-left form-control farbric_type"  value="CRINCLE" class="farbric_type" />
                                </td>
                                <td id="thread_type">
                                    <input type="text"  value="POLYSTER" class="thread_type text-left form-control"  />
                                </td>
                               	<td id="design_no">
                                    <input type="text"  value="" class="design_no text-left form-control" />
                                </td>
                                <td id="item">
                                    <select class="product_id show-tick form-control" data-live-search="true">
                                       <option selected value=""></option>
<?php
                                    if(mysql_num_rows($productList)){
                                        while($account = mysql_fetch_array($productList)){
?>
                                       <option data-subtext="<?php echo $account['PROD_CODE']; ?>" value="<?php echo $account['ID']; ?>" ><?php echo $account['TITLE']; ?></option>
<?php
                                        }
                                    }
?>
                                    </select>
                                </td>
                                <td id="prod_shift_a">
                                    <input type="text"   name="prod_shiftA" value="" class="prod_shift_a text-center form-control" />
                                </td>
                                <td id="prod_shift_b">
                                	<input type="text"  name="prod_shiftB" value="" class="prod_shift_b text-center form-control" />
                                </td>
                                <td id="total_prod">
                                	<input type="text" name="total_prod" value="" class="text-center form-control total_prod" readonly="readonly" />
                                </td>
                                <td id="yard">
                                	<input type="text" name="yard" value="" class="text-center form-control yard" readonly="readonly" />
                                </td>
                                <td id="thread_weight">
                                    	<input type="text"  name="thread_weight" value="" class="text-center form-control thread_weight" />
                                </td>
                                <td id="rate">
                                    	<input type="text"  name="rate" value="" class="rate text-center form-control" />
                                </td>
                                <td id="thread_price">
                                	<input type="text"  name="thread_price" value="" class="text-center form-control thread_price" readonly="readonly" />
                                </td>
                                <td id="total_price">
                                	<input type="text"  name="total_price" value="" class="text-center form-control total_price" readonly="readonly" />
                                </td>
                                <td  id="stitch">
                               		<input type="text" name="stitch" value="" class="stitch text-center form-control" />
                                </td>
                                <td id="stitch_a">
                                	<input type="text"  name="stitch_a" value="" class="text-center form-control stitch_a" readonly="readonly" />
                                </td>
                                <td id="stitch_b">
                                 	<input type="text" name="stitch_b" value="" class="text-center form-control stitch_b" readonly="readonly" />
                                </td>
                                <td id="sale_rate">
                                    	<input type="text"  name="sale_rate" value="" class="text-center form-control sale_rate" />
                                </td>
                                <td  id="sale">
                                	<input type="text"  name="sale" value="" class="text-center form-control sale" readonly="readonly" />
                                </td>
                                <td class="text-center">
                                	<input type="button" class="add_detail_row btn btn-default btn-block" value="Enter" />
                                </td>
                            </tr>
                        </tbody>
												<tbody>
<?php
											if($report && mysql_num_rows($report_details)){
												while($row = mysql_fetch_assoc($report_details)){
													$product_name = $objEmbProducts->getTitle($row['ITEM']);
													?>
														<tr data-row-id="<?php echo (!isset($_GET['copy']))?$row['ID']:0; ?>" class="calculations" >
															<td style="text-align:center" class="party_code" data-acc-code="<?php echo $row['PARTY_ACC_CODE']; ?>"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['PARTY_ACC_CODE']); ?></td>
															<td style="text-align:center" class="ft"><?php echo $row['FABRIC_TYPE']; ?></td>
															<td style="text-align:center" class="tt"><?php echo $row['THREAD_TYPE']; ?></td>
															<td style="text-align:center" class="dn"><?php echo $row['DESIGN_NO']; ?></td>
															<td style="text-align:center" class="it" data-id="<?php echo $row['ITEM']; ?>"><?php echo $product_name; ?></td>
															<td style="text-align:center" class="pda"><?php echo $row['SHIFT_A']; ?></td>
															<td style="text-align:center" class="pdb"><?php echo $row['SHIFT_B']; ?></td>
															<td style="text-align:center" class="tp"><?php echo $row['TP']; ?></td>
															<td style="text-align:center" class="yr"><?php echo $row['YARD']; ?></td>
															<td style="text-align:center" class="tw"><?php echo $row['THREAD_WEIGHT']; ?></td>
															<td style="text-align:center" class="ra"><?php echo number_format($row['AT_RATE'],2,'.',''); ?></td>
															<td style="text-align:center" class="thp"><?php echo number_format($row['THREAD_PRICE'],2,'.',''); ?></td>
															<td style="text-align:center" class="thc"><?php echo number_format($row['TOTAL_PRICE'],2,'.',''); ?></td>
															<td style="text-align:center" class="st"><?php echo $row['STITCH']; ?></td>
															<td style="text-align:center" class="sta"><?php echo $row['STITCH_A']; ?></td>
															<td style="text-align:center" class="stb"><?php echo $row['STITCH_B']; ?></td>
															<td style="text-align:center" class="sr"><?php echo number_format($row['RATE'],2,'.',''); ?></td>
															<td style="text-align:center" class="sal"><?php echo number_format($row['SALE'],2,'.',''); ?></td>
															<td style="text-align:center;padding: 0px;" class="noprint">
																<button class="btn btn-primary btn-sm btn-xs" onclick="editThisRow(this)"  row-id="<?php echo $row['ID']; ?>"><i class="fa fa-pencil"></i>
																	<button class="btn btn-danger btn-xs" onclick="deleteRow(this)" do="<?php echo $row['ID']; ?>"><i class="fa fa-times"></i></button>
															</td>
                            </tr>
													<?php
												}
											}
?>
												</tbody>
                      	<tfoot>
                      		<tr style="height: 30px;background-color:#f5f5f5;">
                      			<td style="text-align: center" colspan="5">Total</td>
                      			<td style="text-align: center" class="fShiftA">---</td>
                      			<td style="text-align: center" class="fShiftB">---</td>
                      			<td style="text-align: center" class="ftp">---</td>
                      			<td style="text-align: center" class="fyard">---</td>
                      			<td style="text-align: center" class="fThreadWeight">---</td>
                      			<td style="text-align: center" class="frate">---</td>
                      			<td style="text-align: center" class="fPriceThread">---</td>
                      			<td style="text-align: center" class="fThreadCost">---</td>
                      			<td style="text-align: center" class="fstitch">---</td>
                      			<td style="text-align: center" class="fstitchSha">---</td>
                      			<td style="text-align: center" class="fstitchShb">---</td>
                      			<td style="text-align: center" class="fsaleRate">---</td>
                      			<td style="text-align: center" class="fsale">---</td>
                      			<td style="text-align: center">---</td>
                      		</tr>
                      	</tfoot>
                      </table>
											<button class="btn btn-primary btn-sm pull-right m-10" type="submit" name="add_production"><?php echo ($report_main==NULL||isset($_GET['copy']))?"Save":"Update"; ?></button>
											<?php if($report_main!=NULL){ ?>
												<a href="machine-production-report.php" class="btn btn-default pull-left btn-sm m-10">New Report</a>
											<?php } ?>
											<div class="clear"></div>
                		<hr />
                	</div>
                <div class="clear"></div>
                </div> <!--End bodyTab1-->
								<div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
	<div id="xfade"></div>
	<div id="fade"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).calculateIncomeLossTotals();
		$("input[name='sale_rate'],input[name='prod_shiftA'],input[name='rate'],input[name='stitch']").bind('change blur keyup',function(){
			input_row_calculations();
		});
		$("input[name='prod_shiftB'],input[name='thread_weight']").bind('change blur keyup',function(){
			input_row_calculations();
		});
		$("input[name='stitch_a'],input[name='stitch_b']").bind('change blur keyup',function(){
			input_row_calculations();
		});
		$(".new_machine").click(function(){
			window.location.href = '<?php echo basename($_SERVER['PHP_SELF']); ?>';
		});
	});
</script>
<script>
	$(document).ready(function(){
		$("#printReport").click(function(){
			var MaxHeight = 814;
			var RunningHeight = 0;
			var PageNo = 1;
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight = 814;
				}else{
					MaxHeight = 914;
				}

				if (RunningHeight + $(this).height() > MaxHeight) {
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate    = $(".repoDate").text();
			//remove previous thead/tfoot/ReportDate
			$(".tHeader").remove();
			$(".repoDate").remove();
			$(".tableFooter").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				if(i == 1){
					MaxHeight = 814;
				}else{
					MaxHeight = 914;
				}
				$('table.tableBreak').parent().append("<div class='tablePage' style='width:100%;max-height:"+MaxHeight+"' ><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:12px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();
			$(".print_able").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Sitsbook",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			});
		});
	});
</script>
