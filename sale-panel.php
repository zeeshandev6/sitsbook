<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/customers.php');
    include('common/classes/banks.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sales-panel',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$_SESSION['pos'] = 'Y';

	$objSale         = new Sale();
	$objSaleDetails  = new SaleDetails();
	$objItems        = new Items();
	$objItemCategory = new itemCategory();
	$objCustomers    = new Customers();
  $objBanks        = new Banks();
	$objTaxRates     = new TaxRates();
  $objConfigs      = new Configs();

  $user_name = $objConfigs->get_config("USER_NAME");

  if(isset($_GET['discount'])){
    $objConfigs->set_config($_GET['discount'],'DISCOUNT');
    exit();
  }

  $sms_config = $objConfigs->get_config('SMS');

  if(isset($_POST['mobile_number'])){

      if($sms_config == 'N'){
          echo $sms_config;
          exit();
      }

      $invoice_msg   = $objConfigs->get_config('INVOICE_MSG');
      $mobile_number = $_POST['mobile_number'];
      $sale_id       = $_POST['sale_id'];

      $saleDetails   = mysql_fetch_assoc($objSale->getRecordDetails($sale_id));
      $saleItems     = $objSaleDetails->getList($sale_id);

      $sms_search_vars = array();
      $sms_search_vars[] = '[INVOICE]';
      $sms_search_vars[] = '[DATE]';
      $sms_search_vars[] = '[AMOUNT]';

      $sms_replace_vars   = array();
      $sms_replace_vars[] = $saleDetails['BILL_NO'];
      $sms_replace_vars[] = date('d-m-Y',strtotime($saleDetails['SALE_DATE']));
      $sms_replace_vars[] = $saleDetails['BILL_AMOUNT'];

      $invoice_msg = str_replace($sms_search_vars, $sms_replace_vars, $invoice_msg);

      $options = array('location' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/soap-server.php',
                'uri' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/');
      $api = new SoapClient(NULL, $options);
      try{
          echo $api->process($user_name,"N","",$mobile_number,$invoice_msg,0,0);
      }catch(Exception $e){
          // "SMS Could not be sent!";
      }
      exit();
  }

	$customersList       = $objCustomers->getList();
	$itemsCategoryList   = $objItemCategory->getList();
	$taxRateList         = $objTaxRates->getList();
	$newBillNum          = $objSale->genBillNumber();
  $currency_type       = $objConfigs->get_config('CURRENCY_TYPE');
  $individual_discount = false;
  $use_taxes           = false;

  $invoice_format      = $objConfigs->get_config('INVOICE_FORMAT');
  $print_meth          = $objConfigs->get_config('DIRECT_PRINT');
  $sale_scanner_append = $objConfigs->get_config('SALE_SCANNER_APPEND');
  $cash_at_bank_list   = $objBanks->getBanksListByType('M');
  $invoice_format      = explode('_', $invoice_format);
  $invoiceSize         = $invoice_format[0]; // S  L

  if($invoiceSize == 'L'){
      $invoiceFile = 'sales-invoice.php';
  }elseif($invoiceSize == 'M'){
      $invoiceFile = 'sales-invoice-duplicates.php';
  }elseif($invoiceSize == 'S'){
      $invoiceFile = 'sales-invoice-small.php';
  }
  $scanner_toggle = $objConfigs->get_config('SALE_SCANNER');

	$sale_id   = 0;
  $inventory = NULL;
	if(isset($_GET['id'])){
		$sale_id = mysql_real_escape_string($_GET['id']);
		if($sale_id != '' && $sale_id != 0){
			$inventory    = mysql_fetch_array($objSale->getRecordDetails($sale_id));
			$saleDetails  = $objSaleDetails->getList($sale_id);
			$discountType = ($inventory['DISCOUNT_TYPE'] == '')?$discountType:$inventory['DISCOUNT_TYPE'];
		}else{
			$sale_id      = 0;
		}
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Sitsbook Sale Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css" media="screen" />
    <style>
        .custom-height-x{
            overflow-y: auto;
        }
        .caption{
        	padding: 5px !important;
        }
        td.itemName{
        	padding-left: 10px !important;
        }
        td.quantity{
            position: relative;
            cursor: pointer;
        }
        td,th{
            padding: 10px 5px !important;
            border:1px solid #CCC !important;
        }
        input.form-control{
            padding: 6px 12px !important;
        }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sale.panel.config.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
</head>
<body class="body_menu_close">
    <input type="hidden" class="invoice-type" value="<?php echo $invoiceSize; ?>">
    <input type="hidden" class="scanner-append" value="<?php echo $sale_scanner_append; ?>">
    <div id="body-wrapper">
        <div id="sidebar">
					<input type="hidden" class="print_method" value="<?php echo $print_meth; ?>" />
					<?php include("common/left_menu.php"); ?>
    		</div> <!-- End #sidebar -->
        <div class="content-box-top" >
            <div class="content-box-header">
                <p>Sales Panel</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="sale-panel-list.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content" style="padding: 5px;">
                <div id="bodyTab1">
                    <div id="form" style="margin: 20px auto;">
                      <div class="pull-right" style="margin-right:25px;padding:5px;display:none;">
                          <div class="pull-left" style="margin-right:25px;">Discount Type : </div>
                          <input id="cmn-toggle-discount" name="disc_type" value="P" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" checked <?php //echo ($discountType == 'P')?"checked":""; ?>  >
                          <label for="cmn-toggle-discount" class="css-label-radio">Percentage</label>
                          <input id="cmn-toggle-discount-amount" name="disc_type" value="R" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" <?php //echo ($discountType == 'R')?"checked":""; ?>  >
                          <label for="cmn-toggle-discount-amount" class="css-label-radio">Amount</label>
                      </div>
                          <div class="col-xs-7">
                            <form role="form">
                              <div class="panel panel-default">
                                  <div class="clear mt-20"></div>
                                  <div class="col-xs-12 pull-left">
																			<div class="caption ml-0" style="width: 90px;line-height: 36px;">Bill #</div>
																			<div class="field" style="width:20%;">
																					<input type="text"  class="form-control" name="billNum" value="<?php echo ($inventory != NULL)?$inventory['BILL_NO']:$newBillNum; ?>" />
																			</div>
																			<div class="caption">Date </div>
																			<div class="field" style="width:20%;">
																					<input type="text"  class="form-control datepicker" name="rDate" id="rDatex" value="<?php echo ($inventory != NULL)?date("d-m-Y",strtotime($inventory['SALE_DATE'])):date('d-m-Y'); ?>" />
																			</div>
																			<div class="clear"></div>

                                      <div class="caption" style="width:90px;margin: 0px 0px 0px 0px;">A/c Type</div>
                                      <div class="field" style="width:65%;">
                                          <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="A" id="radio1" <?php echo (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"checked":''; ?> class="css-checkbox" />
                                          <label for="radio1" class="css-label-radio radGroup1">A/C</label>
                                          <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="C" id="radio2" <?php echo ((substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')||$inventory == NULL)?"checked":''; ?> class="css-checkbox" />
                                          <label for="radio2" class="css-label-radio radGroup1">Cash</label>
                                          <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="B" id="radio3" <?php echo (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"checked":''; ?> class="css-checkbox" />
                                          <label for="radio3" class="css-label-radio radGroup1">Card</label>
                                      </div>
                                      <div class="clear"></div>


                                      <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"> Account :</label>
                                            <select class="supplierSelector form-control show-tick" id="accntx"
                                                    data-style="btn-default"
                                                    data-live-search="true" data-hide-disabled='true' style="border:none;" >
                                                <option selected value=""></option>
            <?php
                                        if(mysql_num_rows($customersList)){
                                            while($account = mysql_fetch_array($customersList)){
                                                if($inventory != NULL){
                                                    $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":"";
                                                    $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
                                                }else{
                                                    $disabled = "disabled";
                                                }
                                                $selected = ($inventory['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
            ?>
                                                <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
            <?php
                                            }
                                        }
                                        if(mysql_num_rows($cash_in_hand_list)){
                                            while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                                                $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                                                if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
                                                    continue;
                                                }
                                                $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
                                                $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
                                                if($inventory != NULL){
                                                    $selected = ($inventory['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
                                                }else{
                                                    $selected = ($cash_in_hand_acc_code==$cash_rows['CASH_ACC_CODE'])?"selected":"";
                                                }
            ?>
                                                <option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
            <?php
                                            }
                                        }
                                        if(mysql_num_rows($cash_at_bank_list)&&$admin){
                                            while($cash_rows = mysql_fetch_array($cash_at_bank_list)){
                                                if($inventory != NULL){
                                                    $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
                                                    $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":$disabled;
                                                }else{
                                                    $disabled = "disabled";
                                                }
                                                $selected = ($inventory['CUST_ACC_CODE']==$cash_rows['ACCOUNT_CODE'])?"selected=\"selected\"":"";
            ?>
                                                <option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['ACCOUNT_CODE']; ?>" value="<?php echo $cash_rows['ACCOUNT_CODE']; ?>"><?php echo $cash_rows['ACCOUNT_TITLE']; ?></option>
            <?php
                                            }
                                        }
            ?>
                                            </select>
                                        </div>
                                      </div>
                                  </div>

                                  <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"> Mobile # :</label>
                                        <input type="text" id="mobilx" class="customer_mobile form-control text-center" value="<?php echo ($inventory != NULL)?$inventory['MOBILE_NO']:""; ?>" maxlength="11" />
                                    </div>
                                  </div>

                                  <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"> Customer Name :</label>
                                        <input type="text" class="customer_name form-control text-center" value="" />
                                    </div>
                                  </div>

                                  <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"> Customer Address :</label>
                                        <input type="text" class="customer_address form-control text-center" value="" />
                                    </div>
                                  </div>

                                  <div class="col-xs-6 hide">
                                      <input type="text" id="snamex" placeholder="Customer Name" class="supplier_name form-control text-center" name="supplier_name"  value="<?php echo ($inventory != NULL)?$inventory['CUSTOMER_NAME']:""; ?>" style="display:<?php echo (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"none":""; ?>;"  />
                                  </div>

                                  <div class="clear mb-20"></div>
                                  </div>
                                  <input type="hidden" class="sale_id" value="<?php echo $sale_id; ?>" />
                                  <input type="hidden" class="individual_discount" value="<?php echo ($individual_discount)?"Y":"N"; ?>" />
                                  <input type="hidden" class="use_taxes" value="<?php echo ($use_taxes)?"Y":"N"; ?>" />
                            </form>
														<div class="clear"></div>
                            <div class="panel panel-default">
                                <table class="prom">
                                    <thead>
                                        <tr>
                                           <th width="30%" style="font-size:12px;font-weight:normal;text-align:center">Item Name</th>
                                           <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Qty</th>
                                           <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">UnitPrice</th>
                                           <?php if($individual_discount){ ?>
                                           <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Discount</th>
                                           <?php } ?>
                                           <?php if($use_taxes){ ?>
                                           <th width="12%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                           <?php } ?>
                                           <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Amount</th>
                                           <th width="5%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="quickSubmit">
                                            <td>
                                                <select class="itemSelector show-tick form-control"
                                                        data-style="btn-default"
                                                        data-live-search="true" style="border:none">
                                                   <option selected value=""></option>
        <?php
                                            $item_category_array = array();
                                            if(mysql_num_rows($itemsCategoryList)){
                                                while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                                                    $item_category_array[] = $ItemCat;
                                                    $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
        ?>
                                                        <optgroup label="<?php echo $ItemCat['NAME']; ?>">
        <?php
                                                    if(mysql_num_rows($itemList)){
                                                        while($theItem = mysql_fetch_array($itemList)){
                                                            if($theItem['ACTIVE'] == 'N'){
                                                                continue;
                                                            }
                                                            if($theItem['INV_TYPE'] == 'B'){
                                                                continue;
                                                            }
        ?>
                                                            <option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
        <?php
                                                        }
                                                    }
        ?>
                                                        </optgroup>
        <?php
                                                }
                                            }
        ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="quantity form-control text-center"/>
                                            </td>
                                            <td>
                                                <input type="text" class="unitPrice form-control text-center" />
                                            </td>
                                            <?php if($individual_discount){ ?>
                                            <td>
                                                <input type="text" class="discount form-control text-center" value="" />
                                            </td>
                                            <?php } ?>
                                            <?php if($use_taxes){ ?>
                                            <td class="taxTd">
                                                <input type="text" class="form-control taxRate text-center" value=""  />
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <input type="text"  readonly value="0" class="totalAmount form-control text-center"/>
                                            </td>
                                            <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                        </tr>
                                    </tbody>
                                    <tbody class="calculations_body">
        <?php
                                $whole_tax = 0;
                                if(isset($saleDetails) && mysql_num_rows($saleDetails)){
                                    while($invRow = mysql_fetch_array($saleDetails)){
                                        $itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
        ?>
                                        <tr class="alt-row calculations transactions dynamix" data-row-id='<?php echo $invRow['ID']; ?>'>
                                            <td style="text-align:left;"
                                                class="itemName"
                                                data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
                                                <?php echo $itemName; ?>
                                            </td>
                                            <td style="text-align:center;" class="quantity"><?php echo $invRow['QUANTITY'] ?></td>
                                            <td style="text-align:center;" sub-amount="<?php echo $invRow['SUB_AMOUNT'] ?>" class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                            <?php if($individual_discount){ ?>
                                            <td style="text-align:center;" class="discount" data-discount-amount="<?php echo $invRow['SALE_DISCOUNT']; ?>"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                            <?php } ?>
                                            <?php if($use_taxes){ ?>
                                            <td style="text-align:center;" tax-amount="<?php echo $invRow['TAX_AMOUNT'] ?>" class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                                            <?php } ?>
                                            <td style="text-align:right;" class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                            <td style="text-align:center;">
                                               <a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
        <?php
                                        $whole_tax += $invRow['TAX_AMOUNT'];
                                    }
                                }
        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr class="totals">
                                            <td style="text-align:center;background-color:#EEEEEE;">Total</td>
                                            <td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                            <?php if($individual_discount){ ?>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                            <?php } ?>
                                            <?php if($use_taxes){ ?>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                            <?php } ?>
                                            <td style="text-align:center;background-color:#f5f5f5;" class="amountTotal"></td>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="clear"></div>
                            <div class="pull-right">
                                <div class="hide">
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Discount :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right discount_dom" placeholder="apply to all items"  value="" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Total Discount :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right total_discount"  value="<?php echo $inventory['SALE_DISCOUNT']; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Tax Amount :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right whole_tax" readonly  value="<?php echo isset($whole_tax)?$whole_tax:0; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right inv_charges"  value="<?php echo $inventory['CHARGES']; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
                                <div class="caption" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right grand_total" readonly="readonly" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Tendered :</div>
                                <div class="caption" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right received_cash" value="<?php echo $inventory['RECEIVED_CASH']; ?>" />
                                </div>
                                <div class="clear"></div>

                                <div class="balance-of-customer hide">
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Balance Receivable :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control customer-balance text-right" readonly />
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="return-to-customer">
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Change Given :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right change_return" value="<?php echo $inventory['CHANGE_RETURNED']; ?>" readonly />
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="recovered-from-customer hide">
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">
                                        Balance Recovered :
                                        <input id="cmn-toggle-recovered" value="X" class="css-checkbox recovered_balance" onclick="return recover_balance(this);" type="checkbox" <?php echo ($inventory['RECOVERED_BALANCE'] == 'Y')?"checked":""; ?> />
                                        <label for="cmn-toggle-recovered" class="css-label" style="margin-top:5px;margin-right:-25px;"></label>
                                    </div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right recovery_amount" value="<?php echo $inventory['RECOVERY_AMOUNT']; ?>" readonly />
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="remains-to-customer hide" style="display:<?php echo ((substr($inventory['CUST_ACC_CODE'], 0,6)!='010101')||$inventory==NULL)?"":"none"; ?>;">
                                    <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Receivable :</div>
                                    <div class="caption" style="width: 150px;margin-left:0px;">
                                        <input type="text" class="form-control text-right remaining_amount" value="<?php echo $inventory['REMAINING_AMOUNT']; ?>" readonly />
                                    </div>
                                    <div class="clear"></div>

                                </div>
                            </div>
                            <div class="clear" style="height:30px;"></div>
                              <div class="col-xs-12" style="text-align: right;padding-right:15px;">
	                              <?php
	                                  if(($sale_id > 0 && in_array('modify-sales',$permissionz)) || $admin == true || $sale_id == 0){
	                              ?>
	                                  <div class="button savePurchase"><?php echo ($sale_id > 0)?"Update":"Save"; ?></div>
	                                  <?php if($sms_config == 'Y' && ($sale_id > 0)){ ?>
	                                      <div class="button btn-sm" onclick="sendSmsPopUp();" > <i class="fa fa-commenting-o" style="font-size:1.8em;"></i></div>
	                                  <?php
	                                      }
	                                  ?>
	                              <?php
	                                  }
	                              ?>
	                              <?php
	                                  if($inventory != NULL){
	                              ?>
	                                  <a class="button print_that" target="new" href="<?php echo $invoiceFile; ?>?id=<?php echo $inventory['ID']; ?>"><i class="fa fa-print"></i> Print</a>
	                              <?php
	                                  }
	                              ?>
	                              <button class="button save_scan_and_print"><?php echo ($inventory==NULL)?"Save":"Update"; ?>  &amp; Print</button>
	                              <div class="button" onclick="window.location.href='sale-panel.php';">New Form</div>
	                              <div class="clear"></div>
	                          </div><!--underTheTable-->
                          </div><!--col-xs-8-->
                          <div class="col-xs-5">
                              <div style="display:<?php echo ($scanner_toggle=='Y')?"":"none"; ?>">
                                  <div id="form" class="scanner_div" style="text-align: center;margin:0px;;">
                                      <input type="text" class="barcode_input form-control" value="" placeholder="Scan Barcode" style="height:50px;font-size:26px;">
                                  </div> <!-- End form -->
                                  <div class="clear"></div>
                              </div>
                              <div class="clear mt-10"></div>
                              <div class="panel panel-default">
                                  <div class="panel-heading">
                                      Products &amp; Services
                                  </div>
                                  <div class="panel-body custom-height-x">
                                      <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                          <div class="carousel-inner" role="listbox">
                                              <div class="item active">
                                                  <?php
                                                      foreach($item_category_array as $key => $ItemCat) {
                                                      ?>
                                                          <div class="product-thumb" onclick="show_category_child('<?php echo $ItemCat['ITEM_CATG_ID']; ?>');">
                                                              <a href="#">
                                                                  <i class="fa fa-clone"></i>
                                                                  <p><?php echo $ItemCat['NAME']; ?></p>
                                                              </a>
                                                          </div>
                                                      <?php
                                                      }
                                                  ?>
                                              </div>
                                              <div class="item">
                                                  <?php
                                                      foreach ($item_category_array as $key => $ItemCat) {
                                                          $cat_id = $ItemCat['ITEM_CATG_ID'];
                                                          ?>
                                                              <ul class="list-group item-category-list" data-cat-id="<?php echo $cat_id ?>">
                                                                <li class="list-group-item btn text-left" onclick="show_category_list();">
                                                                    <a href="#" class="text-info"><i class="fa fa-arrow-left"></i> Back </a>
                                                                </li>
                                                              <?php
                                                                  $itemList = $objItems->getActiveListCatagorically($cat_id);
                                                                  if(mysql_num_rows($itemList)){
                                                                      while($theItem = mysql_fetch_array($itemList)){
                                                                          if($theItem['ACTIVE'] == 'N'){
                                                                              continue;
                                                                          }
                                                                          if($theItem['INV_TYPE'] == 'B'){
                                                                              continue;
                                                                          }
                                                              ?>
                                                                      <li class="list-group-item btn text-left m-5 touch-select"  data-item-idx="<?php echo $theItem['ID']; ?>"><?php echo $theItem['NAME']; ?></li>
                                                              <?php
                                                                      }
                                                                  }
                                                              ?>
                                                              </ul>
                                                          <?php
                                                      }
                                                  ?>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="clear"></div>
                          <div class="col-xs-8 hide">
                              <div class="panel panel-default pull-left" style="width:550px;">
                                  <div class="panel-heading">Notes : </div>
                                  <textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $inventory['NOTES']; ?></textarea>
                              </div>
                          </div>
                        <div class="clear" style="height: 20px;"></div>
                        <div class="clear"></div>
					</div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){

        $("#myCarousel").carousel({
            interval:false
        });

        $("input.customer_mobile").bind('keyup blur',function(e){
            if($(this).val().substr(0,1) != 0 || ($(this).val().length > 1 && $(this).val().substr(1,1) != 3)){
            	$("input.customer_mobile").val('');
            }
        });
        $("input.customer_mobile").on('change',function(e){
            var customer_mobile     = $("input.customer_mobile").val();
            if(customer_mobile != ''&&customer_mobile.length > 4){
                $.post('db/get-customer-contact.php',{customer_mobile:customer_mobile},function(data){
                    data = $.parseJSON(data);
                    if(data == null){
                        return;
                    }
                    $("input.customer_mobile").val(data['CUSTOMER_MOBILE']);
                    $("input.customer_name").val(data['CUSTOMER_NAME']);
                    $("input.customer_address").val(data['CUSTOMER_ADDRESS']);
                    $("input.customer_name").focus();
                });
            }
        });
	$("input.customer_mobile").trigger('change');
        $(".reload_item").click(function(){
            $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(data){
                 $("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
            });
        });
		$('.supplierSelector').selectpicker();
		$('.itemSelector').selectpicker();
		$('.taxRate').selectpicker();
		$(this).calculateColumnTotals();
        $("label#mor1").tooltipster({
            theme: 'tooltipster-light'
        });
        $("input[name=radiog_dark]").change(function(){
            if($(this).val() == 'C'){
                $("div.remains-to-customer").hide();
            }else{
                $("div.remains-to-customer").show();
            }
        });
        $("input.received_cash").change(function(){
            $(this).calculateColumnTotals();
        });
		$("input[name='billNum']").numericOnly();
		$("input.quantity").numericOnly();
        $(".whole_discount").numericFloatOnly();
		$("input[name='mobile_no']").numericOnly();
        $("input.customer_mobile").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("input[name='billNum']");
		$("input[name='billNum']").setFocusTo(".dropdown-toggle:first");
		$(".supplierSelector").change(function(){
            var cust_code = $(".supplierSelector option:selected").val();
            if(cust_code != ''){
                $.get('db/get-customer-details.php',{acc_code:cust_code},function(data){
                    if(data != ''){
                        data = $.parseJSON(data);
                        if($("input.customer_mobile").length){
                        	$("input.customer_mobile").val(data['MOBILE']).focus();
                        }else{
                        	$("div.itemSelector button").focus();
                        }
                    }
                });
            }
		});
        // $("input.customer_mobile").setFocusTo("input.customer_name");
        $("input.customer_name").setFocusTo("input.customer_address");
        $("input.customer_address").setFocusTo("div.itemSelector button");
		$("div.itemSelector button").keyup(function(e){
			if(e.keyCode == 13 && $('select.itemSelector option:selected').val() != ''){
				$("input.quantity").focus();
			}
		});
        $(document).keydown(function(e){
            if(e.keyCode == 32 && e.ctrlKey){
                $(".trasactionType").click();
                $(".supplier_name").focus();
            }
        });
        $("input.barcode_input").on('blur keyup',function(e){
            if(e.type == 'blur'||e.keyCode == 13){
                $(this).quickScan();
            }
        });
		$("div.itemSelector button").blur(function(e){
			if($('select.itemSelector option:selected').val() != ''){
				$(this).getItemDetails();
			}
		});
		var qtyE = 0;
		$("input.quantity").keyup(function(e){
            stockOs();
			if(e.keyCode == 13){
				if(parseInt($(this).val())||0){
					if(e.keyCode == 13){
                        if($("input.discount").length){
                            $("input.discount").focus();
                        }else{
                            if($("input.taxRate").length){
                                $("input.taxRate").focus();
                            }else{
                                $(this).calculateRowTotal();
                                $(".addDetailRow").focus();
                            }

                        }
                    }
				}
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
                if($("input.discount").length){
                    $("input.discount").focus();
                }else{
                    if($("input.taxRate").length){
                        $("input.taxRate").focus();
                    }else{
                        $(this).calculateRowTotal();
                        $(".addDetailRow").focus();
                    }

                }
			}
		});
        $("table").on('dblclick',"td.quantity",function(){
            var thisElm   = $(this);
            var thisQty   = parseInt($(this).text())||0;
            var item_id   = parseInt($(this).parent().find("[data-item-id]").attr("data-item-id"))||0;
            var itemPrice = parseFloat($(this).parent().find("td.unitPrice").text())||0;
            $.post('db/get-item-details.php',{s_item_id:item_id},function(data){
                data = $.parseJSON(data);
                $(thisElm).append('<input class="form-control input-nested" value="'+thisQty+'" />');
                $(thisElm).find(".input-nested").focus();
                $(thisElm).find(".input-nested").on('keyup blur',function(e){
                    stockOsQuick($(this),itemPrice,1);
                    var newQty = parseInt($(this).val())||0;
                    if(e.keyCode == 13||e.type == 'blur'){
                        if(newQty!=0){
                            $(this).parent().text(newQty);
                            $(this).calculateColumnTotals();
                        }
                    }
                });
            });
        });
        $("input.inv_charges").on('change keyup',function(e){
            $(this).calculateColumnTotals();
        });
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});
        $("input.taxRate").keydown(function(e){
            if(e.keyCode == 13){
                $(this).calculateRowTotal();
                $(".addDetailRow").focus();
            }
        });
		$(".taxTd").find(".taxRate").change(function(){
			$(this).calculateRowTotal();
			$(".addDetailRow").focus();
		});

		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
			if(e.keyCode == 27){
				$(".taxTd").find(".dropdown-toggle").focus();
			}
		});
		$(".addDetailRow").dblclick(function(e){
			e.preventDefault;
		});
		$(".savePurchase").click(function(){
			saveSale();
		});
        $(".save_scan_and_print").click(function(){
            print_method = 'Y';
            saveSale();
        });
		$("input[name='mobile_no']").focus(function(){
			check_sms_service();
		});

        $(".whole_discount").on('blur change keyup',function(){
            $(this).calculateColumnTotals();
        });
        $(".whole_discount").blur();

        $(".discount_dom").on('blur change',function(){
            var dominate_discount = $(this).val();

            if(dominate_discount == ''){
                return;
            }

            var discount_type = $("input.discount_type:checked").val();
            if(discount_type == 'R'){
                dominate_discount = dominate_discount/(parseInt($("tr.transactions").length)||0);
            }

            $("tr.transactions").each(function(){
                $(this).find("td.discount").text(dominate_discount);

                var taxType = ($(".taxType").is(":checked"))?"I":"E";
                var taxRate = parseFloat($(this).find("td.taxRate").text())||0;
                var thisVal = parseFloat($(this).find("td.quantity").text())||0;
                var thatVal = parseFloat($(this).find("td.unitPrice").text())||0;
                var amount  = Math.round((thisVal*thatVal)*100)/100;
                var discountAvail = parseFloat($(this).find("td.discount").text())||0;

                var discountPerCentage = 0;
                amount = Math.round(amount*100)/100;
                if($("input.individual_discount").val()=='Y'){
                    if(discount_type == 'R'){
                        discountPerCentage = discountAvail;
                    }else if(discount_type == 'P'){
                        discountAvail = Math.round(discountAvail*100)/100;
                        discountPerCentage = amount*(discountAvail/100);
                        discountPerCentage = Math.round(discountPerCentage*100)/100;
                    }
                    $(this).find("td.discount").attr('data-amount',discountPerCentage);
                    amount -= discountPerCentage;
                    amount = Math.round(amount*100)/100;
                }

                var taxAmount = 0;
                if(taxRate > 0){
                    if(taxType == 'I'){
                        taxAmount = amount*(taxRate/100);
                    }else if(taxType == 'E'){
                        taxAmount = amount*(taxRate/100);
                    }
                }
                if(taxType == 'I'){
                    amount -= taxAmount;
                }else{
                    amount += taxAmount;
                }
                $(this).find("td.totalAmount").text(amount);
            });
            $(this).calculateColumnTotals();
            $("input[name=radiog_dark]:checked").change();
        });

        $("div.itemSelector button").focus();
        $(window).keyup(function(e){
            if(e.keyCode == 113){
                window.location.href = 'sale-details.php';
            }
        });
        $(window).keydown(function(e){
            if(e.altKey == true&&e.keyCode == 73){
                e.preventDefault();
                $("#form").click();
                $("div.itemSelector button").click();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 65){
                e.preventDefault();
                $("#form").click();
                $("div.supplierSelector button").click();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 66){
                e.preventDefault();
                $("#form").click();
                $("input.barcode_input").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 67){
                e.preventDefault();
                $("#form").click();
                $("input.supplier_name").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 77){
                e.preventDefault();
                $("#form").click();
                $("input.customer_mobile").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 78){
                e.preventDefault();
                $("#form").click();
                $("textarea.inv_notes").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 68){
                e.preventDefault();
                $("#form").click();
                $("input.whole_discount").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 79){
                e.preventDefault();
                $("#form").click();
                $("input.inv_charges").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
<?php
        if(isset($_GET['print'])){
?>
        var win = window.open("<?php echo $invoiceFile; ?>?id=<?php echo $inventory['ID']; ?>","_blank");
        if(win){
            win.focus();
        }
<?php
        }
?>
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Bill Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Bill Updated Successfully!');
<?php
		}
?>

</script>
