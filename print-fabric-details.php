<?php
	ob_start();
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
    include('common/classes/company_details.php');    
	include('common/classes/item_category.php');
	include('common/classes/items.php');
	include('common/classes/customers.php');
	include('common/classes/suppliers.php');
	include('common/classes/brokers.php');
	include('common/classes/branches.php');
	include('common/classes/branch_stock.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/fabric_contract_details.php');

	//Permission
  if(!in_array('fabric-contracts',$permissionz) && $admin != true){
  echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
  echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
  echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
  echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
  echo '</div>';
  exit();
  }
  //Permission ---END--

	$objItemCategory        	 = new ItemCategory();
	$objItems               	 = new Items();
  $objCompanyDetails         = new CompanyDetails();
	$objCustomer            	 = new Customers();
	$objSupplier        			 = new Suppliers();
	$objBrokers    		  			 = new Brokers();
	$objUserAccount         	 = new UserAccounts();
	$objBranches  	   				 = new Branches();
	$objBranchStock    				 = new BranchStock();
	$objFabricContracts     	 = new FabricContracts();
	$objFabricContractDetails  = new FabricContractDetails();

	$supplier_list 			= $objSupplier->getList();
	$broker_list 				= $objBrokers->getList();
  $company_profile    = $objCompanyDetails->getActiveProfile();

	$fabric_id 				 = 0;
	$fabric_inward_row = NULL;

	if(isset($_GET['id'])){
		$fabric_id = (int)(mysql_real_escape_string($_GET['id']));
		$fabric_inward_row = $objFabricContracts->getDetail($fabric_id);
		$fabric_contract_list= $objFabricContractDetails->getListById($fabric_id);
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/hitex-print.css" type="text/css" media="screen" />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/towords.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($(".for-print").length){
                $(".for-print").printThis({
                    debug: false,
                    importCSS: false,
                    printContainer: false,
                    loadCSS: 'resource/css/hitex-print.css',
                    pageTitle: "SIT Solution",
                    removeInline: false,
                    printDelay: 500,
                    header: null
                });
            }
            $("*").removeAttr('style');
            $("textarea").each(function(){
                $(this).replaceWith("<div class='textarea'>"+$(this).html()+"</div>");
            });
            $("input").focus(function(){
                $(this).blur();
            });
        });
    </script>
</head>
<body>
<div id="bodyWrapper">
    <div class="content-box-top" style="overflow:visible;">
        <div class="summery_body">
            <div class="for-print">
                <div class="hitex-header">
                    <img class="hitext-logo pull-left" src="uploads/logo/<?php echo $company_profile['LOGO'] ?>" />
                    <div class="title text-left">
                      <?php echo $company_profile['NAME'] ?>
                    </div>
                    <p>
                      <?php echo $company_profile['ADDRESS']; ?><br />
                      <?php echo $company_profile['CONTACT']; ?><br />
                      <?php echo $company_profile['EMAIL']; ?>
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div>
                    <div class="title">FABRIC PURCHASE CONTRACT</div>
                    <div class="caption">Lot # :</div>
                            <div class="field mid">
                                <input type="text" class="form-control" name="sc_no" id="pon" value="<?php echo $fabric_inward_row['LOT_NO']; ?>" />
                            </div>
                            <div class="caption"> Purchase Date :</div>
                            <div class="field datepicker">
                                <input type="text" class="form-control" name="contract_date" value="<?php echo date('d-m-Y',strtotime($fabric_inward_row['CONTRACT_DATE'])) ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Supplier :</div>
                            <div class="field mid">
                                <input type="text" class="form-control" name="sc_no" id="pon" value="<?php echo $objSupplier->getTitle($fabric_inward_row['SUPPLIER_ID']); ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Broker :</div>
                            <div class="field mid">
                                <input type="text" class="form-control" name="sc_no" id="pon" value="<?php echo $objBrokers->getTitleByCode($fabric_inward_row['BROKER_ID']); ?>" />
                            </div>
                            <div class="caption">Broker Commission:</div>
                            <div class="field datepicker">
                                <input type="text" class="form-control" name="ref" value="<?php echo $fabric_inward_row['BROKER_COMMISSION']; ?>" />
                            </div>
                            <div class="clear"></div>

														<div class="caption">To Dyeing Unit :</div>
                            <div class="field mid">
                                <input type="text" class="form-control" name="sc_no" id="pon" value="<?php echo $objSupplier->getTitle($fabric_inward_row['PROCESSING_UNIT']); ?>" />
                            </div>
                            <div class="caption">Dyeing UT Lot #:</div>
                            <div class="field datepicker">
                                <input type="text" class="form-control" name="ref" value="<?php echo $fabric_inward_row['DYEING_UNIT_LOT_NO']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <hr />
                            <table class="col-xs-12">
        											<thead>
        												<tr>
        													<th width="20%" class="text-center">Construction</th>
        													<th width="20%" class="text-center">Variety</th>
																	<th width="10%" class="text-center">Thaan</th>
        													<th width="10%" class="text-center">Meters</th>
        													<th width="10%" class="text-center">Rate/Mtr</th>
        													<th width="10%" class="text-center">Amount</th>
        												</tr>
        											</thead>
        											<tbody>
        												<?php
        													$total_quantity = 0;
        													$total_amount = 0;
        													if(isset($fabric_contract_list)&&mysql_num_rows($fabric_contract_list)){
        														while($fabric_detail_row = mysql_fetch_assoc($fabric_contract_list)){
        															$total_quantity += $fabric_detail_row['QUANTITY'];
        															$total_amount   += $fabric_detail_row['TOTAL_AMOUNT'];
        																?>
        																<tr class='table_row' data-id="<?php echo $fabric_detail_row['ID']; ?>">
        																	<td class='text-left'><?php echo $fabric_detail_row['CONSTRUCTION']; ?></td>
        																	<td class='text-center'><?php echo $fabric_detail_row['VARIETY']; ?></td>
																					<td class='text-center'><?php echo $fabric_detail_row['THAAN']; ?></td>
        																	<td class='text-center'><?php echo $fabric_detail_row['QUANTITY']; ?></td>
        																	<td class='text-center'><?php echo $fabric_detail_row['UNIT_PRICE']; ?></td>
        																	<td class='text-center'><?php echo $fabric_detail_row['TOTAL_AMOUNT']; ?></td>
        																</tr>
        																<?php
        														}
        													}
        												?>
        											</tbody>
        											<tfoot>
        												<tr>
        													<td class="text-right" colspan="2"><span class="pr-10">Total</span></td>
        													<td class="text-center quantity_total"><?php echo $total_quantity; ?></td>
        													<td class="text-center"> - - - </td>
																	<td class="text-center"> - - - </td>
        													<td class="text-center amount_total"><?php echo $total_amount; ?></td>
        												</tr>
        											</tfoot>
        										</table>
                            <div class="clear"></div>
                </div><!--form-->
                <div class="hitex-signatures"></div><!--hitex-signatures-->
            </div><!--bodyTab1-->
        </div><!--summery_body-->
    </div><!--content-box-top-->
    <div style = "clear:both; height:20px"></div>
    </body>
    </html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
