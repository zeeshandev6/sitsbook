<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/sale.php');
	include('common/classes/accounts.php');
	include('common/classes/j-voucher.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/customers.php');

	//Permission
	if( (!in_array('sales',$permissionz)) && (!in_array('sales-panel',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objSale               = new Sale();
	$objJournalVoucher     = new JournalVoucher();
	$objAccountCodes       = new ChartOfAccounts();
	$objItems              = new Items();
	$objServices           = new Services();
	$objCustomers          = new Customers();
	$objConfigs            = new Configs();

	$sms_config 	 = $objConfigs->get_config('SMS');

	$totalRows = $objSale->countRows();

	$salesPanelFile = (in_array('sales-panel',$permissionz) && $salePanelModule == 'Y')?"sale-panel.php":"sale-details.php";

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);

	$currency_type  = $objConfigs->get_config('CURRENCY_TYPE');

	$total 		      = $objConfigs->get_config('PER_PAGE');

	$invoiceSize    = $invoice_format[0]; // S  L

	if($invoiceSize == 'L'){
		$invoiceFile  	 = 'sales-invoice.php';
		$invoiceFileUrdu = 'sales-invoice-urdu.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile  		= 'sales-invoice-duplicates.php';
		$invoiceFileUrdu  = 'sales-invoice-duplicates-urdu.php';
	}elseif($invoiceSize == 'S'){
		$invoiceFile  		= 'sales-invoice-small.php';
		$invoiceFileUrdu  = 'sales-invoice-small-urdu.php';
	}
	$origin_invoice = $invoiceFile;

	$customersList  = $objCustomers->getList();
	$cashAccounts   = $objAccountCodes->getAccountByCatAccCode('010101');

	$objSale->fromDate 		  	= (isset($_GET['fromDate']))?date('Y-m-d',strtotime($_GET['fromDate'])):"";
	$objSale->toDate 		  		= (isset($_GET['toDate']))?date('Y-m-d',strtotime($_GET['toDate'])):"";
	$objSale->supplierAccCode = (isset($_GET['supplierAccCode']))?mysql_real_escape_string($_GET['supplierAccCode']):"";
	$objSale->tax_bill_num    = (isset($_GET['tax_bill_num']))?(int)$_GET['tax_bill_num']:"";
	$objSale->billNum   	  	= (isset($_GET['billNum']))?(int)$_GET['billNum']:"";
	$objSale->with_tax     	  = (isset($_GET['tax_invoice']))?$_GET['tax_invoice']:"";

	if(!$admin){
		$objSale->user_id = $user_id;
	}

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$saleList 		 = $objSale->search($start, $total);
	$found_records = $objSale->found_records;
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		table td{
			padding: 7px !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sale.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" >
	$(document).ready(function() {
		$(".selectpicker").selectpicker();
		$("a.delete_record").click(function(){
			$(this).delete_main_list_record("db/del-sale.php");
		});
		$("td[data-items-list]").on("mouseover",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data);
		});
		$("td[data-items-list]").on("mouseout",function(){
			var data = $(this).attr("data-items-list");
			$(this).text(data.substr(0,40));
		});
	});
	</script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<?php include("common/left_menu.php") ?>
	</div> <!-- End #sidebar -->
	<div class="content-box-top">
		<div class="content-box-header">
			<p>Sale Management - <?php echo $found_records; ?> Records</p>
			<span id="tabPanel">
				<div class="tabPanel">
					<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
					<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
					<a href="<?php echo $salesPanelFile; ?>"><div class="tab">New</div></a>
				</div>
			</span>
			<div class="clear"></div>
		</div> <!-- End .content-box-header -->
		<div class="content-box-content">
			<div id="bodyTab1" style="display:block;">
				<table width="100%" cellspacing="0" >
					<thead>
						<tr>
							<th style="text-align:center;width:5%;">Invoice#</th>
							<th style="text-align:center;width:5%;" class="<?php echo ($saleTaxModule=='Y')?"":"hide"; ?>">S.T.Inv#</th>
							<th style="text-align:center;width:10%;">InvoiceDate</th>
							<th style="text-align:center;width:15%;">Customer</th>
							<th style="text-align:center;width:20%;">Items</th>
							<th style="text-align:center;width:10%">Qty Sold</th>
							<th style="text-align:center;width:10%;">Total Amount(<?php echo $currency_type; ?>)</th>
							<th style="text-align:center;width:5%" class="<?php echo ($saleTaxModule=='Y')?"":"hide"; ?>">TAX Invoice</th>
							<?php if($sms_config=='Y'){ ?>
							<th style="text-align:center;width:5%">SMS</th>
							<?php } ?>
							<th style="text-align:center;width:5%;">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(mysql_num_rows($saleList)){
							while($saleRow = mysql_fetch_array($saleList)){
								$billTotal 					= $objSale->getInventoryAmountSum($saleRow['ID']);
								$supplierTitle 			= $objAccountCodes->getAccountTitleByCode($saleRow['CUST_ACC_CODE']);
								$billTotalQuantity 	= $objSale->getQuantityPerBill($saleRow['ID']);
								$itemsArray    			= $objSale->getSaleDetailArrayItemOnly($saleRow['ID']);
								$servicesArray 			= $objSale->getSaleDetailArrayServiceOnly($saleRow['ID']);
								$itemsList 					= '';

								foreach($servicesArray as $key => $service_id){
									if($key > 0){
										$itemsList .= ', ';
									}
									$itemsList .= $objServices->getTitle($service_id);
								}
								if($itemsList != ''){
									$itemsList .= ', ';
								}
								foreach($itemsArray as $key => $item_id){
									if($key > 0){
										$itemsList .= ', ';
									}
									$itemsList .= $objItems->getItemTitle($item_id);
								}

								if($saleRow['DISCOUNT_TYPE'] == 'P'){
									$billTotal -= (($billTotal*$saleRow['DISCOUNT'])/100);
								}else{
									$billTotal -= $saleRow['DISCOUNT'];
								}

								$billTotal += $saleRow['CHARGES'];

								if($saleRow['WITH_TAX']=="Y"){
						      $invoiceFile = 'sales-tax-invoice.php';
						    }else{
									$invoiceFile = $origin_invoice;
								}
								?>
								<tr data-row-id="<?php echo $saleRow['ID']; ?>" data-main-id="<?php echo $saleRow['ID']; ?>">
									<td style="text-align:center"><?php echo $saleRow['BILL_NO']; ?></td>
									<td style="text-align:center" class="<?php echo ($saleTaxModule=='Y')?"":"hide"; ?>"><?php echo $saleRow['TAX_BILL_NO']; ?></td>
									<td style="text-align:center"><?php echo date('d-m-Y',strtotime($saleRow['SALE_DATE'])); ?></td>
									<td style="text-align:left"><?php echo $supplierTitle; ?></td>
									<td style="text-align:left;" class="pl-10" data-items-list="<?php echo $itemsList; ?>"><?php echo substr($itemsList, 0,40); ?></td>
									<td style="text-align:center"><?php echo $billTotalQuantity; ?></td>
									<td style="text-align:center"><?php echo number_format($billTotal,2); ?></td>
									<td class="text-center <?php echo ($saleRow['WITH_TAX']=='Y')?"text-success":"text-primary"; ?> <?php echo ($saleTaxModule=='Y')?"":"hide"; ?>"><?php echo ($saleRow['WITH_TAX']=='Y')?"YES":"NO"; ?></td>
									<?php if($sms_config=='Y'){ ?>
									<td style="text-align:center">
										<button type="button" class="btn btn-low-height btn-<?php echo $saleRow['SMS_STATUS']=='Y'?"success":"default"; ?> btn-sm" data-id="<?php echo $saleRow['ID']; ?>" onclick="send_sms_from_list(this,'sale-details.php');"><i class="fa fa-<?php echo $saleRow['SMS_STATUS']=='Y'?"check":"envelope"; ?>"></i></button>
									</td>
									<?php } ?>
									<td style="text-align:center;padding:5px 0px !important;">
										<div class="dropdown">
											<button class="dropdown-toggle" id="view_button" type="button" data-toggle="dropdown"> <i class="fa fa-bars"></i>
												<span class="caret"></span></button>
												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="<?php echo $salesPanelFile; ?>?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-pencil"></i> Edit </a></li>
													<li><a href="<?php echo $salesPanelFile; ?>?c&id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-copy"></i> Copy</a></li>
													<li><a target="_blank" href="<?php echo $invoiceFile ?>?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-print"></i> Invoice</a></li>
													<?php if($urdu_account_names == 'Y'){ ?>
													<li><a target="_blank" href="<?php echo $invoiceFileUrdu; ?>?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-print"></i> Invoice Urdu</a></li>
													<?php } ?>
													<li><a target="_blank" href="voucher-print.php?id=<?php echo $saleRow['VOUCHER_ID']; ?>" title="Print"> <i class="fa fa-print"></i> Voucher</a></li>
													<?php if(in_array('delete-sales',$permissionz) || $admin == true){ ?>
														<li><a href="#" do="<?php echo $saleRow['ID']; ?>" class="delete_record" title="Delete"> <i class="fa fa-times"></i> Delete</a></li>
														<?php } ?>
														<?php if(in_array('sale-returns',$permissionz) || $admin == true){ ?>
															<li><a href="sale-return-details.php?sid=<?php echo $saleRow['ID']; ?>" title="Return" > <i class="fa fa-reply"></i> Return</a></li>
															<?php } ?>
														</ul>
													</div>
												</td>
											</tr>
											<?php
										}
									}else{
										?>
										<tr>
											<th colspan="100" style="text-align:center;">
												<?php echo (isset($_POST['searchInventory']))?"0 Records Found!!":"0 Records!!" ?>
											</th>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
							<div class="col-xs-12 text-center">
								<?php
								if($found_records > $total){
									$get_url = "";
									foreach($_GET as $key => $value){
										$get_url .= ($key == 'page')?"":"&".$key."=".$value;
									}
									?>
									<nav>
										<ul class="pagination">
											<?php
											$count = $found_records;
											$total_pages = ceil($count/$total);
											$i = 1;
											$thisFileName = basename($_SERVER['PHP_SELF']);
											if(isset($this_page) && $this_page>0){
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
												</li>
												<?php
											}
											if(isset($this_page) && $this_page>=1){
												$prev = $this_page;
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
												</li>
												<?php
											}
											$this_page_act = $this_page;
											$this_page_act++;
											while($total_pages>=$i){
												$left = $this_page_act-5;
												$right = $this_page_act+5;
												if($left<=$i && $i<=$right){
													$current_page = ($i == $this_page_act)?"active":"";
													?>
													<li class="<?php echo $current_page; ?>">
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
													</li>
													<?php
												}
												$i++;
											}
											$this_page++;
											if(isset($this_page) && $this_page<$total_pages){
												$next = $this_page;
												?>
												<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
												<?php
											}
											if(isset($this_page) && $this_page<$total_pages){
												?>
												<li>
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
												</li>
											</ul>
										</nav>
										<?php
									}
								}
								?>
							</div>
						</div> <!--End bodyTab1-->
						<div style="height:0px;clear:both"></div>
						<div id="bodyTab2" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"block":"none"; ?>;" >
							<div id="form">
								<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

									<div class="caption"></div>
									<div class="message red"><?php echo (isset($message))?$message:""; ?></div>
									<div class="clear"></div>

									<div class="caption">From Date </div>
									<div class="field">
										<input type="text" class="form-control datepicker" name="fromDate" style="width:150px;" />
									</div>
									<div class="clear"></div>

									<div class="caption">To Date </div>
									<div class="field">
										<input type="text" class="form-control datepicker" name="toDate" style="width:150px;" value="<?php echo date('d-m-Y'); ?>" />
									</div>
									<div class="clear"></div>

									<?php if($saleTaxModule=='Y'){ ?>
									<div class="caption">Tax Invoice </div>
									<div class="field">

										<input id="cmn-toggle-taxinvoicea" value="" class="css-checkbox tax_invoice" type="radio" name="tax_invoice" checked />
										<label for="cmn-toggle-taxinvoicea" class="css-label-radio" style="margin-top:5px;margin-right:25px;">Any</label>

										<input id="cmn-toggle-taxinvoicey" value="Y" class="css-checkbox tax_invoice" type="radio" name="tax_invoice" />
										<label for="cmn-toggle-taxinvoicey" class="css-label-radio" style="margin-top:5px;margin-right:25px;">Yes</label>

										<input id="cmn-toggle-taxinvoicen" value="N" class="css-checkbox tax_invoice" type="radio" name="tax_invoice" />
										<label for="cmn-toggle-taxinvoicen" class="css-label-radio" style="margin-top:5px;margin-right:25px;">No</label>
									</div>
									<div class="clear"></div>

									<?php } ?>

									<div class="caption">Customer</div>
									<div class="field" style="width:305px;">
										<select class="selectpicker form-control"
										name='supplierAccCode'
										data-style="btn-default"
										data-live-search="true" style="border:none">
										<option selected value=""></option>
										<?php
										if(mysql_num_rows($cashAccounts)){
											while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
												?>
												<option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
												<?php
											}
										}
										?>
										<?php
										if(mysql_num_rows($customersList)){
											while($account = mysql_fetch_array($customersList)){
												?>
												<option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
												<?php
											}
										}
										?>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption">Invoice No</div>
								<div class="field">
									<input type="text" value="" name="billNum" class="form-control" />
								</div>
								<div class="clear"></div>

								<?php if($saleTaxModule=='Y'){ ?>
								<div class="caption">Tax Invoice No.</div>
								<div class="field">
									<input type="text" value="" name="tax_bill_num" class="form-control" />
								</div>
								<div class="clear"></div>
								<?php } ?>

								<div class="caption"></div>
								<div class="field">
									<input type="submit" value="Search" name="search" class="button"/>
								</div>
								<div class="clear"></div>
							</form>
						</div><!--form-->
					</div> <!-- End bodyTab2 -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->
		</div><!--body-wrapper-->
		<div id="xfade"></div>
		</body>
		</html>
		<?php include('conn.close.php'); ?>
		<script>
			$(document).ready(function() {
				$(".shownCode,.loader").hide();
				$("input.supplierTitle").keyup(function(){
					$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
				});
				$(window).keydown(function(e){
					if(e.keyCode==113){
						e.preventDefault();
						window.location.href = "<?php echo "sale-details.php"; ?>";
					}
				});
			});
			$(function(){
				$("#fromDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
				$("#toDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});<?php echo (isset($_POST['searchInventory']))?"tab('1', '1', '2');":""; ?><?php echo (isset($_GET['tab']) && $_GET['tab']=='search')?"tab('2', '1', '2');":""; ?>
			});
			</script>
