<?php
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/machines.php');
    include ('common/classes/sheds.php');

    $objMachines  = new Machines();
    $objSheds     = new Sheds();

    if(isset($_POST['delete_id'])){
        $delete_id =  mysql_real_escape_string($_POST['delete_id']);
        $deleted = $objMachines->delete($delete_id);
        exit();
    }
    $mode = "";
    if(!isset($_GET['search'])){
        $path = "machine-management.php?title=&status=&search=Search";
        if(isset($_GET['tab'])){
            $path .= "&tab=search";
        }
        echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        exit();
    }
    if(isset($_GET['search'])){
        $end = 20;
        $objMachines->title    = $_GET['title'];
        $objMachines->status   = $_GET['status'];
        $myPage = 1;
        $start  = 0;
        if(isset($_GET['page'])){
            $page   = $_GET['page'];
            $myPage = $page;
            $start  = ((int)$page * $end) - $end;
        }
        $machine_list = $objMachines->search($start,$end);
        $row          = mysql_num_rows($machine_list);
        $queryCount   = $objMachines->totalMatchRecords;
        $pageCount    = ceil($queryCount/$end);
    }else{
        $machine_list = $objMachines->getList();
        $row          = mysql_num_rows($machine_list);
    }
?>
<!DOCTYPE html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
        <link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
        <link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
        <link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
        <link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
        <link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
        <link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
        <style media="screen">
          table th{
            font-weight: bold !important;
          }
          table th,table td{
            padding: 10px !important;
          }
        </style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
            //delete product
            function deleteMachine(x){
                $("#myConfirm").modal('show');
                $(document).on('click', 'button#delete', function(){
                    var delete_id = $(x).attr("value");
                    var del = $.post("machine-management.php", {delete_id:delete_id}, function(data){});
                    if(del){
                        $(x).parent().parent().remove();
                    }
                });
                $(document).on('click', 'button#cancell', function(){
                    x = null;
                });
            }
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class="content-box-header">
                  <p>Machines Management</p>
                  <span id="tabPanel">
                      <div class="tabPanel">
                          <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                          <a href="machine-details.php"><div class="tab">New</div></a>
                      </div>
                  </span>
                  <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>
                <div id="bodyTab1">
                    <table class="table table-responsive">
                        <thead>
                          <tr>
                              <th width="10%" class="text-center">Mach No.</th>
                              <th width="40%" class="text-center">Name</th>
                              <th width="10%" class="text-center">Rate</th>
                              <th width="10%" class="text-center">Machine RPM</th>
                              <th width="10%" class="text-center">Location</th>
                              <th width="10%" class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($row>0){
                            $counter = 1;
                            while($machine_row = mysql_fetch_assoc($machine_list)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="measure_type-list text-center"><?php echo $machine_row['MACHINE_NO']; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $machine_row['NAME']; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $machine_row['RATE']; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $machine_row['MACHINE_RPM']; ?></td>
                                    <td class="measure_type-list"><?php echo $objSheds->getTitleStr($machine_row['SHED_ID']); ?></td>
                                    <td style="text-align:center">
                                        <a href="machine-details.php?id=<?php echo $machine_row['ID']; ?>">
                                            <input type="button" value="View" id="view_button" title="View/Update" />
                                        </a>
                                        <a onClick="deleteMachine(this);" id="del" value="<?php echo $machine_row['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times" ></i></a>
                                    </td>
                                </tr>
                            <?php
                                $counter++;
                            }
                        }
                        else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php
                    if(isset($queryCount)){
                        if($queryCount>$pageCount){
                            ?>
                    <table align="center" border="0" id="navbar">
                        <tr>
                            <td style="text-align: center">
                                        <nav>
                                            <ul class="pagination">
                                                <?php
                                                if($myPage > 1)
                                                {
                                                    echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=1'>Fst</a></li>";
                                                    echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage-1) ."'>Prv</a></li>";
                                                }

                                                for($x=$myPage-4; $x<$myPage; $x++)
                                                {
                                                    if($x > 0){
                                                        echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $x ."'>". $x ."</a></li>";
                                                    }
                                                }

                                                print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                                                for($y=$myPage+1; $y<$myPage+5; $y++)
                                                {
                                                    if($y <= $pageCount){
                                                        echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $y ."'>". $y ."</a></li>";
                                                    }
                                                }

                                                if($myPage < $pageCount)
                                                {
                                                    echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage+1) ."'>Nxt</a></li>";
                                                    echo "<li><a href='machine-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $pageCount ."'>Lst</a></li>";
                                                }

                                                ?>
                                            </ul>
                                        </nav>
                            </td>
                        </tr>
                    </table>
                    <?php
                  }
                }
                ?>
                </div> <!--bodyTab1-->
                <div class="clear"></div>
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
    </div><!--bodyWrapper-->
    <!-- Delete confirmation popup -->
    <div id="myConfirm" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </body>
    </html>
<?php include('conn.close.php'); ?>
