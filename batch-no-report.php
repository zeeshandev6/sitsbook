<?php
    $out_buffer = ob_start();
    include('msgs.php');
    include('common/connection.php');
    include('common/config.php');
    include('common/classes/items.php');
    include('common/classes/itemCategory.php');
    include('common/classes/item_batch_stock.php');
    include('common/classes/company_details.php');

    $objItems        			 = new Items();
    $objItemCategory 			 = new itemCategory();
    $objItemBatchStock     = new ItemBatchStock();
    $objCompanyDetails     = new CompanyDetails();

    if(isset($_POST['change_state'])){
      $status = mysql_real_escape_string($_POST['change_state']);
      $id     = mysql_real_escape_string($_POST['id']);
      $objItemBatchStock->setBatchStatus($id,$status);
      exit();
    }
    $itemsCategoryList   = $objItemCategory->getList();
    if(isset($_GET['search'])){
      $objItemBatchStock->item_id           = mysql_real_escape_string($_GET['item_id']);
      $objItemBatchStock->batch_no          = mysql_real_escape_string($_GET['batch_no']);
      $objItemBatchStock->from_date         = ($_GET['from_date']=='')?"":date('Y-m-d',strtotime($_GET['from_date']));
      $objItemBatchStock->to_date           = ($_GET['to_date']=='')?"":date('Y-m-d',strtotime($_GET['to_date']));
      $objItemBatchStock->stock             = 0;
      $objItemBatchStock->active_status     = mysql_real_escape_string($_GET['active_status']);
      $batch_list                           = $objItemBatchStock->search();

    }
    $company_details = $objCompanyDetails->getActiveProfile();
?>
    <!DOCTYPE html 
        >
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <style media="screen">
          table.table-responssive th{
            padding: 5px !important;
            font-size: 16px !important;
          }
          table.table-responssive td{
            padding: 5px !important;
            font-size: 14px !important;
          }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/configuration.js"></script>
        <script src="resource/scripts/printThis.js"></script>
        <script src="resource/scripts/tab.js"></script>
        <script>
            $(document).ready(function(){
              $("select").selectpicker();
              $("input.status-change").change(function(){
                var status = $(this).is(":checked")?"Y":"N";
                var id     = parseInt($(this).attr("data-id"))||0;
                $.post('?',{change_state:status,id:id},function(data){});
              });
              $("button.print-now").click(function(){
                $(".print-div").printThis({
        					debug: false,
        					importCSS: false,
        					printContainer: true,
        					loadCSS: 'resource/css/daily-report.css',
        					pageTitle: "sitsbook.net",
        					removeInline: false,
        					printDelay: 500,
        					header: null
        				});
              });
              <?php if(isset($_GET['search'])){ ?>
                tab('1', '1', '2');
              <?php } ?>
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
      <div class="content-box-top">
    		<div class="content-box-header">
    			<p>Available Batch Stock</p>
    			<span id="tabPanel">
    				<div class="tabPanel">
    					<div class="tab" id="tab1" onClick="tab('1', '1', '2');">List</div>
    					<div class="tabSelected" id="tab2" onClick="tab('2', '1', '2');">Search</div>
    				</div>
    			</span>
    			<div class="clear"></div>
    		</div> <!-- End .content-box-header -->
    		<div class="content-box-content">
    			<div id="bodyTab1" style="display:none;">
            <button type="button" class="btn btn-default pull-right print-now mr-10"><i class="fa fa-print"></i></button>
            <div class="clear"></div>

            <div class="print-div col-xs-12">
              <div class="pageHeader">
								<div class="logo_aside">
									<h1 class="h1"><?php echo $company_details['NAME']; ?></h1>
									<p class="slogan">
										Batch Stock Report
									</p>
									<p class="underSlogan">
										Report Date : <?php echo date("d-m-Y"); ?>
									</p>
								</div>
								<div class="clear"></div>
							</div>
              <table class="table-responssive mt-10">
                <thead>
                  <tr>
                    <th class="text-center">Sr. #</th>
                    <th class="text-center">Item Title</th>
                    <th class="text-center">Batch #</th>
                    <th class="text-center">Expiry Date</th>
                    <th class="text-center">Stock</th>
                    <th class="text-center noPrint">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $counter = 1;
                    if(isset($batch_list)&&mysql_num_rows($batch_list)){
                      while($row = mysql_fetch_assoc($batch_list)){
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $counter; ?></td>
                    <td class="text-left"><?php echo $objItems->getItemTitle($row['ITEM_ID']) ?></td>
                    <td class="text-center"><?php echo $row['BATCH_NO'] ?></td>
                    <td class="text-center"><?php echo date('d-m-Y',strtotime($row['EXPIRY_DATE'])) ?></td>
                    <td class="text-center"><?php echo $row['STOCK'] ?></td>
                    <td class="text-center noPrint">
                      <input id="cmn-toggle-<?php echo $row['ID']; ?>" value="Y" class="css-checkbox status-change" data-id="<?php echo $row['ID']; ?>" type="checkbox" <?php echo ($row['ACTIVE_STATUS'] == 'Y')?"checked":""; ?> />
                      <label for="cmn-toggle-<?php echo $row['ID']; ?>" class="css-label" style="margin-top:5px;margin-right:-25px;"></label>
                    </td>
                  </tr>
                  <?php
                      $counter++;
                    }
                  }else{
                    ?>
                    <tr>
                      <td colspan="6" class="text-center">
                         <p>
                           <b><i class="fa fa-search text-primary"></i> <?php echo isset($_GET['search'])?"No Records Found!":"Please submit search form."; ?></b>
                         </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
    		</div> <!--End bodyTab1-->
        <div class="clear"></div>
				<div id="bodyTab2">
					<div id="form">
						<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

							<div class="caption">From Date </div>
							<div class="field">
								<input type="text" class="form-control datepicker" name="from_date" style="width:150px;" value="<?php echo isset($_GET['from_date'])&&$_GET['from_date']!=''?date('d-m-Y',strtotime($_GET['from_date'])):""; ?>" />
							</div>
							<div class="clear"></div>

							<div class="caption">To Date </div>
							<div class="field">
                <input type="text" class="form-control datepicker" name="to_date" style="width:150px;" value="<?php echo isset($_GET['to_date'])&&$_GET['to_date']!=''?date('d-m-Y',strtotime($_GET['to_date'])):''; ?>" />
							</div>
							<div class="clear"></div>

              <div class="caption">Items</div>
              <div class="field">
                <select class="show-tick form-control" name="item_id" data-style="btn-default" data-live-search="true">
                   <option selected value=""></option>
<?php
            if(mysql_num_rows($itemsCategoryList)){
                while($ItemCat = mysql_fetch_array($itemsCategoryList)){
$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
if(mysql_num_rows($itemList)){
while($theItem = mysql_fetch_array($itemList)){
  if($theItem['ACTIVE'] == 'N'){
    continue;
  }
  if($theItem['INV_TYPE'] == 'B'){
    continue;
  }
?>
  <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
}
}
?>
</optgroup>
<?php
                }
            }
?>
                </select>
              </div>
              <div class="clear"></div>

              <div class="caption">Batch #</div>
              <div class="field">
                <input type="text" class="form-control" name="batch_no" style="width:150px;" value="<?php echo isset($_GET['batch_no'])?$_GET['batch_no']:''; ?>" />
              </div>
              <div class="clear"></div>

              <div class="caption">Priority</div>
              <div class="field" style="padding-top: 10px;">
                <input id="cmn-toggle-active-1" name="active_status" value="" class="css-checkbox" type="radio" <?php echo !isset($_GET['active_status'])||(isset($_GET['active_status'])&&$_GET['active_status']=='')?"checked":""; ?> />
                <label for="cmn-toggle-active-1" class="css-label-radio">Any</label>
                <input id="cmn-toggle-active-2" name="active_status" value="Y" class="css-checkbox" type="radio" <?php echo isset($_GET['active_status'])&&$_GET['active_status']=='Y'?"checked":""; ?> />
                <label for="cmn-toggle-active-2" class="css-label-radio">High</label>
                <input id="cmn-toggle-active-3" name="active_status" value="N" class="css-checkbox" type="radio" <?php echo isset($_GET['active_status'])&&$_GET['active_status']=='N'?"checked":""; ?> />
                <label for="cmn-toggle-active-3" class="css-label-radio">Normal</label>
              </div>
              <div class="clear"></div>

							<div class="caption"></div>
							<div class="field">
								<input type="submit" value="Search" name="search" class="button"/>
							</div>
							<div class="clear"></div>
					</form>
				</div><!--form-->
			</div> <!-- End bodyTab2 -->
    </div> <!-- End .content-box-content -->
  </div> <!-- End .content-box -->
</div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
