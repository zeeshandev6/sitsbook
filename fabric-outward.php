<?php
	ob_start();
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/items.php');
	include('common/classes/item_category.php');
	include('common/classes/customers.php');
	include('common/classes/branches.php');
	include('common/classes/branch_stock.php');
	include('common/classes/stock_history.php');

	//Permission
  if(!in_array('fabric-outward',$permissionz) && $admin != true){
		errorMessage('You Are Not Allowed To View This Panel!');
	  exit();
  }
  //Permission ---END--

	$objChartOfAccounts     = new ChartOfAccounts();
	$objItems               = new Items();
	$objItemCategory        = new ItemCategory();
	$objCustomer            = new Customers();
	$objUserAccount         = new UserAccounts();
	$objBranches  	   			= new Branches();
	$objBranchStock    			= new BranchStock();
	$objStockHistory        = new StockHistory();
	$objStockHistoryDetail  = new StockHistoryDetail();

	$godown_list 				 = $objBranches->getListByType('G');
	$stock_check 				 = $objConfigs->get_config("STOCK_CHECK");
	$customer_codes 		 = $objChartOfAccounts->getAccountByCatAccCode('010104');
	$supplier_codes 		 = $objChartOfAccounts->getAccountByCatAccCode('040101');

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
		$objItems        = new Items();
		$objBranchStock  = new BranchStock();
		if($branch_id > 0){
			$objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
		}else{
			$objItems->addStockValue($item_id,$quantity,$total_cost);
		}
	}
	function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
		$objItems        = new Items();
		$objBranchStock  = new BranchStock();
		if($branch_id > 0){
			$objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
		}else{
			$objItems->removeStockValue($item_id,$quantity,$total_cost);
		}
	}

	$fabric_inward_row = NULL;
	if(isset($_GET['id'])){
		$id = (int)(mysql_real_escape_string($_GET['id']));
		$fabric_inward_row = $objStockHistory->getDetail($id);
		$fabric_inward_list= $objStockHistoryDetail->getListByHisId($id);
	}

?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" 		type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" 		type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" 	type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" 	 	type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" 		type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" 	type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style media="screen">
		td,th{
			padding: 10px !important;
			font-size: 14px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/fabric.outward.config.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		var  stock_check = 'Y';
		$(document).ready(function(){
			stock_check = '<?php echo $stock_check; ?>';
			$('input[name=date]').datepicker({
				dateFormat:'dd-mm-yy',
				showAnim: 'show',
				changeMonth: true,
				changeYear: true,
				yearRange: '2000:+10'
			});
			$("select").selectpicker();
			$(".quickSubmit input[type=text]").addClass("text-center");
			$("table").change(function(){
				$("#view_button").parent().addClass("text-center");
			});
			$(".reload_item").click(function(){
				$.get('fabric-outward.php',{},function(data){
					$("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
					$("select.itemSelector1").html($(data).find("select.itemSelector1").html()).selectpicker('refresh');
				});
			});
		});
	</script>
</head>
<body>
	<div id="body-wrapper">
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
<?php
			//save and update
			if(isset($_POST['details'])){
				$StockHisId     								= (int)(isset($_GET['id'])?$_GET['id']:0);
				$update         								= '';
				$prevStockHistoryDetail 				= $objStockHistory->getDetail($StockHisId);
				$objStockHistory->account_id    = $_SESSION['classuseid'];
				$objStockHistory->po_no         = mysql_real_escape_string($_POST['po_no']);
				$objStockHistory->date          = date('Y-m-d',strtotime($_POST['date']));
				$objStockHistory->type          = isset($_POST['stage'])?$_POST['stage']:"";
				$objStockHistory->party_name    = mysql_real_escape_string($_POST['party_code']);
				$objStockHistory->gp_no         = mysql_real_escape_string($_POST['gatepass']);
				$objStockHistory->location      = 0;
				$objStockHistory->ref_no        = mysql_real_escape_string($_POST['ref_no']);
				$objStockHistory->through       = mysql_real_escape_string($_POST['through']);
				$objStockHistory->type          = mysql_real_escape_string($_POST['typee']);
				$objStockHistory->lot_no        = mysql_real_escape_string($_POST['lot_no']);
				$objStockHistory->vehicle_no    = mysql_real_escape_string($_POST['vehicle_no']);
				$objStockHistory->vehicle_type  = mysql_real_escape_string($_POST['vehicle_type']);
				$objStockHistory->io_status     = 'O';
				$objStockHistory->notes         = mysql_real_escape_string($_POST['notes']);

				$prev_godown_id 								= (int)($prevStockHistoryDetail['LOCATION']);
				$godown_id      								= $objStockHistory->location;

				if($StockHisId > 0){
					$update = $objStockHistory->update($StockHisId);
					$action = "U";
				}elseif($StockHisId == 0){
					$save = $objStockHistory->save();
					$saveId = mysql_insert_id();
					$StockHisId = $saveId;
					$action = "S";
				}

				if($StockHisId){
					$arrPro_table = json_decode($_POST['details'], true);
					$rows_deleted = (isset($_POST['deleted_rows']))?$_POST['deleted_rows']:array();
					$rowId = 0;

					foreach($rows_deleted as $key => $id){
						$detail  = $objStockHistoryDetail->getRecordDetailsById($id);
						if($detail != NULL){
							$deleted = $objStockHistoryDetail->delete($id);
							$objGodownDetails->removeStock($detail['ITEM'], $prev_godown_id,$detail['QUANTITY']);
						}
					}

					$rowId = 0;
					foreach($arrPro_table as $key => $val){
						//product table details
						$rowId                                  = (int)$arrPro_table[$key]['rowId'];
						$item                                   = $arrPro_table[$key]['itemValue'];
						$stockInHand                            = $arrPro_table[$key]['itemStock_hand'];
						$stockUpdate                            = $arrPro_table[$key]['itemQty_receipt'];
						$itemQty_receiptNew                     = $arrPro_table[$key]['itemQty_receiptNew'];
						$itemCarton                             = $arrPro_table[$key]['itemCarton'];
						$itemQty_carton                         = $arrPro_table[$key]['itemQty_carton'];
						$itemQty_receipt                        = $arrPro_table[$key]['itemQty_receipt'];
						$unit_cost                        			= $arrPro_table[$key]['unit_cost'];
						$total_cost                        			= $arrPro_table[$key]['total_cost'];

						$objStockHistoryDetail->item            = mysql_real_escape_string($item);
						$objStockHistoryDetail->unit            = mysql_real_escape_string($itemCarton);
						$objStockHistoryDetail->qty_per_unit    = mysql_real_escape_string($itemQty_carton);
						$objStockHistoryDetail->quantity        = mysql_real_escape_string($itemQty_receipt);
						$objStockHistoryDetail->unit_cost       = mysql_real_escape_string($unit_cost);
						$objStockHistoryDetail->total_cost      = mysql_real_escape_string($total_cost);

						//update item
						$existRowDetail = $objStockHistoryDetail->getRecordDetailsById($rowId);
						if($existRowDetail != NULL){
							$itemsNo    = $existRowDetail['ITEM'];
							$itemsUnit  = $existRowDetail['UNIT'];
							$qtyPerUnit = $existRowDetail['QTY_PER_UNIT'];
							$receiptQty = $existRowDetail['QUANTITY'];

							if($rowId > 0){

								$prev_cost  = $objItems->getPurchasePrice($itemsNo);
								$total_cost_prev = $prev_cost*$receiptQty;

								$this_cost  		 = $objItems->getPurchasePrice($item);
								$total_cost_this = $this_cost*$objStockHistoryDetail->quantity;

								add_branch_stock($itemsNo, $prev_godown_id,$receiptQty,$total_cost_prev);
								$updated          = $objStockHistoryDetail->update($rowId);
								remove_branch_stock($item, $godown_id,$stockUpdate,$total_cost_this);
							}
						}
						if($rowId == 0){
							if(!isset($saveId)){ $objStockHistoryDetail->historyId  = $StockHisId; }
							if(isset($saveId)) { $objStockHistoryDetail->historyId  = $saveId; }
							$objStockHistoryDetail->save();
							$stockDetailId  = mysql_insert_id();
							if($stockDetailId){

								$this_cost  		 = $objItems->getPurchasePrice($item);
								$total_cost_this = $this_cost*$stockUpdate;

								remove_branch_stock($item, $godown_id,$stockUpdate,$total_cost_this);
							}
						}
					}
				}
				if($action == 'S'){
					exit(header('location: fabric-outward.php?id='.$saveId.'&action=added'));
				}elseif($action == 'U'){
					exit(header('location: fabric-outward.php?id='.$StockHisId.'&action=updated'));
				}
			}
			if(isset($_POST['itemId'])){
				$itemId 				= $_POST['itemId'];
				$rowId  				= $_POST['rowId'];
				$stockDetail    = $objStockHistoryDetail->getRecordDetailsById($rowId);
				$stockDetailArr = mysql_fetch_assoc($stockDetail);
				$itemIdd 				= $stockDetailArr['ITEM'];
				$itemQty 				= $stockDetailArr['QUANTITY'];
				if($itemId = $itemIdd){
					$objItems->removeStock($itemId, $itemQty);
				}
				$itemStock = $objItems->getRecordDetails($itemId);
				echo $objStockHistoryDetail->delete($rowId);
			}
?>
			<div class="content-box-top">
				<div class="content-box-header">
					<p>Inventory Outward Management</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="fabric-outward-list.php?tab=list"><div class="tab">List</div></a>
							<a href="fabric-outward-list.php?tab=search"><div class="tab">Search</div></a>
							<div class="tabSelected">Details</div>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="content-box-content">
					<div id="bodyTab1">
						<div id="form">
							<form method="post" action="" name="add-history">
									<div class="hide">
										<div class="caption">PO No :</div>
										<div class="form-group field" style="width:350px;margin-bottom:0px;">
											<input type="text" class="form-control" name="po_no" id="pon" value="<?php echo $fabric_inward_row['PO_NO']; ?>" style="width: 350px;" >
											<input type="hidden" name="details" value="" />
										</div>
									</div>
									<div class="caption">Lot # :</div>
									<div class="field" style="width:350px">
										<input type="text" name="lot_no" class="form-control" value="<?php echo $fabric_inward_row['LOT_NO']; ?>" />
									</div>
									<div class="caption">Outward Date :</div>
									<div class="field" style="width:150px">
										<input type="text" name="date" id="datepicker" value="<?php echo ($fabric_inward_row==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($fabric_inward_row['INWARD_DATE'])); ?>" class="form-control datepicker" />
									</div>
									<div class="clear"></div>

									<div class="caption">Party Name :</div>
									<div class="form-group field" style="width:350px;margin-bottom:0px;">
										<select class="form-control show-tick" name="party_code" data-live-search="true">
											<option value=""></option>
											<optgroup label="Customers">
<?php
											if(mysql_num_rows($customer_codes)){
												while($row = mysql_fetch_assoc($customer_codes)){
													$select = ($fabric_inward_row['PARTY_CODE']==$row['ACC_CODE'])?"selected":"";
													?><option <?php echo $select ?> value="<?php echo $row['ACC_CODE']; ?>"><?php echo $row['ACC_TITLE']; ?></option><?php
												}
											}
?>
											</optgroup>
											<optgroup label="Suppliers">
<?php
											if(mysql_num_rows($supplier_codes)){
												while($row = mysql_fetch_assoc($supplier_codes)){
													$select = ($fabric_inward_row['PARTY_CODE']==$row['ACC_CODE'])?"selected":"";
													?><option <?php echo $select ?> value="<?php echo $row['ACC_CODE']; ?>"><?php echo $row['ACC_TITLE']; ?></option><?php
												}
											}
?>
											</optgroup>
										</select>
									</div>
									<div class="caption">Reference # :</div>
									<div class="field" style="width:150px">
										<input type="text" name="ref_no" class="form-control" value="<?php echo $fabric_inward_row['REFERENCE_NO']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="caption">Type :</div>
									<div class="field" style="width:350px">
										<select class="typee form-control" name="typee">
											<option value="GL" <?php echo ($fabric_inward_row['TYPEE']=='GL'||$fabric_inward_row['TYPEE']=='')?"selected":""; ?> >General</option>
											<option value="FC" <?php echo ($fabric_inward_row['TYPEE']=='FC')?"selected":""; ?>>Fabric Contract</option>
											<option value="PC" <?php echo ($fabric_inward_row['TYPEE']=='PC')?"selected":""; ?>>Processing Contract</option>
											<option value="PK" <?php echo ($fabric_inward_row['TYPEE']=='PC')?"selected":""; ?>>Packing</option>
											<option value="EC" <?php echo ($fabric_inward_row['TYPEE']=='EC')?"selected":""; ?>>Embroidery Contract</option>
										</select>
									</div>
									<div class="caption">Gate Pass # :</div>
									<div class="field" style="width:150px">
										<input type="text" name="gatepass" class="form-control" value="<?php echo $fabric_inward_row['GPNO']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="caption">Through :</div>
									<div class="field" style="width:350px">
										<input type="text" name="through" class="form-control" value="<?php echo $fabric_inward_row['THROUGH']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="caption">Vehicle Type :</div>
									<div class="field" style="width:350px">
										<input type="text" name="vehicle_type" class="form-control" value="<?php echo $fabric_inward_row['VEHICLE_TYPE']; ?>" />
									</div>
									<div class="clear"></div>

									<div class="caption">Vehicle # :</div>
									<div class="field" style="width:350px">
										<input type="text" name="vehicle_no" class="form-control" value="<?php echo $fabric_inward_row['VEHICLE_NO']; ?>" />
									</div>

									<div class="caption">Notes :</div>
									<div class="field" style="width:420px">
										<input type="text" name="notes" class="form-control" value="<?php echo $fabric_inward_row['NOTES']; ?>" />
									</div>
									<div class="clear"></div>
									<div style="margin-top:0px"></div>
								</div> <!-- End form -->
								<div class="clear"></div>
								<div id="form">
									<div class="caption"></div>
									<div class="field" style="width:950px">
										<table id="pro_table" style="width:100%;">
											<thead>
												<tr>
													<th width="30%" style="font-size:12px;font-weight:normal;text-align:center">
														Item
														<a href="item-details.php" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus"></i></a>
														<a href="#" class="reload_item btn btn-default btn-xs pull-right"><i class="fa fa-refresh"></i></a>
													</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Thaan/Suits</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Length</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
													<th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">UnitCost</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">TotalCost</th>
													<th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Stock</th>
													<th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
												</tr>
											</thead>

											<tbody>
												<tr class="quickSubmit" style="background:none">
													<td>
														<select class="itemSelector show-tick form-control"
																		data-style="btn-default"
																		data-live-search="true" style="border:none">
															 <option selected value=""></option>
<?php
												$itemsCategoryList   = $objItemCategory->getList();
												if(mysql_num_rows($itemsCategoryList)){
														while($ItemCat = mysql_fetch_array($itemsCategoryList)){
					$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
						<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
					if(mysql_num_rows($itemList)){
						while($theItem = mysql_fetch_array($itemList)){
							if($theItem['ACTIVE'] == 'N'){
								continue;
							}
							if($theItem['INV_TYPE'] == 'B'){
								continue;
							}
?>
							<option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
						}
					}
?>
						</optgroup>
<?php
														}
												}
?>
														</select>
													</td>
													<td >
														<input type="text" name="carton" class="numCorton form-control"/>
													</td>
													<td >
														<input type="text" name="qty_carton" class="perCorton form-control"/>
													</td>
													<td >
														<input type="text" name="qty_receipt" readonly  class="qtyReceipt form-control"/>
													</td>
													<td>
														<input type="text" name="unit_cost"  class="unit_cost form-control"/>
													</td>
													<td>
														<input type="text" name="total_cost" readonly  class="total_cost form-control"/>
													</td>
													<td >
														<input type="text"  name="stock_hand" tabindex="-1"  value="" readonly class="form-control" />
														<input type="hidden" class="stock_hand" value="" />
													</td>
													<td >
														<button type="button" style="height:30px;" class="button enter_new_row" name="enter">Enter</button>
													</td>
												</tr>
											</tbody>
											<tbody>
												<?php
												$total_cartons = 0;
												$total_qty 		 = 0;
												$total_cost  	 = 0;
												if(isset($fabric_inward_list)){
													if (mysql_num_rows($fabric_inward_list)){
														while ($stockHistoryArr = mysql_fetch_assoc($fabric_inward_list)){
															if (isset($stockHistoryArr)){

																$itemDetailId = $stockHistoryArr['ITEM'];
																$itemDetail = $objItems->getRecordDetails($itemDetailId);
																$itemId = $itemDetail['ID'];
																$itemName = $itemDetail['NAME'];
																$itemStock = $itemDetail['STOCK_QTY'];

																$total_cartons += $stockHistoryArr['UNIT'];
																$total_qty     += $stockHistoryArr['QUANTITY'];
																$total_cost    += $stockHistoryArr['TOTAL_COST'];
																?>
																<tr id='itemRecord' data-id="<?php echo $stockHistoryArr['ID']; ?>">
																	<td id='item' data-id="<?php echo $itemId; ?>"><?php echo $itemName; ?></td>
																	<td id='carton' 		 class="text-center"><?php echo $stockHistoryArr['UNIT']; ?></td>
																	<td id='qty_carton'  class="text-center"><?php echo $stockHistoryArr['QTY_PER_UNIT']; ?></td>
																	<td id='qty_receipt' class="text-center"><?php echo $stockHistoryArr['QUANTITY']; ?></td>
																	<td id='unit_cost'   class="text-center"><?php echo $stockHistoryArr['UNIT_COST']; ?></td>
																	<td id='total_cost'  class="text-center"><?php echo $stockHistoryArr['TOTAL_COST']; ?></td>
																	<td id='stock_hand'  class="text-center" data-id=""> - - - - - - -</td>
																	<td class="text-center">
																		<a id="view_button" onclick='editMe(this);'><i class="fa fa-pencil"></i></a>
																		<a class="pointer" onclick='deleteMe(this);'><i class="fa fa-times"></i></a>
																	</td>
																	<input type='hidden' id='products_val' value="">
																	<input type='hidden' id='RecordId' value="">
																</tr>
																<?php
															}
														}
													}
												}else{
													?>
													<?php
												}
												?>
											</tbody>
											<tfoot>
												<tr>
													<td>Total</td>
													<td class="text-center cartons_total"><?php echo (float)$total_cartons; ?></td>
													<td class="text-center"> - - - </td>
													<td class="text-center qty_total"><?php echo (float)$total_qty; ?></td>
													<td class="text-center"> - - - </td>
													<td class="text-center costa_total"><?php echo (float)$total_cost; ?></td>
													<td class="text-center" colspan="2"> - - - </td>
												</tr>
											</tfoot>
										</table>
									</div>
									<div class="clear"></div>
									<div class="caption"></div>
									<div class="field">
										<input type="button" name="save" value="<?php if (isset($_GET['id'])) {
											echo "Update";
										} else {
											echo "Save";
										} ?>" class="button" />
											<input type="button" value="New Form" class="button" onclick="window.location.href='fabric-outward.php'";  />
										</div>
										<div class="clear"></div>
									</form>
								</div> <!-- End form -->
							</div> <!-- End #tab1 -->
						</div> <!-- End .content-box-content -->
					</div> <!-- End .content-box -->

					<!-- Delete confirmation popup -->
					<div id="myConfirm" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Confirmation</h4>
								</div>
								<div class="modal-body">
									<p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
									<button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<!-- Loading bar confirmation popup -->
					<div id="myLoading" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background: #72a9d8; color: white;">
									<h4 class="modal-title">Processing...</h4>
								</div>
								<div class="modal-body">
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
										aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
										<span class="sr-only">100% Complete</span>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->


				<div id="selecCustomer" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="background: #72a9d8; color: white;">
								<h4 class="modal-title">Attention!</h4>
							</div>
							<div class="modal-body">
								<p class="text-danger" style="font-weight: bold;">Please Select Customer first.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->



				<!-- Edit popup -->
				<div id="myedit" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header" style="background: #72a9d8; color: white;">
								<h4 class="modal-title">Edit Record</h4>
							</div>
							<div id="form" class="modal-body">
								<form class="editmyform" id="pro_table">
									<input type="hidden" class="form-control" name="rowid" value="" />
									<input type="hidden" class="form-control" name="stock_hand1" value="" />
									<div style="width:450px; margin: auto;">
										<div class="caption"> Item :</div>
										<div class="field">
											<select class="itemSelector1 show-tick form-control"
															data-style="btn-default"
															data-live-search="true" style="border:none">
												 <option selected value=""></option>
<?php
									$itemsCategoryList   = $objItemCategory->getList();
									if(mysql_num_rows($itemsCategoryList)){
											while($ItemCat = mysql_fetch_array($itemsCategoryList)){
		$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
			<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
		if(mysql_num_rows($itemList)){
			while($theItem = mysql_fetch_array($itemList)){
				if($theItem['ACTIVE'] == 'N'){
					continue;
				}
				if($theItem['INV_TYPE'] == 'B'){
					continue;
				}
?>
				<option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
			}
		}
?>
			</optgroup>
<?php
											}
									}
?>
											</select>
										</div>
										<div class="clear"></div>
										<div class="caption">Carton/Cloth :</div>
										<div class="field">
											<input class="form-control" name="carton1" value="" />
										</div>
										<div class="clear"></div>

										<div class="caption">Qty/Length :</div>
										<div class="field">
											<input class="form-control" name="qty_carton1" value="" />
										</div>
										<div class="clear"></div>

										<div class="caption">Qty Receipt :</div>
										<div class="field">
											<input class="form-control" name="qty_receipt1" value="" style="background-color:#F3F3F3;" readonly="readonly" />
										</div>
										<div class="clear"></div>

										<div class="caption">Unit Cost :</div>
										<div class="field">
											<input class="form-control" name="unit_cost1" value=""  />
										</div>
										<div class="clear"></div>

										<div class="caption">Total Cost :</div>
										<div class="field">
											<input class="form-control" name="total_cost1" value=""   readonly />
										</div>
										<div class="clear"></div>


										<div class="clear" style="height:30px"></div>

										<div class="modal-footer">
											<button type="button" class="btn btn-primary btn-sm" id="addrow" data-dismiss="modal">Update</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										</div>

									</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!--body-wrapper-->
			<div id="xfade"></div>
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
