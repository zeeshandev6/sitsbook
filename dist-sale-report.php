<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/customers.php');
	include('common/classes/dist_sale.php');
	include('common/classes/dist_sale_details.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/dist_sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	  			= new ChartOfAccounts();
	$objCustomers 						= new Customers();
	$objDistributorSale 			 	= new DistributorSale();
	$objDistributorSaleDetails      	= new DistributorSaleDetails();
	$objDistributorSaleReturn 			= new DistributorSaleReturn();
	$objDistributorSaleReturnDetails  	= new DistributorSaleReturnDetails();
	$objItems              	  			= new Items();
	$objServices           	  			= new Services();
	$objItemCategory       	  			= new itemCategory();
	$objTaxRates           	  			= new TaxRates();
	$objDepartments        	  			= new Departments();
	$objConfigs 		   	  			= new Configs();

	$suppliersList   = $objAccountCodes->getAccountByCatAccCode('010104');
	$cashAccounts  	 = $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList     = $objTaxRates->getList();

	$brands_list     = $objItems->getBrandNameList();

	$area_list    = $objCustomers->get_area_array();
	$sectors_list = $objCustomers->get_sector_array();


	$currency_type 			 = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   			 = $objConfigs->get_config('USE_CARTONS');

	if(isset($_GET['search'])){

		$objDistributorSale->fromDate 		= ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objDistributorSale->toDate   		= ($_GET['toDate']=='')?"":date('Y-m-d',strtotime($_GET['toDate']));

		$objDistributorSale->user_id		= isset($_GET['salesman'])?$_GET['salesman']:"";
		$objDistributorSale->order_taker 	= isset($_GET['order_taker'])?$_GET['order_taker']:"";


		$objDistributorSale->area 	= isset($_GET['area'])?  mysql_real_escape_string($_GET['area']):"";
		$objDistributorSale->sector = isset($_GET['sector'])?mysql_real_escape_string($_GET['sector']):"";

		$sales_man_name   = $objAccounts->getFullName($objDistributorSale->user_id);
		$order_taker_name = $objAccounts->getFullName($objDistributorSale->order_taker);

		$objDistributorSale->item     		= mysql_real_escape_string($_GET['item']);
		$objDistributorSale->supplierAccCode  = mysql_real_escape_string($_GET['supplier']);
		$objDistributorSale->account_type     = mysql_real_escape_string($_GET['account_type']);
		$repoType  					= isset($_GET['report_type'])?$_GET['report_type']:"S";
		$objDistributorSale->with_tax  		= "";
		$objDistributorSale->cat              = (isset($_GET['item_category']))?mysql_real_escape_string($_GET['item_category']):"";
		$objDistributorSale->brand_name       = (isset($_GET['brand_name']))?mysql_real_escape_string($_GET['brand_name']):"";
		$objDistributorSale->billNum					= '';

		$supplierTypeReport   =  ($objDistributorSale->supplierAccCode == '')?false:true;
		$itemTypeReport       =  ($objDistributorSale->item == '')?false:true;

		//both selected
		if(($objDistributorSale->supplierAccCode != '') && ($objDistributorSale->item != '')){
			$supplierTypeReport = true;
		}

		$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objDistributorSale->supplierAccCode);
		$theItemTitle      = $objItems->getItemTitle($objDistributorSale->item);

		if($supplierTypeReport){
			$titleRepo = $supplierHeadTitle;
		}elseif($itemTypeReport){
			$titleRepo = $theItemTitle;
		}else{
			$titleRepo = '';
		}
		if($objDistributorSale->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objDistributorSale->fromDate = $startThisMonth;
		}
		if(isset($_GET['group_by'])&&$_GET['group_by']=='Y'){
			$saleReport = $objDistributorSale->saleReportGroupByBill();
		}else{
			$saleReport = $objDistributorSale->saleReport($repoType);
		}
	}
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$(".printThis").click(function(){
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports-horizontal.css',
				pageTitle: "Sit Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		<?php if(isset($_GET['group_by'])&&$_GET['group_by']=='n'){ ?>
		var $field = $("td.tax_amnt_tf");
		var $fieldBeacon = $("th.tax_amnt_th");
		var tax_amnt_total = parseFloat($field.text())||0;
		if(tax_amnt_total == 0){
			var $fieldRow = $fieldBeacon.parent();
			var tax_amnt_col_pos = $fieldBeacon.index();
			var tax_amnt_col_pos2 = tax_amnt_col_pos - 1;
			var sub_amnt_col_pos = tax_amnt_col_pos2 - 1;

			$("thead tr").each(function(i,v){
				$(this).find('th').eq(tax_amnt_col_pos).remove();
				$(this).find('th').eq(tax_amnt_col_pos2).remove();
				$(this).find('th').eq(sub_amnt_col_pos).remove();
			});
			$("tbody tr").each(function(i,v){
				$(this).find('td').eq(tax_amnt_col_pos).remove();
				$(this).find('td').eq(tax_amnt_col_pos2).remove();
				$(this).find('td').eq(sub_amnt_col_pos).remove();
			});
			$field.prev().prev().remove();
			$field.prev().remove();
			$field.remove();
		}
		<?php } ?>
		$('select').selectpicker();
		if($(".carton_th").length){
			var colspan = $(".carton_th").prevAll().length;
		}else{
			var colspan = $(".tHeader th").eq(4).prevAll().length;
		}
		$(".total_tf").attr('colspan',colspan);
		$("select[name=group_by]").change(function(){
			var group_by = $(this).find("option:selected").val();
			if(group_by == 'Y'){
				$("div.repo_type_selector").hide();
				$("div.repo_type_selector").find("select option[value='S']").prop("selected",true);
				$("div.repo_type_selector").find("select").selectpicker('refresh');
			}else{
				$("div.repo_type_selector").show();
			}
		});
		$("select[name=group_by]").change();
	});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sales Reports</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="dist-sale-report.php">
							<div class="caption">From Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>

							<div class="caption">To Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>

							<?php
							if($_SESSION['classuseid'] == 1){
								$sales_man_arr = array();
								$users_list = $objAccounts->getActiveList();
								if(mysql_num_rows($users_list)){
									while($user = mysql_fetch_assoc($users_list)){
										if($user['DESIGNATION_TYPE'] == ''){
											//$user['DESIGNATION_TYPE'] = 'S';
											continue;
										}
										if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
											$sales_man_arr[$user['DESIGNATION_TYPE']] = array();
										}
										$sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
									}
								}
								?>
								<div class="caption">OrderTaker</div>
								<div class="field" style="width:270px;position:relative;">
									<select class="user_id" name="order_taker">
										<option value=""></option>
										<?php
										if(isset($sales_man_arr['O'])){
											foreach ($sales_man_arr['O'] as $key => $user){
													?>
													<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
													<?php
											}
										}
										 ?>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption">Salesman</div>
								<div class="field" style="width:270px;position:relative;">
									<select class="user_id" name="salesman">
										<option value=""></option>
										<?php
										if(isset($sales_man_arr['S'])){
											foreach ($sales_man_arr['S'] as $key => $user){
													?>
													<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
													<?php
											}
										}
										 ?>
									</select>
								</div>
								<div class="clear"></div>
								<?php
							}
							?>

							<div class="caption">Customer</div>
							<div class="field" style="width:300px;position:relative;">
								<select class="supplierSelector form-control "
								name="supplier"
								data-style="btn btn-default"
								data-live-search="true" style="border:none" >
								<option selected value=""></option>
								<?php
								if(mysql_num_rows($cashAccounts)){
									while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
										?>
										<option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
										<?php
									}
								}
								?>
								<?php
								if(mysql_num_rows($suppliersList)){
									while($account = mysql_fetch_array($suppliersList)){
										$selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
										?>
										<option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="clear"></div>

						<div class="caption">Category </div>
						<div class="field" style="width:300px;">
							<select class="itemSelector show-tick form-control"
							data-style="btn btn-default"
							data-live-search="true" style="border:none" name="item_category">
							<option selected value=""></option>
							<?php
							$itemsCategoryList  = $objItemCategory->getList();
							if(mysql_num_rows($itemsCategoryList)){
								while($ItemCat = mysql_fetch_array($itemsCategoryList)){
									?>
									<option value="<?php echo $ItemCat['ITEM_CATG_ID']; ?>" ><?php echo $ItemCat['NAME']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="clear"></div>

					<div class="caption">Brands</div>
					<div class="field" style="width:300px;">
						<select class="form-control" name="brand_name">
							<option value=""></option>

							<?php if(mysql_num_rows($brands_list)){ ?>
								<?php while($brand = mysql_fetch_assoc($brands_list)){ ?>
							<option value="<?php echo $brand['COMPANY']; ?>"><?php echo $brand['COMPANY']; ?></option>
							<?php 	} ?>
							<?php } ?>
						</select>
					</div>
					<div class="clear"></div>

					<div class="caption">Item </div>
					<div class="field" style="width:300px;">
						<select class="itemSelector show-tick form-control"
						data-style="btn btn-default"
						data-live-search="true" style="border:none" name="item">
						<option selected value=""></option>
						<?php
						$itemsCategoryList  = $objItemCategory->getList();
						if(mysql_num_rows($itemsCategoryList)){
							while($ItemCat = mysql_fetch_array($itemsCategoryList)){
								$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
								?>
								<optgroup label="<?php echo $ItemCat['NAME']; ?>">
									<?php
									if(mysql_num_rows($itemList)){
										while($theItem = mysql_fetch_array($itemList)){
											if($theItem['ACTIVE'] == 'N'){
												continue;
											}
											if($theItem['INV_TYPE'] == 'B'){
												continue;
											}
											?>
											<option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
											<?php
										}
									}
									?>
								</optgroup>
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="clear"></div>

				<div class="caption"> Area </div>
				<div class="field" style="width:300px;">
					<select class="form-control" name="area">
						<option value=""></option>
						<?php foreach ($area_list as $key => $value) { ?>
							<option value="<?=$value;?>"><?=$value;?></option>
						<?php } ?>
					</select>
				</div><!--field-->
				<div class="clear"></div>

				<div class="caption"> Sector </div>
				<div class="field" style="width:300px;">
					<select class="form-control" name="sector">
						<option value=""></option>
						<?php foreach ($sectors_list as $key => $value) { ?>
							<option value="<?=$value;?>"><?=$value;?></option>
						<?php } ?>
					</select>
				</div><!--field-->
				<div class="clear"></div>

				<div class="caption">Group By Bill</div>
				<div class="field" style="width:300px;">
					<select class="form-control" name="group_by">
						<option value="Y" selected>Yes</option>
						<option value="N">No</option>
					</select>
				</div><!--field-->
				<div class="clear"></div>

				<div class="repo_type_selector">
					<div class="caption">Report Type</div>
					<div class="field" style="width:300px;">
						<select class="form-control" name="report_type">
							<option value="S" selected>Sale</option>
							<option value="R">Sale Returns</option>
						</select>
					</div><!--field-->
					<div class="clear"></div>
				</div>


				<div class="caption">Transaction Type</div>
				<div class="field" style="width:300px;position:relative;">
					<input type="radio" name="account_type"  value="" id="radio0" checked class="css-checkbox" />
					<label for="radio0" class="css-label-radio radGroup1">All </label>
					<input type="radio" name="account_type"  value="010101" id="radio1" class="css-checkbox" />
					<label for="radio1" class="css-label-radio radGroup1"> Cash </label>
					<input type="radio" name="account_type"  value="010104" id="radio2" class="css-checkbox" />
					<label for="radio2" class="css-label-radio radGroup1"> Account </label>
					<input type="radio" name="account_type"  value="010102" id="radio3" class="css-checkbox" />
					<label for="radio3" class="css-label-radio radGroup1"> Card </label>
				</div>
				<div class="clear"></div>

				<div class="caption"></div>
				<div class="field">
					<input type="submit" value="Search" name="search" class="button"/>
				</div>
				<div class="clear"></div>
			</form>
		</div><!--form-->

		<?php
		if(isset($saleReport)&&isset($_GET['group_by'])&&$_GET['group_by']=='N'){
			?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							Sale <?php echo ($repoType == 'R')?"Return":""; ?> Report
							<?php
							if($objDistributorSale->user_id != ''){
								$ozer_name = $objAccounts->getFullName($objDistributorSale->user_id);
								?>
								- <?php echo $ozer_name['FIRST_NAME']." ".$ozer_name['LAST_NAME']; ?>
								<?php
							}else{
								?>
								<?php echo ($titleRepo == '')?"":" - ".$titleRepo; ?>
								<?php
							}
							?>
						</p>
						<span>
							<?php echo ($objDistributorSale->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objDistributorSale->fromDate)); ?>
							<?php echo ($objDistributorSale->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objDistributorSale->toDate)); ?>
						</span>
						<span class="pull-right">Report Generated On: <?php echo date('d-m-Y'); ?></span>
						<div class="clear"></div>
					</div>
					<?php
					$prevBillNo = '';
					if(mysql_num_rows($saleReport)){
						?>
						<table class="tableBreak">
							<thead class="tHeader">
								<tr style="background:#EEE;">
									<th width="6%" style="font-size:12px;text-align:center">Memo#</th>
									<?php
									if($repoType == 'R'){
										?>
										<th width="6%" style="font-size:12px;text-align:center">ReturnDate</th>
										<?php
									}
									?>
									<th width="6%" style="font-size:12px;text-align:center">SaleDate</th>
									<?php
									if(!$supplierTypeReport){
										?>
										<th width="15%" style="font-size:12px;text-align:center">Customer</th>
										<?php
									}
									if(!$itemTypeReport){
										?>
										<th width="15%" style="font-size:12px;text-align:center">Item</th>
										<?php
									}
									?>
										<th width="5%"  class="carton_th" style="font-size:12px;text-align:center">Crtns</th>
										<th width="5%"  style="font-size:12px;text-align:center">Rate</th>
										<th width="5%"  class="dzn_th" style="font-size:12px;text-align:center">Dzns</th>
										<th width="5%"  style="font-size:12px;text-align:center">Rate</th>
										<th width="5%"  style="font-size:12px;text-align:center">Piece</th>
										<th width="5%" style="font-size:12px;text-align:center">Rate</th>
										<th width="5%" style="font-size:12px;text-align:center">Disc /@</th>
										<th width="5%" style="font-size:12px;text-align:center">Amount</th>
										<th width="5%" style="font-size:12px;text-align:center">Total Amt</th>
										<th width="5%" style="font-size:12px;text-align:center">Cost</th>
										<th width="5%" style="font-size:12px;text-align:center">Profit/Loss</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$total_crtn   = 0;
									$total_dzns   = 0;
									$total_qty    = 0;
									$total_price  = 0;
									$total_tax    = 0;
									$total_amount = 0;
									$total_cost   = 0;
									$total_pnl    = 0;
									while($detailRow = mysql_fetch_array($saleReport)){
										$supplierTitle = $objAccountCodes->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);

										$item_id  = $detailRow['ITEM_ID'];
										$itemName = $objItems->getRecordDetails($detailRow['ITEM_ID']);
										$itemName = substr($itemName['NAME'],0,25)."(".$itemName['ITEM_BARCODE'].")";

										$user_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
										$user_full_name = $user_full_name['FIRST_NAME']." ".$user_full_name['LAST_NAME'];

										if($repoType == 'S'){
											$qty_cartn = $objItems->getItemQtyPerCarton($detailRow['ITEM_ID']);
											$qty_carton= $detailRow['CARTONS']*$qty_cartn;
											$qull_cost = $detailRow['COST_PRICE']*($detailRow['QUANTITY']+$qty_carton);
										}else{
											$sale_details = $objDistributorSale->getDetail($detailRow['BILL_ID']);
											$cost_price   = $objDistributorSaleReturnDetails->getHistoryCostPrice($detailRow['ITEM_ID'],$detailRow['BILL_ID']);
											$qty_cartn    = $objItems->getItemQtyPerCarton($detailRow['ITEM_ID']);
											$qty_carton   = $detailRow['CARTONS']*$qty_cartn;
											$qull_cost    = $cost_price*($detailRow['QUANTITY']+$qty_carton);
										}

										$discount_type = ($detailRow['DISCOUNT_TYPE'] == 'P')?"%":$currency_type;

										if($detailRow['BILL_NO'] !== $prevBillNo){
											$thisBillNo = $detailRow['BILL_NO'];
										}else{
											$thisBillNo = '';
										}

										$total_pnl += ($detailRow['SUB_AMOUNT']-$qull_cost);
										?>
										<tr id="recordPanel" class="alt-row">
											<td class="text-center"><?php echo $thisBillNo; ?></td>
											<td class="text-left"><?php echo date('d-m-Y',strtotime($detailRow['SALE_DATE'])); ?></td>
											<?php
											if($repoType == 'R'){
												?>
												<td class="text-center"><?php echo date('d-m-Y',strtotime($sale_details['SALE_DATE'])); ?></td>
												<?php
											}
											?>
											<?php
											if(!$supplierTypeReport){
												?>
												<td class="text-left"><?php echo substr($supplierTitle, 0,16); ?></td>
												<?php
											}
											if(!$itemTypeReport){
												?>
												<td class="text-left"><?php echo $itemName; ?></td>
												<?php
											}
											?>
												<?php if($use_cartons == 'Y'){ ?>
												<td class="text-center"><?php echo $detailRow['CARTONS']; ?></td>
												<td class="text-center"><?php echo $detailRow['RATE_CARTON']; ?></td>
												<?php } ?>
												<td class="text-center"><?php echo $detailRow['DOZENS']; ?></td>
												<td class="text-center"><?php echo $detailRow['DOZEN_PRICE']; ?></td>
												<td class="text-center"><?php echo $detailRow['QUANTITY']; ?></td>
												<td class="text-center"><?php echo number_format($detailRow['UNIT_PRICE'],2); ?></td>
												<td class="text-center">
													<?php echo number_format($detailRow['SALE_DISCOUNT'],2); ?>
													<?php echo ($detailRow['DISCOUNT_TYPE'] == 'P')?"%":""; ?>
												</td>
												<td class="text-right"><?php echo number_format($detailRow['SUB_AMOUNT'],2); ?></td>
												<td class="text-right"><?php echo number_format($detailRow['TOTAL_AMOUNT'],2); ?></td>
												<td class="text-right"><?php echo number_format(($qull_cost),2); ?></td>
												<td class="text-right"><?php echo number_format(($detailRow['SUB_AMOUNT']-$qull_cost),2); ?></td>
											</tr>
											<?php
											$total_crtn  += $detailRow['CARTONS'];
											$total_dzns  += $detailRow['DOZENS'];
											$total_qty   += $detailRow['QUANTITY'];
											$total_price += $detailRow['SUB_AMOUNT'];
											$total_tax += $detailRow['TAX_AMOUNT'];
											$total_amount   += $detailRow['TOTAL_AMOUNT'];
											$prevBillNo = $detailRow['BILL_NO'];
											$total_cost += ($qull_cost);
										}
										?>

									</tbody>
									<?php
								}//end if
								if(mysql_num_rows($saleReport)){
									$total_price  = number_format($total_price,2);
									$total_tax    = number_format($total_tax,2);
									$total_amount = number_format($total_amount,2);
									$total_cost   = number_format($total_cost,2);

									$columnSkip = 3;
									if($itemTypeReport || $supplierTypeReport){
										$columnSkip = 4;
									}
									?>
									<tfoot class="tableFooter">
										<tr>
											<td style="text-align:right;" colspan="" class="total_tf">Total:</td>
											<?php if($use_cartons == 'Y'){ ?>
												<td style="text-align:center;"> <?php echo (isset($total_crtn))?$total_crtn:"0"; ?> </td>
												<td style="text-align:center;"> - - - </td>
												<?php } ?>
												<td style="text-align:center;"> <?php echo (isset($total_dzns))?$total_dzns:"0"; ?> </td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> - - - </td>
												<td class="text-right sub_amnt_tf"> <?php echo (isset($total_price))?$total_price:"0"; ?> </td>
												<td class="text-center tax_rate_tf"> - - - </td>
												<td class="text-right"> <?php echo (isset($total_cost))?$total_cost:"0"; ?> </td>
												<td class="text-right"> <?php echo (isset($total_pnl))?number_format($total_pnl,2):"0"; ?> </td>
											</tr>
										</tfoot>
										<?php
									}
									?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					}elseif(isset($saleReport)&&isset($_GET['group_by'])&&$_GET['group_by']=='Y'){
?>
						<div id="form">
						<span style="float:right;"><button class="button printThis">Print</button></span>
						<div class="clear"></div>
							<div id="bodyTab" class="printTable" style="margin: 0 auto;">
								<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
									<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
										Sale <?php echo ($repoType == 'R')?"Return":""; ?> Report
										<?php
										if($objDistributorSale->user_id != ''){
											$ozer_name = $objAccounts->getFullName($objDistributorSale->user_id);
											?>
											- <?php echo $ozer_name['FIRST_NAME']." ".$ozer_name['LAST_NAME']; ?>
											<?php
										}else{
											?>
											<?php echo ($titleRepo == '')?"":" - ".$titleRepo; ?>
											<?php
										}
										?>
									</p>
									<p style="font-size:16px;text-align:left;padding: 0px;">
										<?php echo ($objDistributorSale->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objDistributorSale->fromDate)); ?>
										<?php echo ($objDistributorSale->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objDistributorSale->toDate)); ?>
									</p>
									<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
									<div class="clear"></div>
								</div>
								<?php
								$prevBillNo = '';
								if(mysql_num_rows($saleReport)){
									?>
									<table class="tableBreak">
										<thead class="tHeader">
											<tr style="background:#EEE;">
													<th width="5%" style="font-size:12px;text-align:center">Inv#</th>
													<th width="8%" style="font-size:12px;text-align:center">Date</th>
													<th width="20%" style="font-size:12px;text-align:center">Customer</th>
													<th width="30%" style="font-size:12px;text-align:center">Item</th>
													<th width="5%"  class="carton_th" style="font-size:12px;text-align:center">Crtns</th>
													<th width="5%"  style="font-size:12px;text-align:center">Dzns</th>
													<th width="5%"  style="font-size:12px;text-align:center">Qty</th>
													<th width="8%" style="font-size:12px;text-align:center">Amount</th>
													<th width="5%" style="font-size:12px;text-align:center">Trade Off</th>
													<th width="8%" style="font-size:12px;text-align:center">Net Amnt</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$total_crtn   = 0;
												$total_dzn    = 0;
												$total_qty    = 0;
												$total_price  = 0;
												$total_tax    = 0;
												$total_amount = 0;
												$trade_off_total = 0;
												$net_total_amount = 0;
												$total_cost   = 0;
												$total_pnl    = 0;

												while($detailRow = mysql_fetch_array($saleReport)){
													$supplierTitle = $objAccountCodes->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);
													$items_list    = $objDistributorSale->getSaleDetailArrayItemOnly($detailRow['SALE_ID']);
													$item_comm_sep = '';
													foreach($items_list as $key => $value){
														$itemName = $objItems->getRecordDetails($value);
														$itemName = substr($itemName['NAME'],0,25);
														if($key > 0){
															$item_comm_sep  .= ', ';
														}
														$item_comm_sep  .= $itemName;
													}

													if($detailRow['BILL_NO'] !== $prevBillNo){
														$thisBillNo = $detailRow['BILL_NO'];
													}else{
														$thisBillNo = '';
													}
													?>
													<tr id="recordPanel" class="alt-row">
														<td class="text-center"><?php echo $thisBillNo; ?></td>
														<?php
														if($repoType == 'R'){
															$returnDate = $objDistributorSale->getDateByBillAndCode($detailRow['BILL_NO'],$detailRow['CUST_ACC_CODE']);
															?>
															<td class="text-center"><?php echo $returnDate; ?></td>
															<?php
														}
														?>

														<td class="text-center"><?php echo date('d-m-y',strtotime($detailRow['SALE_DATE'])); ?></td>
														<td class="text-left"><?php echo substr($supplierTitle, 0,16); ?></td>
														<td class="text-left"><?php echo $item_comm_sep; ?></td>
														<td class="text-center"><?php echo $detailRow['CARTONS']; ?></td>
															<td class="text-center"><?php echo $detailRow['DOZENS']; ?></td>
															<td class="text-center"><?php echo $detailRow['QUANTITY']; ?></td>
															<td class="text-right"><?php echo  number_format($detailRow['TOTAL_AMOUNT'],2); ?></td>
															<td class="text-right"><?php echo  number_format($detailRow['TRADE_OFFER'],2); ?></td>
															<td class="text-right"><?php echo  number_format($detailRow['TOTAL_AMOUNT']-$detailRow['TRADE_OFFER'],2); ?></td>
														</tr>
														<?php
														$total_crtn  += $detailRow['CARTONS'];
														$total_dzn   += $detailRow['DOZENS'];
														$total_qty   += $detailRow['QUANTITY'];
														$total_price += $detailRow['SUB_AMOUNT'];
														$total_tax += $detailRow['TAX_AMOUNT'];
														$total_amount   += $detailRow['TOTAL_AMOUNT'];
														$prevBillNo = $detailRow['BILL_NO'];
														$trade_off_total += $detailRow['TRADE_OFFER'];
														$total_pnl += ($detailRow['TOTAL_AMOUNT']-$detailRow['TRADE_OFFER']);
													}
													?>

												</tbody>
												<?php
											}//end if
											if(mysql_num_rows($saleReport)){
												$total_price  = number_format($total_price,2);
												$total_tax    = number_format($total_tax,2);
												$total_amount = number_format($total_amount,2);
												$total_cost   = number_format($total_cost,2);

												$columnSkip = 3;
												if($itemTypeReport || $supplierTypeReport){
													$columnSkip = 4;
												}
												?>
													<tfoot class="tableFooter">
														<tr>
															<td style="text-align:right;" colspan="" class="total_tf">Total:</td>
															<td style="text-align:center;"> <?php echo (isset($total_crtn))?$total_crtn:"0"; ?> </td>
															<td class="text-center"> <?php echo (isset($total_dzn))?$total_dzn:"0"; ?> </td>
															<td class="text-center"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
															<td class="text-right"> <?php echo (isset($total_amount))?$total_amount:"0"; ?> </td>
															<td class="text-right"> <?php echo (isset($trade_off_total))?$trade_off_total:"0"; ?> </td>
															<td class="text-right"> <?php echo (isset($total_pnl))?$total_pnl:"0"; ?> </td>
														</tr>
													</tfoot>
													<?php
												}
												?>
											</table>
											<div class="clear"></div>
										</div> <!--End bodyTab-->
									</div> <!--End #form-->
<?php
					}
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
if(isset($reportType)&&$reportType=='generic'){
	?>
	tab('1', '1', '2')
	<?php
}
?>
<?php
if(isset($reportType)&&$reportType=='specific'){
	?>
	tab('2', '1', '2')
	<?php
}
?>
</script>
