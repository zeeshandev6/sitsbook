<?php
  
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/accounts.php';
	
	$objJournalVoucher =new JournalVoucher();
	$objAccounts       =new ChartOfAccounts();

	//Permission
	if(!in_array('journal-voucher',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--
	$accountsList=$objJournalVoucher->getAccountsList();
	$jvNum=$objJournalVoucher->genJvNumber();

	$id=0;

    if(isset($_POST['nar'])){
        $voucher_detail_id = mysql_real_escape_string($_POST['id']);
        $narration         = mysql_real_escape_string($_POST['nar']);
        $objJournalVoucher->update_narration($voucher_detail_id,$narration);
        exit();
    }

	if(isset($_GET['id'])){
		$id=$_GET['id'];
		$jVoucher=mysql_fetch_array($objJournalVoucher->getRecordDetails($id));
		$jVoucherDetailList=$objJournalVoucher->getVoucherDetailList($id);
	}
?>
<!DOCTYPE html 
   >

   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />	
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" rel="stylesheet">

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/ledger.configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
        	var refresh_accounts=function(){
        		$.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(html){
        			var options=$(html).find('select.accCodeSelector').html();
        			$("select.accCodeSelector").html(options);
        			$("select.accCodeSelector").selectpicker("refresh");
        		});
        	};
            $(function(){
                $("button.edit").click(function(){
                    var id = parseInt($(this).parent().parent().attr("row-id"))||0;
                    save_narration(id,$(this));
                });
            });
            var save_narration = function(id,elm){
                var nar = $(elm).parent().parent().find("input.narration").val();
                if(id){
                    $(elm).text("Wait..").prop("disabled",true);
                    $(elm).prop("disabled",true);
                    $.post("edit-voucher.php",{id:id,nar:nar},function(data){
                        if(data == ''){
                            $(elm).text("Save").prop("disabled",false);
                            $(elm).prop("disabled",false);
                        }
                    });
                }
            }
        </script>
   </head>
   <body>
      <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php"); ?>    
        </div> <!-- End #sidebar -->
      	<div id="bodyWrapper">
        	<div class="content-box-top" style="overflow:visible;">
            	<div class="summery_body">
               		<div class="content-box-header">
                  		<p>Voucher Narration Modification</p>
                        <div class="clear"></div>
               		</div><!-- End .content-box-header -->
              		<div id="bodyTab1">
                      <div id="form">
                            <div class="caption" style="width:100px;">JV#</div>
                            <div class="field_1">
                               <input class="form-control" name="jvNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['VOUCHER_NO']:$jvNum; ?>" readonly />
                            </div>
                            <div class="caption" style="width:100px;">Date</div>
                            <div class="field_1">
                               <input class="form-control" type="text" name="jvDate" id="datepicker" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['VOUCHER_DATE'])):date('d-m-Y'); ?>" readonly />
                            </div>
                            <div class="caption" style="width:100px;display: none;">Reference</div>
                            <div class="field_1" style="width:150px;">
                               <input class="form-control" style="display: none;" type="text" name="jvReference" value="<?php echo (isset($jVoucher))?$jVoucher['REFERENCE']:""; ?>" />
                            </div>
                            <div style="clear:both;height:10px;"></div>
                            <div class="panel panel-default"  style="width:80%;margin:0px auto;">
                                  <table cellspacing="0" style="width:100%;">
                                     <thead>
                                        <tr>
                                           <th width="15%" class="text-center" >GL/Account Code</th>
                                           <th width="15%" class="text-center" >A/C Title</th>
                                           <th width="40%" class="text-center" >Narrtion</th>
                                           <th width="10%" class="text-center" >Debit</th>
                                           <th width="10%" class="text-center" >Credit</th>
                                           <th width="10%" class="text-center" >Action</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                      <tr class="transactions" style="display:none;"></tr>
            <?php
                        $debit_total=0;
                        $credit_total= 0;
                        if(isset($jVoucherDetailList) && mysql_num_rows($jVoucherDetailList)){
                          while($detailRow=mysql_fetch_array($jVoucherDetailList)){
            ?>
                                        <tr class="transactions amountRow"  row-id="<?php echo $detailRow['VOUCH_DETAIL_ID'] ?>">
                                          <td style="text-align:right;"     class='accCode'><?php echo $detailRow['ACCOUNT_CODE'] ?></td>
                                            <td style="text-align:left;"    class='accTitle'><?php echo $detailRow['ACCOUNT_TITLE'] ?></td>
                                            <td style="text-align:left;"    ><input class="form-control narration" value="<?php echo htmlspecialchars($detailRow['NARRATION'],ENT_QUOTES,'UTF-8'); ?>" /></td>
                                            <td style="text-align:center;"  class="debitColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Dr')?number_format($detailRow['AMOUNT'],2):""; ?></td>
                                            <td style="text-align:center;"  class="creditColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Cr')?number_format($detailRow['AMOUNT'],2):""; ?></td>
                                            <td style="text-align:center;"> <button type="text" class="btn btn-xs edit" id="view_button">Save</button> </td>
                                        </tr>
            <?php
                            $debit_total += ($detailRow['TRANSACTION_TYPE'] == 'Dr')?$detailRow['AMOUNT']:0;
                            $credit_total+= ($detailRow['TRANSACTION_TYPE'] == 'Cr')?$detailRow['AMOUNT']:0;
                          }
                        }
            ?>
                          <tr style="background-color:#F8F8F8;height: 30px;">
                                          <td colspan="3" style="text-align:center;"> Total/Difference </td>
                                            <td class="debitTotal" style="text-align:center;color:#042377;" title="Debit"><?php echo number_format($debit_total,2); ?></td>
                                            <td class="creditTotal" style="text-align:center;color:#042377;" title="Credit"><?php echo number_format($credit_total,2); ?></td>
                                            <td class="drCrDiffer" style="text-align:center;color:#042377;" title="Diffrence"></td>
                                        </tr>
                                     </tbody>
                                  </table>
                                  <div class="underTheTable pull-right">
                                      <input type="hidden" value="<?php echo $id; ?>" class="jVoucher_id" />
                                      <?php
                                        if(isset($_GET['id'])){
                                      ?>
                                        <a class="button pull-right" target="_blank" href="voucher-print.php?id=<?php echo $id; ?> "><i class="fa fa-print"></i> Print </a>
                                      <?php
                            }else{
                          ?>
                            <button class="button save_voucher"><?php echo (isset($_GET['id']))?"Update":"Save"; ?></button>
                          <?php
                            }
                                      ?>
                                        <input type="button" class="button" onclick="window.location.href='create-voucher.php';" value="New Voucher" >
                                  </div>
                                  <div class="clear"></div>
                        </div><!--content-box-content-->
                      </div><!--form-->
                      <div style="clear:both;"></div>
               		</div>
            	</div>     <!-- End summer -->
         	</div>   <!-- End .content-box-top -->
	</div>  <!--body-wrapper-->
</div>  <!--body-wrapper-->
<div id="fade"></div>
<div id="xfade"></div>
<div id="popUpForm"></div><!--popUpForm-->
</body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
	$(window).load(function(){
    $('.accCodeSelector').selectpicker();
    $("div.accCodeSelector button").focus();
    $(".narration").setFocusTo(".debit");
    $(".debit").setFocusToIfVal(".credit");
    $(".narration").escFocusTo("button.dropdown-toggle");
    $(".debit").escFocusTo(".narration");
    $(".credit").escFocusTo(".debit");
    $(".debit").keyup(function(e){
    	if(e.keyCode==13){
    		$(this).quickSubmitDrCr();
    	}
    });
    $(".credit").keyup(function(e){
    	if(e.keyCode==13){
    		$(this).quickSubmitDrCr();
    	}
    });
    $(".cdr_btn").on('click',function(e){
      $(this).quickSubmitDrCr();
    });
    $("select.accCodeSelector").change(function(){
      if($("select.accCodeSelector option:selected").val() != 0){
        var account_code=$("select.accCodeSelector option:selected").val();
        var account_title= $("select.accCodeSelector option:selected").text();
        getAccountBalance(account_code);
        $(".insertAccTitle").fadeIn();
        $(".narration").focus();
      }
    });
    $(".save_voucher").click(function(){
    	$(this).saveJournal();
    });
    $("#datepicker").setFocusTo("div.accCodeSelector button");
    $(document).showDifference();
    $(".deleteVoucher").click(function(){
    	$(this).deleteVoucher();
    });
    $(window).keyup(function(e){
        if(e.altKey == true&&e.keyCode == 83){
            e.preventDefault();
            $("#form").click();
            $(".save_voucher").click();
            return false;
        }
    });
  });
</script>