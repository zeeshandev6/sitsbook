<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_outward.php');
	include('common/classes/ledger_report.php');
	include('common/classes/cash_management.php');
	include('common/classes/company_details.php');

	//Permission
	if( (!in_array('cheques-payments-report',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts = new ChartOfAccounts();
	$objJournalVoucher  = new JournalVoucher();
	$objOutward   	 		= new outward();
	$objLedgerReport    = new ledgerReport();
	$objCashManagement  = new CashManagement();
	$objCompanyDetails 	= new CompanyDetails();

	$customersList       = $objChartOfAccounts->getLevelFourList();

	$fromDate   = date("01-m-Y");
	$toDate   	= date("d-m-Y");
	$accCode   	= '';

	if(isset($_POST['fromDate'])){
		$fromDate 	= isset($_POST['fromDate'])?date('Y-m-d',strtotime($_POST['fromDate'])):"";
		$toDate 		= isset($_POST['toDate'])?date('Y-m-d',strtotime($_POST['toDate'])):"";
		$accCode 		= (isset($_POST['customer_code']))?mysql_real_escape_string($_POST['customer_code']):"";
		$status 		= (isset($_POST['status']))?mysql_real_escape_string($_POST['status']):"";
		$type   		= 'P';
		$cheques_report = $objCashManagement->search($fromDate,$toDate,$accCode,$status,$type);
	}
	$status_array 		 = array();
	$status_array['O'] = "Cheque in Hand";
	$status_array['R'] = "Dishonour";
	$status_array['C'] = "Pass";
	$status_array['P'] = "Post Dated";
	$status_array['I'] = "Issued";
	$status_array['D'] = "Deposite";
	$status_array['X'] = "Cancelled";

	$company_profile = $objCompanyDetails->getActiveProfile();
?>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />

		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Cheques Payable Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <form method="post" action="">
													<div class="caption">Account:</div>
													<div class = "field_1" style="width:230px;">
														<select class="form-control" name="customer_code" data-live-search="true">
															<option value=""></option>
<?php
																if(mysql_num_rows($customersList)){
																		while($account = mysql_fetch_array($customersList)){
																			$selected = ($accCode == $account['ACC_CODE'])?"selected":"";
?>
																			 <option <?php echo $selected; ?> data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" ><?php echo $account['ACC_TITLE']; ?></option>
<?php
																		}
																}
?>
														</select>
													</div>
													<div class="clear"></div>

													<div class="caption">Cheque Status:</div>
													<div class = "field_1" style="width:230px;">
														<select name="status" class="form-control">
															<option value="">All</option>
															<option value="O">Cheque in Hand</option>
															<option value="R">Dishonour</option>
															<option value="C">Pass</option>
															<option value="P">Post Dated</option>
															<option value="I">Issued</option>
															<option value="D">Deposite</option>
														</select>
													</div>
													<div class="clear"></div>

													<div class="caption">From Date:</div>
													<div class = "field_1">
														 <input class="form-control datepicker" name="fromDate" type="text" value="<?php echo date("d-m-Y",strtotime($fromDate)); ?>" style="width:230px;" />
													</div>
													<div class="clear"></div>

													<div class="caption">To Date:</div>
													<div class = "field_1">
														 <input class="form-control datepicker" name="toDate" type="text" value="<?php echo date("d-m-Y",strtotime($toDate)); ?>" style="width:230px;" />
													</div>
													<div class="clear"></div>

                          <div class="caption"></div>
                          <div class="field">
                              <input type="submit" value="Generate" name="search" class="btn btn-primary"/>
                          </div>
                          <div class="clear"></div
                        </form>
					</div><!--form-->
                	<div style="clear:both"></div>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
<?php
	if(isset($cheques_report)){
?>
            <div class="content-box-content" id="PrintMe" style="padding: 20px;">
            	<span style="float:right;"><button class="btn btn-primary printThis" type="button">Print</button></span>
            	<div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    <div style="text-align:left;margin-bottom:20px;height: 50px;" class="pageHeader">
											<p style="font-size:20px;padding:0px;margin: 0px;">
												<em><?php echo $company_profile['NAME']; ?></em>
												<hr />
												<em>Cheques Payable Report</em>
												<span class="pull-right">Date : <?php echo date('d-m-Y'); ?></span>
											</p>
                    </div>
                    <div class="clear"></div>

										<table cellspacing = "0" align = "center" style="width:100%;">
                     <thead>
                        <tr>
                           <th width="10%" style="text-align:center" class="bg-color"> Entry Date </th>
                           <th width="15%" style="text-align:center" class="bg-color"> Account Title </th>
                           <th width="15%" style="text-align:center" class="bg-color"> Bank Name </th>
                           <th width="10%" style="text-align:center" class="bg-color"> Cheque# </th>
                           <th width="10%" style="text-align:center" class="bg-color"> Cheque Date </th>
                           <th width="10%" style="text-align:center" class="bg-color"> Amount </th>
                           <th width="10%" style="text-align:center" class="bg-color"> Status </th>
													 <th width="10%" style="text-align:center" class="bg-color"> Chq. Posses </th>
                           <th width="10%" style="text-align:center" class="bg-color"> Settlement Date </th>
                        </tr>
                     </thead>

                     <tbody>
                        <tr class="transactions" style="display:none;"></tr>
<?php
                    if(mysql_num_rows($cheques_report)){
											$totalAmount = 0;
                    	while($row = mysql_fetch_array($cheques_report)){
?>
                        <tr class="transactions">
                            <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['TODAY'])); ?></td>
                            <td style="text-align:left;"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['PARTY_ACC_CODE']); ?></td>
                            <td style="text-align:left;"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['BANK_ACC_CODE']); ?></td>
                             <td style="text-align:left;"><?php echo $row['CHEQUE_NO']; ?></td>
                            <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['CHEQUE_DATE'])); ?></td>
                            <td style="text-align:right;"><?php echo $row['AMOUNT']; ?></td>
                            <td style="text-align:center;" class="statusTd<?php echo $row['ID']; ?>">
															<?php echo (isset($status_array[$row['STATUS']]))?$status_array[$row['STATUS']]:"Unknown"; ?>
                            </td>
														<td style="text-align:left;"><?php echo $row['MEMO']; ?></td>
                            <td style="text-align:center;" class="clearanceDate<?php echo $row['ID']; ?>">
                              <?php echo ($row['STATUS_DATE']=='0000-00-00'||$row['STATUS_DATE']=='1970-01-01')?"":date('d-m-Y',strtotime($row['STATUS_DATE'])); ?>
                            </td>
                        </tr>
                    <?php
						$totalAmount += $row['AMOUNT'];
                    	}
                    }
                    ?>
                        <tr style="background-color:#F8F8F8;height: 30px;">
                            <td colspan="5" style="text-align:right;"> Total: </td>
                            <td class="creditTotal" style="text-align:right;color:#BD0A0D;"><?php echo isset($totalAmount)?number_format($totalAmount,2):"0";; ?></td>
                            <td style="text-align:center;" colspan="3"> - - - </td>
                        </tr>
                     </tbody>
                    </table>
                </div> <!--End bodyTab-->
                <div style="clear:both"></div>
            </div> <!-- End .content-box-content -->
<?php
	}
?>
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script>
	$(window).load(function(){
		$("select").selectpicker();
		//calculate Totals
		$("td.thanColumn").sumColumn("td.thanTotal");
		$("td.thanLengthColumn").sumColumn("td.thanLengthTotal");

		$(".printThis").click(function(){

			var MaxHeight = 1200;
			var RunningHeight = 0;
			var PageNo = 1;
			var MaxHeight_after = 0;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight_after = 1100;
				}else{
					MaxHeight_after = 1200;
				}
				if (RunningHeight + $(this).height() > MaxHeight_after) {
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate    = $(".repoDate").text();
			//remove previous thead/tfoot/ReportDate
			$(".tHeader").remove();
			$(".repoDate").remove();
			$(".tableFooter").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);

			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();
			$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				   loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Unique Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
		});
		$.fn.stDigits = function(){
			return this.each(function(){
				$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			})
		};
		$("td.hasComma").stDigits();
	});
</script>
