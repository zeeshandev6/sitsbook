<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/issue_items.php');
  include('common/classes/issue_items_details.php');
	include('common/classes/sheds.php');
	include('common/classes/machines.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('emb-stocks-issue-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts  = new ChartOfAccounts();
	$objIssueItems       = new IssueItems();
	$objIssueItemDetails = new IssueItemDetails();
	$objShed    	 	 		 = new Sheds();
	$objMachines    	 	 = new Machines();
	$objItems 		 	 		 = new items();
	$objItemCategory 	 	 = new itemCategory();

	$shedList      	 	 = $objShed->getList();
	$machine_list        = $objMachines->getList();
	$itemsCategoryList   = $objItemCategory->getList();

	$firstDayOfThisMonth = date('01-m-Y');

	if(isset($_POST['report'])){
		$objIssueItems->from_date   = ($_POST['fromDate']=="")?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$objIssueItems->to_date 	= ($_POST['toDate']=="")?"":date('Y-m-d',strtotime($_POST['toDate']));
		if($objIssueItems->from_date==''){
			$objIssueItems->from_date = date('01-01-Y');
		}
		$objIssueItems->shed_id 	 = mysql_real_escape_string($_POST['shedId']);
		$objIssueItems->machine_id = mysql_real_escape_string($_POST['machine_id']);
		$objIssueItems->item_id  	 = mysql_real_escape_string($_POST['item']);

		$inventory_issue_items = $objIssueItems->getReport();
		$report = true;
	}
?>
<!DOCTYPE html   >

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<style media="screen">
			table.input_table th,table.input_table td{
				padding: 10px !important;
			}
			tr.quickSubmit td{
				padding: 1px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.lot.register.config.js"></script>
    <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(function(){
			$('select').selectpicker();
			$(".printThis").click(function(){
				var MaxHeight = 914;
				var RunningHeight = 0;
				var PageNo = 1;
				var MaxHeight_after = 0;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if(PageNo == 1){
						MaxHeight = 800;
					}else{
						MaxHeight = 900;
					}
					if (RunningHeight + $(this).height() > MaxHeight) {
						RunningHeight = 0;
						PageNo += 1;
					}
					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate    = $(".repoDate").text();
				//remove previous thead/tfoot/ReportDate
				$(".tHeader").remove();
				$(".repoDate").remove();
				$(".tableFooter").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);

				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Emque Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			<?php if(isset($_GET['error'])){ ?>
				$("button.dropdown-toggle, input[name='fromDate'],input[name='toDate'] ").attr("title","Required!");
				$("button.dropdown-toggle,.datepicker ").tooltipster({
				   animation : 'grow',
				   position  : 'right',
				   theme 	 : 'tooltipster-red',
				   delay 	 : 100,
				   maxWidth  : '250',
				   trigger 	 : 'custom'
				});
				setTimeout(function(){
					$("button.dropdown-toggle, input[name='fromDate'],input[name='toDate'] ").tooltipster('show');
				},500);
			<?php } ?>
		});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Inventory Issue</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="stocks-issue-report.php">

						<div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="<?php echo (isset($objIssueItems->from_date))?date('d-m-Y',strtotime($objIssueItems->from_date)):$firstDayOfThisMonth; ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo (isset($objIssueItems->to_date))?date('d-m-Y',strtotime($objIssueItems->to_date)):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">Shed No.</div>
                        <div class="field" style="width:250px;">
                            <select style="width:150px; height:28px;"
                             	class="shedId form-control show-tick"
                            		name="shedId">
                                    <option value=""></option>
<?php
							if(mysql_num_rows($shedList)){
								while($shedRow = mysql_fetch_array($shedList)){
?>
                                <option value="<?php echo $shedRow['SHED_ID']; ?>" ><?php echo $shedRow['SHED_TITLE']; ?></option>
<?php
								}
							}
?>
                            </select>
                        </div>
                        <div class="clear"></div>


												<div class="caption">Machine</div>
												<div class="field">
													<select name="machine_id" id="" class="machine_id form-control">
															<option value=""></option>
															<?php
																	if(mysql_num_rows($machine_list)){
																			while($machine = mysql_fetch_assoc($machine_list)){
																					?>
																					<option value="<?php echo $machine['ID']; ?>" ><?php echo $machine['NAME']; ?></option>
																					<?php
																			}
																	}
															?>
													</select>
												</div>

                        <div class="caption">Item</div>
                        <div class="field" style="width:250px;">
                            <select style="width:150px; height:28px;"
                             	class="item form-control show-tick" name="item">
                                <option selected value=""></option>
<?php
                              if(mysql_num_rows($itemsCategoryList)){
                                  while($ItemCat = mysql_fetch_array($itemsCategoryList)){
								$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
									<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
								if(mysql_num_rows($itemList)){
									while($theItem = mysql_fetch_array($itemList)){
										if($theItem['ACTIVE'] == 'N'){
											continue;
										}
										if($theItem['INV_TYPE'] == 'B'){
											continue;
										}
?>
                                                 <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
									}
								}
?>
									</optgroup>
<?php
                                  }
                              }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="report" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
<?php
			if(isset($report)){
?>
        	<div class="content-box-content">
<?php
	$columns = 0;
	if(mysql_num_rows($inventory_issue_items)){
?>
            <span style="float:right;margin-right: 10px;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printTable" style="padding: 10px;">
                	<div class="clear"></div>
                	<div style="text-align:center;margin-bottom:0px;" class="pageHeader">
                        <p style="font-size:26px;text-align:left;padding:0px;">Inventory Issue Report</p>
                        <p style="text-align:left;padding:0px;float:left;" >From Date : <b><?php echo date('d-m-Y',strtotime($objIssueItems->from_date)); ?></b> To Date : <b><?php echo date('d-m-Y',strtotime($objIssueItems->to_date)); ?></b></p>
                        <p class="repoDate" style="float: right;padding: 0px;">Report generated On: <?php echo date('d-m-Y'); ?></p>
                    </div>
                    <table class="tableBreak">
                        <thead class="tHeader">
                            <tr style="background:#EEE;">
								<th width="10%" class="text-center">IssueDate</th>
								<th width="15%" class="text-center">Shed</th>
								<th width="10%" class="text-center">Machine</th>
								<th width="15%" class="text-center">Item</th>
                                <th width="10%" class="text-center">Quantity</th>
                                <th width="10%" class="text-center">Unit.Cost</th>
                                <th width="10%" class="text-center">Total.Cost</th>
                            </tr>
                        </thead>
                        <tbody style="page-break-inside:avoid;">
<?php
			$quantity     = 0;
			$cost_price   = 0;
			$total_cost   = 0;
			while($report_row = mysql_fetch_array($inventory_issue_items)){
?>
                            <tr id="recordPanel" class="alt-row">
								<td class="text-center"><?php echo date("d-m-Y",strtotime($report_row['ISSUE_DATE'])); ?></td>
								<td class="text-center"><?php echo $objChartOfAccounts->getAccountTitleByCode($report_row['CHARGED_TO']); ?></td>
								<td class="text-center"><?php echo $objMachines->getName($report_row['MACHINE_ID']); ?></td>
								<td class="text-center"><?php echo $objItems->getItemTitle($report_row['ITEM_ID']); ?></td>
								<td class="text-center"><?php echo $report_row['QUANTITY']; ?></td>
								<td class="text-center"><?php echo $report_row['COST_PRICE']; ?></td>
								<td class="text-center"><?php echo $report_row['TOTAL_COST']; ?></td>
                            </tr>
<?php
							$showTotal = true;

							$quantity     += $report_row['QUANTITY'];
							$cost_price   += $report_row['COST_PRICE'];
							$total_cost   += $report_row['TOTAL_COST'];
?>
						</tbody>
<?php
			}
?>

<?php
						if(isset($showTotal)){
?>
                        <tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;" colspan="4">Total:</td>
                                <td style="text-align:center;"><?php echo number_format($quantity,3); ?></td>
                                <td style="text-align:center;"><?php echo number_format($cost_price,2); ?></td>
                                <td style="text-align:center;"><?php echo number_format($total_cost,2); ?></td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div style="height:30px;"></div>
<?php
	}//if shedList NumRows
?>
                    <div class="clear"></div>
                </div> <!--End bodyTab-->
                <div style="height:20px;clear:both"></div>
            </div> <!-- End .content-box-content -->
<?php
			}
?>
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
