<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/emb_inward.php');
	include('common/classes/customers.php');

	//Permission
	if(!in_array('emb-inward',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objEmbroideryInward 	= new EmbroideryInward();
	$objCustomers 				= new Customers();

	$total 		 = $objConfigs->get_config('PER_PAGE');

	$this_page = 0;

	$customersList = $objCustomers->getList();

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$objEmbroideryInward->account_code  = isset($_POST['acc_code'])?mysql_real_escape_string($_POST['acc_code']):"";
	$objEmbroideryInward->lot_num  		= isset($_POST['lot_num'])?mysql_real_escape_string($_POST['lot_num']):"";

	$inwardList 		 = $objEmbroideryInward->search($start, $total);
	$found_records = $objEmbroideryInward->found_records;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       							/>
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     							/>
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     							/>
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  								/>
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  							/>
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
		<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  									/>
		<style media="screen">
			td{
				font-size: 14px !important;
				padding: 10px !important;
			}
			th{
				font-size: 14px !important;
				padding: 10px !important;
			}
		</style>

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
			$(function(){
				$("select").selectpicker();
			});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Cloth Receipt Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
											<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
											<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
											<a href="emb-inward-details.php"><div class="tab">Add</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"none":""; ?>;" >
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
															<th class="text-center col-xs-2">Inward Date</th>
															<th class="text-center col-xs-1">Lot No</th>
															<th class="text-center col-xs-3">Party Title</th>
															<th class="text-center col-xs-1">Thaan(s)</th>
															<th class="text-center col-xs-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
				if(mysql_num_rows($inwardList)){
					$serial = ($this_page>0)?$this_page*$total:$this_page;
					$serial++;
					while($row = mysql_fetch_array($inwardList)){
						$party_title = $objCustomers->getTitle($row['ACCOUNT_CODE']);
						$thaans 	 = $objEmbroideryInward->getTotalThaans($row['ID']);
?>
                            <tr id="recordPanel" class="onsearch_hide" data-row-id="<?php echo $row['ID']; ?>">
															<td class="text-center" ><?php echo date('d-m-Y',strtotime($row['INWD_DATE'])); ?></td>
															<td class="text-center" ><?php echo $row['LOT_NO']; ?></td>
															<td class="text-left" ><?php echo $party_title; ?></td>
															<td class="text-center" ><?php echo $thaans; ?></td>
															<td class="text-center" >
																<a href='emb-inward-details.php?id=<?php echo $row['ID']; ?>' target="_blank" id="view_button"><i class="fa fa-pencil"></i></a>
																<?php if(in_array('emb-inward-delete',$permissionz) || $admin == true){ ?>
																<a class="pointer" do="<?php echo $row['ID']; ?>"><i class="fa fa-times"></i></a>
																<?php } ?>
                            	</td>
                            </tr>
<?php
						$serial++;
					}
				}else{
?>
							<tr>
                            	<th colspan="100" style="text-align:center;">
                                	<?php echo (isset($_POST['search']))?"0 Records Found!!":"No Records." ?>
                                </th>
                            </tr>
<?php
				}
?>
                        </tbody>
<?php
					if(isset($_POST['search'])){
?>
						<tfoot>
                        	<tr>
                                <td colspan="6" style="text-align:center;">
                                    <a class="btn btn-warning" href="emb-inward.php"><i class="fa fa-refresh"></i> Clear Search</a>
                                </td>
                            </tr>
                        </tfoot>
<?php
					}
?>
                    </table>
										<div class="col-xs-12 text-center">
											<?php
											if($found_records > $total){
												$get_url = "";
												foreach($_GET as $key => $value){
													$get_url .= ($key == 'page')?"":"&".$key."=".$value;
												}
												?>
												<nav>
													<ul class="pagination">
														<?php
														$count = $found_records;
														$total_pages = ceil($count/$total);
														$i = 1;
														$thisFileName = basename($_SERVER['PHP_SELF']);
														if(isset($this_page) && $this_page>0){
															?>
															<li>
																<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
															</li>
															<?php
														}
														if(isset($this_page) && $this_page>=1){
															$prev = $this_page;
															?>
															<li>
																<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
															</li>
															<?php
														}
														$this_page_act = $this_page;
														$this_page_act++;
														while($total_pages>=$i){
															$left = $this_page_act-5;
															$right = $this_page_act+5;
															if($left<=$i && $i<=$right){
																$current_page = ($i == $this_page_act)?"active":"";
																?>
																<li class="<?php echo $current_page; ?>">
																	<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
																</li>
																<?php
															}
															$i++;
														}
														$this_page++;
														if(isset($this_page) && $this_page<$total_pages){
															$next = $this_page;
															?>
															<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
															<?php
														}
														if(isset($this_page) && $this_page<$total_pages){
															?>
															<li>
																<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
															</li>
														</ul>
													</nav>
													<?php
												}
											}
											?>
										</div>
                </div> <!--End bodyTab1-->
                <div class="clear"></div>

                <div id="bodyTab2" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"block":"none"; ?>;">
                    <div id="form">
                    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?tab=list">
                        <div class="caption"></div>
                        <?php echo (isset($message))?"<div class=\"message red\">$message</div>":""; ?>
                        <div class="clear"></div>

                        <div class="caption">Customer Title</div>
                        <div class="field" style="width:300px;">
                            <select class="acc_title show-tick form-control" name="acc_code" data-live-search="true">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
								$selected = (isset($inward)&&$inward['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>

                        </div>
                        <div class="clear"></div>

                        <div class="caption">Lot No</div>
                        <div class="field">
                        	<input type="text" value="" name="lot_num" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>

                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<script>
	$(document).ready(function(){
		$("a.pointer").click(function(){
			var idValue    = $(this).attr("do");
			var currentRow = $(this).parent().parent().parent().parent().parent();
			var billNumbr  = currentRow.find('td').first().text();
			$("#xfade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm' style='font-weight:bold;'>Confirm Delete?</p><a class='dodelete btn btn-info'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#xfade").fadeIn();
			var win_hi 			= $(window).height()/2;
			var win_width 	= $(window).width()/2;
			win_hi 					= win_hi-$("#popUpDel").height()/2;
			win_width 			= win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel .confirm").text("Sure to Delete Lot #"+billNumbr+"?");
			$("#popUpDel").slideDown();
			$(".dodelete").click(function(){
				$.post('db/del-emb-inward.php', {cid : idValue}, function(data){
					if(data == 'Y'){
						$("tr[data-row-id='"+idValue+"']").slideUp();
						$("#popUpDel").slideUp(function(){
							$(this).remove();
							$("#xfade").fadeOut();
						});
					}else{
						$("#popUpDel .confirm").text("Lot is under process, cannot be deleted.");
						$(".nodelete").text('Close');
						$(".dodelete").hide();
					}
				});
			});
			$(".nodelete").click(function(){
				$("#popUpDel").slideUp(function(){
					$(this).remove();
				});
				$("#xfade").fadeOut();
			});
		});
		$("input[type='text']").keyup(function(e){
			if(e.keyCode==27){
				$(this).blur();
			}
		});
	});
</script>
<script>
<?php
	if(isset($_GET['tab']) && $_GET['tab'] =='search'){
		echo "tab('2', '1', '2');";
	}
?>
<?php
	if(isset($_POST['search'])){
		echo "tab('1', '1', '2');";
	}
?>
</script>
