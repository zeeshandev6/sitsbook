<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/sale.php');
	include('common/classes/accounts.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/sale_orders.php');
	include('common/classes/sale_order_details.php');
	include 'common/classes/j-voucher.php';
	include('common/classes/itemCategory.php');
	include 'common/classes/items.php';
	include('common/classes/customers.php');

	//Permission
	if( (!in_array('sale-orders',$permissionz)) && (!in_array('sales-panel',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objSale               	= new Sale();
	$objJournalVoucher     	= new JournalVoucher();
	$objScanPurchaseDetails	= new ScanPurchaseDetails();
	$objScanSale   	 	    	= new ScanSale();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objSaleOrders         	= new SaleOrders();
	$objSaleOrderDetails   	= new SaleOrderDetails();
	$objAccountCodes       	= new ChartOfAccounts();
	$objItemCategory 			 	= new itemCategory();
	$objItems              	= new Items();
	$objCustomers          	= new Customers();
	$objConfigs            	= new Configs();

	if(isset($_POST['set_product_id'])){
		$order_details_id = (int)mysql_real_escape_string($_POST['order_details_id']);
		$product_id 			= (int)mysql_real_escape_string($_POST['set_product_id']);
		$objSaleOrderDetails->set_product_id($order_details_id,$product_id);
		exit();
	}

	$order_id = 0;
	if(isset($_POST['order_rows'])){
		$order_date = date("Y-m-d",strtotime($_POST['order_date']));
		$order_rows = $_POST['order_rows'];
		$order_rows = json_decode($order_rows);

		$objSaleOrders->order_date = $order_date;
		if(count($order_rows)){
			$order_id 								 = $objSaleOrders->save();
		}
		if($order_id>0){
			$objSaleOrderDetails->order_id = $order_id;
			foreach($order_rows as $key => $row){
				$objSaleOrderDetails->order_no			= $row->order_no;
				$objSaleOrderDetails->customer_name	= $row->customer_name;
				$objSaleOrderDetails->product_id	  = $row->product_id;
				$objSaleOrderDetails->product_title	= $row->product_name;
				$objSaleOrderDetails->quantity		  = $row->quantity_td;
				$objSaleOrderDetails->amount 			  = $row->amount;
				$objSaleOrderDetails->email 			  = $row->email;
				$objSaleOrderDetails->mobile 			  = $row->mobile;

				$objSaleOrderDetails->save();
			}
			echo $order_id;
		}
		exit();
	}

	$message = '';
	$lines_arr = array();
	if(isset($_POST['save'])){
		if(!$_FILES['orders_csv']['error'] > 0){
			if(pathinfo($_FILES['orders_csv']['name'],PATHINFO_EXTENSION)=='csv'){
				$filename 	 = $_FILES['orders_csv']['tmp_name'];
				$file 		   = fopen($filename,'r');
				while(!feof($file)){
					$lines_arr[] = fgets($file);
				}
			}else{
				$message = 'Invalid File!';
			}
		}else{
			$message =  'File Not Selected!';
		}
	}

	if(isset($_GET['id'])){
		$order_id = (int)$_GET['id'];
		$order_date = $objSaleOrders->getOrderDate($order_id);
		$order_date = date('d-m-Y',strtotime($order_date));

		$sale_order_rows = $objSaleOrderDetails->getListByOrderId($order_id);

		if(mysql_num_rows($sale_order_rows)){
			while($row = mysql_fetch_assoc($sale_order_rows)){
				if(!isset($lines_arr_ol[$row['ORDER_NO']])){
					$lines_arr_ol[$row['ORDER_NO']] = array();
				}
				$str = '';
				$str .= $row['ORDER_NO'].",";
				$str .= $row['CUSTOMER_NAME'].",";
				$str .= $row['PRODUCT_TITLE'].",";
				$str .= $row['QUANTITY'].",";
				$str .= $row['AMOUNT'].",";
				$str .= $row['EMAIL'].",";
				$str .= $row['MOBILE'];
				$lines_arr_ol[$row['ORDER_NO']][$row['ID']] = $str;
				$lines_arr[]   									= $row;
			}
		}

	}else{
		$order_date = date('d-m-Y');
	}

	$categ_arr = array();
	$items_arr = array();
	$itemsCategoryList   = $objItemCategory->getList();
	if(mysql_num_rows($itemsCategoryList)){
		while($ItemCat = mysql_fetch_array($itemsCategoryList)){
			$categ_arr[$ItemCat['ITEM_CATG_ID']] = $ItemCat;
			$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
			if(mysql_num_rows($itemList)){
				while($theItem = mysql_fetch_array($itemList)){
					if($theItem['ACTIVE'] == 'N'){
						continue;
					}
					if($theItem['INV_TYPE'] != 'B'){
						continue;
					}
					if(!isset($items_arr[$ItemCat['ITEM_CATG_ID']])){
						$items_arr[$ItemCat['ITEM_CATG_ID']] = array();
					}
					$items_arr[$ItemCat['ITEM_CATG_ID']][] = $theItem;
				}
			}
		}
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		table td{
			padding: 2px !important;
		}
		table td .form-control{
			font-family: "Trebuchet MS" !important;
			font-size: 14px !important;
			padding: 2px !important;
			height: auto !important;
		}
		table td .btn-default{
			font-size: 14px !important;
			padding: 2px !important;
		}
		.bg-danger{
			background-color: rgba(255,0,0,0.5) !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sale.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("select").selectpicker();
		$("a.delete_record").click(function(){
			//$(this).deleteMainRow("db/del-sale.php");
		});
		$("select.product_selector").change(function(){
			var row_id = $(this).attr('row-id');
			var product_id = parseInt($(this).find("option:selected").val())||0;
			if(product_id>0&&row_id>0){
				$.post('sale-orders.php',{order_details_id:row_id,set_product_id:product_id},function(){

				});
			}
		});
		$("td.quantity_td").each(function(){
			var qty    = parseInt($(this).find("input").val())||0;
			var amount = parseFloat($(this).next('td').find("input").val())||0;
			amount     = amount/qty;
			var counter = 1;
			if(qty>1){
				$this_row= $(this).parent();
				$(this).parent().addClass("qty_explode");
				while(counter<=qty){
					$row_elm = $($this_row).clone();
					$($row_elm).insertAfter($this_row);
					counter++;
				}
				$("tr.qty_explode").first().nextAll("tr.qty_explode").each(function(){
					$(this).find("td.quantity_td").find("input").val(1);
					$(this).find("td.quantity_td").next().find("input").val(amount);
				});
				$("tr.qty_explode").first().remove();
				$("tr.qty_explode").removeClass("qty_explode");
			}
		});
		$(".save_sale_orders").click(function(){
			if($("table.sale_orders_table tbody tr").length==0){
				return;
			}
			$(this).hide();
			var order_date = $("input[name='order_date']").val();
			var jSonString = {};
			var success    = true;
			$("table.sale_orders_table tbody tr").each(function(index,elm){
				$(this).find("td.product_id").removeClass("bg-danger");
				if((parseInt($(this).find("td.product_id select option:selected").val())||0)==0){
					$(this).find("td.product_id").addClass("bg-danger");
					success    = false;
					return
				}
				jSonString[index]             		= {};
				jSonString[index].order_no 				= parseInt($(this).find("td.order_no input").val())||0;
				jSonString[index].customer_name 	= $(this).find("td.customer_name input").val();
				jSonString[index].product_name 		= $(this).find("td.product_name input").val();
				jSonString[index].product_id 			= $(this).find("td.product_id select option:selected").val();
				jSonString[index].quantity_td 		= parseInt($(this).find("td.quantity_td input").val())||0;
				jSonString[index].amount 					= parseFloat($(this).find("td.amount input").val())||0;
				jSonString[index].email 					= $(this).find("td.email input").val();
				jSonString[index].mobile 					= $(this).find("td.mobile input").val();
			});
			if(!success){
				return;
			}
			jSonString = JSON.stringify(jSonString);
			$.post("sale-orders.php",{order_date:order_date,order_rows:jSonString},function(data){
					data = parseInt(data)||0;
					if(data>0){
						window.location.href = 'sale-orders.php?id='+data;
					}else{
						$(".save_sale_orders").show();
					}
			});
		});
	});
	</script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sale Orders</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<a href="sale-orders-list.php"><div class="tab" id="tab1">List</div></a>
						<a href="sale-orders.php"><div class="tabSelected" id="tab1">Details</div></a>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form" class="">
						<?php
						if(count($lines_arr)==0){
							?>
							<form class="" action="" method="post" enctype="multipart/form-data">
								<div class="caption">Orders File <span class="text-danger">*.csv</span> </div>
								<div class="field">
									<input type="file" name="orders_csv" class="btn btn-default btn-sm" accept=".csv" />
								</div>
								<div class="clear"></div>

								<div class="caption"></div>
								<div class="field">
									<input type="submit" name="save" value="Upload" class="button" />
								</div>
								<div class="clear"></div>
							</form>

							<div class="clear mt-25"></div>

							<?php
						}
						if(count($lines_arr)>0){
							?>

							<div class="caption">Order Date</div>
							<div class="field" style="width: 150px;">
								<input type="text" name="order_date" value="<?php echo $order_date; ?>" class="form-control datepicker" />
							</div>
							<div class="clear mt-20"></div>

							<table class="sale_orders_table">
								<thead>
									<tr>
										<th class="text-center" style="width:2.5%;">Order #</th>
										<th class="text-center" style="width:10%;">CustomerName</th>
										<th class="text-center" style="width:10%;">ProductName</th>
										<th class="text-center" style="width:10%;">ActualProduct</th>
										<th class="text-center" style="width:2.5%;">Quantity</th>
										<th class="text-center" style="width:2.5%;">Amount</th>
										<th class="text-center" style="width:10%;">Email</th>
										<th class="text-center" style="width:5%;">Mobile</th>
										<?php if($order_id>0){ ?>
										<th class="text-center" style="width:2.5%;">Action</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody class="order_rows_tbody">
<?php
									if($order_id==0){
										$lines_arr_ol = array();
										foreach ($lines_arr as $key => $order_row){
											if($key==0)  continue;
											$orow = $order_row;
											$order_row = explode(',',$order_row);
											$order_row[0] = (int)$order_row[0];
											if(!isset($lines_arr_ol[$order_row[0]])){
												$lines_arr_ol[$order_row[0]] = array();
											}
											$lines_arr_ol[$order_row[0]][] = $orow;
										}
										ksort($lines_arr_ol,1);
									}

									foreach ($lines_arr_ol as $kex => $order_rows){
										foreach($order_rows as $main_index => $order_row){
											$order_row = explode(',',$order_row);

											if(count($order_row)==7){
												$amount = (float)$order_row[4];
												$amount = str_ireplace('"','',$amount);
												$amount = str_ireplace("'",'',$amount);
												$amount = str_ireplace("Rs.",'',$amount);
												$amount = (float)$amount;
												$email  = $order_row[5];
												$mobile = $order_row[6];
											}elseif(count($order_row)==8){
												$amount = $order_row[4].$order_row[5];
												$amount=str_ireplace('"','',$amount);
												$amount=str_ireplace("'",'',$amount);
												$amount=str_ireplace("Rs.",'',$amount);
												$amount=(float)$amount;
												$email  = $order_row[6];
												$mobile = $order_row[7];
											}else{
												continue;
											}
											$order_row[0]=(int)$order_row[0];
											$order_row[1]=str_ireplace('"','',$order_row[1]);
											$order_row[1]=str_ireplace("'",'',$order_row[1]);

											$order_row[2]=str_ireplace('"','',$order_row[2]);
											$order_row[2]=str_ireplace("'",'',$order_row[2]);
											$order_row[3]=(float)$order_row[3];

											$mobiles_found = $objCustomers->checkByColumnXvalue('MOBILE',$mobile);
											$emails_found  = $objCustomers->checkByColumnXvalue('EMAIL',$email);
											$customer_code = ($mobiles_found==NULL)?"":$mobiles_found;
											$customer_code = ($emails_found==NULL)?$customer_code:$emails_found;
?>
											<tr>
												<td class="text-center order_no"><input type="text" class="form-control text-center" value="<?php echo $order_row[0]; ?>"></td>
												<td class="text-left 	 customer_name"><input type="text" class="form-control" value="<?php echo $order_row[1]; ?>"></td>
												<td class="text-left   product_name"><input type="text" class="form-control" value="<?php echo $order_row[2]; ?>"></td>
												<td class="text-left   product_id">
													<select class="product_selector form-control" name="">
														<option value=""></option>
<?php
															foreach($categ_arr as $category_id => $category_row){
																$item_rows = $items_arr[$category_id];
																?><optgroup label="<?php echo $category_row['NAME']; ?>"><?php
																foreach($item_rows as $cat_id => $item_row){
																	$selected = ($item_row['NAME']==$order_row[2])?"selected":"";
?>
																	<option value="<?php echo $item_row['ID']; ?>" <?php echo $selected; ?>><?php echo $item_row['NAME']; ?></option>
<?php
																}
																?></optgroup><?php
															}
?>
													</select>
												</td>
												<td class="text-center quantity_td"><input type="text" class="form-control text-center" value="<?php echo $order_row[3]; ?>" readonly></td>
												<td class="text-center amount"><input type="text" class="form-control text-center" value="<?php echo $amount; ?>"></td>
												<td class="<?php echo ($emails_found!=NULL)?"bg-info":""; ?> email"><input type="text" class="form-control" value="<?php echo $email; ?>"></td>
												<td class="<?php echo ($mobiles_found!=NULL)?"bg-success":""; ?> mobile"><input type="text" class="form-control" value="<?php echo $mobile; ?>"></td>
												<?php if($order_id>0){ ?>
													<td class="text-center">
														<?php
															$sale_row = $objScanSaleDetails->order_number_placed($order_row[0]);
															if($sale_row!=NULL){
														?>
															<a href="mobile-sale-details.php?id=<?php echo $sale_row['SS_ID']; ?>" target="_blank">Bill#<?php echo $sale_row['BILL_NO']; ?></a>
														<?php }else{ ?>
														<a href="mobile-sale-details.php?saleorder=<?php echo $order_row[0]; ?>&customer_code=<?php echo $customer_code; ?>" target="_blank">Post</a>
														<?php } ?>
													</td>
												<?php } ?>
											</tr>
											<?php
										}
									}
									?>
								</tbody>
							</table>
							<div class="clear mt-20"></div>
<?php
							if(!isset($_GET['id'])){
?>
							<div class="pull-right">
								<button type="button" class="button save_sale_orders">Save</button>
							</div>
							<div class="clear mt-20"></div>
							<?php
							}
						}
						?>
					</div><!--End form-->
				</div> <!--End bodyTab1-->
			</div> <!-- End .content-box-content -->
		</div> <!-- End .content-box -->
	</div><!--body-wrapper-->
	<div id="xfade"></div>
</body>
</html>
