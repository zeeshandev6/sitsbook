<?php
    include('common/connection.php');
    include('common/config.php');
    include('common/classes/customers.php');

    $objCustomers  = new Customers();

    if(isset($_POST['dcol'])){
        $col_name = mysql_real_escape_string(trim($_POST['dcol']));
        $dbefore = mysql_real_escape_string(trim($_POST['dbefore']));
        $dafter = mysql_real_escape_string(trim($_POST['dafter']));
        $updated = $objCustomers->updateAreaSectorByNameGroup($col_name,$dbefore,$dafter);
        $array = array();
        if($updated){
            $array['MSG'] = ucfirst(strtolower($col_name))." Updated Successfully.";
            $array['TYPE']= 'success';
        }else{
            $array['MSG'] = "Error! Cannot update ".ucfirst(strtolower($col_name)).".";
            $array['TYPE']= 'danger';
        }
        echo json_encode($array);
        exit();
    }

    $sector_list = $objCustomers->get_sector_array();
    $areas_list   = $objCustomers->get_area_array();
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/measure-type.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("body").on('change',"input[data-after]",function(){
                $(this).parent().removeClass("bg-success");
                $(this).parent().addClass("bg-warning");
            });
            $("body").on('click',"button.save",function(){
                var btn = $(this);
                $(btn).prop("disabled",true);
                var target_id = $(this).attr("data-target");
                var dbefore  = $("input[data-before='"+target_id+"']").val();
                var dafter   = $("input[data-after='"+target_id+"']").val();
                var dcol   = $("input[data-after='"+target_id+"']").attr("data-col");
                $.post("",{dbefore:dbefore,dafter:dafter,dcol:dcol},function(data){
                    $(btn).prop("disabled",false);
                    data = JSON.parse(data);
                    if(data['TYPE'] == 'success'){
                        $("input[data-before='"+target_id+"']").val(dafter);
                        $("input[data-after='"+target_id+"']").parent().removeClass("bg-warning");
                        $("input[data-after='"+target_id+"']").parent().addClass("bg-success");
                    }
                    $.notify({
                        message: data['MSG']
                    },{
                        type: data['TYPE'],
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                });
            });
        });
    </script>
</head>
<body>
<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
<div id="bodyWrapper">
    <div class = "content-box-top" style="overflow:visible;">
        <div class = "summery_body">
            <div class = "content-box-header">
                <p><B>Area and Sector Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span></p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>

            <div id="bodyTab1">
                <div class="mt-30"></div>
                <div class="col-sm-12 col-md-6">
                    <h4 class="bold-text">Sector List</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%" class="bold-text text-center">Sr No.</th>
                                <th width="80%" class="bold-text text-center" colspan="2">Sector Title</th>
                                <th width="10%" class="bold-text text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($sector_list as $key => $sector_name){
                                    if(trim($sector_name)==''){
                                        continue;
                                    }
                            ?>
                            <tr>
                                <td class="text-center"><?=$key+1;?></td>
                                <td class="text-center">
                                    <input type="text" class="form-control" data-before="s-<?=$key;?>"  value="<?=trim($sector_name);?>" readonly/>
                                </td>
                                <td class="text-center">
                                    <input type="text" class="form-control" data-col="SECTOR" data-after="s-<?=$key;?>" value="<?=trim($sector_name);?>" />
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-primary save" data-target="s-<?=$key;?>">Save</button>
                                </td>
                            </tr>

                            <?php 
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-12 col-md-6">
                    <h4 class="bold-text">Area List</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%" class="bold-text text-center">Sr No.</th>
                                <th width="80%" class="bold-text text-center" colspan="2">Area Title</th>
                                <th width="10%" class="bold-text text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($areas_list as $key => $area_name){
                                    if(trim($area_name)==''){
                                        continue;
                                    }
                            ?>
                            <tr>
                                <td class="text-center"><?=$key+1;?></td>
                                <td class="text-center">
                                    <input type="text" class="form-control" data-before="a-<?=$key;?>"  value="<?=trim($area_name);?>" readonly/>
                                </td>
                                <td class="text-center">
                                    <input type="text" class="form-control" data-col="AREA" data-after="a-<?=$key;?>" value="<?=trim($area_name);?>" />
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-primary save" data-target="a-<?=$key;?>">Save</button>
                                </td>
                            </tr>

                            <?php 
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div> <!--bodyTab1-->
        </div><!-- End summer -->
    </div><!-- End .content-box-top-->
</div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
