<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
    include('common/classes/customers.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     = new ChartOfAccounts();
    $objCustomers        = new Customers();
	$objCompanyDetails   = new CompanyDetails();
	$objConfigs 		 = new Configs();

	$company_details 	 = $objCompanyDetails->getActiveProfile();

	if(isset($_GET['search'])){
		$objCustomers->area 		 = mysql_real_escape_string(trim($_GET['area']));
		$objCustomers->sector 		 = mysql_real_escape_string(trim($_GET['sector']));
		$objCustomers->city 		 = mysql_real_escape_string(trim($_GET['city']));

		$customer_report = $objCustomers->search(0,100000);
	}
?>
<!DOCTYPE html>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
		$(window).on('load',function(){
			$(".printThis").click(function(){
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/print-table-only-reports.css',
				  pageTitle: "Sitsbook.com",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
		});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sales Reports</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="">


							<div class="caption">City :</div>
							<div class="field">
								<input type="text" class="form-control" name="city" />
							</div>
							<div class="clear"></div>

							<div class="caption">Sector :</div>
							<div class="field">
								<input type="text" class="form-control" name="sector" />
							</div>
							<div class="clear"></div>

							<div class="caption">Area :</div>
							<div class="field">
								<input type="text" class="form-control" name="area" />
							</div>
							<div class="clear"></div>

							<div class="caption"></div>
							<div class="field">
								<input type="submit" value="Search" name="search" class="button"/>
							</div>
							<div class="clear"></div>
						</form>
					</div><!--form-->
<?php
		if(isset($customer_report)){
?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							<?php echo $company_details['NAME']; ?>
						</p>
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							Customers List
						</p>
						<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
						<div class="clear"></div>
					</div>
					<?php
					$prevBillNo = '';
					if(mysql_num_rows($customer_report)){
						?>
						<table class="tableBreak">
								<thead class="tHeader">
									<tr>
										<th class="text-center" width="5%">Sr.</th>
										<th class="text-center" width="20%">Customer Title</th>
										<th class="text-center" width="20%">Person</th>
										<th class="text-center" width="10%">Mobile</th>
										<th class="text-center" width="30%">Address</th>
										<th class="text-center" width="10%">Area</th>
										<th class="text-center" width="10%">Sector</th>
									</tr>
								</thead>
								<tbody>
<?php
									$counter = 1;
									while($detailRow = mysql_fetch_array($customer_report)){
?>
										<tr id="recordPanel" class="alt-row">
											<td class="text-center"><?=$counter;?></td>
											<td class="text-left"><?php echo $detailRow['CUST_ACC_TITLE']; ?></td>
											<td class="text-center"><?php echo $detailRow['CONT_PERSON']; ?></td>
											<td class="text-center"><?php echo $detailRow['MOBILE']; ?></td>
											<td class="text-center"><?php echo $detailRow['ADDRESS']; ?></td>
											<td class="text-center"><?php echo $detailRow['AREA']; ?></td>
											<td class="text-center"><?php echo $detailRow['SECTOR']; ?></td>
										</tr>
<?php
										$counter++;
									}
?>
									</tbody>
									<?php
								}
?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					} //end if is generic report
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
if(isset($reportType)&&$reportType=='generic'){
	?>
	tab('1', '1', '2')
	<?php
}
?>
<?php
if(isset($reportType)&&$reportType=='specific'){
	?>
	tab('2', '1', '2')
	<?php
}
?>
</script>
