<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');
	include('common/classes/customers.php');
	include('common/classes/sale.php');
	include('common/classes/dist_sale.php');
	include('common/classes/emb_outward.php');
	include('common/classes/emb_billing.php');
	include('common/classes/ledger_report.php');
	include('common/classes/cash_management.php');

	//Permission
	if( (!in_array('party-receivables-report',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes    = new ChartOfAccounts();
	$objJournalVoucher  = new JournalVoucher();
	$objSale 						= new Sale();
	$objDistSale        = new DistributorSale();
	$objOutward   	 		= new outward();
	$objEmbBilling 			= new EmbBilling();
	$objLedgerReport    = new ledgerReport();
	$objCashManagement  = new CashManagement();
	$objCustomers 			= new Customers();

	if(isset($_POST['report_date'])){
		$report_date = date('Y-m-d',strtotime($_POST['report_date']));

		$objCustomers->sector= isset($_POST['sector'])?mysql_real_escape_string($_POST['sector']):"";
		$partyCodes  = $objCustomers->search();
	}
	$salesman_list = $objAccounts->getActiveListByDesType('S');

	$sector_list = $objCustomers->get_sector_array();
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />

		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$("select").selectpicker();
			});
		</script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Party Receivable Report</p>
                <div class="clear"></div>
            </div> <!--End .content-box-header-->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <form method="post" action="">
                            <div class="caption">Report Date</div>
                            <div class="field" style="width:150px;">
                                <input type="text" name="report_date" class="form-control datepicker" style="width:145px;font-size: 14px;" value="<?php echo isset($report_date)?date('d-m-Y',strtotime($report_date)):date('d-m-Y'); ?>" />
                            </div>
														<div class="clear"></div>

														<div class="caption">Sector</div>
                            <div class="field" style="width:200px;">
															<select class="form-control" name="sector">
																<option value=""></option>
																<?php
																	foreach ($sector_list as $key => $sector) {
																		?><option value="<?php echo $sector ?>"><?php echo $sector; ?></option><?php
																	}
																?>
															</select>
                            </div>
														<div class="clear"></div>

														<div class="caption"></div>
														<div class="field" style="width:200px;">
															<input type="submit" value="Generate" name="search" class="btn btn-primary pull-left"/>
														</div>
                            <div class="class"></div>
                        </form>
										</div><!--form-->
                	<div class="class"></div>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
<?php
	if(isset($partyCodes)){
?>
            <div class="content-box-content" id="PrintMe" style="padding: 20px;">
            	<span style="float:right;"><button class="btn btn-primary printThis">Print</button></span>
							<div class="clear"></div>

            	<div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    <div style="text-align:left;margin-bottom:20px;height: 50px;" class="pageHeader">
                    	<p style="font-size:20px;padding:0px;margin: 0px;">Party Receivable Report</p>
                        <p style="font-weight:bold;padding:0px;margin: 0px;"> Date : <?php echo date('d-m-Y',strtotime($report_date)); ?> </p>
                    </div>
                    <div class="clear"></div>

                    <table class='tableBreak'>
                        <thead class="tHeader" >
                            <tr style="background:#EEE;">
                               <th width="5%"  style="font-size: 12px;text-align:center">S.No</th>
                               <th width="15%" style="font-size: 12px;text-align:center">Account</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Last Bill #</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Bill Dated</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Bill Amount</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Last Receipt Date</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Receipt Amount</th>
                               <th width="10%" style="font-size: 12px;text-align:center">Ledger Balance</th>
															 <th width="10%" style="font-size: 12px;text-align:center">PostDated Receipts</th>
															 <th width="10%" style="font-size: 12px;text-align:center">PostDated Payments</th>
															 <th width="10%" style="font-size: 12px;text-align:center">AdjustedLedger Balance</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
						$counter 							 = 1;
						$total_balance 				 = 0;
						$post_amount_rec_total = 0;
						$post_amount_pay_total = 0;

						$last_bill 						 = NULL;
						$last_bill_amount			 =  '';
						$last_bill_date				 =  '';

						if(mysql_num_rows($partyCodes)){
							while($partyCode 	= mysql_fetch_array($partyCodes)){
								$openingBalance = $objLedgerReport->getLedgerOpeningBalance($report_date, $partyCode['CUST_ACC_CODE']);
								if($embProdAddon == 'Y'){
									$last_bill    = $objEmbBilling->getLastBill($report_date,$partyCode['CUST_ACC_CODE']);
									if(strtotime($last_bill['OUTWD_DATE']) > strtotime($last_bill['SALE_DATE'])){
										$last_bill_number = $last_bill['BILL_NO'];
										$last_bill_amount = $objEmbBilling->getAmountSum($last_emb_bill['OUTWD_ID']);
										$last_bill_date 	= date('d-m-Y',strtotime($last_bill['OUTWD_DATE']));
									}
									if(strtotime($last_bill['OUTWD_DATE']) < strtotime($last_bill['SALE_DATE'])){
										$last_bill_number = $last_bill['BILL_NO'];
										$last_bill_amount = $last_bill['BILL_AMOUNT'];
										$last_bill_date 	= date('d-m-Y',strtotime($last_bill['SALE_DATE']));
									}
								}
								if($salePurchaseModule == 'Y'){
									$last_bill 				= $objSale->getLastBillByAccountCode($report_date,$partyCode['CUST_ACC_CODE']);
									$last_bill_number = $last_bill['BILL_NO'];
									$last_bill_amount = $objSale->getTotalAmount($last_bill['ID']);
									$last_bill_date 	= date('d-m-Y',strtotime($last_bill['SALE_DATE']));
								}
								if($distSalesAddon == 'Y'){
									$last_bill 				= $objDistSale->getLastBillByAccountCode($report_date,$partyCode['CUST_ACC_CODE']);
									$last_bill_number = $last_bill['BILL_NO'];
									$last_bill_amount = $objDistSale->getTotalAmount($last_bill['ID']);
									$last_bill_date 	= date('d-m-Y',strtotime($last_bill['SALE_DATE']));
								}

								$last_credit    = $objJournalVoucher->getLastSpecificTransaction($report_date,$partyCode['CUST_ACC_CODE'],'Cr');

								$last_credit['AMOUNT'] 				= isset($last_credit['AMOUNT'])?$last_credit['AMOUNT']:'';
								$last_credit['VOUCHER_DATE'] 	=  isset($last_credit['VOUCHER_DATE'])?date('d-m-Y',strtotime($last_credit['VOUCHER_DATE'])):'';
								$partyCode['CUST_ACC_TITLE'] 			= strtolower($partyCode['CUST_ACC_TITLE']);
								if($openingBalance == 0){
									continue;
								}
								$party_balance 		 = $objJournalVoucher->getBalanceType($partyCode['CUST_ACC_CODE'],$openingBalance);
								$post_dated_rec 	 = (float)$objCashManagement->getPostDatedChequesAmount($partyCode['CUST_ACC_CODE'],$report_date,'R');
								$post_dated_pay 	 = (float)$objCashManagement->getPostDatedChequesAmount($partyCode['CUST_ACC_CODE'],$report_date,'P');
?>
                            <tr id="recordPanel" class="alt-row">
                                <td style="text-align:center;"><?php echo $counter; ?></td>
                                <td style="font-size: 14px;"  ><?php echo ucfirst(substr($partyCode['CUST_ACC_TITLE'],0,20));?></td>
                                <td style="text-align:center;"><?php echo ($last_bill == NULL)?"":$last_bill_number; ?></td>
                                <td style="text-align:center;"><?php echo ($last_bill == NULL)?"":$last_bill_date; ?></td>
                                <td style="text-align:right;" class="hasComma"><?php echo ($last_bill == NULL)?"":number_format($last_bill_amount,2); ?></td>
                                <td style="text-align:center;"><?php echo $last_credit['VOUCHER_DATE']; ?></td>
                                <td style="text-align:right;" class="hasComma"><?php echo $last_credit['AMOUNT'];?></td>
                                <td style="text-align:right;" class="hasComma"><?php echo $party_balance['BALANCE']." ".$party_balance['TYPE']; ?></td>
																<td style="text-align:right;" class="hasComma"><?php echo number_format($post_dated_rec,2); ?></td>
																<td style="text-align:right;" class="hasComma"><?php echo number_format($post_dated_pay,2); ?></td>
																<td style="text-align:right;" class="hasComma"><?php echo number_format(($post_dated_rec+$openingBalance)-$post_dated_pay,2); ?></td>
                            </tr>
<?php
								$counter++;
								$total_balance 				 += $openingBalance;
								$post_amount_rec_total += $post_dated_rec;
								$post_amount_pay_total += $post_dated_pay;
							}
						}
?>
                        </tbody>
                        <tfoot class='tableFooter'>
                            <tr style="background-color:#FFF;font-size: 12px;border: 1px solid #666;">
                                <td style="text-align:right; font-weight:bold;" colspan="3">Total:</td>
                                <td style="text-align:center;">- - -</td>
                                <td style="text-align:center;">- - -</td>
                                <td style="text-align:center;">- - -</td>
                                <td style="text-align:center;">- - -</td>
                                <td style="text-align:center;">- - -</td>
																<td style="text-align:right;font-size: 12px;"><?php echo number_format($post_amount_rec_total,2); ?></td>
																<td style="text-align:right;font-size: 12px;"><?php echo number_format($post_amount_pay_total,2); ?></td>
																<td style="text-align:center;">- - -</td>
                            </tr>
                        </tfoot>
                    </table><!--end table 1-->
                </div> <!--End bodyTab-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
<?php
	}
?>
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script>
	$(window).load(function(){
		$("td.thanColumn").sumColumn("td.thanTotal");
		$("td.thanLengthColumn").sumColumn("td.thanLengthTotal");
		$(".printThis").click(function(){
			var MaxHeight = 500;
			var RunningHeight = 0;
			var PageNo = 1;
			var MaxHeight_after = 0;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight_after = 500;
				}else{
					MaxHeight_after = 600;
				}
				if (RunningHeight + $(this).height() > MaxHeight_after) {
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate    = $(".repoDate").text();
			//remove previous thead/tfoot/ReportDate
			$(".tHeader").remove();
			$(".repoDate").remove();
			$(".tableFooter").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);

			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();
			$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				   loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Unique Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
		});
		$.fn.stDigits = function(){
			return this.each(function(){
				$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			})
		};
		$("td.hasComma").stDigits();
	});
</script>
