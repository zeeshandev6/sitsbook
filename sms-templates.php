<?php
  include('common/connection.php');
  include('common/config.php');
  include('common/classes/sms_templates.php');

  //Permission
	if( !in_array('sms-templates-managment',$permissionz) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

  $objSmsTemplates   = new SmsTemplates();

  if(isset($_POST['get_template'])){
    $id = (int)mysql_real_escape_string($_POST['get_template']);
    $template = $objSmsTemplates->get_template($id);
    $template['SMS_VARS'] = str_ireplace('|','<br>',$template['SMS_VARS']);
    echo json_encode($template);
    exit();
  }

  if(isset($_POST['update'])){
    $id  = (int)mysql_real_escape_string($_POST['update']);
    $sms = mysql_real_escape_string($_POST['text']);
    $objSmsTemplates->update_body($id,$sms);
    exit();
  }

  $sms_template_list = $objSmsTemplates->getList();
?>
    <!DOCTYPE html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style media="screen">
          p.info{
            color: #06C;
          }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/measure-type.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
              $("select").selectpicker();
              $("select[name=template_id]").change(function(){
                var id = parseInt($(this).selectpicker('val'))||0;
                $("div.animaty").fadeOut(function(){
                  $.post("?",{get_template:id},function(data){
                    data = $.parseJSON(data);
                    $("textarea[name=sms-body]").val(data['SMS_BODY']);
                    $("p.info").html(data['SMS_VARS']);
                    $("div.animaty").fadeIn();
                  });
                });
              });
              $("input[name='save']").click(function(){
                var id = parseInt($("select[name='template_id']").selectpicker('val'))||0;
                var sms= $("textarea[name=sms-body]").val();
                if(id==0){
                  displayMessage('Error! Please select a template');
                  return;
                }
                if(sms==''){
                  displayMessage('Error! Please provide valid template body.');
                }
                $.post('?',{update:id,text:sms},function(data){
                  displayMessage('SMS template updated successfully.');
                });
              });
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p>SMS Templates Management</p>
                    <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear" style="height:20px;"></div>
                <div id="bodyTab1">
                  <div id="form">
                    <div class="caption">Templates</div>
                    <div class="field">
                      <select class="form-control" name="template_id">
                        <option value=""></option>
<?php
                        if(mysql_num_rows($sms_template_list)){
                          while($row = mysql_fetch_assoc($sms_template_list)){
?>
                        <option value="<?php echo $row['ID']; ?>"><?php echo $row['TITLE']; ?></option>
<?php
                          }
                        }
?>
                      </select>
                    </div>
                    <div class="clear"></div>


                    <div class="animaty">
                      <div class="caption">SMS Body</div>
                      <div class="field">
                        <textarea name="sms-body" class="form-control" rows="8" cols="40"></textarea>
                      </div>
                      <div class="field">
                        <p class="info col-xs-12"></p>
                      </div>
                      <div class="clear"></div>

                      <div class="caption"></div>
                      <div class="field">
                        <input type="button" name="save" value="Update" class="btn btn-primary" />
                      </div>
                      <div class="clear"></div>

                    </div>

                  </div>
                </div> <!--bodyTab1-->
                <div class="clear" style="height:20px;"></div>
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
    </div><!--bodyWrapper-->
    </body>
    </html>
<?php include('conn.close.php'); ?>
