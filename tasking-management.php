<?php
    $out_buffer = ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/tasking.php');
    include ('common/classes/tasking-docs.php');
    include ('common/classes/customers.php');

	//Permission
	if(!in_array('messages',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

    $objTasking         = new Tasking();
    $objTaskingDocs     = new TaskingDocs();
    $objCustomer        = new Customers();

    if(isset($_POST['status'])){
    	$doc_id = mysql_real_escape_string($_POST['doc_id']);
    	$status = mysql_real_escape_string($_POST['status']);
    	$objTasking->updateTaskingtatus($doc_id,$status);
    	exit();
    }

    if(isset($_POST['deldoc'])){
    	$doc_id   = mysql_real_escape_string($_POST['deldoc']);
    	$file     = $_POST['file'];
    	if($doc_id){
    		if(is_file($file)){
				unlink($file);
			}
			$objTaskingDocs->delete($doc_id);
    	}
    	exit();
    }
    if(isset($_POST['delete_task'])){
    	$task_id = mysql_real_escape_string($_POST['delete_task']);
    	$doc_date= date('d-m-Y',strtotime($_POST['doc_date']));
    	if($task_id){
    		$objTasking->delete($task_id);
    		$rows = $objTaskingDocs->getList($task_id);
    		if(mysql_num_rows($rows)){
    			while($row = mysql_fetch_assoc($rows)){
    				$fileLocation = "uploads/docs/".$doc_date."/".$row['FILE_NAME'];
    				if(is_file($fileLocation)){
    					unlink($fileLocation);
    				}
    			}
    		}
    		$objTaskingDocs->delete_docs($task_id);
    	}
    	exit();
    }

    $mode 	  = "";
	$loadPage = true;

	if(!is_dir("uploads")){
		mkdir("uploads",0777);
	}

	if(!is_dir("uploads/docs/") && count($_FILES['docs']) > 0){
		mkdir("uploads/docs/",0777);
	}

	$dir      = "uploads/docs/";

	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}else{
		exit(header('location:'.$_SERVER['PHP_SELF'].'?mode=list'));
	}

    if(isset($_POST['save'])){
    	$tasking_id      			  	  = mysql_real_escape_string($_POST['task_id']);
		$objTasking->from_user_id 		  = $_SESSION['classuseid'];
        $objTasking->to_user_id 		  = mysql_real_escape_string($_POST['to_user']);
        $objTasking->task_date   		  = date('Y-m-d H:i:s');
        $objTasking->subject              = mysql_real_escape_string($_POST['subject']);
        $objTasking->description          = mysql_real_escape_string($_POST['about']);

		if($tasking_id == 0){
			$tasking_id = $objTasking->save();
			if($tasking_id){
				$action = 'S';
			}else{
				$action = 'E';
			}
		}else{
			$updated = $objTasking->update($tasking_id);
			if($updated){
				$action = 'U';
			}else{
				$action = 'E';
			}
		}

		$datedFolder = $dir.date('d-m-Y')."/";

		if(!is_dir($datedFolder)){
			mkdir($datedFolder,0777);
		}

		if($tasking_id){
			$fcount = 0;
			foreach($_FILES['docs']['name'] as $filename){
				$tmp     = $_FILES['docs']['tmp_name'][$fcount];
				$fcount += 1;
				$title   = basename($filename);
				$ext     = pathinfo($filename,PATHINFO_EXTENSION);
				$target_filename = $tasking_id.md5(microtime()).".".$ext;
				$target  = $datedFolder.$target_filename;
				move_uploaded_file($tmp,$target);

				$objTaskingDocs->task_id 	= $tasking_id;
				$objTaskingDocs->title  	= $title;
				$objTaskingDocs->file_name  = $target_filename;

				$objTaskingDocs->save();
			}
		}

        if($action == 'S'){
            exit(header('location: '.$_SERVER['PHP_SELF'].'?mode=new&action=added&id='.$tasking_id));
        }else{
        	exit(header('location: '.$_SERVER['PHP_SELF'].'?mode=new&action=error&id='.$tasking_id));
        }
    }
	if(isset($_POST['search'])){
		$from_date = $_POST['from-date'];
		$todate    = $_POST['to-date'];
		$subject   = $_POST['subject'];
	}else{
		$from_date  = date('d-m-Y');
		$todate     = date('d-m-Y');
		$subject    = '';
	}
	if($mode != 'new'){
		$from = $_SESSION['classuseid'];
		$to   = '';
	}
	if($mode != 'new'){
		$messageList = $objTasking->search($from,$to,$from_date, $todate, $subject);
	}
	$userList  		    = $objAccounts->getActiveList();

	$users_array = array();

	if(mysql_num_rows($userList)){
		while($user = mysql_fetch_assoc($userList)){
			$users_array[] = $user;
		}
	}

	$taskDetail = NULL;

	if(isset($_GET['id'])){
		$task_id 	= mysql_real_escape_string($_GET['id']);
		$taskDetail = $objTasking->getDetails($task_id);
		$documents 	= $objTaskingDocs->getList($task_id);
	}

?>
    <!DOCTYPE html 
        >
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/deletebyid.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery.nicefileinput.min.js"></script>
        <script type="text/javascript" src="resource/libs/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("input[type=file]").nicefileinput();
                $("select.to_user").selectpicker();
            	$("div.to_user button").focus();
            	tinymce.init({
		            selector: "textarea",
		            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		        });
		        $(".unread").attr({"data-toggle":"tooltip","data-placement":"top","title":"Under Process"});
		        <?php if($mode != 'sent'){ ?>
		        $(".mark-read").click(function(){
		        	$(this).parent().parent().removeClass("unread").attr({"data-toggle":"","data-placement":"","title":""}).tooltip('destroy');;
		        });
		        <?php } ?>
		        $("a[data-del-id]").click(function(){
		        	var doc_id = $(this).attr("data-del-id");
		        	var docDate= $(this).attr("data-date");
		        	$(".doc_idx,.doc_dax").remove();
		        	$("body").append($("<input>", {class:"doc_idx"}));
		        	$("body").append($("<input>", {class:"doc_dax"}));
		        	$(".doc_idx").val(doc_id);
		        	$(".doc_dax").val(docDate);
		        	promptx(delete_docs);
		        });
		        $(".delete_doc").click(function(){
		        	var doc_id = $(this).attr("data-id");
		        	var file   = $(this).attr("data-url");
		        	$(".doc_idx,.doc_dax").remove();
		        	$("body").append($("<input>", {class:"doc_idx"}));
		        	$("body").append($("<input>", {class:"doc_dax"}));
		        	$(".doc_idx").val(doc_id);
		        	$(".doc_dax").val(file);
		        	promptx(delete_doc);
		        });
            });
            $(function(){
			  $('[data-toggle="tooltip"]').tooltip();
			  $(".doc_links").change(function(){
			  	if($(".hover-stripes").length == 0){
			  		$(".doc_links").remove();
			  	}
			  });
			});
			var delete_doc  = function(){
				var doc_id  = $("input.doc_idx").val();
				var file    = $("input.doc_dax").val();
				if(doc_id){
					$.post('tasking-management.php',{deldoc:doc_id,file:file},function(data){
						$("a.delete_doc[data-id='"+doc_id+"']").parent().parent().parent().fadeOut(function(){
							$(this).remove();
						});
						$(".doc_idx,.doc_dax").remove();
					});
				}
			}
			var delete_docs  = function(){
				var doc_id   = $("input.doc_idx").val();
				var doc_date = $("input.doc_dax").val();
				if(doc_id){
					$.post('tasking-management.php',{delete_task:doc_id,doc_date:doc_date},function(data){
						$("a[data-del-id='"+doc_id+"']").parent().parent().fadeOut(function(){
							$(this).remove();
						});
						$(".doc_idx,.doc_dax").remove();
					});
				}
			}
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p> <i class="fa fa-tasks"></i> Tasking</p>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>
                <div class="col-md-12 col-xs-12">
                	<div role="tabpanel">
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs"  role="tablist">
					    <li role="presentation" class="<?php echo $mode=='list'?"active":""; ?>"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?mode=list"> <i class="fa fa-list"></i> List </a></li>
					    <li role="presentation" class="<?php echo $mode=='new'?"active":""; ?>"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?mode=new"> <i class="fa fa-tasks"></i> Assign </a></li>
					  </ul>
					  <!-- Tab panes -->
<?php
				if($mode!='new'){
?>
					  <div class="col-md-12 col-xs-12">
					  	<div style="height: 10px;"></div>
	                	<div class="panel panel-default">
	                		<form method="post" action="tasking-management.php?mode=<?php echo $mode; ?>">
		                		<div id="form">
			                		<div class="caption" style="width: 100px;padding: 5px;">From Date</div>
			                		<div class="field" style="width: 150px;">
			                			<input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="from-date" style="width: 150px;" />
			                		</div>
			                		<div class="caption" style="width: 100px;padding: 5px;">To Date</div>
			                		<div class="field" style="width: 150px;">
			                			<input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="to-date" style="width: 150px;" />
			                		</div>
			                		<div class="clearfix"></div>

			                		<div class="caption" style="width: 100px;padding: 5px;">User : </div>
			                		<div class="field" style="width: 430px;">
			                		<select required name="to_user" class="to_user form-control show-tick" data-live-search="true">
                                    	<option value=""></option>
<?php
										foreach($users_array as $key => $user){
											if($user['ID'] == 1){
												continue;
											}
?>
											<option value="<?php echo $user['ID']; ?>" data-subtext="<?php echo $user['DESIGNATION']; ?>" ><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']; ?></option>
<?php
										}
?>
                                    </select>
                                    </div>
                                    <div class="clear"></div>

			                		<div class="caption" style="width: 100px;padding: 5px;">Subject</div>
			                		<div class="field" style="width: 430px;">
			                			<input type="text" class="form-control" name="subject" style="width: 430px;" />
			                		</div>
			                		<div class="clearfix"></div>

			                		<div class="caption" style="width: 100px;padding: 5px;"></div>
			                		<div class="field" style="width: 150px;">
			                			<input type="submit" name="search" value="Search" class="button" />
			                		</div>
			                		<div class="clearfix"></div>
		                		</div>
	                		</form>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>
	                <div class="clearfix"></div>
<?php
				}
?>
					<div class="col-md-12">
					<div class="clear" style="height:20px;">&nbsp;</div>
					<div class="panel panel-default">
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane fade in active" id="home">
<?php
				if($mode != 'new'){
?>
					    	<table cellspacing="0" width="100%" class="table-hover" >
		                        <thead>
		                        <tr>
		                            <th width="10%" style="text-align:center">Date</th>
		                            <th width="12%" style="text-align:center"><?php echo $mode=='list'?"To":""; ?></th>
		                            <th width="23%" style="text-align:center">Subject</th>
		                            <th width="5%" style="text-align:center">Action</th>
		                        </tr>
		                        </thead>

		                        <tbody>
		                        <?php
		                        if(mysql_num_rows($messageList)){
		                            while($notificationDetails = mysql_fetch_assoc($messageList)){
		                            	if($mode=='inbox'){
		                            		$sender_detail = $objAccounts->getDetails($notificationDetails['FROM_USER']);
		                            	}else{
		                            		$sender_detail = $objAccounts->getDetails($notificationDetails['TO_USER']);
		                            	}
										if($notificationDetails['TASK_STATUS'] != 'C'){
											$message_read_class = 'unread';
										}else{
											$message_read_class = 'success';
										}
								?>
		                                <tr class="<?php echo $message_read_class; ?>">
		                                    <td style="text-align:center"><?php echo date('d-m-Y', strtotime($notificationDetails['TASK_DATE'])); ?></td>
		                                    <td style="text-align:left; padding-left: 20px;"><?php echo $sender_detail['FIRST_NAME']." ".$sender_detail['LAST_NAME']; ?></td>
		                                    <td style="text-align:left; padding-left: 20px;"><?php echo $notificationDetails['SUBJECT']; ?></td>
		                                    <td style="text-align:center">
		                                        <a href="tasking-management.php?id=<?php echo $notificationDetails['ID']; ?>&mode=new" class="button mark-read btn-xs">View</a>
		                                        <a data-del-id="<?php echo $notificationDetails['ID']; ?>" data-date="<?php echo $notificationDetails['TASK_DATE']; ?>" class="btn btn-danger btn-xs"> <i class="fa fa-times"></i> </a>
		                                    </td>
		                                </tr>
		                            <?php
		                            }
		                        }else{
		                            ?>
		                            <tr id="recordPanel">
		                                <td style="text-align:center" colspan="6"> No Record Found!</td>
		                            </tr>
		                        <?php
		                        }
		                        ?>
		                        </tbody>
		                    </table>
<?php
				}else{
					if($loadPage==true){
						if(isset($_GET['action'])){
?>
							<div class="col-xs-12 col-md-12">
								<?php
			                        if($_GET['action']=='updated'){
			                            ?>
			                            <div id="msg" class="alert alert-success">
			                                <button type="button" class="close">&times;</button>
			                                <span style="font-weight: bold;">Success!</span> Message Updated.
			                            </div>
			                        <?php
			                        }elseif($_GET['action']=='added'){
			                            ?>
			                            <div id="msg" class="alert alert-success">
			                                <button type="button" class="close">&times;</button>
			                                <span style="font-weight: bold;">Success!</span> Task Assigned.
			                            </div>
			                        <?php
			                        }
			                    ?>
							</div>
						<?php
						}
	                ?>
	                <?php
	                	if($taskDetail['TASK_STATUS'] == 'C'){
	                		?>
	                			<div id="msg" class="alert alert-info" style="margin: 20px;">
							        <button type="button" class="close">&times;</button>
							        "This task has been completed."
							    </div>
	                		<?php
	                	}
	                ?>
	                <?php
	                	if($taskDetail['TASK_STATUS'] == 'P'){
	                		?>
	                		<div id="msg" class="alert alert-warning" style="margin: 20px;">
						        <button type="button" class="close">&times;</button>
						        "This task has not been completed."
						    </div>
	                		<?php
	                	}
	                ?>
						<form method="post" action="" enctype="multipart/form-data">
                            <div id="form">
                            	<input type="hidden" name="task_id" value="<?php echo $taskDetail['ID']==''?0:$taskDetail['ID']; ?>" />
                                <div class="caption">User Name :</div>
                                <div class="field">
                                    <select required name="to_user" class="to_user form-control show-tick" data-live-search="true">
                                    	<option value=""></option>
<?php
										foreach($users_array as $key => $user){
											if($_SESSION['classuseid']==$user['ID']){
												continue;
											}
											$selected = ($taskDetail['TO_USER'] == $user['ID'])?"selected":"";
?>
											<option <?php echo $selected; ?> value="<?php echo $user['ID']; ?>" data-subtext="<?php echo $user['DESIGNATION']; ?>" ><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']; ?></option>
<?php
										}
?>
                                    </select>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Date :</div>
                                <div class="field">
                                    <input type="text" id="datepicker" readonly="readonly" class="form-control date" name="date" value="<?php echo ($taskDetail!=NULL)?date('d-m-Y',strtotime($taskDetail['TASK_DATE'])):date('d-m-Y'); ?>" style=" width:57%" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Subject :</div>
                                <div class="field" style="width:690px;">
                                    <input type="text" class="form-control" name="subject" value="<?php echo $taskDetail['SUBJECT'] ?>" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption">Description :</div>
                                <div class="field" style="width:700px;">
                                    <textarea name="about" ><?php echo $taskDetail['DESCRIPTION'] ?></textarea>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Attachments :</div>
                                <div class="field" style="width:690px;">
                                    <input type="file" class="nice" name="docs[]" multiple />
                                </div>
                                <div class="clear"></div>

<?php
                                if(isset($documents) && mysql_num_rows($documents)){
?>
							<div class="caption"></div>
                            <div class="field"  style="width:550px;">
                                <div class="panel panel-default doc_links" style="padding: 1em;text-align: justify;">
<?php
                                    while($document = mysql_fetch_assoc($documents)){
                                        $icondir  = "resource/images/mime-types/";
                                        $iconfile = $icondir.strtolower(pathinfo($document['FILE_NAME'],PATHINFO_EXTENSION)).".png";
?>
                                    <div class="hover-stripes">
                                        <div class="col-xs-2">
                                        	<img src="<?php echo $iconfile; ?>" style="height:25px;" />
                                        </div>
                                        <div class="col-xs-8">
                                        	<?php echo $document['TITLE']; ?>
                                        </div>
                                        <div class="col-xs-2">
                                        	<div class="btn-group">
	                                        	<a href="<?php echo "uploads/docs/".date('d-m-Y',strtotime($taskDetail['TASK_DATE']))."/".$document['FILE_NAME']; ?>" download="<?php echo $document['TITLE']; ?>" class="button btn-xs" >
	                                        		<i class="fa fa-download"></i>
	                                        	</a>
	                                        	<a data-id="<?php echo $document['ID']; ?>" data-url="<?php echo "uploads/docs/".date('d-m-Y',strtotime($taskDetail['TASK_DATE']))."/".$document['FILE_NAME']; ?>" download="<?php echo $document['TITLE']; ?>" class="btn btn-danger btn-xs delete_doc">
	                                        		<i class="fa fa-times"></i>
	                                        	</a>
	                                        </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="pull-left" style="height:10px;"></div>
                                        <div class="clear"></div>
                                    </div>
<?php
                                    }
?>
                                </div>
                            </div>
                            <div class="clear"></div>
<?php
                                }
?>

                                <div class="caption"></div><br />
                                <?php
                                if(!isset($_GET['id'])){
                                    ?>
                                    <button type="submit" name="save"   value="Send"     class="btn btn-primary btn-sm btn0bg"> <i class="fa fa-check"></i> Send </button>
                                <?php
                                }else{
                                    ?>
                                    <input type="submit" name="save"   value="Update"   class="btn btn-default" />
                                    <input type="button" name="print"  value="Print"    class="btn btn-default" />
                                    <input type="button" name="new"    value="New Form" class="btn btn-default" onclick="window.location.href='tasking-management.php?mode=new';" />
                                <?php
                                }
                                ?>
                                <div style = "clear:both; height:20px"></div>
                            </div>
                        </form>
<?php
					}//Load Page if true end
				}
?>
					    </div>
					  </div>
					  </div>
					  </div>
					</div>
                </div>
                <div id = "bodyTab1">

                </div> <!--bodyTab1-->

                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Notification</div>

                            <div class="caption">From Date :</div>
                            <div class="field">
                                <input type="text" id="datepicker" name="fromdate" class="form-control date" style=" width:57%" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">To Date :</div>
                            <div class="field">
                                <input type="text" name="todate" class="form-control date datepicker" style=" width:57%" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"> Subject :</div>
                            <div class="field">
                                <input type="text" name="subject" class="form-control" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input type="submit" class="button" name="search" value="Search" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab2-->
                <div style = "clear:both; height:20px"></div>




                      <!-- Delete confirmation popup -->
                <div id="myConfirm" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->




            </div>     <!-- End summer -->
        </div>   <!-- End .content-box-top -->
    </div>
    <div id="fade"></div>
    </body>
    </html>
<?php include('conn.close.php'); ?>
