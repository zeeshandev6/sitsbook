<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     = new ChartOfAccounts;
	$objSales 			 = new Sale;
	$objSaleDetails      = new SaleDetails;
	$objItems            = new Items;
	$objItemCategory     = new itemCategory;
	$objTaxRates         = new TaxRates;
	$objDepartments      = new Departments;
	$objConfigs 		 = new Configs();

	$suppliersList   = $objAccountCodes->getAccountByCatAccCode('010104');
	$cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');

	$itemsCategoryList = $objItemCategory->getList();
	$taxRateList     = $objTaxRates->getList();

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	if(isset($_POST['search'])){

		$objSales->fromDate 		= ($_POST['fromDate']=='')?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$objSales->toDate   		= ($_POST['toDate']=='')?"":date('Y-m-d',strtotime($_POST['toDate']));
		$objSales->mobile_no  		= $_POST['mobile_no'];
		$objSales->billNum			= '';

		$supplierTypeReport   =  ($objSales->supplierAccCode == '')?false:true;
		$itemTypeReport =  ($objSales->item == '')?false:true;

		//both selected
		if(($objSales->supplierAccCode != '') && ($objSales->item != '')){
			$supplierTypeReport = true;
		}

		$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objSales->supplierAccCode);
		$theItemTitle      = $objItems->getItemTitle($objSales->item);

		if($supplierTypeReport){
			$titleRepo = $supplierHeadTitle;
		}elseif($itemTypeReport){
			$titleRepo = $theItemTitle;
		}else{
			$titleRepo = '';
		}
		if($objSales->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objSales->fromDate = $startThisMonth;
		}

		$purchaseReport = $objSales->customer_sale_report();
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 750;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){

					if(PageNo == 1){
						MaxHeight = 750;
					}

					if(PageNo != 1){
						MaxHeight = 820;
					}

					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){

					if(i == 1){
						MaxHeight = 750;
					}

					if(i != 1){
						MaxHeight = 820;
					}
					$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
					  debug: false,
					  importCSS: false,
					  printContainer: true,
					  loadCSS: 'resource/css/reports.css',
					  pageTitle: "Sitsbook.com",
					  removeInline: false,
					  printDelay: 500,
					  header: null
				});
			});
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sales Reports</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="">
                        <div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>
						<div class="caption">Mobile</div>
                        <div class="field" style="width:150px;">
                        	<input name="mobile_no" value="" class="form-control" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
<?php
					if(isset($purchaseReport)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;"></p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">

                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                            <div class="clear"></div>
                    	</div>
<?php
						$prevBillNo = '';
						$total_amount = 0;
						$count = 1;
						if(mysql_num_rows($purchaseReport)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="10%" style="font-size:12px;text-align:center">Sr #</th>
                                   <th width="20%" style="font-size:12px;text-align:center">Mobile #</th>
                                   <th width="40%" style="font-size:12px;text-align:center">Customer Title</th>
                                   <th width="20%" style="font-size:12px;text-align:center">Customer Type</th>
                                   <th width="10%" style="font-size:12px;text-align:center">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
							while($row = mysql_fetch_assoc($purchaseReport)){
								if(substr($row['CUST_ACC_CODE'], 0,6) == '010101'){
									$customer_name = $row['CUSTOMER_NAME'];
									$customer_type = "Walk In Customer";
								}else{
									$customer_name = $objAccountCodes->getAccountTitleByCode($row['CUST_ACC_CODE']);
									$customer_type = "Regular Customer";
								}
?>
                            	<tr>
                            		<td style="text-align:center;"><?php echo $count; ?></td>
                                    <td style="text-align:center;"><?php echo $row['MOBILE_NO']; ?></td>
                                    <td style="text-align:center;"><?php echo $customer_name; ?></td>
                                    <td style="text-align:center;"><?php echo $customer_type; ?></td>
                                    <td style="text-align:right;"><?php echo number_format($row['PURCHASES'],2,'.',','); ?></td>
                                </tr>
<?php
								$total_amount += (float)$row['PURCHASES'];
								$count++;
							}//end while
?>
                            </tbody>
<?php
						}//end if
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:center;font-weight:bold;" colspan="4"> TOTAL </td>
                                <td style="text-align:right;font-weight:bold;"> <?php echo number_format($total_amount,2,'.',','); ?> </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
