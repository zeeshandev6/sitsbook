<?php
	include('common/connection.php');
	include('common/classes/userAccounts.php');
	include('common/classes/md.php');
	include('common/classes/company_details.php');
	include('common/classes/settings.php');

	$objAccounts        = new UserAccounts();
	$objCompanyDetails  = new CompanyDetails();
	$objConfigs         = new Configs();

	$use_branches       = $objConfigs->get_config('USE_BRANCHES');
	$barcode_print_flag = $objConfigs->get_config('BARCODE_PRINT');
	$barcode_print_flag = ($barcode_print_flag == 'Y')?true:false;
	$sessionUserDetails = $objAccounts->getDetails($user_id);
	$fullName           = $objAccounts->getFullName($user_id);
	$usersFullName      = $fullName['FIRST_NAME']."".$fullName['LAST_NAME'];
	$userNameDiv        = '<div class="userNameBanner">'.$usersFullName.'</div>';
	$permissionz        = $objAccounts->getPermissions($user_id);
	$permissionz        = explode('|',$permissionz);

	if($sessionUserDetails['CASH_IN_HAND'] == 'Y'){
		$cash_in_hand_acc_code  = $sessionUserDetails['CASH_ACC_CODE'];
		$cash_in_hand_acc_title = "Cash In Hand A/c ".$sessionUserDetails['FIRST_NAME']." ".$sessionUserDetails['LAST_NAME'];
	}else{
		$cash_in_hand_acc_code  = '0101010001';
		$cash_in_hand_acc_title = 'Cash In Hand';
	}
	$cash_in_hand_list = $objAccounts->getFullListCashInHand();
	$cash_in_bank_list = $objAccounts->getAccountByCatAccCode('010102');

	$thisFile = basename($_SERVER['PHP_SELF']);
	$companyTitle = $objCompanyDetails->getTitle('1');
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<style>
		a.poplight{
			color:#666;
		}
		h3{
			font-size: 14px;
		}
	</style>
	<script type = "text/javascript" src = "resource/scripts/jquery.1.11.min.js"></script>
	<script type = "text/javascript" src = "resource/scripts/jquery-ui.min.js"></script>
	<script type = "text/javascript" src = "resource/scripts/bootstrap.min.js"></script>
	<script type = "text/javascript" src = "resource/scripts/configuration.js"></script>
	<script type = "text/javascript" src = "resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
		<div id="bodyWrapper">
			<div class = "content-box-top"  style="padding-bottom: 30px;">
				<div class="summery_body">
					<div class = "content-box-header">
						<p> <i class="fa fa-cog"></i> System Configuration</p>
					</div><!-- End .content-box-header -->
					<div class="clear"></div>
					<?php
					if(in_array('company-profile',$permissionz) || $admin){
						?>
						<a href="company-detail.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="<?php echo isset($logoPath)?$logoPath:''; ?>" class="panelIcon hvr-outline-in"  />
								<h3>Company Profile</h3>
							</div>
						</a>
						<?php
					}
					if($admin){
						?>
						<a href="users-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/user-add.png" class="panelIcon hvr-outline-in" />
								<h3>User Accounts</h3>
							</div>
						</a>
						<?php
					}
					?>
					<a href="change-password.php">
						<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
							<img src="resource/images/MedIcons/safe.png" class="panelIcon hvr-outline-in" />
							<h3>Change Password</h3>
						</div>
					</a>
					<div class="clear"></div>

<?php
				if($turn_off_operational_forms == 'N'){
?>

					<hr />
					<?php
					if($use_branches == 'Y'){
						if(in_array('branch-managment',$permissionz) || $admin){
							?>
							<a href="branch-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/big-gear.png" class="panelIcon hvr-outline-in" />
									<h3>Branches</h3>
								</div>
							</a>
							<?php
						}
					}
					if($salePurchaseModule=='Y'||$inwardOutwardAddon=='Y'||$mobileAddon=='Y'||$distSalesAddon=='Y'){
						if(in_array('measure-managment',$permissionz) || $admin){
							?>
							<a href="measure-type-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/measurements.png" class="panelIcon hvr-outline-in" />
									<h3>Measure</h3>
								</div>
							</a>
							<?php
						}
						if(in_array('category-managment',$permissionz) || $admin){
							?>
							<a href="item-category-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/item_categories.png" class="panelIcon hvr-outline-in" />
									<h3>Category</h3>
								</div>
							</a>
							<?php
						}
						if(in_array('items-managment',$permissionz) || $admin){
							?>
							<a href="items-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/items.png" class="panelIcon hvr-outline-in" />
									<h3>Items</h3>
								</div>
							</a>
							<?php
						}
					}
					if($use_branches == 'Y'){
						if(in_array('stock-managment',$permissionz) || $admin){
							?>
							<a href="stock-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/storage.jpg" class="panelIcon hvr-outline-in" />
									<h3>Branch Stock Transfer</h3>
								</div>
							</a>
							<?php
						}
					}
					if($embProdAddon == 'Y'){
						if(in_array('shed-management',$permissionz) || $admin){
							?>
							<a href="sheds-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/sheds.png" class="panelIcon hvr-outline-in" />
									<h3>Sheds</h3>
								</div>
							</a>
							<?php
						}
					}
					?>
					<div class="clear"></div>
					<hr />
					<?php
					if(in_array('banks-managment',$permissionz) || $admin){
						?>
						<a href="banks-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/banks.png" class="panelIcon hvr-outline-in" />
								<h3>Banks</h3>
							</div>
						</a>
						<?php
					}
					if(in_array('suppliers-management',$permissionz) || $admin){
						?>
						<a href="suppliers-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/supplier.png" class="panelIcon hvr-outline-in" />
								<h3>Suppliers</h3>
							</div>
						</a>
						<?php
					}
					if(in_array('customer-management',$permissionz) || $admin){
						?>
						<a href="customer-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/customer.png" class="panelIcon hvr-outline-in" />
								<h3>Customers</h3>
							</div>
						</a>
						<a href="edit-area-sector.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/location.png" class="panelIcon hvr-outline-in" />
								<h3>Areas/Sectors</h3>
							</div>
						</a>
						<div class="clear"></div>
						<hr />
						<div class="clear"></div>
						<?php
					}
					if(in_array('brokers-managment',$permissionz) || $admin){
					?>
						<a href="brokers-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/broker.png" class="panelIcon hvr-outline-in" />
								<h3>Brokers</h3>
							</div>
						</a>
					<?php
					}
					if(in_array('employees-managment',$permissionz) || $admin){
						?>
						<a href="payroll-management.php">
							<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<img src="resource/images/MedIcons/payrolls.png" class="panelIcon hvr-outline-in" />
								<h3>Employee</h3>
							</div>
						</a>
						<?php
					}
						?>
						<?php
						if(in_array('service-managment',$permissionz) || $admin){
							?>
							<a href="services-management.php">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/items_manage.png" class="panelIcon hvr-outline-in" />
									<h3>Services</h3>
								</div>
							</a>
							<?php
						}
						if($embSalesAddon == 'Y'||$embProdAddon == 'Y'){
							if(in_array('machines-managment',$permissionz) || $admin){
								?>
								<a href="machine-management.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/mechanics.png" class="panelIcon hvr-outline-in" />
										<h3>Machines</h3>
									</div>
								</a>
								<?php
							}
						}
						if($embProdAddon == 'Y'){
							if(in_array('emb-product-managment',$permissionz) || $admin){
								?>
								<a href="emb-product-management.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/products.png" class="panelIcon hvr-outline-in" />
										<h3>Products</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('emb-design-managment',$permissionz) || $admin){
								?>
								<a href="emb-design-list.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/design.png" class="panelIcon hvr-outline-in" />
										<h3>Designs</h3>
									</div>
								</a>
								<?php
							}
						}
						if(in_array('currency-managment',$permissionz) || $admin){
							?>
							<a href="currency-type-management.php" target="_blank">
								<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<img src="resource/images/MedIcons/currency.png" class="panelIcon hvr-outline-in" />
									<h3>Currency Types</h3>
								</div>
							</a>
							<?php
						}
						?>
						<?php if($barcode_print_flag){ ?>
							<?php
							if(in_array('brage-managment',$permissionz) || $admin){
								?>
								<a href="item-barcode-generate.php" target="_blank">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/scanner.png" class="panelIcon hvr-outline-in" />
										<h3>Barcode By Range</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('bname',$permissionz) || $admin){
								?>
								<a href="bulk-item-barcodes-print.php" target="_blank">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/scanner.png" class="panelIcon hvr-outline-in" />
										<h3>Barcode By Items</h3>
									</div>
								</a>
								<?php
							}
							?>
							<?php   } //end if_ $barcode_print_flag ?>
							<?php
							if(in_array('coa-managment',$permissionz) || $admin){
								?>
								<a href="accounts-management.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/chart.png" class="panelIcon hvr-outline-in" />
										<h3>Chart of Accounts</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('linked-coa',$permissionz) || $admin){
								?>
								<a href="linked-accounts.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/chart.png" class="panelIcon hvr-outline-in" />
										<h3>Linked Accounts</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('ea-managment',$permissionz) || $admin){
								?>
								<a href="expense-accounts.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/MedIcons/chart.png" class="panelIcon hvr-outline-in" />
										<h3>Expense Accounts</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('sms-templates-managment',$permissionz) || $admin){
								?>
								<a href="sms-templates.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/setting.png" class="panelIcon hvr-outline-in" />
										<h3>SMS Templates</h3>
									</div>
								</a>
								<?php
							}
							if(in_array('misc-setting-managment',$permissionz) || $admin){
								?>
								<a href="misc-settings.php">
									<div class="panel1 hvr-underline-from-center col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<img src="resource/images/setting.png" class="panelIcon hvr-outline-in" />
										<h3>Misc. Settings</h3>
									</div>
								</a>
								<?php
							}
							?>
						<div style=" clear:both;"></div>
<?php } ?>
					</div><!--summery_body-->
				</div><!-- End .content-box -->
			</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
	</body>
	</html>
	<?php include('conn.close.php'); ?>
