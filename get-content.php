<?php
	header('Access-Control-Allow-Origin: *');  
	header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Get Content</title>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript">
		var pg = 1;
		var max= 15;
		//line numbers
		//1596 first paid
		$(function(){
			//make_csvs();
		});
		var make_csvs = function(){
			var name = '';
			var link = '';
			var pr   = '';
			var rows = '';
			if($(".dir-tb-lst").length){
				$(".dir-tb-lst").each(function(){
					name = $(this).find(".lst-con-sec a").text();
					link = $(this).find(".lst-con-sec a").attr('href');
					pr   = $(this).find(".lst-con-trd").text();
					rows+= $.trim(name)+','+link+','+pr+'\n';
				});
				$("body").html('<pre>'+rows+'</pre>');
			}
		};
		var getcontent = function(){
			//return;
			var pg = parseInt($(".pg").val())||0;
			$.get("http://www.directorycritic.com/article-directory-list.html",{pg:pg},function(data){
				$(".span12").append($(data).find(".row-fluid.dir-table .span12").html());
				$("#dir-tb-had").remove();
				++pg;
				$(".pg").val(pg);
				$("button").text("Load Page"+pg);
				if(pg <= max){
					$("button").click();
				}
				if(pg > max){
					make_csvs();
				}
			});
		};
	</script>
</head>
<body>
	<input type="hidden" class="pg" value="1" />
	<button data-id="1" onclick="getcontent();">Load Page 1</button>
	<div class="span12">		
	</div>
</body>
</html>
<?php include('conn.close.php'); ?>