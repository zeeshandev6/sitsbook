<?php
	include('common/connection.php');
	include('common/classes/emb_outward.php');
	include('common/classes/emb_inward.php');
	include('common/classes/ledger_report.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/emb_products.php');
  include('common/classes/design.php');
	include('common/classes/company_details.php');

	$objLedgerReport 				= new ledgerReport();
	$objOutwards 						= new outward();
  $objDesign   						= new design();
	$objInwards  						= new EmbroideryInward();
	$objLotRegisterDetails  = new EmbLotRegister();
	$objEmbProducts  				= new EmbProducts();
	$objCompanyDetails 			= new CompanyDetails();

  $outward_get_id = isset($_GET['id'])?mysql_real_escape_string($_GET['id']):0;

  $invoiceDesignCodes = $objOutwards->getDesignsList($outward_get_id);

  $design_image_dir = 'uploads/designs/';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
          $design_image_dir = 'uploads/designs/';
          if(isset($invoiceDesignCodes) && mysql_num_rows($invoiceDesignCodes)){
    ?>
                        <div style="width: 100%;">
    <?php
            while($design_code = mysql_fetch_array($invoiceDesignCodes)){
              $file_name = $objDesign->getImageByCode($design_code['DESIGN_CODE']);
              $image_link = $design_image_dir.$file_name;
    ?>
    <?php
                        if($file_name != ''){
    ?>
                                <img class="img-thumbnail" src="<?php echo $image_link; ?>" style="width: 100%;margin-top:15px; page-break-inside: avoid;" />
    <?php
                        }
    ?>
    <?php
            }
    ?>
    <?php
          }
    ?>
  </body>
</html>
