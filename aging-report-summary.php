<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/aging-report.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/accounts.php';

	//Permission
	if(!in_array('aging-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAgingSummaryReport = new AgingReportSummary;
	$objJournalVoucher	   = new JournalVoucher;
	$objAccounts 		   = new ChartOfAccounts;

	if(isset($_POST['report'])){
		$toDate   = ($_POST['toDate']!=="")?date('Y-m-d',strtotime($_POST['toDate'])):"";
	}
	$firstDayOfTheMonth = date('01-m-Y');

	$customersForAging = $objAccounts->getAccountByCatAccCode('010104');
?>

<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      	<!-- jQuery -->
      	<script type = "text/javascript" src = "resource/scripts/jquery.1.11.min.js"></script>
        <script type = "text/javascript" src = "resource/scripts/jquery-ui.min.js"></script>
				<script type = "text/javascript" src = "resource/scripts/bootstrap.min.js" ></script>
      	<script type = "text/javascript" src = "resource/scripts/tab.js"></script>
      	<script type = "text/javascript" src = "resource/scripts/configuration.js" ></script>
        <script type = "text/javascript" src = "resource/scripts/sideBarFunctions.js" ></script>
        <script type = "text/javascript" src = "resource/scripts/printThis.js"></script>
        <script type = "text/javascript">
            $(function(){
                $(window).load(function(){
                    $("tbody.inspective tr").each(function(i,e){
                        if((parseFloat($(this).find("td").eq(1).text())||0) == 0){
                            $(this).remove();
                        }
                    });
                });
            });
        </script>
   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >Aging Report Summary</p>
                            <div class="clear"></div>
                  		</div><!-- End .content-box-header -->

                    	<div id = "bodyTab1">
                            <div class="clear"></div>
                            <div id="form">
                                <form method="post" action="">
                                    <div class="caption">Current Date:</div>
                                    <div class="field" style="width:150px">
                                        <input type="text" name="toDate" value="<?php echo (isset($toDate))?date('d-m-Y',strtotime($toDate)):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px" />
                                    </div>
                                    <div class="caption" style="width:100px"><input type="submit" value="Generate" name="report" class="btn btn-default" style="font-size:14px !important;" /></div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->
            		</div> <!-- End .content-box-content -->
					<?php
                    	if(isset($_POST['report'])){
                    ?>
        			<div class="content-box-content">
                    	<span style="float:right;"><button class="button printThis">Print</button></span>
                        <div class="clear"></div>
                		<div id="bodyTab" class="printTable loadingContent" style="margin:0px auto; width:98%;min-height: 400px;">
                        	<header>
                            	<div style="text-align:left;">

                            		<p style="font-size:18px;padding:0;"><?php echo $companyTitle; ?></p>
                                	<p style="font-size:18px;padding:0;">Account Receivable Aging Report</p>
                                    <p class="repoGen" style="display: none;">Report generated on : <?php echo date('d-M-Y'); ?></p>
                            	</div>
                            	<div style="clear:both; height:1;"></div>
                            </header>
                            <table class="tableBreak" style="display:none; margin:0px auto; width:100%;" cellspacing="0" align="center">

                            	<thead class="tHeader">
                                    <tr>
                                       <th style="width:28%;text-align:center;">Customer Name</th>
                                       <th style="width:8%;text-align:center;">Recoverable</th>
                                       <th style="width:8%;text-align:center;"><?php echo date('d-M',strtotime($toDate)); ?></th>
                                       <th style="width:8%;text-align:center;">15 D</th>
                                       <th style="width:8%;text-align:center;">30 D</th>
                                       <th style="width:8%;text-align:center;">45 D</th>
                                       <th style="width:8%;text-align:center;">60 D</th>
                                       <th style="width:8%;text-align:center;">75 D</th>
                                       <th style="width:10%;text-align:center;">90 &amp;Above</th>

                                    </tr>
                                </thead>
<?php

								$periodRange = 15;
								$columnTotals = array();
								$recoverableTotal = 0;
								if(mysql_num_rows($customersForAging)){
?>
								<tbody class="inspective">
<?php
									while($customer = mysql_fetch_array($customersForAging)){
										$balanceToday = $objAgingSummaryReport->openingBalanceX($toDate,$customer['ACC_CODE']);
										$recoverableTotal += $balanceToday;
?>
                                    <tr>
                                       <td style="text-align:right;"><?php echo substr($customer['ACC_TITLE'], 0,22); ?></td>
                                       <td style="text-align:right;"><?php echo number_format($balanceToday,2); ?></td>
<?php
								$i = 0;
								$prevCustBal = '0';
								$difference = array();
								$prevAgeDate  = 'X';
								while($i <= 90){
									$amountToShow = 0;
									$agedDate = date('Y-m-d',strtotime($toDate.' - '.$i.' days'));
									if($i > 0){
										$endDate = $prevAgeDate;
									}else{
										$endDate = $toDate;
									}
									$customerBalance = $objAgingSummaryReport->openingBalanceX($agedDate,$customer['ACC_CODE']);
									$currentDate     = date('Y-m-d',strtotime($toDate.' + 1  days'));
									$currentDebits   = $objAgingSummaryReport->getCustomerDebitsForPeriod($customer['ACC_CODE'],$agedDate,$currentDate);
                                    //$currentCredits  = $objAgingSummaryReport->getCustomerCreditsForPeriod($customer['ACC_CODE'],$agedDate,$currentDate);
                                    //$currentDebits -= ((float)($currentCredits));
									if($i == 90){
										//reserve aged date for use after customer debits at end of 90 days
										$prevAgeDate = $agedDate;
										$agedDate    = '';
									}
									$customerDebits  = $objAgingSummaryReport->getCustomerDebitsForPeriod($customer['ACC_CODE'],$agedDate,$endDate);
                                    //$customerCredits = $objAgingSummaryReport->getCustomerCreditsForPeriod($customer['ACC_CODE'],$agedDate,$endDate);
                                    //$customerDebits -= ((float)($customerCredits));
									if($i == 90){
										//reserved date to reset
										$agedDate = $prevAgeDate;
									}

									if($i == 0){
										$customerDebits = $currentDebits;
									}

									if($i > 0){
										$difference[$i] =  $prevCustBal - $customerDebits;
									}else{
										$difference[$i] =  $balanceToday - $customerDebits;
									}

									if($difference[$i] < $customerDebits){
										$amountToShow = $prevCustBal;
									}

									if($difference[$i] > 0){
										$amountToShow = $customerDebits;
									}

									if($difference[$i] > $customerDebits){
										$amountToShow = $customerDebits;
									}

									if($i == 0 && $currentDebits != 0 && $balanceToday != 0){
										if($currentDebits > $customerBalance){
											$amountToShow = $currentDebits;
										}else{
											$amountToShow = $currentDebits;
										}
									}

									if($amountToShow < 0){
										$amountToShow = 0;
									}

									$prevCustBal = $difference[$i];
?>
										<td style="text-align:right;background-color:#FFF;"><?php echo number_format($amountToShow,2); ?></td>
<?php
										//$amountToShow = (float) preg_replace('/\D/', '', $amountToShow);
										if(isset($columnTotals[$i])){
											$columnTotals[$i] += $amountToShow;
										}else{
											$columnTotals[$i]  = $amountToShow;
										}
										$prevAgeDate = $agedDate;
										$i += $periodRange;
								}
?>
                                    </tr>
<?php
									}
?>
                                    <tr style="background:none">
                                       <td style="text-align:right;font-weight:bold">Totals:</td>
                                       <td style="text-align:right;font-weight:bold;border-top:1px solid #2D2D2D"> <?php echo number_format($recoverableTotal,2); ?> </td>
<?php
								if($columnTotals != NULL){
									foreach($columnTotals as $key => $value){
?>
									   <td style="text-align:right;font-weight:normal;border-top:1px solid #2D2D2D;padding-right:5px;"><?php echo number_format($value,2); ?></td>
<?php
									}
								}
?>
                                    </tr>
                                </tbody>
<?php
								}
?>
	                        </table>
                        </div><!--bodyTab2-->
						<?php
                           	}
                        ?>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(window).on('load', function(){
		$(".loadingContent").removeClass("loadingContent");
		$(".tableBreak").show();
		$(".printThis").click(function(){
			var MaxHeight = 750;
			var RunningHeight = 0;
			var PageNo = 1;
			var MaxHeight_after = 0;

			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight = 750;
				}
				if(PageNo != 1){
					MaxHeight = 850;
				}
				if (RunningHeight + $(this).height() > MaxHeight){
					RunningHeight = 0;
					PageNo += 1;
				}
				if($(this).height() > 29){

				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var repoDate = $(".repoGen").text();
			$(".repoGen").remove();
			//remove previous thead/tfoot
			$(".tHeader").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){

				if(i == 1){
					MaxHeight = 750;
				}
				if(i != 1){
					MaxHeight = 850;
				}

				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}

			$('table.tableBreak').remove();

			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports.css',
				pageTitle: "Sitsbook.com",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
	});
</script>
