<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/j-voucher.php');
	include('common/classes/company_details.php');

	$objCompanyDetails   = new CompanyDetails();
	$objSales 		     	 = new Sale();
	$objSaleDetails      = new SaleDetails();
	$objCustomers        = new Customers();
	$objItems  	         = new Items();
	$objServices       	 = new Services();
	$objJournalVoucher   = new JournalVoucher();
	$objConfigs   	     = new Configs();

	$print_meth     	 	 = $objConfigs->get_config('DIRECT_PRINT');
	$currency_type 		 	 = $objConfigs->get_config('CURRENCY_TYPE');
	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;
	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
	$use_taxes           = ($use_taxes=='Y')?true:false;
	$invoice_format 	   = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format 	   = explode('_', $invoice_format);

	if(isset($_GET['id'])){
		$sale_id 			     		 = mysql_real_escape_string($_GET['id']);
		$saleDetails 		   		 = mysql_fetch_array($objSales->getRecordDetails($sale_id));
		$saleDetailList 	     = $objSaleDetails->getList($sale_id);
		$customer_balance 	   = $objJournalVoucher->getInvoiceBalance($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID']);
		$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
	}

	/// INVOICE_FORMAT Config Size - Header - Duplicate
	$invoice_num 		   = $invoice_format[1];
	$invoiceStyleCss 	 = 'resource/css/invoiceStyleLanscape.css';
	$duplicates 		   = 'Y';
	$max_counter 		   = 30;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			<?php
				if($duplicates=='Y'){
				?>
				var copy = $(".invoiceLeftPrint").html();
				$("<div class='invoiceLeftPrint last'>"+copy+"</div>").insertAfter($(".invoiceLeftPrint"));

			<?php
				}
				?>
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "<?php echo $invoiceStyleCss; ?>",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			$(window).keyup(function(e){
				if(e.ctrlKey && e.keyCode == 17){
					$(".printThis").click();
					e.preventDefault();
					return false;
				}
			});
			$(".ledger_view").click(function(){
				var gl_date = $(this).attr('data-date');
				var gl_code = $(this).attr('data-code');
				if(gl_code != ''){
					$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
						if(data == ''){
							var win = window.open('invoice-ledger-print.php','_blank');
							if(win){
							    win.focus();
							}else{
							    displayMessage('Please allow popups for This Site');
							}
						}
					});
				}
			});
			$(window).keyup(function(e){
				if(e.keyCode == 27){
					$(".cancel-button").click();
				}
			});
		});
		$(window).load(function(){
			$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	$balance_os_show = true;
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) != '010104'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $saleDetails['CUSTOMER_NAME'];
			$balance_os_show = false;
			$cutomer_balance_array['BALANCE'] = "";
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$bill_discount = $saleDetails['SALE_DISCOUNT'];
		$companyLogo   = $objCompanyDetails->getLogo();
		$company  		 = $objCompanyDetails->getActiveProfile();
?>

<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="button printThis pull-left" title="Print"> <i class="fa fa-print"></i> Print </button>
            <button class="btn btn-default pull-right cancel-button" onclick="window.close();" title="Close Window"> <span class="glyphicon glyphicon-remove"></span></button>
            <div style="height:20px;clear:both;"></div>
        </div><!--headerWrapper-->
    </div><!--header-->
    <div class="clear"></div>
    <div class="printThisDiv" style="position: relative;">
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
            <div class="invoiceHead" style="width: 100%;margin: 0px auto;">
<?php
				//if($companyLogo == ''){
				if(false){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:10px;font-size: 10px;width: 95%;line-height:15px;">
            		<div class="company-title"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']; ?>
										<?php echo ($company['CONTACT']=='')?"":"Contact: ".$company['CONTACT']; ?>
                    <?php echo ($company['EMAIL']=='')?"":"Email: ".$company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="clear" style="height: 5px;"></div>
<?php
				if($invoice_num == '1'){
?>
                <div class="infoPanel pull-left">
                	<div class="infoHead">Customer Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:12px;"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right">
                	<div class="infoHead">Invoice Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:12px;">Inv # <?php echo $saleDetails['BILL_NO']; ?>  </span>
                	<span class="pull-right" style="padding:0px 2px;font-size:12px;"><?php echo $sale_date; ?> <?php echo date('h:i A',strtotime($saleDetails['RECORD_TIME'])); ?> </span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
<?php
				}

				if($invoice_num == '2'){
?>
				<div class="invoPanel">
					<div class="invoPanel">
						<div class="clear"></div>
	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Name: </span>
	                		<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Address: </span>
	                		<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">City: </span>
	                		<span class="text_line"><?php echo $customer['CITY']; ?></span>
	                	</span>
	                	<div class="clear"></div>
	                </div>
				</div>
				<div class="clear" style="height:10px;"></div>
<?php
				}
?>
				<div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 100%;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="5%">Sr</th>
                            <th width="35%">Description</th>
                            <th width="5%">Qty</th>
														<th width="5%">Rate</th>
                            <?php if($individual_discount){ ?>
                            <th width="10%">Disc</th>
                            <?php } ?>
                            <?php if($use_taxes){ ?>
                            <th width="10%">TaxRate</th>
                            <?php } ?>
                            <th width="15%">Amount(<?php echo $currency_type; ?>)</th>
                        </tr>
                    </thead>
                    <tbody style="border-bottom:1px solid #222 !important;">
                    	<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <?php if($individual_discount){ ?>
                            <td>&nbsp;</td>
                            <?php } ?>
                            <?php if($use_taxes){ ?>
                            <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                        </tr>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($saleDetailList)){
							while($row = mysql_fetch_array($saleDetailList)){
								if($row['SERVICE_ID'] > 0){
                                    $itemName = $objServices->getTitle($row['SERVICE_ID']);
                                }else{
																		$item_detail = $objItems->getRecordDetails($row['ITEM_ID']);
																		$itemName    = $item_detail['ITEM_BARCODE']."-".$item_detail['NAME'];
                                }
								$row['TOTAL_AMOUNT'] = round($row['TOTAL_AMOUNT']);
?>
								<tr>
									<td><?php echo $counter; ?></td>
									<td>
										<span style="float:left;margin-left:5px;"><?php echo $itemName; ?></span>
										<span style="float:left;margin-left:5px;"><?php echo ($row['ITEM_DESCRIPTION']!='')?"(".$row['ITEM_DESCRIPTION'].")":""; ?></span>
									</td>
									<td><?php echo ($row['SERVICE_ID'] > 0)?"---":$row['QUANTITY']; ?></td>
									<td><?php echo $row['UNIT_PRICE']; ?></td>
									<?php if($individual_discount){ ?>
									<td><?php echo $row['SALE_DISCOUNT'] ; ?> <?php echo ($saleDetails['DISCOUNT_TYPE'] == 'P')?"%":""; ?></td>
									<?php } ?>
									<?php if($use_taxes){ ?>
									<td><?php echo $row['TAX_RATE']; ?></td>
									<?php } ?>
									<td><span style="float:right;margin-right:5px;"> <?php echo number_format($row['TOTAL_AMOUNT'],2); ?></span></td>
								</tr>
<?php
									$quantity    += $row['QUANTITY'];
									$subAmount 	 += $row['SUB_AMOUNT'];
									$taxAmount   += $row['TAX_AMOUNT'];
									$totalAmount += $row['TOTAL_AMOUNT'];
									$counter++;
							}
						}
?>
<?php
						$class = '';
						$other_counter = $max_counter - $counter;
						for ($i=0; $i <= $other_counter; $i++) {
							if($other_counter == $i){
								$class = '';
							}
?>
							<tr>
	              <td class="<?php echo $class; ?>" ><?php //echo $i+$counter; ?></td>
	              <td>&nbsp;</td>
	              <td>&nbsp;</td>
								<td>&nbsp;</td>
	              <?php if($individual_discount){ ?>
	              <td>&nbsp;</td>
	              <?php } ?>
	            	<?php if($use_taxes){ ?>
	              <td>&nbsp;</td>
	              <?php } ?>
	              <td>&nbsp;</td>
	          </tr>
<?php
						}
?>
							<tr>
	                            <td class="bottom-border">&nbsp;</td>
	                            <td class="bottom-border">&nbsp;</td>
	                            <td class="bottom-border">&nbsp;</td>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php if($individual_discount){ ?>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php } ?>
                            	<?php if($use_taxes){ ?>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php } ?>
	                            <td class="bottom-border">&nbsp;</td>
	                        </tr>
                    </tbody>
                </table>
                <div class="clear"></div>
                <table>
                	<?php
                    	$colum_skipper = 0;
                    	$columns       = 2;
                    	$colum_skipper += ($individual_discount)?0:1;
                    	$colum_skipper += ($use_taxes)?0:1;
                    	$columns 	   += $colum_skipper;
                    ?>
                    <?php
                    	$finalAmount = $totalAmount;
                    	if($saleDetails['DISCOUNT_TYPE'] == 'P'){
                    		$finalAmount-= ($totalAmount*$saleDetails['DISCOUNT'])/100;
                    	}else{
                    		$finalAmount-= $saleDetails['DISCOUNT'];
                    	}
                    	$finalAmount+= $saleDetails['CHARGES'];
                    ?>
                    <tfoot>
                    	<tr>
                    		<td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;opacity:1.0;"><span style="float:right;margin-right:15px;">Total Qty</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;opacity:1.0;"><span style="float:right;margin-right:5px;"> <?php echo round($quantity); ?></span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;"><span style="float:right;margin-right:15px;">Bill Total</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;"><span style="float:right;margin-right:5px;"> <?php echo number_format($finalAmount,2); ?></span></td>
                        </tr>
                        <tr>
                        	<td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;opacity:0.0;"><span style="float:right;margin-right:15px;">Discount Amount</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;opacity:0.0;"><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['SALE_DISCOUNT'],2); ?></span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;"><span style="float:right;margin-right:15px;">Amount Tendered</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;"><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['RECEIVED_CASH'],2); ?></span></td>
                        </tr>
                        <tr>
                        	<td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;opacity:0.0;"><span style="float:right;margin-right:15px;">Tax Amount</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;opacity:0.0;"><span style="float:right;margin-right:5px;"><?php echo number_format($taxAmount,2); ?></span></td>
                            <?php if($saleDetails['REMAINING_AMOUNT'] != 0){ ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;"><span style="float:right;margin-right:15px;">Bill Receivable</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;"><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['REMAINING_AMOUNT'],2); ?></span></td>
                            <?php }else{ ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:15px;">Change </span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['CHANGE_RETURNED'],2); ?></span></td>
                            <?php } ?>
                        </tr>
                        <tr>
                        	<td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;opacity:0.0;"><span style="float:right;margin-right:15px;">Other Charges</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;opacity:0.0;"><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['CHARGES'],2); ?></span></td>
														<?php if(substr($saleDetails['CUST_ACC_CODE'],0,6) == '010104'){ ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;"><span style="float:right;margin-right:15px;">Balance O/S</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;"><span style="float:right;margin-right:5px;"><?php echo round($cutomer_balance_array['BALANCE']); ?></span></td>
														<?php }else{ ?>
														<td class="bottom-border" style="text-align:right;padding:5px !important;width:30%;"><span style="float:right;margin-right:15px;"></span></td>
	                          <td class="bottom-border" style="text-align:right;padding:5px !important;width:20%;"><span style="float:right;margin-right:5px;"></span></td>
														<?php } ?>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>
                <div class="invoice_footer">
	                <div class="auths pull-left">
	                	<?php echo $usersFullName; ?>
	                </div><!--partyTitle-->
	                <div class="auths pull-left">
	                	Checked By
	                </div><!--partyTitle-->
	                <div class="clear" style="height: 10px;"></div>
	                <div class="invoice-developer-info"> Designed &amp; Developed By <b>SIT SOLUTIONS</b> , 041-8722200, www.sitsol.net </div>
	        	</div>
            </div><!--invoiceBody-->
        </div><!--invoiceLeftPrint-->
        <div class="clear"></div>
    </div><!--invoiceContainer-->
    </div><!--printThisDiv-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
