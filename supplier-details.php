<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/suppliers.php');

	$objAccountCodes = new ChartOfAccounts();
	$objSuppliers 	 = new suppliers();

  if(isset($_POST['supp_name'])){
    $objSuppliers->account_title = mysql_real_escape_string($_POST['supp_name']);
    if($objSuppliers->account_title == ''){
      $account_code_id = 0;
    }else{
      $account_code_id = $objAccountCodes->addSubMainChild($objSuppliers->account_title,'040101');
    }
    if($account_code_id){
      $account_code_details = $objAccountCodes->getDetail($account_code_id);
      $objSuppliers->account_code = $account_code_details['ACC_CODE'];
      $inserted = $objSuppliers->save();
      if($inserted){
        echo json_encode(array('OK'=>'Y','ACC_CODE'=>$objSuppliers->account_code,'ACC_TITLE'=>$objSuppliers->account_title));
      }else{
        echo json_encode(array('OK'=>'N'));
      }
    }
    exit();
  }


  //Permission
  if(!in_array('suppliers-management',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	if(isset($_POST['submit'])){
		$objSuppliers->account_title = $_POST['title'];
		if($objSuppliers->account_title == ''){
			$account_code_id = 0;
		}else{
			$account_code_id = $objAccountCodes->addSubMainChild($objSuppliers->account_title,'040101');
		}
		if($account_code_id){
			$account_code_details = $objAccountCodes->getDetail($account_code_id);
			$objSuppliers->account_code 	= $account_code_details['ACC_CODE'];
			$objSuppliers->urdu_title 		= isset($_POST['urdu_title'])?mysql_real_escape_string($_POST['urdu_title']):"";
			$objSuppliers->contact_person = mysql_real_escape_string($_POST['contact_person']);
			$objSuppliers->email 					= mysql_real_escape_string($_POST['email']);
			$objSuppliers->mobile 				= mysql_real_escape_string($_POST['mobile']);
			$objSuppliers->phones 				= mysql_real_escape_string($_POST['phones']);
			$objSuppliers->city 					= mysql_real_escape_string($_POST['city']);
			$objSuppliers->address 				= mysql_real_escape_string($_POST['address']);
			$objSuppliers->tax_reg_no 		= mysql_real_escape_string($_POST['tax_reg_no']);
			$objSuppliers->ntn_reg_no 		= mysql_real_escape_string($_POST['ntn_reg_no']);
			$objSuppliers->cnic_no 				= mysql_real_escape_string($_POST['cnic_no']);
			$objSuppliers->send_sms 			= isset($_POST['send_sms'])?"Y":"N";

			$inserted = $objSuppliers->save();
		}else{
			$inserted = false;
		}
		if($inserted){
			echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$inserted."&added');</script>";
		}else{
			$message = 'Error! Supplier Record Could Not Be Saved.';
		}
	}
	if(isset($_POST['update'])){
		$supplierId = $_POST['supplier_id'];
		$objSuppliers->account_title = $_POST['title'];
		if($objSuppliers->account_title == ''){
			$supplierId = 0;
		}
		$objSuppliers->urdu_title 		= isset($_POST['urdu_title'])?mysql_real_escape_string($_POST['urdu_title']):"";
		$objSuppliers->contact_person = mysql_real_escape_string($_POST['contact_person']);
		$objSuppliers->email 					= mysql_real_escape_string($_POST['email']);
		$objSuppliers->mobile 				= mysql_real_escape_string($_POST['mobile']);
		$objSuppliers->phones 				= mysql_real_escape_string($_POST['phones']);
		$objSuppliers->city 					= mysql_real_escape_string($_POST['city']);
		$objSuppliers->address 				= mysql_real_escape_string($_POST['address']);
		$objSuppliers->tax_reg_no 		= mysql_real_escape_string($_POST['tax_reg_no']);
		$objSuppliers->ntn_reg_no 		= mysql_real_escape_string($_POST['ntn_reg_no']);
		$objSuppliers->cnic_no 				= mysql_real_escape_string($_POST['cnic_no']);
		$objSuppliers->send_sms 			= isset($_POST['send_sms'])?"Y":"N";

		if($supplierId && $objSuppliers->account_title != ''){
			$supplier_details = mysql_fetch_array($objSuppliers->getRecordDetails($supplierId));
			$objAccountCodes->updateTitleByCode($supplier_details['SUPP_ACC_CODE'],$objSuppliers->account_title);
			$updated = $objSuppliers->update($supplierId);
		}else{
			$updated = false;
		}
		if($updated){
			echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$supplierId."&updated');</script>";
		}else{
			$message = 'Error! Supplier Record Could Not Be Updated.';
		}
	}
	if(isset($_GET['cid'])){
		$cid = (int)mysql_real_escape_string($_GET['cid']);
		$customerDetail = $objSuppliers->getRecordDetails($cid);
		$customerRow = mysql_fetch_array($customerDetail);
	}
?>
<!DOCTYPE html>
<html><head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

      	<script type="text/javascript" src = "resource/scripts/jquery.1.11.min.js"></script>
				<script type="text/javascript" src = "resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
      	<script type="text/javascript" src = "resource/scripts/tab.js"></script>
      	<script type="text/javascript" src = "resource/scripts/configuration.js"></script>
      	<script type="text/javascript" src = "resource/scripts/jquery.validate.min.js"></script>
        <script type="text/javascript">
				$(document).ready(function(){
	<?php
						if(isset($message)){
	?>
							displayMessage('<?php echo $message; ?>');
	<?php
						}
	?>
					$("form#form").validate({
						rules:{
							title: "required",
							email: "email"
					    },messages:{
					    	title: "Account Title Is required!",
					    	email: {email:"The Email is Not Valid!"}
					    }
					});
					$("input[type='text']").attr('autocomplete','off');
					$(".get_code_for_title").keyup(function(e){
						if(e.keyCode==9){
							$("input.cPerson").focus();
						}
					});
				});
		</script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div>
      	<div id = "bodyWrapper">
         	<div class = "content-box">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Supplier Details</p>
                  		<span id="tabPanel">
                     		<div class = "tabPanel">
	                    		<a href="suppliers-management.php?tab=list"><div class="tab">List</div></a>
	                        <a href="suppliers-management.php?tab=search"><div class="tab">Search</div></a>
													<div class="tabSelected">Details</div>
	                        <div class="clear"></div>
                     		</div>
                  		</span>
               		</div><!-- End .content-box-header -->
               		<div class="clear"></div>
               		<div id = "bodyTab1">
                  		<div class="col-xs-12 mt-20">
                     	<form method="post" action="" id='form' class="form-horizontal">
                            <?php
                            	if(isset($customerRow)){
                            ?>
														<div class="form-group">
															<label class="control-label col-sm-2"> Account Code :</label>
															<div class="col-sm-10">
																<input name="code"  value="<?php echo (isset($customerRow))?$customerRow['SUPP_ACC_CODE']:""; ?>" type="text" class="form-control get_code_for_title" readonly />
															</div>
														</div>
                            <?php
                            	}
                            ?>
														<div class="form-group">
															<label class="control-label col-sm-2"> Account Title :</label>
															<div class="col-sm-10">
																<input name="title" value="<?php echo (isset($customerRow))?$customerRow['SUPP_ACC_TITLE']:""; ?>" type="text" class="form-control"  autofocus />
															</div>
														</div>
														<?php if($urdu_account_names == 'Y'){ ?>
														<div class="form-group">
															<label class="control-label col-sm-2"> Account Title :</label>
															<div class="col-sm-10">
																<input name="urdu_title" dir="rtl" value="<?php echo (isset($customerRow))?$customerRow['URDU_TITLE']:""; ?>" type="text" class="form-control"  />
															</div>
														</div>
														<?php } ?>
														<hr />

														<div class="form-group">
															<label class="control-label col-sm-2"> Tax Reg. # :</label>
															<div class="col-sm-10">
																<input name="tax_reg_no" value="<?php echo (isset($customerRow))?$customerRow['TAX_REG_NO']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> CNIC # :</label>
															<div class="col-sm-10">
																<input name="cnic_no" value="<?php echo (isset($customerRow))?$customerRow['CNIC_NO']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> NTN # :</label>
															<div class="col-sm-10">
																<input name="ntn_reg_no" value="<?php echo (isset($customerRow))?$customerRow['NTN_REG_NO']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> Contact Person </label>
															<div class="col-sm-10">
																<input name="contact_person" value="<?php echo (isset($customerRow))?$customerRow['CONT_PERSON']:""; ?>" type="text" class="form-control cPerson" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> Email</label>
															<div class="col-sm-10">
																<input name="email"  value="<?php echo (isset($customerRow))?$customerRow['EMAIL']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> Mobile</label>
															<div class="col-sm-7">
																<input name="mobile" value="<?php echo (isset($customerRow))?$customerRow['MOBILE']:""; ?>" type="text" class="form-control" />
															</div>
															<div class="col-sm-3">
																<input id="cmn-toggle-sms" value="" class="css-checkbox"  type="checkbox" <?php echo (isset($customerRow)&&$customerRow['SEND_SMS'] == 'Y')?"checked":""; ?> name="send_sms" />
																<label for="cmn-toggle-sms" class="css-label" style="margin-top:5px;margin-right:-25px;">SMS</label>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"> Telephones</label>
															<div class="col-sm-10">
																<input name="phones" value="<?php echo (isset($customerRow))?$customerRow['PHONES']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Address</label>
															<div class="col-sm-10">
																<textarea class="form-control" name="address" style="min-height:100px;"><?php echo (isset($customerRow))?$customerRow['ADDRESS']:""; ?></textarea>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">City</label>
															<div class="col-sm-10">
																<input name="city" value="<?php echo (isset($customerRow))?$customerRow['CITY']:""; ?>" type="text" class="form-control" />
															</div>
														</div>


														<div class="form-group">
															<label class="control-label col-sm-2"></label>
															<div class="col-sm-10">
<?php 									if(isset($customerRow)){ ?>
															 <input type="hidden" name="supplier_id" value="<?php echo $customerRow['SUPP_ID']; ?>"  />
                               <input name="update" type="submit" value="Update" class="button" />
                               <input type="button" value="Cancel" class="button" onclick="window.location.replace('suppliers-management.php');" />
<?php
							}else{
?>
                               <input name="submit" type="submit" value="Save" class="button" />
                               <input type="reset" value="Reset" class="button" />
                               <input type="button" value="Cancel" class="button" onclick="window.location.replace('suppliers-management.php');" />
<?php
							}
?>
															</div>
														</div>
                     	</form>
                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab1-->
            	</div><!-- End summery_body -->
         	</div><!-- End content-box -->
      	</div><!--End body-wrapper-->
        <div id='xfade'></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
