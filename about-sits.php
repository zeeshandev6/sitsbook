<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>About Sitsbook</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <style type="text/css">
    </style>
    <script src="resource/scripts/jquery.1.11.min.js" type="text/javascript"></script>
    <script src="resource/scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            var win_height = $(window).height();
            var win_height = $("body").height();
            $("body").height(win_height);
            $("iframe").height(win_height - 3);
        });
    </script>
</head>
	<body style="padding:0px;overflow:visible;">
		<iframe style="width:100%;" src="http://sitsbook.com"></iframe>
  </body>
</html>
<?php include('conn.close.php'); ?>
