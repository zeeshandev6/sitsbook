<?php
    $out_buffer = ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/banks.php');
    include ('common/classes/accounts.php');

    $objBanks         = new Banks();
    $objAccountCodes  = new ChartOfAccounts();

    if(isset($_POST['save'])){
        //add new
        $id     = isset($_GET['id'])?$_GET['id']:0;
        if($id == 0){
            $objAccountCodes->account_title =   ucfirst($_POST['title']);
            $objAccountCodes->addSubMainChild($objAccountCodes->account_title,'010102');
        }
        $objBanks->account_title = ucfirst($_POST['title']);
        $objBanks->account_code  = $objAccountCodes->account_code;
        $objBanks->description   = mysql_real_escape_string($_POST['description']);
        $objBanks->type          = mysql_real_escape_string($_POST['bank_type']);
        $objBanks->merch_rate    = (isset($_POST['merch_rate']))?mysql_real_escape_string($_POST['merch_rate']):"";

        if($id){
            $objAccountCodes->updateTitleByCode($_POST['code'],$objBanks->account_title);
            $itemid = $objBanks->update($id);
            exit(header('location: add-bank-detail.php?id='.$id.'&action=updated'));
        }else{
            $idd = $objBanks->save();
            if(!isset($_POST['quick'])){
                exit(header('location: add-bank-detail.php?id='.$idd.'&action=added'));
            }else{
                if($idd > 0){
                    echo json_encode(array('OK'=>'Y','ACC_CODE'=>$objBanks->account_code,'ACC_TITLE'=>$objBanks->account_title));
                }else{
                    echo json_encode(array('OK'=>'N'));
                }
            }
        }
        exit();
    }
?>
    <!DOCTYPE html 
        >
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){

            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

<?php
        //get one record by id
        $merchant    = '';
        $nonmerchant = '';
        if(isset($_GET['id'])){
            if(!empty($_GET['id'])){
                $id = $_GET['id'];
                $array           = $objBanks->getDetail($id);
                $id              = $array['ID'];
                $code            = $array['ACCOUNT_CODE'];
                $title           = $array['ACCOUNT_TITLE'];
                $description     = $array['DESCRIPTION'];
                $merchant        = ($array['BANK_TYPE'] == 'M')?"checked":"";
                $nonmerchant     = ($array['BANK_TYPE'] == 'N')?"checked":"";
                $merch_rate      = $array['MERCH_RATE'];
            }
        }

    ?>


    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p >Bank Details</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="banks-management.php"><div class="tab">List</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Bank updated.");}
                    elseif($_GET['action']=='added'){ successMessage("New Bank added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not saved.");}
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo isset($_GET['id'])?$_GET['id']:"0"; ?>" />
                        <div class="col-xs-12">
                            <?php if(isset($_GET['id'])){ ?>
                            <div class="form-group">
                              <label class="control-label col-sm-3"> Account Code :</label>
                              <div class="col-sm-9">
                                <input class="form-control" readonly name="code" value="<?php if(isset($code)){ echo $code;} ?>" required="required"/>
                              </div>
                            </div>
                            <?php } ?>

                            <div class="form-group">
                              <label class="control-label col-sm-3"> Bank Name :</label>
                              <div class="col-sm-9">
                                <input class="form-control" name="title" value="<?php if(isset($title)){ echo $title;} ?>" required="required" autofocus />
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-sm-3"> Account Type :</label>
                              <div class="col-sm-9">
                                <input type="radio" class="css-checkbox" id="type-m" value="M" name="bank_type" <?php echo ($nonmerchant == '')?"checked":""; ?> />
                                <label class="css-label-radio" for="type-m">Merchant</label>
                                <input type="radio" class="css-checkbox" id="type-n" value="N" name="bank_type" <?php echo $nonmerchant; ?> />
                                <label class="css-label-radio" for="type-n">Non Merchant</label>
                              </div>
                            </div>


                            <div class="form-group">
                              <label class="control-label col-sm-3"> Charges/Rate :</label>
                              <div class="col-sm-9">
                                <input class="form-control" name="merch_rate" value="<?php if(isset($merch_rate)){ echo $merch_rate;} ?>" />
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-sm-3"> Description :</label>
                              <div class="col-sm-9">
                                <textarea style="height:70px;" name="description" class="form-control"><?php if(isset($description)){ echo $description;}  ?></textarea>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-sm-3"></label>
                              <div class="col-sm-9">
                                <input type="submit" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="btn btn-primary" />
                                <input type="button" value="New Form" onclick="window.location.href='add-bank-detail.php'";  class="btn btn-default pull-right" />
                              </div>
                            </div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>

        </div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
