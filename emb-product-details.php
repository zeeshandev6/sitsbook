<?php
    $out_buffer = ob_start();
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/emb_products.php');

    $objEmbProducts  = new EmbProducts();

    $id              = 0;
    $emb_product_details = NULL;

    if(isset($_POST['save'])){
      $id                                                  = (int)$_POST['id'];
      $objEmbProducts->title                               = mysql_real_escape_string(ucfirst($_POST['title']));
      $objEmbProducts->code                                = mysql_real_escape_string($_POST['product_no']);
      $objEmbProducts->description                         = mysql_real_escape_string(ucfirst($_POST['description']));
      if($id == 0){
        $id = $objEmbProducts->save();
      }else{
        $objEmbProducts->update($id);
      }
      exit(header('location: emb-product-details.php?id='.$id));
      exit();
    }

    //get one record by id
    if(isset($_GET['id'])){
        $id = mysql_real_escape_string($_GET['id']);
        $emb_product_details = $objEmbProducts->getDetails($id);
    }
?>
    <!DOCTYPE html 
        >

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css"                         />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css"                         />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"                       />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css"                          />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"                          />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css"                       />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"                  />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"                 />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"              />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p ><B>Product Details</B></p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="emb-product-management.php"><div class="tab">List</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>

                <?php
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type updated.
                        </div>
                    <?php
                    }elseif($_GET['action']=='added'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type added.
                        </div>
                    <?php
                    }
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" name="cust" class="form-horizontal">
                        <input type="hidden" name="measure_type" value="<?php if(isset($_GET['id'])){ echo 'update';} else { echo 'add'; } ?>" />
                        <input type="hidden" name="id" value="<?php if(isset($_GET['id'])){ echo $id;} ?>" />
                        <div class="col-xs-12 mt-20">
                          <div class="form-group">
                            <label class="control-label col-sm-2">Title </label>
                            <div class="col-sm-10">
                              <input class="form-control" name="title" value="<?php echo $emb_product_details['TITLE']; ?>" required="required"/>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2">Product Code </label>
                            <div class="col-sm-10">
                              <input class="form-control" name="product_no" value="<?php echo ($emb_product_details==NULL)?$objEmbProducts->newProductCode():$emb_product_details['PROD_CODE']; ?>"  />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2">Description </label>
                            <div class="col-sm-10">
                              <textarea style="height:100px; width: 535px;" class="form-control" name="description"><?php echo $emb_product_details['DESCRIPTION']; ?></textarea>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <input  type="submit"  name="save"         value="<?php echo ($id>0)?"Update":"Save"; ?>" class="btn btn-primary pull-left" />
                              <a href="emb-product-details.php" class="btn btn-default pull-right">New Product</a>
                            </div>
                          </div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>
    </div><!--bodyWrapper-->
    </body>
    </html>
<?php include('conn.close.php'); ?>
