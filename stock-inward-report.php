<?php
    ob_start();
    include('common/connection.php');
    include 'common/config.php';
    include('common/classes/sale.php');
    include('common/classes/accounts.php');
    include('common/classes/j-voucher.php');
    include('common/classes/items.php');
    include('common/classes/itemCategory.php');
    include('common/classes/customers.php');
    include('common/classes/stock_history.php');

    //Permission
    if(!in_array('stock-inward-report',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objSale           = new Sale();
    $objJournalVoucher = new JournalVoucher();
    $objAccountCodes   = new ChartOfAccounts();
    $objItems          = new Items();
    $objItemCategory   = new ItemCategory();
    $objCustomers      = new Customers();
    $objConfigs        = new Configs();
    $objStockHistory   = new StockHistory();

    $total             = $objConfigs->get_config('PER_PAGE');
    $customersList     = $objCustomers->getList();

    if(isset($_GET['search'])){
        //$objInvoice->from_date   = ($_GET['fromDate'] == '')?"":date('Y-m-d',strtotime($_GET['fromDate']));
        //$objInvoice->to_date     = ($_GET['toDate'] == '')?"":date('Y-m-d',strtotime($_GET['toDate']));
        $objStockHistory->lot_no   = mysql_real_escape_string($_GET['lot_no']);

        $inward_report             = $objStockHistory->getStockReport();
    }
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <style type="text/css">
        .ui-effects-transfer {
            border: 1px solid #CCC;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/project.expense.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" >
        $(document).ready(function() {
            $("select").selectpicker();
            $("*[data-toggle=tooltip]").tooltip();
            $("a.pointer").on("click",function(){
                $(this).deleteMainRow("invoice-sales-list.php");
            });
            var page_url = $("input.page-url").val();
            $("div.pagination-container a").each(function(i,e){
                var href = $(this).attr("href");
                $(this).attr("href",href+page_url);
            });
        });
    </script>
    <script type="text/javascript">
        $(window).on('load',function(){
            $(".printThis").click(function(){
                var MaxHeight = 884;
                var RunningHeight = 0;
                var PageNo = 1;
                //Sum Table Rows (tr) height Count Number Of pages
                $('table.tableBreak>tbody>tr').each(function(){

                    if(PageNo == 1){
                        MaxHeight = 750;
                    }

                    if(PageNo != 1){
                        MaxHeight = 820;
                    }

                    if (RunningHeight + $(this).height() > MaxHeight){
                        RunningHeight = 0;
                        PageNo += 1;
                    }

                    RunningHeight += $(this).height();
                    //store page number in attribute of tr
                    $(this).attr("data-page-no", PageNo);
                });
                //Store Table thead/tfoot html to a variable
                var tableHeader = $(".tHeader").html();
                var tableFooter = $(".tableFooter").html();
                var repoDate = $(".repoDate").text();
                //remove previous thead/tfoot
                $(".tHeader").remove();
                $(".tableFooter").remove();
                $(".repoDate").remove();
                //Append .tablePage Div containing Tables with data.
                for(i = 1; i <= PageNo; i++){

                    if(i == 1){
                        MaxHeight = 750;
                    }

                    if(i != 1){
                        MaxHeight = 820;
                    }

                    $('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
                    //get trs by pagenumber stored in attribute
                    var rows = $('table tr[data-page-no="' + i + '"]');
                    $('#Table' + i).find("thead").append(tableHeader);
                    $('#Table' + i).find("tbody").append(rows);
                }
                $(".newTable").last().append(tableFooter);
                $('table.tableBreak').remove();
                $(".printTable").printThis({
                  debug: false,
                  importCSS: false,
                  printContainer: true,
                  loadCSS: 'resource/css/reports.css',
                  pageTitle: "Sit Solution",
                  removeInline: false,
                  printDelay: 500,
                  header: null
              });
            });
            $('select').selectpicker();
        });
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Inward Stock Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <div class="clear"></div>
                    <form>
                        <div class="clear"></div>

                        <div class="caption">Lot #</div>
                        <div class="field">
                            <input type="text" name="lot_no" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" name="search" class="button" value="Search" />
                        </div>
                        <div class="clear"></div>
                    </form>
                    <div class="clear m-20"></div>
                <?php if(isset($invoice_list)){ ?>

                <div class="col-xs-12">
                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                        <div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                            <p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">General Invoice Report</p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                                <?php echo ($objInvoice->from_date=="")?"":"From. ".date('d-m-Y',strtotime($objInvoice->from_date)); ?>
                                <?php echo ($objInvoice->to_date=="")?" To. ".date('d-m-Y'):"To. ".date('d-m-Y',strtotime($objInvoice->to_date)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                        </div>
                        <table class="table" id="invoice_rows">
                            <thead>
                                <tr>
                                    <th class="text-center col-xs-1">Sr.</th>
                                    <th class="text-center col-xs-1">Lot#</th>
                                    <th class="text-center col-xs-2">InwardDate</th>
                                    <th class="text-center col-xs-3">Items</th>
                                    <th class="text-center col-xs-1">Than</th>
                                    <th class="text-center col-xs-1">Length</th>
                                    <th class="text-center col-xs-1">Siuts</th>
                                    <th class="text-center col-xs-1">C.P</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total_amount = 0.00;
                                    $counter = 1;
                                    if(mysql_num_rows($inward_report)){
                                        while($row = mysql_fetch_assoc($inward_report)){
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-center"><?php echo $row['LOT_NO'] ?></td>
                                    <td class="text-center description"><?php echo date("d-m-Y",strtotime($row['INWARD_DATE'])); ?></td>
                                    <td class="text-center"><?php echo $row['ITEM_ID'] ?></td>
                                    <td class="text-center"><?php echo $row['UNIT']; ?></td>
                                    <td class="text-center"><?php echo $row['QUANTITY']; ?></td>
                                    <td class="text-center"><?php echo number_format($row['AMOUNT'],2) ?></td>
                                </tr>
                                            <?php
                                            $counter++;
                                            //$total_amount+=$row['AMOUNT'];
                                        }
                                    }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="5">Total</th>
                                    <th class="text-center"><b><?php echo number_format($total_amount,2); ?></b></th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php } ?>
                </div> <!--End bodyTab1-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
