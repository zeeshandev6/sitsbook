<?php
	ob_start();
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/items.php');
	include('common/classes/item_category.php');
	include('common/classes/customers.php');
	include 'common/classes/branches.php';
	include 'common/classes/branch_stock.php';
	include('common/classes/stock_history.php');

	//Permission
	if(!in_array('fabric-inward',$permissionz) && $admin != true){
		errorMessage('You Are Not Allowed To View This Panel!');
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts     = new ChartOfAccounts();
	$objItems               = new Items();
	$objItemCategory        = new ItemCategory();
	$objCustomer            = new Customers();
	$objUserAccount         = new UserAccounts();
	$objBranches  	   		= new Branches();
	$objBranchStock    		= new BranchStock();
	$objStockHistory        = new StockHistory();
	$objStockHistoryDetail  = new StockHistoryDetail();

	$customer_codes 				= $objChartOfAccounts->getAccountByCatAccCode('010104');
	$supplier_codes 				= $objChartOfAccounts->getAccountByCatAccCode('040101');

	function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
		$objItems        = new Items();
		$objBranchStock  = new BranchStock();
		if($branch_id > 0){
			$objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
		}else{
			$objItems->removeStockValue($item_id,$quantity,$total_cost);
		}
	}
	if(isset($_POST['delete_id'])){
			$delete_id 				 = (int)(mysql_real_escape_string($_POST['delete_id']));
			$fabric_inward_row = $objStockHistory->getDetail($delete_id);
			$rows 		 				 = $objStockHistoryDetail->getListByHisId($delete_id);
			if(mysql_num_rows($rows)){
				while($row = mysql_fetch_assoc($rows)){
					remove_branch_stock($row['ITEM'], $fabric_inward_row['LOCATION'],$row['QUANTITY'],$row['TOTAL_COST']);
				}
			}
			$objStockHistory->delete($delete_id);
			$objStockHistoryDetail->delete_io($delete_id);
			exit();
	}
	$total              = $objConfigs->get_config('PER_PAGE');

	$objStockHistory->io_status       = "I";

	$objStockHistory->from_date       = isset($_GET['fromdate'])?$_GET['fromdate']:"";
	$objStockHistory->to_date         = isset($_GET['todate'])?$_GET['todate']:"";
	$objStockHistory->pon             = isset($_GET['pon'])?mysql_real_escape_string($_GET['pon']):"";
	$objStockHistory->pon             = trim($objStockHistory->pon);
	$objStockHistory->party_name      = isset($_GET['party_name'])?mysql_real_escape_string($_GET['party_name']):"";
	$objStockHistory->ref_no       		= isset($_GET['reference'])?mysql_real_escape_string($_GET['reference']):"";
	$objStockHistory->through         = isset($_GET['through'])?mysql_real_escape_string($_GET['through']):"";
	$objStockHistory->vehicle_type    = isset($_GET['v_type'])?mysql_real_escape_string($_GET['v_type']):"";
	$objStockHistory->vehicle_no      = isset($_GET['v_no'])?mysql_real_escape_string($_GET['v_no']):"";
	$objStockHistory->gp_no           = isset($_GET['gp_no'])?mysql_real_escape_string($_GET['gp_no']):"";

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$fabric_inward_list = $objStockHistory->getPages($start,$total);
	$found_records 		= $objStockHistory->found_records;
?>
<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Admin Panel</title>
<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<!-- jQuery -->
<style media="screen">
	td,th{
		padding: 5px !important;
		font-size: 14px !important;
	}
</style>
<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
<script type="text/javascript" src="resource/scripts/configuration.js"></script>
<script type="text/javascript" src="resource/scripts/tab.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("a.pointer").click(function(){
		var file       = 'fabric-inward-list.php';
		var idValue    = $(this).attr("do");
		var clickedDel = $(this);

		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$.post(file, {delete_id : idValue}, function(data){
				$("#popUpDel").children(".confirm").text("Success! Record deleted Successfully.");
				$("#popUpDel").children(".dodelete").hide();
				$("#popUpDel").children(".nodelete").text("Close");
				clickedDel.parent('td').parent('tr').remove();
			});
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
		});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#fade").fadeOut('fast');
		});
	});
	<?php if(isset($_GET['tab'])&&$_GET['tab'] == 'search'){
		?>
		tab('2', '1', '2');
		<?php
	}else{
		?>
		tab('1', '1', '2');
		<?php
	}
	?>
	$("select").selectpicker();
});
</script>
</head>
<body>
<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
<div id="bodyWrapper">
</div> <!-- End #sidebar -->
<div class="content-box-top">
	<div class="content-box-header">
		<p>Inventory Inward Management</p>
		<span id="tabPanel">
			<div class="tabPanel">
				<a href="fabric-inward-list.php?page=1"><div class="tabSelected" id="tab1" >List</div></a>
				<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
				<a href="fabric-inward.php"><div class="tab">New</div></a>
				</div>
			</span>
			<div class="clear"></div>
		</div> <!-- End .content-box-header -->

		<div class="content-box-content">

			<div id="bodyTab1" style="display:block;">
				<table width="100%" cellspacing="0" >
					<thead>
						<tr>
							<th width="5%" style="text-align:center">Ref#</th>
							<th width="10%" style="text-align:center">Inward Date</th>
							<th width="20%" style="text-align:center">Party Name</th>
							<th width="5%" style="text-align:center">Gate Pass #</th>
							<th width="8%" style="text-align:center">Vehicle ( type / No. )</th>
							<th width="5%" style="text-align:center">Through</th>
							<th width="10%" style="text-align:center">Action</th>
						</tr>
					</thead>

					<tbody>
<?php
						$counter_start = $start;
						$counter_start++;
						if(mysql_num_rows($fabric_inward_list)){
							while($row = mysql_fetch_assoc($fabric_inward_list)){
?>
								<tr>
									<td style="text-align:center;"><?php echo $row['REFERENCE_NO']; ?></td>
									<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($row['INWARD_DATE'])); ?></td>
									<td style="text-align:left;"><?php echo $objChartOfAccounts->getAccountTitleByCode($row['PARTY_CODE']); ?></td>
									<td style="text-align:center;"><?php echo $row['GPNO']; ?></td>
									<td style="text-align:center;"><?php echo $row['VEHICLE_TYPE']." ".$row['VEHICLE_NO']; ?></td>
									<td style="text-align:center;"><?php echo $row['THROUGH']; ?></td>
									<td style="text-align:center;">
										<a href="fabric-inward.php?id=<?php echo $row['STH_ID']; ?>" id="view_button">View</a>
										<a href="#" do="<?php echo $row['STH_ID']; ?>" class="pointer"><i class="fa fa-times"></i></a>
									</td>
								</tr>
<?php
										$counter_start++;
									}
								}
?>
								<tr>
									<th colspan="100" style="text-align:center;">

									</th>
								</tr>
							</tbody>
							<tfoot></tfoot>
						</table>
						<div class="col-xs-12 text-center">
<?php
							if(!isset($_GET['search'])){
?>
								<nav>
									<ul class="pagination">
										<?php
										$count = $found_records;
										$total_pages = ceil($count/$total);
										$i = 1;
										$thisFileName = $_SERVER['PHP_SELF'];
										if(isset($this_page) && $this_page>0){
											?>
											<li>
												<a href="<?php echo $thisFileName; ?>?page=1">First</a>
											</li>
											<?php
										}
										if(isset($this_page) && $this_page>=1){
											$prev = $this_page;
											?>
											<li>
												<a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
											</li>
											<?php
										}
										$this_page_act = $this_page;
										$this_page_act++;
										while($total_pages>=$i){
											$left = $this_page_act-5;
											$right = $this_page_act+5;
											if($left<=$i && $i<=$right){
												$current_page = ($i == $this_page_act)?"active":"";
												?>
												<li class="<?php echo $current_page; ?>">
													<?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
												</li>
												<?php
											}
											$i++;
										}
										$this_page++;
										if(isset($this_page) && $this_page<$total_pages){
											$next = $this_page;
											echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
										}
										if(isset($this_page) && $this_page<$total_pages){
											?>
											<li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
										</ul>
									</nav>
<?php
								}
							}
?>
						</div>
					</div> <!--End bodyTab1-->
					<div style="height:0px;clear:both"></div>
					<div id="bodyTab2" style="display: none" >
						<form method="get" action="">
							<div id="form">
								<input type="hidden" name="search" value="" />
								<div class="caption">From Date :</div>
								<div class="field" style="width:150px;">
									<input type="text" name="fromdate" class="form-control datepicker" />
								</div>
								<div class="clear"></div>

								<div class="caption">To Date :</div>
								<div class="field" style="width:150px;">
									<input type="text" name="todate" class="form-control datepicker" />
								</div>
								<div class="clear"></div>

								<div class="caption">PO # :</div>
								<div class="field" style="width:300px;">
									<input type="text"   name="pon" class="form-control" />
									<input type="hidden" name="no" />
								</div>
								<div class="clear"></div>

								<div class="caption">Party Name :</div>
								<div class="field" style="width:300px;">
									<select class="form-control show-tick" name="party_name" data-live-search="true">
										<option value=""></option>
										<optgroup label="Customers">
	<?php
										if(mysql_num_rows($customer_codes)){
											while($row = mysql_fetch_assoc($customer_codes)){
												?><option value="<?php echo $row['ACC_CODE']; ?>"><?php echo $row['ACC_TITLE']; ?></option><?php
											}
										}
	?>
										</optgroup>
										<optgroup label="Suppliers">
	<?php
										if(mysql_num_rows($supplier_codes)){
											while($row = mysql_fetch_assoc($supplier_codes)){
												?><option value="<?php echo $row['ACC_CODE']; ?>"><?php echo $row['ACC_TITLE']; ?></option><?php
											}
										}
	?>
										</optgroup>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption">Reference # :</div>
								<div class="field" style="width:300px;">
									<input type="text"   name="reference" class="form-control" />
								</div>
								<div class="clear"></div>

								<div class="caption">Through :</div>
								<div class="field" style="width:300px;">
									<input type="text" name="through" class="form-control" />
								</div>
								<div class="clear"></div>

								<div class="caption">Vehicle Type :</div>
								<div class="field" style="width:300px;">
									<input type="text" name="v_type" class="form-control" />
								</div>
								<div class="clear"></div>

								<div class="caption">Vehicle # :</div>
								<div class="field" style="width:300px;">
									<input type="text" name="v_no" class="form-control" />
								</div>
								<div class="clear"></div>

								<div class="caption">Gate Pass # :</div>
								<div class="field" style="width:300px;">
									<input type="text" name="gp_no" class="form-control" />
								</div>
								<div class="clear"></div>

								<div class="caption"></div>
								<div class="field">
									<input type="submit" class="button" name="search" value="Search" />
								</div>
								<div class="clear"></div>
							</div>
						</form>
					</div> <!-- End bodyTab2 -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->
		</div><!--body-wrapper-->
		<div id="fade"></div>
		<div id="xfade"></div>
	</body>
	</html>
