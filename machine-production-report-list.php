<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/machine_production.php');
	include('common/classes/machine_production_details.php');
	include('common/classes/machines.php');
	include('common/classes/customers.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_inward.php');

	//Permission
	if(!in_array('machine-production-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objMachineProduction 				= new MachineProduction();
	$objMachineProductionDetails  = new MachineProductionDetails();
	$objMachines  								= new machines();
	$objCustomers 								= new customers();
	$objChartOfAccounts  					= new ChartOfAccounts();
	$objEmbroideryInward   				= new EmbroideryInward();

	$list_date 										= isset($_GET['list_date'])?date('Y-m-d',strtotime($_GET['list_date'])):date('Y-m-d');

	$machine_prod_list 						= $objMachineProduction->getReportList($list_date);
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link type="text/css"  href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css"  href="resource/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css"  href="resource/css/bootstrap-select.css" rel="stylesheet">
		<style>
			table th,table td{
				font-size: 14px !important;
				padding: 6px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/machine.production.config.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$("#bodyTab1").on('click','a.delete_production',function(){
					var file 				= 'db/del_machine_production.php';
					var idValue 		= $(this).attr("do");
					var clickedDel 	= $(this);
					$("#fade").hide();
					$("#popUpDel").remove();
					$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
					$("#popUpDel").hide();
					$("#popUpDel").centerThisDiv();
					$("#fade").fadeIn('slow');
					$("#popUpDel").fadeIn();
					$(".dodelete").click(function(){
							$.post(file, {id : idValue}, function(data){
								data = $.parseJSON(data);
								$("#popUpDel").children(".confirm").text(data['MSG']);
								$("#popUpDel").children(".dodelete").hide();
								$("#popUpDel").children(".nodelete").text("Close");
								if(data['OKAY'] == 'Y'){
									clickedDel.parent('td').parent('tr').remove();
								}
								$("#fade").fadeOut();
								$("#popUpDel").fadeOut();
							});
						});
					$(".nodelete").click(function(){
						$("#fade").fadeOut();
						$("#popUpDel").fadeOut();
					});
					$(".close_popup").click(function(){
						$("#popUpDel").slideUp();
						$("#fade").fadeOut('fast');
					});
				});
				$("input[name='report_date']").change(function(){
					if($("input[name='report_date']").val()!=''){
						$.get("?",{list_date:$("input[name='report_date']").val()},function(html){
							$("table#report_list").html($(html).find("table#report_list").html());
						});
					}
				});
				$("button[name='print_button_blank']").click(function(){
					window.open('machine-production-report-print.php?report_date='+$("input[name='report_date']").val());
				});
			});
		</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Daily Machine Production Report</p>
								<span id="tabPanel">
									<div class="tabPanel">
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>"><div class="tabSelected">List</div></a>
										<a href="machine-production-report.php"><div class="tab">Report</div></a>
									</div>
								</span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
									<div id="form">
										<div class="title">
											<button type="button" name="print_button_blank" class="btn btn-default pull-right" ><i class="fa fa-print"></i></button>
											<input type="text" name="report_date" class="datepicker form-control pull-right" style="margin-right: 60px;" value="<?php echo date('d-m-Y',strtotime($list_date)); ?>">
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
										<table id="report_list">
											<thead>
												<tr>
													<th class="text-center col-xs-2">Report Date</th>
													<th class="text-center col-xs-4">Machine</th>
													<?php if(in_array('machine-production-report-full',$permissionz) || $admin == true){ ?>
													<th class="text-center col-xs-2">Action</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
											<?php
												if(mysql_num_rows($machine_prod_list)){
													while($row = mysql_fetch_assoc($machine_prod_list)){
														$machine_details = $objMachines->getRecordDetailsById($row['MACHINE_ID']);
											?>
												<tr>
													<td class="text-center col-xs-1"><?php echo date("d-m-Y",strtotime($row['REPORT_DATE'])) ?></td>
													<td class="text-center col-xs-3"><?php echo $machine_details['MACHINE_NO']."-".$machine_details['NAME']; ?></td>
													<?php if(in_array('machine-production-report-full',$permissionz) || $admin == true){ ?>
													<td class="text-center col-xs-3">
														<a href="machine-production-report.php?id=<?php echo $row['ID']; ?>" class="" id="view_button"><i class="fa fa-pencil"></i></a>
														<a href="machine-production-report.php?copy&id=<?php echo $row['ID']; ?>" class="" id="view_button"><i class="fa fa-copy"></i></a>
														<a do="<?php echo $row['ID']; ?>" class="delete_production pointer"><i class="fa fa-times"></i></a>
													</td>
													<?php } ?>
												</tr>
											<?php
													}
												}
											 ?>
											</tbody>
										</table>
									</div>
                </div> <!--End bodyTab1-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
	<div id="xfade"></div>
	<div id="fade"></div>
</body>
</html>
