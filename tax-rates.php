<?php
	$out_buffer = ob_start();
	include("common/connection.php");
	include 'common/config.php';
	include("common/classes/accounts.php");
	include("common/classes/tax-rates.php");

	$objAccounts = new ChartOfAccounts;
	$objTaxRates = new TaxRates;

	if(isset($_POST['save'])){
		$id    = $_POST['id'];
		$title = $_POST['title'];
		$rate  = $_POST['rate'];
		if($title != ""){
			$account_code_id = $objAccounts->addSubMainChild($title,'040105');
			if($account_code_id){
				$account_code    =  mysql_fetch_assoc($objAccounts->getAccountCodeById($account_code_id));
				if($account_code['ACC_CODE'] != ''){
					$tax_id = $objTaxRates->save($account_code['ACC_CODE'],$title,$rate);
					if($tax_id){
						exit(header('location:tax-rates.php?action=added'));
					}else{
						exit(header('location:tax-rates.php?action=updated'));
					}
				}
			}
		}
	}
	$taxRateList = $objTaxRates->getList();
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<style>
			html{
			}
			ui-tooltip{
				font-size: 12px;
				padding: 5px;
				box-shadow: none;
				border: 1px solid #999;
			}
			.input_sized, input.other{
				float:left;
				width: 300px;
				padding-left: 5px;
				color:rgb(102, 102, 102) !important;
				border: 1px solid rgb(153, 153, 153);
				height:0px;
				-webkit-box-shadow:#F4F4F4 0 0 0 2px;

				border-top-right-radius: 3px;
				border-top-left-radius: 3px;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;

				-moz-border-radius-topleft:3px;
				-moz-border-radius-topright:3px;
				-moz-border-radius-bottomleft:3px;
				-moz-border-radius-bottomright:5px;

				-webkit-border-top-left-radius:3px;
				-webkit-border-top-right-radius:3px;
				-webkit-border-bottom-left-radius:3px;
				-webkit-border-bottom-right-radius:3px;

				box-shadow: 0 0 2px #eee;
				transition: box-shadow 300ms;
				-webkit-transition: box-shadow 300ms;
			 }
			 .input_sized:hover{
				border: 1px solid rgb(41, 153, 255) !important;
				box-shadow: 0 0 2px #9ecaed;
			 }
			 table th, table td{
				 text-align:center
			 }

		</style>
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript"></script>
   </head>

   	<body>
   		<div id = "body-wrapper">
      		<div id="sidebar">
				<?php include("common/left_menu.php") ?>
            </div> <!-- End #sidebar -->


      		<div id = "bodyWrapper">
        		<div class = "content-box-top">
            		<div class = "summery_body">
               			<div class = "content-box-header">
                  			<p >Tax Rates</p>
                            <span id="tabPanel">
                                <div class="tabPanel">
                                    <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">Tax</div>
                                    <div class="tab" id="tab2" onclick="tab('2','1','2');">List</div>
                                </div>
                            </span>
                        	<div style = "clear:both;"></div>
               			</div><!-- End .content-box-header -->
                        <div style = "height:30px"></div>

                        <div id = "bodyTab1" style="display:block">
                        <form method="post" action="">
                            <div id = "form">
                                <div class = "caption">Title:</div>
                                <div class = "field">
                                    <input type="text" class="input_size" value="<?php echo (isset($taxRateDetail))?$taxRateDetail['TAX_TITLE']:"" ?>" name="title" style="width:200px;" />
                                </div>
                                <div style="clear:both;"></div>

                                <div class = "caption">Rate:</div>
                                <div class = "field">
                                    <input type="text" class="input_size" style="width:100px;" value="<?php echo (isset($taxRateDetail))?$taxRateDetail['TAX_RATE']:"" ?>" name="rate" /> <span style="font-size:18px;padding:10px;position:relative;top:5px;">%</span>
                                </div>
                                <div style="clear:both;"></div>
                                <div class = "caption"></div>
                                <div class = "field">
                                	<input type="hidden" name="id" value="<?php echo isset($taxRateDetail)?$taxRateDetail['TAX_ID']:0; ?>" />
                                    <input type="submit" value="Save" name="save" class="button" />
                                    <?php if(isset($taxRateDetail)){ ?>
                                    	<a class="button" href="tax-rates.php">New</a>
                                   	<?php } ?>
                                </div>
                                <div style = "clear:both;"></div>
                            </div><!--form-->
                            <div style = "clear:both;bodyWrapper"></div>
                        </form>
                        </div><!--bodyTab1-->

                        <div id = "bodyTab2" style="display:none">
                            <table style="width:90%">
                                <thead>
                                    <tr>
                                    	<th style="width: 20%;">Account Code</th>
                                        <th style="width: 20%;">Account Title</th>
                                        <th style="width: 20%;">Rate</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
		if(mysql_num_rows($taxRateList)){
			while($taxRateRow = mysql_fetch_array($taxRateList)){
?>
                                    <tr>
                                    	<td><?php echo $taxRateRow['TAX_ACC_CODE']; ?></td>
                                        <td><?php echo $taxRateRow['TAX_TITLE']; ?></td>
                                        <td><?php echo $taxRateRow['TAX_RATE']; ?></td>
                                        <td>
                                        	<a onclick="deleteRow(<?php echo $taxRateRow['TAX_ID']; ?>,'db/del-tax.php',this);" class="pointer"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
<?php
			}
		}
?>
                                </tbody>
                            </table>
                            <div style = "clear:both;"></div>
                        </div><!--bodyTab2-->
            		</div><!--summery_body-->
	         	</div><!--End.content-box-top-->
      		</div><!--bodyWrapper-->
      	</div>  <!--body-wrapper-->
      	<div id="fade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>
<?php echo ob_end_flush(); ?>
