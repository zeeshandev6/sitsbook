<?php
  include('msgs.php');
  include('common/connection.php');
  include('common/config.php');
  include('common/classes/accounts.php');
  include('common/classes/issue_items.php');
  include('common/classes/issue_items_details.php');
  include('common/classes/customers.php');
  include('common/classes/payrolls.php' );
  include('common/classes/sheds.php' );
  include('common/classes/machines.php' );
  include('common/classes/banks.php');
  include('common/classes/items.php');
  include('common/classes/itemCategory.php');
  include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('emb-issue-items',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

    $objChartOfAccounts          = new ChartOfAccounts();
    $objIssueItems        = new IssueItems();
    $objIssueItemDetails  = new IssueItemDetails();
    $objItems             = new items();
    $objItemCategory      = new ItemCategory();
    $objCustomers         = new customers();
    $objPayrolls          = new Payrolls();
    $objSheds             = new Sheds();
    $objMachines          = new Machines();
    $objBanks             = new Banks();
    $objTaxRates          = new TaxRates();
    $objConfigs           = new Configs();


    $total = $objConfigs->get_config('PER_PAGE');

    $po_new = '';
    if(isset($_GET['po'])){
      $po_new = base64_decode($_GET['po']);
    }

	$itemsCategoryList   = $objItemCategory->getList();
  $shed_accounts       = $objSheds->getList();
  $machine_list        = $objMachines->getList();

  $chart_of_account_list = $objChartOfAccounts->getLevelFourByGeneral('03');

    $tab = 'new';
    if(isset($_GET['tab'])){
        switch ($_GET['tab']){
            case 'list':
                $tab = 'list';
                break;
            case 'search':
                $tab = 'search';
                break;
            default:
                $tab = 'new';
                break;
        }
    }

    if(isset($_GET['page'])){
        $tab = 'list';
    }

    $issue_id            = 0;
    $issue               = NULL;
	if(isset($_GET['id'])){
		$issue_id = mysql_real_escape_string($_GET['id']);
		if($issue_id != '' && $issue_id != 0){
			$issue        = $objIssueItems->getDetail($issue_id);
			$saleDetails  = $objIssueItemDetails->getList($issue_id);
		}
	}

    $employee_list = $objPayrolls->getList();
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  />
    <style type="text/css">
        td{
            padding: 7px !important;
        }
        .pointer{

        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/issue.item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $("select").selectpicker();
            $("*[data-toggle='tooltip']").tooltip();
            $(".row_main").click(function(){
                $(this).deleteMainRow("db/delete.issue.items.php");
            });
            $(".reload_shed").click(function(){
                $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(data){
                    $("select.expensed_to").html($(data).find("select.expensed_to").html()).selectpicker('refresh');
                });
            });
        });
    </script>
</head>

<body class="body_menu_close">
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php"); ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top" style="padding-bottom: 0px" >
            <div class="content-box-header">
                <p> <i class="fa fa-stack-overflow"></i> Inventory Issue</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="emb-issue-items.php?tab=list"><div class="<?php echo ($tab == 'list')?"tabSelected":"tab"; ?>">List</div></a>
                        <a href="emb-issue-items.php?tab=search"><div class="<?php echo ($tab == 'search')?"tabSelected":"tab"; ?>">Search</div></a>
                        <a href="emb-issue-items.php?tab=new"><div class="<?php echo ($tab == 'new')?"tabSelected":"tab"; ?>">Details</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;">
<?php
            if($tab == 'list'){
                if(isset($_GET['page'])){
                    $this_page = $_GET['page'];
                    if($this_page>=1){
                        $this_page--;
                        $start = $this_page * $total;
                    }
                }else{
                    $start = 0;
                    $this_page = 0;
                }

                if(isset($_GET['search'])){
                    $objIssueItems->from_date   = (isset($_GET['from_date'])&&$_GET['from_date'] == '')?"":date('Y-m-d',strtotime($_GET['from_date']));
                    $objIssueItems->to_date     = (isset($_GET['to_date'])&&$_GET['to_date'] == '')?"":date('Y-m-d',strtotime($_GET['to_date']));
                    $objIssueItems->item_id     = (isset($_GET['item_id']))?$_GET['item_id']:"";
                    $objIssueItems->charged_to  = (isset($_GET['shed_id']))?$_GET['shed_id']:"";
                }

                $issue_list    = $objIssueItems->getPages($start,$total);
                $found_records = $objIssueItems->found_records;
?>
                <div id="bodyTab1">
                    <div id="form" style="margin: 20px auto;">
                        <table class="prom">
                            <thead>
                                <tr>
                                    <th class="col-xs-1 text-center">Sr.</th>
                                    <th class="col-xs-2 text-center">Issue Date</th>
                                    <th class="col-xs-2">Shed </th>
                                    <th class="col-xs-5">Issued Items </th>
                                    <th class="col-xs-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $counter = 1 + $start;
                                if(mysql_num_rows($issue_list)){
                                    while($issue_row   = mysql_fetch_assoc($issue_list)){
                                        $detail_list   = $objIssueItemDetails->getList($issue_row['ID']);
                                        $shed_title = $objChartOfAccounts->getAccountTitleByCode($issue_row['CHARGED_TO']);
                            ?>
                                <tr>
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-center"><?php echo date("d-m-Y",strtotime($issue_row['ISSUE_DATE'])); ?></td>
                                    <td class="text-center"><?php echo $shed_title; ?></td>
                                    <td class="text-left">
                                        <?php
                                            if(mysql_num_rows($detail_list)){
                                                while($detail_row = mysql_fetch_assoc($detail_list)){
                                                    $item_name = $objItems->getItemTitle($detail_row['ITEM_ID']);
                                                    echo $item_name." ( ".number_format($detail_row['QUANTITY'],2)." ) ";
                                                }
                                            }
                                        ?>
                                    </td>

                                    <td class="text-center">

                                        <a href="emb-issue-items.php?id=<?php echo $issue_row['ID'] ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                      <?php if(in_array('emb-issue-items-delete',$permissionz) || $admin == true){ ?>
                                        <button class="pointer row_main" do="<?php echo $issue_row['ID'] ?>"><i class="fa fa-times"></i></button>
                                      <?php } ?>
                                    </td>
                                </tr>
                            <?php
                                        $counter++;
                                    }
                                }else{
                            ?>
                                <tr>
                                    <td class="text-center" colspan="7">
                                        Found 0 Records.
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                        <div class="col-xs-12 text-center">
                                <nav>
                                  <ul class="pagination">
    <?php
                            $count = $found_records;
                            $total_pages = ceil($count/$total);
                            $i = 1;
                            $thisFileName = $_SERVER['PHP_SELF'];
                            if(isset($this_page) && $this_page>0){
    ?>
                                <li>
                                    <a href="<?php echo $thisFileName; ?>?page=1">First</a>
                                </li>
    <?php
                            }
                            if(isset($this_page) && $this_page>=1){
                                $prev = $this_page;
    ?>
                                <li>
                                    <a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
                                </li>
    <?php
                            }
                            $this_page_act = $this_page;
                            $this_page_act++;
                            while($total_pages>=$i){
                                $left = $this_page_act-5;
                                $right = $this_page_act+5;
                                if($left<=$i && $i<=$right){
                                $current_page = ($i == $this_page_act)?"active":"";
    ?>
                                    <li class="<?php echo $current_page; ?>">
                                        <?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                    </li>
    <?php
                                }
                                $i++;
                            }
                            $this_page++;
                            if(isset($this_page) && $this_page<$total_pages){
                                $next = $this_page;
                                echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
                            }
                            if(isset($this_page) && $this_page<$total_pages){
    ?>
                                    <li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
                                </ul>
                                </nav>
    <?php
                        }
    ?>
                        </div>
                    </div>
                </div>
<?php
            }
?>
<?php
            if($tab == 'search'){
?>
                <div id="form">
                    <form method="get" action="">
                        <div class="caption">From Date :</div>
                        <div class="field" style="width:150px;">
                            <input type="text" name="from_date" value="" class="form-control datepicker" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">To Date :</div>
                        <div class="field" style="width:150px;">
                            <input type="text" name="to_date" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Shed Expense A/c</div>
                        <div class="field">
                            <select class="show-tick shed_id form-control" name="shed_id" data-live-search="true">
                                <option value=""></option>
                                <?php
                                    if(mysql_num_rows($chart_of_account_list)){
                                        while($exp_acc = mysql_fetch_assoc($chart_of_account_list)){
                                            ?>
                                            <option value="<?php echo $exp_acc['ACC_CODE']; ?>" ><?php echo $exp_acc['ACC_TITLE']; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Item :</div>
                        <div class="field" style="width:250px;">
                            <select class="itemSelector show-tick form-control"
                                    data-style="btn-default"
                                    name="item_id"
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($itemsCategoryList)){
                            while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                                $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
                                    <optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
                                if(mysql_num_rows($itemList)){
                                    while($theItem = mysql_fetch_array($itemList)){
                                        if($theItem['ACTIVE'] == 'N'){
                                            continue;
                                        }
                                        if($theItem['INV_TYPE'] == 'B'){
                                            continue;
                                        }
?>
                                        <option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
                                    }
                                }
?>
                                    </optgroup>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field" style="width:150px;">
                            <input type="hidden" name="tab" value="list" />
                            <input type="submit" name="search" value="Search" class="button" />
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>
<?php
            }
            if($tab == 'new'){
?>
                <div id="bodyTab1">
                    <div id="form" style="margin: 20px auto;">
                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Issue Date</div>
                            <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                                <input type="text" name="issue_date" value="<?php echo ($issue != NULL)?date("d-m-Y",strtotime($issue['ISSUE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
                            </div>

                            <div class="caption" style="margin-left:0px;padding: 0px;">Shed Expense A/c</div>
                            <div class="field" style="width:220px;margin-left:0px;padding: 0px;">
                                <select class="expensed_to show-tick" data-width="220" data-live-search="true">
                                    <option value=""></option>
                                    <?php
                                        if(mysql_num_rows($chart_of_account_list)){
                                            while($exp_acc = mysql_fetch_assoc($chart_of_account_list)){
                                                $emp_selected = ($issue['CHARGED_TO'] == $exp_acc['ACC_CODE'])?"selected":"";
                                                ?>
                                                <option value="<?php echo $exp_acc['ACC_CODE']; ?>" <?php echo $emp_selected; ?> ><?php echo $exp_acc['ACC_TITLE']; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="field text-left">
                              <a target="_blank" class="btn btn-default ml-10 pull-left reload_shed"> <i class="fa fa-refresh"></i> </a>
                            </div>
                            <div class="clear"></div>

                            <div class="clear" style="height:10px;"></div>
                            <table class="prom">
                                <thead>
                                    <tr>
                                       <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">
                                            Item
                                            <a href="#" class="reload_item"><i class="fa fa-refresh pull-right" style="color:#06C;font-size:18px;margin-right:10px;" title="Refresh Items List" data-toggle="tooltip"></i></a>
                                       </th>
                                       <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">
                                            Machine
                                            <a href="#" class="reload_machine"><i class="fa fa-refresh pull-right" style="color:#06C;font-size:18px;margin-right:10px;" title="Refresh Machines List" data-toggle="tooltip"></i></a>
                                       </th>
                                       <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                       <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Cost</th>
                                       <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Total Cost</th>
                                       <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Stock In Hand</th>
                                       <th width="5%"   style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="quickSubmit" style="background:none">
                                        <td>
                                            <select class="itemSelector show-tick form-control"
                                                    data-style="btn-default"
                                                    data-live-search="true" style="border:none">
                                               <option selected value=""></option>
        <?php
                                        if(mysql_num_rows($itemsCategoryList)){
                                            while($ItemCat = mysql_fetch_array($itemsCategoryList)){
        										$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
        ?>
        											<optgroup label="<?php echo $ItemCat['NAME']; ?>">
        <?php
        										if(mysql_num_rows($itemList)){
        											while($theItem = mysql_fetch_array($itemList)){
        												if($theItem['ACTIVE'] == 'N'){
        													continue;
        												}
        												if($theItem['INV_TYPE'] == 'B'){
        													continue;
        												}
        ?>
                                               			<option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
        <?php
        											}
        										}
        ?>
        											</optgroup>
        <?php
                                            }
                                        }
        ?>
                                            </select>
                                        </td>
                                        <td class="text-center">
                                            <select name="machine_id" id="" class="machine_id form-control">
                                                <option value=""></option>
                                                <?php
                                                    if(mysql_num_rows($machine_list)){
                                                        while($machine = mysql_fetch_assoc($machine_list)){
                                                            ?>
                                                            <option value="<?php echo $machine['ID']; ?>" ><?php echo $machine['NAME']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="quantity form-control text-center"/>
                                        </td>
                                        <td>
                                            <input type="text" class="unitPrice form-control text-center"/>
                                        </td>
                                        <td>
                                            <input type="text"  readonly value="0" class="totalAmount form-control text-center"/>
                                        </td>
                                        <td>
                                            <input type="text" class="inStock form-control text-center" theStock="0" readonly />
                                        </td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                    </tr>
                                </tbody>
                                <tbody>
                                	<!--doNotRemoveThisLine-->
                                        <tr style="display:none;" class="calculations"></tr>
                                    <!--doNotRemoveThisLine-->
<?php
                            $whole_tax = 0;
        					if(isset($saleDetails) && mysql_num_rows($saleDetails)){
        						while($invRow = mysql_fetch_array($saleDetails)){
        							$itemName     = $objItems->getItemTitle($invRow['ITEM_ID']);
                                    $machine_name = $objMachines->getName($invRow['MACHINE_ID']);
?>
                                    <tr class="alt-row calculations transactions dynamix" data-row-id='<?php echo $invRow['ID']; ?>'>
                                        <td class="itemName text-left" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
                                        <td class="machine_id text-left" data-machine-id='<?php echo $invRow['MACHINE_ID']; ?>'><?php echo $machine_name; ?></td>
                                        <td style="text-align:center;" class="quantity"><?php echo $invRow['QUANTITY'] ?></td>
                                        <td style="text-align:center;" class="unitPrice"><?php echo $invRow['COST_PRICE'] ?></td>
                                        <td style="text-align:center;" class="totalAmount"><?php echo $invRow['TOTAL_COST'] ?></td>
                                        <td style="text-align:center;"> - - - </td>
                                        <td style="text-align:center;">
                                           <a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
                                           <a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
<?php
        						}
        					}
?>
                                    <tr class="totals">
                                        	<td style="text-align:center;background-color:#EEEEEE;" colspan="2">Total</td>
                                            <td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                            <td style="text-align:center;background-color:#f5f5f5;" class="amountTotal"></td>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                            <td style="text-align:center;background-color:#EEE;">- - -</td>
                                        </tr>
                                </tbody>
                            </table>
                        <div class="clear" style="height:10px;"></div>
                        <div class="panel panel-default pull-left" style="width:550px;">
                            <div class="panel-heading">Notes : </div>
                            <textarea class="form-control inv_notes" style="height:130px;border-radius:0px;"><?php echo $issue['NOTES']; ?></textarea>
                        </div>
                        <div class="clear"></div>
                        <div style="height: 10px;"></div>
                        <hr />
                        <div class="underTheTable col-md-4 pull-right" style="text-align: center;">
                            <input type="hidden" value="<?php echo $issue_id; ?>" name="issue_id" />
                            <?php if(((in_array('emb-issue-items-modify',$permissionz) || $admin == true) && $issue_id > 0) || $issue_id == 0 ){ ?>
			                      <div class="button savePurchase" style="margin-right: 20px;"><?php echo ($issue_id > 0)?"Update":"Save"; ?></div>
                            <?php } ?>
                            <div class="button" onclick="window.location.href='emb-issue-items.php?tab=new';">New Form</div>
                            <div class="clear"></div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
<?php
            }
?>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
        $("input[name=quotation_no]").keyup(function(e){
            var quotation_no  = $(this).val();
            if(e.keyCode == 13){
                window.location.href = "sale-details.php?qid="+quotation_no;
            }
        });
        $(".customer_mobile").bind('keyup blur',function(e){
            if($(this).val().substr(0,1) != 0 || ($(this).val().length > 1 && $(this).val().substr(1,1) != 3)){
            	$(".customer_mobile").val('');
            }
        });
        $(".reload_item").click(function(){
            $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(data){
                 $("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
            });
        });
		$('.supplierSelector').selectpicker();
		$('.itemSelector').selectpicker();
		$('.taxRate').selectpicker();
		$(this).calculateColumnTotals();
        $("label#mor1").tooltipster({
            theme: 'tooltipster-light'
        });
		$("input[name='billNum']").numericOnly();
		$("input.quantity").numericOnly();
        $(".whole_discount").numericFloatOnly();
		$("input[name='mobile_no']").numericOnly();
        $(".customer_mobile").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("input[name='billNum']");
		$("input[name='billNum']").setFocusTo(".dropdown-toggle:first");

        $("div.expensed_to button").keyup(function(e){
			if(e.keyCode == 13 && $('select.expensed_to option:selected').val() != ''){
				$("div.itemSelector button").focus();
			}
		});
		$("div.itemSelector button").keyup(function(e){
			if(e.keyCode == 13 && $('select.itemSelector option:selected').val() != ''){
				$("div.machine_id button").focus();
			}
		});
        var machine_focus_counter = 0;
        $("div.machine_id button").keydown(function(e){
            if(e.keyCode == 13){
                if(machine_focus_counter == 0){
                    machine_focus_counter = 1;
                }else if(machine_focus_counter == 1){
                    $("input.quantity").focus();
                    machine_focus_counter = 0;
                }
            }
		});
        $(document).keydown(function(e){
            if(e.keyCode == 32 && e.ctrlKey){
                $(".trasactionType").click();
                $(".supplier_name").focus();
            }
        });
        $("input.barcode_input").on('blur keyup',function(e){
            if(e.type == 'blur'||e.keyCode == 13){
                $(this).quickScan();
            }
        });
		$("div.itemSelector button").blur(function(e){
			if($('select.itemSelector option:selected').val() != ''){
				$(this).getItemDetails();
			}
		});
		var qtyE = 0;
		$("input.quantity").keyup(function(e){
            stockOs();
			if(e.keyCode == 13){
				if(parseFloat($(this).val())||0){
					$("input.unitPrice").focus();
				}
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
                if($("input.discount").length){
                    $("input.discount").focus();
                }else{
                    if($("input.taxRate").length){
                        $("input.taxRate").focus();
                    }else{
                        $(this).calculateRowTotal();
                        $(".addDetailRow").focus();
                    }

                }
			}
		});
        $("table").on('dblclick',"td.quantity",function(){
            var thisElm   = $(this);
            var thisQty   = parseFloat($(this).text())||0;
            var item_id   = parseFloat($(this).parent().find("[data-item-id]").attr("data-item-id"))||0;
            var itemPrice = parseFloat($(this).parent().find("td.unitPrice").text())||0;
            $.post('db/get-item-details.php',{s_item_id:item_id},function(data){
                data = $.parseJSON(data);
                var itemStock = parseFloat(data['STOCK'])||0;
                var this_sold = 0;
                $("td[data-item-id='"+item_id+"']").each(function(){
                    this_sold += parseFloat($(this).parent().find("td.quantity").text())||0;
                });
                itemStock += thisQty;
                itemStock -= this_sold;
                $(thisElm).append('<input class="form-control input-nested" value="'+thisQty+'" />');
                $(thisElm).find(".input-nested").focus();
                $(thisElm).find(".input-nested").on('keyup blur',function(e){
                    stockOsQuick($(this),itemPrice,itemStock);
                    var newQty = parseFloat($(this).val())||0;
                    if(e.keyCode == 13||e.type == 'blur'){
                        if(newQty!=0){
                            $(this).parent().text(newQty);
                            $(this).calculateColumnTotals();
                        }
                    }
                });
            });
        });
        $("input.inv_charges").on('change keyup',function(e){
            $(this).calculateColumnTotals();
        });
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});
        $("input.taxRate").keydown(function(e){
            if(e.keyCode == 13){
                $(this).calculateRowTotal();
                $(".addDetailRow").focus();
            }
        });
		$(".taxTd").find(".taxRate").change(function(){
			$(this).calculateRowTotal();
			$(".addDetailRow").focus();
		});

		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
			if(e.keyCode == 27){
				$(".taxTd").find(".dropdown-toggle").focus();
			}
		});
		$(".addDetailRow").dblclick(function(e){
			e.preventDefault;
		});
		$(".savePurchase").click(function(){
			saveSale();
		});
		$("input[name='mobile_no']").focus(function(){
			check_sms_service();
		});

        $(".whole_discount").on('blur change keyup',function(){
            $(this).calculateColumnTotals();
        });
        $(".whole_discount").blur();

        $(".discount_dom").on('blur change',function(){
            var dominate_discount = $(this).val();

            if(dominate_discount == ''){
                return;
            }

            var discount_type = $("input.discount_type:checked").val();
            if(discount_type == 'R'){
                dominate_discount = dominate_discount/(parseFloat($("tr.transactions").length)||0);
            }

            $("tr.transactions").each(function(){
                $(this).find("td.discount").text(dominate_discount);

                var taxType = ($(".taxType").is(":checked"))?"I":"E";
                var taxRate = parseFloat($(this).find("td.taxRate").text())||0;
                var thisVal = parseFloat($(this).find("td.quantity").text())||0;
                var thatVal = parseFloat($(this).find("td.unitPrice").text())||0;
                var amount  = Math.round((thisVal*thatVal)*100)/100;
                var discountAvail = parseFloat($(this).find("td.discount").text())||0;

                var discountPerCentage = 0;
                amount = Math.round(amount*100)/100;
                if($("input.individual_discount").val()=='Y'){
                    if(discount_type == 'R'){
                        discountPerCentage = discountAvail;
                    }else if(discount_type == 'P'){
                        discountAvail = Math.round(discountAvail*100)/100;
                        discountPerCentage = amount*(discountAvail/100);
                        discountPerCentage = Math.round(discountPerCentage*100)/100;
                    }
                    $(this).find("td.discount").attr('data-amount',discountPerCentage);
                    amount -= discountPerCentage;
                    amount = Math.round(amount*100)/100;
                }

                var taxAmount = 0;
                if(taxRate > 0){
                    if(taxType == 'I'){
                        taxAmount = amount*(taxRate/100);
                    }else if(taxType == 'E'){
                        taxAmount = amount*(taxRate/100);
                    }
                }
                if(taxType == 'I'){
                    amount -= taxAmount;
                }else{
                    amount += taxAmount;
                }
                $(this).find("td.totalAmount").text(amount);
            });
            $(this).calculateColumnTotals();
        });

        $("div.itemSelector button").focus();
        $(window).keyup(function(e){
            if(e.keyCode == 113){
                window.location.href = 'sale-details.php';
            }
        });
        $(window).keydown(function(e){
            if(e.altKey == true&&e.keyCode == 73){
                e.preventDefault();
                $("#form").click();
                $("div.itemSelector button").click();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 65){
                e.preventDefault();
                $("#form").click();
                $("div.supplierSelector button").click();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 66){
                e.preventDefault();
                $("#form").click();
                $("input.barcode_input").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 67){
                e.preventDefault();
                $("#form").click();
                $("input.supplier_name").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 77){
                e.preventDefault();
                $("#form").click();
                $("input.customer_mobile").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 78){
                e.preventDefault();
                $("#form").click();
                $("textarea.inv_notes").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 68){
                e.preventDefault();
                $("#form").click();
                $("input.whole_discount").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 79){
                e.preventDefault();
                $("#form").click();
                $("input.inv_charges").focus();
                return false;
            }
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
<?php
        if(isset($_GET['print'])){
?>
        var win = window.open("<?php echo $invoiceFile; ?>?id=<?php echo $issue['ID']; ?>","_blank");
        if(win){
            win.focus();
        }
<?php
        }
?>
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Record Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Record Updated Successfully!');
<?php
		}
?>

</script>
