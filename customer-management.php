<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/customers.php');

	//Permission
	if(!in_array('customer-management',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objCustomers = new Customers();

	if(isset($_POST['column_type'])){
		$colum_type = $_POST['column_type'];
		if($colum_type == 'area'){
			$colum_type = 'AREA';
		}
		if($colum_type == 'sector'){
			$colum_type = 'SECTOR';
		}
		$value 		 = mysql_real_escape_string($_POST['value']);
		$customer_id = mysql_real_escape_string($_POST['customer_id']);
		echo $objCustomers->update_column($colum_type,$value,$customer_id);
		exit();
	}

	//Pagination Settings
	$total = $objConfigs->get_config('PER_PAGE');
	//Pagination Settings { END }

	if(isset($_POST['customerSearch'])){
		$objCustomers->account_title = mysql_real_escape_string($_POST['accTitle']);
		$objCustomers->city 				 = mysql_real_escape_string($_POST['city_country']);
		$objCustomers->area 			   = mysql_real_escape_string($_POST['area']);
		$objCustomers->sector 			 = mysql_real_escape_string($_POST['sector']);
		$objCustomers->mobile 			 = mysql_real_escape_string($_POST['mobile']);	
	}

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$customerDetail = $objCustomers->search($start,$total);
	$found_records  = $objCustomers->found_records;
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
				<link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" />
				<link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" />
				<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
				<style type="text/css">
					th{
						font-size: 14px !important;
						padding: 5px !important;
					}
				</style>
      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class="summery_body">
               		<div class = "content-box-header">
                  		<p class="bold-text">Customers Management  <span class="badge text-primary"><?php echo $found_records; ?> Total Records</span></p>
                  		<span id="tabPanel">
                     		<div class="tabPanel">
                        		<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
														<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        		<a href="customer-detail.php"><div class="tab">New</div></a>
                     		</div>
                  		</span>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
               		<div class="clear"></div>

               		<div id = "bodyTab1" class="table-responsive">
                        <table class="table min-1178">
                            <thead>
                                <tr>
                                   	<th width="5%" 		class="text-center">Sr#</th>
																		<th width="5%" 		class="text-center">A/c Code</th>
                                    <th width="20%" 	class="text-center">Title</th>
                                   	<th width="20%" 	class="text-center">Contact Person</th>
                                   	<th width="10%" 	class="text-center">Area</th>
                                   	<th width="10%" 	class="text-center">Sector</th>
                                   	<th width="10%" 	class="text-center">Contact No.</th>
                                   	<th width="10%"  	class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="body_t">
<?php
						if(isset($customerDetail)){
							$counter = $start + 1;
							while($customerRow = mysql_fetch_array($customerDetail)){
?>
                                <tr id="recordPanel">
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-center"><?php echo $customerRow['CUST_ACC_CODE']; ?></td>
                                    <td class="customerNametd">
																			<span class="pull-left"><?php echo $customerRow['CUST_ACC_TITLE']; ?></span>
																			<span class="pull-right urdu-font"><?php echo $customerRow['URDU_TITLE']; ?></span>
																		</td>
                                    <td class="text-left"><?php echo $customerRow['CONT_PERSON']; ?></td>
                                    <td class="text-left text-info updatable" data-type="area" data-id="<?php echo $customerRow['CUST_ID']; ?>" ><?php echo $customerRow['AREA']; ?></td>
																		<td class="text-left text-info updatable" data-type="sector" data-id="<?php echo $customerRow['CUST_ID']; ?>" ><?php echo $customerRow['SECTOR']; ?></td>
                                    <td class="text-left text-primary">
                                        <?php echo ($customerRow['MOBILE'] != '')?$customerRow['MOBILE']:""; ?>
                                        <?php echo ($customerRow['MOBILE'] != ''&&$customerRow['PHONES'] != '')?",":""; ?>
                                        <?php echo ($customerRow['PHONES'] != '')?$customerRow['PHONES']:""; ?>
										<?php if($customerRow['WHATSAPP_MOBILE']=='Y'){ ?>
										<button type="button" class="btn btn-success btn-xs" disabled ><i class="fa fa-whatsapp" style="font-size: 18px !important;"></i></button>
										<?php } ?>
                                    </td>
                                    <td class="appendable" style="text-align:center;position:relative;">
	                                	<a id="view_button"  href="customer-detail.php?cid=<?php echo $customerRow['CUST_ID']; ?>"><i class="fa fa-pencil"></i></a>
	                                    <a type="button" do="<?php echo $customerRow['CUST_ACC_CODE']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
                            		</td>
                                </tr>
<?php
							$counter++;
							}
						}
?>
                            </tbody>
                        </table>
						<div class="col-xs-12 text-center">

									<?php
									if($found_records > $total){
										$get_url = "";
										foreach($_GET as $key => $value){
											$get_url .= ($key == 'page')?"":"&".$key."=".$value;
										}
										?>
										<nav>
											<ul class="pagination">
												<?php
												$count = $found_records;
												$total_pages = ceil($count/$total);
												$i = 1;
												$thisFileName = basename($_SERVER['PHP_SELF']);
												if(isset($this_page) && $this_page>0){
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
													</li>
													<?php
												}
												if(isset($this_page) && $this_page>=1){
													$prev = $this_page;
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
													</li>
													<?php
												}
												$this_page_act = $this_page;
												$this_page_act++;
												while($total_pages>=$i){
													$left = $this_page_act-5;
													$right = $this_page_act+5;
													if($left<=$i && $i<=$right){
														$current_page = ($i == $this_page_act)?"active":"";
														?>
														<li class="<?php echo $current_page; ?>">
															<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
														</li>
														<?php
													}
													$i++;
												}
												$this_page++;
												if(isset($this_page) && $this_page<$total_pages){
													$next = $this_page;
													?>
													<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
													<?php
												}
												if(isset($this_page) && $this_page<$total_pages){
													?>
													<li>
														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
													</li>
												</ul>
											</nav>
											<?php
										}
									}
									?>
								</div>
                        <div style = "clear:both; height:20px"></div>
               		</div><!-- End #bodyTab1 -->

               		<div id = "bodyTab2" style="display:none">
                  		<div id = "form">
                     	<form method="post" action="customer-management.php?tab=list">
                            <div class="caption">Title</div>
                            <div class="field">
                               <input name="accTitle" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>

														<div class="caption">Mobile</div>
                            <div class="field">
                               <input name="mobile" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>

														<div class="caption">Sector</div>
                            <div class="field">
                               <input name="sector" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>

														<div class="caption">Area</div>
                            <div class="field">
                               <input name="area" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>

														<div class="caption">City/Country</div>
                            <div class="field">
                               <input name="city_country" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>

                            <div class="caption"></div>
                            <div class="field">
                               <input name="customerSearch" type="submit" value="Search" class="button" />
                            </div>
                            <div class="clear"></div>
                     	</form>
                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab2-->
            	</div><!-- End summer -->
         	</div><!-- End .content-box -->
			<div id="confirmation" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Update Field</h4>
							</div>
							<div class="modal-body">
								<input type="text" class="form-control td_update_value"  />
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
      	</div><!--body-wrapper-->
        <div id="xfade"></div>
        <div id="fade"></div>
   	</body>
    <script>
		$(document).ready(function() {
			$(document).keyup(function(e){
				if(e.keyCode==27){
					$("#popUpDel,#xfade").fadeOut();
				}
			});
			$("td.updatable").dblclick(function(){
				var col_value   = $(this).text();
				var column_type = $(this).attr("data-type");
				var data_id     = $(this).attr("data-id");
				$(this).addClass('bg-danger');
				$("#confirmation").modal('show');
				setTimeout(function(){
					$('input.td_update_value').focus();
					$('input.td_update_value').val(col_value);
					$('input.td_update_value').attr('data-type',column_type);
					$('input.td_update_value').attr('data-id',data_id);
					$('input.td_update_value').keydown(function(e){
						if(e.keyCode == 13){
							update_column_value($(this));
							$('input.td_update_value').blur();
						}
					});
				},200);
			});
			$("#confirmation").on("hidden.bs.modal", function () {
				$("td.updatable.bg-danger").removeClass("bg-danger");
			});
			$(".pointer").click(function(){
   				var acc_code = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.get('db/del-account-code.php', {acc_code : acc_code}, function(data){
							data = $.parseJSON(data);
							$("#popUpDel").children(".confirm").text(data['MSG']);
							$("#popUpDel").children(".dodelete").hide();
							$("#popUpDel").children(".nodelete").text("Close");
							if(data['OK'] == 'Y'){
								clickedDel.parent('td').parent('tr').remove();
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
				});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#fade").fadeOut('fast');
				});
   			});
			$("#tab1,#tab2").click(function(){
				$(".h3_php_error").show();
			});
			$(".getAccTitle").keyup(function(){
				var codeCode = $(this).val();
				$.post('db/get-customer-title.php',{title:codeCode},function(data){
					var gotCode = $.parseJSON(data);
					var titles = [];
					$.each(gotCode,function(index,value){
						$.each(value,function(i,v){
							if(i=='CUST_ACC_TITLE'){
								titles.push(v);
							}
						});
					});
					$(".getAccTitle").autocomplete({
							source: titles,
							width: 300,
							scrollHeight:220,
							matchContains: true,
							selectFirst: false,
							minLength: 1
					});
					$.each(titles,function(i,v){
						if(codeCode==v){
							$(".insertAccCode").val(gotCode[0]['CUST_ACC_CODE']);
						}else{
							$(".insertAccCode").val("");
						}
					});
				});
			});
		});
		var update_column_value = function(elm){
			var col_val = $(elm).val();
			var col_typ = $(elm).attr('data-type');
			var col_id  = $(elm).attr('data-id');

			$.post('customer-management.php',{column_type:col_typ,value:col_val,customer_id:col_id},function(data){
				$("td.updatable.bg-danger").text(col_val).removeClass("bg-danger");
				$("#confirmation").modal('hide');
			});
		}
	</script>
    <script>
<?php
	if(isset($_GET['tab']) && $_GET['tab'] == 'list'){
?>
		tab('1', '1', '2');
<?php
	}elseif(isset($_GET['tab']) && $_GET['tab'] == 'search'){
?>
		tab('2', '1', '2');
<?php
	}
?>
	</script>
</html>
<?php include('conn.close.php'); ?>
