<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/dist_sale.php');
	include('common/classes/dist_sale_details.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/dist_sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');
	include('common/classes/j-voucher.php');

	//Permission
	if(!in_array('distributor-sale-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objUserAccounts      = new UserAccounts();
	$objAccountCodes      = new ChartOfAccounts();
	$objSales 			  		= new DistributorSale();
	$objSaleDetails       = new DistributorSaleDetails();
	$objSaleReturn        = new DistributorSaleReturn();
	$objSaleReturnDetails = new DistributorSaleReturnDetails();
	$objItems             = new Items();
	$objServices          = new Services();
	$objItemCategory      = new itemCategory();
	$objTaxRates          = new TaxRates();
	$objDepartments       = new Departments();
	$objConfigs 		 		  = new Configs();
	$objJournalVoucher    = new JournalVoucher();

	$suppliersList   = $objAccountCodes->getAccountByCatAccCode('010104');
	$cashAccounts  	 = $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList     = $objTaxRates->getList();

	$brands_list     = $objItems->getBrandNameList();

	$currency_type 	 = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   	 = $objConfigs->get_config('USE_CARTONS');

	$salesman_only = false;
	$report_date = date("d-m-Y");
	if(isset($_GET['search'])){
		$report_date 		= ($_GET['report_date']=='')?"":date('Y-m-d',strtotime($_GET['report_date']));
		if($report_date != ''){
			$objSales->saleDate 			= $report_date;
			if(isset($_GET['salesman'])){
				$objSales->user_id 			= ($_SESSION['classuseid'] == 1)?$_GET['salesman']:$_GET['salesman'];
			}else{
				$objSales->user_id 			= $_SESSION['classuseid'];
			}
			$objSales->order_taker 		= isset($_GET['order_taker'])?$_GET['order_taker']:"";
			$gate_pass 								= isset($_GET['gate_pass'])?"Y":"N";
		}
		if($objSales->user_id!=''&&$objSales->order_taker==''){
			$salesman_only = true;
		}
		$show_report = true;
	}


	if(isset($show_report)){

		$dist_salesman_report 			 = $objSales->distSalesmanReport();

		$objSaleReturn->saleDate 		 = $objSales->saleDate;
		$objSaleReturn->order_taker  	 = $objSales->order_taker;
		$objSaleReturn->user_id 		 = $objSales->user_id;

		$dist_salesman_return_report = $objSaleReturn->distSalesmanReport();

		$report_rows = array();
		$return_rows = array();

		$table_sale_rows   = array();
		$table_return_rows = array();

		$items_affected    = array();
		$user_id_list      = array();

		$issue_ctn_col_total = 0;
		$issue_dzn_col_total = 0;
		$issue_pcs_col_total = 0;
		$rtrn_ctn_col_total  = 0;
		$rtrn_dzn_col_total  = 0;
		$rtrn_pcs_col_total  = 0;
		$sale_ctn_col_total  = 0;
		$sale_dzn_col_total  = 0;
		$sale_pcs_col_total  = 0;
		$tamount_ctn_col_total  = 0;
		$tamount_dzn_col_total  = 0;
		$tamount_pcs_col_total  = 0;
		$grand_col_total  = 0;

		if(mysql_num_rows($dist_salesman_report)){
			while($dist_sale_row = mysql_fetch_assoc($dist_salesman_report)){
				if($salesman_only){
					$dist_sale_row['ORDER_TAKER'] = 0;
				}
				if(!isset($report_rows[$dist_sale_row['ORDER_TAKER']])){
					$report_rows[$dist_sale_row['ORDER_TAKER']] = array();
				}
				if(!isset($report_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']])){
					$report_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']] = array();
				}
				$report_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']][] = $dist_sale_row;
			}
		}

		if(mysql_num_rows($dist_salesman_return_report)){
			while($dist_sale_row = mysql_fetch_assoc($dist_salesman_return_report)){
				$row_index = $dist_sale_row['USER_ID'].".".$dist_sale_row['ORDER_TAKER'];
				if($salesman_only){
					$dist_sale_row['ORDER_TAKER'] = 0;
				}
				if(!isset($return_rows[$dist_sale_row['ORDER_TAKER']])){
					$return_rows[$dist_sale_row['ORDER_TAKER']] = array();
				}
				if(!isset($return_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']])){
					$return_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']] = array();
				}
				$return_rows[$dist_sale_row['ORDER_TAKER']][$dist_sale_row['USER_ID']][] = $dist_sale_row;
			}
		}

		foreach($report_rows as $order_taker => $user_id_row){
			if(!isset($user_id_list[$order_taker])){
				$user_id_list[$order_taker] = array();
			}
			foreach($user_id_row as $user_id => $report_row){
				if(!in_array($user_id,$user_id_list[$order_taker])){
					$user_id_list[$order_taker][] = $user_id;
				}
				foreach($report_row as $key => $roow){
					$detail_list = $objSaleDetails->getList($roow['ID']);
					if(mysql_num_rows($detail_list)){
						while($row = mysql_fetch_assoc($detail_list)){

							if(!isset($table_sale_rows[$order_taker])){
								$table_sale_rows[$order_taker] = array();
							}
							if(!isset($table_sale_rows[$order_taker][$user_id])){
								$table_sale_rows[$order_taker][$user_id] = array();
							}
							if(!isset($table_sale_rows[$order_taker][$user_id][$row['ITEM_ID']])){
								$table_sale_rows[$order_taker][$user_id][$row['ITEM_ID']] = array();
							}
							$item_details 		   = $objItems->getRecordDetails($row['ITEM_ID']);
							$row['ITEM_NAME'] 	   = $item_details['NAME'];
							$row['QTY_PER_CARTON'] = $item_details['QTY_PER_CARTON'];
							$row['IS_CARTON']  	   = $item_details['IS_CARTON'];
							$row['IS_DOZEN']  	   = $item_details['IS_DOZEN'];
							$table_sale_rows[$order_taker][$user_id][$row['ITEM_ID']][] = $row;

							$items_affected[] = $row['ITEM_ID'];
						}
					}
				}
			}
		}
		foreach($return_rows as $order_taker => $user_id_row){
			if(!isset($user_id_list[$order_taker])){
				$user_id_list[$order_taker] = array();
			}
			foreach($user_id_row as $user_id => $report_row){
				if(!in_array($user_id,$user_id_list[$order_taker])){
					$user_id_list[$order_taker][] = $user_id;
				}
				foreach($report_row as $key => $roow){
					$detail_list = $objSaleReturnDetails->getList($roow['ID']);
					if(mysql_num_rows($detail_list)){
						while($row = mysql_fetch_assoc($detail_list)){
							if(!isset($table_return_rows[$order_taker])){
								$table_return_rows[$order_taker] = array();
							}
							if(!isset($table_return_rows[$order_taker][$user_id])){
								$table_return_rows[$order_taker][$user_id] = array();
							}
							if(!isset($table_return_rows[$order_taker][$user_id][$row['ITEM_ID']])){
								$table_return_rows[$order_taker][$user_id][$row['ITEM_ID']] = array();
							}
							$item_details 		   = $objItems->getRecordDetails($row['ITEM_ID']);
							$row['ITEM_NAME'] 	   = $item_details['NAME'];
							$row['QTY_PER_CARTON'] = $item_details['QTY_PER_CARTON'];
							$row['IS_CARTON']  	   = $item_details['IS_CARTON'];
							$row['IS_DOZEN']  	   = $item_details['IS_DOZEN'];
							$table_return_rows[$order_taker][$user_id][$row['ITEM_ID']][] = $row;

							$items_affected[] = $row['ITEM_ID'];
						}
					}
				}
			}
		}
		$items_affected = (count($items_affected)>0)?array_unique($items_affected):$items_affected;
	}
	//$user_id_list 	= $user_id_list;

?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css"	  										 type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" 												 type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" 											 type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" 												 type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" 												 type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css"  										 type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" 								 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" 								 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" 						 type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<style>
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js" ></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$("td.num-test").each(function(){
			if((parseInt($(this).text())||0)==0) $(this).text('');
		});
		$("tr.append-to-last-table").clone().appendTo("table.tableBreak:last tfoot");
		$("table.removable-table").remove();
		$(".printThis").click(function(){
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports-horizontal.css',
				pageTitle: "Sitsbook",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$("select").selectpicker();
	});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Distributor Sales Report</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="">
							<div class="caption">Report Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="report_date" value="<?php echo date('d-m-Y',strtotime($report_date)); ?>" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>
							<?php
							if($_SESSION['classuseid'] == 1){
								$sales_man_arr = array();
								$users_list = $objAccounts->getActiveList();
								if(mysql_num_rows($users_list)){
									while($user = mysql_fetch_assoc($users_list)){
										if($user['DESIGNATION_TYPE'] == ''){
											//$user['DESIGNATION_TYPE'] = 'S';
											continue;
										}
										if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
											$sales_man_arr[$user['DESIGNATION_TYPE']] = array();
										}
										$sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
									}
								}
								?>
								<div class="caption">OrderTaker</div>
								<div class="field" style="width:270px;position:relative;">
									<select class="user_id" name="order_taker">
										<option value=""></option>
										<?php
										if(isset($sales_man_arr['O'])){
											foreach ($sales_man_arr['O'] as $key => $user){
													?>
													<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
													<?php
											}
										}
										 ?>
									</select>
								</div>
								<div class="clear"></div>

								<div class="caption">Salesman</div>
								<div class="field" style="width:270px;position:relative;">
									<select class="user_id" name="salesman">
										<option value=""></option>
										<?php
										if(isset($sales_man_arr['S'])){
											foreach ($sales_man_arr['S'] as $key => $user){
													?>
													<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
													<?php
											}
										}
										 ?>
									</select>
								</div>
								<div class="clear"></div>
								<?php
							}
							?>

							<div class="caption">Gate Pass :</div>
							<div class="field">
								<input id="cmn-toggle-gatepass" value="Y" class="css-checkbox" type="checkbox" name="gate_pass"  />
								<label for="cmn-toggle-gatepass" class="css-label" style="margin-top:5px;margin-right:-25px;"></label>
							</div>
							<div class="clear"></div>

							<div class="caption"></div>
							<div class="field">
								<input type="submit" value="Search" name="search" class="button"/>
							</div>
							<div class="clear"></div>
			</form>
		</div><!--form-->

		<?php
		if(isset($show_report)){
			?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							<?php
								if($salesman_only){
									$sales_man_name = $objUserAccounts->getFullName($objSales->user_id);
								}
							?>
							Distributor Sales Report <?php echo ($salesman_only)?" - ".$sales_man_name['FIRST_NAME']." ".$sales_man_name['LAST_NAME']:""; ?>
						</p>
						<p style="font-size:16px;text-align:left;padding: 0px;">
							<?php echo ($report_date=="")?"":"Report Date ".date('d-m-Y',strtotime($report_date)); ?>
						</p>

						<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
						<div class="clear"></div>
					</div>

<?php
				foreach($user_id_list as $order_taker_id => $user_id_list_salesman){
					$order_taker_name = $objUserAccounts->getFullName($order_taker_id);
 ?>
						 <div class="caption_report pull-left">
							 Order Taker : <?php echo $order_taker_name['FIRST_NAME']." ".$order_taker_name['LAST_NAME']; ?>
						 </div>
						 <div class="clear-right <?php echo (!$salesman_only)?"clear-left":""; ?>"></div>
<?php
					foreach($user_id_list_salesman as $key => $salesman_id){
						$sales_man_name = $objUserAccounts->getFullName($salesman_id);
?>
						<?php if(!$salesman_only){ ?>
						<div class="caption_report pull-left">
							Sales Man : <?php echo $sales_man_name['FIRST_NAME']." ".$sales_man_name['LAST_NAME']; ?>
						</div>
						<?php } ?>
						<div class="caption_report  pull-right">
							<span>Recovery Amount : <?php echo $objJournalVoucher->getSalesmanReceiptAmount($report_date,$order_taker_id,$salesman_id); ?></span>
						</div>
						<div class="clear"></div>

						<table class="tableBreak">
							<thead class="tHeader">
								<tr style="background:#EEE;">
									<th class="text-center" rowspan="2">PRODUCT</th>
									<th class="text-center" colspan="3">ISSUE STOCK</th>
									<?php if($gate_pass == 'N'){ ?>
									<th class="text-center" colspan="3">RTN STOCK</th>
									<th class="text-center" colspan="3">SALE</th>
									<th class="text-center" colspan="3">RATE</th>
									<th class="text-center" colspan="3">TOTAL AMOUNT</th>
									<th class="text-center" rowspan="2">GRAND TOTAL</th>
									<?php } ?>
								</tr>
								<tr>
									<th class="text-center">CTN</th>
									<th class="text-center">DZN</th>
									<th class="text-center">PCS</th>

									<?php if($gate_pass == 'N'){ ?>
									<th class="text-center">CTN</th>
									<th class="text-center">DZN</th>
									<th class="text-center">PCS</th>

									<th class="text-center">CTN</th>
									<th class="text-center">DZN</th>
									<th class="text-center">PCS</th>

									<th class="text-center">CTN</th>
									<th class="text-center">DZN</th>
									<th class="text-center">PCS</th>

									<th class="text-center">CTN</th>
									<th class="text-center">DZN</th>
									<th class="text-center">PCS</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php
									if(!$salesman_only){
										$issue_ctn_col_total = 0;
										$issue_dzn_col_total = 0;
										$issue_pcs_col_total = 0;
										$rtrn_ctn_col_total  = 0;
										$rtrn_dzn_col_total  = 0;
										$rtrn_pcs_col_total  = 0;
										$sale_ctn_col_total  = 0;
										$sale_dzn_col_total  = 0;
										$sale_pcs_col_total  = 0;
										$tamount_ctn_col_total  = 0;
										$tamount_dzn_col_total  = 0;
										$tamount_pcs_col_total  = 0;
										$grand_col_total  = 0;
									}
									$sale_discount = 0;
									foreach($items_affected as $key => $item_id){
										if(!isset($table_sale_rows[$order_taker_id][$salesman_id][$item_id]) && !isset($table_return_rows[$order_taker_id][$salesman_id][$item_id])){
											continue;
										}
										$item_details  = $objItems->getRecordDetails($item_id);

										$dist_sales    = isset($table_sale_rows[$order_taker_id][$salesman_id][$item_id])?$table_sale_rows[$order_taker_id][$salesman_id][$item_id]:NULL;
										$rtn_sales     = isset($table_return_rows[$order_taker_id][$salesman_id][$item_id])?$table_return_rows[$order_taker_id][$salesman_id][$item_id]:NULL;

										$issue_detail  									= array();
										$issue_detail['TOTAL_CARTONS']  				= 0;
										$issue_detail['TOTAL_DOZENS']  				    = 0;
										$issue_detail['TOTAL_QTY'] 						= 0;
										$issue_detail['RATE_CARTON'] 					= 0;
										$issue_detail['RATE_DOZEN'] 					= 0;
										$issue_detail['UNIT_PRICE'] 					= 0;
										$issue_detail['TOTAL_AMOUNT'] 					= 0;


										$rtn_detail    									= array();
										$rtn_detail['TOTAL_CARTONS']  					= 0;
										$rtn_detail['TOTAL_DOZENS']  					= 0;
										$rtn_detail['TOTAL_QTY'] 						= 0;
										$rtn_detail['RATE_CARTON'] 						= 0;
										$rtn_detail['RATE_DOZEN'] 						= 0;
										$rtn_detail['UNIT_PRICE'] 						= 0;
										$rtn_detail['TOTAL_AMOUNT'] 					= 0;

										$avg_unit_rate = 0;
										$avg_crtn_rate = 0;

										$avg_unit_count = 0;
										$avg_crtn_count = 0;

										if(count($dist_sales)){
											foreach($dist_sales as $key => $sale_detail_row){
												if($sale_detail_row['IS_CARTON'] == 'Y'){
													$sale_detail_row['CARTONS'] += (floor($sale_detail_row['QUANTITY']/$sale_detail_row['QTY_PER_CARTON']));
													$sale_detail_row['QUANTITY'] = $sale_detail_row['QUANTITY'] % $sale_detail_row['QTY_PER_CARTON'];
												}
												//if qty more than a dozen
												if($sale_detail_row['IS_DOZEN'] == 'Y'){
													$sale_detail_row['DOZENS'] += (floor($sale_detail_row['QUANTITY']/12));
													$sale_detail_row['QUANTITY'] = $sale_detail_row['QUANTITY'] % 12;
												}

												$issue_detail['TOTAL_CARTONS'] += $sale_detail_row['CARTONS'];
												$issue_detail['TOTAL_DOZENS']  += $sale_detail_row['DOZENS'];
												$issue_detail['TOTAL_QTY']     += $sale_detail_row['QUANTITY'];
												$issue_detail['RATE_CARTON']   += $sale_detail_row['RATE_CARTON'];
												$issue_detail['RATE_DOZEN']   += $sale_detail_row['DOZEN_PRICE'];
												$issue_detail['UNIT_PRICE']    += $sale_detail_row['UNIT_PRICE'];
												$issue_detail['TOTAL_AMOUNT']  += $sale_detail_row['TOTAL_AMOUNT'];
											}
										}
										if(count($rtn_sales)){
											foreach($rtn_sales as $key => $return_detail_row){

												if($return_detail_row['IS_CARTON'] == 'Y'){
													$return_detail_row['CARTONS'] += (floor($return_detail_row['QUANTITY']/$return_detail_row['QTY_PER_CARTON']));
													$return_detail_row['QUANTITY'] = $return_detail_row['QUANTITY'] % $return_detail_row['QTY_PER_CARTON'];
												}
												//if qty more than a dozen
												if($return_detail_row['IS_DOZEN'] == 'Y'){
													$return_detail_row['DOZENS'] += (floor($return_detail_row['QUANTITY']/12));
													$return_detail_row['QUANTITY'] = $return_detail_row['QUANTITY'] % 12;
												}

												$rtn_detail['TOTAL_CARTONS'] += $return_detail_row['CARTONS'];
												$rtn_detail['TOTAL_DOZENS'] += $return_detail_row['DOZENS'];
												$rtn_detail['TOTAL_QTY']  	 += $return_detail_row['QUANTITY'];
												$rtn_detail['RATE_CARTON']   += $return_detail_row['RATE_CARTON'];
												$rtn_detail['RATE_DOZEN']   += $return_detail_row['DOZEN_PRICE'];
												$rtn_detail['UNIT_PRICE']  	 += $return_detail_row['UNIT_PRICE'];
												$rtn_detail['TOTAL_AMOUNT']  += $return_detail_row['TOTAL_AMOUNT'];
											}
										}

										$sale_ctn      = $issue_detail['TOTAL_CARTONS'] - $rtn_detail['TOTAL_CARTONS'];
										$sale_dzn      = $issue_detail['TOTAL_DOZENS'] - $rtn_detail['TOTAL_DOZENS'];
										$sale_pcs      = $issue_detail['TOTAL_QTY']     - $rtn_detail['TOTAL_QTY'];

										$sale_total_val= $issue_detail['TOTAL_AMOUNT']-$rtn_detail['TOTAL_AMOUNT'];

										$total_pcs     = ($sale_ctn*$item_details['QTY_PER_CARTON'])+($sale_dzn*12)+$sale_pcs;

										if($total_pcs==0){
											$unit_rt       = 0;
										}else{
											$unit_rt       = $sale_total_val/$total_pcs;
										}


										$cartn_rt      = $unit_rt*$item_details['QTY_PER_CARTON'];

										$dzn_rate      = ($issue_detail['RATE_DOZEN']/count($dist_sales));


										$amount_ctn    = $sale_ctn*$cartn_rt;
										$amount_dzn    = $sale_dzn*$dzn_rate;
										$amount_qty    = $sale_pcs*$unit_rt;
								 ?>
									<tr>
										<td class="text-left"><?php echo $item_details['NAME']; ?></td>
										<td class="text-center num-test sum"><?php echo $issue_detail['TOTAL_CARTONS']; ?></td>
										<td class="text-center num-test sum"><?php echo $issue_detail['TOTAL_DOZENS']; ?></td>
										<td class="text-center num-test sum"><?php echo $issue_detail['TOTAL_QTY']; ?></td>
										<?php if($gate_pass == 'N'){ ?>
										<td class="text-center num-test sum"><?php echo $rtn_detail['TOTAL_CARTONS']; ?></td>
										<td class="text-center num-test sum"><?php echo $rtn_detail['TOTAL_DOZENS']; ?></td>
										<td class="text-center num-test sum"><?php echo $rtn_detail['TOTAL_QTY']; ?></td>
										<td class="text-center num-test sum"><?php echo $sale_ctn; ?></td>
										<td class="text-center num-test sum"><?php echo $sale_dzn; ?></td>
										<td class="text-center num-test sum"><?php echo $sale_pcs; ?></td>
										<td class="text-center num-test"><?php echo number_format($cartn_rt,2); ?></td>
										<td class="text-center num-test"><?php echo number_format($dzn_rate,2); ?></td>
										<td class="text-center num-test"><?php echo number_format($unit_rt,2); ?></td>
										<td class="text-center num-test sum"><?php echo number_format($amount_ctn,2); ?></td>
										<td class="text-center num-test sum"><?php echo number_format($amount_dzn,2); ?></td>
										<td class="text-center num-test sum"><?php echo number_format($amount_qty,2); ?></td>
										<td class="text-center num-test sum"><?php echo number_format($sale_total_val,2); ?></td>
										<?php } ?>
									</tr>
								<?php
									$issue_ctn_col_total  += $issue_detail['TOTAL_CARTONS'];
									$issue_dzn_col_total  += $issue_detail['TOTAL_DOZENS'];
									$issue_pcs_col_total  += $issue_detail['TOTAL_QTY'];


									$rtrn_ctn_col_total += $rtn_detail['TOTAL_CARTONS'];
									$rtrn_dzn_col_total += $rtn_detail['TOTAL_DOZENS'];
									$rtrn_pcs_col_total += $rtn_detail['TOTAL_QTY'];

									$sale_ctn_col_total += $sale_ctn;
									$sale_dzn_col_total += $sale_dzn;
									$sale_pcs_col_total += $sale_pcs;

									$tamount_ctn_col_total += $amount_ctn;
									$tamount_dzn_col_total += $amount_dzn;
									$tamount_pcs_col_total += $amount_qty;
									$grand_col_total += $sale_total_val;
								 }
								 $sale_discount = $objSales->getDistributorBillDiscount($report_date,$order_taker_id,$salesman_id);
								 $grand_col_total 		   -= $sale_discount;
								?>
								</tbody>
								<tfoot class="tableFooter">
									<?php if($gate_pass == 'N'){ ?>
									<tr>
										<th class="text-right" colspan="16"><span class="pull-right mr-10">Discount</span></th>
										<th class="text-center">
											<?php echo $sale_discount; ?>
										</th>
									</tr>
									<?php } ?>
									<?php if(!$salesman_only){ ?>
									<tr class="total_tf">
										<th class="text-center">Total:</th>
										<th class="text-center num-test"><?php echo number_format($issue_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($issue_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($issue_pcs_col_total); ?></th>
										<?php if($gate_pass == 'N'){ ?>
										<th class="text-center num-test"><?php echo number_format($rtrn_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($rtrn_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($rtrn_pcs_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_pcs_col_total); ?></th>
										<th class="text-center num-test" colspan="3">- - - - -</th>
										<th class="text-center num-test"><?php echo number_format($tamount_ctn_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format($tamount_dzn_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format($tamount_pcs_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format(round($grand_col_total),2); ?></th>
										<?php } ?>
									</tr>
									<?php } ?>
								</tfoot>
							</table>
<?php
					}
				}
?>
							<?php if($salesman_only){ ?>
							<table class="removable-table">
								<tfoot>
									<tr class="total_tf append-to-last-table">
										<th class="text-center">Total:</th>
										<th class="text-center num-test"><?php echo number_format($issue_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($issue_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($issue_pcs_col_total); ?></th>
										<?php if($gate_pass == 'N'){ ?>
										<th class="text-center num-test"><?php echo number_format($rtrn_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($rtrn_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($rtrn_pcs_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_ctn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_dzn_col_total); ?></th>
										<th class="text-center num-test"><?php echo number_format($sale_pcs_col_total); ?></th>
										<th class="text-center num-test" colspan="3">- - - - -</th>
										<th class="text-center num-test"><?php echo number_format($tamount_ctn_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format($tamount_dzn_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format($tamount_pcs_col_total,2); ?></th>
										<th class="text-center num-test"><?php echo number_format(round($grand_col_total),2); ?></th>
										<?php } ?>
									</tr>
								</tfoot>
							</table>
							<?php } ?>
							<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					} //end if is generic report
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
</script>
