/* global displayToastMessage */
/* global displayMessage */
/* global $ */
var balance_recovered = "";
var stock_check       = 'Y';
var print_method      = '';
var redirect_method   = 'U';
var use_cartons       = '';
$(document).ready(function(){
	//change status
	$(".booking_status_change").change(function(){
		var booking_status = $(this).val();
		var row_id = $(this).attr("row-id");
		$.post("db/update_booking_status.php",{booking_status:booking_status,row_id:row_id});
	});

	$("input.total_discount").prop("readonly",true);
	print_method   = $("input.print_method").val();
	use_cartons    = $("input.use_cartons").val();
	stock_check = $("input.stock_check").val();
	stock_check     = (stock_check == 'Y')?true:false;
	balance_recovered = ($("input.recovered_balance").is(":checked"))?"Y":"N";
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.numericFloatOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
					||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
					|| (e.keyCode == 65 && e.ctrlKey === true)
					|| (e.keyCode >= 35 && e.keyCode <= 39)){
						return true;
					}else{
						if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
							e.preventDefault();
						}
					}
				});
			};
			$("select.supplierSelector").change(function(){
				getAccountBalance();
			});
			$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
			$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
			$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
			$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
				function() {
					$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
					$(this).addClass('current'); // Add class "current" to clicked tab
					var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
					$(currentTab).siblings().hide(); // Hide all content divs
					$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
					return false;
				}
			);

			//Close button:

			$(".close").click(
				function () {
					$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);
		// Alternating table rows:
		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
		// Check all checkboxes when the one in a table head is checked:
		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.sumColumn = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseInt($(this).text());
			});
			$(insertToFeild).text(sumAll);
		};
		$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseFloat($(this).text())||0;
			});
			sumAll = Math.round(sumAll*100)/100;
			$(insertToFeild).text((sumAll).toFixed(2));
		};
		$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
			$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
		};
		$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
			$(this).keyup(function(e){
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var productVal = Math.round((thisVal*thatVal)*100)/100;
				$(writeProductToElm).val(productVal);
			});
		};
		$.fn.deleteMainRow = function(file){
			var idValue    = $(this).attr("do");
			var currentRow = $(this).parent().parent().parent().parent().parent();
			var orderNumbr  = currentRow.find('td').first().text();
			$("#xfade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#xfade").fadeIn();
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$("#popUpDel").height()/2;
			win_width = win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel .confirm").text("Are Sure you Want To Delete Order #"+orderNumbr+"?");
			$("#popUpDel").slideDown();
			$(".dodelete").click(function(){
				$.post(file, {cid : idValue}, function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("tr[data-row-id='"+idValue+"']").slideUp();
					}
					$("#popUpDel .confirm").text(data['MSG']);
					$(".nodelete").text('Close');
					$(".dodelete").hide();
				});
			});
			$(".nodelete").click(function(){
				$("#popUpDel").slideUp(function(){
					$(this).remove();
				});
				$("#xfade").fadeOut();
			});
		};

		$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
			$(this).keyup(function(e){
				var taxRate = $("input.taxRate").val();
				var taxType   = ($(".taxType").is(":checked"))?"I":"E";
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var amount = Math.round((thisVal*thatVal)*100)/100;
				if(taxRate > 0){
					if(taxType == 'I'){
						taxAmount = amount*(taxRate/100);
						amount -= taxAmount;
					}else if(taxType == 'E'){
						taxAmount = amount*(taxRate/100);
					}

				}else{
					taxAmount = 0;
				}
				taxAmount = Math.round(taxAmount*100)/100;
				amount = Math.round(amount*100)/100;
				var finalAmount = Math.round((amount+taxAmount)*100)/100;
				$(taxAmountElm).val(taxAmount);
				$(writeProductToElm).val(finalAmount);
				$(writeProductToElmNoTax).val(amount);
			});
		};
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
		//inventory.php end
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};
		$(".datepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			showAnim : 'show',
			changeMonth: true,
			changeYear: true,
			yearRange: '2000:+10'
		});
		$.fn.quickSave = function(){
			stockOs();
			var quantity    				= parseInt($("input.quantity").val())||0;
			var item_id   					= $("select.item_id option:selected").val();
			var item_name 					= $("select.item_id option:selected").text();
			if(quantity==0){
				return;
			}
			if(item_id == 0){
				return;
			}
			var updateMode    = $(".updateMode").length;
			var update_row_id = 0;
			if(updateMode){
				update_row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
			}
			var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="'+update_row_id+'">'
									  +  '<td class="item_id text-left" data-item-id="'+item_id+'">'+item_name+'</td>';
			$theNewRow    += '<td style="text-align:center;" class="quantity">'+quantity+'</td>';
			$theNewRow 		+= '<td class="text-center">'
			+'<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>'
			+' <a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
			+ '</td>'
			+ '</tr>';

			if(updateMode == 0){
				$("tbody#transactions_list").append($theNewRow);
				$(".addDetailRow").setFocusTo("div.item_id button");
			}else if(updateMode == 1){
				$(".updateMode").replaceWith($theNewRow);
				$(".addDetailRow").setFocusTo("div.item_id button");
			}
			$(this).clearPanelValues();
		};
		$.fn.calculateColumnTotals = function(){
			var discount_type    = $("input.discount_type:checked").val();
			var transaction_type = $("input[name=radiog_dark]:checked").val();
			$(this).sumColumn("td.quantity","td.qtyTotal");
			$(this).sumColumn("td.cartons","td.carton_total");
			$(this).sumColumnFloat("td.unitPrice","td.price_total");
			$(this).sumColumnFloat("td.subAmount","td.amountSub");
			$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
			var amountTotal = parseFloat($("td.amountTotal").text())||0;

			var disk = 0;
			var rate = 0;
			var qty  = 0;
			var sub  = 0;
			if(discount_type == 'R'){
				$("td.discount").each(function(){
					disk += parseFloat($(this).text())||0;
				});
			}else{
				var dit = 0;
				$("tr.transactions").each(function(){
					qty  = parseFloat($(this).find("td.quantity").text())||0;
					rate = parseFloat($(this).find("td.unitPrice").text())||0;
					dit  = parseFloat($(this).find("td.discount").text())||0;

					disk +=  parseFloat(((qty*rate*dit) / 100))||0;
				});
			}

			var discount_amount = parseFloat($("input.whole_discount").val())||0;

			if(discount_type == 'P'){
				discount_amount = (amountTotal*discount_amount) / 100;
			}
			amountTotal -= discount_amount;

			$("input.total_discount").val((discount_amount+disk).toFixed(2));

			var charges = parseFloat($("input.inv_charges").val())||0;
			amountTotal += charges;
			$("input.grand_total").val((amountTotal).toFixed(2));

			var received    = parseFloat($("input.received_cash").val())||0;
			var grand_total = $("input.grand_total").val();
			var returnin    = received - grand_total;

			if(transaction_type == 'C'){
				if(received >= grand_total){
					$("input.change_return").val((returnin).toFixed(2));
					$("input.remaining_amount").val("");
				}else{
					$("input.remaining_amount").val("");
					$("input.change_return").val("");
					$("input.received_cash").val("");
				}
			}else{
				if(received < grand_total){
					returnin *= -1;
					$("input.remaining_amount").val((returnin).toFixed(2));
				}else{
					$("input.remaining_amount").val("");
				}
				if(received > grand_total){
					$("input.change_return").val((returnin).toFixed(2));
				}else{
					$("input.change_return").val("");
				}
			}
			var rec_amount = $("input.recovery_amount").val();
			var chg_amount = $("input.change_return").val();

			if(balance_recovered == 'Y'){
				$("input.recovery_amount").val(chg_amount);
				$("input.change_return").val('');
			}else{
				$("input.recovery_amount").val('');
			}
		};
		$.fn.addToThis = function(that_amount){
			var this_amount = parseFloat($(this).val())||0;
			$(this).va(that_amount+this_amount);
		};
		$.fn.calculateRowTotal = function(){
			var item_type = $("select.itemSelector option:selected").attr("data-type");
			var taxRate   = parseFloat($("input.taxRate").val())||0;
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal   = parseFloat($("input.quantity").val())||0;
			var thatVal   = parseFloat($("input.unitPrice").val())||0;
			if(item_type == 'I'){
				var amount  = Math.round((thisVal*thatVal)*100)/100;
			}else{
				var amount  = thatVal;
			}
			var discountAvail = parseFloat($("input.discount").val())||0;
			var discount_type = $("input.discount_type:checked").val();

			var discountPerCentage = 0;
			amount = Math.round(amount*100)/100;
			if($("input.individual_discount").val()=='Y'){
				if(discount_type == 'R'){
					discountPerCentage = discountAvail;
				}else if(discount_type == 'P'){
					discountAvail = Math.round(discountAvail*100)/100;
					discountPerCentage = amount*(discountAvail/100);
					discountPerCentage = Math.round(discountPerCentage*100)/100;
				}
				$("input.discount").attr('data-amount',discountPerCentage);
				amount -= discountPerCentage;
				amount = Math.round(amount*100)/100;
			}

			var taxAmount = 0;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}
			}
			if(taxType == 'I'){
				amount -= taxAmount;
			}else{
				amount += taxAmount;
			}
			$("input.totalAmount").val(amount);
		};
		$.fn.updateStockInHand = function(){
			var stockOfBill = 0;
			$("td.quantity").each(function(index, element) {
				stockOfBill += parseInt($(this).text())||0;
			});
			stockOfBill += parseInt($("input.quantity").val())||0;
		};

		$.fn.clearPanelValues = function(){
			$("input.quantity").val('');
			$("input.returndate").val('');
			$("select.item_id option").prop('selected',false);
			$("select.item_id").selectpicker('refresh');
			$("input.delivereddate").val('');
		};
	});
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var showDeleteRowDilog = function(rowChildElement){
		var id = $(rowChildElement).parent().parent().attr("data-row-id");
		var clickedDel = $(rowChildElement);
		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Record is in edit Mode!');
			return false;
		}
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
			$("body").append("<input type='hidden' value='"+id+"' class='deleted_rows' />")
			$(this).calculateColumnTotals();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
		});
		$(".close_popup").click(function(){
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('fast');
		});
	};
	var validatePhone = function(txtPhone) {
		var a = $(txtPhone).val();
		var filter = /^[0-9-+]+$/;
		if(a[0] == 0){
			a = a.substring(1,11);
		}
		if(a[0] != 3){
			return false;
		}
		if(a.length != 10){
			return false;
		}
		if(filter.test(a)) {
			return a;
		}else{
			return false;
		}
	};
	var sum_tax_amount = function(){
		var tax = 0;
		$("td.taxRate").each(function(){
			tax += parseFloat($(this).attr('tax-amount'))||0;
		});
		$(".whole_tax").val((tax).toFixed(2));
	}
	var stockOs = function(){
		var thisQty      = parseInt($("input.quantity").val())||0;
		var inStock      = parseInt($("input.in_stock").attr('data-stock'))||0;
		var selectedItem = $("select.item_id option:selected").val();
		var NewStock     = inStock - thisQty;
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');

		var quantity 			= thisRow.find('td.quantity').text();
		var item_id 			= parseInt(thisRow.find('td.item_id').attr('data-item-id'))||0;
		var delivereddate = thisRow.find('td.delivereddate').text();
		var returndate 		= thisRow.find('td.returndate').text();
		var order_status 	= thisRow.find('td.order_status').attr('data-state');

		$("input.quantity").val(quantity);
		$("select.item_id option[value='"+item_id+"']").prop('selected',true);
		$("select.item_id").selectpicker('refresh');
		$("input.returndate").val(returndate);
		$("input.delivereddate").val(delivereddate);
		var order_status = (order_status=='Y')?true:false;
		$("input.order_status").prop("checked",order_status);
	};
	var saveRentalBooking = function(){

		var rental_id						= $("input[name='rbid']").val();
		var title  	   					= $("input[name='title']").val();
		var description  	   		= $("textarea[name='description']").val();
		var booking_date_time  	= $("input[name='booking_date_time']").val();
		var try_date_time  	   	= $("input[name='try_date_time']").val();
		var delivery_date_time  = $("input[name='delivery_date_time']").val();
		var booking_status  	  = $("select[name='booking_status']").val();
		var clinet_name  	   		= $("input[name='clinet_name']").val();
		var clinet_mobile  	   	= $("input[name='clinet_mobile']").val();
		var clinet_phone  	   	= $("input[name='clinet_phone']").val();
		var client_address  	  = $("textarea[name='client_address']").val();
		var total_price  	   		= $("input[name='total_price']").val();
		var advance  	   				= $("input[name='advance']").val();
		var remaining  	   			= $("input[name='remaining']").val();
		var booking_order_no  	= $("input[name='booking_order_no']").val();

		if($("tr.transactions").length == 0){
			displayMessage('No Transaction Existed, Bill Can not be Saved!');
			$("div.itemSelector button").addClass("btn-warning");
			$("div.itemSelector button").focus();
			return false;
		}

		$(".save_rental").hide();
		$("#xfade").fadeIn();
		var deleted_rows = {};
		var jSonString = {};
		$("tr.transactions").each(function(index, element){
			//var rowId        = $(this).attr('data-row-id');
			jSonString[index]  							 = {};
			jSonString[index].row_id  			 = parseInt($(this).attr("data-row-id"))||0;
			jSonString[index].item_id        = parseInt($(this).find('td.item_id').attr('data-item-id'))||0;
			jSonString[index].returndate		 = $(this).find('td.returndate').text();
			jSonString[index].quantity			 = $(this).find('td.quantity').text();
			jSonString[index].delivereddate  = $(this).find('td.delivereddate').text();
			jSonString[index].order_status   = $(this).find('td.order_status').attr('data-state');
		});
		jSonString = JSON.stringify(jSonString);

		$("input.deleted_rows").each(function(i,e){
			var id = parseInt($(this).val())||0;
			if(id>0){
				deleted_rows[i] = id;
			}
		});
		deleted_rows = JSON.stringify(deleted_rows);

		$.post("db/saveRentalBooking.php",{title:title,
															rental_id:rental_id,
															description:description,
															booking_date_time:booking_date_time,
															try_date_time:try_date_time,
															delivery_date_time:delivery_date_time,
															booking_status:booking_status,
															clinet_name:clinet_name,
															clinet_mobile:clinet_mobile,
															clinet_phone:clinet_phone,
															client_address:client_address,
															total_price:total_price,
															advance:advance,
															remaining:remaining,
															booking_order_no:booking_order_no,
															deleted_rows:deleted_rows,
			jSonString:jSonString},function(data){

				//data = $.parseJSON(data);

				//window.location.href = 'rental-booking-details.php?id='+data;
				if(data['ID'] > 0){
					var msg_type = '';
					if(sale_id == 0){
						msg_type = "&saved";
					}else{
						msg_type = "&updated";
					}

					//window.location.href = 'rental-booking-details?'+rental_id+msg_type;
				}else{
					displayMessage(data['MSG']);
					$("#xfade").fadeOut();
				}
			});
		};
			var hidePopUpBox = function(){
				$("#popUpBox").css({'left':'-=500px'});
				$("#popUpBox").css({'transform':'scaleY(0.0)'});
				$("#xfade").fadeOut();
				$("#popUpBox").remove();
			};
			var add_supplier = function(){
				var code_type = $("input[name='radiog_dark']:checked").val();
				if(code_type == 'C'){
					return false;
				}
				var url,captions,args = {};
				if(code_type == 'B'){
					url = 'add-bank-detail.php'
					captions = 'Bank';
					args['save']  = '';
					args['quick'] = '';
					args['title'] = '';
					args['bank_type'] = 'M';
					args['description'] = '';
				}else if(code_type == 'A'){
					url = 'customer-detail.php'
					captions = 'Customer';
					args['supp_name'] = '';
				}
				$("body").append("<div id='popUpBox'></div>");
				var formContent  = '';
				formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
				formContent += '<div id="form">';
				formContent += '<p>New '+captions+' Title:</p>';
				formContent += '<p class="textBoxGo">';
				formContent += '<input type="text" class="form-control categoryName" />';
				formContent += '</p>';
				formContent += '</div>';
				$("#popUpBox").append(formContent);
				$("#xfade").fadeIn();
				$("#popUpBox").fadeIn('200').centerThisDiv();
				$(".categoryName").focus();
				$(".categoryName").keyup(function(e){
					var name = $(".categoryName").val();
					if(code_type == 'B'){
						args['title'] = name;
					}else if(code_type == 'A'){
						args['supp_name'] = name;
					}
					if(e.keyCode == 13){
						$(".categoryName").blur();
						if(name != ''){
							$.post(url,args,function(data){
								data = $.parseJSON(data);
								if(data['OK'] == 'Y'){
									$("select.supplierSelector").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
									$("select.supplierSelector option:selected").prop('selected',false);
									$("select.supplierSelector option").last().prop('selected',true);
									$("select.supplierSelector").selectpicker('refresh');
									hidePopUpBox();
								}else if(data['OK'] == 'N'){
									alert('New '+captions+' Could Not be Created!');
								}
							});
						}
					}
				});
			};
			var getAccountBalance = function(){
				var acc_code = $("select.supplierSelector option:selected").val();
				$.post("db/get-account-balance.php",{supplierAccCode:acc_code},function(data){
					data = $.parseJSON(data);
					$("input.customer-balance").val(data['AMOUNT']);
				});
			}
			var discount_type_change = function(thiss){
				var this_val = $(thiss).val();
				if($("tr.dynamix").length){
					return false;
				}
				$.get('sale-details.php',{discount:this_val});
			};
			var recover_balance = function(thiss){
				if($(thiss).is(":checked")){
					balance_recovered = "Y";
				}else{
					balance_recovered = "N";
				}
				$(this).calculateColumnTotals();
			}
			var sendSmsPopUp = function(){
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel' style='width:280px;'><p class='confirm text-left'>Please Confirm Mobile Number <button id='close-pop' class='pull-right btn btn-xs btn-danger'> <i class='fa fa-times'></i> </button> </p>  <input class='form-control confirmed-mobile pull-left text-center' style='width:150px;margin-left:25px;' value='"+$("input.customer_mobile").val()+"' /> <a class='nodelete btn btn-info'>Send</a></div>");
				centerThisDiv("#popUpDel");
				$("#popUpDel").hide();
				$("#xfade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$("#close-pop").click(function(){
					$("#popUpDel").fadeOut();
					$("#xfade").fadeOut();
				});
				$("#xfade").click(function(){
					$("#popUpDel").fadeOut();
					$("#xfade").fadeOut();
				});
				$(".nodelete").click(function(){
					sendSmsInit();
				});
			};
			var sendSmsInit = function(){
				$("#popUpDel input[type=text]").remove();
				$("#popUpDel p.confirm").text("Please Wait...");
				var mobile_number = ($("input.confirmed-mobile").length)?$("input.confirmed-mobile").val():"";
				if(mobile_number == ''){
					return;
				}
				var sale_id 	  = $("input.sale_id").val();
				$.post("sale-details.php",{mobile_number:mobile_number,sale_id:sale_id},function(data){
					if(data == 'N'){
						data = "Error! Cannot send sms."
					}
					$("#popUpDel p.confirm").text(data);
					$("#popUpDel").css({"width":"350px"});
					$("#popUpDel").centerThisDiv();
					$("input.confirmed-mobile").val('');
					$("input.confirmed-mobile").hide();
					$(".nodelete").text("OK");
					$(".nodelete").click(function(){
						$("#popUpDel").fadeOut();
						$("#xfade").fadeOut();
					});
				});
			};
			var hidePopUpBox = function(){
				$("#popUpBox").css({'transform':'scaleY(0.0)'});
				$("#fade").fadeOut();
				$("#popUpBox").remove();
			};
			var showPopUpBox = function(){
				$("#fade").fadeIn();
				$("#popUpBox").fadeIn('200').centerThisDiv();
				$('input[name=delivery_time1], input[name=return_time1]').datetimepicker({
					format: "dd-mm-yyyy HH:ii P",
					weekStart: 1,
	        todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					forceParse: 0,
	        showMeridian: 1
				});
			};
