$(document).ready(function() {
	use_cartons = $("input.use_cartons").val();
	$(window).keyup(function(e){
		if(e.keyCode == 27){
			$(this).clearPanelValues();
			$("div.itemSelector button").focus();
			if($(".updateMode").length){
				$(".updateMode").removeClass('updateMode');
			}
		}
	});

	$.fn.serializeObject = function(){
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	$.fn.barcodeAvailable = function(){
		var $thisElm = $(this);
		var item_id  = parseInt($("input[name=itemID]").val())||0;
		$(this).on('keyup blur',function(e){
			$.get('db/compare-barcode.php',{q:$(this).val(),id:item_id},function(data){
				if(data > 0){
					$("#popUpBox input:submit").prop('disabled',true);
					$($thisElm).addClass('error');
				}else{
					$("#popUpBox input:submit").prop('disabled',false);
					$($thisElm).removeClass('error');
				}
			});
		});
	};

	$(".h3_php_error").click(function(){
		$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});
	$("#main-nav li ul").hide();
	$("#main-nav li a.nav-top-item").click(
		function () {
			$(this).parent().siblings().find("ul").slideUp("normal");
			$(this).next().slideToggle("normal");
		}
	);
	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);

	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	/*$(".content-box-header h3").click(
	function () {
	$(this).parent().next().toggle();
	$(this).parent().parent().toggleClass("closed-box");
	$(this).parent().find(".content-box-tabs").toggle();
}
);*/

// Content box tabs:

$.fn.numericOnly = function(){
	$(this).keydown(function(e){
		if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 116
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
		$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

		$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
			function() {
				$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
				$(this).addClass('current'); // Add class "current" to clicked tab
				var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
				$(currentTab).siblings().hide(); // Hide all content divs
				$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
				return false;
			}
		);

		//Close button:

		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);

	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			sumAll += parseInt($(this).text());
		});
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			sumAll += parseFloat($(this).text())||0;
		});
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text((sumAll).toFixed(2));
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		var dt = currentRow.children("td").first().next().text();
		$("#popUpDel .confirm").text("Are Sure you Want To Delete Records Dated : "+dt+" ?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {cid : idValue}, function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					currentRow.remove();
				}
				$("#popUpDel .confirm").text(data['MSG']);
				$(".nodelete").text('Close');
				$(".dodelete").remove();
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
		});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': '100px',
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		if(item_id != ''){
			$.post('db/get-item-details.php',{item_id:item_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
				var itemPrice = data['P_PRICE'];
				if(itemPrice == 0){
					itemPrice = '';
				}
				$("input.unitPrice").val(itemPrice);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id     = parseInt($("select.itemSelector option:selected").val())||0;
		var item_name   = $("select.itemSelector option:selected").text();
		var from_branch = $("select.from_branch  option:selected").val();
		var from_branchn= $("select.from_branch  option:selected").text();
		var to_branch   = $("select.to_branch    option:selected").val();
		var to_branchn  = $("select.to_branch    option:selected").text();
		var from_stock  = parseFloat($("input.from_stock").val())||0;
		var quantity    = parseFloat($("input.quantity").val())||0;
		var unit_cost   = $("input.unit_cost").val();
		var total_cost  = $("input.total_cost").val();
		$(this).blur();
		if(item_id == 0){
			return false;
		}
		if(quantity == 0){
			$("input.quantity").effect('highlight', { color: "#ff0000" },200);
			return false;
		}
		if(from_stock<0){
			$("input.from_stock").effect('highlight', { color: "#ff0000" },200);
			$(this).focus();
			return false;
		}
		if(from_branch == to_branch){
			return;
		}
		var updateMode = $(".updateMode").length;
		if(updateMode == 0){
			var the_row  = '<tr class="alt-row calculations transactions" data-row-id="0"><td style="text-align:left;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>';
			the_row     += '<td style="text-align:center;" class="from_branch" data-id="'+from_branch+'">'+from_branchn+'</td><td style="text-align:center;" class="to_branch" data-id="'+to_branch+'">'+to_branchn+'</td><td style="text-align:center;" class="quantity">'+quantity+'</td><td style="text-align:center;" class="unit_cost">'+unit_cost+'</td><td style="text-align:center;" class="total_cost">'+total_cost+'</td><td style="text-align:center;"><a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a><a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a></td></tr>';
			$(the_row).insertAfter($(".calculations").last());
		}else if(updateMode == 1){
			$(".updateMode").find('td.itemName').text(item_name);
			$(".updateMode").find('td.itemName').attr('data-item-id',item_id);
			$(".updateMode").find('td.from_branch').text(from_branchn);
			$(".updateMode").find('td.from_branch').attr('data-id',from_branch);
			$(".updateMode").find('td.to_branch').text(to_branchn);
			$(".updateMode").find('td.to_branch').attr('data-id',to_branch);
			$(".updateMode").find('td.quantity').text(quantity);
			$(".updateMode").find('td.unit_cost').text(unit_cost);
			$(".updateMode").find('td.total_cost').text(total_cost);
			$(".updateMode").removeClass('updateMode');
		}
		$(this).clearPanelValues();
		$(this).calculateColumnTotals();
		$("select.itemSelector").parent().find(".dropdown-toggle").focus();
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumnFloat("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.total_cost","td.costTotal");
	};
	$.fn.calculateRowTotal = function(){
		var taxRate = $("input.taxRate").val();
		var taxType   = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount = Math.round((thisVal*thatVal)*100)/100;
		var discountAvail = parseFloat($("input.discount").val())||0;
		var discount_type = $("input.discount_type:checked").val();
		var discountPerCentage = 0;
		amount = Math.round(amount*100)/100;
		if(discount_type == 'R'){
			discountPerCentage = discountAvail;
		}else if(discount_type == 'P'){
			discountAvail = Math.round(discountAvail*100)/100;
			discountPerCentage = amount*(discountAvail/100);
			discountPerCentage = Math.round(discountPerCentage*100)/100;
		}
		amount -= discountPerCentage;
		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount * (taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount * (taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		taxAmount = Math.round(taxAmount*100)/100;

		amount = Math.round(amount*100)/100;
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		-= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
			stockOfBill += parseInt($(this).text())||0;
		});
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$("table.prom select option").prop('selected',false);
		$("table.prom select").find("option").first().prop('selected',true);
		$("table.prom select").selectpicker('refresh');
		$("input.quantity,input.unit_cost,input.total_cost").val('');
		$("input.from_stock,input.to_stock").val('');
	};
});

var use_cartons = '';
// PreLoad FUNCTION //
//////////////////////
var letMeGo = function(thisElm){
	$(thisElm).fadeOut(300,function(){
		$(this).html('');
	});
}
var editThisRow = function(thisElm){
	var thisRow 	= $(thisElm).parent('td').parent('tr');
	$(".updateMode").removeClass('updateMode');
	thisRow.addClass('updateMode');

	var item_name 	= thisRow.find('td.itemName').text();
	var item_id 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
	var cartons     = parseFloat(thisRow.find('td.cartons').text())||0;
	var per_carton  = parseFloat(thisRow.find('td.per_carton').text())||0;
	var quantity 	= parseFloat(thisRow.find('td.quantity').text())||0;
	var unitPrice 	= parseFloat(thisRow.find('td.unitPrice').text())||0;
	var discount 	= parseFloat(thisRow.find('td.discount').text())||0;
	var subAmount 	= parseFloat(thisRow.find('td.subAmount').text())||0;
	var taxRate 	= parseFloat(thisRow.find('td.taxRate').text())||0;
	var taxAmount   = parseFloat(thisRow.find('td.taxAmount').text())||0;
	var totalAmount = parseFloat(thisRow.find('td.totalAmount').text())||0;

	$(".itemSelector option[value='"+item_id+"']").prop('selected',true);
	$(".itemSelector").selectpicker('refresh');
	$("input.cartons").val(cartons);
	$("input.per_carton").val(per_carton);
	$("input.quantity").val(quantity);
	$("input.unitPrice").val(unitPrice);
	$("input.discount").val(discount);
	$("input.subAmount").val(subAmount);
	$("input.taxRate").val(taxRate);
	$("input.taxAmount").val(taxAmount);
	$("input.totalAmount").val(totalAmount);
	$("input.inStock").val('').attr('thestock','');
	$("div.itemSelector button").focus();
};
var stockOs = function(){
	var item_id     = parseInt($("select.itemSelector option:selected").val())||0;
	var thisQty     = parseFloat($("input.quantity").val())||0;
	var from_stock  = parseFloat($("input.from_stock").attr('data-stock'))||0;
	var to_stock    = $("input.to_stock").attr('data-stock');
	var from_branch = parseInt($("select.from_branch option:selected").val())||0;
	var to_branch   = parseInt($("select.to_branch option:selected").val())||0;
	var inStock     = from_stock;
	var shifted_qty = 0;
	//calculations of input row only.......
	$("input.to_stock").val( ((parseFloat($("input.to_stock").attr('data-stock'))||0) + thisQty) );
	if($("tr.updateMode").length){
		shifted_qty += parseInt($("tr.updateMode td.quantity").text())||0;
	}
	inStock += shifted_qty;
	inStock -= thisQty;
	if(inStock < 0){
		$("input.quantity").val('');
		return;
	}
	$("input.from_stock").val(inStock);
};

var saveTransferRecord = function(){
	var transfer_id		 = $("input.transfer_id").val();
	var transfer_date 	 = $("input[name=transfer_date]").val();

	if($("tr.transactions").length == 0){
		displayMessage('No Transaction Found!');
		return false;
	}
	$(".saveTransferRecord").hide();
	$("#xfade").show();
	var jSonString = {};

	$error = false;

	$("tr.transactions").each(function(index, element){
		var row_id      = $(this).attr('data-row-id');
		var thisRow     = $(this);

		var item_id 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
		var from_branch = parseInt(thisRow.find('td.from_branch').attr('data-id'))||0;
		var to_branch   = parseInt(thisRow.find('td.to_branch').attr('data-id'))||0;
		var quantity    = parseFloat(thisRow.find('td.quantity').text())||0;
		var unit_cost   = parseFloat(thisRow.find('td.unit_cost').text())||0;
		var total_cost  = parseFloat(thisRow.find('td.total_cost').text())||0;

		if(item_id > 0 && quantity > 0){
			jSonString[index]             = {};
			jSonString[index].row_id      = row_id;
			jSonString[index].item_id     = item_id;
			jSonString[index].quantity    = quantity;
			jSonString[index].from_branch = from_branch;
			jSonString[index].to_branch   = to_branch;
			jSonString[index].unit_cost   = unit_cost;
			jSonString[index].total_cost  = total_cost;
		}
	});
	if($error){
		displayMessage('Error! From/To branches cannot be identical.');
		return;
	}
	jSonString = JSON.stringify(jSonString);
	$.post("db/saveTransferRecord.php",{transfer_id:transfer_id,
		transfer_date:transfer_date,
		jSonString:jSonString},function(data){
			data = $.parseJSON(data);
			if(data['ID'] > 0){
				var msg_type = '';
				if(transfer_id == 0){
					msg_type = "&saved";
				}else{
					msg_type = "&updated";
				}
				window.location.href = 'stock-management.php?id='+data['ID']+msg_type;
			}else{
				$("#xfade").hide();
				displayMessage(data['MSG']);
			}
		});
	};

	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};
	var displayToastMessage = function(message){
		$.toaster({ priority : 'success', title : 'Alert', message : message});
		$("#xfade").fadeOut('slow');
	};

	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);

		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Row Is In Edit Mode!');
			return false;
		}

		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
		});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#fade").fadeOut('fast');
		});
	};
	var hidePopUpBox = function(){
		$("#popUpBox").css({'transform':'scaleY(0.0)'});
		$("#xfade").fadeOut();
		$("#popUpBox").remove();
	};
	var add_supplier = function(){
		$("body").append("<div id='popUpBox'></div>");
		var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New Supplier Title:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control categoryName" />';
		formContent += '</p>';
		formContent += '</div>';
		$("#popUpBox").append(formContent);
		$("#xfade").fadeIn();
		$("#popUpBox").fadeIn('200').centerThisDiv();
		$(".categoryName").focus();
		var disabled = '';
		disabled = ($(".trasactionType").is(":checked"))?"disabled":"";
		$(".categoryName").keyup(function(e){
			var name = $(".categoryName").val();
			if(e.keyCode == 13){
				$(".categoryName").blur();
				if(name != ''){
					$.post('supplier-details.php',{supp_name:name},function(data){
						data = $.parseJSON(data);
						if(data['OK'] == 'Y'){
							$("select.supplierSelector").append('<option '+disabled+' data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
							if(!$(".trasactionType").is(":checked")){
								$("select.supplierSelector option:selected").prop('selected',false);
								$("select.supplierSelector option").last().prop('selected',true);
							}
							$("select.supplierSelector").selectpicker('refresh');
							hidePopUpBox();
						}else if(data['OK'] == 'N'){
							alert('New Supplier Could Not be Created!');
						}
					});
				}
			}
		});
	};

	var add_new_item_submit = function(){
		var formData = $("#popUpBox form").serializeObject();
		$.post('item-details.php',formData,function(data){
			hidePopUpBox();
			$(".reload_item").click();
		});
	};

	var add_new_item = function(){
		$("body").append('<div id="popUpBox"><button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button></div>');
		$("#popUpBox").append('<form action="" method="post"></form>');
		$.get('item-details.php',{},function(html){
			var formContent = $(html).find(".popup_content_nostyle").html();
			formContent+= $(html).find(".form-submiters").html();
			$("#popUpBox form").append(formContent);
			$("#popUpBox form").append('<input name="addItem" type="hidden" />');
			$("#popUpBox .popuporemov").remove();
			$("#popUpBox .selectpicker").remove();
			$("#popUpBox input:submit").addClass("prop_item_submit");
			$("#popUpBox .prop_item_submit").prop('type','button');
			$("#popUpBox .prop_item_submit").click(function(){
				add_new_item_submit();
			});
			$("#popUpBox select").selectpicker();
			$("#popUpBox input.item-barcode").barcodeAvailable();
			$("#xfade").fadeIn();
			$("#popUpBox").fadeIn('200').centerThisDiv();
		});
	};
	var discount_type_change = function(thiss){
		var this_val = $(thiss).val();
		if($("tr.transactions").length){
			return false;
		}
		$.get('sale-details.php',{discount:this_val});
	};
	var get_branch_stock = function(branch_id,item_id,output_elm){
		if(branch_id > 0){
			$.get('branch-management.php',{branch_id:branch_id,item_stock_id:item_id},function(data){
				data = JSON.parse(data);
				data['STOCK'] = parseFloat(data['STOCK'])||0;
				if(output_elm == '.from_stock'){
					var item_id     = parseInt($("select.itemSelector option:selected").val())||0;
					$("td.from_branch[data-id='"+branch_id+"']").each(function(index, element){
						var row_id = parseInt($(this).parent().attr('data-row-id'))||0;
						var iid    = parseInt($(this).prev("td.itemName").attr('data-item-id'))||0;
						if(item_id == iid && row_id == 0){
							data['STOCK'] -= parseInt($(this).next('td').next('td').text())||0;
						}
					});
				}
				$(output_elm).val(data['STOCK']);
				$(output_elm).attr('data-stock',data['STOCK']);
			});
		}else{
			$.post('db/get-item-details.php',{item_id:item_id},function(data){
				data = $.parseJSON(data);
				data['STOCK'] = parseFloat(data['STOCK'])||0;
				if(data['P_PRICE'] == 0){
					data['P_PRICE'] = '';
				}
				$("input.unit_cost").val(data['P_PRICE']);
				if(output_elm == '.from_stock'){
					var item_id     = parseInt($("select.itemSelector option:selected").val())||0;
					$("td.from_branch[data-id='"+branch_id+"']").each(function(index, element){
						var row_id = parseInt($(this).parent().attr('data-row-id'))||0;
						var iid    = parseInt($(this).prev("td.itemName").attr('data-item-id'))||0;
						if(item_id == iid && row_id == 0){
							data['STOCK'] -= parseInt($(this).next('td').next('td').text())||0;
						}
					});
				}
				$(output_elm).val(data['STOCK']);
				$(output_elm).attr('data-stock',data['STOCK']);
			});
		}
	}
