$(document).ready(function() {

	$(window).keyup(function(e){
		if(e.keyCode == 27){
			$(this).clearPanelValues();
			$("div.itemSelector button").focus();
			if($(".updateMode").length){
				$(".updateMode").removeClass('updateMode');
			}
		}
	});

    $(".h3_php_error").click(function(){
	$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});
	$("#main-nav li ul").hide();
	$("#main-nav li a.nav-top-item").click(
		function () {
			$(this).parent().siblings().find("ul").slideUp("normal");
			$(this).next().slideToggle("normal");
		}
	);
	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);

	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	/*$(".content-box-header h3").click(
		function () {
		  $(this).parent().next().toggle();
		  $(this).parent().parent().toggleClass("closed-box");
		  $(this).parent().find(".content-box-tabs").toggle();
		}
	);*/

	// Content box tabs:

	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 116
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseInt($(this).text());
        });
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseFloat($(this).text())||0;
        });
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text(sumAll);
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		var bill = currentRow.children("td").first().text();
		$("#popUpDel .confirm").text("Are Sure you Want To Delete Bill # "+bill+"?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {cid : idValue}, function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					currentRow.remove();
				}
				$("#popUpDel .confirm").text(data['MSG']);
				$(".nodelete").text('Close');
				$(".dodelete").remove();
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = $("select.taxRate option:selected").val();
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		if(item_id != ''){
			$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
				var itemPrice = data['P_PRICE'];
				$("input.unitPrice").val(itemPrice);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id  = $(".itemSelector option:selected").val();
		var item_name  = $(".itemSelector option:selected").text();
		var quantity = $("input.quantity").val();
		var unitPrice = $("input.unitPrice").val();
		var discount = $("input.discount").val();
		var subAmount = $("input.subAmount").val();
		var taxRate   = $("select.taxRate option:selected").val();
		var taxAmount = $("input.taxAmount").val();
		var totalAmount = $("input.totalAmount").val();
		$(this).blur();
		if(taxRate == '' || taxRate == 0){
			taxAmount = 0;
		}
		var updateMode = $(".updateMode").length;
		if(item_id != '' &&item_name != '' &&quantity != '' &&unitPrice != '' &&subAmount != '' &&totalAmount != ''){
			if(updateMode == 0){
			$('<tr class="alt-row calculations transactions" data-row-id="0"><td style="text-align:left;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td><td style="text-align:center;" class="quantity">'+quantity+'</td><td style="text-align:center;" class="unitPrice">'+unitPrice+'</td><td  style="text-align:center;" class="discountAmount">'+discount+'</td><td style="text-align:center;" class="subAmount">'+subAmount+'</td><td style="text-align:center;" class="taxRate">'+taxRate+'</td><td style="text-align:center;" class="taxAmount">'+taxAmount+'</td><td style="text-align:center;" class="totalAmount">'+totalAmount+'</td><td style="text-align:center;"> - - - </td><td style="text-align:center;"><a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a><a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a></td></tr>').insertAfter($(".calculations").last());
			}else if(updateMode == 1){
				$(".updateMode").find('td').eq(0).text(item_name);
				$(".updateMode").find('td').eq(0).attr('data-item-id',item_id);
				$(".updateMode").find('td').eq(1).text(quantity);
				$(".updateMode").find('td').eq(2).text(unitPrice);
				$(".updateMode").find('td').eq(3).text(discount);
				$(".updateMode").find('td').eq(4).text(subAmount);
				$(".updateMode").find('td').eq(5).text(taxRate);
				$(".updateMode").find('td').eq(6).text(taxAmount);
				$(".updateMode").find('td').eq(7).text(totalAmount);
				$(".updateMode").removeClass('updateMode');
			}
			$(this).clearPanelValues();
			$(this).calculateColumnTotals();
			$(".itemSelector").parent().find(".dropdown-toggle").focus();
		}else{
			alert('Values Missing!');
		}
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
	$.fn.calculateRowTotal = function(){
		var taxRate = $("select.taxRate option:selected").val();
		var taxType   = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount = Math.round((thisVal*thatVal)*100)/100;
		var discountAvail = parseFloat($("input.discount").val())||0;

		amount = Math.round(amount*100)/100;
		discountAvail = Math.round(discountAvail*100)/100;
		discountPerCentage = amount*(discountAvail/100);
		discountPerCentage = Math.round(discountPerCentage*100)/100;
		amount -= discountPerCentage;

		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount * (taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount * (taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		taxAmount = Math.round(taxAmount*100)/100;

		amount = Math.round(amount*100)/100;
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		-= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
            stockOfBill += parseInt($(this).text())||0;
        });
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$(".itemSelector option").prop('selected',false);
		$(".itemSelector").find("option").first().prop('selected',true);
		$(".itemSelector").selectpicker('refresh');
		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.subAmount").val('');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	}
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	= thisRow.find('td').eq(0).text();
		var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
		var quantity 	= thisRow.find('td').eq(1).text();
		var unitPrice 	= thisRow.find('td').eq(2).text();
		var discount 	= thisRow.find('td').eq(3).text();
		var subAmount 	= thisRow.find('td').eq(4).text();
		var taxRate 	= parseFloat(thisRow.find('td').eq(5).text())||0;
		var taxAmount   = thisRow.find('td').eq(6).text();
		var totalAmount = thisRow.find('td').eq(7).text();

		$(".itemSelector option[value='"+item_id+"']").prop('selected',true);
		$(".itemSelector").selectpicker('refresh');
		$("input.quantity").val(quantity);
		$("input.unitPrice").val(unitPrice);
		$("input.discount").val(discount);
		$("input.subAmount").val(subAmount);
		if(taxRate == 0){
			$("select.taxRate option").prop('selected',false);
			$("select.taxRate").selectpicker('refresh');
		}else{
			$("select.taxRate option").each(function(index, element) {
                if($(this).val() == taxRate){
					$(this).prop('selected',true);
				}else{
					$(this).prop('selected',false);
				}
            });
			$("select.taxRate").selectpicker('refresh');
		}
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(totalAmount);
		$("input.inStock").val('').attr('thestock','');
		$("div.itemSelector button").focus();
	};
	var stockOs = function(){
		var thisQty = parseInt($("input.quantity").val())||0;
		var inStock = parseInt($("input.inStock").attr('thestock'))||0;

		var thisBillQuantity = 0;

		var item_id = parseInt($("select.itemSelector option:selected").val())||0;

		$("td.quantity").each(function(index, element){
			if(item_id == parseInt($(this).prev("td.itemName").attr('data-item-id'))||0){
				thisBillQuantity += parseInt($(this).text())||0;
			}
        });

		var NewStock = thisQty + inStock;
		NewStock += thisBillQuantity;

		$("input.inStock").val(NewStock);
	};

	var savePurchase = function(){
		var purchase_id		 = $(".purchase_id").val();
		var purchaseDate 	 = $(".datepicker").val();
		var billNum 		 = $("input[name='billNum']").val();
		var gp_num  		 = $("input[name='gpNum']").val();
		var supplierCode 	 = $("select.supplierSelector option:selected").val();

		if(supplierCode <= 0){
			displayMessage('Supplier Not Selected!');
			return false;
		}

		if($("tr.transactions").length == 0){
			displayMessage('No Transaction Found!');
			return false;
		}

		var jSonString = '{';

		$("tr.transactions").each(function(index, element) {
			var rowId       = $(this).attr('data-row-id');
			var thisRow     = $(this);
			var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
			var quantity 	= thisRow.find('td').eq(1).text();
			var unitPrice 	= thisRow.find('td').eq(2).text();
			var discount 	= parseFloat(thisRow.find('td').eq(3).text())||0;
			var subAmount 	= thisRow.find('td').eq(4).text();
			var taxRate 	= thisRow.find('td').eq(5).text();
			var taxAmount   = thisRow.find('td').eq(6).text();
			var totalAmount = thisRow.find('td').eq(7).text();
			if(item_id != '' && quantity != 0){
				if(index > 0){
					jSonString += ',';
				}
				jSonString += '"'+index+'":';
				jSonString += '{';
				jSonString += '"row_id":"'+rowId+'",'+'"item_id":"'+item_id+'","quantity":"'+quantity+'",';
				jSonString += '"unitPrice":"'+unitPrice+'","discount":"'+discount+'","subAmount":"'+subAmount+'",';
				jSonString += '"taxRate":"'+taxRate+'","taxAmount":"'+taxAmount+'",';
				jSonString += '"totalAmount":"'+totalAmount+'"';
				jSonString += '}';
			}
        });

		jSonString += '}';
		$.post("db/savePurchase.php",{purchase_id:purchase_id,
									  purchaseDate:purchaseDate,
									  billNum:billNum,
									  gp_num:gp_num,
									  supplierCode:supplierCode,
									  jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					var msg_type = '';
					if(purchase_id == 0){
						msg_type = "&saved";
					}else{
						msg_type = "&updated";
					}
					window.location.href = 'inventory-details.php?id='+data['ID']+msg_type;
				}else{
					displayMessage(data['MSG']);
				}
		});
	};

	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};

	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);

		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Row Is In Edit Mode!');
			return false;
		}

		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
		});
	};
	var makeItCash = function(checker){
		var cashAccCode = $('.cash-in-hand').attr('data-code');
		var current_cashAccCode = cashAccCode;
		var cashAccTitle = $('.cash-in-hand').attr('data-title');
		var what = $(checker).is(":checked");
		if(what){
			$("select.supplierSelector option").prop('disabled',true).prop('selected',false);
			$.each($('.cash-in-hand'),function(){
				cashAccCode = $(this).attr('data-code');
				cashAccTitle = $(this).attr('data-title');
				$("select.supplierSelector").append('<option value="'+cashAccCode+'" >'+cashAccTitle+'</option>');
			});
			$("select.supplierSelector option[value="+current_cashAccCode+"]").prop('selected',true);
			$("select.supplierSelector").selectpicker('refresh');
			$("input[name='supplier_name']").show().focus();
		}else{
			$("select.supplierSelector option").prop('disabled',false);
			$("select.supplierSelector").find("option[value^=010101]").remove();
			$("select.supplierSelector").selectpicker('refresh');
			$("input[name='supplier_name']").hide();
		}
	};
