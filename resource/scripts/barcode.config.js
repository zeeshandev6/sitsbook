$(document).ready(function(){
	
	$.fn.getBarcodeRecord = function(){
		$("#xfade").fadeIn();
		var $scanner = $(this);
		var barcode  = $scanner.val();
		if(barcode == ''){
			return false;
		}
		$($scanner).val('');
		$.get('db/get-barcode-record.php',{b_code:barcode},function(data){
			$("#xfade").fadeOut();
			$(".result").html(data);
		});
	};
});
