$(window).load(function(){
		$.fn.addNewAccountCode = function(aa,aabb,aabbcc){
			var accountTitle = $(this).val(); // take the value of this text input as account title
			$(this).val("");
			$(this).blur();
			if(accountTitle!==""){
				$.post('db/addNewAccountCode.php',{
							addAccount:true,
							title:accountTitle,
							general:aa,
							main:aabb,
							subMain:aabbcc
					},function(data){
					$("select.selectpicker:first").append("<option data-subtext=\""+data.replace(/\s+/g, '')+"\" value='"+data.replace(/\s+/g, '')+"'>"+accountTitle+"</option>");
					$("select.selectpicker:first").selectpicker('refresh');
					$("ul.selectpicker li a").click(function(){
						$(this).selectCutomerAccountCode();
					});
					$(this).hideOverLay();
					$("button.dropdown-toggle").first().focus();
				});
			}
		};
		$.fn.addNewMachine = function(){
			var machineTitle = $(this).val(); // take the value of this text input as account title
			var shed_id      = $(".shed_id_pop option:selected").val();
			$(this).val("");
			$(this).blur();
			$.post('db/newMachine.php',{machine:machineTitle,shed_id:shed_id},function(data){
				data = $.parseJSON(data);
				if(data['FLAG'] == 'Y'){
					$("select.machineNum").append('<option value="'+data['MACHINE_ID']+'">'+data['MACHINE_NO']+'-'+data['MACHINE_NAME']+'</option>');
					$("select.machineNum").find('option').last().prop('selected',true).siblings().prop('selected',false);
					$("select.machineNum").focus();
					$("#popUpForm3").fadeOut();$(this).hideOverLay();
				}
			});
		};
		$.fn.addNewStitch = function(){
			var stitchTitle = $(this).val();
			$(this).blur();
			$.post('db/newStitch.php',{title:stitchTitle},function(data){
				var returnedData = $.parseJSON(data);
				if(returnedData['STITCH_ID'] !== 0){
					$("select[name='stitchAccount']").append("<option value='"+returnedData['STITCH_ID']+"'>"+stitchTitle+"</option>");
					$("#popUpForm2").fadeOut();$(this).hideOverLay();
					$("select[name='stitchAccount']").focus();
				}
			});
		};
		$.fn.showAddNewStitchDiv = function(){
			$("#fade").fadeIn();
			$("#popUpForm2").fadeIn('200').centerThisDiv();
			$("#popUpForm2 input[type='text']:first").focus();
			$("#popUpForm2 .fa-times").click(function(){ $("#popUpForm2").fadeOut();$(this).hideOverLay(); });
			$(".newStitchTitle").keyup(function(f){
				if(f.keyCode==13&&$(this).val()!==''){
					$(this).addNewStitch();
				}
			});
		};
		$.fn.showAddNewMachineDiv = function(){
			$("#fade").fadeIn();
			$("#popUpForm3").fadeIn('200').centerThisDiv();
			$("#popUpForm3 select").selectpicker();
			$("#popUpForm3 input[type='text']:first").focus();
			$("#popUpForm3 .fa-times").click(function(){ $("#popUpForm3").fadeOut();$(this).hideOverLay(); });
			$(".newMachineTitle").keyup(function(f){
				if(f.keyCode==13&&$(this).val()!==''){
					$(this).addNewMachine();
				}
			});
		};
		$.fn.hideOverLay = function(){
			$("#popUpDel").each(function(index, element) {
				$(this).remove();
			});
			$("#popUpForm2,#popUpForm3,#xfade,#fade").fadeOut();
			$("#popUpForm2,#popUpForm3").css({'top':'-=100px'});
			$("#popUpForm").fadeOut(function(){});
		};
		$(".addNewStitch").click(function(){
			$(this).showAddNewStitchDiv();
		});
		$(".addNewMachine").click(function(){
			$(this).showAddNewMachineDiv();
		});
});
