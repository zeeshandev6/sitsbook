//posting product data to add-performa-invoice.php
$(document).on('click', 'input[name=save]', function(){
    if($('input[name=sc_no]').val() == ''){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("PO Number is required!");
        return false;
    }

    if($('select[name=supplier]').val() == ''){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("Please Select a supplier!");
        return false;
    }
    if((parseFloat($('#pro_table #total_amount').text())||0) <= 0){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("Payment Amount is required!");
        return false;
    }
    var pon = $('input[name=sc_no]').val();
    //table data
    var jsonString;
    var arrReturn = {};
    $('tr#productRecord').each(function(x){
        arrReturn[x] = {
            rowId:          $(this).attr('data-id'),
            processName:    $(this).find('td#process').html(),
            processValue:   $(this).find('td#process').attr('data-id'),
            typeName:       $(this).find('td#type').html(),
            typeValue:      $(this).find('td#type').attr('data-id'),
            design:         $(this).find('td#design').html(),
            construction:   $(this).find('td#construction').html(),
            blend:          $(this).find('td#blend').html(),
            griege_width:   $(this).find('td#griege_width').html(),
            finished_width: $(this).find('td#finished_width').html(),
            quantity:       $(this).find('td#quantity').html(),
            price:          $(this).find('td#price').html(),
            gst:            $(this).find('td#gst').html(),
            amount:         $(this).find('td#amount').html()
        };
    });
    jsonString = JSON.stringify(arrReturn);
    //console.log(jsonString);
    $('input[name=tableValues]').val(jsonString);

    $('form[name=add-processing]').submit(function(){
        $('#myLoading').modal('show');
        return true;
    });
    $('form[name=add-processing]').submit();
});


function calSum(){
    var sum1=0;
    $("td#quantity").each(function(){
        sum1 += parseFloat($(this).text());
    });
    $("td#total_quantity").text(sum1);
    var sum2=0;
    $("td#amount").each(function(){
        sum2 += parseFloat($(this).text());
    });
    $("td#total_amount").text(sum2.toFixed(2));
}

//delete product row
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var rowId   = $(x).parent().parent().attr('data-id');
        $.post("add-processing-detail.php",{del_id:rowId},function(data){});
        $(x).parent().parent().remove();
        calSum();
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}

//adding neow row in detail table
$(document).ready(function(){

    calSum();

    $('#pro_table input').not('input[name=amount]').blur(function(){
        if( $(this).val().length === 0 ) {
            $(this).css({'border-color':'#b94a48', 'box-shadow':'0 0 5px #b94a48'});
        }
        if( $(this).val().length != 0 ) {
            $(this).css({'border-color':'#cccccc','box-shadow':'none'});
        }
    });
    //multiply quantity and unit rate and show total amount
    $("input[name=quantity], input[name=price], input[name=gst]").on('keyup', function(){

        var quantity    = parseFloat($('input[name=quantity]').val()) || 0;
        var rate        = parseFloat($('input[name=price]').val()) || 0;
        var gst         = parseFloat($('input[name=gst]').val()) || 0;
        var total       = quantity*rate;
        var gstPercent  = (total*gst)/100;
        $('input[name=amount]').val((total + gstPercent).toFixed(2));
    });
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow   = $(this).parent().parent();

        //initializing vars with user values
        var process_val   = $('select[name=process]').val();
        var process_text  = $('select[name=process] option:selected').text();
        var type_val      = $('select[name=type]').val();
        var type_text     = $('select[name=type] option:selected').text();

        var design          = $('input[name=design]').val();
        var construction    = $('input[name=construction]').val();
        var blend           = $('input[name=blend]').val();
        var griege_width    = $('input[name=griege_width]').val();
        var finished_width  = $('input[name=finished_width]').val();
        var quantity        = $('input[name=quantity]').val();
        var price           = $('input[name=price]').val();
        var gst             = $('input[name=gst]').val();
        var amount          = $('input[name=amount]').val();

        if((process_val!='')    && (type_val!='')       && (design!='')     && (construction!='')   && (blend!='') &&
           (griege_width!='')   && (finished_width!='') && (quantity!='')   && (price!='')          && (gst!='') && (amount!='') )
        {
            var rw  = "<tr id='productRecord' data-id='0'>"
                +"<td id='blend'>"+blend+"</td><td id='construction'>"+construction+"</td>"
                +"<td id='process' data-id="+process_val+">"+process_text+"</td>"
                +"<td id='type' data-id="+type_val+">"+type_text+"</td>"
                +"<td id='design'>"+design+"</td>"
                +"<td id='griege_width'>"+griege_width+"</td>"
                +"<td id='finished_width'>"+finished_width+"</td>"
                +"<td id='quantity'>"+quantity+"</td>"
                +"<td id='price'>"+price+"</td>"
                +"<td id='gst'>"+gst+"</td>"
                +"<td id='amount'>"+amount+"</td>"
                +"<td>"
                +'<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
                +'<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>'
                +'</td>'
                +'</tr>';

            $('#pro_table').append(rw);
            calSum();
            inputRow.find("input").val('');
            $('select[name=process], select[name=type]').val('');
        }
    });//End appending new row
});

//edit table row in update popup
function editMe(x){
    $(".updatemode").removeClass('updatemode');
    $(x).parent().parent().addClass('updatemode');
    var rowId           = $(".updatemode").attr('data-id');
    var process_id      = $(".updatemode").find('td#process').attr('data-id');
    var type_id         = $(".updatemode").find('td#type').attr('data-id');
    var design          = $(".updatemode").find('td#design').text();
    var construction    = $(".updatemode").find('td#construction').text();
    var blend           = $(".updatemode").find('td#blend').text();
    var griege_width    = $(".updatemode").find('td#griege_width').text();
    var finished_width  = $(".updatemode").find('td#finished_width').text();
    var quantity        = $(".updatemode").find('td#quantity').text();
    var price           = $(".updatemode").find('td#price').text();
    var gst             = $(".updatemode").find('td#gst').text();
    var amount          = $(".updatemode").find('td#amount').text();

    $('select[name=process1] option[value='+process_id+']').prop('selected',true);
    $('select[name=process1]').selectpicker('refresh');
    $('select[name=type1]  option[value='+type_id+']').prop('selected',true).parent().selectpicker('refresh');;
    $('select[name=type1]').selectpicker('refresh');

    $('input[name=rowid]').val(rowId);
    $('input[name=design1]').val(design);
    $('input[name=construction1]').val(construction);
    $('input[name=blend1]').val(blend);
    $('input[name=griege_width1]').val(griege_width);
    $('input[name=finished_width1]').val(finished_width);
    $('input[name=quantity1]').val(quantity);
    $('input[name=price1]').val(price);
    $('input[name=gst1]').val(gst);
    $('input[name=amount1]').val(amount);

    $("#myedit").modal('show');

    //multiply quantity and unit rate and show total amount
    $("input[name=quantity1], input[name=price1], input[name=gst1]").on('keyup', function(){
        var quantity1   = parseFloat($('input[name=quantity1]').val()) || 0;
        var price1      = parseFloat($('input[name=price1]').val()) || 0;
        var gst1        = parseFloat($('input[name=gst1]').val()) || 0;
        var total1      = quantity1*price1;
        var gstPercent1 = (total1*gst1)/100;
        $('input[name=amount1]').val((total1 + gstPercent1).toFixed(2));
    });

    $('#addrow').on('click', function(){
        //initializing vars with user values
        var rowid           = $('input[name=rowid]').val();
        var process1_val    = $('select[name=process1]').val();
        var process1_txt    = $('select[name=process1] option:selected').text();
        var type1_val       = $('select[name=type1]').val();
        var type1_txt       = $('select[name=type1] option:selected').text();

        var design1          = $('input[name=design1]').val();
        var construction1    = $('input[name=construction1]').val();
        var blend1           = $('input[name=blend1]').val();
        var griege_width1    = $('input[name=griege_width1]').val();
        var finished_width1  = $('input[name=finished_width1]').val();
        var quantity1        = $('input[name=quantity1]').val();
        var price1           = $('input[name=price1]').val();
        var gst1             = $('input[name=gst1]').val();
        var amount1          = $('input[name=amount1]').val();

        if((process1_val!='')    && (type1_val!='')       && (design1!='')     && (construction1!='')   && (blend1!='') &&
            (griege_width1!='')   && (finished_width1!='') && (quantity1!='')   && (price1!='')          && (gst1!='') && (amount1!='') )
        {
            var rw  = "<tr id='productRecord' data-id='"+rowid+"'>"
                +"<td id='blend'>"+blend1+"</td><td id='construction'>"+construction1+"</td>"
                +"<td id='process' data-id="+process1_val+">"+process1_txt+"</td>"
                +"<td id='type' data-id="+type1_val+">"+type1_txt+"</td>"
                +"<td id='design'>"+design1+"</td>"
                +"<td id='griege_width'>"+griege_width1+"</td>"
                +"<td id='finished_width'>"+finished_width1+"</td>"
                +"<td id='quantity'>"+quantity1+"</td>"
                +"<td id='price'>"+price1+"</td>"
                +"<td id='gst'>"+gst1+"</td>"
                +"<td id='amount'>"+amount1+"</td>"
                +"<td>"
                +'<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
                +'<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>'
                +'</td>'
                +'</tr>';

            $("tr.updatemode").replaceWith(rw);
            calSum();
            $('select[name=process1], select[name=type1]').selectpicker('val','');
        }
    });
}
