var order_status = function(elm){
  //may be not required
  /*
  <button type="button" class="btn btn-default" onclick="order_status(this);" data-stage="1" data-order-id="<?php echo $i; ?>">
    <i class="fa fa-archive"></i>
  </button>
  */
  var stage    = $(elm).attr("data-stage");
  var order_id = $(elm).attr("data-order-id");
  var status   = '';
  if(stage == 1){
    if($(elm).hasClass('btn-default')){
      $(elm).switchClass('btn-default','btn-success');
      status = 'K';
    }
    if($(elm).hasClass('btn-success')){
      $(elm).switchClass('btn-success','btn-default');
      status = 'N';
    }
    var message = " New Order Issued to Kitchen : Order Number : "+order_id+", Customer Name : " + $(elm).parent().siblings("td.customer_name").text() ;
    notifyMe('uploads/logo/default.png','Food Point',message,null);
  }
};
var enter_deal_row = function(elm){
  if($("tr.deals-quick-insert").first().find("select").selectpicker('val') == ''){
    return;
  }
  $("tr.deals-quick-insert").first().clone().appendTo('tbody.deals-trows');
  var selct = $("tbody.deals-trows tr").last().find("select").clone();
  $("tbody.deals-trows tr").last().find("select").parent().remove();
  $("tbody.deals-trows tr").last().find("td.deals_selector_td").append(selct);
  $("tbody.deals-trows tr").last().find("td.deals_selector_td select").selectpicker().selectpicker('val',$("tr.deals-quick-insert option:selected").val());
  $("tbody.deals-trows tr").last().find("button#deal_enter").replaceWith('<button type="button"  class="btn btn-danger btn-block" name="deal_remove" id="deal_remove" value=""><i class="fa fa-times"></i></button>');
  $("button[name='deal_remove']").taphold(function(){
    $(this).parent().parent().remove();
  });
  $("tbody.deals-trows tr").last().removeClass("deals-quick-insert");

  $("tr.deals-quick-insert").first().find("select").selectpicker('val','');
  $("tr.deals-quick-insert").first().find("input[type=number]").val('');  
};
