//posting product data to add-performa-invoice.php
$(document).on('click', 'input[name=save]', function(){
    $(this).prop("disabled",true);
    if($('input[name=scid]').val() == ''){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("PO Number is required!");
        $(this).prop("disabled",false);
        return false;
    }
    if($('select[name=contract] option:selected').val() == ''){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("Processing Unit Not Selected!");
        $(this).prop("disabled",false);
        return false;
    }
    if($("tr.record_row").length == 0){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("Erro! No table Data found.");
        $(this).prop("disabled",false);
        return false;
    }
    //table data
    var jsonString;
    var arrReturn = {};
    $('tr.record_row').each(function(x){
        arrReturn[x] = {
            row_id:    $(this).attr('data-id'),
            quality:   $(this).find('td.quality').text(),
            issue:     $(this).find('td.issue').text(),
            fresh:     $(this).find('td.fresh').text(),
            sample:    $(this).find('td.sample').text(),
            b_grade:   $(this).find('td.b_grade').text(),
            c_p:       $(this).find('td.c_p').text(),
            received:  $(this).find('td.received').text(),
            difference:$(this).find('td.difference').text()
        };
    });
    jsonString = JSON.stringify(arrReturn);
    $('input[name=json_data]').val(jsonString);
    $('form[name=add_folding_reports]').submit(function(){
        $('#myLoading').modal('show');
        return true;
    });
    $('form[name=add_folding_reports]').submit();
    $(this).prop("disabled",false);
});

//delete product row
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var rowId   = $(x).parent().parent().attr('data-id');
        var del     = $.post('add-processing-report-detail.php', {RowId:rowId}, function(data){});
        if(del){
            $(x).parent().parent().remove();
            calSum();
            $("#pro_table").change();
        }
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
//sum of columns in detail table
function calSum(){
    var sum1=0;
    $("td#fresh").each(function(){
        sum1 += parseFloat($(this).text());
    });
    $("td#total_fresh").text(sum1);
    var sum2=0;
    $("td#amount").each(function(){
        sum2 += parseFloat($(this).text());
    });
    $("td#total_amount").text(sum2);
}


//adding neow row in detail table
$(document).ready(function(){
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow   = $(this).parent().parent();

        var quality     = $('table input.quality').val();
        var issue       = $('table input.issue').val();
        var fresh       = $('table input.fresh').val();
        var sample      = $('table input.sample').val();
        var b_grade     = $('table input.b_grade').val();
        var c_p         = $('table input.c_p').val();
        var received    = $('table input.received').val();
        var difference  = parseInt($('table input.difference').val())||0;

        var raw  = "<tr class='record_row' data-id='0'><td class='quality'>"+quality+"</td><td class='issue'>"+issue+"</td><td class='fresh'>"+fresh+"</td><td class='sample'>"+sample+"</td>"
               +   "<td class='b_grade'>"+b_grade+"</td><td class='c_p'>"+c_p+"</td><td class='received'>"+received+"</td><td class='difference'>"+difference+"</td>"
               +   '<td>'
               +   '<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
               +   '<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a> '
               +   '</td>'
               +   '</tr>';
        $('#pro_table tbody').append(raw);
        $("tr.design").find("input").val('');
        $("tr.design").find("input").first().val(quality);
        $('table input.quality').focus();
        $("#pro_table").change();
    });//End appending new row

    $("table .addup,table .issue").on('keyup blur',function(){
        var issue,received,dif;
        issue = $("table .issue").val();
        received = 0;
        $("table .addup").each(function(){
            received += parseFloat($(this).val())||0;
        });
        $("table .received").val(received);
        if(received > issue){
          dif = received - issue;
        }else{
          dif = issue - received;
        }
        $("table .difference").val((dif).toFixed(0));
    });

    $("#myedit .addup,#myedit .issue").on('keyup blur',function(){
        var issue = $("#myedit .issue").val();
        var received = 0;
        $("#myedit .addup").each(function(){
            received += parseFloat($(this).val())||0;
        });
        if(received > issue){
          dif = received - issue;
        }else{
          dif = issue - received;
        }
        $("#myedit .received").val(received);
        $("#myedit .difference").val((dif).toFixed(0));
    });

});

//edit table row in update popup
function editMe(x){
    $(".updatemode").removeClass('updatemode');
    var rowId       = $(x).parent().parent().attr('data-id');
    var quality     = $(x).parent().parent().find('td.quality').text();
    var issue       = $(x).parent().parent().find('td.issue').text();
    var fresh       = $(x).parent().parent().find('td.fresh').text();
    var sample      = $(x).parent().parent().find('td.sample').text();
    var b_grade     = $(x).parent().parent().find('td.b_grade').text();
    var c_p         = $(x).parent().parent().find('td.c_p').text();
    var received    = $(x).parent().parent().find('td.received').text();
    var difference  = $(x).parent().parent().find('td.difference').text();

    $('#myedit input[name="rowid"]').val(rowId);
    $('#myedit input.quality').val(quality);
    $('#myedit input.issue').val(issue);
    $('#myedit input.fresh').val(fresh);
    $('#myedit input.sample').val(sample);
    $('#myedit input.b_grade').val(b_grade);
    $('#myedit input.c_p').val(c_p);
    $('#myedit input.received').val(received);
    $('#myedit input.difference').val(difference);

    $(x).parent().parent().addClass('updatemode');
    $("#myedit").modal('show');

    $('#addrow').on('click', function(){
        //initializing vars with user values
        var rowid          = $('input[name=rowid]').val();

        var quality     = $('#myedit input.quality').val();
        var issue       = $('#myedit input.issue').val();
        var fresh       = $('#myedit input.fresh').val();
        var sample      = $('#myedit input.sample').val();
        var b_grade     = $('#myedit input.b_grade').val();
        var c_p         = $('#myedit input.c_p').val();
        var received    = $('#myedit input.received').val();
        var difference  = $('#myedit input.difference').val();

        var raw  = "<tr class='record_row' data-id='"+rowid+"'><td class='quality'>"+quality+"</td><td class='issue'>"+issue+"</td><td class='fresh'>"+fresh+"</td><td class='sample'>"+sample+"</td>"
               +   "<td class='b_grade'>"+b_grade+"</td><td class='c_p'>"+c_p+"</td><td class='received'>"+received+"</td><td class='difference'>"+difference+"</td>"
               +   '<td>'
               +   '<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
               +   '<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a> '
               +   '</td>'
               +   '</tr>';

        $("tr.updatemode").replaceWith(raw);
        $("tr.design").find("input").val('');
        $("tr.design").find("input").first().val(quality);
        $('table input.quality').focus();
        $("#pro_table").change();
    });
}
