$(document).ready(function() {
    $(".h3_php_error").click(function(){
	$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});
	$("#main-nav li ul").hide();
	$("#main-nav li a.nav-top-item").click(
		function(){
			$(this).parent().siblings().find("ul").slideUp("normal");
			$(this).next().slideToggle("normal");
		}
	);
	var fileSelf = $(".this_file_name").val();
	$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
	$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");
	$("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);

	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	/*$(".content-box-header h3").click(
		function () {
		  $(this).parent().next().toggle();
		  $(this).parent().parent().toggleClass("closed-box");
		  $(this).parent().find(".content-box-tabs").toggle();
		}
	);*/

	// Content box tabs:

	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseInt($(this).text());
        });
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseFloat($(this).text())||0;
        });
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text(sumAll);
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$.post(file, {id : idValue}, function(data){
			var supplierName = currentRow.children("td").first().next().next().text();
			if(data==1){
				$("#popUpDel .confirm").text(" "+supplierName+"Contains Information! Do You Really Want To Delete "+supplierName+"?.");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						if(data == 'L'){
							$("#popUpDel .confirm").text(" Stock is Issued To Sheds Cannot Delete This Record! ");
							$(".dodelete").hide();
							$(".nodelete").text('Close');
							$(".nodelete").click(function(){
								$("#popUpDel").slideUp();
								$("#xfade").fadeOut();
							});
						}else{
							currentRow.slideUp();
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut();
						}
					});
				});
			}else{
				$("#popUpDel .confirm").text("Are Sure you Want To Delete "+supplierName+"?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						currentRow.slideUp();
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				});
			}
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = parseFloat($("input.taxRate").val())||0;
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
			$(this).calculateRowTotal();
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		if(item_id != ''){
			$.post('db/get-item-details.php',{s_item_id:item_id},function(data){
				data = $.parseJSON(data);
				$("input.unitPrice").val(data['S_PRICE']);
				if (data['CARTON_DOZEN'] == 'D') {
					$('input.dozen_price').val(data['DOZEN_RATE']);
				} else {
					$('input.qty_carton').val(data['QTY_CARTON']);
					$('input.rate_carton').val(data['RATE_CARTON']);
				}
				$("input.inStock").attr('thestock',data['STOCK']).val(data['STOCK']);
			});
		}
	};
	$.fn.quickSave = function(){
		$(this).calculateRowTotal();
		var item_id      = parseInt($(".itemSelector option:selected").val())||0;
		var item_name    = $(".itemSelector option:selected").text();
		var cartons      = parseFloat($("input.cartons").val())||0;
		var qty_carton   = parseFloat($("input.qty_carton").val())||0;
		var rate_carton  = parseFloat($("input.rate_carton").val())||0;
		var dozen 		 = parseFloat($("input.dozen").val()) || 0;
		var dozen_price  = parseFloat($("input.dozen_price").val()) || 0;
		var quantity     = parseFloat($("input.quantity").val())||0;
		var unitPrice    = parseFloat($("input.unitPrice").val())||0;
		var discount     = $("input.discount").val();
		var subAmount    = $("input.subAmount").val();
		var taxRate      = parseFloat($("input.taxRate").val())||0;
		var taxAmount    = $("input.taxAmount").val();
		var totalAmount  = $("input.totalAmount").val();
		$(this).blur();
		if(taxRate == '' || taxRate == 0){
			taxAmount = 0;
		}
		var updateMode = $(".updateMode").length;
		if(item_id > 0 && ((quantity+cartons+dozen) > 0) && unitPrice > 0){
			if(updateMode == 0){
				var tr = '';
				tr += '<tr class="alt-row calculations transactions" data-row-id="0">';
				tr += '<td style="text-align:left;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>';
				tr += '<td class="text-center cartons" data-qty="'+qty_carton+'">'+cartons+'</td>';
				tr += '<td class="text-center rate_carton">'+rate_carton+'</td>';
				tr += '<td class="text-center dozen">' + dozen + '</td>';
				tr += '<td class="text-center dozen_price">' + dozen_price + '</td>';
				tr += '<td class="text-center quantity">'+quantity+'</td>';
				tr += '<td style="text-align:center;" class="unitPrice">'+unitPrice+'</td>';
				tr += '<td style="text-align:center;" class="discountAmount">'+discount+'</td>';
				tr += '<td style="text-align:center;" class="subAmount">'+subAmount+'</td>';
				tr += '<td style="text-align:center;" class="taxRate">'+taxRate+'</td>';
				tr += '<td style="text-align:center;" class="taxAmount">'+taxAmount+'</td>';
				tr += '<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>';
				tr += '<td style="text-align:center;"> - - - </td>';
				tr += '<td style="text-align:center;">';
				tr += '<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>';
				tr += '<a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a>';
				tr += '</td>';
				tr += '</tr>';
				$(tr).insertAfter($(".calculations").last());
			}else if(updateMode == 1){
				$(".updateMode").find('td.itemName').attr('data-item-id',item_id);
				$(".updateMode").find('td.cartons').text(cartons);
				$(".updateMode").find('td.cartons').attr('data-qty',qty_carton);
				$(".updateMode").find('td.rate_carton').text(rate_carton);
				$(".updateMode").find('td.dozen').text(dozen);
				$(".updateMode").find('td.dozen_price').text(dozen_price);
				$(".updateMode").find('td.quantity').text(quantity);
				$(".updateMode").find('td.unitPrice').text(unitPrice);
				$(".updateMode").find('td.discount').text(discount);
				$(".updateMode").find('td.subAmount').text(subAmount);
				$(".updateMode").find('td.taxRate').text(taxRate);
				$(".updateMode").find('td.taxAmount').text(taxAmount);
				$(".updateMode").find('td.totalAmount').text(totalAmount);
				$(".updateMode").removeClass('updateMode');
			}
			$(this).clearPanelValues();
			$(this).calculateColumnTotals();
			$("div.itemSelector button").focus();
		}else{
			displayMessage("Values Missing!");
		}
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.cartons","td.total_cartons");
		$(this).sumColumn("td.dozen", "td.total_dozens");
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
	$.fn.calculateRowTotal = function(){
    var taxRate,taxType,cartons,rate_carton,dozen,dozen_price,qty,price,amount,discountAvail;
		taxRate       = parseFloat($("input.taxRate").val())||0;
		taxType       = ($(".taxType").is(":checked"))?"I":"E";
		cartons       = parseFloat($("input.cartons").val())||0;
		rate_carton   = parseFloat($("input.rate_carton").val())||0;
		dozen		  = parseFloat($("input.dozen").val())||0;
		dozen_price	  = parseFloat($("input.dozen_price").val())||0;
		qty           = parseFloat($("input.quantity").val())||0;
		price         = parseFloat($("input.unitPrice").val())||0;
		amount = Math.round(((cartons * rate_carton) + (dozen*dozen_price)+(qty*price))*100)/100;
		discountAvail = parseFloat($("input.discount").val())||0;

		amount              = Math.round(amount*100)/100;
		discountAvail       = Math.round(discountAvail*100)/100;
		discountPerCentage  = amount*(discountAvail/100);
		discountPerCentage  = Math.round(discountPerCentage*100)/100;
		amount -= discountPerCentage;

		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount * (taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount * (taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		taxAmount = Math.round(taxAmount*100)/100;

		amount          = Math.round(amount*100)/100;
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount     = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		  -= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
            stockOfBill += parseInt($(this).text())||0;
        });
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$(".itemSelector option").prop('selected',false);
		$(".itemSelector").find("option").first().prop('selected',true);
		$(".itemSelector").selectpicker('refresh');
		$("tr.quickSubmit input[type=text]").val("");
		$("input.inStock").val('').attr('thestock','');
	};
});

	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name       = thisRow.find('td.itemName').text();
		var item_id         = (parseInt(  thisRow.find('td.itemName').attr('data-item-id'))||0);
		var cartons         = (parseFloat(thisRow.find('td.cartons').text())||0);
		var qty_carton      = (parseFloat(thisRow.find('td.cartons').attr('data-qty'))||0);
		var rate_carton     = (parseFloat(thisRow.find('td.rate_carton').text())||0);
		var dozen 			= (parseFloat(thisRow.find('td.dozen').text()) || 0);
		var dozen_price 	= (parseFloat(thisRow.find('td.dozen_price').text()) || 0);
		var quantity        = (parseFloat(thisRow.find('td.quantity').text())||0);
		var quantityLimit   = (parseFloat(thisRow.find('td.quantity').attr('data-limit'))||0);
		var unitPrice       = (parseFloat(thisRow.find('td.unitPrice').text())||0);
		var discount        = (parseFloat(thisRow.find('td.discount').text())||0);
		var subAmount       = (parseFloat(thisRow.find('td.subAmount').text())||0);
		var taxRate         = (parseFloat(thisRow.find('td.taxRate').text())||0);
		var taxAmount       = (parseFloat(thisRow.find('td.taxAmount').text())||0);
		var totalAmount     = (parseFloat(thisRow.find('td.totalAmount').text())||0);

		if(item_id == 0){
			return false;
		}
		$("select.itemSelector option[value='"+item_id+"']").prop("selected",true);
		$("select.itemSelector").selectpicker('refresh');
		$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
			data = $.parseJSON(data);
			var itemStock = data['STOCK'];
			$("input.inStock").attr('thestock',itemStock).val(itemStock);
			$("tr.quickSubmit #itemName").text(item_name);
			$("input.cartons").val(cartons).attr('data-qty',qty_carton);
			$("input.dozen").val(dozen);
			$("input.dozen_price").val(dozen_price);
			$("input.rate_carton").val(rate_carton);
			$("input.quantity").val(quantity).attr('data-limit',quantityLimit);
			$("input.quantity").animate({'background-color':'rgba(102,255,102,0.7)'},300,function(){
				$("input.quantity").animate({'background-color':'#FFF'},300).keyup();
			});
			$("input.unitPrice").val(unitPrice);
			$("input.discount").val(discount);
			$("input.subAmount").val(subAmount);
			taxRate = (taxRate == '')?"0":taxRate;
			$("tr.quickSubmit .taxTd input").val(taxRate);
			$("input.taxAmount").val(taxAmount);
			$("input.totalAmount").val(totalAmount);
			stockOs();
			$("input.quantity").focus();
		});
	};
	var stockOs = function(){
    var this_qty,qty,carton,qty_carton,inStock,NewStock,thisBillQuantity;
		qty        = parseFloat($("input.quantity").val())||0;
		carton     = parseFloat($("input.cartons").val())||0;
		qty_carton = parseFloat($("input.qty_carton").val())||0;
		this_qty   = parseFloat(qty + (carton*qty_carton))||0;
		inStock    = parseInt($("input.inStock").attr('thestock'))||0;
		thisBillQuantity = 0;
		$("tr.transactions").each(function(){
			qty               = parseInt($(this).find("td.quantity").text())||0;
			carton            = parseInt($(this).find("td.cartons").text())||0;
			qty_carton        = parseInt($(this).find("td.qty_carton").text())||0;
			thisBillQuantity += parseFloat(qty + (carton*qty_carton))||0;
		});
		NewStock = this_qty + inStock;
		NewStock += thisBillQuantity;
		$("input.inStock").val(NewStock);
	};
	var saveSale = function(){
		var sale_id		    = $(".sale_id").val();
		var saleDate 	    = $(".datepicker").val();
		var billNum 	    = $("input.bill_num").val();
		var po_number 	    = $("input.po_number").val();
		var supplierCode    = $("select.supplierSelector option:selected").val();
		var order_taker     = $("select[name=order_taker] option:selected").val();
    	var salesman        = $("select[name=salesman] option:selected").val();
		var customer_name   = $("input.customer_name").val();
		var over_total      = parseFloat($("td.amountTotal").text())||0;
		var bill_discount   = 0;
		if(supplierCode  == ''){
			displayMessage('Please, Select an Account!');
			return false;
		}
		if(billNum == 0){
			displayMessage('Please, Enter Memo Number!');
			return false;
		}
		if($("tr.transactions").length == 0){
			displayMessage('Please Make at least One Transaction!');
			return false;
		}
		$(".saveSale").hide();
		var trans_rows = {};
		$("tr.transactions").each(function(i, element){
			var thisRow   = $(this);
      trans_rows[i] = {
        row_id          : $(this).attr('data-row-id'),
        item_id         : (parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0),
        cartons         : (parseFloat(thisRow.find('td.cartons').text())||0),
        qrt_carton      : (parseFloat(thisRow.find('td.cartons').attr('data-qty'))||0),
		rate_carton     : (parseFloat(thisRow.find('td.rate_carton').text())||0),
		dozen 			: (parseFloat(thisRow.find('td.dozen').text()) || 0),
		dozen_price     : (parseFloat(thisRow.find('td.dozen_price').text()) || 0),
        quantity        : (parseFloat(thisRow.find('td.quantity').text())||0),
        unitPrice       : (parseFloat(thisRow.find('td.unitPrice').text())||0),
        discount        : (parseFloat(thisRow.find('td.discount').text())||0),
        subAmount       : (parseFloat(thisRow.find('td.subAmount').text())||0),
        taxRate         : (parseFloat(thisRow.find('td.taxRate').text())||0),
        taxAmount       : (parseFloat(thisRow.find('td.taxAmount').text())||0),
        totalAmount     : (parseFloat(thisRow.find('td.totalAmount').text())||0)
      };
    });
    trans_rows = JSON.stringify(trans_rows);
		$.post("db/saveDistSaleReturn.php",{sale_id:sale_id,
									        return_type:'O',
									        order_taker:order_taker,
									        salesman:salesman,
									        saleDate:saleDate,
									        billNum:billNum,
									        po_number:po_number,
									        customerCode:supplierCode,
									        customer_name:customer_name,
									        discount_type:'P',
									        over_discount:0,
									        bill_discount:bill_discount,
									        over_total:over_total,
									        trans_rows:trans_rows},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					var msg_type = '';
					if(sale_id == 0){
						msg_type = "&saved";
					}else{
						msg_type = "&updated";
					}
					window.location.href = 'dist-sale-return-details.php?id='+data['ID']+msg_type;
				}else{
					displayMessage(data['MSG']);
          $(".saveSale").show();
				}
		});
	};
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};

	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
		});
	};

	var getBillsBySupplierCode = function(){
		var supplierCode = $("select.supplierSelector option:selected").val();
		if(supplierCode == ''){
			$("div.supplierSelector button").focus();
			return false;
		}
		$.post('db/getBillListByCustomerCode.php',{supplierCode:supplierCode},function(data){
			if(data != ''){
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").append(data);
				$("select.billSelector").selectpicker('refresh');
				$("div.billSelector button").focus();
			}else{
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").selectpicker('refresh');
				$("div.supplierSelector button").focus();
			}
		});
	};
	var makeItCash = function(checker){
		var what = $(checker).is(":checked");
		if(what){
			$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
			$("select.supplierSelector").selectpicker('refresh');
			$(".supplier_name_div").show().focus();
		}else{
			$("select.supplierSelector option[value^='010104']").prop('disabled',false).prop('selected',false);
			$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector").selectpicker('refresh');
			$(".supplier_name_div").val('').hide();
		}
	};
	var getBillDetails = function(){
		var supplier_code  = $("select.supplierSelector option:selected").val();
		var bill_number	   = parseInt($("select.billSelector option:selected").val())||0;
		var sale_id    = $(".sale_id").val();
		if(bill_number == 0 && supplier_code == ''){
			return false;
		}
		$.post('db/getSaleBill.php',{sale_id:sale_id,supplier_code:supplier_code,bill_num:bill_number},function(data){
			$("tr.calculations").first().nextAll(".calculations").remove();
			$(data).insertAfter($("tr.calculations").first());
			$(this).calculateColumnTotals();
			if(sale_id == 0){
				$("tr.isRedRow").find('td').animate({'background-color':'rgba(255,0,0,0.2)'},300);;
			}
		});
	};
