$(document).ready(function(e){

	$("table").on('click',"a.pointer",function(){
		$(this).deleteItem('db/del-item.php');
	});

	$("table").on('change','.checkActive',function(){
		var checked = 'N';
		var data_id = parseInt($(this).attr('do'))||0;
		if($(this).is(":checked")){
			checked = 'Y';
		}
		if(data_id > 0){
			$.post('db/itemStatus.php',{data_id:data_id,status:checked},function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'N'){
					alert('Status Could Not Be Changed!!');
				}
			});
		}
	});
	$.fn.barcodeAvailable = function(){
		var $thisElm = $(this);
		var item_id  = parseInt($("input[name=itemID]").val())||0;
		$(this).on('keyup blur',function(e){
			$.get('db/compare-barcode.php',{q:$(this).val(),id:item_id},function(data){
				if(data > 0){
					$("input:submit").prop('disabled',true);
					$($thisElm).addClass('error');
				}else{
					$("input:submit").prop('disabled',false);
					$($thisElm).removeClass('error');
				}
			});
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};

    $.fn.deleteItem = function(file){
		var idValue = $(this).attr("do");
		var clickedDel = $(this);
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
				$.post(file, {id : idValue}, function(data){
					data = $.parseJSON(data);
					$("#popUpDel").children(".confirm").text(data['MSG']);
					$("#popUpDel").children(".dodelete").hide();
					$("#popUpDel").children(".nodelete").text("Close");
					if(data['OK'] == 'Y'){
						clickedDel.parent().parent().remove();
					}
				});
			});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#fade").fadeOut('fast');
		});
	};
	$("tr.itemCategory").click(function(){
		$(this).toggle_category_list();
	});
	$.fn.toggle_category_list = function(){
		$toggle_button = $(this).find(".toggle_cat");
		var cat_id = parseInt($toggle_button.attr('data-cat-id'))||0;
		if(cat_id > 0){
			if($("#recordPanel[data-cat-id='"+cat_id+"']").length){
				if($("#recordPanel[data-cat-id='"+cat_id+"']").is(":visible")){
					$("#recordPanel[data-cat-id='"+cat_id+"']").hide();
					$toggle_button.find("i").addClass('fa-plus').removeClass('fa-minus');
				}else{
					$("#recordPanel[data-cat-id='"+cat_id+"']").show();
					$toggle_button.find("i").addClass('fa-minus').removeClass('fa-plus');
				}
			}else{
				$("#xfade").fadeIn();
				$.post('db/get-item-list.php',{cat_id:cat_id},function(data){
					$(data).insertAfter($("tr[data-cat-id='"+cat_id+"']"));
					$("#xfade").fadeOut();
					$toggle_button.find("i").addClass('fa-minus').removeClass('fa-plus');
					$("td[data-grey]").css({"background-color":"#F5F5F5"});
				});
			}
		}
	};
});
var hidePopUpBox = function(){
	$("#popUpBox").css({'left':'-=500px'});
	$("#popUpBox").css({'transform':'scaleY(0.0)'});
	$("#xfade").fadeOut();
	$("#popUpBox").remove();
};
var addCategory = function(){
	$("body").append("<div id='popUpBox'></div>");
	var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New Category Title:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control categoryName" />';
		formContent += '</p>';
		formContent += '</div>';
	$("#popUpBox").append(formContent);
	$("#xfade").fadeIn();
	$("#popUpBox").fadeIn('200').centerThisDiv();
	$("#popUpBox .categoryName").focus();
	$(".categoryName").keyup(function(e){
		var name = $(".categoryName").val();
		if(e.keyCode == 13){
			$(".categoryName").blur();
			if(name != ''){
				$.post('db/saveCategory.php',{name:name},function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("select.catSelector").append('<option value="'+data['ID']+'">'+name+'</option>');
						$("select.catSelector option:selected").prop('selected',false);
						$("select.catSelector option").last().prop('selected',true);
						$("select.catSelector").selectpicker('refresh');
						hidePopUpBox();
					}else if(data['OK'] == 'N'){
						alert('New Category Could Not be Created!');
					}
				});
			}
		}
	});
};
var deleteCategory = function(){
	var idValue = parseInt($("select.catSelector option:selected").val()) || 0;
	var categoryName = $("select.catSelector option:selected").text();
	var file = "db/deleteCategory.php";
	if(idValue == 0){
		displayMessage('Category Not Selected');
		return false;
	}
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete ( "+categoryName+" )?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").hide();
	$("#popUpDel").centerThisDiv();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
			$.post(file, {id : idValue}, function(data){
				data = $.parseJSON(data);
				$("#popUpDel").children(".confirm").text(data['MSG']);
				$("#popUpDel").children(".dodelete").hide();
				$("#popUpDel").children(".nodelete").text("Close");
				if(data['OK'] == 'Y'){
					$("select.catSelector option[value='"+idValue+"']").remove();
					$("select.catSelector option").prop('selected',false).first().prop('selected',true);
					$("select.catSelector").selectpicker('refresh');
				}
			});
		});
	$(".nodelete").click(function(){
		$("#fade").fadeOut();
		$("#popUpDel").fadeOut();
	});
	$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
	});
};
var addMeasure = function(){
	$("body").append("<div id='popUpBox'></div>");
	var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New Measure Name:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control measureName" />';
		formContent += '</p>';
		formContent += '</div>';
	$("#popUpBox").append(formContent);
	$("#xfade").fadeIn();
	$("#popUpBox").fadeIn('200').centerThisDiv();
	$("#popUpBox .measureName").focus();
	$(".measureName").keyup(function(e){
		var name = $(".measureName").val();
		if(e.keyCode == 13){
			$(".measureName").blur();
			if(name != ''){
				$.post('db/measureManagement.php',{save:name},function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("select.measureSelector").append('<option value="'+data['ID']+'">'+name+'</option>');
						$("select.measureSelector option:selected").prop('selected',false);
						$("select.measureSelector option").last().prop('selected',true);
						$("select.measureSelector").selectpicker('refresh');
						hidePopUpBox();
					}else if(data['OK'] == 'N'){
						alert('New Measure Could Not be Created!');
					}
				});
			}
		}
	});
};
var deleteMeasure = function(){
	var idValue = parseInt($("select.measureSelector option:selected").val()) || 0;
	var categoryName = $("select.measureSelector option:selected").text();
	var file = "db/measureManagement.php";
	if(idValue == 0){
		displayMessage('Measure Not Selected');
		return false;
	}
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete ( "+categoryName+" )?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").hide();
	$("#popUpDel").centerThisDiv();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
			$.post(file, {del : idValue}, function(data){
				data = $.parseJSON(data);
				$("#popUpDel").children(".confirm").text(data['MSG']);
				$("#popUpDel").children(".dodelete").hide();
				$("#popUpDel").children(".nodelete").text("Close");
				if(data['OK'] == 'Y'){
					$("select.measureSelector option[value='"+idValue+"']").remove();
					$("select.measureSelector option").prop('selected',false).first().prop('selected',true);
					$("select.measureSelector").selectpicker('refresh');
				}
			});
		});
	$(".nodelete").click(function(){
		$("#fade").fadeOut();
		$("#popUpDel").fadeOut();
	});
	$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
	});
};
var getInventoryHierarchy = function(thisElement){
	var thisType = $(thisElement).attr('data-type');
	var thisId   = $(thisElement).find("option:selected").val();

	$.post('db/getInventoryHierarchy.php',{type:thisType,id:thisId},function(data){
		if(thisType == 'D'){
			$("select.CategorySelector").html(data);
			$("select.CategorySelector").selectpicker('refresh');
		}

	});
};
