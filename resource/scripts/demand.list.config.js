$(function(){
    $("select.item_list_def").clone().appendTo($("table td.item_select"));
    $("table select").removeClass('item_list_def');
    var ixd;
    $("table select").each(function(){
        ixd = parseInt($(this).parent().attr('data-item-id'))||0;
        if(ixd > 0){
            $(this).find("option[value='"+ixd+"']").prop("selected",true);
        }
    });
    $("table select").selectpicker();
    $('input.datepicker').datepicker({
        dateFormat:'dd-mm-yy',
        showAnim: 'show',
        changeMonth: true,
        changeYear: true,
        yearRange: '2000:+5'
    });
    $("button.save_demand_list").click(function(){
        saveDemandList();
    });
    $("input.status_id").change(function(){
        var status,id;
        id     = parseInt($(this).attr('data-id'))||0;
        status = $(this).prop("checked")?"Y":"N";
        $.post('purchase-ordering.php',{status_id:id,status:status},function(data){});
    });
});
var add_row       = function(elm){
    var row = $(elm).parent().parent();
    $(row).addClass('cp').clone().insertAfter(row);
    $("tr.cp").last().find("td.item_select").html('');
    $("tr.cp").last().find("input:text").val('');
    $("tr.cp").last().attr('data-row-id',0);
    $("select.item_list_def").clone().appendTo($("tr.cp").last().find("td.item_select"));
    $("table select").removeClass('item_list_def');
    $("tr.cp").removeClass('cp');
    $("table select").selectpicker();
}
var delete_row = function(elm){
    var row = $(elm).parent().parent();
    if($(row).siblings().length > 0){
        $(row).addClass('deletting');
        $("#confirmation").modal('show');
        $("#confirmation #delete").click(function(){
            $("tr.deletting").hide(function(){
                $(this).remove();
            });
        });
    }
}
var delete_demand_list = function(elm){
    var this_row = $(elm).parent().parent();
    $("#confirmation").modal('show');
    var delete_id = $(elm).attr('value');
    $("#confirmation #delete").unbind('click');
    $("#confirmation #delete").bind('click',function(){
        $.post('purchase-ordering.php',{delete_id:delete_id},function(data){
            alert('check');
            if(data == 1){
                showInformation('Success!',"Record Deleted Successfully!");
                $(this_row).remove();
            }
        });
    });
}
var showInformation = function(title,message){
    $("#information .modal-title").text(title);
    $("#information .modal-body p").text(message);
    $("#information").modal('show');
}
var saveDemandList  = function(){
    var dl_id             = parseInt($("input[name='dl_id']").val())||0;
    var po_number         = $("input[name='po_number']").val();
    var entry_date        = $("input[name='entry_date']").val();
    var required_date     = $("input[name='required_date']").val();
    var supplier_acc_code = $("select[name='supplier_acc_code'] option:selected").val();
    if(po_number == ''){
        showInformation('Error!','PO Number is missing.');
        return;
    }
    var json_data = {};
    var titem;

    $("tr.item_row").each(function(i,e){
        titem                       = parseInt($(this).find('td.item_select select option:selected').val())||0;
        if(titem == 0){
            return;
        }
        json_data[i]                = {};
        json_data[i]['row_id']      = parseInt($(this).attr('data-row-id'))||0;
        json_data[i]['item_id']     = titem;
        json_data[i]['item_qty']    = $(this).find('td.item_qty input').val();
        json_data[i]['unit_rate']   = $(this).find('td.unit_rate input').val();
    });
    json_data = JSON.stringify(json_data);

    $.post('purchase-ordering.php',{dl_id:dl_id,po_number:po_number,supplier_acc_code:supplier_acc_code,entry_date:entry_date,required_date:required_date,json_data:json_data},function(data){
        if(data == 'save'){
            var msg = " PO # "+po_number+"  Saved Successfully."
        }
        if(data == 'update'){
            var msg =  " PO # "+po_number+"  Updated Successfully."
        }
        notifyMe('uploads/logo/default.png','Purchase Ordering',msg,null);
        window.location.href = 'purchase-ordering.php?mode=form';
    });
}