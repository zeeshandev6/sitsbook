$(document).ready(function (e) {
	$("input[name=labour],input[name=overhead]").prop('readonly', true);
	initializeIssueEvents();
	initializeProduceEvents();
	$("input.issue_quantity,input.produce_quantity").numericFloatOnly();
	var work_in_process_id = $(".wip_id").val();
	if (work_in_process_id > 0) {
		loadWorkInProcess(work_in_process_id);
	}
});
var initializeIssueEvents = function () {
	$('select').selectpicker();
	$(".timepicker").timepicker();
	$("select.labour_account").change(function () {
		if ($(this).selectpicker('val') != '') {
			$("input[name=labour]").prop('readonly', false).focus();
		} else {
			$("input[name=labour]").prop('readonly', true);
			$("input[name=labour]").val(0);
		}
		calculateColumnTotals('issue');
	});
	$("select.overhead_account").change(function () {
		if ($(this).selectpicker('val') != '') {
			$("input[name=overhead]").prop('readonly', false).focus();
		} else {
			$("input[name=overhead]").prop('readonly', true);
			$("input[name=overhead]").val(0);
		}
		calculateColumnTotals('issue');
	});
	$("select.labour_account").change();
	$("select.overhead_account").change();
	$("input[name=labour],input[name=overhead]").keyup(function () { calculateColumnTotals('issue') });
	$(".issue_item_select").change(function () {
		getItemStock($(this));
	});
	$("div.issue_item_select button").keydown(function (e) {
		if (e.keyCode == 13 && $("select.issue_item_select option:selected").val() != '') {
			$("input.issue_quantity").focus();
		}
	});
	$("input.issue_quantity").keyup(function (e) {
		var thisQty = parseFloat($("input.issue_quantity").val()) || 0;
		var stockOs = parseInt($(".issue_stock_in_hand").attr('stock_os')) || 0;
		var issue_unit_cost = parseFloat($(".issue_unit_cost").val()) || 0;
		var stock_check = $("input.stock_check").val();
		var qtyModifier = 0;
		var selected_item_id = $("select.issue_item_select option:selected").val();

		if ($("tr.transactions").length) {
			$("tr.updateMode").each(function (index, element) {
				if ($(this).find('td.issue_item_name').attr('data-item-id') == selected_item_id) {
					qtyModifier -= parseInt($(this).find("td.issue_quantity").text()) || 0;
				}
			});
		}
		stockOs -= qtyModifier;
		$("input.issue_stock_in_hand").val(stockOs - thisQty);
		if (issue_unit_cost <= 0) {
			//$("input.issue_quantity").val('');
			//return false;
		} else {
			$("input.issue_gross_cost").val((thisQty * issue_unit_cost).toFixed(2));
		}
		if ((stockOs < thisQty) && stock_check == 'Y') {
			//$("input.issue_quantity").val('');
			//$("input.issue_stock_in_hand").val($("input.issue_stock_in_hand").attr('stock_os'));
		}
	});
	$("input.issue_quantity").keydown(function (e) {
		if (e.keyCode == 13) {
			$("input.quick_submit[data-type='issue']").focus();
		}
	});
	$("input.quick_submit").keydown(function (e) {
		if (e.keyCode == 13) {
			quickSave($(this));
		}
	});
	$("button.saveProcess").click(function () {
		saveProcess();
	});
	$(window).keyup(function (e) {
		if (e.altKey == true && e.keyCode == 83) {
			e.preventDefault();
			$("#form").click();
			$("button.saveProcess").click();
			return false;
		}
	});
};
var initializeProduceEvents = function () {
	$("select.produce_item_select").change(function () {
		getItemStock($(this));
	});
	$("div.produce_item_select button").keydown(function (e) {
		if (e.keyCode == 13 && $("select.produce_item_select option:selected").val() != '') {
			$("input.produce_quantity").focus();
		}
	});
	calculateColumnTotals('produce');
	$("input.produce_quantity,input.produce_unit_cost").keyup(function (e) {
		var thisQty = parseFloat($("input.produce_quantity").val()) || 0;
		var stockOs = parseInt($("input.produce_stock_in_hand").attr('stock_os')) || 0;
		var produce_unit_cost = parseFloat($("input.produce_unit_cost").val()) || 0;
		var stock_check = $("input.stock_check").val();
		var qtyModifier = 0;
		var selected_item_id = $("select.produce_item_select option:selected").val();

		if ($("tr.transactions").length) {
			$("tr.updateMode").each(function (index, element) {
				if ($(this).find('td.produce_item_name').attr('data-item-id') == selected_item_id) {
					qtyModifier -= parseInt($(this).find("td.produce_quantity").text()) || 0;
				}
			});
		}
		stockOs -= qtyModifier;
		$("input.produce_stock_in_hand").val(stockOs + thisQty);
		if (produce_unit_cost <= 0) {
			//$("input.produce_quantity").val('');
			//return false;
		} else {
			$("input.produce_gross_cost").val((thisQty * produce_unit_cost).toFixed(2));
		}
	});
	$("input.produce_quantity").keydown(function (e) {
		if (e.keyCode == 13) {
			$("input.produce_unit_cost").focus();
		}
	});
	$("input.produce_unit_cost").keydown(function (e) {
		if (e.keyCode == 13) {
			$("input.quick_submit[data-type='produce']").focus();
		}
	});
};
var enterEvents = function ($elm1, $elm2) {
	$($elm1).keyup(function (e) {
		if (e.keyCode == 13 && $($elm1).val() != '') {
			$($elm2).focus();
		}
	});
};
var delete_detail_row = function (elm) {
	var id = parseInt($(elm).parent().parent().attr("data-row-id")) || 0;
	var type = $(elm).parent().parent().attr("data-type");
	if (id > 0) {
		$("div#form").first().append('<input value="' + id + '" class="deleted_' + type + '_rows" />');
	}
	$(elm).parent().parent().remove();
}
var clearPanelValues = function (type) {
	$("select." + type + "_item_select").selectpicker('val', '');
	$("input." + type + "_quantity").val('');
	$("input." + type + "_unit_cost").val('');
	$("input." + type + "_gross_cost").val('');
};
var calculateColumnTotals = function (type) {
	var quantity = 0;
	var gross_total = 0;

	$("td." + type + "_quantity").each(function (index, element) {
		quantity += parseFloat($(this).text()) || 0;
	});
	$("td." + type + "_gross_cost").each(function (index, element) {
		gross_total += parseFloat($(this).text()) || 0;
	});
	$("td." + type + "_quantity_total").text(quantity);
	$("td." + type + "_cost_total").text((gross_total).toFixed(2));
	if (type == 'issue') {
		gross_total += parseFloat($("input[name=labour]").val()) || 0;
		gross_total += parseFloat($("input[name=overhead]").val()) || 0;
		$("input[name=final_cost]").val((gross_total).toFixed(2));
	}
};
var quickSave = function (elm) {
	var type = $(elm).attr("data-type");
	var item_id = parseInt($("select." + type + "_item_select option:selected").val()) || 0;
	var item_name = $("select." + type + "_item_select option:selected").text();
	var quantity = parseInt($("input." + type + "_quantity").val()) || 0;
	var issue_unit_cost = $("input." + type + "_unit_cost").val();
	var issue_gross_cost = $("input." + type + "_gross_cost").val();
	var updateMode = $(".updateMode").length;
	$(".quick_submit").blur();
	if (item_id == 0 && quantity == 0) {
		displayMessage('Values Are Missing!');
		return false;
	}
	var row_id = 0;
	if(updateMode == 1){
		row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
	}
	var $theNewRow = '<tr class="alt-row transactions" data-type="' + type + '" data-row-id="'+row_id+'"><td style="text-align:left;" class="' + type + '_item_name" data-item-id="' + item_id + '">' + item_name + '</td><td style="text-align:center;" class="' + type + '_quantity">' + quantity + '</td><td style="text-align:center;" class="' + type + '_unit_cost">' + issue_unit_cost + '</td><td style="text-align:center;" class="' + type + '_gross_cost">' + issue_gross_cost + '</td><td style="text-align:center;"> - - - </td><td style="text-align:center;"><a id="view_button" data-type="' + type + '" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a><a class="pointer" do="" title="Delete" onclick="delete_detail_row($(this));"><i class="fa fa-times"></i></a></td></tr>';
	if (updateMode == 0) {
		$($theNewRow).appendTo($("tbody." + type).last());
	} else if (updateMode == 1) {
		$(".updateMode").replaceWith($theNewRow);
		$(".updateMode").removeClass('updateMode');
	}
	clearPanelValues(type);
	calculateColumnTotals(type);
	$("div." + type + "_item_select button").focus();
};
var editThisRow = function (thisElm) {
	var thisRow = $(thisElm).parent('td').parent('tr');
	var type = $(thisElm).attr("data-type");
	$(".updateMode").removeClass('updateMode');
	thisRow.addClass('updateMode');
	var item_name = thisRow.find('td.' + type + '_item_name').text();
	var item_id = parseInt(thisRow.find('td.' + type + '_item_name').attr('data-item-id')) || 0;
	var quantity = thisRow.find('td.' + type + '_quantity').text();
	var issue_unit_cost = thisRow.find('td.' + type + '_unit_cost').text();
	var subAmount = thisRow.find('td.' + type + '_gross_cost').eq(3).text();

	$("select." + type + "_item_select option[value='" + item_id + "']").prop('selected', true);
	$("select." + type + "_item_select").selectpicker('refresh');
	$("input." + type + "_quantity").val(quantity);
	$("input." + type + "_unit_cost").val(issue_unit_cost);
	$("input." + type + "_gross_cost").val(subAmount);
	$("input." + type + "_stock_in_hand").val('').attr('stock_os', '');
	getItemStock($("." + type + "_item_select"));
	$("div." + type + "_item_select button").focus();
};

var saveProcess = function () {
	var wip_id = parseInt($(".wip_id").val()) || 0;
	var started_on_date = $("input[name='started_on']").val();
	var labour_account = $("select.labour_account option:selected").val();
	var labour = parseFloat($("input[name='labour']").val()) || 0;
	var overhead_account = $("select.overhead_account option:selected").val();
	var overhead = parseFloat($("input[name='overhead']").val()) || 0;
	var final_cost = parseFloat($("input[name=final_cost]").val()) || 0;

	if (labour_account != '' && labour == 0) {
		displayMessage("Error! External Expense amount is missing.");
		return;
	}
	if (overhead_account != '' && overhead == 0) {
		displayMessage("Error! Internal Expense amount is missing.");
		return;
	}
	var misc_expense = 0;

	if ($("tr[data-type=issue]").length == 0) {
		displayMessage('Error! No items to issue!');
		return false;
	}

	if ($("tr[data-type=produce]").length == 0) {
		displayMessage('Error! No items to produce!');
		return false;
	}

	var issue_rows = {};
	$("tr[data-type=issue]").each(function (index, element) {
		issue_rows[index] = {};
		issue_rows[index].row_id = $(this).attr('data-row-id');
		issue_rows[index].item_id = parseInt($(this).find('td.issue_item_name').attr('data-item-id')) || 0;
		issue_rows[index].quantity = parseFloat($(this).find('td.issue_quantity').text()) || 0;
		issue_rows[index].unit_cost = parseFloat($(this).find('td.issue_unit_cost').text()) || 0;
		issue_rows[index].gross_cost = parseFloat($(this).find('td.issue_gross_cost').text()) || 0;
	});
	issue_rows = JSON.stringify(issue_rows);

	var produce_rows = {};
	$("tr[data-type=produce]").each(function (index, element) {
		produce_rows[index] = {};
		produce_rows[index].row_id = $(this).attr('data-row-id');
		produce_rows[index].item_id = parseInt($(this).find('td.produce_item_name').attr('data-item-id')) || 0;
		produce_rows[index].quantity = parseFloat($(this).find('td.produce_quantity').text()) || 0;
		produce_rows[index].unit_cost = parseFloat($(this).find('td.produce_unit_cost').text()) || 0;
		produce_rows[index].gross_cost = parseFloat($(this).find('td.produce_gross_cost').text()) || 0;
	});
	produce_rows = JSON.stringify(produce_rows);

	var deleted_issue_rows = {};
	$("input.deleted_issue_rows").each(function (i, e) {
		var id = parseInt($(this).val()) || 0;
		deleted_issue_rows[i] = id;
	});
	deleted_issue_rows = JSON.stringify(deleted_issue_rows);

	var deleted_produce_rows = {};
	$("input.deleted_produce_rows").each(function (i, e) {
		var id = parseInt($(this).val()) || 0;
		deleted_produce_rows[i] = id;
	});
	deleted_produce_rows = JSON.stringify(deleted_produce_rows);

	$("button.saveProcess").prop('disabled', true);
	$.post("db/save_wip.php", {
		wip_id: wip_id,
		started_on_date: started_on_date,
		labour_account: labour_account,
		labour: labour,
		overhead_account: overhead_account,
		overhead: overhead,
		misc_expense: misc_expense,
		issue_rows: issue_rows,
		produce_rows: produce_rows,
		deleted_issue_rows: deleted_issue_rows,
		deleted_produce_rows: deleted_produce_rows,
		final_cost: final_cost
	}, function(data){
		$("button.saveProcess").prop('disabled', false);
		data = $.parseJSON(data);
		displayMessage(data['MSG']);
		if (data['ID'] > 0) {
			window.location.href = 'wip-details.php?wid=' + data['ID'];
		}
	});
};

var getItemStock = function ($itemSelector) {
	var item_id = $($itemSelector).val();
	var type = $($itemSelector).attr('data-type');
	if (item_id != '') {
		$.post('db/get-item-stock.php',{item_id:item_id},function(data){
			data = $.parseJSON(data);
			$("input." + type + "_stock_in_hand").val(data['QTY']);
			$("input." + type + "_stock_in_hand").attr('stock_os', data['QTY']);
			$("input." + type + "_unit_cost").val(data['PRICE']);
			$("input." + type + "_quantity").focus();
		});
	}
};
var loadWorkInProcess = function (wip_id) {
	if (wip_id > 0) {
		//do stuff
	}
};
