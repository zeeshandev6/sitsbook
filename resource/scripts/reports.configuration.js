$(document).ready(function(){
		$(".content-box-header h3").css({ "cursor":"default" });
		$(".closed-box .content-box-content").hide();
		$(".closed-box .content-box-tabs").hide();
    // Content box tabs:
		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
		$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
		$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
			function() {
				$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
				$(this).addClass('current'); // Add class "current" to clicked tab
				var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
				$(currentTab).siblings().hide(); // Hide all content divs
				$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
				return false;
			}
		);
    //Close button:
		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);
		$("input.datepicker").datepicker({
			format: 'dd-mm-yyyy',
			clearBtn:true,
			disableTouchKeyboard:true,
			autoclose:true
		}).on('changeDate',function(ev){
			$(this).datepicker('hide');
			$(this).trigger('change');
		});
    // Alternating table rows:

		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:

		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);

		$(".get_code_for_title").keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
		$(".input_readonly").css({"cursor":"default"});
		$(".input_readonly").focus(function(e){
			$(this).blur();
		});
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
		$.fn.sumColumn = function(showTotal){
			var totalThaans = 0;
			$(this).each(function() {
				var rowVal = $(this).text().replace(/\-/g,"");
				rowVal = (rowVal.replace(/\s+/g,'')=="")?0:rowVal.replace(/\s+/g,'');
				totalThaans += parseInt(rowVal);
				$(showTotal).text(totalThaans);
			});
		};
		$.fn.sumColumn2 = function(showTotal){
			var totalThaans = 0;
			$(this).each(function(){
				var rowVal = $(this).text();
				rowVal = (rowVal.replace(/\s+/g,'')=="")?0:rowVal.replace(/\s+/g,'');
				totalThaans += parseInt(rowVal);
				$(showTotal).text(totalThaans);
			});
		};
		/* Ledgers Reporting Function */
			$("button.printButton").click(function(){
				$("div#bodyTab").print();
				alert('google');
			});
			$("input.insertAccTitle").keydown(function(e){
				if(e.keyCode==16){
					$("div.listEye").accountList();
				}
			});
			$("div.listEye").click(function(){
				$("div.listEye").accountList();
			});
			$("input.insert_title").keyup(function(e){
				if(e.keyCode==16){
					$("div#fade").fadeOut('fast');
					$("div.popupList").fadeOut('fast');
				}
			});
			$.fn.accountList = function(){
				$("div.popupList").centerThisDiv();
				var offsetTop = $(this).offset().top;
				var offsetLeft = $(this).offset().left;
				offsetLeft -= $("div.popupList").width();
				offsetLeft += $(this).width();
				offsetLeft += 20;
				$("div.popupList").css({'top':offsetTop,'left':offsetLeft});
				//$("div#fade").fadeIn('fast');
				$("div.popupList").fadeIn('fast');
				$("div.popupList div.scrollableDiv").customScrollbar();
				$("div.popupList ul.ul-list li").click(function(){
					$("input.insertAccTitle").val($(this).attr('acc-title'));
					$("input.insertAccCode").val($(this).attr('acc-code'));
					$("div#fade").fadeOut('fast');
					$("div.popupList").fadeOut('fast');
					$("div.submitArrow").fadeIn();
					$("input.insertAccTitle").focus();
				});
				$("div.popupList p.popupHead .fa-times").click(function(){
					$("div#fade").fadeOut('fast');
					$("div.popupList").fadeOut('fast');
				});
			};
});
