//var $ = $.noConflict(true);
$(document).ready(function(){
		$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
			$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
		};
		$.fn.sumColumn = function(showTotal){
			var totalThaans = 0;
			$(this).each(function() {
				var rowVal = $(this).text().replace(/\-/g,"");
				rowVal = (rowVal.replace(/\s+/g,'')=="")?0:rowVal.replace(/\s+/g,'');
				totalThaans += parseInt(rowVal);
				$(showTotal).text(totalThaans);
			});
		};
		$("input[type='text']").attr("autocomplete","off");
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};
		$.fn.getUnitPriceOfItem = function(insertPriceToElement){
			var itemCode = $(this).val();
			$.post('db/get-unit-price.php',{itemCode:itemCode},function(data){
				data = $.parseJSON(data);
				var price = (data[0]==null)?0:data[0];
				$(insertPriceToElement).val(price);
			});
		};
		$(function(){
			$(window).keyup(function(e){
				if(e.keyCode==27){
					$("#popUpDel").slideUp();
					$("#xfade").fadeOut();
				}
			});
		});
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
		$.fn.createAdjustmentEntry = function(){
			$("#popUpDel").remove();
			var machineSalesId = $(this).attr('do');
			$.post('db/create-adjustment-entry.php',{machineSalesId:machineSalesId},function(data){
				if(data==""){
					window.location.href = 'machine-sales.php';
				}else{
					$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>"+data+"</p><a class='nodelete'>Close</a></div>");
					$("#popUpDel").centerThisDiv();
					$("#fade").fadeIn('slow');
					$("#popUpDel").fadeIn();
					$(".nodelete").click(function(){
						$("#fade").fadeOut();
						$("#popUpDel").fadeOut();
					});
					$(".close_popup").click(function(){
						$("#popUpDel").slideUp();
						$("#fade").fadeOut('fast');
					});
				}
			});
		};
});
