$(document).ready(function(){
    $(".bP,.temp,.doc_fee,.lab_fee,.diagnosis").keyup(function(e){
		if(e.keyCode ==13){
			$(this).saveThisFeild();
		}
	});
	checkUpAvailabilityCheck();
	$("input[name='token']").keyup(function(e){
		if(e.keyCode == 13){
			var tokenNumber = $(this).val();
			$(this).blur();
			$.post('db/getPatientIdByToken.php',{token:tokenNumber},function(data){
				data = $.parseJSON(data);
				if(data['ID'] != 0){
					$(this).attr("data-id",data['ID']);
					var $thisElement = $(this);
					getPatientsDetails($thisElement);
				}else{
					var prevNum = parseInt($(this).attr("data-id"))||0;
					if(prevNum != 0){
						var $thisElement = $(this);
						getPatientsDetails($thisElement);
					}else{
						displayMessage('Token Not Yet Issued!');
					}
				}
			});
		}
	});
	$(".testPrice").keydown(function(e){
		if(e.keyCode == 13){
			$(this).parent().parent().find(".test_desc").focus();
		}
	});
	$(".test_desc").keydown(function(e){
		if(e.keyCode == 13){
			$(this).parent().parent().find(".labQuickSave").focus();
		}
	});
	hideSideBarOnBlur();
	$(".bP,.temp,.doc_fee,.lab_fee,.diagnosis").blur(function(){
		$(this).saveThisFeild();
	});
	
	$(".labQuickSave").click(function(){
		saveLabTests();
	});
	$(".labQuickSave").keyup(function(e){
		if(e.keyCode == 13){
			saveLabTests();
		}
	});
	$.fn.saveThisFeild = function(){
		var patient_id = $(".patient_id").val();
		var data_type = $(this).attr('data-type');
		var thisField = $(this);
		var data = $(this).val();
		if(patient_id > 0){
			$.post('db/updateSinglePatientField.php',{patient_id:patient_id,data:data,data_type:data_type},function(data){
				data = $.parseJSON(data);
				if(data['OK'] != 'Y'){
					thisField.val(data['DEF']);
				}
			});
		}
	}
	
	$(".labTest").change(function(){
		calculateLabFee();
	});
	$("select.testSelector").change(function(){
		var test_price = $(this).find('option:selected').attr('data-price');
		$(".testPrice").val(test_price).focus();
	});
	$(".miniText").keyup(function(e){
		if(e.keyCode == 13){
			$(this).animate({'background-color':'rgba(0,255,0,0.6)'},200,function(){
				calculateLabFee();
				$(this).animate({'background-color':'#FFF'},200);
			});
		}
	});
	$(".miniText").blur(function(e){
		$(this).animate({'background-color':'rgba(0,255,0,0.6)'},200,function(){
			calculateLabFee();
			$(this).animate({'background-color':'#FFF'},200);
		});
	});
	$.fn.quickSave = function(){
		var row_id    = parseInt($(".editMode").find("#view_button").attr('data-id'))||0;
		var thisTable = $(this).parent().parent().parent().parent();
		var type      = $(this).attr('data-type');
		var medicineName = thisTable.find(".medicineSelector option:selected").text();
		var takenTime = '';
		thisTable.find(".dosage:checked").each(function(index, element) {
            var Dose = $(this).val();
			takenTime += (index > 0)?"|":"";
			takenTime += Dose;
        });
		if(thisTable.find(".duration").val() == '' || thisTable.find(".duration").val() == 0){
			var duration = 'Nill';
		}else{
			var duration = thisTable.find(".duration").val()+" Days ";
		}
		var quantity = parseInt(thisTable.find(".quantityFy").val()) || 0;
		var instructions = thisTable.find(".instructions").val();
		
		var patient_id = $(".patient_id").val();
		$(this).blur();
		
		if(medicineName != '' && patient_id > 0 && quantity > 0){
			$.post('db/savePrescription.php',{ row_id:row_id,
											   type:type,
											   patient_id:patient_id,
											   medicineName:medicineName,
											   takenTime:takenTime,
											   duration:duration,
											   quantity:quantity,
											   instructions:instructions},function(data){
				data = $.parseJSON(data);
				var doc_type_id = data['ID'];
				var tableTr = '';
				tableTr += '<tr class="calculations ajaxRow">';
				tableTr += '<td class="medicineTd" style="text-align:left;">'+medicineName+'</td>';
				tableTr += '<td class="dosageTd">'+takenTime+'</td>';
				tableTr += '<td class="durationTd">'+duration+'</td>';
				tableTr += '<td class="quantityTd">'+quantity+'</td>';
				tableTr += '<td class="istructionTd" style="text-align:"left;>'+instructions+'</td>';
				tableTr += '<td width="10%" style="text-align:center;" class="actionTd">';
				tableTr += '<button id="view_button" data-id="'+doc_type_id+'" data-type="'+type+'" onclick="editRow(this);" ><i class="fa fa-pencil"></i></button>';
				tableTr += '&nbsp;';
				tableTr += '<button class="pointer" data-id="'+doc_type_id+'" data-type="'+type+'" onclick="deleteRow(this);" ><i class="fa fa-times"></i></button>';
				tableTr += '</td>';
				tableTr += '</tr>';
				if(data['OK'] == 'Y'){
					if(row_id == 0){
						$(tableTr).insertAfter(thisTable.find(".calculations").last());
					}
					if(row_id > 0){
						$(".editMode").replaceWith(tableTr);
						$(".editMode").removeClass('editMode');
					}
					thisTable.find(".duration").val('');
					thisTable.find(".quantityFy").val('');
					thisTable.find(".instructions").val('');
					thisTable.find("select.medicineSelector").find("option").first().prop("selected",true);
					thisTable.find("select.medicineSelector").selectpicker("refresh");
					thisTable.find("div.medicineSelector button").focus();
				}
			});
		}
	}
	
	
	//Actions For user
	$(".quickSave").keydown(function(e){
		if(e.keyCode == 13){
			$(this).quickSave();
		}
	});
	$(".quickSave").click(function(){
		$(this).quickSave();
	});
	$(".printPage").click(function(){
		var patient_id = $(".patient_id").val();
		if(patient_id > 0){
			window.location.href = 'print-prescription.php?id='+patient_id;
		}
	});
});

$(window).load(function(){
	initializeEvents();
});

var deleteRow = function(thisElm){
	var type = $(thisElm).attr('data-type');
	var thisRow = $(thisElm).parent('td').parent('tr');
	var row_id = parseInt($(thisElm).attr('data-id')) || 0;
	var message = "Confirm Deletion?";
	if(row_id > 0){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$(".dodelete").hide();
			$.post('db/deletePrescrition.php',{id:row_id,type:type},function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					thisRow.fadeOut(function(){
						$(this).remove();
					});
					$("#popUpDel .confirm").text("Record Deleted Successfully!");
					$(".nodelete").text('Close');
				}else{
					$("#popUpDel .confirm").text("Error while Deleting Record!");
					$(".nodelete").text('Close');
				}
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	}
}
var initializeEvents = function(){
	$("div.medicineSelector").find(".dropdown-toggle").keydown(function(e){
		var $theValue = $(this).parent().parent().parent().find("select.medicineSelector option:selected").val();
		if(e.keyCode == 13 && $theValue != ''){
			$(this).parent().parent().parent().find("input.duration").focus();
		}
	});
	$("input.duration").keydown(function(e){
		if(e.keyCode == 13){
			calculateTheQty($(this));
			$(this).parent().parent().find("input.quantityFy").focus();
		}
	});
	
	$("input.quantityFy").keyup(function(e){
		if(e.keyCode == 13 && $(this).val() != '' && $(this).val() != 0){
			$(this).parent().parent().find("input.instructions").focus();
		}
	});
	
	$("input.instructions").keyup(function(e){
		if(e.keyCode == 13){
			$(this).parent().parent().find("input.quickSave").focus();
		}
	});
}
var closeSideBar = function(){
	$(".sideBar").removeClass("sideBarOpen");
	$(".sideBar").addClass("sideBarClose");
}
var openSideBar = function(){
	$(".sideBar").removeClass("sideBarOpen");
	$(".sideBar").addClass("sideBarClose");
}
var toggleSideBar = function(){
	if($(".sideBar").hasClass("sideBarOpen")){
		$(".sideBar").removeClass("sideBarOpen");
		$(".sideBar").addClass("sideBarClose");
	}else{
		$(".sideBar").removeClass("sideBarClose");
		$(".sideBar").addClass("sideBarOpen");
	}
	getPatientsList();
	countPatients();
}
var countPatients = function(){
	var patientsCount = $(".sideBar ul li").length;
	$(".sideBar .confirm .numPatients").text('').text("( "+patientsCount+" )");
}
var getPatientsList = function(){
	$.post('db/getPatientList.php',{li:true},function(data){
		$(".sideBar ul").html(data);
		countPatients();
	});
}

var getPatientsDetails = function(thisElem){
	$(".lightFade").fadeIn();
	idVal = parseInt($(thisElem).attr('data-id'));
	var doc_default_fee = $(".doc-default-fee").val();
	$('tr.ajaxRow').remove();
	
	$.post('db/getPrescriptions.php',{D:idVal},function(data){
		$(".disTable").append(data);
	});
	$.post('db/getPrescriptions.php',{M:idVal},function(data){
		$(".medTable").append(data);
	});
	$.post('db/getPrescriptions.php',{L:idVal},function(data){
		$(".labTable").html('');
		$(".labTable").append(data);
		var $labFee = 0;
		$(".theLabFeeTd").each(function(index, element) {
            $labFee += parseFloat($(this).text())||0;
        });
		$(".lab_fee").val($labFee);
	});
	$.post('db/getPatientDetails.php',{id:idVal},function(data){
		var patientDetails = $.parseJSON(data);
		$(".patient_id").val(idVal);
		$("input[name='token']").val(patientDetails['TOKEN_NO']);
		$("input[name='patientName']").val(patientDetails['NAME']);
		$("input[name='age']").val(patientDetails['AGE']);
		if(patientDetails['GENDER'] == 'M'){
			patientDetails['GENDER'] = 'Male';
		}else if(patientDetails['GENDER'] == 'F'){
			patientDetails['GENDER'] = 'Female';
		}else if(patientDetails['GENDER'] == 'C'){
			patientDetails['GENDER'] = 'Child';
		}
		$("input[name='gender']").val(patientDetails['GENDER']);
		var priority = 'Normal';
		if(patientDetails['PRIORITY'] == 'E'){
			priority = 'Emergency';
		}
		$("input[name='priority']").val(priority);
		$("input[name='mobile']").val(patientDetails['MOBILE']);
		$(".bP").val(patientDetails['BP']);
		$(".temp").val(patientDetails['TEMP']);
		
		if(patientDetails['DOC_FEE'] == 0){
			patientDetails['DOC_FEE'] = doc_default_fee;
		}
		$(".doc_fee").val(patientDetails['DOC_FEE']);
		$(".doc_fee").saveThisFeild();
		$(".diagnosis").val(patientDetails['DIAGNOSIS']);
		if(patientDetails['STATUS'] == 'C'){
			$("#checkUp").prop('checked',true);
		}else{
			$("#checkUp").prop('checked',false);
		}
		
		closeSideBar();
		$(".lightFade").hide();
		if(patientDetails['BP'] == ''){
			$(".bP").focus();
		}else{
			$("div.medicineSelector").first().find(".dropdown-toggle").focus();
		}
		calculateLabFee();
		checkUpAvailabilityCheck();
	});
}
var saveLabTests = function(){
	var row_id    = parseInt($(".editMode").find("#view_button").attr('data-id'))||0;
	var patient_id = $(".patient_id").val();
	var labTests = '';
	var test_id = $(".testSelector option:selected").val();
	var test_desc = $(".test_desc").val();
	var test_type = 'G';
	if(test_id == 'U'){
		test_id = 0;
		test_type = 'U';
	}
	var test_name = $(".testSelector option:selected").text();
	var testPrice = $(".testPrice").val();
	$(":focus").blur();
	if( patient_id > 0 ){
		$.post('db/recommendLabTest.php',{doc_lab_id:row_id,patient_id:patient_id,test_id:test_id,test_type:test_type,test_desc:test_desc,testPrice:testPrice},function(data){
			data = $.parseJSON(data);
			var $testRow = '';
			if(data['ID'] > 0){
				$testRow += '<tr>';
                	$testRow += '<td class="testNameTd">'+test_name+'</td>';
                    $testRow += '<td class="theLabFeeTd">'+testPrice+'</td>';
					$testRow += '<td class="theLabDescTd">'+test_desc+'</td>';
                    $testRow += '<td style="text-align:center;"><button id="view_button" data-id="'+data['ID']+'" data-type="L" onclick="editRow(this);" ><i class="fa fa-pencil"></i></button> ';
                    $testRow += ' <button class="pointer" data-id="'+data['ID']+'" data-type="L" onclick="deleteRow(this);" ><i class="fa fa-times"></i></button></td>';
                $testRow += '</tr>';
				$(".testPrice").val('');
				$(".test_desc").val('');
				$(".testSelector option").first().prop('selected',true);
				$(".testSelector").selectpicker("refresh");
				if(row_id == 0){
					$(".labTable").append($testRow);
				}else if(row_id > 0){
					$(".editMode").replaceWith($testRow);
					$(".editMode").removeClass('editMode');
				}
				$("tr[data-type='lab']").find("div.testSelector button").focus();
			}
		});
	}
}
var editRow = function(thisElm){
	$(".editMode").removeClass('editMode');
	var $thisRow = $(thisElm).parent().parent();
	var thisType = $(thisElm).attr('data-type');
	var row_id   = $(thisElm).attr('data-id');
	if(thisType != 'L'){
		var $thisTypeInput = $('.quickSave[data-type="'+thisType+'"]').parent('td').parent('tr');
	}else{
		var $thisTypeInput = $('.labQuickSave[data-type="'+thisType+'"]').parent('td').parent('tr');
	}
	//In Case Of Laboratory Scenario Is Different.
	if(thisType != 'L'){
		var medicineName = $thisRow.find(".medicineTd").text();
		var dosage       = $thisRow.find(".dosageTd").text();
		var duration     = parseInt($thisRow.find(".durationTd").text())||0;
		var quantity     = $thisRow.find(".quantityTd").text();
		var instructions = $thisRow.find(".istructionTd").text();
	}else{
		var test_name = $thisRow.find(".testNameTd").text();
		var test_fee  = $thisRow.find(".theLabFeeTd").text();
		var test_desc  = $thisRow.find(".theLabDescTd").text();
	}
	
	//In Case Of Laboratory Senario Is Different.
	if(thisType != 'L'){
		$thisTypeInput.find(".medicineSelector option").each(function(index, element) {
			if($(this).attr('data-name') == medicineName){
				$(this).prop('selected',true).siblings().prop('selected',false);
			}
		});
		$(".medicineSelector").selectpicker("refresh");
		
		dosage = dosage.split('|');
		$thisTypeInput.find(".dosage").each(function(index, element) {
			var currentElem    = $(this);
			var currentElemVal = $(this).val();
			var found = $.inArray(currentElemVal,dosage);
			if(found !== -1){
				currentElem.prop('checked',true);
			}else{
				currentElem.prop('checked',false);
			}
		});
		$thisTypeInput.find(".duration").val(duration);
		$thisTypeInput.find(".quantityFy").val(quantity);
		$thisTypeInput.find(".instructions").val(instructions);
	}else{
		$thisTypeInput.find(".testSelector option").each(function(index, element) {
            if($(this).text() == test_name){
				$(this).prop('selected',true).siblings().prop('selected',false);
			}
        });
		$(".testSelector").selectpicker('refresh');
		$thisTypeInput.find("input.testPrice").val(test_fee);
		$thisTypeInput.find("input.test_desc").val(test_desc);
	}
	$thisRow.addClass('editMode');
}

var calculateTheQty = function(thisElement){
	var dura = parseInt($(thisElement).val()) || 0;
	var timesTaken = $(thisElement).parent().parent().find(".dosage:checked").length;
	var totalQty = dura * timesTaken;
	$(thisElement).parent().parent().find("input.quantityFy").val(totalQty);
}

var calculateLabFee = function(){
	var totalLabFee = 0;
	$(".labTest:checked").each(function(index, element) {
        var testPrice = parseInt($(this).parent().find('.miniText').val())||0;
		totalLabFee += testPrice;
    });
	$(".lab_fee").val(totalLabFee);
}
var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	}
var confirmCheck = function(){
	var patient_id = $(".patient_id").val();
	var status = 'N';
	if(patient_id > 0 && $("#checkUp").is(":checked")){
		status = 'C';
	}else{
		status = 'W';
	}
	$.post('db/patientGo.php',{patient_id:patient_id,status:status},function(data){
		data = $.parseJSON(data);
		if(data['OK'] == 'N' && status == 'C'){
			$("#checkUp").prop('checked',false);
		}
		if(data['OK'] == 'N' && status == 'W'){
			$("#checkUp").prop('checked',true);
		}
	});
}
var checkUpAvailabilityCheck = function(){
	var patient_id =  $(".patient_id").val();
	if(patient_id > 0){
		$("label.checkUpLabel,.printPage").show();
	}else{
		$("label.checkUpLabel,.printPage").hide();
	}
}
var formCleanUp = function(){
	//remove lab tests
	$(".labTable").html('');
	//remove  dispensory , Medical Store Rows
	$('tr.ajaxRow').remove();
	//remove patient Details
	$(".patient_id").val(0);
	$("input[name='token']").val('');
	$("input[name='patientName']").val('');
	$("input[name='age']").val('');
	$("input[name='gender']").val('');
	$("input[name='priority']").val('');
	$("input[name='mobile']").val('');
	$(".bP").val('');
	$(".temp").val('');
	$(".diagnosis").val('');
	checkUpAvailabilityCheck();
}



//side bar hide function
var hideSideBarOnBlur=function(){$(window).click(function(e){var container = $(".sideBar");if(!container.is(e.target) && container.has(e.target).length === 0){if(container.hasClass("sideBarOpen")){container.removeClass("sideBarOpen");container.addClass("sideBarClose");}}});}