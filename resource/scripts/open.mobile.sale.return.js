$(document).ready(function() {
    $(".h3_php_error").click(function(){
	$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});
	$(window).keyup(function(e){
		if(e.keyCode == 27){
			$(this).clearPanelValues();
			$("div.itemSelector button").focus();
			if($(".updateMode").length){
				$(".updateMode").removeClass('updateMode');
			}
		}
	});
	$("#main-nav li ul").hide();
	$("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	$("#main-nav li a.nav-top-item").click(
		function () {
			$(this).parent().siblings().find("ul").slideUp("normal");
			$(this).next().slideToggle("normal");
		}
	);
	var fileSelf = $(".this_file_name").val();
		$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
		$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");
	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);
	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	/*$(".content-box-header h3").click(
		function () {
		  $(this).parent().next().toggle();
		  $(this).parent().parent().toggleClass("closed-box");
		  $(this).parent().find(".content-box-tabs").toggle();
		}
	);*/

	// Content box tabs:

	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseInt($(this).text());
        });
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseFloat($(this).text())||0;
        });
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text(sumAll);
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$.post(file, {id : idValue}, function(data){
			var supplierName = currentRow.children("td").first().next().next().text();
			if(data==1){
				$("#popUpDel .confirm").text(" "+supplierName+"Contains Information! Do You Really Want To Delete "+supplierName+"?.");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						if(data == 'L'){
							$("#popUpDel .confirm").text(" Stock is Issued To Sheds Cannot Delete This Record! ");
							$(".dodelete").hide();
							$(".nodelete").text('Close');
							$(".nodelete").click(function(){
								$("#popUpDel").slideUp();
								$("#xfade").fadeOut();
							});
						}else{
							currentRow.slideUp();
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut();
						}
					});
				});
			}else{
				$("#popUpDel .confirm").text("Are Sure you Want To Delete "+supplierName+"?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						currentRow.slideUp();
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				});
			}
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
		});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.deletePanel = function(file){
		var idValue = $(this).attr("do");
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		$("#popUpDel").centerThisDiv();
		$("#popUpDel .confirm").text("Are you Sure You Want to Delete?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {msid : idValue}, function(data){
				if(data == 'L'){
					$("#popUpDel .confirm").text(" Not Enough Stock! ");
					$(".dodelete").hide();
					$(".nodelete").text('Close');
					$(".nodelete").click(function(){
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				}else{
					$("#panel-"+idValue).slideUp(function(){
						$(this).remove();
					});
					$("#popUpDel").slideUp();
					$("#xfade").fadeOut();
				}
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.quickSave = function(){
		var barcode = $("#scanner-field").val();
		var $thisInputBarcode = $(this);
		if(barcode==''){
			$("#scanner-field").focus();
			return false;
		}
		$("#scanner-field").prop('readonly',true).blur();
		$.get('db/scan-item.php',{msrcode:barcode},function(data){
			if(data == ''){
				displayTooltipMessage($thisInputBarcode,"BARCODE Not Found!","red-tooltip");
				$("#scanner-field").prop('readonly',false).focus();
				return;
			}
			data = $.parseJSON(data);
			if(data['STOCK_STATUS'] == 'A'){
				displayTooltipMessage($thisInputBarcode,data['ITEM_NAME']+" ["+data['BARCODE']+"] already available in stock!","red-tooltip");
				$("#scanner-field").prop('readonly',false).focus();
				return;
			}
			if(data['STOCK_STATUS'] == 'R'){
				displayTooltipMessage($thisInputBarcode,data['ITEM_NAME']+" ["+data['BARCODE']+"] already returned &amp; available in stock!","red-tooltip");
				$("#scanner-field").prop('readonly',false).focus();
				return;
			}
			if($("[data-return-barcode="+barcode+"]").length){
				$("#scanner-field").animate({'backgroundColor':"rgba(255,0,0,0.2)"},400,function(){
					$(this).animate({'backgroundColor':"rgba(255,255,255,0.6)"},400);
				});
				$("[data-return-barcode="+barcode+"] td").animate({'backgroundColor':"rgba(255,0,0,0.2)"},400,function(){
					$(this).animate({'backgroundColor':"rgba(255,255,255,0.2)"},400);
				});
				$("#scanner-field").val('');
				$("#scanner-field").prop('readonly',false).focus();
				return false;
			}
			$("#scanner-field").blur();
			$("#scanner-field").val('');
			var newRow  = '<tr class="new-row" data-msdid="'+data['SALE_INFO']['MSDID']+'" data-ssid="'+data['SS_ID']+'" data-return-barcode="'+barcode+'">';
				newRow += '<td style="text-align:center;" class="invoice-no">'+data['SALE_INFO']['BILL_NO']+'</td>';
				newRow += '<td style="text-align:center;" class="sale-date">'+data['SALE_INFO']['SALE_DATE']+'</td>';
			    newRow += '<td style="text-align:center;" class="return-barcode">'+barcode+'</td>';
				newRow += '<td style="text-align:center;" class="return-item" data-item="'+data['ITEM_ID']+'">'+data['ITEM_NAME']+'</td>';
				newRow += '<td style="text-align:center;" class="return-price"><input class="form-control text-right" value="'+data['SALE_INFO']['SALE_PRICE']+'"/></td>';
				newRow += '<td style="text-align:center;"><button class="pointer" onclick="removeRow(this);"><i class="fa fa-times"></i></button></td>';
				newRow += '</tr>';
			$(".tbody-for-rows").append(newRow);
			$("#scanner-field").prop('readonly',false).focus();
		});
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
	$.fn.calculateRowTotal = function(){
		var taxRate = $("select.taxRate option:selected").val();
		var taxType   = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount = Math.round((thisVal*thatVal)*100)/100;
		var discountAvail = parseFloat($("input.discount").val())||0;

		taxAmount = Math.round(taxAmount*100)/100;
		amount = Math.round(amount*100)/100;
		discountAvail = Math.round(discountAvail*100)/100;
		discountPerCentage = amount*(discountAvail/100);
		discountPerCentage = Math.round(discountPerCentage*100)/100;
		amount -= discountPerCentage;
		amount = Math.round(amount*100)/100;

		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount*(taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		-= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
            stockOfBill += parseInt($(this).text())||0;
        });
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$("select.itemSelector option").prop('selected',false);
		$("select.itemSelector").selectpicker('refresh');
		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.subAmount").val('');
		$("select.taxRate option").prop('selected',false);
		$("select.taxRate").selectpicker('refresh');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	= thisRow.find('td').eq(0).text();
		var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
		var quantity 	= thisRow.find('td').eq(1).text();
		var unitPrice 	= thisRow.find('td').eq(2).text();
		var discount 	= thisRow.find('td').eq(3).text();
		var subAmount 	= thisRow.find('td').eq(4).text();
		var taxRate 	= parseFloat(thisRow.find('td').eq(5).text())||0;
		var taxAmount   = thisRow.find('td').eq(6).text();
		var totalAmount = thisRow.find('td').eq(7).text();
		if(item_id == 0){
			return false;
		}
		$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
			data = $.parseJSON(data);
			var itemStock = data['STOCK'];
			$("input.inStock").attr('thestock',itemStock).val(itemStock);

			$(".itemSelector option[value='"+item_id+"']").prop('selected',true);
			$(".itemSelector").selectpicker('refresh');
			$("input.quantity").val(itemStock);
			$("input.quantity").animate({'background-color':'rgba(102,255,102,0.7)'},300,function(){
				$("input.quantity").animate({'background-color':'#FFF'},300).keyup();
			});
			$("input.unitPrice").val(unitPrice);
			$("input.discount").val(discount);
			$("input.subAmount").val(subAmount);
			if(taxRate == 0){
				$("select.taxRate option").prop('selected',false);
				$("select.taxRate").selectpicker('refresh');
			}else{
				$("select.taxRate option").each(function(index, element){
	                if($(this).val() == taxRate){
						$(this).prop('selected',true);
					}else{
						$(this).prop('selected',false);
					}
	            });
				$("select.taxRate").selectpicker('refresh');
			}
			$("input.taxAmount").val(taxAmount);
			$("input.totalAmount").val(totalAmount);
			stockOs();
			$("input.quantity").focus();
		});
	};
	var stockOs = function(){
		var thisQty = parseInt($("input.quantity").val())||0;
		var inStock = parseInt($("input.inStock").attr('thestock'))||0;
		var limit   = parseInt($("input.qty_limit").val())||0;
		var item_id = parseInt($("select.itemSelector option:selected").val());
		var item_title = $("select.itemSelector option:selected").text();

		$("td[data-item-id='"+item_id+"']").each();

		if(limit == 0){
			$("input.quantity").val(limit);
			$("input.inStock").val(inStock);
			return false;
		}
		if(limit < thisQty){
			$(".wel_made tr td[data-item='"+item_id+"']").next("td").animate({'background-color':'rgba(255,0,0,0.6)'},200,function(){
				$(".wel_made tr td[data-item='"+item_id+"']").next("td").animate({'background-color':'rgba(255,255,255,0.6)'},200);
			});
			$("input.quantity").val(limit);
			var NewStock =  inStock - limit;
			$("input.inStock").val(NewStock);
			return false;
		}
		var NewStock =  inStock - thisQty;
		if(NewStock < 0){
			$("input.quantity").val(0);
		}else{
			$("input.inStock").val(NewStock);
		}
	};
	var que = 0; //purchase_id_array_next_index  // MOIBLE SALE PRIMARY KEY ID
	var saveSale = function(){
		var purchase_id_array = getPurchaseIdsArray();
		var purchase_id		  = purchase_id_array[que];
		if(!purchase_id){
			//window.location.href = "open-mobile-sale-return.php";
			alert("END");
			return;
		}

		var return_date 	   = $("input[name=rDate]").val();
		var ms_id 			   = parseInt($("input.ms_id").val())||0;
		var sale_invoice_link  = (ms_id>0)?"Y":"N";
		var supplier		   = $("input.this_supplier").val();
		var bill_number		   = $("input.this_bill_num").val();
		if($("tr.new-row").length == 0){
			displayMessage('No Transactions Made!');
			return false;
		}
		$(".saveSale").hide();
		var jSonString = {};

		$("tr.new-row[data-ssid='"+purchase_id+"']").each(function(index, element){
			jSonString[index] = {};
			jSonString[index].msdid    = parseInt($(this).attr("data-msdid"))||0;
			jSonString[index].itemId  = parseInt($(this).find(".return-item").attr("data-item"))||0;
			jSonString[index].barcode = $(this).attr("data-return-barcode");
			jSonString[index].price   = parseFloat($(this).find(".return-price").find("input").val())||0;
        });
		jSonString = JSON.stringify(jSonString);
		$.post("db/saveScanSaleReturn.php",{purchase_id:purchase_id,
											    return_date:return_date,
											    bill_number:bill_number,
												ms_id:ms_id,
												sale_invoice_link:sale_invoice_link,
											    jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					que += 1;
					$("tr.new-row[data-ssid='"+purchase_id+"'] td.return-item").each(function(i,e){
						$(this).text($(this).text()+"[ RETURNED ]")
					});
					$("tr.new-row[data-ssid='"+purchase_id+"'] td.return-item").animate({"color":"blue"},500);
					if((parseInt(purchase_id_array[que])||0) == 0){
						displayMessage("Sale Returns Saved Successfully!");
						return;
					}else{
						saveSale();
					}
				}else{
					displayMessage(data['MSG']);
				}
		});
	};
	var displayTooltipMessage = function(elm,message,type,duration){
		if(!duration){
			var duration = 4000;
		}
		if(!type){
			type = "red-tooltip";
		}
		$(elm).attr({
			"data-toggle":"tooltip",
			"data-placement":"top",
			"title":message
		});
		$(elm).addClass(type);
		$(".tooltip-inner").css({"max-width":"300px","width":"300px"});

		$('[data-toggle="tooltip"]').tooltip("show");
		setTimeout(function(){
			$('[data-toggle="tooltip"]').tooltip("destroy");
			$(elm).attr({
				"data-toggle":"",
				"data-placement":"",
				"title":""
			});
			$(elm).removeClass(type);
		},duration);
	};
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};

	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
		});
	};
	var getBillsBySupplierCode = function(){
		var supplierCode = $("select.supplierSelector option:selected").val();
		if(supplierCode == ''){
			$("div.supplierSelector button").focus();
			return false;
		}
		$.post('db/getBillListBySupplerCode.php',{supplierCode:supplierCode},function(data){
			if(data != ''){
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").append(data);
				$("select.billSelector").selectpicker('refresh');
				$("div.billSelector button").focus();
			}else{
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").selectpicker('refresh');
				$("div.supplierSelector button").focus();
			}
		});
	};
	var display_alert = function(message){
		$(".message_div").text(message);
		$(".message_div").fadeIn(1000).delay(1000).fadeOut(1000);
	};
	var removeRow = function(thisElement){
		var theRow = $(thisElement).parent().parent();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel' style='display:none;'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#xfade").fadeIn();
		$("#popUpDel").fadeIn();
		$("#popUpDel").centerThisDiv();
		$(".dodelete").click(function(){
			$(theRow).remove();
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
		});
	};
	var goto_barcode = function(thisElm){
		var barcode  = $(thisElm).val();
		$.get('mobile-purchase-return-details.php',{check_barcode_for_return:barcode,type:'sale'},function(data){
			data = parseInt(data)||0;
			if(data > 0){
				window.location.href = 'mobile-sale-return-details.php?id='+data+"&bc="+barcode;
			}
		});
	}
	var showScannerPopUp = function(){
		return;
		$("body").append("<div id='popUpDel' style='display:none;'><p class='confirm'> Scan item barcode to return</p> <input class='form-control' onchange='goto_barcode(this);' id='barcoder' /> <a class='nodelete btn btn-info pull-right'>Cancel</a></div>");
		$("#xfade").fadeIn();
		$("#popUpDel").fadeIn();
		$("#popUpDel").centerThisDiv();
		setTimeout(function(){
			$("#barcoder").focus();
		},300);
		$(".nodelete,#xfade").click(function(){
			window.location.href = "mobile-sale.php";
		});
	}
	var getPurchaseIdsArray = function(){
		var aray = [];
		$("tr[data-ssid]").each(function(i,e){
			if($.inArray($(this).attr("data-ssid"), aray) == -1){
				aray.push($(this).attr("data-ssid"));
			}
		});
		return aray;
	}
