var quick_save = function(){
  var row_id=0,product_id,product_name,quality,color,thaan,measure_id,length,total_length;

  product_id    = parseFloat($("select.product_id option:selected").val())||0;
  product_name  = $("select.product_id option:selected").text();
  quality       = $("input.quality").val();
  color         = $("input.color").val()||0;
  thaan         = parseFloat($("input.thaan").val())||0;
  measure_id    = parseFloat($("select.measure_id option:selected").val())||0;
  measure_name  = $("select.measure_id option:selected").text();
  length        = parseFloat($("input.length").val())||0;
  total_length  = parseFloat($("input.total_length").val())||0;

  if(product_id == 0){
    displayMessage('Error! Product not selected.');
    return;
  }

  if(total_length == 0){
    displayMessage('Error! Length must be > 0.');
    return;
  }

  if($("tr.updateMode").length){
    row_id = parseInt($("tr.updateMode").attr('data-row-id'))||0;
  }

  var new_row;
  new_row += '<tr class="transcation" data-row-id="'+row_id+'">';
  new_row += '<td class="text-left 	  product_id" data-id="'+product_id+'">'+product_name+'</td>';
  new_row += '<td class="text-center  quality">'+quality+'</td>';
  new_row += '<td class="text-center  color">'+color+'</td>';
  new_row += '<td class="text-center  thaan">'+thaan+'</td>';
  new_row += '<td class="text-center  measure_id" data-id="'+measure_id+'">'+measure_name+'</td>';
  new_row += '<td class="text-center  length">'+length+'</td>';
  new_row += '<td class="text-center  total_length">'+total_length+'</td>';
  new_row += '<td class="text-center">';
  new_row += '<a type="button"   id="view_button" onclick="edit_row(this);"><i class="fa fa-pencil"></i></a>';
  new_row += '<a class="pointer" do="" onclick="delete_row(this);"><i class="fa fa-times"></i></a>';
  new_row += '</td>';
  new_row += '</tr>';

  if($("tr.updateMode").length){
    $("tr.updateMode").replaceWith(new_row);
  }else{
    $("tbody.transactions").append(new_row);
  }
  clearPanelValues();
  calculateColumnTotals();
  $("div.product_id button").focus();
};
var edit_row = function(elm){
  var row_id,product_id,product_name,quality,color,thaan,measure_id,length,total_length;
  var thisRow   = $(elm).parent().parent();
  $("tr.updateMode").removeClass('updateMode');
  $(thisRow).addClass("updateMode");
  product_id    = parseInt($(thisRow).find('td.product_id').attr('data-id'))||0;
  quality       = $(thisRow).find('td.quality').text()
  color         = $(thisRow).find('td.color').text()
  thaan         = parseFloat($(thisRow).find('td.thaan').text())||0;
  measure_id    = parseInt($(thisRow).find('td.measure_id').attr('data-id'))||0;
  length        = parseFloat($(thisRow).find('td.length').text())||0;
  total_length  = parseFloat($(thisRow).find('td.total_length').text())||0;

  $("select.product_id").selectpicker('val',product_id);
  $("input.quality").val(quality);
  $("input.color").val(color);
  $("input.thaan").val(thaan);
  $("select.measure_id").selectpicker('val',measure_id);
  $("input.length").val(length);
  $("input.total_length").val(total_length);

  $("div.product_id button").focus();
};
var delete_row   = function(rowChildElement){
  var row_id    = $(rowChildElement).parent().parent().attr("data-row-id");
  var clickedDel = $(rowChildElement);
  if($(rowChildElement).parent().parent().hasClass('updateMode')){
    displayMessage('Record is in edit Mode!');
    return false;
  }
  $("#fade").hide();
  $("#popUpDel").remove();
  $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
  $("#popUpDel").hide();
  $("#popUpDel").centerThisDiv();
  $("#fade").fadeIn('slow');
  $("#popUpDel").fadeIn();
  $(".dodelete").click(function(){
    $("#fade").fadeOut();
    $("#popUpDel").fadeOut(function(){
      $("#popUpDel").remove();
    });
    clickedDel.parent().parent().remove();
    if(row_id>0){
      $("div#form").first().append('<input type="hidden" class="deleted_rows" value="'+row_id+'" />');
    }
    calculateColumnTotals();
  });
  $(".nodelete").click(function(){
    $("#fade").fadeOut();
    $("#popUpDel").fadeOut(function(){
      $("#popUpDel").remove();
    });
  });
  $(".close_popup").click(function(){
    $("#popUpDel").fadeOut(function(){
      $("#popUpDel").remove();
    });
    $("#fade").fadeOut('fast');
  });
};
var inputRowCalculation = function(){
  var thaan  = parseFloat($("input.thaan").val())||0;
  var length = parseFloat($("input.length").val())||0;
  $("input.total_length").val(thaan*length);
};
var calculateColumnTotals = function(){
  $(this).sumColumnFloat("td.thaan","td.total_thaan");
  $(this).sumColumnFloat("td.total_length","td.length_total");
};
var clearPanelValues = function(){
  $("select.product_id").selectpicker('val','');
  $("input.quality").val('');
  //$("input.color").val('');
  $("input.thaan").val('');
  //$("select.measure_id").selectpicker('val','');
  //$("input.length").val('');
  $("input.total_length").val('');
};
var save_inward = function(){
  $("input.save_inward").prop("disabled",true);
  var inward_id    = parseInt($("input.inward_id").val())||0;
  var inward_date  = $("input[name=inward_date]").val();
  var lot_num      = $("input[name=lot_num]").val();
  var gate_pass_no = $("input[name=gate_pass_no]").val();
  var account_code = $("select[name=account_code] option:selected").val();

  if(account_code == ''){
    displayMessage('Please select an Account!');
    $("input.save_inward").prop("disabled",false);
    return false;
  }

  if(lot_num == ''){
    displayMessage('Error! Please provide a lot number.');
    $("input.save_inward").prop("disabled",false);
    return false;
  }

  if($("tbody.transactions tr.transcation").length==0){
    displayMessage('Please make at least  1 transaction!');
    $("input.save_inward").prop("disabled",false);
    return false;
  }
  var post_data    = {};
  var json_string  = {};
  var deleted_rows = {};
  $("tbody.transactions tr.transcation").each(function(index,element){
    json_string[index]                = {};
    json_string[index].row_id         = parseInt($(this).attr("data-row-id"))||0;
    json_string[index].product_id     = parseInt($(this).find("td.product_id").attr("data-id"))||0;
    json_string[index].quality        = $(this).find("td.quality").text();
    json_string[index].colour         = $(this).find("td.color").text();
    json_string[index].thaan          = parseFloat($(this).find("td.thaan").text())||0;
    json_string[index].measure_id     = parseInt($(this).find("td.measure_id").attr("data-id"))||0;
    json_string[index].len            = parseFloat($(this).find("td.length").text())||0;
    json_string[index].total_length   = parseFloat($(this).find("td.total_length").text())||0;
  });
  json_string = JSON.stringify(json_string);
  $("input.deleted_rows").each(function(i,e){
     deleted_rows[i] = parseInt($(this).val())||0;
  });
  deleted_rows  = JSON.stringify(deleted_rows);

  post_data['inward_id']    = inward_id;
  post_data['json_data']    = json_string;
  post_data['inward_date']  = inward_date;
  post_data['deleted_rows'] = deleted_rows;
  post_data['lot_num']      = lot_num;
  post_data['gate_pass_no'] = gate_pass_no;
  post_data['account_code'] = account_code;

  $.post('db/saveEmbInward.php',post_data,function(data){
    var id = parseInt(data)||0;
    window.location.href = 'emb-inward-details.php?id='+id;
    $("input.save_inward").prop("disabled",false);
  });
};
$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
  var sumAll = 0;
  $(sumOfFeild).each(function(index, element) {
    sumAll += parseFloat($(this).text())||0;
  });
  sumAll = Math.round(sumAll*100)/100;
  $(insertToFeild).text((sumAll).toFixed(2));
};
