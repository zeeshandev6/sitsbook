//var $ = $.noConflict(true);
$(document).ready(function(){
		$(".h3_php_error").click(function(){
			$(".h3_php_error").fadeOut('slow');
		});
		var fileSelf = $(".this_file_name").val();
		$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
		$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");

		$("#main-nav li ul").hide();
		$("#main-nav li a.current").parent().find("ul").show();
		$("#main-nav li a.nav-top-item").click(
			function () {
				$(this).parent().siblings().find("ul").slideUp("normal");
				$(this).next().slideToggle("normal");
			}
		);
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': '100px',
				'left': win_width
			});
		};
		$(".content-box-header h3").css({ "cursor":"default" });
		$(".closed-box .content-box-content").hide();
		$(".closed-box .content-box-tabs").hide();

		/*$(".content-box-header h3").click(
			function () {
			  $(this).parent().next().toggle();
			  $(this).parent().parent().toggleClass("closed-box");
			  $(this).parent().find(".content-box-tabs").toggle();
			}
		);*/

    // Content box tabs:

		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
		$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

		$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
			function() {
				$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
				$(this).addClass('current'); // Add class "current" to clicked tab
				var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
				$(currentTab).siblings().hide(); // Hide all content divs
				$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
				return false;
			}
		);

    //Close button:

		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);

    // Alternating table rows:

		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:

		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.numericInputOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 120 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.numericOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 120 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.stDigits = function(){
			return this.each(function(){
				if($(this).is(":input")){
					$(this).val( $(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
				}else{
					$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
				}
			});
		};
});
var event_status = function(id,status){
	$.post('calendar.php',{id:id,status:status},function(){
		$("div[data-remove='"+id+"']").hide('clip');
	});
}
var promtxClose = function(){
	$("#fade").fadeOut();
	$("#popUpDel").fadeOut(function(){
		$("#popUpDel").remove()
	});
}
var promptx = function(fn){
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").hide();
	$("#popUpDel").centerThisDiv();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").on('click',fn);
	$(".dodelete").on("mouseup",promtxClose);
	$(".nodelete,.close_popup").on("click",promtxClose);
}
