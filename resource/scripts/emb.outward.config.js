//var $ = $.noConflict(true);
$(document).ready(function(){
    // Alternating table rows:

		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:

		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.numericOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 120 || e.keyCode == 110 || e.keyCode == 190
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.quickEditRow 	 = function(){
			var id 						 = parseInt($(this).attr("do"))||0;
			$("tr.updateMode").removeClass('updateMode bg-warning');
			$(this).parent().parent().addClass('updateMode bg-warning');
			var product_id 		 = parseInt($(this).parent().siblings("td.product_id").attr("data-product-id"))||0;
			var lot_no         = $(this).parent().siblings("td.lot_no").text();
			var quality        = $(this).parent().siblings("td.quality").text();
			var measure_id     = $(this).parent().siblings("td.measure_id").attr('data-measure-id');
			var billing_type   = $(this).parent().siblings("td.billing_type").attr('data-billing-type');
			var stitches       = $(this).parent().siblings("td.stitches").text();
			var total_laces    = $(this).parent().siblings("td.total_laces").text();
			var measure_length = $(this).parent().siblings("td.measure_length").text();
			var emb_rate       = $(this).parent().siblings("td.emb_rate").text();
			var emb_amount     = $(this).parent().siblings("td.emb_amount").text();
			var stitch_rate    = $(this).parent().siblings("td.stitch_rate").text();
			var stitch_amount  = $(this).parent().siblings("td.stitch_amount").text();
			var stitch_acc     = $(this).parent().siblings("td.stitch_acc").attr('data-stitch-id');
			var design_code    = $(this).parent().siblings("td.design_code").text();
			var machine_id     = $(this).parent().siblings("td.machine_id").text();

			$("select.product_id").selectpicker('val',product_id);
			$("input.lotNum").val(lot_no);
			$("select.qualityDropDown").selectpicker('val',quality);
			$("select.measure").selectpicker('val',measure_id);
			$("select.billing_type").selectpicker('val',billing_type);
			$("input.stitches").val(stitches);
			$("input.total_laces").val(total_laces);
			$("input.length").val(measure_length);
			$("input.embRate").val(emb_rate);
			$("input.embAmount").val(emb_amount);
			$("input.stitchRate").val(stitch_rate);
			$("input.stitchAmount").val(stitch_amount);
			$("select[name=stitchAccount]").selectpicker('val',stitch_acc);
			$("input.designNum").val(design_code);
			$("select.machine_id").selectpicker('val',machine_id);
			$("input.lotNum").trigger('change');
			setTimeout(function(){
				$("div.product_id button").focus();
			},300);
		};
		$(".lotNum,.length,.embRate,.stitchRate,.machineNum").numericOnly();
				$(".insert_prod_title").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_quality']").focus();
					}
				});
				$("input[name='prod_quality']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_color']").focus();
					}
				});
				$("input[name='prod_color']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_than']").focus();
					}
				});
				$("input[name='prod_than']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='measure']").focus();
					}else{
						var lent = $(this).val();
						var thaanNum = $(".prod_len").val();
						$(".total_ammount").text(lent*thaanNum);
					}
				});
				$(".get_code_for_title").blur(function(){
					var codeCode = $(this).val();
					if(codeCode==""){
						$(".insert_title").val("");
					}
				});
				$(".prod_len").keyup(function(){
					var lent = $(this).val();
					var thaanNum = $(".prod_than").val();
					$(".total_ammount").text(lent*thaanNum);
				});

			$("input[type='text']").attr("autocomplete","off");
			$(".getCustomerTitle").keyup(function(){
				var codeCode = $(this).val();
					$.post('db/get-customer-title.php',{title:codeCode},function(data){
						var gotCode = $.parseJSON(data);
						var titles = [];
						$.each(gotCode,function(index,value){
							$.each(value,function(i,v){
								if(i=='CUST_ACC_TITLE'){
									titles.push(v);
								}
							});
						});
						$(".getCustomerTitle").autocomplete({
								source: titles,
								width: 300,
								scrollHeight:220,
								matchContains: true,
								selectFirst: false,
								minLength: 1
						});
						$.each(titles,function(i,v){
							if(codeCode==v){
								$(".insertCustomerCode").val(gotCode[0]['CUST_ACC_CODE']);
							}else{
								$(".insertCustomerCode").val("");
							}
						});
					});
			});
			$.fn.getProductQualities = function(){
				var lotNum 			 = $(this).val();
				var outward_date = $("input.outwards_date").val();
				var product_id 	 = $("select.product_id  option:selected").val();
				var customerCode = $("select.custCodeSelector option:selected").val();
				$.post('db/get-product-qualities.php',{
					getDetail:true,
					outward_date:outward_date,
					lotNum:lotNum,
					product_id:product_id,
					customerCode:customerCode
					},
					function(data){
						var details = $.parseJSON(data);
						$("select.qualityDropDown").find("option").remove();
						$(".thaanLengthOS").attr("inwardOS",details['T_TOTAL']).val(details['T_TOTAL']);
						$.each(details['QUALITY_LIST'],function(index,quality_name) {
							$("select.qualityDropDown").append("<option value='"+quality_name+"'>"+quality_name+"</option>");
						});
						$("select.qualityDropDown").selectpicker("refresh");
						$("div.qualityDropDown button").focus();
					});
			};
			$.fn.getProductQualitiesForUpdateForm = function(){
				var lotNum 				= $(this).val();
				var outward_date 	= $("input.outwards_date").val();
				var prodCode 			= $("#popUpForm select[name='productCode'] option:selected").val();
				var customerCode 	= $("select.custCodeSelector option:selected").val();
				var quality_saved = $("select[name=quality] option:selected").val();
				var make_selected = '';
				$.post('db/get-product-qualities.php',{
					getDetailsForUpdate:true,
					outward_date:outward_date,
					lotNum:lotNum,
					prodCode:prodCode,
					customerCode:customerCode
					},
					function(data){
						$("#popUpForm .quality").find("option").remove();
						var details = $.parseJSON(data);
						$("#popUpForm .stockInHand").attr("inwardOS",details[0]['T_TOTAL']).val(details[0]['T_TOTAL']);
						$.each(details,function(index,value){
							make_selected = (quality_saved == value['QUALITY'])?"selected":"";
							$("#popUpForm select.quality").append("<option value='"+value['QUALITY']+"'  "+make_selected+" >"+value['QUALITY']+"</option>");
						});
						$("#popUpForm select.quality").selectpicker('refresh');
						$("#popUpForm input.length").focus();
				});
			};
			$.fn.editOutwardDetails = function(){
				var idValue = $(this).attr("do");
				$.post('pop-up-form/update-outward-detail-posted.php',{ oDetail_id:idValue },function(data){
					$("#popUpForm").html(data);
					$("#popUpForm").width(590);
					$("#popUpForm select").selectpicker('refresh');
					$("#popUpForm").centerThisDiv();
					$("#xfade").fadeIn();
					$("#popUpForm").fadeIn();
					$("#popUpForm .closePopUpForm").click(function(){ $("#popUpForm").fadeOut();$("#xfade").fadeOut();$("#popUpForm").html(""); });
					$(".input_readonly").css({'cursor':'default'});
					$(".input_readonly").focus(function(){
						$(this).blur();
					});
					$("#popUpForm input[type='text']").attr('autocomplete','off');
					$("#popUpForm input[name='length'],#popUpForm input[name='embRate'],#popUpForm input[name='stitchRate']").numericOnly();
					$("#popUpForm input[name='length']").keyup(function(){
						$("input[name='thanLengthOs']").val($("input[name='thanLengthOs']").attr("thanOutStanding")-$("#popUpForm input[name='length']").val());
					});
					$("#popUpForm input[name='embRate']").keyup(function(){
						$("input[name='thanLengthOs']").val($("input[name='thanLengthOs']").attr("thanOutStanding")-$("#popUpForm input[name='length']").val());
					});
					$("#popUpForm input[name='stitchRate']").keyup(function(){
						$("input[name='thanLengthOs']").val($("input[name='thanLengthOs']").attr("thanOutStanding")-$("#popUpForm input[name='length']").val());
					});
					$("#popUpForm input[name='lotNum']").getProductQualitiesForUpdateForm();
					$("#popUpForm input[name='lotNum']").keydown(function(e){
						if(e.keyCode==13){
							e.preventDefault();
							$(this).getProductQualitiesForUpdateForm();
						}
					});
					$("#popUpForm .insert_prod_title").focus();
					$("#popUpForm .productCode").setFocusTo("#popUpForm .quality");
					$("#popUpForm .quality").setFocusTo("#popUpForm .measure");
					$("#popUpForm .measure").setFocusTo("#popUpForm input[name='length']");
					$("#popUpForm input[name='length']").setFocusTo("#popUpForm input[name='embRate']");
					$("#popUpForm input[name='embRate']").setFocusTo("#popUpForm input[name='stitchRate']");
					$("#popUpForm input[name='stitchRate']").setFocusTo("#popUpForm .stitchAccount");
					$("#popUpForm .stitchAccount").setFocusTo("#popUpForm input[name='designNum']");
					$("#popUpForm input[name='designNum']").setFocusTo("#popUpForm .machineNum");
					$("#popUpForm .machineNum").setFocusTo("#popUpForm input[type='submit']");
					$("#popUpForm select.billing_type2,#popUpForm input[name='total_laces'],#popUpForm input[name='stitchRate'],#popUpForm input[name='stitches2'],#popUpForm input[name='embRate']").change(function(){
						embroideryFormulasPopup();
					});
					$(".lengthColumn").sumColumn(".sumLenTotal");
					$(".embAmountColumn").sumColumn(".embAmountTotal");
					$(".stichAmountColumn").sumColumn(".stichAmountTotal");
				});
			};
			$.fn.quickSubmit = function(){
				//P for issue/Processing  AND R For Claim/Refund
				var goodToGo = true;
				var claimOrIssue = 'P';
				if($(".claimCheck").is(":checked")){
					claimOrIssue = 'R';
				}
				var product_id 	 				= parseInt($("select.product_id option:selected").val())||0;
				var lotNum 	 						= $("input.lotNum").val();
				var pQuality 						= $("select.qualityDropDown option:selected").val();
				var pMeasure 						= $("select.measure option:selected").val();
				var billing_type 				= $("select.billing_type option:selected").val();
				var stitches 						= $("input.stitches").val();
				var total_laces 				= $("input.total_laces").val();
				var pLength 						= $("input.length").val();
				var embRate 						= $("input.embRate").val();
				var embAmount 					= $("input.embAmount").val();
				var stitchRate 					= $(".stitchRate").val();
				var stitchAmount 				= $(".stitchAmount").val();
				var stitchAccount 			= $("select[name=stitchAccount] option:selected").val();
				var designNum 					= $(".designNum").val();
				var machineNum 					= parseInt($("select.machine_id option:selected").val())||0;

				var product_name 	 			= $("select.product_id option:selected").text();
				var pMeasureName 				= $("select.measure option:selected").text();
				var billing_type_name 	= $("select.billing_type option:selected").text();
				var stitchAccountName 	= $("select[name=stitchAccount] option:selected").text();
				var machineName 				= $("select.machine_id option:selected").text();

				if(claimOrIssue == 'R' &&  embAmount >= 0){
					goodToGo = false;
					$(".embAmount").css({'background':'#F00','color':'#FFF'});
				}else{
					$(".embAmount").css({'background':'#FFF','color':'#666'});
				}
				if(claimOrIssue == 'P' &&  embAmount < 0){
					goodToGo = false;
					$(".embAmount").css({'background':'#F00','color':'#FFF'});
				}else{
					$(".embAmount").css({'background':'#FFF','color':'#666'});
				}

				if(product_id == 0){
					alert('error:Product');
					return;
				}
				$(this).blur();
				if(!goodToGo){
					alert('error');
					return;
				}
				var row_id = (parseInt($("tr.updateMode").attr("data-lot-id"))||0);
				var guid   = guidGenerator();
				var row_class_type = 'lot-register-rows';
				if($("tr.updateMode").length){
					if($("tr.updateMode").hasClass('bill-rows')){
						row_class_type = 'bill-rows';
					}
				}
				var row = ''
				+ '<tr class="alt-row calculations '+row_class_type+'" data-lot-id="'+row_id+'">'
				+ 	'<td class="text-center product_id" data-product-id="'+product_id+'">'+product_name+'</td>'
				+ 	'<td class="text-center lot_no">'+lotNum+'</td>'
				+ 	'<td class="text-center quality">'+pQuality+'</td>'
				+ 	'<td class="text-center measure_id" data-measure-id="'+pMeasure+'">'+pMeasureName+'</td>'
				+ 	'<td class="text-center billing_type" data-billing-type="'+billing_type+'">'+billing_type_name+'</td>'
				+ 	'<td class="text-center stitches">'+stitches+'</td>'
				+ 	'<td class="text-center total_laces">'+total_laces+'</td>'
				+ 	'<td class="text-center measure_length lengthColumn">'+pLength+'</td>'
				+ 	'<td class="text-center emb_rate">'+embRate+'</td>'
				+ 	'<td class="text-center emb_amount embAmountColumn">'+embAmount+'</td>'
				+ 	'<td class="text-center stitch_rate">'+stitchRate+'</td>'
				+ 	'<td class="text-center stitch_amount stichAmountColumn">'+stitchAmount+'</td>'
				+ 	'<td class="text-center stitch_acc" data-stitch-id="'+stitchAccount+'">'+stitchAccountName+'</td>'
				+ 	'<td class="text-center design_code">'+designNum+'</td>'
				+ 	'<td class="text-center machine_id" data-machine-id="'+machineNum+'">'+machineNum+'</td>'
				+ 	'<td><a class="quick_edit" id="view_button" do="'+row_id+'" title="Modify"><i class="fa fa-pencil"></i></a></td>'
				+ 	'<td class="text-center">'
				+ 		'<input type="checkbox" class="css-checkbox" id="'+guid+'" value="'+row_id+'" />'
				+ 		'<label for="'+guid+'" class="css-label"></label>'
				+ 	'</td>'
				+ '</tr>';

				if($("tr.updateMode").length){
					$("tr.updateMode").replaceWith(row);
				}else{
					$(row).insertBefore($("table.outwardDetailsTable tbody tr.totals"));
				}
				if(claimOrIssue == 'R'){
					$("tr.calculations").last().find('td').css('color','#C30');
				}
				$("a.pointer").click(function(){
					$(this).deleteRow();
				});
				$("tr.quickSubmit input[type='text']").val("");
				$("tr.quickSubmit select.qualityDropDown").find("option").remove();
				$("tr.quickSubmit select.qualityDropDown").selectpicker('refresh');
				$("select.product_id").selectpicker('val','');
				$("div.product_id button").focus();
			};
			$.fn.deleteRow = function(){
				var idValue = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Are You Sure?</p><a class='dodelete'>Confirm</a><a class='nodelete'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.get("db/del-outward.php", {did : idValue}, function(data){
							if(data==1){
								$("#popUpDel").children(".confirm").text("1 Row Deleted Successfully!");
								$("#popUpDel").children(".dodelete").hide();
								$("#popUpDel").children(".nodelete").text("Close");
								clickedDel.parent().siblings().text("0").parent().slideUp();
							}else{
								$("#popUpDel").children(".confirm").text("Unable to Delete.");
								$("#popUpDel").children(".dodelete").hide();
								$("#popUpDel").children(".nodelete").text("Close");
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
					});
				$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#fade").fadeOut('fast');
				});
			};
			$.fn.insertProductCode = function(insertToElementClassName,file,sendTitle,getCode){
				var thisElm = $(this);
				var codeCode = thisElm.val();
					if(codeCode!=="" & codeCode.length>0){
						$.post(file,{title:codeCode},function(data){
							var gotCode = $.parseJSON(data);
							var pTitles = [];
							$.each(gotCode,function(index,value){
								$.each(value,function(i,v){
									if(i==sendTitle){
										pTitles.push(v);
									}
								});
							});
							$(thisElm).autocomplete({
									source: pTitles,
									scrollHeight:220,
									matchContains: true,
									selectFirst: false
							});
							$.each(pTitles,function(i,v){
								if(codeCode==v){
									$(insertToElementClassName).val(gotCode[0][getCode]);
								}else{
									$(insertToElementClassName).val("");
								}
							});
						});
					}else{
						$(insertToElementClassName).val("");
					}
			};
			$.fn.insertProductCodeUpdate = function(insertToElementClassName,file,sendTitle,getCode){
				var thisElm = $(this);
				var codeCode = thisElm.val();
					if(codeCode!=="" & codeCode.length>0){
						$.post(file,{title:codeCode},function(data){
							var gotCode = $.parseJSON(data);
							var pTitles = [];
							$.each(gotCode,function(index,value){
								$.each(value,function(i,v){
									if(i==sendTitle){
										pTitles.push(v);
									}
								});
							});
							$(thisElm).autocomplete({
									source: pTitles,
									scrollHeight:220,
									matchContains: true,
									selectFirst: false
							});
							$.each(pTitles,function(i,v){
								if(codeCode==v){
									$(insertToElementClassName).val(gotCode[0][getCode]);
								}else{
									$(insertToElementClassName).val("");
								}
							});
						});
					}else{
						$(insertToElementClassName).val("");
					}
			};
			$.fn.getCompletedLotsByCustomer = function(outward_id_element){
				var custCode 	 = $(this).val();
				var outward_id = $(outward_id_element).val();
				if(custCode != ''){
					$.post('db/getCompletedLots.php',{custCode:custCode,outward_id:outward_id},function(data){
						if(data != ''){
							$('table.prom').children('tbody').remove();
							$('table.prom').append(data);
							$("a.pointer").click(function(){
								$(this).deleteRow();
							});
							$(".editDetailsFrom").click(function(){
								$(this).editOutwardDetails();
							});
						}
					});
				}
			};
			$.fn.sumColumn = function(total){
				var totalThaans = 0;
				$(this).each(function() {
					totalThaans += parseInt($(this).text());
				});
				$(total).text(totalThaans);
			};
			$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
				$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
			};
			$.fn.centerThisDiv = function(){
				var win_hi = $(window).height()/2;
				var win_width = $(window).width()/2;
				win_hi = win_hi-$(this).height()/2;
				win_width = win_width-$(this).width()/2;
				$(this).css({
					'position': 'absolute',
					'top': '50px',
					'left': win_width
				});
			};
			$.fn.setFocusTo = function(Elm){
				$(this).keydown(function(e){
					if(e.keyCode==13){
						if($(this).val()==""){
							e.preventDefault();
						}else{
							e.preventDefault();
							$(Elm).focus();
						}
					}
				});
			};
			$(".input_readonly").css({'cursor':'default'});
			$(".input_readonly").focus(function(){
				$(this).blur();
			});
			$(function(){
				$(window).keyup(function(e){
					if(e.keyCode==27){
						$(window).hideOverLay();
					}
				});
			});
			$(".quickSubmit input").keyup(function(e){
				if(e.keyCode==27){
					$(".quickSubmit input[type='text']").val("");
					$(".quickSubmit input.thaanLengthOS").val("");
					$(".quickSubmit .qualityDropDown").find("option").first().nextAll().remove();
					$("button.dropdown-toggle").last().focus();
					$("#popUpDel").slideUp(200,function(){
						$("#popUpDel").remove();
					});
					$("#xfade").fadeOut();
				}
			});
			$.fn.journalizeThis = function(){
				if($("input[name='outwd_id']").length){
					var mainId = $("input[name='outwd_id']").val();
				}else{
					var mainId = 0;
				}

				var outward_date     = $("input[name='outwardDate']").val();
				var billNumber       = $("input[name='billNum']").val();
				var cloth 					 = $("input[name='cloth']").val();
				var yarn 					 	 = $("input[name='yarn']").val();
				var transport 			 = $("input[name='transport']").val();
				var customerAccCode  = $("select.custCodeSelector").val();
				var customerAccTitle = $("select.custCodeSelector option:selected").text();
				var third_party_code = $("select.third_party_code option:selected").val();

				var lot_register_rows = {};
				var bill_rows   			= {};
				var deleted_rows   		= {};

				var total_rows = 0;

				$("tr.lot-register-rows").each(function(i,e){
					if(!$(this).find("input[type='checkbox']").is(":checked")){
						return;
					}
					lot_register_rows[i] 								 = {};
					lot_register_rows[i].row_id 				 = $(this).attr("data-lot-id");
					lot_register_rows[i].product_id 		 = parseInt($(this).find("td.product_id").attr("data-product-id"))||0;
					lot_register_rows[i].lot_no          = $(this).find("td.lot_no").text();
					lot_register_rows[i].quality         = $(this).find("td.quality").text();
					lot_register_rows[i].measure_id      = parseInt($(this).find("td.measure_id").attr('data-measure-id'))||0;
					lot_register_rows[i].billing_type    = $(this).find("td.billing_type").attr('data-billing-type');
					lot_register_rows[i].stitches        = parseInt($(this).find("td.stitches").text())||0;
					lot_register_rows[i].total_laces     = parseFloat($(this).find("td.total_laces").text())||0;
					lot_register_rows[i].measure_length  = parseFloat($(this).find("td.measure_length").text())||0;
					lot_register_rows[i].emb_rate        = parseFloat($(this).find("td.emb_rate").text())||0;
					lot_register_rows[i].emb_amount      = parseFloat($(this).find("td.emb_amount").text())||0;
					lot_register_rows[i].stitch_rate     = parseFloat($(this).find("td.stitch_rate").text())||0;
					lot_register_rows[i].stitch_amount   = parseFloat($(this).find("td.stitch_amount").text())||0;
					lot_register_rows[i].stitch_acc      = $(this).find("td.stitch_acc").attr('data-stitch-id');
					lot_register_rows[i].design_code     = $(this).find("td.design_code").text();
					lot_register_rows[i].machine_id      = parseInt($(this).find("td.machine_id").text())||0;
					total_rows++;
				});
				lot_register_rows = JSON.stringify(lot_register_rows);

				$("tr.bill-rows").each(function(i,e){
					if(!$(this).find("input[type='checkbox']").is(":checked")){
						deleted_rows[i] = parseInt($(this).attr("data-lot-id"))||0;
						return;
					}
					bill_rows[i] 								 = {};
					bill_rows[i].row_id 				 = $(this).attr("data-lot-id");
					bill_rows[i].product_id 		 = parseInt($(this).find("td.product_id").attr("data-product-id"))||0;
					bill_rows[i].lot_no          = $(this).find("td.lot_no").text();
					bill_rows[i].quality         = $(this).find("td.quality").text();
					bill_rows[i].measure_id      = parseInt($(this).find("td.measure_id").attr('data-measure-id'))||0;
					bill_rows[i].billing_type    = $(this).find("td.billing_type").attr('data-billing-type');
					bill_rows[i].stitches        = parseInt($(this).find("td.stitches").text())||0;
					bill_rows[i].total_laces     = parseFloat($(this).find("td.total_laces").text())||0;
					bill_rows[i].measure_length  = parseFloat($(this).find("td.measure_length").text())||0;
					bill_rows[i].emb_rate        = parseFloat($(this).find("td.emb_rate").text())||0;
					bill_rows[i].emb_amount      = parseFloat($(this).find("td.emb_amount").text())||0;
					bill_rows[i].stitch_rate     = parseFloat($(this).find("td.stitch_rate").text())||0;
					bill_rows[i].stitch_amount   = parseFloat($(this).find("td.stitch_amount").text())||0;
					bill_rows[i].stitch_acc      = $(this).find("td.stitch_acc").attr('data-stitch-id');
					bill_rows[i].design_code     = $(this).find("td.design_code").text();
					bill_rows[i].machine_id      = parseInt($(this).find("td.machine_id").text())||0;
					total_rows++;
				});
				bill_rows = JSON.stringify(bill_rows);

				deleted_rows = JSON.stringify(deleted_rows);

				if(total_rows == 0){
					displayMessage("Error! No lot selected against this outward. ");
					return;
				}

				$.post('db/createOutwardVoucher.php',{createVoucher:true,
													  mainId:mainId,
														lot_register_rows:lot_register_rows,
														bill_rows:bill_rows,
														deleted_rows:deleted_rows,
													  outward_date:outward_date,
													  billNumber:billNumber,
														cloth:cloth,
														yarn:yarn,
														transport:transport,
													  customerAccCode:customerAccCode,
													  customerAccTitle:customerAccTitle,
														third_party_code:third_party_code},function(data){
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>"+data+"</p><a class='btn btn-info nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.href = 'emb-outward-details.php?wid='+mainId;
							});
							$("#xfade").fadeOut();
						});
						$(".close_popup").click(function(){
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut('fast');
						});
				});
			};
			$.fn.reverseVoucher = function(outward_id_element,CustomerAccCodeElem,outward_date_element){
				var voucher_id  = $(this).attr('data-voucher-id');
				var outward_id  = $(outward_id_element).val();
				var outward_date= $(outward_date_element).val();
				var accountCode = $(CustomerAccCodeElem).val();

				$.post('db/reverseOutwardVoucher.php',{reverseVoucher:true,outward_date:outward_date,accountCode:accountCode,outward_id:outward_id,voucher_id:voucher_id},function(data){
					if(data.replace(/\s+/g, '') == 'X'){
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Floating entry removed from record!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut();
						});
					}else if(data==1){
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Reversal Entry Posted Successfully!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut();
						});
						$(".close_popup").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut('fast');
						});
					}else{
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Error Occured While Reversing Entry!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut();
						});
						$(".close_popup").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut('fast');
						});
					}
				});
			};
});
var checkLots = function(thisElement){
	$("input[type='checkbox']").prop("checked",$(thisElement).prop("checked"));
}
var createOutward = function(){
	var lotList = '';
	$("input.lotCheckBox:checked").each(function(index, element){
		if(index !== 0){
			lotList += ',';
		}
	   lotList += $(this).val();
	});
	var outward_id = $(".outwd_id").val();
	$.post('db/createOutwardEntries.php',{lotList:lotList,outward_id:outward_id},function(data){
		//alert(data);
	});
}

var getCompletedLots = function(){
	var customerCode = $("select.custCodeSelector option:selected").val();
	$.post("db/getCompletedLots.php",{customerCode:customerCode},function(data){
		$(".outwardDetailsTable tbody").remove();
		$(".outwardDetailsTable").append(data);
		$(".innerTdRed").find('td').css('color','#C30');
		$("div.createVoucher").remove();
		$(".underTheTable").append('<div class="btn btn-primary pull-right createVoucher">Save</div>');
		$("div.createVoucher").click(function(){
			$(this).journalizeThis();
		});
	});
}
var noComment = function(){
	$("#fade").fadeOut();
	$("#popUpForm4").fadeOut();
}
var applyComment = function(){
	$("#fade").fadeIn();
	$("#popUpForm4").centerThisDiv();
	$("#popUpForm4").fadeIn();
	$("#fade").click(function(){
		$("#fade").fadeOut();
		$("#popUpForm4").fadeOut();
	});
}
var embroideryFormulasPopup = function(){
	if($("#popUpForm select.billing_type2 option:selected").val() == 'S'){
		var stitches 	 = parseInt($("#popUpForm input[name='stitches2']").val())||0;
		var stitchRate = parseFloat($("#popUpForm input[name='stitchRate']").val())||0;
		var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

		var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
		stitch_amount     = parseFloat(stitch_amount)||0;
		stitch_amount  		= (stitch_amount).toFixed(2);
		$("#popUpForm input[name='stitchAmount']").val(stitch_amount);
		$("#popUpForm input[name='embAmount']").val(0);
	}
	if($("#popUpForm select.billing_type2 option:selected").val() == 'L'){
		var stitches 	 = parseInt($("#popUpForm input[name='stitches2']").val())||0;
		var stitchRate = parseFloat($("#popUpForm input[name='stitchRate']").val())||0;
		var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

		var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
		stitch_amount     = parseFloat(stitch_amount)||0;
		stitch_amount  		= (stitch_amount).toFixed(2);
		$("#popUpForm input[name='stitchAmount']").val(stitch_amount);

		var emb_rate 	    = parseFloat($("#popUpForm input[name='embRate']").val())||0;
		var total_laces   = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

		var emb_amount = emb_rate * total_laces;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  	 = (emb_amount).toFixed(2);

		$("#popUpForm input[name='embAmount']").val(emb_amount);
	}
	if($("#popUpForm  select.billing_type2 option:selected").val() == 'Y'){
		var emb_rate 	 = parseFloat($("#popUpForm input[name='embRate']").val())||0;
		var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

		var emb_amount = ( emb_rate * 15 ) * len;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  		= (emb_amount).toFixed(2);

		$("#popUpForm input[name='embAmount']").val(emb_amount);
		$("#popUpForm input[name='stitchAmount']").val(0);
	}
	if($("#popUpForm select.billing_type2 option:selected").val() == 'U'){
		var emb_rate 	 = parseFloat($("#popUpForm input[name='embRate']").val())||0;
		var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

		var emb_amount = emb_rate * len;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  	= (emb_amount).toFixed(2);

		$("#popUpForm input[name='embAmount']").val(emb_amount);
		$("#popUpForm input[name='stitchAmount']").val(0);
	}
};
var embroideryFormulas = function(){
	if($("select.billing_type option:selected").val() == 'S'){
		var stitches 	 		= parseInt($("input.stitches").val())||0;
		var stitchRate 		= parseFloat($("input.stitchRate").val())||0;
		var len     	 		= parseFloat($("input.total_laces").val())||0;
		var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
		stitch_amount     = parseFloat(stitch_amount)||0;
		stitch_amount  		= (stitch_amount).toFixed(2);
		$("input.stitchAmount").val(stitch_amount);
		$("input.embAmount").val(0);
	}
	if($("select.billing_type option:selected").val() == 'L'){
		var stitches 	 		= parseInt($("input.stitches").val())||0;
		var stitchRate 		= parseFloat($("input.stitchRate").val())||0;
		var len     	 		= parseFloat($("input.total_laces").val())||0;
		var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
		stitch_amount     = parseFloat(stitch_amount)||0;
		stitch_amount  		= (stitch_amount).toFixed(2);
		$("input.stitchAmount").val(stitch_amount);

		var emb_rate 	 = parseFloat($("input.embRate").val())||0;
		var total_laces= parseFloat($("input.total_laces").val())||0;
		var emb_amount = emb_rate * total_laces;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  	 = (emb_amount).toFixed(2);
		$("input.embAmount").val(emb_amount);
	}
	if($("select.billing_type option:selected").val() == 'Y'){
		var emb_rate 	 = parseFloat($("input.embRate").val())||0;
		var len     	 = parseFloat($("input.total_laces").val())||0;
		var emb_amount = ( emb_rate * 15 ) * len;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  	 = (emb_amount).toFixed(2);
		$("input.embAmount").val(emb_amount);
		$("input.stitchAmount").val(0);
	}
	if($("select.billing_type option:selected").val() == 'U'){
		var emb_rate 	 = parseFloat($("input.embRate").val())||0;
		var len     	 = parseFloat($("input.total_laces").val())||0;
		var emb_amount = emb_rate * len;
		emb_amount     = parseFloat(emb_amount)||0;
		emb_amount  	 = (emb_amount).toFixed(2);
		$("input.embAmount").val(emb_amount);
		$("input.stitchAmount").val(0);
	}
};
