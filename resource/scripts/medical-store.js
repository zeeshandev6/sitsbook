$(document).ready(function(){
	hideSideBarOnBlur();
	$("select.medicineSelector").selectpicker();

	$("select.tokenSelector").change(function(){
		getPatientsDetailsByDropDown($(this).find('option:selected'));
	});
	$("#saveButton").click(function(){
		medicalProcess();
	});
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	//Actions For user
	$(".quickSave").keydown(function(e){
		if(e.keyCode == 13){
			$(this).quickSave();
		}
	});
	$(".quickSave").click(function(){
		$(this).quickSave();
	});
	$(".printPage").click(function(){
		var patient_id = $(".patient_id").val();
		if(patient_id > 0){
			window.location.href = 'print-prescription.php?id='+patient_id;
		}
	});

	$.fn.quickSave = function(){
		var $numOfEditRows = $(".editMode").length;
		var thisTable = $(this).parent().parent().parent().parent();
		var medicineName = thisTable.find(".medicineSelector option:selected").text();
		var quantity = thisTable.find(".quantity").val();
		var price = thisTable.find(".price").val();
		var totalAmount = thisTable.find(".totalAmount").val();

		var patient_id = $(".patient_id").val();
		$(this).blur();

		if(medicineName != ''){
			var tableTr = '';
			tableTr += '<tr class="calculations ajaxRow ashRow">';
			tableTr += '<td class="medicineTd">'+medicineName+'</td>';
			tableTr += '<td class="quantityTd">'+quantity+'</td>';
			tableTr += '<td class="priceTd">'+price+'</td>';
			tableTr += '<td class="totalAmountTd">'+totalAmount+'</td>';
			tableTr += '<td> - - - </td>';
			tableTr += '<td width="10%" style="text-align:center;" class="actionTd">';
			tableTr += '<button id="view_button" onclick="editRow(this);" ><i class="fa fa-pencil"></i></button>';
			tableTr += '&nbsp;';
			tableTr += '<button class="pointer" onclick="deleteRow(this);" ><i class="fa fa-times"></i></button>';
			tableTr += '</td>';
			tableTr += '</tr>';
			if($numOfEditRows > 0){
				$(".editMode").replaceWith(tableTr);
			}else{
				$(tableTr).insertAfter(thisTable.find(".calculations").last());
			}
			calculateColumnTotals();
			thisTable.find(".medicineSelector option:selected").prop('selected',false);
			thisTable.find(".medicineSelector").find('option').first().prop('selected',true);;
			thisTable.find(".medicineSelector").selectpicker('refresh');
			thisTable.find(".quantity").val('');
			thisTable.find(".price").val('');
			thisTable.find(".totalAmount").val('');
			thisTable.find("div.medicineSelector button").focus();
			thisTable.find(".medicineSelector").selectpicker('close');
		}
	}


	$(".quickSave").keydown(function(e){
		if(e.keyCode == 13){
			$(this).quickSave();
		}
	});
	$(".quickSave").click(function(){
		$(this).quickSave();
	});

});

$(window).load(function(){
	initializeEvents();
});

var editRow = function(thisElm){
	$(".editMode").removeClass('editMode');
	var $thisRow = $(thisElm).parent().parent();
	var thisType = $(thisElm).attr('data-type');
	var row_id   = $(thisElm).attr('data-id');
	var $thisTypeInput = $('.quickSave').parent('td').parent('tr');

	var medicineName = $thisRow.find(".medicineTd").text();
	var quantity       = $thisRow.find(".quantityTd").text();
	var price     = parseInt($thisRow.find(".priceTd").text())||0;
	var totalAmount = $thisRow.find(".totalAmountTd").text();


	$thisTypeInput.find(".medicineSelector option").each(function(index, element) {
        if($(this).attr('data-name') == medicineName){
			$(this).prop('selected',true).siblings().prop('selected',false);
		}
    });
	$("select.medicineSelector").selectpicker("refresh");

	$thisTypeInput.find("input.quantity").val(quantity);
	$thisTypeInput.find("input.price").val(price);
	$thisTypeInput.find("input.totalAmount").val(totalAmount);

	$thisRow.addClass('editMode');
	$thisTypeInput.find("div.medicineSelector button").focus();
}

var deleteRow = function(thisElm){
	var type = $(thisElm).attr('data-type');
	var thisRow = $(thisElm).parent('td').parent('tr');
	var message = "Confirm Deletion?";
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").hide();
	$("#fade").fadeIn('slow');
	var win_hi = $(window).height()/2;
	var win_width = $(window).width()/2;
	win_hi = win_hi-$("#popUpDel").height()/2;
	win_width = win_width-$("#popUpDel").width()/2;
	$("#popUpDel").css({
		'position': 'fixed',
		'top': win_hi,
		'left': win_width
	});
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
		thisRow.remove();
		calculateColumnTotals();
		$("#popUpDel").slideDown(function(){
			$("#popUpDel").remove();
		});
		$("#fade").fadeOut('slow');
	});
	$(".nodelete").click(function(){
		$("#popUpDel").slideDown(function(){
			$("#popUpDel").remove();
		});
		$("#fade").fadeOut('slow');
	});
}
var initializeEvents = function(){
	$("select.medicineSelector").change(function(){
		var itemName = $(".medicineSelector option:selected").val();
		getItemDetails(itemName);
		$(this).parent().parent().parent().find("input.quantity").focus();
	});
	$("div.medicineSelector").find(".dropdown-toggle").keyup(function(e){
		if(e.keyCode == 13){
			var itemName = $(".medicineSelector option:selected").val();
			getItemDetails(itemName);
			$(this).parent().parent().parent().find("input.quantity").focus();
		}
	});
	$("input.quantity").keyup(function(e){
		reduceStockOs();
		calculateTotalAmount();
		if(e.keyCode == 13 && $(this).val() != ''){
			$(this).parent().parent().find("input.price").focus();
		}
	});
	$("input.price").keyup(function(e){
		calculateTotalAmount();
		if(e.keyCode == 13 && $(this).val() != ''){
			$(this).parent().parent().find("input.totalAmount").focus();
		}
	});
	$("input.totalAmount").keyup(function(e){
		if(e.keyCode == 13 && $(this).val() != ''){
			$(this).parent().parent().find("input.quickSave").focus();
		}
	});
}

var toggleSideBar = function(){
	if($(".sideBar").hasClass("sideBarOpen")){
		$(".sideBar").removeClass("sideBarOpen");
		$(".sideBar").addClass("sideBarClose");
	}else{
		$(".sideBar").removeClass("sideBarClose");
		$(".sideBar").addClass("sideBarOpen");
	}
	getPatientsList();
	countPatients();
}
var countPatients = function(){
	var patientsCount = $(".sideBar ul li").length;
	$(".sideBar .confirm .numPatients").text('').text("( "+patientsCount+" )");
}
var getPatientsList = function(){
	$.post('db/getPatientList.php',{nonMedical:true},function(data){
		$(".sideBar ul").html(data);
		countPatients();
	});
}
var getPaitentListByDate = function(dateElement){
 	var dateForSearch = $(dateElement).val();
	$.post('db/getPatientList.php',{getListByDate:dateForSearch},function(data){
		$("input[name='token']").hide();
		$("select.tokenSelector").find('option').first().nextAll().remove();
		$("select.tokenSelector").append(data);
		$("select.tokenSelector").selectpicker('refresh');
		$("div.tokenSelector").show();
	});
}
var getPatientsDetails = function(thisElem){
	$(".lightFade").fadeIn();
	idVal = parseInt($(thisElem).attr('data-id'));

	$('tr.ajaxRow').remove();

	$.post('db/getPrescriptions.php',{getMedicals:idVal},function(data){
		$(".docTbody").append(data);
		$(".docTable").show();
		$(".prescribed").show();
	});
	$.post('db/getPatientDetails.php',{id:idVal},function(data){
		var patientDetails = $.parseJSON(data);
		$(".patient_id").val(idVal);
		$("div.tokenSelector").hide();
		$("input[name='token']").show();
		$("input[name='token']").val(patientDetails['TOKEN_NO']);
		$("input[name='patientName']").val(patientDetails['NAME']);
		$("input[name='age']").val(patientDetails['AGE']);
		if(patientDetails['GENDER'] == 'M'){
			patientDetails['GENDER'] = 'Male';
		}else if(patientDetails['GENDER'] == 'F'){
			patientDetails['GENDER'] = 'Female';
		}else if(patientDetails['GENDER'] == 'C'){
			patientDetails['GENDER'] = 'Child';
		}
		$("input[name='gender']").val(patientDetails['GENDER']);
		var priority = 'Normal';
		if(patientDetails['PRIORITY'] == 'E'){
			priority = 'Emergency';
		}
		$("input[name='priority']").val(priority);
		$("input[name='mobile']").val(patientDetails['MOBILE']);
		$(".bP").val(patientDetails['BP']);
		$(".temp").val(patientDetails['TEMP']);
		$(".doc_fee").val(patientDetails['DOC_FEE']);
		$(".lab_fee").val(patientDetails['LAB_FEE']);
		$(".diagnosis").val(patientDetails['DIAGNOSIS']);
		toggleSideBar();
		$(".lightFade").hide();
		if(patientDetails['BP'] == ''){
			$(".bP").focus();
		}else{
			$("div.medicineSelector").first().find(".dropdown-toggle").focus();
		}
	});
}

var getPatientsDetailsByDropDown = function(thisElem){
	$(".lightFade").fadeIn();
	idVal = parseInt($(thisElem).attr('data-id'));

	$('tr.ajaxRow').remove();

	$.post('db/getPrescriptions.php',{getMedicals:idVal},function(data){
		$(".docTbody").append(data);
		$(".docTable").show();
		$(".prescribed").show();
	});
	$.post('db/getPatientDetails.php',{id:idVal},function(data){
		var patientDetails = $.parseJSON(data);
		$(".patient_id").val(idVal);
		$("input[name='patientName']").val(patientDetails['NAME']);
		$("input[name='age']").val(patientDetails['AGE']);
		if(patientDetails['GENDER'] == 'M'){
			patientDetails['GENDER'] = 'Male';
		}else if(patientDetails['GENDER'] == 'F'){
			patientDetails['GENDER'] = 'Female';
		}else if(patientDetails['GENDER'] == 'C'){
			patientDetails['GENDER'] = 'Child';
		}
		$("input[name='gender']").val(patientDetails['GENDER']);
		var priority = 'Normal';
		if(patientDetails['PRIORITY'] == 'E'){
			priority = 'Emergency';
		}
		$("input[name='priority']").val(priority);
		$("input[name='mobile']").val(patientDetails['MOBILE']);
		$(".bP").val(patientDetails['BP']);
		$(".temp").val(patientDetails['TEMP']);
		$(".doc_fee").val(patientDetails['DOC_FEE']);
		$(".lab_fee").val(patientDetails['LAB_FEE']);
		$(".diagnosis").val(patientDetails['DIAGNOSIS']);
		$(".lightFade").hide();
		if(patientDetails['BP'] == ''){
			$(".bP").focus();
		}else{
			$("div.medicineSelector").first().find(".dropdown-toggle").focus();
		}
	});
}
var medicalProcess = function(){
	var patient_id = $(".patient_id").val();

	var discount   = $("input.discount").val()||0;

	var jSonString  = '';
	jSonString += '{';
	$(".medTable .ajaxRow").each(function(index, element){
		var medicineName = $(this).find("td.medicineTd").text();
		var quantity     = parseInt($(this).find("td.quantityTd").text())||0;
		var price 		 = parseFloat($(this).find("td.priceTd").text())||0;
		var totalAmount  = parseFloat($(this).find("td.totalAmountTd").text())||0;
		if(index > 0){
			jSonString += ',';
		}
		if(medicineName != '' && quantity != 0 && price != 0){
			jSonString += '"'+index+'":';
			jSonString += '{';
			jSonString += '"medicineName":"'+medicineName+'","quantity":"'+quantity+'",';
			jSonString += '"price":"'+price+'",';
			jSonString += '"totalAmount":"'+totalAmount+'"';
			jSonString += '}';
		}
	});
	jSonString += '}';
	$.post('db/postMedicalStore.php',{patient_id:patient_id,discount:discount,jSonString:jSonString},function(data){
		data = $.parseJSON(data);
		if(data['ID']>0){
			window.location.href = 'medical-store-invoice.php?id='+data['ID'];
		}
	});
}
var reduceStockOs = function(){
	var quantity = parseInt($("input.quantity").val())||0;
	var stockOs  = parseInt($("input.stockOs").attr('stockOs'))||0;
	var thisPageTotalQty = 0;
	$("tr.ajaxRow").each(function(index, element){
		if(!$(this).hasClass("editMode")){
			var medName = $(this).find("td").first().text();
			var medQty  = parseInt($(this).find("td").first().next().text())||0;

			if($("select.medicineSelector option:selected").attr('data-name') == medName){
				thisPageTotalQty += medQty;
			}
		}
    });
	stockOs -= thisPageTotalQty;
	if(quantity <= stockOs){
		var $remainingStock = stockOs - quantity;
		$("input.stockOs").val($remainingStock);
	}else{
		$("input.quantity").val('').keyup();
	}
}
var calculateTotalAmount = function(){
	var price = $("input.price").val();
	var quantity = parseInt($("input.quantity").val())||0;
	var totalAmount = price * quantity;
	$("input.totalAmount").val(totalAmount);
}
var calculateColumnTotals = function(){
	var $qtyTotal = 0;
	var $amountTotal = 0;
	$(".ashRow").each(function(index, element) {
        $qtyTotal += parseInt($(this).find("td.quantityTd").text())||0;
		$amountTotal += parseInt($(this).find("td.totalAmountTd").text())||0;
    });
	$(".qtyTotalTd").text($qtyTotal);
	$(".amountColumnTd").text($amountTotal);
}
var checkEmAll = function(thisElm){
	$(".distribute").prop('checked',$(thisElm).prop('checked'));
}
var getItemDetails = function(s_item){
	if(s_item != ''){
		$.post('db/get-item-details.php',{s_item_id:s_item},function(data){
			data = $.parseJSON(data);
			$(".price").val(data['S_PRICE']);
			$(".stockOs").val(data['STOCK']).attr('stockOs',data['STOCK']);
		});
	}
}
//side bar hide function
var hideSideBarOnBlur=function(){$(window).click(function(e){var container = $(".sideBar");if(!container.is(e.target) && container.has(e.target).length === 0){if(container.hasClass("sideBarOpen")){container.removeClass("sideBarOpen");container.addClass("sideBarClose");}}});}
