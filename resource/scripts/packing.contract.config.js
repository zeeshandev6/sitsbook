$(document).on('click', 'input[name=save]', function(){
    $(this).prop("disabled",true);

    if($("select[name='supplier_id'] option:selected").val() == ''){
        notifyMe('uploads/logo/default.png','Error!','Account not selected.',null);
        $(this).prop("disabled",false);
        return;
    }

    if($("tr.table_row").length == 0){
        displayMessage("At least One Item is required!");
        $(this).prop("disabled",false);
        return false;
    }
    //table data
    var jsonString;
    var arrReturn   = {};
    var submit_form = true;
    var inputRow,row,row_id,fabric_type,grey_width,quantity;
    $('tr.table_row').each(function(x){
        arrReturn[x] = {};
        inputRow     = $(this);
        row_id       = parseInt($(inputRow).attr("data-id"))||0;
        arrReturn[x].row_id       = row_id;
        arrReturn[x].fabric_type = $(inputRow).find('td.fabric_type').attr('data-value');
        arrReturn[x].variety      = parseInt($(inputRow).find('td.variety').attr('data-id'))||0;
        arrReturn[x].thaan        = $(inputRow).find('td.thaan').text();
        arrReturn[x].quantity     = parseFloat($(inputRow).find('td.quantity').text())||0;
        arrReturn[x].rate         = $(inputRow).find('td.rate').text();
        arrReturn[x].amount       = $(inputRow).find('td.amount').text();
        if(arrReturn[x][quantity] == 0){
            submit_form = false;
        }
    });
    jsonString = JSON.stringify(arrReturn);
    if(submit_form){
        $('input[name=details]').val(jsonString);
        $('form[name=add-history]').submit();
        $('#myLoading').modal('show');
    }else{
      alert('no submition');
    }
    $(this).prop("disabled",false);
});
$(document).ready(function(){
    var qtyStock = '';
    var stock    = '';
    $($('select#item')).on('change', function(){
        var items_vall  = parseInt($('select#item').val())||0;
        var godown_id   = parseInt($("select[name=location] option:selected").val())||0;
        if(godown_id == 0||items_vall == 0){
            return;
        }
        var items_txtt  = $('select#item option:selected').text();
        var stockInHand = $.post('db/item_stock_qty.php', {itemStockGodown:items_vall,godown_id:godown_id}, function(data){
            stock = $.parseJSON(data);
            qtyStock = stock['stock'];
            $('input.stock_hand,input[name=stock_hand]').val(qtyStock);
        });
    });
    $("input.thaan").on('change keyup',function(){
        $("input.quantity").val($("input.thaan").val()*$("input.qty_per_thaan").val());
    });
    $("select.variety").change(function(){
        var item_id = $(this).find("option:selected").val();
        $.post('db/get-item-details.php',{item_id:item_id},function(data){
            data = $.parseJSON(data);
            $("input.qty_per_thaan").val(data['PER_CARTON']);
            $("input.thaan").trigger('keyup');
        });
    });
});
function calcoltotal(){
    var total_thaans   = 0;
    var total_quantity = 0;
    var total_amount   = 0;
    $("#pro_table tbody tr").each(function(){
        total_thaans    = (parseFloat($(this).find("td.thaan").text())||0);
        total_quantity  = (parseFloat($(this).find("td.quantity").text())||0);
        total_amount    = (parseFloat($(this).find("td.amount").text())||0);
    });
    $("#pro_table tfoot tr td.thaan_total").text(total_thaans);
    $("#pro_table tfoot tr td.quantity_total").text(total_quantity);
    $("#pro_table tfoot tr td.amount_total").text(total_amount);
}
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).parent().parent().attr("data-id");
        $("form[name=add-history]").append('<input type="hidden" value="'+id+'" name="deleted_rows[]" />');
        $(x).parent().parent().remove();
        x = null;
        calcoltotal();
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
//adding neow row in detail table
$(document).ready(function(){
    $('select').selectpicker();
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow,row,row_id,fabric_type,fabric_type_name,grey_width,quantity,fab_type,from_party;
        inputRow = $(this).parent().parent();

        fabric_type       = $(inputRow).find('select.fabric_type option:selected').val();
        fabric_type_name  = $(inputRow).find('select.fabric_type option:selected').text();
        variety      = parseInt($(inputRow).find('select.variety option:selected').val())||0;
        variety_name = $(inputRow).find('select.variety option:selected').text();
        thaan        = $(inputRow).find('input.thaan').val();
        quantity     = parseFloat($(inputRow).find('input.quantity').val())||0;
        rate         = $(inputRow).find('input.rate').val();
        amount       = $(inputRow).find('input.amount').val();

        if(fabric_type == '' || amount == 0){
            notifyMe('uploads/logo/default.png','Error!','Values Missing',null);
            $(this).prop("disabled",false);
            return;
        }

        row = '<tr class="table_row" data-id="0">'
          +'<td class="fabric_type text-left" data-value="'+fabric_type+'">'+fabric_type_name+'</td>'
          +'<td class="variety text-left" data-id="'+variety+'">'+variety_name+'</td>'
          +'<td class="thaan text-center">'+thaan+'</td>'
          +'<td class="quantity text-center">'+quantity+'</td>'
          +'<td class="rate text-center">'+rate+'</td>'
          +'<td class="amount text-center">'+amount+'</td>'
          +'<td class="text-center">'
            +'<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
            +'<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>'
          +'</td>'
        +'</tr>';

        if($(".updatemode").length){
          row_id =parseInt($(".updatemode").attr("data-id"))||0;
          $(row).insertAfter(".updatemode");
          $(".updatemode").next("tr").attr("data-id",row_id);
          $(".updatemode").remove();
        }else{
          $('#pro_table').append(row);
        }
        $(inputRow).find("input:text").val('');
        calcoltotal();
        $(inputRow).find("select.fabric_type option:selected").focus();
    });//End appending new row
});

//edit table row in update popup
function editMe(elm){
    var thisRow,row,fabric_type,variety,thaan,quantity,rate,amount;
    thisRow = $(elm).parent().parent();

    $(".updatemode").removeClass('updatemode');
    $(thisRow).addClass('updatemode');

    fabric_type = $(thisRow).find('td.fabric_type').attr('data-value');
    variety      = parseInt($(thisRow).find('td.variety').attr('data-id'))||0;
    thaan        = parseFloat($(thisRow).find('td.thaan').text())||0;
    quantity     = $(thisRow).find('td.quantity').text();
    rate         = $(thisRow).find('td.rate').text();
    amount       = $(thisRow).find('td.amount').text();
    $("tr.inputRow select.fabric_type option[value='"+fabric_type+"']").prop('selected',true).parent().selectpicker('refresh');
    $("tr.inputRow select.variety").selectpicker('val',variety);
    $("tr.inputRow input.thaan").val(thaan);
    $("tr.inputRow input.quantity").val(quantity);
    $("tr.inputRow input.rate").val(rate);
    $("tr.inputRow input.amount").val(amount);

    $("tr.inputRow select.variety").trigger('change');

    $("tr.inputRow").find("div.fabric_type button").focus();
}
