/* global displayMessage */
/* global $ */
$(document).ready(function(e) {
	$("input[name=labour],input[name=overhead]").prop('readonly',true);
	initializeEvents();
	var work_in_process_id = $(".oh_id").val();
	if(work_in_process_id > 0){
		loadWorkInProcess(work_in_process_id);
	}
});

var initializeEvents = function(){
	$('select').selectpicker();
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$(".timepicker").timepicker();
    $("select.labour_account").change(function(){
        if($(this).selectpicker('val') != ''){
            $("input[name=labour]").prop('readonly',false).focus();
        }else{
            $("input[name=labour]").prop('readonly',true);
            $("input[name=labour]").val(0);
        }
        calculateColumnTotals();
    });
    $("select.overhead_account").change(function(){
        if($(this).selectpicker('val') != ''){
            $("input[name=overhead]").prop('readonly',false).focus();;
        }else{
            $("input[name=overhead]").prop('readonly',true);
            $("input[name=overhead]").val(0);
        }
        calculateColumnTotals();
    });
    
    $("select.labour_account").change();
    $("select.overhead_account").change();
	//keyEvents
    $("input[name=labour],input[name=overhead]").keyup(function(){calculateColumnTotals()});
	$("div.resultingItemSelector button").keyup(function(e){
		if(e.keyCode == 13 && $("select.resultingItemSelector option:selected").val() != ''){
			$("input[name='result_qty']").focus();
		}
	});
	
	$("input[name='result_qty']").keyup(function(e){
		if($(this).val() == 0){
			return false;
		}
		if(e.keyCode == 13 && $(this).val() != ''){
			$("div.issueItemSelector button").focus();
		}
	});
	
	$(".issueItemSelector").change(function(){
		getItemStock($(this));
	});
	$("div.issueItemSelector button").keyup(function(e){
		if(e.keyCode == 13 && $("select.issueItemSelector option:selected").val() != ''){
			$("input.quantity").focus();
		}
	});
	$("input.quantity").keyup(function(e){
		var thisQty      = $(this).val();
		var stockOs      = parseInt($(".stock_in_hand").attr('stock_os'))||0;
		var unitPrice    = parseFloat($(".unitPrice").val())||0;
		var qtyModifier  = 0;
		var selected_item_id = $("select.issueItemSelector option:selected").val();
		if($("tr.transactions").length){
            $("tr.updateMode").each(function(index, element){
				if($(this).find('td.itemName').attr('data-item-id') == selected_item_id){
                	qtyModifier -= parseInt($(this).find("td.quantity").text())||0;
				}
            });
		}
		stockOs -= qtyModifier;
		$("input.stock_in_hand").val(stockOs - thisQty);
		if(unitPrice <= 0){
			$("input.quantity").val('');
			return false;
		}else{
			$("input.gross_cost").val(thisQty*unitPrice);
		}
		if(stockOs < thisQty){
			$("input.quantity").val('');
			$("input.stock_in_hand").val($("input.stock_in_hand").attr('stock_os'));
		}
		if(e.keyCode == 13 && thisQty != ''){
			$(".stock_in_hand");
			$("input.quick_submit").focus();
		}
	});
	$("tr.totals").hover(function(){
		calculateColumnTotals();
	});
	
	$("input.quick_submit").keyup(function(e){
		if(e.keyCode == 13){
			quickSave();
		}
	});
	$(".saveProcess").click(function(){
		saveProcess();
	});
    $(window).keyup(function(e){
        if(e.altKey == true && e.keyCode == 83){
            e.preventDefault();
            $("#form").click();
            $(".saveProcess").click();
            return false;
        }
    });
};

var enterEvents = function($elm1,$elm2){
	$($elm1).keyup(function(e){
		if(e.keyCode == 13 && $($elm1).val() != ''){
			$($elm2).focus();
		}
	});
};

var clearPanelValues = function(){
	$("select.issueItemSelector").selectpicker('val','');
	$("input.quantity").val('');
	$("input.unitPrice").val('');
	$("input.gross_cost").val('');
};
var calculateColumnTotals = function(){
	var quantity = 0;
	var gross_total = 0;
	
	$("td.quantity").each(function(index, element) {
        quantity +=  parseInt($(this).text())||0;
    });
	$("td.gross_price").each(function(index, element) {
        gross_total +=  parseFloat($(this).text())||0;
    });
	$("td.quantityTotal").text(quantity);
	$("td.costTotal").text(gross_total);
    gross_total += parseFloat($("input[name=labour]").val())||0;
    gross_total += parseFloat($("input[name=overhead]").val())||0;
    $("input[name=final_cost]").val((gross_total).toFixed(2));
};
var quickSave = function(){
	var item_id    = $("select.issueItemSelector option:selected").val();
	var item_name  = $("select.issueItemSelector option:selected").text();
	var quantity   = $("input.quantity").val();
	var unitPrice   = $("input.unitPrice").val();
	var gross_cost = $("input.gross_cost").val();
	var updateMode = $(".updateMode").length;
	
	$(".quick_submit").blur();
	
	if(item_id == '' && quantity == ''){
		displayMessage('Values Are Missing!');
		return false;
	}
	
	var $theNewRow = '<tr class="alt-row calculations transactions" data-row-id="0"><td style="text-align:left;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td><td style="text-align:center;" class="quantity">'+quantity+'</td><td style="text-align:center;" class="unitPriceTd">'+unitPrice+'</td><td style="text-align:center;" class="gross_price">'+gross_cost+'</td><td style="text-align:center;"> - - - </td><td style="text-align:center;"><a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a><a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a></td></tr>';
	
	if(updateMode == 0){
	$($theNewRow).insertAfter($(".calculations").last());
	}else if(updateMode == 1){
		$(".updateMode").replaceWith($theNewRow);
		$(".updateMode").removeClass('updateMode');
	}
	clearPanelValues();
	calculateColumnTotals();
	$("div.issueItemSelector button").focus();
};

var editThisRow = function(thisElm){
	var thisRow 	= $(thisElm).parent('td').parent('tr');
	$(".updateMode").removeClass('updateMode');
	thisRow.addClass('updateMode');
	var item_name 	= thisRow.find('td.itemName').text();
	var item_id 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
	var quantity 	= thisRow.find('td.quantity').text();
	var unitPrice 	= thisRow.find('td.unitPriceTd').text();
	var subAmount 	= thisRow.find('td.gross_price').eq(3).text();
	
	$("select.issueItemSelector option[value='"+item_id+"']").prop('selected',true);
	$("select.issueItemSelector").selectpicker('refresh');
	$("input.quantity").val(quantity);
	$("input.unitPrice").val(unitPrice);
	$("input.gross_cost").val(subAmount);
	$("input.stock_in_hand").val('').attr('stock_os','');
	getItemStock($(".issueItemSelector"));
	$("div.itemSelector button").focus();
};

var saveProcess = function(){
	var oh_id		    	= $(".oh_id").val();
	var started_on_date 	= $("input[name='started_on']").val();
	var resulting_item_id  	= $("select.resultingItemSelector option:selected").val();
	var resulting_item_qty  = $("input[name='result_qty']").val();

    var labour_account 		= $("select.labour_account option:selected").val();
	var labour 			    = $("input[name='labour']").val();
    var overhead_account 	= $("select.overhead_account option:selected").val();
	var overhead 			= $("input[name='overhead']").val();
    
    if(labour_account != '' && labour == 0){
        displayMessage("Error! External Expense amount is missing.");
        return;
    }
    
    if(overhead_account != '' && overhead == 0){
        displayMessage("Error! Internal Expense amount is missing.");
        return;
    }
    
	var misc_expense 		= 0;

	if(resulting_item_id <= 0){
		displayMessage('Please Select Item To Produce!');
		return false;
	}

	if($("tr.transactions").length == 0){
		displayMessage('No Transaction Found!');
		return false;
	}
	
	var jSonString = '{';
	
	$("tr.transactions").each(function(index, element) {
		var rowId       = $(this).attr('data-row-id');
		var thisRow     = $(this);
		var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
		var quantity 	= thisRow.find('td').eq(1).text();
		var cost_price  = parseFloat(thisRow.find('td.unitPriceTd').text())||0;

		if(item_id != '' && quantity != 0){
			if(index > 0){
				jSonString += ',';
			}
			jSonString += '"'+index+'":';
			jSonString += '{';
			jSonString += '"row_id":"'+rowId+'",'+'"item_id":"'+item_id+'","quantity":"'+quantity+'","cost_price":"'+cost_price+'"';
			jSonString += '}';
		}
	});
	jSonString += '}';
    $(".saveProcess").hide();
	$.post("db/saveWorkInProcess.php",{oh_id:oh_id, 
								  started_on_date	 : started_on_date,
								  resulting_item_id	 : resulting_item_id,
								  resulting_item_qty : resulting_item_qty,
                                  labour_account:labour_account,
								  labour:labour,
                                  overhead_account:overhead_account,
								  overhead:overhead,
								  misc_expense:misc_expense,
								  jSonString:jSonString},function(data){
									    $(".saveProcess").show();
										data = $.parseJSON(data);
										displayMessage(data['MSG']);
										if(data['ID'] > 0){
											window.location.href = 'wip-details.php?wid='+data['ID'];
										}
	});
};

var getItemStock = function($itemSelector){
	var item_id = $itemSelector.val();
	if(item_id != ''){
		$.post('db/get-item-stock.php',{item_id:item_id},function(data){
			data = $.parseJSON(data);
			$("input.stock_in_hand").val(data['QTY']);
			$("input.stock_in_hand").attr('stock_os',data['QTY']);
			$("input.unitPrice").val(data['PRICE']);
			$("input.quantity").focus();
		});
	}
};

var loadWorkInProcess = function(oh_id){
	if(oh_id > 0){
		//do stuff
	}
};