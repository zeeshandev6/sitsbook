/* global displayToastMessage */
/* global displayMessage */
/* global $ */
var balance_recovered = "";
var stock_check       = 'Y';
var print_method      = '';
var save_only         = false;
var redirect_method   = 'U';
var use_cartons       = '';
$(document).ready(function(){
	$("#addCustomerModal").on("shown.bs.modal",function(){
		$("button.addNewCustomer").click(function(){
			var customer_name = $("#addCustomerModal input[name=customer_name]").val();
			var modal_sectors = $("#addCustomerModal select.modal_sectors").val();
			$(this).prop('disabled',true);
			$.post("customer-detail.php", { addCustomer: true,return_json:true, title: customer_name, sector: modal_sectors},function(data){
				data = JSON.parse(data);
				if (data['DONE'] == 'Y'){
					displayMessage('Created New Customer Account.');
					$(this).prop('disabled',false);
					$("#addCustomerModal input[name=customer_name]").val('');
					$("#addCustomerModal select.modal_sectors").val();
					$("#addCustomerModal select.modal_sectors").selectpicker("refresh");
					$.get("?",{},function(html_data){
						$("select.supplierSelector").html($(html_data).find("select.supplierSelector").html());
						$("select.supplierSelector").selectpicker("refresh");
					});
				}
				$("[data-dismiss='modal']").trigger("click");
			});
		});
	});
	$("input.total_discount").prop("readonly",true);
	print_method   = $("input.print_method").val();
	use_cartons    = $("input.use_cartons").val();
	stock_check    = $("input.stock_check").val();
	stock_check     = (stock_check == 'Y')?true:false;
	balance_recovered = ($("input.recovered_balance").is(":checked"))?"Y":"N";
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.numericFloatOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
					||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
					|| (e.keyCode == 65 && e.ctrlKey === true)
					|| (e.keyCode >= 35 && e.keyCode <= 39)){
						return true;
					}else{
						if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
							e.preventDefault();
						}
					}
				});
			};
			$("select.supplierSelector").change(function(){
				getAccountBalance();
			});
			$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
			$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
			$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
			$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
				function() {
					$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
					$(this).addClass('current'); // Add class "current" to clicked tab
					var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
					$(currentTab).siblings().hide(); // Hide all content divs
					$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
					return false;
				}
			);

			//Close button:

			$(".close").click(
				function () {
					$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);
		// Alternating table rows:
		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
		// Check all checkboxes when the one in a table head is checked:
		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.sumColumn = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseInt($(this).text());
			});
			$(insertToFeild).text(sumAll);
		};
		$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseFloat($(this).text())||0;
			});
			sumAll = Math.round(sumAll*100)/100;
			$(insertToFeild).text((sumAll).toFixed(2));
		};
		$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
			$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
		};
		$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
			$(this).keyup(function(e){
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var productVal = Math.round((thisVal*thatVal)*100)/100;
				$(writeProductToElm).val(productVal);
			});
		};
		$.fn.deleteMainRow = function(file){
			var idValue    = $(this).attr("do");
			var currentRow = $(this).parent().parent().parent().parent().parent();
			var billNumbr  = currentRow.find('td').first().text();
			$("#xfade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#xfade").fadeIn();
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$("#popUpDel").height()/2;
			win_width = win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel .confirm").text("Are Sure you Want To Delete Bill # "+billNumbr+"?");
			$("#popUpDel").slideDown();
			$(".dodelete").click(function(){
				$.post(file, {cid : idValue}, function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("tr[data-row-id='"+idValue+"']").slideUp();
						setTimeout(function(){
							$("#popUpDel").slideUp(function(){
								$(this).remove();
							});
							$("#xfade").fadeOut();
						},1000);
					}
					$("#popUpDel .confirm").text(data['MSG']);
					$(".nodelete,.dodelete").hide();
				});
			});
		};

		$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
			$(this).keyup(function(e){
				var taxRate = $("input.taxRate").val();
				var taxType   = ($(".taxType").is(":checked"))?"I":"E";
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var amount = Math.round((thisVal*thatVal)*100)/100;
				if(taxRate > 0){
					if(taxType == 'I'){
						taxAmount = amount*(taxRate/100);
						amount -= taxAmount;
					}else if(taxType == 'E'){
						taxAmount = amount*(taxRate/100);
					}

				}else{
					taxAmount = 0;
				}
				taxAmount = Math.round(taxAmount*100)/100;
				amount = Math.round(amount*100)/100;
				var finalAmount = Math.round((amount+taxAmount)*100)/100;
				$(taxAmountElm).val(taxAmount);
				$(writeProductToElm).val(finalAmount);
				$(writeProductToElmNoTax).val(amount);
			});
		};
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
		//inventory.php end
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};
		$.fn.getItemDetails = function(){
			var item_id = $(".itemSelector option:selected").val();
			var row_id  = 0;
			$("input.unitPrice").attr('data-title','');
			if(item_id != ''){
				$.post('db/get-item-details.php',{s_item_id:item_id,row_id:0},function(data){
					data = $.parseJSON(data);
					var itemStock = parseInt(data['STOCK'])||0;
					var itemPrice = data['S_PRICE'];
					var itemDisc  = data['DISCOUNT'];
					var itemCosta = data['AVG_COSTA']?data['AVG_COSTA']:0;
					$('input.dozen_price').val(data['DOZEN_RATE']);
					$('input.qty_carton').val(data['QTY_CARTON']);
					$('input.rate_carton').val(data['RATE_CARTON']);
					itemCosta     = parseFloat(itemCosta)||0;
					var this_sold = 0;
					$("td[data-type='I'][data-item-id='"+item_id+"']").each(function(){
						if(!$(this).parent().hasClass("updateMode")){
							this_sold -= parseInt($(this).parent().find("td.quantity").text())||0;
							this_sold -= ((parseInt($(this).parent().find("td.cartons").text())||0)*(parseInt($(this).parent().find("td.cartons").attr('data-qty'))||0));
						}
					});
					if($(".updateMode").length){
						this_sold += parseFloat($(".updateMode").find("td.quantity").text())||0;
					}
					itemDisc = (itemDisc > 0)?itemDisc:"";
					itemPrice = (itemPrice > 0)?itemPrice:"";
					if($(".updateMode").length == 0){
						$("input.discount").val(itemDisc);
						$("input.unitPrice").val(itemPrice);
						$("input.unitPrice").attr('data-title',"Avg.Cost : "+(itemCosta).toFixed(2));
					}else{
						$("input.unitPrice").attr('data-title','');
					}
					itemStock += this_sold;
					$("input.inStock").attr('thestock',itemStock).val(itemStock);
				});
			}
		};
		$.fn.quickSave = function(){
			stockOs();
			var item_id       = $("select.itemSelector option:selected").val();
			var item_type     = $("select.itemSelector option:selected").attr('data-type');
			var item_name     = $("select.itemSelector option:selected").text();
			var cartons 	  = parseFloat($('input.cartons').val()) || 0;
			var rate_carton   = parseFloat($('input.rate_carton').val()) || 0;
			var qty_carton    = parseFloat($('input.qty_carton').val()) || 0;
			var dozen 	      = parseFloat($("input.dozen").val())||0;
			var dozen_price = parseFloat($("input.dozen_price").val()) || 0;
			var quantity      = parseFloat($("input.quantity").val())||0;
			var unitPrice     = $("input.unitPrice").val();
			var discount      = parseFloat($("input.discount").val()) || 0;
			var taxRate       = parseFloat($("input.taxRate").val()) || 0;
			var totalAmount   = $("input.totalAmount").val();
			var discount_type = $("input.discount_type:checked").val();
			var taxType       = ($(".taxType").is(":checked"))?"I":"E";

			var inStock = $("input.inStock").val();

			if(item_type == 'S'){
				quantity = 1;
			}

			$("input.discount").attr('data-amount','');
			$(this).blur();

			if ((quantity + cartons + dozen) == 0){
				displayMessage('Selected Quantity is Zero.');
				$("input.cartons").focus();
				return false;
			}
			var subAmount = 0;
			var discountPerCentage = 0;
			subAmount     = quantity*unitPrice;
			subAmount    += cartons*rate_carton;
			subAmount += (dozen * dozen_price);
			subAmount = Math.round(subAmount*100)/100;
			if($("input.individual_discount").val()=='Y'){
				if(discount_type == 'R'){
					discountPerCentage = discount;
				}else if(discount_type == 'P'){
					discountPerCentage = subAmount*(discount/100);
				}
				subAmount -= discountPerCentage;
				subAmount = Math.round(subAmount*100)/100;
			}
			var taxAmount = 0;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = subAmount*(taxRate/100);
				}else if(taxType == 'E'){
					taxAmount = subAmount*(taxRate/100);
				}
			}
			if(item_type == 'S'){

			}
			var updateMode    = $(".updateMode").length;
			var update_row_id = 0;
			if(updateMode){
				update_row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
			}
			if(item_id > 0){
				var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="'+update_row_id+'">'
				+ '<td style="text-align:center;" class="itemName" data-type="'+item_type+'" data-item-id="'+item_id+'">'+item_name+'</td>';
				$theNewRow += '<td style="text-align:center;" class="cartons" data-qty="' + qty_carton + '">' + cartons + '</td>';
				$theNewRow += '<td style="text-align:center;" class="rate_carton">' + rate_carton + '</td>';
				$theNewRow += '<td style="text-align:center;" class="dozen">' + dozen + '</td>';
				$theNewRow += '<td style="text-align:center;" class="dozen_price">' + dozen_price + '</td>';
				$theNewRow += '<td style="text-align:center;" class="quantity">'+quantity+'</td>'
						   +  '<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
				if($("input.individual_discount").val()=='Y'){
					$theNewRow	+= '<td class="discount text-center">'+discount+'</td>';
				}
				if($("input.use_taxes").val()=='Y'){
					$theNewRow += '<td style="text-align:center;" class="taxRate"  tax-amount="'+taxAmount+'">'+taxRate+'</td>';
				}
				$theNewRow += '<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
				+ '<td style="text-align:center;" data-rem-stock="'+inStock+'"> - - - </td>'
				+ '<td style="text-align:center;">'
				+'<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>'
				+'<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
				+ '</td>'
				+ '</tr>';
				if(updateMode == 0){
					$($theNewRow).insertBefore($(".calculations").first());
				}else if(updateMode == 1){
					$(".updateMode").replaceWith($theNewRow);
				}
				$(this).clearPanelValues();
				sum_tax_amount();
				$(this).calculateColumnTotals();
				$(".itemSelector").parent().find(".dropdown-toggle").focus();
			}else{
				displayMessage('Values Missing!');
			}
		};
		$.fn.quickScan = function(){
			var append_row =($("input.scanner-append").val() == 'Y')?true:false;
			var barcode  = $("input.barcode_input").val();
			$("input.barcode_input").val('');
			if(barcode == ''){
				return false;
			}
			$("input.barcode_input").css({'border':'1px solid #CCCCCC'});
			$.get('db/get-item-details.php',{barcode:barcode},function(data){
				if(data == ''||data==null){
					$("input.barcode_input").css({'border':'1px solid red'});
					return false;
				}
				data = $.parseJSON(data);
				var item_id   = data['ID'];
				var item_name = data['NAME'];
				var quantity  = 1;
				var quantity_sold = 0;
				if($("td[data-item-id="+item_id+"][data-type=I]").length){
					$("td[data-item-id='"+item_id+"'][data-type=I]").each(function(i,e){
						quantity_sold = (parseFloat(quantity_sold)||0) + (parseFloat($(this).parent().find(".quantity").text())||0);
					});
				}
				quantity_sold       = (parseFloat(quantity_sold)||0) + (parseFloat(quantity)||0);
				if(append_row == false){
					quantity = quantity_sold;
				}
				var unitPrice       = data['SALE_PRICE'];
				var discount  		= 0;
				var discount_amount = 0;
				var subAmount   = (quantity*unitPrice).toFixed(2);
				var taxRate     = 0;
				var taxAmount   = 0;
				var totalAmount = (quantity*unitPrice).toFixed(2);
				var available   = data['STOCK_QTY'] - quantity_sold;
				if(available<0){
					displayMessage('Not Enought Stock!');
					return false;
				}
				var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="0">'
				+'<td style="text-align:center;" class="itemName" data-type="I" data-item-id="'+item_id+'">'+item_name+'</td>'
				+'<td style="text-align:center;" class="quantity">'+quantity+'</td>'
				+'<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
				if($("input.individual_discount").val()=='Y'){
					$theNewRow	+= '<td class="discount text-center">'+discount+'</td>';
				}
				if($("input.use_taxes").val()=='Y'){
					$theNewRow	+= '<td style="text-align:center;" class="taxRate"  tax-amount="'+taxAmount+'">'+taxRate+'</td>';
				}
				$theNewRow +=''
				+'<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
				+'<td style="text-align:center;"> - - - </td>'
				+'<td style="text-align:center;">'
				+'<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>'
				+'<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
				+'</td>'
				+'</tr>';
				if(append_row){
					$($theNewRow).insertAfter($(".calculations").last());
				}else{
					if($("td[data-item-id="+item_id+"][data-type='I']").length){
						$("td[data-item-id="+item_id+"][data-type='I']").parent('tr').replaceWith($theNewRow);
					}else{
						$($theNewRow).insertAfter($(".calculations").last());
					}
				}
				$(this).calculateColumnTotals();
				$("input.barcode_input").focus();
			});
		};
		$.fn.calculateColumnTotals = function(){
			var discount_type    = $("input.discount_type:checked").val();
			var transaction_type = $("input[name=radiog_dark]:checked").val();
			var trade_offer = parseFloat($("input.trade_offer").val())||0;
			$(this).sumColumn("td.quantity","td.qtyTotal");
			$(this).sumColumn("td.cartons","td.carton_total");
			$(this).sumColumn("td.dozen", "td.dozen_total");
			$(this).sumColumnFloat("td.unitPrice","td.price_total");
			$(this).sumColumnFloat("td.subAmount","td.amountSub");
			var disk = 0, rate, crt, pcrt, dzn, dznrte, rte, qty, sub, dit;
			$("tr.transactions").each(function(){
				crt    = parseFloat($(this).find('td.cartons').text()) || 0;
				pcrt   = parseFloat($(this).find('td.cartons').attr('data-qty')) || 0;
				rte    = parseFloat($(this).find('td.rate_carton').text()) || 0;
				dzn    = parseFloat($(this).find('td.dozen').text()) || 0;
				dznrte = parseFloat($(this).find('td.dozen_price').text()) || 0;
				qty    = parseFloat($(this).find('td.quantity').text()) || 0;
				rate   = parseFloat($(this).find('td.unitPrice').text()) || 0;
				dit    = parseFloat($(this).find('td.discount').text()) || 0;
				if(discount_type == 'P'){
					dit = parseFloat(((((crt * rte) + (dzn * dznrte) + (qty * rate)) * dit) / 100)) || 0;
				}
				disk += dit;
				$(this).find('td.totalAmount').text(((crt * rte) + (dzn * dznrte) + (qty * rate)) - dit);
			});
			$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
			var amountTotal = parseFloat($("td.amountTotal").text())||0;

			$("input.total_discount").val(disk);
			amountTotal += parseFloat($("input.inv_charges").val())||0;
			amountTotal -= trade_offer;
			$("input.grand_total").val((Math.round(amountTotal)).toFixed(2));

			var received    = parseFloat($("input.received_cash").val())||0;
			var grand_total = $("input.grand_total").val();
			var returnin    = received - grand_total;

			if(transaction_type == 'C'){
				if(received >= grand_total){
					$("input.change_return").val((returnin).toFixed(2));
					$("input.remaining_amount").val("");
				}else{
					$("input.remaining_amount").val("");
					$("input.change_return").val("");
					$("input.received_cash").val("");
				}
			}else{
				if(received < grand_total){
					returnin *= -1;
					$("input.remaining_amount").val((returnin).toFixed(2));
				}else{
					$("input.remaining_amount").val("");
				}
				if(received > grand_total){
					$("input.change_return").val((returnin).toFixed(2));
				}else{
					$("input.change_return").val("");
				}
			}
			var rec_amount = $("input.recovery_amount").val();
			var chg_amount = $("input.change_return").val();

			if(balance_recovered == 'Y'){
				$("input.recovery_amount").val(chg_amount);
				$("input.change_return").val('');
			}else{
				$("input.recovery_amount").val('');
			}
		};
		$.fn.addToThis = function(that_amount){
			var this_amount = parseFloat($(this).val())||0;
			$(this).va(that_amount+this_amount);
		};
		$.fn.calculateRowTotal = function(){
			var item_type, taxRate, taxType, carton_sub_amount, dozen_sub_amount, thisVal, thatVal;
			item_type = $('select.itemSelector option:selected').attr('data-type');
			taxRate = parseFloat($('input.taxRate').val()) || 0;
			taxType = ($('.taxType').is(':checked')) ? 'I' : 'E';
			carton_sub_amount = parseFloat($('input.carton_sub_amount').val()) || 0;
			dozen_sub_amount = parseFloat($('input.dozen_sub_amount').val()) || 0;
			thisVal = parseFloat($('input.quantity').val()) || 0;
			thatVal = parseFloat($('input.unitPrice').val()) || 0;
			if (item_type == 'I') {
				var amount = Math.round((thisVal * thatVal) * 100) / 100;
			} else {
				var amount = thatVal;
			}
			amount += carton_sub_amount;
			amount += dozen_sub_amount;
			var discountAvail = parseFloat($('input.discount').val()) || 0;
			var discount_type = $('input.discount_type:checked').val();
			var discountPerCentage = 0;
			amount = Math.round(Math.round(amount * 100) / 100);
			if ($('input.individual_discount').val() == 'Y') {
				if (discount_type == 'R') {
					discountPerCentage = discountAvail;
				} else if (discount_type == 'P') {
					discountAvail = Math.round(discountAvail * 100) / 100;
					discountPerCentage = amount * (discountAvail / 100);
					discountPerCentage = Math.round(discountPerCentage * 100) / 100;
				}
				$('input.discount').attr('data-amount', discountPerCentage);
				amount -= discountPerCentage;
				amount = Math.round(Math.round(amount * 100) / 100);
			}

			var taxAmount = 0;
			if (taxRate > 0) {
				if (taxType == 'I') {
					taxAmount = amount * (taxRate / 100);
				} else if (taxType == 'E') {
					taxAmount = amount * (taxRate / 100);
				}
			}
			if (taxType == 'I') {
				amount -= taxAmount;
			} else {
				amount += taxAmount;
			}
			$('input.totalAmount').val((Math.round(amount)).toFixed(2));
		};
		$.fn.updateStockInHand = function(){
			var stockOfBill = 0;
			$("td.quantity").each(function(index, element) {
				stockOfBill += parseInt($(this).text())||0;
			});
			stockOfBill += parseInt($("input.quantity").val())||0;
		};

		$.fn.clearPanelValues = function(){
			$(".itemSelector option").prop('selected',false);
			$(".itemSelector").find("option").first().prop('selected',true);
			$(".itemSelector").selectpicker('refresh');
			$("input.cartons").val('');
			$("input.rate_carton").val('');
			$("input.dozen").val('');
			$("input.dozen_price").val('');
			$("input.qty_carton").val('');
			$("input.quantity").val('');
			$("input.unitPrice").val('');
			$("input.discount").val('');
			$("input.taxRate").val('');
			$("input.subAmount").val('');
			$("input.taxAmount").val('');
			$("input.totalAmount").val('');
			$("input.inStock").val('').attr('thestock','');
		};
	});
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var showDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Record is in edit Mode!');
			return false;
		}

		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").hide();
			setTimeout(function(){
				$("#fade").fadeOut();
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
				});
				clickedDel.parent().parent().remove();
				$(this).calculateColumnTotals();
			},800);
		});
		$(".close_popup").click(function(){
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('fast');
		});
	};
	var validatePhone = function(txtPhone) {
		var a = $(txtPhone).val();
		var filter = /^[0-9-+]+$/;
		if(a[0] == 0){
			a = a.substring(1,11);
		}
		if(a[0] != 3){
			return false;
		}
		if(a.length != 10){
			return false;
		}
		if(filter.test(a)) {
			return a;
		}else{
			return false;
		}
	};
	var sum_tax_amount = function(){
		var tax = 0;
		$("td.taxRate").each(function(){
			tax += parseFloat($(this).attr('tax-amount'))||0;
		});
		$(".whole_tax").val((tax).toFixed(2));
	}
	var stockOs = function(){
		var item_type = $('select.itemSelector option:selected').attr('data-type');
		var thisQty = parseInt($('input.quantity').val()) || 0;
		thisQty += ((parseInt($('input.cartons').val()) || 0) * (parseInt($('input.qty_carton').val()) || 0));
		thisQty += ((parseInt($('input.dozen').val()) || 0) * 12);
		var inStock = parseInt($('input.inStock').attr('thestock')) || 0;
		var selectedItem = $('select.itemSelector option:selected').val();
		var NewStock = inStock - thisQty;
		if (item_type == 'S') {
			return
		}
		if (stock_check) {
			if (thisQty <= inStock) {
				$('input.inStock').val(NewStock);
			} else {
				$('input.quantity').val('');
			}
		} else {
			$('input.inStock').val(NewStock);
		}
	};
	var stockOsQuick = function(elm,price,inStock){
		var thisQty = parseInt($(elm).val())||0;
		var NewStock = inStock - thisQty;
		if(stock_check){
			if(thisQty <= inStock){
				$(elm).parent().parent().find("td.subAmount").text((thisQty*price).toFixed(2));
				$(elm).parent().parent().find("td.totalAmount").text((thisQty*price).toFixed(2));
			}else{
				$(elm).val('');
			}
		}else{
			$(elm).parent().parent().find("td.subAmount").text((thisQty*price).toFixed(2));
			$(elm).parent().parent().find("td.totalAmount").text((thisQty*price).toFixed(2));
		}
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	= thisRow.find('td.itemName').text();
		var item_id 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
		var cartons 	= thisRow.find('td.cartons').text();
		var per_carton 	= thisRow.find('td.per_carton').text();
		var dozen     	= thisRow.find('td.dozen').text();
		var dozen_price = thisRow.find('td.dozen_price').text();
		var quantity 	= thisRow.find('td.quantity').text();
		var unitPrice 	= thisRow.find('td.unitPrice').text();
		var discount 	= thisRow.find('td.discount').text();
		var taxRate 	= parseFloat(thisRow.find('td.taxRate').text())||0;
		var taxAmount 	= parseFloat(thisRow.find('td.taxRate').attr('tax-amount'))||0;
		var totalAmount = thisRow.find('td.totalAmount').text();

		$("select.itemSelector option[data-type='I'][value='"+item_id+"']").prop('selected',true);
		$("select.itemSelector").selectpicker('refresh');
		$("input.cartons").val(cartons);
		$("input.per_carton").val(per_carton);
		$("input.dozen").val(dozen);
		$("input.dozen_price").val(dozen_price);
		$("input.quantity").val(quantity);
		$("input.unitPrice").val(unitPrice);
		$("input.discount").val(discount);

		$("input.taxRate").val(taxRate);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(totalAmount);
		$("input.inStock").val('').attr('thestock','');
		$("div.itemSelector button").focus();
	};

	var saveSale = function(){
		$("select.supplierSelector").selectpicker("refresh");
		var sale_id		        = $("input.sale_id").val();
		var user_id 	        = $("select.user_id").length>0?$("select.user_id option:selected").val():"";
		var order_taker         = $("select.order_taker").length>0?$("select.order_taker option:selected").val():"";
		var saleDate 	        = $("input[name=rDate]").val();
		var order_date          = $("input[name=order_date]").val();
		var billNum  	        = $("input[name='billNum']").val();
		var po_number  	        = $("input[name='po_number']").val();
		var subject		        = ($("input[name='subject']").length)?$("input[name='subject']").val():"";
		var discount_unit       = parseFloat($(".whole_discount").val())||0;
		var discount_type       = $("input.discount_type:checked").val();
		var total_discount      = parseFloat($(".total_discount").val())||0;
		var supplierCode        = $("select.supplierSelector option:selected").val();
		var customer_name       = $("input.supplier_name").val();
		var inv_charges         = parseFloat($("input.inv_charges").val())||0;
		var inv_notes           = $("textarea.inv_notes").val();
		var tax_invoice         = ($("input.tax_invoice:checked").length>0)?"Y":"N";
		var registered_tax      = ($("input.registered_tax:checked").length>0)?"Y":"N";
		var tax_bill_num        = $("input[name=taxBillNum]").val();
		var trade_offer 		= parseFloat($("input.trade_offer").val()) || 0;
		var transaction_type  	= $("input[name=radiog_dark]:checked").val();
		var grand_total  	  	= parseFloat($("input.grand_total").val())||0;
		var received_cash       = parseFloat($("input.received_cash").val())|0;
		var change_return       = $("input.change_return").val();
		var recovery_amount     = $("input.recovery_amount").val();
		var remaining_amount    = $("input.remaining_amount").val();

		var out_of_stock = 0;
		$("td[data-rem-stock]").each(function(){
			if((parseFloat($(this).attr('data-rem-stock'))||0) < 0){
				out_of_stock += 1;
			}
		});

		if(confirm("There "+out_of_stock+" items out of stock, do you want to continue?")){

			var customer_mobile   = '';

			if(transaction_type == 'C' && grand_total > received_cash){
				$("input.received_cash").addClass("error");
				$("input.received_cash").focus();
				return false;
			}
			$("input.received_cash").removeClass("error");
			if(supplierCode == ''){
				displayMessage('Please select an Account!');
				$("div.supplierSelector button").addClass("btn-warning");
				$("div.supplierSelector button").focus();
				return false;
			}
			if($(".customer_mobile").length){
				customer_mobile = $(".customer_mobile").val();
				if($(".customer_mobile").val() != ''){
					if($(".customer_mobile").val().substr(0,1) != 0 || ($(".customer_mobile").val().length > 1 && $(".customer_mobile").val().substr(1,1) != 3) || $(".customer_mobile").val().length != 11){
						displayMessage('Please Enter a valid Moblie Number!');
						return false;
					}
				}
			}
			if($("tr.transactions").length == 0){
				displayMessage('No Transaction Existed, Bill Can not be Saved!');
				$("div.itemSelector button").addClass("btn-warning");
				$("div.itemSelector button").focus();
				return false;
			}
			if(supplierCode == ''){
				displayMessage('Customer Not Selected!');
				return false;
			}
			$(".save_sale").hide();
			$("#xfade").fadeIn();
			var trans_rows = {};
			$("tr.transactions").each(function(index, element){
				trans_rows[index] = {};

				var thisRow = $(this);

				trans_rows[index].row_id  		= thisRow.attr('data-row-id');
				trans_rows[index].item_id 	 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
				trans_rows[index].item_type 	= thisRow.find('td.itemName').attr('data-type');
				trans_rows[index].cartons 	 	= thisRow.find('td.cartons').text();
				trans_rows[index].qty_carton 	= parseFloat(thisRow.find('td.cartons').attr('data-qty')) || 0;
				trans_rows[index].rate_carton  	= thisRow.find('td.rate_carton').text();
				trans_rows[index].dozen 		= thisRow.find('td.dozen').text();
				trans_rows[index].dozen_price 	= thisRow.find('td.dozen_price').text();
				trans_rows[index].quantity 	 	= thisRow.find('td.quantity').text();
				trans_rows[index].unitPrice 	= thisRow.find('td.unitPrice').text();
				trans_rows[index].discount 	 	= parseFloat(thisRow.find('td.discount').text()) || 0;
				trans_rows[index].subAmount 	= parseFloat(thisRow.find('td.unitPrice').attr('sub-amount'))||0;
				trans_rows[index].taxRate 	 	= parseFloat(thisRow.find('td.taxRate').text())||0;
				trans_rows[index].taxAmount     = parseFloat(thisRow.find('td.taxRate').attr('tax-amount'))||0;
				trans_rows[index].totalAmount   = thisRow.find('td.totalAmount').text();
			});
			trans_rows = JSON.stringify(trans_rows);
			$.post("db/saveDistSale.php",{sale_id:sale_id,
				user_id:user_id,
				order_taker:order_taker,
				saleDate:saleDate,
				order_date:order_date,
				customer_mobile:customer_mobile,
				billNum:billNum,
				po_number:po_number,
				supplierCode:supplierCode,
				customer_name:customer_name,
				inv_charges:inv_charges,
				inv_notes:inv_notes,
				subject:subject,
				bill_amount:grand_total,
				received_cash:received_cash,
				balance_recovered:balance_recovered,
				recovery_amount:recovery_amount,
				change_return:change_return,
				remaining_amount:remaining_amount,
				trade_offer: trade_offer,
				discount:discount_unit,
				discount_type:discount_type,
				total_discount:total_discount,
				tax_invoice:tax_invoice,
				registered_tax:registered_tax,
				tax_bill_num:tax_bill_num,
				transaction_type:transaction_type,
				trans_rows:trans_rows},function(data){
					data = $.parseJSON(data);
					if(data['ID'] > 0){
						var msg_type = '';
						if(sale_id == 0){
							msg_type = "&saved";
						}else{
							msg_type = "&updated";
						}
						var print_opt = '';
						if($(".invoice-type").val()=='S'){
							print_opt = "&print"
						}else if(print_method=='Y'){
							print_opt = "&print"
						}
						var the_id = "id="+data['ID'];
						if(redirect_method == 'N'){
							the_id = 'method='+data['ID'];
						}
						if(save_only){
							window.location.href = 'dist-sale-details.php?'+the_id+msg_type;
						}else{
							window.location.href = 'dist-sale-details.php?'+the_id+msg_type+print_opt;
						}
					}else{
						displayMessage(data['MSG']);
						$(".save_sale").show();
						$("#xfade").fadeOut();
					}
				});
			}
		};
		var barcode_scan_for_sale = function(){
			var barcode = $(".barcode_input").val();
			$(".barcode_input").val('').blur();
			if(barcode == ''){
				$(".barcode_input").focus();
				return false;
			}
			var tr = '';
			$.get('db/scan_item.php',{b_code:barcode},function(data){
				if(data == ''){
					displayMessage('Item Does Not Exist!');
					return false;
				}
				var item_details = $.parseJSON(data);
				if(item_details['OK'] == 'N'){
					displayMessage(item_details['MSG']);
					return false;
				}
				if($("*[data-bi='"+item_details['BI']+"']").length){
					displayMessage('Item Already Scanned!');
					return false;
				}
				tr = '<tr class="alt-row calculations transactions dynamix" data-row-id="0" data-bi="'+item_details['BI']+'">';
				tr += '<td style="text-align:left;" class="itemName" data-item-id="'+item_details['ID']+'">'+item_details['NAME']+'</td>';
				tr += '<td style="text-align:center;" class="quantity">1</td>';
				tr += '<td style="text-align:center;" class="unitPrice">'+item_details['SALE_PRICE']+'</td>';
				tr += '<td style="text-align:center;"><a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a></td>';
				tr += '</tr>';
				$(tr).insertAfter($('.calculations').last());
				tr = '';
				$(this).calculateColumnTotals();
				$(".barcode_input").focus();
			});
		};
		var check_sms_service = function(){
			$.get('db/check-sms-service.php',{},function(data){
				if(data == 'N'){
					displayMessage('Sms Service Is Not Active!');
					$("input[name='mobile_no']").css({'border':'1px solid red'});
				}else if(data == 'Y'){
					$("input[name='mobile_no']").css({'border':'1px solid #008000'});
				}
			});
		};
		var save_mobile_sale = function(){
			var sale_id		 = $(".sale_id").val();
			var saleDate = $(".datepicker").val();
			var billNum = $("input[name='billNum']").val();
			var supplier_name = $(".supplier_name").val();
			var supplierCode = $("select.supplierSelector option:selected").val();
			var mobile_no = '';
			if($("input[name='mobile_no']").val() != ''){
				mobile_no = validatePhone("input[name='mobile_no']");
				if(validatePhone("input[name='mobile_no']") == false){
					displayMessage('Please Enter a valid Moblie Number!');
					return false;
				}
			}
			if($("tr.transactions").length == 0){
				displayMessage('No Transaction Existed, Bill Can not be Saved!');
				return false;
			}
			if(supplierCode == ''){
				displayMessage('Customer Not Selected!');
				return false;
			}
			var jSonString = '{';
			$("tr.transactions").each(function(index, element) {
				var rowId       = parseInt($(this).attr('data-row-id'))||0;
				var bi			= parseInt($(this).attr('data-bi'))||0;
				var thisRow     = $(this);
				var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
				var quantity 	= parseInt(thisRow.find('td').eq(1).text())||0;
				var unitPrice 	= parseInt(thisRow.find('td').eq(2).text())||0;
				var discount 	= 0;
				var subAmount 	= quantity*unitPrice;
				var taxRate 	= 0;
				var taxAmount   = 0;
				var totalAmount = subAmount;
				if(item_id != '' && quantity != 0){
					if(index > 0){
						jSonString += ',';
					}
					jSonString += '"'+index+'":';
					jSonString += '{';
					jSonString += '"row_id":"'+rowId+'",'+'"row_bi":"'+bi+'","item_id":"'+item_id+'","quantity":"'+quantity+'",';
					jSonString += '"unitPrice":"'+unitPrice+'","discount":"'+discount+'","subAmount":"'+subAmount+'",';
					jSonString += '"taxRate":"'+taxRate+'","taxAmount":"'+taxAmount+'",';
					jSonString += '"totalAmount":"'+totalAmount+'"';
					jSonString += '}';
				}
			});

			jSonString += '}';
			$.post("db/saveBarcodeSale.php",{sale_id:sale_id,
				saleDate:saleDate,
				billNum:billNum,
				supplierCode:supplierCode,
				supplier_name:supplier_name,
				mobile_no:mobile_no,
				jSonString:jSonString},function(data){
					data = $.parseJSON(data);
					if(data['ID'] > 0){
						window.location.href = 'sales-invoice.php?id='+data['ID'];
					}else{
						displayMessage(data['MSG']);
					}
				});
			};
			var makeItCash = function(checker){
				var what = $(checker).is(":checked");
				var this_val = $(checker).val();
				$(".balance-of-customer").hide();
				$(".balance-of-customer input").val(0);
				var current_val = $("select.supplierSelector option:selected").val();
				if(this_val=='A'){
					$("select.supplierSelector option[value^='010104']").prop('disabled',false).prop('selected',false);
					$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
					$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
					if((current_val).substring(0,6) == '010104'){
						$("select.supplierSelector option[value='"+current_val+"']").prop("selected",true);
					}
					$("select.supplierSelector").selectpicker('refresh');
					$("input[name='supplier_name']").val('').hide();
					getAccountBalance();
					$(".balance-of-customer").show();
				}else if(this_val=='B'){
					$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
					$("select.supplierSelector option[value^='010102']").prop('disabled',false).prop('selected',false);
					$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
					if((current_val).substring(0,6) == '010102'){
						$("select.supplierSelector option[value='"+current_val+"']").prop("selected",true);
					}
					$("select.supplierSelector").selectpicker('refresh');
					$("input[name='supplier_name']").attr('placeholder','Card Number');
					$("input[name='supplier_name']").show().focus();
				}else if(this_val=='C'){
					$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
					$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
					$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
					if((current_val).substring(0,6) == '010101'){
						$("select.supplierSelector option[value='"+current_val+"']").prop("selected",true);
					}
					$("select.supplierSelector").selectpicker('refresh');
					$("input[name='supplier_name']").attr('placeholder','Customer Name');
					$("input[name='supplier_name']").show().focus();
				}
			};
			var hidePopUpBox = function(){
				$("#popUpBox").css({'left':'-=500px'});
				$("#popUpBox").css({'transform':'scaleY(0.0)'});
				$("#xfade").fadeOut();
				$("#popUpBox").remove();
			};
			var add_supplier = function(){
				var code_type = $("input[name='radiog_dark']:checked").val();
				if(code_type == 'C'){
					return false;
				}
				var url,captions,args = {};
				if(code_type == 'B'){
					url = 'add-bank-detail.php'
					captions = 'Bank';
					args['save']  = '';
					args['quick'] = '';
					args['title'] = '';
					args['bank_type'] = 'M';
					args['description'] = '';
				}else if(code_type == 'A'){
					url = 'customer-detail.php'
					captions = 'Customer';
					args['supp_name'] = '';
				}
				$("body").append("<div id='popUpBox'></div>");
				var formContent  = '';
				formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
				formContent += '<div id="form">';
				formContent += '<p>New '+captions+' Title:</p>';
				formContent += '<p class="textBoxGo">';
				formContent += '<input type="text" class="form-control categoryName" />';
				formContent += '</p>';
				formContent += '</div>';
				$("#popUpBox").append(formContent);
				$("#xfade").fadeIn();
				$("#popUpBox").fadeIn('200').centerThisDiv();
				$(".categoryName").focus();
				$(".categoryName").keyup(function(e){
					var name = $(".categoryName").val();
					if(code_type == 'B'){
						args['title'] = name;
					}else if(code_type == 'A'){
						args['supp_name'] = name;
					}
					if(e.keyCode == 13){
						$(".categoryName").blur();
						if(name != ''){
							$.post(url,args,function(data){
								data = $.parseJSON(data);
								if(data['OK'] == 'Y'){
									$("select.supplierSelector").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
									$("select.supplierSelector option:selected").prop('selected',false);
									$("select.supplierSelector option").last().prop('selected',true);
									$("select.supplierSelector").selectpicker('refresh');
									hidePopUpBox();
								}else if(data['OK'] == 'N'){
									alert('New '+captions+' Could Not be Created!');
								}
							});
						}
					}
				});
			};
			var getAccountBalance = function(){
				var acc_code = $("select.supplierSelector option:selected").val();
				$.post("db/get-account-balance.php",{supplierAccCode:acc_code},function(data){
					data = $.parseJSON(data);
					$("input.customer-balance").val(data['AMOUNT']);
				});
			}
			var discount_type_change = function(thiss){
				var this_val = $(thiss).val();
				if($("tr.dynamix").length){
					return false;
				}
				$.get('dist-sale-details.php',{discount:this_val});
			};
			var recover_balance = function(thiss){
				if($(thiss).is(":checked")){
					balance_recovered = "Y";
				}else{
					balance_recovered = "N";
				}
				$(this).calculateColumnTotals();
			}
			var sendSmsPopUp = function(){
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel' style='width:280px;'><p class='confirm text-left'>Please Confirm Mobile Number <button id='close-pop' class='pull-right btn btn-xs btn-danger'> <i class='fa fa-times'></i> </button> </p>  <input class='form-control confirmed-mobile pull-left text-center' style='width:150px;margin-left:25px;' value='"+$("input.customer_mobile").val()+"' /> <a class='nodelete btn btn-info'>Send</a></div>");
				centerThisDiv("#popUpDel");
				$("#popUpDel").hide();
				$("#xfade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$("#close-pop").click(function(){
					$("#popUpDel").fadeOut();
					$("#xfade").fadeOut();
				});
				$("#xfade").click(function(){
					$("#popUpDel").fadeOut();
					$("#xfade").fadeOut();
				});
				$(".nodelete").click(function(){
					sendSmsInit();
				});
			};
			var sendSmsInit = function(){
				$("#popUpDel input[type=text]").remove();
				$("#popUpDel p.confirm").text("Please Wait...");
				var mobile_number = ($("input.confirmed-mobile").length)?$("input.confirmed-mobile").val():"";
				if(mobile_number == ''){
					return;
				}
				var sale_id 	  = $("input.sale_id").val();
				$.post("dist-sale-details.php",{mobile_number:mobile_number,sale_id:sale_id},function(data){
					if(data == 'N'){
						data = "Error! Cannot send sms."
					}
					$("#popUpDel p.confirm").text(data);
					$("#popUpDel").css({"width":"350px"});
					$("#popUpDel").centerThisDiv();
					$("input.confirmed-mobile").val('');
					$("input.confirmed-mobile").hide();
					$(".nodelete").text("OK");
					$(".nodelete").click(function(){
						$("#popUpDel").fadeOut();
						$("#xfade").fadeOut();
					});
				});
			};