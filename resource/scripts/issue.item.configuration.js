$(document).ready(function(){
	$("input.total_discount").prop("readonly",true);
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46
				||  e.keyCode == 8   || e.keyCode == 9
				||  e.keyCode == 27  || e.keyCode == 13
				||  e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65  && e.ctrlKey === true)
				|| (e.keyCode >= 35  && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function(){
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);
	$(".close").click(
		function(){
			$(this).parent().fadeTo(400, 0, function(){ // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);
	// Alternating table rows:
	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
	// Check all checkboxes when the one in a table head is checked:
	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			 sumAll += parseFloat($(this).text());
		});
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			 sumAll += parseFloat($(this).text())||0;
		});
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text((sumAll).toFixed(2));
	};
	$.fn.sumColumnFloat3 = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			 sumAll += parseFloat($(this).text())||0;
		});
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text((sumAll).toFixed(3));
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteMainRow = function(file){
		var idValue    = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-info'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel .confirm").text("Are Sure you Want To Delete this record?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {cid : idValue}, function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					currentRow.slideUp();
				}
				$("#popUpDel .confirm").text(data['MSG']);
				$(".nodelete").text('Close');
				$(".dodelete").hide();
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp(function(){
				$(this).remove();
			});
			$("#xfade").fadeOut();
		});
	};

	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = $("input.taxRate").val();
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		var row_id  = 0;
		if(item_id != ''){
			$.post('db/get-item-details.php',{issue_item_id:item_id, row_id:0},function(data){
				data = $.parseJSON(data);
				var itemStock = parseFloat(data['STOCK'])||0;
				var itemPrice = data['P_PRICE'];
				var this_sold = 0;
				$("td[data-item-id='"+item_id+"']").each(function(){
					if((!$(this).parent().hasClass("updateMode")) && $(this).parent().attr("data-row-id") == 0){
						this_sold -= parseFloat($(this).parent().find("td.quantity").text())||0;
					}
				});
				if($(".updateMode").length){
					this_sold += parseFloat($(".updateMode").find("td.quantity").text())||0;
				}
				itemPrice = (itemPrice > 0)?itemPrice:"";
				if($(".updateMode").length == 0){
					$("input.unitPrice").val(itemPrice);
				}
				itemStock += this_sold;
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id  	  = $("select.itemSelector option:selected").val();
		var item_name     = $("select.itemSelector option:selected").text();
		var machine_id    = $("select.machine_id option:selected").val();
		var machine_name  = $("select.machine_id option:selected").text();
		var quantity 	  = parseFloat($("input.quantity").val())||0;
		var unitPrice     = $("input.unitPrice").val();
		var discount 	  = parseFloat($("input.discount").val()) || 0;
		var taxRate   	  = parseFloat($("input.taxRate").val()) || 0;
		var totalAmount   = $("input.totalAmount").val();
		var discount_type = $("input.discount_type:checked").val();
		var taxType   	  = ($(".taxType").is(":checked"))?"I":"E";

		$("input.discount").attr('data-amount','');
		$(this).blur();

		if(quantity == 0){
			displayMessage('Selected Quantity is Zero.');
			return false;
		}
		var subAmount = 0;
		var discountPerCentage = 0;
		subAmount     = quantity*unitPrice;
		subAmount = Math.round(subAmount*100)/100;
		if($("input.individual_discount").val()=='Y'){
			if(discount_type == 'R'){
				discountPerCentage = discount;
			}else if(discount_type == 'P'){
				discountPerCentage = subAmount*(discount/100);
			}
			subAmount -= discountPerCentage;
			subAmount = Math.round(subAmount*100)/100;
		}
		var taxAmount = 0;
		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = subAmount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = subAmount*(taxRate/100);
			}
		}

		var updateMode    = $(".updateMode").length;
		var update_row_id = 0;
		if(updateMode){
			update_row_id = parseFloat($(".updateMode").attr('data-row-id'))||0;
		}
		if($("td.itemName[data-item-id='"+item_id+"']").length && updateMode == 0){
			//displayMessage(item_name+' Already Included In Bill!');
			//$(this).clearPanelValues();
			//return false;
		}
		if(item_id > 0 && totalAmount > 0){
			var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="'+update_row_id+'">'
						   + '<td style="text-align:center;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>'
						   + '<td style="text-align:center;" class="machine_id" data-machine-id="'+machine_id+'">'+machine_name+'</td>'
						   + '<td style="text-align:center;" class="quantity">'+quantity+'</td>'
						   + '<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
				$theNewRow +='<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
							+'<td style="text-align:center;"> - - - </td>'
							+'<td style="text-align:center;">'
								+'<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>'
								+'<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
							+'</td>'
							+'</tr>';
			if(updateMode == 0){
				$($theNewRow).insertAfter($(".calculations").last());
			}else if(updateMode == 1){
				$(".updateMode").replaceWith($theNewRow);
			}
			$(this).clearPanelValues();
			sum_tax_amount();
			$(this).calculateColumnTotals();
			$(".itemSelector").parent().find(".dropdown-toggle").focus();
		}else{
			displayMessage('Values Missing!');
		}
	};
	$.fn.calculateColumnTotals = function(){
		var discount_type = $("input.discount_type:checked").val();
		$(this).sumColumnFloat3("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.unitPrice","td.price_total");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
		var amountTotal = parseFloat($("td.amountTotal").text())||0;

		var disk = 0;
		var rate = 0;
		var qty  = 0;
		var sub  = 0;
		if(discount_type == 'R'){
			$("td.discount").each(function(){
				disk += parseFloat($(this).text())||0;

			});
		}else{
			var dit = 0;
			$("tr.transactions").each(function(){
				qty  = parseFloat($(this).find("td.quantity").text())||0;
				rate = parseFloat($(this).find("td.unitPrice").text())||0;
				dit  = parseFloat($(this).find("td.discount").text())||0;

				disk +=  parseFloat(((qty*rate*dit) / 100))||0;
			});
		}

		var discount_amount = parseFloat($("input.whole_discount").val())||0;

		if(discount_type == 'P'){
			discount_amount = (amountTotal*discount_amount) / 100;
		}
		amountTotal -= discount_amount;

		$("input.total_discount").val((discount_amount+disk).toFixed(2));

		var charges = parseFloat($("input.inv_charges").val())||0;
		amountTotal += charges;
		$("input.grand_total").val((amountTotal).toFixed(2));
	};
	$.fn.addToThis = function(that_amount){
		var this_amount = parseFloat($(this).val())||0;
		$(this).va(that_amount+this_amount);

	};
	$.fn.calculateRowTotal = function(){
		var taxRate = parseFloat($("input.taxRate").val())||0;
		var taxType = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount  = Math.round((thisVal*thatVal)*100)/100;
		var discountAvail = parseFloat($("input.discount").val())||0;
		var discount_type = $("input.discount_type:checked").val();

		var discountPerCentage = 0;
		amount = Math.round(amount*100)/100;
		if($("input.individual_discount").val()=='Y'){
			if(discount_type == 'R'){
				discountPerCentage = discountAvail;
			}else if(discount_type == 'P'){
				discountAvail = Math.round(discountAvail*100)/100;
				discountPerCentage = amount*(discountAvail/100);
				discountPerCentage = Math.round(discountPerCentage*100)/100;
			}
			$("input.discount").attr('data-amount',discountPerCentage);
			amount -= discountPerCentage;
			amount = Math.round(amount*100)/100;
		}

		var taxAmount = 0;
		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount*(taxRate/100);
			}
		}
		if(taxType == 'I'){
			amount -= taxAmount;
		}else{
			amount += taxAmount;
		}
		$("input.totalAmount").val(amount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
			stockOfBill += parseFloat($(this).text())||0;
		});
		stockOfBill += parseFloat($("input.quantity").val())||0;
	};

	$.fn.clearPanelValues = function(){
		$(".itemSelector option").prop('selected',false);
		$(".itemSelector").find("option").first().prop('selected',true);
		$(".itemSelector").selectpicker('refresh');

		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.taxRate").val('');
		$("input.subAmount").val('');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var showDeleteRowDilog = function(rowChildElement){
			var idValue = $(rowChildElement).attr("do");
			var clickedDel = $(rowChildElement);
			if($(rowChildElement).parent().parent().hasClass('updateMode')){
				displayMessage('Record is in edit Mode!');
				return false;
			}

			$("#fade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-info'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#popUpDel").centerThisDiv();
			$("#fade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".dodelete").click(function(){
				$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
				$("#popUpDel").children(".dodelete").hide();
				$("#popUpDel").children(".nodelete").text("Close");
				clickedDel.parent().parent().remove();
				$(this).calculateColumnTotals();
			});
			$(".nodelete").click(function(){
				$("#fade").fadeOut();
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
				});
			});
			$(".close_popup").click(function(){
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
				});
				$("#fade").fadeOut('fast');
			});
	};
	var validatePhone = function(txtPhone) {
		var a = $(txtPhone).val();
		var filter = /^[0-9-+]+$/;
		if(a[0] == 0){
			a = a.substring(1,11);
		}
		if(a[0] != 3){
			return false;
		}
		if(a.length != 10){
			return false;
		}
		if(filter.test(a)) {
			return a;
		}else{
			return false;
		}
	};
	var sum_tax_amount = function(){
		var tax = 0;
		$("td.taxRate").each(function(){
			tax += parseFloat($(this).attr('tax-amount'))||0;
		});
		$(".whole_tax").val((tax).toFixed(2));
	}
	var stockOs = function(){
		var thisQty = parseFloat($("input.quantity").val())||0;
		var inStock = parseFloat($("input.inStock").attr('thestock'))||0;
		var selectedItem = $("select.itemSelector option:selected").val();
		var NewStock = inStock - thisQty;
		if(thisQty <= inStock){
			$("input.inStock").val(NewStock);
		}else{
			$("input.quantity").val('');
		}
	};
	var stockOsQuick = function(elm,price,inStock){
		var thisQty = parseFloat($(elm).val())||0;
		var NewStock = inStock - thisQty;
		if(thisQty <= inStock){
			$(elm).parent().parent().find("td.subAmount").text((thisQty*price).toFixed(2));
			$(elm).parent().parent().find("td.totalAmount").text((thisQty*price).toFixed(2));
		}else{
			$(elm).val('');
		}
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	= thisRow.find('td.itemName').text();
		var item_id 	= parseFloat(thisRow.find('td.itemName').attr('data-item-id'))||0;
		var machine_id 	= parseInt(thisRow.find('td.machine_id').attr('data-machine-id'))||0;
		var quantity 	= thisRow.find('td.quantity').text();
		var unitPrice 	= thisRow.find('td.unitPrice').text();
		var totalAmount = thisRow.find('td.totalAmount').text();

		$("select.itemSelector option[value='"+item_id+"']").prop('selected',true);
		$("select.itemSelector").selectpicker('refresh');
		$("select.machine_id option[value='"+machine_id+"']").prop('selected',true);
		$("select.machine_id").selectpicker('refresh');
		$("input.quantity").val(quantity);
		$("input.unitPrice").val(unitPrice);
		$("input.totalAmount").val(totalAmount);

		$("input.inStock").val('').attr('thestock','');

		$("div.itemSelector button").focus();
	};

	var saveSale = function(){
		var print_method  = $(".print_method").val();
		var issue_id	  = $("input[name='issue_id']").val();
		var issue_date 	  = $("input[name='issue_date']").val();
		var po_number 	  = $("input[name='po_number']").val();
		var issue_usage   = $("select.issue_usage").val();
		var expensed_to   = $("select.expensed_to option:selected").val();
		var inv_notes     = $("textarea.inv_notes").val();

        if(expensed_to == ''){
            displayMessage('Error! Shed account not selected.');
            return;
        }

		if($("tr.transactions").length == 0){
			displayMessage('No transaction exists, Record can not be saved!');
			return false;
		}

		$(".savePurchase").hide();
		$("#xfade").fadeIn();
		var jsonObj = {};

		$("tr.transactions").each(function(index, element){
			var rowId       = $(this).attr('data-row-id');
			var thisRow     = $(this);
			var item_id 	= parseFloat(thisRow.find('td.itemName').attr('data-item-id'))||0;
			var machine_id 	= parseFloat(thisRow.find('td.machine_id').attr('data-machine-id'))||0;
			var quantity 	= thisRow.find('td.quantity').text();
			var unitPrice 	= thisRow.find('td.unitPrice').text();
			var totalAmount = thisRow.find('td.totalAmount').text();
			if(item_id != '' && quantity != 0 && quantity != 0){
				jsonObj[index] = {};
				jsonObj[index].row_id  		= rowId;
				jsonObj[index].item_id 		= item_id;
				jsonObj[index].machine_id 	= machine_id;
				jsonObj[index].quantity     = quantity;
				jsonObj[index].unitPrice    = unitPrice;
				jsonObj[index].totalAmount  = totalAmount;
			}
		});

		var jSonString = JSON.stringify(jsonObj);
		$.post("db/save.issue.items.php",{issue_id:issue_id,
										  issue_date:issue_date,
										  po_number:po_number,
										  expensed_to:expensed_to,
										  inv_notes:inv_notes,
										  jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					var has = "saved";
					if(issue_id > 0){
						has = "updated";
					}
					window.location.href = 'emb-issue-items.php?id='+data['ID']+"&"+has;
				}else{
					displayMessage(data['MSG']);
					$("#xfade").fadeOut();
				}
		});
	};
	var hidePopUpBox = function(){
		$("#popUpBox").css({'left':'-=500px'});
		$("#popUpBox").css({'transform':'scaleY(0.0)'});
		$("#xfade").fadeOut();
		$("#popUpBox").remove();
	};
