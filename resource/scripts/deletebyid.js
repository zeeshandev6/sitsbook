function deleteById(x, page){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).attr("value");
        var del = $.post(page, {id:id}, function(data){});
        if(del){
            $(x).parent().parent().remove();
        }
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
