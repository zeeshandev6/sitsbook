$(document).ready(function(){
	$("input.total_discount").prop("readonly",true);
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);
	// Alternating table rows:
	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
	// Check all checkboxes when the one in a table head is checked:
	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			if(!$(this).parent().find("input.outward_row").is(":checked")){
				return;
			}
			 sumAll += parseInt($(this).text());
		});
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			sumAll += parseFloat($(this).text())||0;
		});
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text((sumAll).toFixed(2));
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteMainRow = function(file){
		var idValue    = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel .confirm").text("Are Sure you Want To Delete ?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {cid : idValue}, function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					currentRow.slideUp();
				}
				$("#popUpDel .confirm").text(data['MSG']);
				$(".nodelete").text('Close');
				$(".dodelete").hide();
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp(function(){
				$(this).remove();
			});
			$("#xfade").fadeOut();
		});
	};

	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = $("input.taxRate").val();
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		var row_id  = 0;
		if(item_id != ''){
			$.post('db/get-item-details.php',{s_item_id:item_id,row_id:0},function(data){
				data = $.parseJSON(data);
				var itemStock = parseInt(data['STOCK'])||0;
				var itemCost = data['P_PRICE'];
				var itemDisc  = data['DISCOUNT'];
				var this_sold = 0;
				$("td[data-item-id='"+item_id+"']").each(function(){
					if(!$(this).parent().hasClass("updateMode")){
						this_sold -= parseInt($(this).parent().find("td.quantity").text())||0;
					}
				});
				if($(".updateMode").length){
					this_sold += parseFloat($(".updateMode").find("td.quantity").text())||0;
				}
				itemCost = (itemCost > 0)?itemCost:"";
				if($(".updateMode").length == 0){
					$("input.unitCost").val(itemCost);
				}
				itemStock += this_sold;
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.quickSave = function(){
		var account_code  	= $("select.account_code option:selected").val();
		var account_name   	= $("select.account_code option:selected").text();
		var narration 		= $("input.narration").val();
		var amount 			= $("input.amount").val();

		if(amount == 0 || account_code == ''){
			return false;
		}

		$(this).blur();

		var discountPerCentage = 0;
		var updateMode    = $(".updateMode").length;
		var update_row_id = 0;

		if(updateMode){
			update_row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
		}

		var $theNewRow  ='<tr class="alt-row calculations transactions dynamix" data-row-id="'+update_row_id+'">'
						+'<td style="text-align:center;" class="account_code" data-code="'+account_code+'">'+account_name+'</td>'
						+'<td style="text-align:center;" class="narration">'+narration+'</td>'
						+'<td style="text-align:center;" class="amount">'+amount+'</td>';
			$theNewRow +='<td style="text-align:center;">'
						+'<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>'
						+'<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
						+'</td>'
						+'</tr>';
		if(updateMode == 0){
			$("table tbody").append($theNewRow);
		}else if(updateMode == 1){
			$(".updateMode").replaceWith($theNewRow);
		}
		$(this).clearPanelValues();
		$(this).calculateColumnTotals();
		$("div.account_code button").focus();
	};
	$.fn.calculateColumnTotals = function(){
		var discount_type = $("input.discount_type:checked").val();
		$(this).sumColumnFloat("td.quantity","td.quantity_total");
		$(this).sumColumnFloat("td.unitCost","td.cost_total");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
	$.fn.addToThis = function(that_amount){
		var this_amount = parseFloat($(this).val())||0;
		$(this).va(that_amount+this_amount);

	};
	$.fn.calculateRowTotal = function(){
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitCost").val())||0;
		var amount  = Math.round((thisVal*thatVal)*100)/100;
		$("input.totalAmount").val(amount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
			stockOfBill += parseInt($(this).text())||0;
		});
		stockOfBill += parseInt($("input.quantity").val())||0;
	};

	$.fn.clearPanelValues = function(){
		$("select.account_code option").prop('selected',false);
		$("select.account_code").find("option").first().prop('selected',true);
		$("select.account_code").selectpicker('refresh');
		$("input.amount").val('');
		$("input.narration").val('');
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var showDeleteRowDilog = function(rowChildElement){
			var idValue = $(rowChildElement).attr("do");
			var clickedDel = $(rowChildElement);
			if($(rowChildElement).parent().parent().hasClass('updateMode')){
				displayMessage('Record is in edit Mode!');
				return false;
			}

			$("#fade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#popUpDel").centerThisDiv();
			$("#fade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".dodelete").click(function(){
				$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
				$("#popUpDel").children(".dodelete").hide();
				$("#popUpDel").children(".nodelete").text("Close");
				$("div.save").addClass('r_button');
				clickedDel.parent().parent().remove();
				$(this).calculateColumnTotals();
			});
			$(".nodelete").click(function(){
				$("#fade").fadeOut();
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
				});
			});
			$(".close_popup").click(function(){
				$("#popUpDel").fadeOut(function(){
					$("#popUpDel").remove();
				});
				$("#fade").fadeOut('fast');
			});
	};
	var validatePhone = function(txtPhone) {
		var a = $(txtPhone).val();
		var filter = /^[0-9-+]+$/;
		if(a[0] == 0){
			a = a.substring(1,11);
		}
		if(a[0] != 3){
			return false;
		}
		if(a.length != 10){
			return false;
		}
		if(filter.test(a)) {
			return a;
		}else{
			return false;
		}
	};
	var stockOs = function(){
		var thisQty = parseInt($("input.quantity").val())||0;
		var inStock = parseInt($("input.inStock").attr('thestock'))||0;
		var selectedItem = $("select.itemSelector option:selected").val();
		var NewStock = inStock - thisQty;
		if(thisQty <= inStock){
			$("input.inStock").val(NewStock);
		}else{
			$("input.quantity").val('');
		}
	};
	var stockOsQuick = function(elm,cost,inStock){
		var thisQty = parseInt($(elm).val())||0;
		var NewStock = inStock - thisQty;
		if(thisQty <= inStock){
			$(elm).parent().parent().find("td.totalAmount").text((thisQty*cost).toFixed(2));
		}else{
			$(elm).val('');
		}
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var account_code 	= thisRow.find('td.account_code').attr('data-code');
		var account_name 	= thisRow.find('td.account_code').text();
		var narration 		= thisRow.find('td.narration').text();
		var amount 			= thisRow.find('td.amount').text();

		$("select.account_code").selectpicker('val',account_code);
		$("input.narration").val(narration);
		$("input.amount").val(amount);
		$("div.account_code button").focus();
	};

	var saveProjectExpense = function(){
		var expense_id		  = parseInt($("input.expense_id").val())||0;
		var project_id		  = parseInt($("input.project_id").val())||0;
		var quotation_id	  = parseInt($("input.quotation_id").val())||0;
		var expense_date	  = $("input[name='expense_date']").val();

		if(project_id == 0){
			return false;
		}
		if($("tr.transactions").length == 0){
			displayMessage('No Transaction Existed, Cannot Save Record!');
			return false;
		}
		$(".save").hide();
		$("#xfade").fadeIn();
		var jsonObject = {};
		var index_counter = 0;

		//service_rows
		$("tr.transactions").each(function(index, element){
			var thisRow     	  = $(this);
			jsonObject[index_counter] = {};
			jsonObject[index_counter].row_id		= $(this).attr('data-row-id');
			jsonObject[index_counter].account_code	= thisRow.find('td.account_code').attr('data-code');
			jsonObject[index_counter].narration		= thisRow.find('td.narration').text();
			jsonObject[index_counter].amount		= thisRow.find('td.amount').text();

			index_counter++;
		});
		jSonString  = JSON.stringify(jsonObject);
		$.post("db/saveProjectExpense.php",{expense_id:expense_id,
								  project_id:project_id,
								  expense_date:expense_date,
								  quotation_id:quotation_id,
								  jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					var msg_type = '';
					if(expense_id == 0){
						msg_type = "&saved";
					}else{
						msg_type = "&updated";
					}
					window.location.href = 'project-expense.php?id='+data['ID']+msg_type;
				}else{
					displayMessage(data['MSG']);
					$("#xfade").fadeOut();
				}
		});
	};
	var barcode_scan_for_sale = function(){
		var barcode = $(".barcode_input").val();
		$(".barcode_input").val('').blur();
		if(barcode == ''){
			$(".barcode_input").focus();
			return false;
		}
		var tr = '';
		$.get('db/scan_item.php',{b_code:barcode},function(data){
			if(data == ''){
				displayMessage('Item Does Not Exist!');
				return false;
			}
			var item_details = $.parseJSON(data);
			if(item_details['OK'] == 'N'){
				displayMessage(item_details['MSG']);
				return false;
			}
			if($("*[data-bi='"+item_details['BI']+"']").length){
				displayMessage('Item Already Scanned!');
				return false;
			}
			tr = '<tr class="alt-row calculations transactions dynamix" data-row-id="0" data-bi="'+item_details['BI']+'">';
			tr += '<td style="text-align:left;" class="itemName" data-item-id="'+item_details['ID']+'">'+item_details['NAME']+'</td>';
			tr += '<td style="text-align:center;" class="quantity">1</td>';
			tr += '<td style="text-align:center;" class="unitCost">'+item_details['SALE_PRICE']+'</td>';
			tr += '<td style="text-align:center;"><a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a></td>';
			tr += '</tr>';
			$(tr).insertAfter($('.calculations').last());
			tr = '';
			$(this).calculateColumnTotals();
			$(".barcode_input").focus();
		});
	};
	var check_sms_service = function(){
		$.get('db/check-sms-service.php',{},function(data){
			if(data == 'N'){
				displayMessage('Sms Service Is Not Active!');
				$("input[name='mobile_no']").css({'border':'1px solid red'});
			}else if(data == 'Y'){
				$("input[name='mobile_no']").css({'border':'1px solid #008000'});
			}
		});
	};
	var makeItCash = function(checker){
		var what = $(checker).is(":checked");
		var this_val = $(checker).val();
		if(this_val=='A'){
			$("select.supplierSelector option[value^='010104']").prop('disabled',false).prop('selected',false);
			$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector").selectpicker('refresh');
			$("input[name='supplier_name']").val('').hide();
		}else if(this_val=='B'){
			$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector option[value^='010102']").prop('disabled',false).prop('selected',false);
			$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector").selectpicker('refresh');
			$("input[name='supplier_name']").attr('placeholder','Card Number');
			$("input[name='supplier_name']").show().focus();
		}else if(this_val=='C'){
			$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
			$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
			$("select.supplierSelector").selectpicker('refresh');
			$("input[name='supplier_name']").attr('placeholder','Customer Name');
			$("input[name='supplier_name']").show().focus();
		}
	};
	var hidePopUpBox = function(){
		$("#popUpBox").css({'left':'-=500px'});
		$("#popUpBox").css({'transform':'scaleY(0.0)'});
		$("#xfade").fadeOut();
		$("#popUpBox").remove();
	};
	var add_supplier = function(){
		var code_type = $("input[name='radiog_dark']:checked").val();
		if(code_type == 'C'){
			return false;
		}
		var url = '';
		var captions = '';
		if(code_type == 'B'){
			url = 'accounts-management.php'
			captions = 'Bank';
		}else if(code_type == 'A'){
			url = 'customer-detail.php'
			captions = 'Customer';
		}
		$("body").append("<div id='popUpBox'></div>");
		var formContent  = '';
			formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
			formContent += '<div id="form">';
			formContent += '<p>New '+captions+' Title:</p>';
			formContent += '<p class="textBoxGo">';
			formContent += '<input type="text" class="form-control categoryName" />';
			formContent += '</p>';
			formContent += '</div>';
		$("#popUpBox").append(formContent);
		$("#xfade").fadeIn();
		$("#popUpBox").fadeIn('200').centerThisDiv();
		$(".categoryName").focus();
		$(".categoryName").keyup(function(e){
			var name = $(".categoryName").val();
			if(e.keyCode == 13){
				$(".categoryName").blur();
				if(name != ''){
					$.post(url,{supp_name:name},function(data){
						data = $.parseJSON(data);
						if(data['OK'] == 'Y'){
							$("select.supplierSelector").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
							$("select.supplierSelector option:selected").prop('selected',false);
							$("select.supplierSelector option").last().prop('selected',true);
							$("select.supplierSelector").selectpicker('refresh');
							hidePopUpBox();
						}else if(data['OK'] == 'N'){
							alert('New '+captions+' Could Not be Created!');
						}
					});
				}
			}
		});
	};
	var discount_type_change = function(thiss){
		var this_val = $(thiss).val();
		if($("tr.dynamix").length){
			return false;
		}
		$.get('sale-details.php',{discount:this_val});
	};
