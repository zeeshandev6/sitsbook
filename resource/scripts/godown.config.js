var getItemDetailsForGodown = function(){
	var item_id	 	= $("select[name=item_id]").val();
	var from_godown = $("select[name=from_godown]").val();
	var to_godown = $("select[name=to_godown]").val();
	
	$.get('db/get-item-details-for-godown.php',{item_id:item_id,from_godown:from_godown,to_godown:to_godown},function(data){
		if(data != ''){
			data = $.parseJSON(data);
			$("input.from_godown_qty").val(data['FROM_GODOWN']).attr('data-qty',data['FROM_GODOWN']);
			$("input.to_godown_qty").val(data['TO_GODOWN']).attr('data-qty',data['TO_GODOWN']);
		}
	});
};
var shift_quantity_from_to_godown = function(){
	var quantity = parseInt($("input[name=quantity]").val())||0;
	var from_qty = parseInt($("input.from_godown_qty").attr('data-qty'))||0;
	if(quantity > from_qty){
		$("input[name=quantity]").val(from_qty).keyup();
	}
};
