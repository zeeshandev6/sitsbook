//delete product
function deleteMeasureType(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var delete_id = $(x).attr("value");
        var del = $.post("measure-type-management.php", {delete_id:delete_id}, function(data){});
        if(del){
            $(x).parent().parent().remove();
        }
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}

function changeStatus(y,fileName){
    var id = $(y).attr("id");
    //set initial state.
    $('#'+id).val($(y).is(':checked'));
    $('#'+id).change(function() {
        if($(y).is(":checked")) {
            var returnVal = $.post(fileName, {check_id:id, check:'check'}, function(data){});
            $(y).attr("checked", returnVal);
        }else{
            var returnVal = $.post(fileName, {check_id:id, check:'uncheck'}, function(data){});
            $('#'+id).val($(y).is(':checked', returnVal));
        }

    });
}
