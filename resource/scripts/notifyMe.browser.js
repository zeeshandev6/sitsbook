// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe(icon_url,title,body_message,page_url){
    //var icon_url,body_message,page_url = null;
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification(title, {
      icon: icon_url,
      body: body_message,
    });

    notification.onclick = function(){
      window.open(page_url);      
    };
    
  }

}
