$(document).ready(function(){
	var total = 0;
	balance_recovered = ($("input.recovered_balance").is(":checked"))?"Y":"N";
	$("td.price_td").each(function(){
		total += parseFloat($(this).find('input').val())||0;
	});
	$("table.barcodes_table td.sale_td input").trigger('change');
	$(".total_amount").val((total).toFixed(2));
	$("input.grand_total").val((total).toFixed(2));
	$("select.supplierSelector").change(function(){
		getAccountBalance();
	});
	getAccountBalance();
	$("table.barcodes_table").on('click','.pointer',function(){
		$(this).parent().parent().remove();
	});
	$("table.barcodes_table").on('keyup','.price_td .form-control',function(e){
		if(e.keyCode == 13){
			$(this).parent().next().children("input").focus();
		}
	});
	transcationCounter();
	$("table.barcodes_table").on('change','td.item_td select',function(){
		var $tdbox  = $(this).parent('td');
		var items   = {};

		$("table.barcodes_table .item_td").each(function(i,e){
			if($(this).find("select option:selected").val() == '') return;
			items[$(this).find("select option:selected").val()] = $(this).find("select option:selected").val();
		});

		$.each(items,function(index,item_id){
			item_id = parseInt(item_id)||0;
			if(item_id == 0) return;
			$.post('db/get-item-details.php',{mobile_item_id:item_id},function(data){
				data = $.parseJSON(data);
				data['STOCK'] = parseFloat(data['STOCK'])||0;
				$("select").each(function(i,e){
					if($(this).find("option:selected").val() == item_id){
						if($(this).parent().parent().hasClass("dynamic")){
							return;
						}
						data['STOCK']--;
						$(this).parent().parent().find("td.stock_td").text(data['STOCK']);
					}
				});
			});
		});
	});
	$("input.received_cash,input.delivery_charges").on("blur change",function(){
		$.fn.calculateColumnTotals();
	});
	$.fn.calculateColumnTotals = function(){
		$("table.barcodes_table td.sale_td input").trigger('change');
	};
	$("table.barcodes_table").on('change',"input",function(){
		$("input[readonly]").prop("tabindex",'-1');
		var discount_type = $(".discount_type").val();
		var transaction_type  = $("input[name=radiog_dark]:checked").val();

		var total 			= 0;
		var discount_total  = 0;
		var tax_total 		= 0;
		var total_price 	= 0;

		var price_td     = 0;
		var sale_td      = 0;
		var discount_td  = 0;
		var taxAmount    = 0;
		var tax_td       = 0;
		var this_discount= 0;

		$("table.barcodes_table tr.barcode_me").each(function(){
			sale_td 	= parseFloat($(this).find('td.sale_td input').val())||0;
			discount_td = parseFloat($(this).find('td.discount_td input').val())||0;
			tax_td      = parseFloat($(this).find('td.tax_td input').val())||0;

			total_price    += sale_td;

			this_discount   = sale_td*(discount_td/100);
			discount_total += this_discount;
			price_td        = sale_td - this_discount;
			if(tax_td > 0){
				taxAmount    = price_td * (tax_td/100);
				tax_total    += taxAmount;
			}
			total   += parseFloat(price_td+taxAmount)||0;
			$(this).find("td.price_td input").val((parseFloat(price_td+taxAmount)||0).toFixed(2));
		});
		$(".total_sale_price").val((total_price).toFixed(2));
		$(".total_discount").val((discount_total).toFixed(2));
		$(".total_tax").val((tax_total).toFixed(2));
		$(".total_amount").val((total).toFixed(2));
		total   += parseFloat($("input.delivery_charges").val())||0;
		$("input.grand_total").val((total).toFixed(2));

		var received    = parseFloat($("input.received_cash").val())||0;
		var grand_total = $("input.grand_total").val();
		var returnin    = received - grand_total;

		if(transaction_type == 'C'){
			if(received >= grand_total){
				$("input.change_return").val((returnin).toFixed(2));
				$("input.remaining_amount").val("");
			}else{
				$("input.remaining_amount").val("");
				$("input.change_return").val("");
				$("input.received_cash").val("");
			}
		}else{
			if(received < grand_total){
				returnin *= -1;
				$("input.remaining_amount").val((returnin).toFixed(2));
			}else{
				$("input.remaining_amount").val("");
			}
			if(received > grand_total){
				$("input.change_return").val((returnin).toFixed(2));
			}else{
				$("input.change_return").val("");
			}
		}
		var rec_amount = $("input.recovery_amount").val();
		var chg_amount = $("input.change_return").val();

		if(balance_recovered == 'Y'){
			$("input.recovery_amount").val(chg_amount);
			$("input.change_return").val('');
		}else{
			$("input.recovery_amount").val('');
		}
	});
	$("input[name=customer_mobile]").numericInputOnly();
	$(".barcode_put").keyup(function(e){
		if(e.keyCode == 13){
			$(".barcode_put").prop('readonly',true).blur();
			$exception = $("input.css-checkbox:checked").length == $("input.css-checkbox").length;
			var $thisInputBarcode = $(this);
			var the_code 		  = $thisInputBarcode.val();
			if(the_code == ''){
				$(".barcode_put").prop('readonly',false).focus();
				return false;
			}else{
				$thisInputBarcode.val('');
			}
			if($("tr[data-code='"+the_code+"']").length){
				$("tr[data-code='"+the_code+"']").animate({"background-color":"rgba(255,0,0,0.4)"},400,function(){
					$("tr[data-code='"+the_code+"']").animate({"background-color":"rgba(255,255,255,1.0)"});
				});
				$(".barcode_put").prop('readonly',false).focus();
				return false;
			}
			$.get('db/scan-item.php',{barcode:the_code},function(data){
				if(data != ''){
					data = $.parseJSON(data);
					var get_def = false;
					if($("table.barcodes_table tbody tr").length){
						var get_def      = true;
						var item_def     = $("table.barcodes_table tbody tr").last().find("td.item_td select option:selected").val();
						if(show_discount=='Y'){
						var discount_def = $("table.barcodes_table tbody tr").last().find("td.discount_td input").val();
						}
						if(show_tax=='Y'){
						var tax_def      = $("table.barcodes_table tbody tr").last().find("td.tax_td input").val();
						}
					}
					data['STOCK'] = parseFloat(data['STOCK'])||0;
					data['STOCK'] = data['STOCK'] - ($("td[data-item='"+data['ITEM_ID']+"']").length);
					data['STOCK']--;
					var $row = new_row_item_object(data['ID'],data['ITEM_ID'],data['STOCK'],data['BARCODE'],data['COMPANY'],data['ITEM_NAME'],data['COLOUR'],data['SALE_PRICE'],'','',data['SALE_PRICE']);
					$("table.barcodes_table tbody").append($row);
					if(get_def){
						$("table.barcodes_table tbody tr").last().find("td.item_td select").selectpicker('val',item_def);
						if(show_discount=='Y'){
						$("table.barcodes_table tbody tr").last().find("td.discount_td input").val(discount_def);
						}
						if(show_tax=='Y'){
						$("table.barcodes_table tbody tr").last().find("td.tax_td input").val(tax_def);
						}
					}
					$("table.barcodes_table td.discount_td input").trigger('change');
					$(".barcode_put").prop('readonly',false).focus();
				}else{
					displayMessage('Item Not Found!');
					$("table.barcodes_table td.sale_td input").change();
					$(".barcode_put").prop('readonly',false).focus();
				}
				$("table.barcodes_table td.discount_td input").trigger('change');
				transcationCounter();
			});
		}
	});
	$("table.barcodes_table ").on('remove',"tr",function(){
		$("table.barcodes_table td.sale_td input").change();
	});
	$("table.barcodes_table td.sale_td input").change();
	$(".save_scan").click(function(){
		direct_print = 'N';
		save_sale_barcodes();
	});
	$(".save_scan_and_print").click(function(){
		direct_print = 'Y';
		save_sale_barcodes();
	});
});
/* Calls */
var balance_recovered = "";
var direct_print  	  = 'N';
var makeItCash = function(checker){
	var what = $(checker).is(":checked");
	var this_val = $(checker).val();
	var selected_customer = $("input.selected_customer").val();
	$(".balance-of-customer").hide();
	$(".balance-of-customer input").val(0);
	if(this_val=='A'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").val('').hide();
		$("div.merch_rate_div").hide();
		$("input.card_charges_applies").prop("checked",false);
		getAccountBalance();
		$(".balance-of-customer").show();
	}else if(this_val=='B'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").attr('placeholder','Card Number');
		$("div.merch_rate_div").show();
		$("input.card_charges_applies").prop("checked",false);
		$("input[name='supplier_name']").show().focus();
	}else if(this_val=='C'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").attr('placeholder','Customer Name');
		$("div.merch_rate_div").hide();
		$("input.card_charges_applies").prop("checked",false);
		$("input[name='supplier_name']").show().focus();
	}
};
var new_row_item_object = function(sp_id,item_id,stock,barcode,company,item_name,colour,price,discount,tax,sale_amount){
	var row = '<tr class="barcode_me" row-id="0" data-sp-id="'+sp_id+'" data-code="'+barcode+'">'
	+'<td class="sr_td"></td>'
	+'<td class="barcode_td">'+barcode+'</td>'
	+'<td class="item_company">'+company+'</td>'
	+'<td class="item_td" data-item="'+item_id+'">'+item_name+' - '+colour+'</td>'
	+'<td class="stock_td">'+stock+'</td>'
	+'<td class="sale_td"><input type="text" value="'+price+'" class="form-control text-center" /></td>';
	if(show_discount=='Y'){
	row += '<td class="discount_td"><input type="text" value="'+discount+'" class="form-control text-center" /></td>';
	}
	if(show_tax=='Y'){
	row += '<td class="tax_td"><input type="text" value="'+tax+'" class="form-control text-center" /></td>';
	}
	row += '<td class="price_td"><input type="text" class="form-control text-center" value="'+sale_amount+'" readonly /></td>'
	+'<td><a class="pointer"><i class="fa fa-times"></i></a></td>'
	+'</tr>';
	return row;
};
var save_sale_barcodes   = function(){
	var scan_sale_id       = parseInt($(".scan_sale_id").val())||0;
	var sale_date 	       = $("input[name=rDate]").val();
	var bill_no 		   		 = $("input[name=billNum]").val();
	var customer_name      = $("input[name=supplier_name]").val();
	var card_charges 			 = ($("input.card_charges_applies").is(":checked"))?$("input.card_charges_applies").val():0;

	var customer_acc_code  = $("select.supplierSelector option:selected").val();
	var bill_discount      = $("input.total_discount").val();
	var sale_order_no  		 = parseInt($("input.sale_order_no").val())||0;
	var bill_tax           = $("input.total_tax").val();

	var transaction_type  = $("input[name=radiog_dark]:checked").val();
	var grand_total  	  = $("input.grand_total").val();

	var received_cash     = parseFloat($("input.received_cash").val())||0;
	var change_return     = parseFloat($("input.change_return").val())||0;
	var recovery_amount   = parseFloat($("input.recovery_amount").val())||0;
	var remaining_amount  = parseFloat($("input.remaining_amount").val())||0;
	var delivery_charges  = parseFloat($("input.delivery_charges").val())||0;

	if(transaction_type == 'C' && grand_total > received_cash){
		displayMessage('Error! Received cash must be equal to bill amount.');
		return false;
	}

	var customer_mobile    = '';

	if(customer_acc_code == ''){
		displayMessage('Please select an Account!');
		return false;
	}
	if($(".customer_mobile").length){
		customer_mobile     = $(".customer_mobile").val();
		if(customer_mobile != ''){
			if($(".customer_mobile").val().substr(0,1) != 0 || ($(".customer_mobile").val().length > 1 && $(".customer_mobile").val().substr(1,1) != 3) || $(".customer_mobile").val().length != 11){
				displayMessage('Please Enter a valid Moblie Number!');
				return false;
			}
		}
	}
	var data_obj = {},data_obj_str = '';
	var validated = true;
	if($("tr.barcode_me").length == 0){
		displayMessage('Please, make at least one entry to continue!');
		return false;
	}
	$("tr.barcode_me").each(function(index,element){
		var price = parseFloat($(this).find("td.price_td input").val())||0;
		if(price < 1){
			validated = false;
		}
	});
	if(validated == false){
		displayMessage('Please, Fill The Form Carefully!');
		return false;
	}

	$(".save_scan").hide();
	$(".pointer").hide();
	$("#xfade").fadeIn();
	var send_data = true;
	$("tr.barcode_me").each(function(index,element){
		if($(this).find("td.bg-danger").length){
			send_data = false;
			return;
		}
		data_obj[index] = {};
		data_obj[index].row_id        = $(this).attr('row-id');
		data_obj[index].sp_detail_id  = $(this).attr('data-sp-id');
		data_obj[index].sale 		  		= $(this).find(".sale_td input").val();
		data_obj[index].discount 	  	= $(this).find(".discount_td input").val();
		data_obj[index].tax 		  		= $(this).find(".tax_td input").val();
		data_obj[index].price 		  	= $(this).find(".price_td input").val();
	});
	if(!send_data){
		$(".save_scan").show();
		$(".pointer").show();
		displayMessage("Error! Items missing from stock.");
		return;
	}
	data_obj_str = JSON.stringify(data_obj);
	$.post('db/save_scan_sale.php',
	{scan_sale_id:scan_sale_id,
		sale_date:sale_date,
		bill_no:bill_no,
		sale_order_no:sale_order_no,
		customer_name:customer_name,
		card_charges:card_charges,
		customer_mobile:customer_mobile,
		customer_acc_code:customer_acc_code,
		sale_discount:bill_discount,
		bill_tax:bill_tax,
		total_amount:grand_total,
		received_cash:received_cash,
		recovered_balance:balance_recovered,
		recovery_amount:recovery_amount,
		change_returned:change_return,
		remaining_amount:remaining_amount,
		delivery_charges:delivery_charges,
		jSonString:data_obj_str},
		function(data){
			$("#xfade").fadeOut();
			if(data != ''){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					if(direct_print == 'N'){
						window.location.href = "mobile-sale-details.php?id="+data['ID'];
					}else{
						window.location.href = "mobile-sale-details.php?print&id="+data['ID'];
					}
				}else{
					displayMessage(data['MSG']);
					$(".pointer").show();
					$(".save_scan").show();
				}
			}
		});
	};
	var hidePopUpBox = function(){
		$("#popUpBox").css({'left':'-=500px'});
		$("#popUpBox").css({'transform':'scaleY(0.0)'});
		$("#xfade").fadeOut();
		$("#popUpBox").remove();
	};
	var add_supplier = function(){
		var code_type = $("input[name='radiog_dark']:checked").val();
		if(code_type == 'C'){
			return false;
		}
		var url = '';
		var captions = '';
		if(code_type == 'B'){
			url = 'accounts-management.php'
			captions = 'Bank';
		}else if(code_type == 'A'){
			url = 'customer-detail.php'
			captions = 'Customer';
		}
		$("body").append("<div id='popUpBox'></div>");
		var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New '+captions+' Title:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control categoryName" />';
		formContent += '</p>';
		formContent += '</div>';
		$("#popUpBox").append(formContent);
		$("#xfade").fadeIn();
		$("#popUpBox").fadeIn('200').centerThisDiv();
		$(".categoryName").focus();
		$(".categoryName").keyup(function(e){
			var name = $(".categoryName").val();
			if(e.keyCode == 13){
				$(".categoryName").blur();
				if(name != ''){
					$.post(url,{supp_name:name},function(data){
						data = $.parseJSON(data);
						if(data['OK'] == 'Y'){
							$("select.supplierSelector").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
							$("select.supplierSelector option:selected").prop('selected',false);
							$("select.supplierSelector option").last().prop('selected',true);
							$("select.supplierSelector").selectpicker('refresh');
							hidePopUpBox();
						}else if(data['OK'] == 'N'){
							alert('New '+captions+' Could Not be Created!');
						}
					});
				}
			}
		});
	};
	var getAccountBalance = function(){
		if($("select.supplierSelector").length){
			var acc_code = $("select.supplierSelector option:selected").val();
			if(acc_code.substr(0,6) != '010104'){
				return;
			}
			$.post("db/get-account-balance.php",{supplierAccCode:acc_code},function(data){
				data = $.parseJSON(data);
				$("input.customer-balance").val(data['AMOUNT']);
			});
		}
	}
	var transcationCounter = function(){
		$("tr.barcode_me").each(function(i,e){
			var sr = i + 1;
			$(this).find('td.sr_td').text(sr);
		});
	}
	var recover_balance = function(thiss){
		if($(thiss).is(":checked")){
			balance_recovered = "Y";
		}else{
			balance_recovered = "N";
		}
		$(this).calculateColumnTotals();
	}
	var sendSmsPopUp = function(){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel' style='width:280px;'><p class='confirm text-left'>Please Confirm Mobile Number <button id='close-pop' class='pull-right btn btn-xs btn-danger'> <i class='fa fa-times'></i> </button> </p>  <input class='form-control confirmed-mobile pull-left text-center' style='width:150px;margin-left:25px;' value='"+$("input.customer_mobile").val()+"' /> <a class='nodelete btn btn-info'>Send</a></div>");
		centerThisDiv("#popUpDel");
		$("#popUpDel").hide();
		$("#xfade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$("#close-pop").click(function(){
			$("#popUpDel").fadeOut();
			$("#xfade").fadeOut();
		});
		$("#xfade").click(function(){
			$("#popUpDel").fadeOut();
			$("#xfade").fadeOut();
		});
		$(".nodelete").click(function(){
			sendSmsInit();
		});
	};
	var sendSmsInit = function(){
		$("#popUpDel input[type=text]").remove();
		$("#popUpDel p.confirm").text("Please Wait...");
		var mobile_number = ($("input.confirmed-mobile").length)?$("input.confirmed-mobile").val():"";
		if(mobile_number == ''){
			return;
		}
		var sale_id 	  = $("input.sale_id").val();
		$.post("mobile-sale-details.php",{mobile_number:mobile_number,sale_id:sale_id},function(data){
			if(data == 'N'){
				data = "Error! Cannot send sms."
			}
			$("#popUpDel p.confirm").text(data);
			$("#popUpDel").css({"width":"350px"});
			$("#popUpDel").centerThisDiv();
			$("input.confirmed-mobile").val('');
			$("input.confirmed-mobile").hide();
			$(".nodelete").text("OK");
			$(".nodelete").click(function(){
				$("#popUpDel").fadeOut();
				$("#xfade").fadeOut();
			});
		});
	};
	var get_bank_rate = function(){
		var acc_code = $("select.supplierSelector option:selected").val();
		var bnk  		 = $("input[name=radiog_dark]:checked").val();
		if(acc_code != ''&& bnk == 'B'){
			$.post('mobile-sale-details.php',{get_bank_rate:acc_code},function(data){
				data = JSON.parse(data);
				$("input.card_charges_applies").val(data['MERCH_RATE']).next().text(data['MERCH_RATE']+"%");
			});
		}else{
			$("input.card_charges_applies").val(0).next().text('');
		}
	}
