//var $ = $.noConflict(true);
$(document).ready(function(){
		$(".h3_php_error").click(function(){
			$(".h3_php_error").fadeOut('slow');
		});
		$("input[type='submit']").click(function(){
					$("#xfade").fadeIn('fast');
		});
		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);
    // Alternating table rows:
		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
    // Check all checkboxes when the one in a table head is checked:
		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.numericOnly = function(){
				$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 190
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 120 || e.keyCode == 110 || e.keyCode == 190
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.signedNumericOnly = function(){
				$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 120 || e.keyCode == 110
				||  e.keyCode == 173 || e.keyCode == 109 || e.keyCode == 189
				|| e.keyCode == 190 || e.keyCode == 173
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$(".stitchRate").numericOnly();
		$(".length,.embRate").signedNumericOnly();
				$(".insert_prod_title").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_quality']").focus();
					}
				});
				$("input[name='prod_quality']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_color']").focus();
					}
				});
				$("input[name='prod_color']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='prod_than']").focus();
					}
				});
				$("input[name='prod_than']").keyup(function(e){
					if(e.keyCode==13){
						e.preventDefault();
						$("input[name='measure']").focus();
					}else{
						var lent = $(this).val();
						var thaanNum = $(".prod_len").val();
						$(".total_ammount").text(lent*thaanNum);
					}
				});
				$(".get_code_for_title").blur(function(){
					var codeCode = $(this).val();
					if(codeCode==""){
						$(".insert_title").val("");
					}
				});
				$(".prod_len").keyup(function(){
					var lent = $(this).val();
					var thaanNum = $(".prod_than").val();
					$(".total_ammount").text(lent*thaanNum);
				});
			$("input[type='text']").attr("autocomplete","off");
			$(".getCustomerTitle").keyup(function(){
				var codeCode = $(this).val();
					$.post('db/get-customer-title.php',{title:codeCode},function(data){
						var gotCode = $.parseJSON(data);
						var titles = [];
						$.each(gotCode,function(index,value){
							$.each(value,function(i,v){
								if(i=='CUST_ACC_TITLE'){
									titles.push(v);
								}
							});
						});
						$(".getCustomerTitle").autocomplete({
								source: titles,
								width: 300,
								scrollHeight:220,
								matchContains: true,
								selectFirst: false,
								minLength: 1
						});
						$.each(titles,function(i,v){
							if(codeCode==v){
								$(".insertCustomerCode").val(gotCode[0]['CUST_ACC_CODE']);
							}else{
								$(".insertCustomerCode").val("");
							}
						});
					});
			});
			$.fn.getProductQualities = function(){
				var lotNum 			 = $(this).val();
				var outward_date = $("input.lot_date").val();
				var product_id 	 = $("select.product_id  option:selected").val();
				var customerCode = $("select.custCodeSelector option:selected").val();
				$.post('db/get-product-qualities.php',{
					getDetail:true,
					outward_date:outward_date,
					lotNum:lotNum,
					product_id:product_id,
					customerCode:customerCode
					},
					function(data){
						var details = $.parseJSON(data);
						if(details['ERROR']==1){
							displayMessage('Error! Lot # not found / incorrect lot #.');
							return;
						}
						$("select.qualityDropDown").find("option").remove();
						$(".thaanLengthOS").attr("inwardOS",details['T_TOTAL']).val(details['T_TOTAL']);
						$.each(details['QUALITY_LIST'],function(index,quality_name) {
							$("select.qualityDropDown").append("<option value='"+quality_name+"'>"+quality_name+"</option>");
						});
						$("select.qualityDropDown").selectpicker("refresh");
						$("div.qualityDropDown button").focus();
					});
			};
			$.fn.getProductQualitiesForUpdateForm = function(){
				//wehen updating get this rows prev length added in available lot stock.
				var lotNum 		 	= $(this).val();
				var outward_date 	= $("#popUpForm input[name='lot_date']").val();
				var product_id		= $("#popUpForm select[name='productCode']").val();
				var customerCode 	= $("#popUpForm select[name='cutomerCode'] option:selected").val();
				var lot_detail_id 	= $("#popUpForm .lot_detail_id").val();
				$.post('db/get-product-qualities.php',{
					getDetail:true,
					outward_date:outward_date,
					lotNum:lotNum,
					product_id:product_id,
					customerCode:customerCode
					},
					function(data){
						if(details['ERROR']==1){
							displayMessage('Error! Lot # not found / incorrect lot #.');
							return;
						}
						var details = $.parseJSON(data);
						$("#popUpForm select.qualityDropDown").find("option").remove();
						$("#popUpForm .length_os").attr("inwardOS",details['T_TOTAL']).val(details['T_TOTAL']);
						$("#popUpForm select.quality option").remove();
						$.each(details['QUALITY_LIST'],function(index,quality_name){
							$("#popUpForm select.quality").append("<option value='"+quality_name+"'>"+quality_name+"</option>");
						});
						$("#popUpForm select.quality").selectpicker("refresh");
						$("#popUpForm div.quality button").focus();
				});
			};
			$.fn.editLotDetails = function(){
				var idValue 	= $(this).attr("do");
				$.post('pop-up-form/update-lot-register-detail.php',{ oDetail_id:idValue },function(data){
					$("#popUpForm").html(data);
					$("#popUpForm").width(590);
					$("#popUpForm").centerThisDiv();
					$("#xfade").fadeIn();
					$("#popUpForm").fadeIn(function(){
						$("#popUpForm select").selectpicker();
					});
					$("#popUpForm .closePopUpForm").click(function(){ $("#popUpForm").fadeOut();$("#xfade").fadeOut();$("#popUpForm").html(""); });
					$(".input_readonly").css({'cursor':'default'});
					$(".input_readonly").focus(function(){
						$(this).blur();
					});
					$("#popUpForm input[name='stitchRate']").numericOnly();
					$("#popUpForm input[name='length'],#popUpForm input[name='embRate']").signedNumericOnly();
					$("#popUpForm input[name='length']").keyup(function(e){
						$(".length_os").val($(".length_os").attr("data-lot-os")-$("#popUpForm input[name='length']").val());
						var len 				= parseInt($("#popUpForm input[name='length']").val()) || 0;
						var outStanding = parseInt($(".length_os").attr("data-lot-os")) || 0;
						if( len > outStanding){
							$(".length_os").val(outStanding);
							$("#popUpForm input[name='length']").val('');
						}
					});
					$("#popUpForm .lotNumber").keydown(function(e){
						if(e.keyCode == 13 || e.keyCode == 9){
							$("#popUpForm .lotNumber").getProductQualitiesForUpdateForm();
						}
					});
					$("#popUpForm input[name='embRate']").keyup(function(){
						$("input[name='thanLengthOs']").val($("input[name='thanLengthOs']").attr("thanOutStanding")-$("#popUpForm input[name='length']").val());
					});
					$("#popUpForm input[name='stitchRate']").keyup(function(){
						$("input[name='thanLengthOs']").val($("input[name='thanLengthOs']").attr("thanOutStanding")-$("#popUpForm input[name='length']").val());
					});

					$("#popUpForm input[name=lot_date]").datepicker({
						format: 'dd-mm-yyyy',
						autoclose:true
					}).on('changeDate',function(ev){
						$(this).datepicker('hide');
						$(this).trigger('change');
					});
					$("#popUpForm input[name=lot_date]").focus();
					$("#popUpForm input[name='lot_date']").setFocusTo("#popUpForm div.cutomerCode button");
					$("#popUpForm div.cutomerCode button").setFocusTo("#popUpForm div.productCode button");
					$("#popUpForm div.productCode button").setFocusTo("#popUpForm input[name='lotNum']");
					$("#popUpForm input[name='lotNum']").setFocusTo("#popUpForm div.quality button");
					$("#popUpForm div.quality button").setFocusTo("#popUpForm div.measure button");
					$("#popUpForm div.measure button").setFocusTo("#popUpForm div.billing_type button");
					$("#popUpForm div.billing_type button").setFocusTo("#popUpForm input[name='stitches']");
					$("#popUpForm input[name='stitches']").setFocusTo("#popUpForm input[name='embRate']");
					$("#popUpForm input[name='embRate']").setFocusTo("#popUpForm input[name='stitchRate']");
					$("#popUpForm input[name='stitchRate']").setFocusTo("#popUpForm div.stitchAccount button");
					$("#popUpForm div.stitchAccount button").setFocusTo("#popUpForm input[name='designNum']");
					$("#popUpForm input[name='designNum']").setFocusTo("#popUpForm div.machineNum button");
					$("#popUpForm div.machineNum button").setFocusTo("#popUpForm input[name='gpNum']");
					$("#popUpForm input[name='gpNum']").setFocusTo("#popUpForm input[type='submit']");

					$("#popUpForm select.billing_type2,#popUpForm input[name='length'],#popUpForm input[name='stitchRate'],#popUpForm input[name='stitches2'],#popUpForm input[name='embRate']").change(function(){
						embroideryFormulasPopup();
					});

					$(".lengthColumn").sumColumn(".sumLenTotal");
					$(".embAmountColumn").sumColumn(".embAmountTotal");
					$(".stichAmountColumn").sumColumn(".stichAmountTotal");

					$("#popUpForm input[name=update]").click(function(){
						save_popup_form();
						$(this).hide();
					});
				});
			};
			$.fn.quickSubmit = function(){
				//P for issue/Processing  AND R For Claim/Refund
				var goodToGo = true;
				var claimOrIssue = 'P';
				if($(".claimCheck").is(":checked")){
					claimOrIssue = 'R';
				}
				var cust_acc_code       = $("select.custCodeSelector option:selected").val();
				var lot_date 						= $("input.lot_date").val();
				var pCode 	 						= parseInt($("select.product_id option:selected").val())||0;
				var pTitle 	 						= $("select.product_id option:selected").text();
				var lotNum 	 						= $("input.lotNum").val();
				var pQuality 						= $("select.qualityDropDown option:selected").val();
				var pMeasure 						= $("select.measure option:selected").val();
				var pMeasureName 				= $("select.measure option:selected").text();
				var billing_type 				= $("select.billing_type option:selected").val();
				var billing_type_name 	= $("select.billing_type option:selected").text();
				var stitches 						= $("input.stitches").val();
				var total_laces 				= $("input.total_laces").val();
				var pLength 						= $("input.length").val();
				var embRate 						= $("input.embRate").val();
				var embAmount 					= $("input.embAmount").val();

				if(claimOrIssue == 'R' &&  embAmount >= 0){
					goodToGo = false;
					$(".embAmount").css({'background':'#F00','color':'#FFF'});
				}else{
					$(".embAmount").css({'background':'#FFF','color':'#666'});
				}
				if(claimOrIssue == 'P' &&  embAmount < 0){
					goodToGo = false;
					$(".embAmount").css({'background':'#F00','color':'#FFF'});
				}else{
					$(".embAmount").css({'background':'#FFF','color':'#666'});
				}
				var stitchRate 					= $(".stitchRate").val();
				var stitchAmount 				= $(".stitchAmount").val();
				var stitchAccount 			= $("select[name=stitchAccount] option:selected").val();
				var stitchAccountName 	= $("select[name=stitchAccount] option:selected").text();
				var designNum 					= $(".designNum").val();
				var machineNum 					= parseInt($("select.machine_id option:selected").val())||0;
				var machineName 				= $("select.machine_id option:selected").text();
				var gp_Number  					= $(".gp_no").val();

				if(pCode == 0){
					return;
				}
				$(this).blur();
				if(goodToGo){
						$.post('db/insert-lot-details.php',{
							insert: true,
							lot_status:claimOrIssue,
							cust_acc_code:cust_acc_code,
							lot_date:lot_date,
							product_id: pCode,
							pTitle:pTitle,
							lotNum:lotNum,
							pQuality:pQuality,
							pMeasure:pMeasure,
							billing_type:billing_type,
							stitches:stitches,
							total_laces:total_laces,
							pLength:pLength,
							embRate:embRate,
							embAmount:embAmount,
							stitchRate:stitchRate,
							stitchAmount:stitchAmount,
							stitchAccount:stitchAccount,
							designNum:designNum,
							machineNum:machineNum,
							gp_Number:gp_Number
						},function(data){
							var new_id = data.replace(/\s+/g, '');
							if(new_id != 0){
								var row = '<tr class="alt-row calculations">'
										+ '<td class="text-left">'+pTitle+'</td>'
										+ '<td>'+lotNum+'</td>'
										+ '<td>'+pQuality+'</td>'
										+ '<td>'+pMeasureName+'</td>'
										+ '<td>'+billing_type_name+'</td>'
										+ '<td>'+stitches+'</td>'
										+ '<td>'+total_laces+'</td>'
										+ '<td class="lengthColumn">'+pLength+'</td>'
										+ '<td>'+embRate+'</td>'
										+ '<td class="embAmountColumn">'+embAmount+'</td>'
										+ '<td>'+stitchRate+'</td>'
										+ '<td class="stichAmountColumn">'+stitchAmount+'</td>'
										+ '<td>'+stitchAccountName+'</td>'
										+ '<td>'+designNum+'</td>'
										+ '<td>'+machineName+'</td>'
										+ '<td>'+gp_Number+'</td>'
										+ '<td>- - -</td>'
										+ '<td><a class="editDetailsFrom" id="view_button" do="'+new_id+'" title="Edit"><i class="fa fa-pencil"></i></a>'
										+ '<a class="pointer" do="'+new_id+'" title="Delete"><i class="fa fa-times"></i></a></td>'
										+ '</tr>';
								$(row).insertAfter($("tbody.removee tr.calculations").last());
								if(claimOrIssue == 'R'){
									$("tr.calculations").last().find('td').css('color','#C30');
								}
								$("a.pointer").click(function(){
									$(this).deleteRow();
								});
								$(".editDetailsFrom").click(function(){
									$(this).editLotDetails();
								});
								$(".quickSubmit input[type='text']").val("");
								$(".quickSubmit .qualityDropDown").find("option").first().nextAll().remove();
								$(".product_id").find('option').first().attr('selected',true).siblings('option').attr('selected',false);
								$(".product_id").selectpicker('refresh');
								$("button.dropdown-toggle").last().focus();
								$("div.product_id button").focus();
							}
						});
				}else{
					$("#popUpDel").remove();
					var myMessage = (claimOrIssue == 'R')?"Amount must be bearing minus(-) sign.":"Amount must be greater than 0.";
					$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>"+myMessage+"</p><a class='nodelete'>Close</a></div>");
					$("#popUpDel").centerThisDiv();
					$("#popUpDel").hide();
					$("#fade").fadeIn('slow');
					$("#popUpDel").fadeIn();
					$(".nodelete").click(function(){
						$("#fade").fadeOut();
						$("#popUpDel").fadeOut(function(){
							$("#popUpDel").remove();
						});
					});
				}
			};
			$.fn.deleteRow = function(){
				var idValue = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Do you want to delete this record ?</p><a class='btn btn-danger dodelete'>Confirm</a><a class='btn btn-info nodelete'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.post("db/del-lot-register.php", {lid : idValue}, function(data){
							if(data==1){
								$("#popUpDel").children(".confirm").text("1 Row Deleted Successfully!");
								$("#popUpDel").children(".dodelete").hide();
								$("#popUpDel").children(".nodelete").text("Close");
								clickedDel.parent().siblings().text("0").parent().slideUp();
							}else{
								$("#popUpDel").children(".confirm").text("Unable to Delete.");
								$("#popUpDel").children(".dodelete").hide();
								$("#popUpDel").children(".nodelete").text("Close");
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
					});
				$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#fade").fadeOut('fast');
				});
			};
			$.fn.insertProductCode = function(insertToElementClassName,file,sendTitle,getCode){
				var thisElm = $(this);
				var codeCode = thisElm.val();
					if(codeCode!=="" & codeCode.length>0){
						$.post(file,{title:codeCode},function(data){
							var gotCode = $.parseJSON(data);
							var pTitles = [];
							$.each(gotCode,function(index,value){
								$.each(value,function(i,v){
									if(i==sendTitle){
										pTitles.push(v);
									}
								});
							});
							$(thisElm).autocomplete({
									source: pTitles,
									scrollHeight:220,
									matchContains: true,
									selectFirst: false
							});
							$.each(pTitles,function(i,v){
								if(codeCode==v){
									$(insertToElementClassName).val(gotCode[0][getCode]);
								}else{
									$(insertToElementClassName).val("");
								}
							});
						});
					}else{
						$(insertToElementClassName).val("");
					}
			};
			$.fn.insertProductCodeUpdate = function(insertToElementClassName,file,sendTitle,getCode){
				var thisElm = $(this);
				var codeCode = thisElm.val();
					if(codeCode!=="" & codeCode.length>0){
						$.post(file,{title:codeCode},function(data){
							var gotCode = $.parseJSON(data);
							var pTitles = [];
							$.each(gotCode,function(index,value){
								$.each(value,function(i,v){
									if(i==sendTitle){
										pTitles.push(v);
									}
								});
							});
							$(thisElm).autocomplete({
									source: pTitles,
									scrollHeight:220,
									matchContains: true,
									selectFirst: false
							});
							$.each(pTitles,function(i,v){
								if(codeCode==v){
									$(insertToElementClassName).val(gotCode[0][getCode]);
								}else{
									$(insertToElementClassName).val("");
								}
							});
						});
					}else{
						$(insertToElementClassName).val("");
					}
			};
			$.fn.sumColumn = function(total){
				var totalThaans = 0;
				$(this).each(function() {
					totalThaans += parseInt($(this).text());
				});
				$(total).text(totalThaans);
			};
			$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
				$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
			};
			$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var productVal = Math.round((thisVal*thatVal)*100)/100;
				$(writeProductToElm).val(productVal);
			};
			$.fn.centerThisDiv = function(){
				var win_hi = $(window).height()/2;
				var win_width = $(window).width()/2;
				win_hi = win_hi-$(this).height()/2;
				win_width = win_width-$(this).width()/2;
				$(this).css({
					'position': 'absolute',
					'top': '50px',
					'left': win_width
				});
			};
			$.fn.setFocusTo = function(Elm){
				$(this).keydown(function(e){
					if(e.keyCode==13){
						$(Elm).focus();
					}
				});
			};
			$(".input_readonly").css({'cursor':'default'});
			$(".input_readonly").focus(function(){
				$(this).blur();
			});
			$(function(){
				$(window).keyup(function(e){
					if(e.keyCode==27){
						$(window).hideOverLay();
					}
				});
			});
			$(".quickSubmit input").keyup(function(e){
				if(e.keyCode==27){
					$(".quickSubmit input[type='text']").val("");
					$(".quickSubmit input.thaanLengthOS").val("");
					$(".quickSubmit .qualityDropDown").find("option").first().nextAll().remove();
					$("button.dropdown-toggle").last().focus();
					$("#popUpDel").slideUp(200,function(){
						$("#popUpDel").remove();
					});
					$("#xfade").fadeOut();
				}
			});
			$.fn.journalizeThis = function(dateElm,accountElm,idElm){
				var thisDate = $(dateElm).val();
				var accountCode = $(accountElm).val();
				var mainId = $(idElm).val();

				$.post('db/createOutwardVoucher.php',{createVoucher:true,thisDate:thisDate,accountCode:accountCode,mainId:mainId},function(data){
					if(data==1){
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Record Posted Successfully!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						setTimeout(function(){
							window.location.href = 'outward-details.php';
						},2000);
					}else{
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Error Occured While Posting Record!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut();
							});
						$(".close_popup").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut('fast');
						});
					}
				});
			};
			$.fn.reverseVoucher = function(outward_id_element,CustomerAccCodeElem,outward_date_element){
				var voucher_id  = $(this).attr('data-voucher-id');
				var outward_id  = $(outward_id_element).val();
				var outward_date= $(outward_date_element).val();
				var accountCode = $(CustomerAccCodeElem).val();

				$.post('db/reverseOutwardVoucher.php',{reverseVoucher:true,outward_date:outward_date,accountCode:accountCode,outward_id:outward_id,voucher_id:voucher_id},function(data){
					if(data==1){
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Reversal Entry Posted Successfully!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						setTimeout(function(){
							window.location.href = 'outward-details.php?wid='+outward_id;
						},2000);
					}else{
						$("#popUpDel").remove();
						$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Error Occured While Reversing Entry!</p><a class='nodelete'>Close</a></div>");
						$("#popUpDel").hide();
						$("#xfade").fadeIn();
						$("#popUpDel").centerThisDiv();
						$("#popUpDel").slideDown();
						$(".nodelete").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut();
							});
						$(".close_popup").click(function(){
							$("#popUpDel").slideUp(function(){
								window.location.reload();
							});
							$("#xfade").fadeOut('fast');
						});
					}
				});
			};
			$.fn.hideOverLay = function(){
				$("#popUpDel,#xfade,#fade").fadeOut();
				$("#popUpForm2,#popUpForm3").fadeOut(function(){
					$("#popUpForm2,#popUpForm3").css({'transform':'scaleY(1.0)'});
				});
				$("#popUpForm2,#popUpForm3").css({'left':'-=500px'});
				$("#popUpForm2,#popUpForm3").css({'transform':'scaleY(0.0)'});
				$("#popUpForm").html("");
			};
});

	var getLotList = function(){
			var lot_date     = ''; //$(".lot_date").val();
			var customerCode = $("select.custCodeSelector option:selected").val();

			if(lot_date == ''){
				lot_date = 'no_date';
			}

			if(customerCode !== ''){
				$("table.prom").slideUp();
				$.post('db/getCompletedLots.php',{getLotList:true,customer_code:customerCode,lot_date:lot_date},function(data){
					$("table.prom").children("tbody.removee").remove();
					$("table.prom").append(data);
					$("table.prom").slideDown();
					$(".content-box-body").slideDown();
					$(".editDetailsFrom").click(function(){
						$(this).editLotDetails();
					});
					$(".innerTdRed").find('td').css('color','#C30');
				});
			}
		}
	var getProcessingLotsByCustomer = function(){
			var lot_date     = '';
			var customerCode = $("select.custCodeSelector option:selected").val();

			if(lot_date == ''){
				lot_date = 'no_date';
			}
			if(customerCode !== ''){
				$("table.prom").slideUp();
				$.post('db/getCompletedLots.php',{getLotList:true,customer_code:customerCode,lot_date:lot_date,no_checks:true},function(data){
					$("table.input_table").children("tbody.removee").remove();
					$("table.input_table").append(data);
					$("tr.innerTdRed").find('td').css('color','#C30');
					$("table.input_table").slideDown();
					$(".content-box-body").slideDown();
					$(".editDetailsFrom").click(function(){
						$(this).editLotDetails();
					});
					$("a.pointer").click(function(){
						$(this).deleteRow();
					});
				});
			}
		}
	var lot_date_before = '';
	var disable_lot_date = function(elem){
			lot_date_before = $(".lot_date").val();
			$(".lot_date").val('');
			$(".lot_date").datepicker('option','disabled',true);
			$(elem).removeAttr('onclick');
			$(elem).attr('onclick','enable_lot_date(this);');
			$(elem).attr('class','button');
		}
	var enable_lot_date = function(elem){
			$(elem).removeAttr('onclick');
			$(elem).attr('onclick','disable_lot_date(this);');
			$(elem).removeClass('button');
			$(elem).addClass('r_button');
			$(".lot_date").datepicker('option','disabled',false);
			$(".lot_date").val(lot_date_before);
		}
	var checkLots = function(thisElement){
		$("input.lotCheckBox").prop("checked",$(thisElement).prop("checked"));
	}
	var processSelectedLots = function(){
		var lotList = '';
		var lotArray = [];
		$("input.lotCheckBox:checked").each(function(index, element) {
			if(index !== 0){
				lotList += ',';
			}
		   lotList += $(this).val();
		   lotArray[index] = $(this).val();
		});
		if(lotList !== ''){
			$.post('db/lot-status.php',{lotList:lotList},function(data){
				if(data == 1){
					$.each(lotArray,function(index,value){
						if(value != ''){
							$(".row-id-"+value).hide();
						}
					});
				}
			});
		}
	}
	var lotProceed = function(thisElement){
		var TypeOfThis = $(thisElement).attr('class');
		var lot_id = $(thisElement).attr("do");
		if(TypeOfThis == 'button-green'){
			var status = 'C';
			$.post('db/lot-status.php',{lot_id:lot_id,status:status},function(data){
				if(data == 0){
					displayMessage("Error! Lot is not qualified for Completion.");
					return;
				}
				if(data == 1){
					$(thisElement).children('i.fa.fa-arrow-right').removeClass('fa-arrow-right').addClass('fa-arrow-left');
					$(thisElement).removeClass('button-green').addClass('button-orange');
					$(thisElement).attr('title','Un-Process');
					$(thisElement).parent('td').prev('td').text('COMPLETED');
					$(thisElement).parent('td').parent('tr').css('background','rgba(0,180,0,0.5)');
				}
			});
		}else if(TypeOfThis == 'button-orange'){
			var status = 'P';
			$.post('db/lot-status.php',{lot_id:lot_id,status:status},function(data){
				if(data == 1){
					$(thisElement).children('i.fa.fa-arrow-left').removeClass('fa-arrow-left').addClass('fa-arrow-right');
					$(thisElement).removeClass('button-orange').addClass('button-green');
					$(thisElement).attr('title','Process');
					$(thisElement).parent('td').prev('td').text('PROCESSING');
					$(thisElement).parent('td').parent('tr').css('background','rgba(200,200,200,0.5)');
				}
			});
		}
	}
	var save_popup_form   = function(){
		var lot_detail_id = parseInt($("#popUpForm input[name=lot_detail_id]").val())||0;
		if(lot_detail_id <= 0){
			return;
		}
		var goodToGo = true;
		var claimOrIssue = 'P';
		var partialCheck = 'N';
		if($("#popUpForm .claimCheck").is(":checked")){
			claimOrIssue = 'R';
		}
		if($("#popUpForm .partialCheck").is(":checked")){
			partialCheck = 'Y';
		}
		var cust_acc_code = $("#popUpForm select.cutomerCode option:selected").val();
		var lot_date 			= $("#popUpForm input[name=lot_date]").val();
		var pCode 	 			= $("#popUpForm select.productCode").val();
		var pTitle 	 			= $("#popUpForm select.productCode option:selected").text();
		var lotNum 	 			= $("#popUpForm input.lotNumber").val();
		var pQuality 			= $("#popUpForm select.quality option:selected").val();
		var pMeasure 			= $("#popUpForm select.measure option:selected").val();
		var pMeasureName 	= $("#popUpForm select.measure option:selected").text();
		var billing_type 	= $("#popUpForm select.billing_type2 option:selected").val();
		var stitches 			= $("#popUpForm input[name=stitches2]").val();
		var total_laces 	= $("#popUpForm input[name=total_laces]").val();
		var pLength 			= $("#popUpForm input[name=length]").val();
		var embRate 			= $("#popUpForm input[name=embRate]").val();
		var embAmount 		= $("#popUpForm input[name=embAmount]").val();

		if(claimOrIssue == 'R' &&  embAmount >= 0){
			goodToGo = false;
			$("#popUpForm input[name=embAmount]").css({'background':'#F00','color':'#FFF'});
		}else{
			$("#popUpForm input[name=embAmount]").css({'background':'#FFF','color':'#666'});
		}
		if(claimOrIssue == 'P' &&  embAmount < 0){
			goodToGo = false;
			$("#popUpForm input[name=embAmount]").css({'background':'#F00','color':'#FFF'});
		}else{
			$("#popUpForm input[name=embAmount]").css({'background':'#FFF','color':'#666'});
		}
		var stitchRate 				= $("#popUpForm input[name=stitchRate]").val();
		var stitchAmount 			= $("#popUpForm input[name=stitchAmount]").val();
		var stitchAccount 		= $("#popUpForm select[name='stitchAccount']").val();
		var stitchAccountName = $("#popUpForm select[name='stitchAccount'] option:selected").text();
		var designNum 				= $("#popUpForm input[name=designNum]").val();
		var machineNum 				= $("#popUpForm select[name=machineNum] option:selected").val();
		var gp_Number  				= $("#popUpForm input[name=gpNum]").val();
		if(!goodToGo){
			return;
		}
		$(this).blur();
		$.post('db/insert-lot-details.php',{
			insert: true,
			lot_details_id:lot_detail_id,
			lot_status:claimOrIssue,
			partialCheck:partialCheck,
			cust_acc_code:cust_acc_code,
			lot_date:lot_date,
			product_id: pCode,
			pTitle:pTitle,
			lotNum:lotNum,
			pQuality:pQuality,
			pMeasure:pMeasure,
			billing_type:billing_type,
			stitches:stitches,
			total_laces:total_laces,
			pLength:pLength,
			embRate:embRate,
			embAmount:embAmount,
			stitchRate:stitchRate,
			stitchAmount:stitchAmount,
			stitchAccount:stitchAccount,
			designNum:designNum,
			machineNum:machineNum,
			gp_Number:gp_Number
		},function(data){
			if($("input.this_file_name").val()=='lot-register.php'){
				window.location.reload();
			}else{
				window.location.href = 'emb-lot-register.php?customer_code='+cust_acc_code
			}
			$("#popUpForm .closePopUpForm").click(function(){ $("#popUpForm").fadeOut();$("#xfade").fadeOut();$("#popUpForm").html(""); });
		});
	};
	var embroideryFormulasPopup = function(){
		if($("#popUpForm select.billing_type2 option:selected").val() == 'S'){
			var stitches 	 		= parseInt($("#popUpForm input[name='stitches2']").val())||0;
			var stitchRate 		= parseFloat($("#popUpForm input[name='stitchRate']").val())||0;
			var len     	 		= parseFloat($("#popUpForm input[name='total_laces']").val())||0;
			var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
			stitch_amount     = parseFloat(stitch_amount)||0;
			stitch_amount  		= (stitch_amount).toFixed(2);
			$("#popUpForm input[name='stitchAmount']").val(stitch_amount);
			$("#popUpForm input[name='embAmount']").val(0);
		}
		if($("#popUpForm select.billing_type2 option:selected").val() == 'L'){
			var stitches 	 		= parseInt($("#popUpForm input[name='stitches2']").val())||0;
			var stitchRate 		= parseFloat($("#popUpForm input[name='stitchRate']").val())||0;
			var len     	 		= parseFloat($("#popUpForm input[name='total_laces']").val())||0;
			var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
			stitch_amount     = parseFloat(stitch_amount)||0;
			stitch_amount  		= (stitch_amount).toFixed(2);
			$("#popUpForm input[name='stitchAmount']").val(stitch_amount);

			var emb_rate 	  = parseFloat($("#popUpForm input[name='embRate']").val())||0;
			var total_laces = parseFloat($("#popUpForm input[name='total_laces']").val())||0;
			var emb_amount  = emb_rate * total_laces;
			emb_amount      = parseFloat(emb_amount)||0;
			emb_amount  	  = (emb_amount).toFixed(2);
			$("#popUpForm input[name='embAmount']").val(emb_amount);
		}
		if($("#popUpForm  select.billing_type2 option:selected").val() == 'Y'){
			var emb_rate 	 = parseFloat($("#popUpForm input[name='embRate']").val())||0;
			var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;
			var emb_amount = ( emb_rate * 15 ) * len;
			emb_amount     = parseFloat(emb_amount)||0;
			emb_amount  		= (emb_amount).toFixed(2);

			$("#popUpForm input[name='embAmount']").val(emb_amount);
			$("#popUpForm input[name='stitchAmount']").val(0);
		}
		if($("#popUpForm select.billing_type2 option:selected").val() == 'U'){
			var emb_rate 	 = parseFloat($("#popUpForm input[name='embRate']").val())||0;
			var len     	 = parseFloat($("#popUpForm input[name='total_laces']").val())||0;

			var emb_amount = emb_rate * len;
			emb_amount     = parseFloat(emb_amount)||0;
			emb_amount  	= (emb_amount).toFixed(2);

			$("#popUpForm input[name='embAmount']").val(emb_amount);
			$("#popUpForm input[name='stitchAmount']").val(0);
		}
	};
	var embroideryFormulas = function(){
		if($("select.billing_type option:selected").val() == 'S'){
			var stitches 	 		= parseInt($("input.stitches").val())||0;
			var stitchRate 		= parseFloat($("input.stitchRate").val())||0;
			var len     	 		= parseFloat($("input.total_laces").val())||0;
			var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
			stitch_amount     = parseFloat(stitch_amount)||0;
			stitch_amount  		= (stitch_amount).toFixed(2);
			$("input.stitchAmount").val(stitch_amount);
			$("input.embAmount").val(0);
		}
		if($("select.billing_type option:selected").val() == 'L'){
			var stitches 	 		= parseInt($("input.stitches").val())||0;
			var stitchRate 		= parseFloat($("input.stitchRate").val())||0;
			var len     	 		= parseFloat($("input.total_laces").val())||0;
			var stitch_amount = ( ( (stitches*15)/1000 ) * stitchRate ) * len;
			stitch_amount     = parseFloat(stitch_amount)||0;
			stitch_amount  		= (stitch_amount).toFixed(2);
			$("input.stitchAmount").val(stitch_amount);

			var emb_rate 	 = parseFloat($("input.embRate").val())||0;
			var total_laces= parseFloat($("input.total_laces").val())||0;
			var emb_amount = emb_rate * total_laces;
			emb_amount     = parseFloat(emb_amount)||0;
			emb_amount  	 = (emb_amount).toFixed(2);
			$("input.embAmount").val(emb_amount);
		}
		if($("select.billing_type option:selected").val() == 'Y'){
			var emb_rate 	 = parseFloat($("input.embRate").val())||0;
			var len     	 = parseFloat($("input.total_laces").val())||0;
			var emb_amount = ( emb_rate * 15 ) * len;
			emb_amount     = parseFloat(emb_amount)||0;
			emb_amount  	 = (emb_amount).toFixed(2);
			$("input.embAmount").val(emb_amount);
			$("input.stitchAmount").val(0);
		}
		if($("select.billing_type option:selected").val() == 'U'){
			var emb_rate 	 = parseFloat($("input.embRate").val())||0;
			var len     	 = parseFloat($("input.total_laces").val())||0;
			var emb_amount = emb_rate * len;
			emb_amount     = parseFloat(emb_amount)||0;
			emb_amount  	 = (emb_amount).toFixed(2);
			$("input.embAmount").val(emb_amount);
			$("input.stitchAmount").val(0);
		}
	};
