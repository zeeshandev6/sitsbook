//posting item data to add-performa-invoice.php
$(document).on('click', 'input[name=save]', function(){
    $(this).prop("disabled",true);
    $('select[name=stage]').selectpicker('refresh');
    if($('input[name=scid]').val() == ''){
        //$("#validation_message").modal("show");
        //$("#validation_message .message_txt").text("PO Number is required!");
        //$(this).prop("disabled",false);
        //return false;
    }
    if($("#itemRecord").length == 0){
        $("#validation_message").modal("show");
        $("#validation_message .message_txt").text("At least One Item is required!");
        $(this).prop("disabled",false);
        return false;
    }
    //table data
    var jsonString;
    var arrReturn   = {};
    var submit_form = true;
    $('tr#itemRecord').each(function(x){
        var itemQty_receiptNewx = $(this).find('td#qty_receipt').attr('data-id');
        var row_id              = $(this).attr('data-id');
        itemQty_receiptNewx     = (itemQty_receiptNewx == undefined)?0:itemQty_receiptNewx;
        row_id                  = (row_id == undefined)?0:row_id;
        arrReturn[x] = {
            rowId:              row_id,
            itemName:           $(this).find('td#item').html(),
            itemValue:          (parseInt($(this).find('td#item').attr('data-id'))||0),
            itemCarton:         $(this).find('td#carton').html(),
            itemQty_carton:     $(this).find('td#qty_carton').html(),
            itemQty_receipt:    $(this).find('td#qty_receipt').html(),
            unit_cost:          $(this).find('td#unit_cost').html(),
            total_cost:         $(this).find('td#total_cost').html(),
            itemQty_receiptNew: itemQty_receiptNewx,
            itemStock_hand:     $(this).find('td#stock_hand').attr('data-id')
        };
        if((parseInt($(this).find('td#item').attr('data-id'))||0) == 0){
            submit_form = false;
        }
    });
    jsonString = JSON.stringify(arrReturn);
    if(submit_form){
        $('input[name=details]').val(jsonString);
        $('form[name=add-history]').submit();
        $('#myLoading').modal('show');
    }
    $(this).prop("disabled",false);
});
$(document).ready(function(){
    $("#pro_table").change(function(){
        calcoltotal();
    });
    var qtyStock = '';
    var stock    = '';
    $('select.itemSelector').on('change', function(){
      var items_vall  = parseInt($('select.itemSelector').val())||0;
      var godown_id   = parseInt($("select[name=location] option:selected").val())||0;
      if(items_vall == 0){
          return;
      }
      $.post('db/get-item-stock.php', {item_id:items_vall,godown_id:godown_id}, function(data){
          stock = $.parseJSON(data);
          qtyStock = stock['QTY'];
          $('input.stock_hand,input[name=stock_hand]').val(qtyStock);
          $("#pro_table").change();
      });
    });
    $.fn.barcodeAvailable = function(){
  		var $thisElm = $(this);
  		var item_id  = parseInt($("input[name=itemID]").val())||0;
  		$(this).on('keyup blur',function(e){
  			$.get('db/compare-barcode.php',{q:$(this).val(),id:item_id},function(data){
  				if(data > 0){
  					$("#popUpBox input:submit").prop('disabled',true);
  					$($thisElm).addClass('error');
  				}else{
  					$("#popUpBox input:submit").prop('disabled',false);
  					$($thisElm).removeClass('error');
  				}
  			});
  		});
  	};
    $.fn.serializeObject = function(){
  	    var o = {};
  	    var a = this.serializeArray();
  	    $.each(a, function() {
  	        if (o[this.name] !== undefined) {
  	            if (!o[this.name].push) {
  	                o[this.name] = [o[this.name]];
  	            }
  	            o[this.name].push(this.value || '');
  	        } else {
  	            o[this.name] = this.value || '';
  	        }
  	    });
  	    return o;
  	};
});
function calcoltotal(){
    var cartons = 0;
    var qty     = 0;
    var cost_sum= 0;
    $("#pro_table tbody tr").each(function(){
        cartons += (parseFloat($(this).find("td#carton").text())||0);
        qty     += (parseFloat($(this).find("td#qty_receipt").text())||0);
        cost_sum+= (parseFloat($(this).find("td#total_cost").text())||0);
    });
    $("#pro_table tfoot tr td.cartons_total").text(cartons);
    $("#pro_table tfoot tr td.qty_total").text(qty);
    $("#pro_table tfoot tr td.costa_total").text((cost_sum).toFixed(2));
}
function calculation(){
    $("input[name=carton], input[name=qty_carton], input[name=unit_cost]").on('keyup', function(){
        var carton      = parseFloat($('input[name=carton]').val()) || 0;
        var qty_carton  = parseFloat($('input[name=qty_carton]').val()) || 0;
        var unit_cost   = parseFloat($('input[name=unit_cost]').val()) || 0;
        var quantity    = carton*qty_carton;
        $('input[name=qty_receipt]').val(quantity);
        $('input[name=total_cost]').val((quantity*unit_cost).toFixed(2));
    });
    $("input[name=qty_carton],input[name=qty_receipt]").on('blur keyup', function(){
        var qty_receipt = parseFloat($('input[name=qty_receipt]').val()) || 0;
        var stock_hand  = parseFloat($('input.stock_hand').val()) || 0;
        var total       = stock_hand-qty_receipt;
        if(total<0&&stock_check=='Y'){
          $('input[name=qty_carton]').val('');
          $('input[name=qty_receipt]').val(0);
          return;
        }
        $('input[name=stock_hand]').val(total);
    });
}
function calculation1(){
    $("input[name=carton1], input[name=qty_carton1], input[name=unit_cost1]").on('keyup', function(){
        var carton      = parseFloat($('input[name=carton1]').val()) || 0;
        var qty_carton  = parseFloat($('input[name=qty_carton1]').val()) || 0;
        var unit_cost   = parseFloat($('input[name=unit_cost1]').val()) || 0;
        var quantity    = carton*qty_carton;
        $('input[name=qty_receipt1]').val(quantity);
        $('input[name=total_cost1]').val((quantity*unit_cost).toFixed(2));
    });
    $("input[name=qty_carton1]").on('blur', function(){
        var qty_receipt = parseFloat($('input[name=qty_receipt1]').val()) || 0;
        var stock_hand  = parseFloat($('input[name=stock_hand1]').val()) || 0;
        var total       = qty_receipt+stock_hand;
        $('input[name=stock_hand1]').val(total);
    });
}
//delete item row
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).parent().parent().attr("data-id");
        $("form[name=add-history]").append('<input type="hidden" value="'+id+'" name="deleted_rows[]" />');
        $(x).parent().parent().remove();
        x = null;
        $("#pro_table").change();
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
//adding neow row in detail table
$(document).ready(function(){
    calculation();
    $('select[name=item]').selectpicker();
    $('#pro_table input').blur(function(){
        if( $(this).val().length === 0 ) {
            $(this).css({'border-color':'#b94a48', 'box-shadow':'0 0 5px #b94a48'});
        }
        if( $(this).val().length != 0 ) {
            $(this).css({'border-color':'#cccccc','box-shadow':'none'});
        }
    });
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow        = $(this).parent().parent();

        //initializing vars with user values
        var items_vall      = $('select.itemSelector option:selected').val();
        var items_txtt      = $('select.itemSelector option:selected').text();
        var carton          = $('input[name=carton]').val();
        var qty_carton      = $('input[name=qty_carton]').val();
        var qty_receipt     = $('input[name=qty_receipt]').val();
        var unit_cost       = $('input[name=unit_cost]').val();
        var total_cost      = $('input[name=total_cost]').val();
        var qty_receiptNew  = 'new';
        var stock_hand      = $('input[name=stock_hand]').val();

        if((items_vall!='') && (carton!='') && (qty_carton!='') && (qty_receipt!='') &&
           (stock_hand!='')){

            var rw  = "<tr id='itemRecord'><td id='item' data-id="+items_vall+">"+items_txtt+"</td><td id='carton' class='text-center'>"+carton+"</td><td id='qty_carton' class='text-center'>"+qty_carton+"</td>" +
                        "<td id='qty_receipt' data-id="+qty_receiptNew+" class='text-center'>"+qty_receipt+"</td><td id='unit_cost' class='text-center'>"+unit_cost+"</td><td id='total_cost' class='text-center'>"+total_cost+"</td><td id='stock_hand' data-id="+stock_hand+" class='text-center'> - - - - - - - </td>";
                rw += "<td class='text-center'>" +
                      '<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> ' +
                      '<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a> </td></tr>';
            $('#pro_table').append(rw);
            $('#pro_table').find("input").val('');
            $('select.itemname').selectpicker('val', '');
            $('select.itemname').selectpicker('refresh');
            $("#pro_table").change();
            $("div.itemname button").focus();
        }
    });//End appending new row
});
//edit table row in update popup
function editMe(x){
    $(".updatemode").removeClass('updatemode');
    var rowId       = $(x).parent().parent().attr('data-id');
    var item        = $(x).parent().parent().find('td#item').text();
    var item_id     = $(x).parent().parent().find('td#item').attr('data-id');
    var carton      = $(x).parent().parent().find('td#carton').text();
    var qty_carton  = $(x).parent().parent().find('td#qty_carton').text();
    var qty_receipt = $(x).parent().parent().find('td#qty_receipt').text();
    var unit_cost   = $(x).parent().parent().find('td#unit_cost').text();
    var total_cost  = $(x).parent().parent().find('td#total_cost').text();

    var qty_receiptParse = parseInt(qty_receipt);
    var qty_receiptNew = $(x).parent().parent().find('td#qty_receipt').attr('data-id');

    var stock_hand  = $(x).parent().parent().find('td#stock_hand').attr('data-id');
    var stock_handParse = parseInt(stock_hand);

    $('select.itemSelector1').find('option[value='+item_id+']').prop('selected',true).siblings().prop('selected',false);
    $('select.itemSelector1').selectpicker('refresh');
    $('input[name=rowid]').val(rowId);
    $('input[name=carton1]').val(carton);
    $('input[name=qty_carton1]').val(qty_carton);
    $('input[name=qty_receipt1]').val(qty_receipt);
    $('input[name=unit_cost1]').val(unit_cost);
    $('input[name=total_cost1]').val(total_cost);
    $('input[name=qty_receipt1]').attr('data-id',qty_receiptNew);
    $('input[name=stock_hand1]').val(stock_hand);

    $(x).parent().parent().addClass('updatemode');
    $("#myedit").modal('show');

    calculation1();

    $('#addrow').on('click', function(){
        //initializing vars with user values
        var rowid           = $('input[name=rowid]').val();
        var item1_val       = $('select.itemSelector1 option:selected').val();
        var item1_txt       = $('select.itemSelector1 option:selected').text();
        var carton1         = $('input[name=carton1]').val();
        var qty_carton1     = $('input[name=qty_carton1]').val();
        var qty_receipt1    = $('input[name=qty_receipt1]').val();
        var unit_cost1      = $('input[name=unit_cost1]').val();
        var total_cost1     = $('input[name=total_cost1]').val();
        var stock_hand1     = $('input[name=stock_hand1]').val();

        if(qty_receiptNew!='new'){
            qty_receiptNew  = 'new';
        }else{
            qty_receiptNew  = $('input[name=qty_receipt1]').attr('data-id');
        }

        var qty_receipt1Parse   = parseInt(qty_receipt1);
        var stock_hand1Parse    = parseInt(stock_hand1);
        var qty_receipt2        = parseInt('0');

        if(qty_receiptParse!=qty_receipt1Parse){
            stock_hand1Parse = stock_handParse - qty_receiptParse;
            stock_hand1Parse = stock_hand1Parse + qty_receipt1Parse;
            qty_receipt2 = qty_receipt1Parse;
        }else{
            qty_receipt2 = qty_receipt1Parse;
        }
        if((item1_val!='') && (carton1!='') && (qty_carton1!='') && (qty_receipt2!='') && (stock_hand1Parse!='') ){
            var newrw  = "<tr id='itemRecord' data-id="+rowid+" ><td id='item' class='text-left' data-id="+item1_val+">"+item1_txt+"</td><td class='text-center' id='carton'>"+carton1+"</td>"
                        +"<td id='qty_carton' class='text-center'>"+qty_carton1+"</td><td id='qty_receipt' class='text-center' data-id="+qty_receiptNew+">"+qty_receipt2+"</td><td id='unit_cost' class='text-center'>"+unit_cost1+"</td><td id='total_cost' class='text-center'>"+total_cost1+"</td><td id='stock_hand' class='text-center' data-id="+stock_hand1Parse+" > - - - - - - - </td>";
                newrw += "<td class='text-center'>" +
                    '<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> ' +
                    '<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a> </td></tr>';
            $("tr.updatemode").replaceWith(newrw);
            $(".updatemode").removeClass('updatemode');
            $('select[name=item1]').selectpicker('val', '');
            $('select[name=item1]').selectpicker('refresh');
            $("#pro_table").change();
        }
    });
}
var hidePopUpBox = function(){
  $("#popUpBox").css({'transform':'scaleY(0.0)'});
  $("#xfade").fadeOut();
  $("#popUpBox").remove();
};
var add_new_item_submit = function(){
  var formData = $("#popUpBox form").serializeObject();
  $.post('item-details.php',formData,function(data){
    hidePopUpBox();
    $(".reload_item").click();
  });
};
var add_new_item = function(){
  $("body").append('<div id="popUpBox"><button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button></div>');
  $("#popUpBox").append('<form action="" method="post"></form>');
  $.get('item-details.php',{},function(html){
    var formContent = $(html).find(".popup_content_nostyle").html();
    formContent+= $(html).find(".form-submiters").html();
    $("#popUpBox form").append(formContent);
    $("#popUpBox form").append('<input name="addItem" type="hidden" />');
    $("#popUpBox .popuporemov").remove();
    $("#popUpBox .selectpicker").remove();
    $("#popUpBox input:submit").addClass("prop_item_submit");
    $("#popUpBox .prop_item_submit").prop('type','button');
    $("#popUpBox .prop_item_submit").click(function(){
      add_new_item_submit();
    });
    $("#popUpBox select").selectpicker();
    $("#popUpBox input.item-barcode").barcodeAvailable();
    $("#xfade").fadeIn();
    $("#popUpBox").fadeIn('200').centerThisDiv();
  });
};
