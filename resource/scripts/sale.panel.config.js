$(document).ready(function(){
	$("input.total_discount").prop("readonly",true);
	balance_recovered = ($("input.recovered_balance").is(":checked"))?"Y":"N";
	calculations_count_check();
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$(".custom-height-x").height($(window).height() - 200);
	$("select.supplierSelector").change(function(){
		getAccountBalance();
	});
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);
	// Alternating table rows:
	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
	// Check all checkboxes when the one in a table head is checked:
	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			 sumAll += parseInt($(this).text());
		});
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
			 sumAll += parseFloat($(this).text())||0;
		});
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text((sumAll).toFixed(2));
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteMainRow = function(file){
		var idValue    = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		var billNumbr  = currentRow.find('td').first().text();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel .confirm").text("Are Sure you Want To Delete Bill #"+billNumbr+"?");
		$("#popUpDel").slideDown();
		$(".dodelete").click(function(){
			$.post(file, {cid : idValue}, function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					currentRow.slideUp();
				}
				$("#popUpDel .confirm").text(data['MSG']);
				$(".nodelete").text('Close');
				$(".dodelete").hide();
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp(function(){
				$(this).remove();
			});
			$("#xfade").fadeOut();
		});
	};

	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = $("input.taxRate").val();
			var taxType   = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'absolute',
			'top': '8%',
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$("#rDatex").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		var row_id  = 0;
		if(item_id != ''){
			$.post('db/get-item-details.php',{s_item_id:item_id,row_id:0},function(data){
				data = $.parseJSON(data);
				var itemStock = parseInt(data['STOCK'])||0;
				var itemPrice = data['S_PRICE'];
				var itemDisc  = data['DISCOUNT'];
				var this_sold = 0;
				$("td[data-item-id='"+item_id+"']").each(function(){
					if(!$(this).parent().hasClass("updateMode")){
						this_sold -= parseInt($(this).parent().find("td.quantity").text())||0;
					}
				});
				if($(".updateMode").length){
					this_sold += parseFloat($(".updateMode").find("td.quantity").text())||0;
				}
				itemDisc = (itemDisc > 0)?itemDisc:"";
				itemPrice = (itemPrice > 0)?itemPrice:"";
				if($(".updateMode").length == 0){
					$("input.discount").val(itemDisc);
					$("input.unitPrice").val(itemPrice);
				}
				itemStock += this_sold;
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id  = $(".itemSelector option:selected").val();
		var item_name  = $(".itemSelector option:selected").text();
		var quantity = parseInt($("input.quantity").val())||0;
		var unitPrice = $("input.unitPrice").val();
		var discount = parseFloat($("input.discount").val()) || 0;
		var taxRate   = parseFloat($("input.taxRate").val()) || 0;
		var totalAmount = $("input.totalAmount").val();
		var discount_type = $("input.discount_type:checked").val();
		var taxType   = ($(".taxType").is(":checked"))?"I":"E";

		$("input.discount").attr('data-amount','');
		$(this).blur();

		if(quantity == 0){
			displayMessage('Selected Quantity is Zero.');
			return false;
		}
		var subAmount = 0;
		var discountPerCentage = 0;
		subAmount     = quantity*unitPrice;
		subAmount = Math.round(subAmount*100)/100;
		if($("input.individual_discount").val()=='Y'){
			if(discount_type == 'R'){
				discountPerCentage = discount;
			}else if(discount_type == 'P'){
				discountPerCentage = subAmount*(discount/100);
			}
			subAmount -= discountPerCentage;
			subAmount = Math.round(subAmount*100)/100;
		}
		var taxAmount = 0;
		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = subAmount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = subAmount*(taxRate/100);
			}
		}

		var updateMode    = $(".updateMode").length;
		var update_row_id = 0;
		if(updateMode){
			update_row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
		}
		if($("td.itemName[data-item-id='"+item_id+"']").length && updateMode == 0){
			//displayMessage(item_name+' Already Included In Bill!');
			//$(this).clearPanelValues();
			//return false;
		}
		if(item_id > 0 && totalAmount > 0){
			var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="'+update_row_id+'">'
							+'<td style="text-align:center;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>'
							+'<td style="text-align:center;" class="quantity">'+quantity+'</td>'
							+'<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
			if($("input.individual_discount").val()=='Y'){
				$theNewRow	+= '<td class="discount text-center">'+discount+'</td>';
			}
			if($("input.use_taxes").val()=='Y'){
				$theNewRow +='<td style="text-align:center;" class="taxRate"  tax-amount="'+taxAmount+'">'+taxRate+'</td>';
			}
				$theNewRow +='<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
							+'<td style="text-align:center;">'
								+'<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
							+'</td>'
							+'</tr>';
			if(updateMode == 0){
				$(".calculations_body").append($theNewRow);
			}else if(updateMode == 1){
				$(".updateMode").replaceWith($theNewRow);
			}
			$(this).clearPanelValues();
			sum_tax_amount();
			$(this).calculateColumnTotals();
			$(".itemSelector").parent().find(".dropdown-toggle").focus();
		}else{
			displayMessage('Values Missing!');
		}
		calculations_count_check();
	};
	$(".touch-select").click(function(){
		var append_row = ($("input.scanner-append").val() == 'Y')?true:false;
		var s_item_id  = parseInt($(this).attr("data-item-idx"))||0;
		var item_name  = $(this).text();
		if(s_item_id == 0){
			return false;
		}
		$.post('db/get-item-details.php',{s_item_id:s_item_id},function(data){
			data = $.parseJSON(data);
			var item_id   = s_item_id;
			var quantity  = 1;
			var quantity_sold = 0;
			if($("td[data-item-id="+item_id+"]").length){
				$("td[data-item-id='"+item_id+"']").each(function(i,e){
					quantity_sold = (parseFloat(quantity_sold)||0) + (parseFloat($(this).parent().find(".quantity").text())||0);
				});
			}
			quantity_sold       = (parseFloat(quantity_sold)||0) + (parseFloat(quantity)||0);
			if(append_row == false){
				quantity = quantity_sold;
			}
			var unitPrice       = data['S_PRICE'];
			var discount  		= 0;
			var discount_amount = 0;
			var subAmount   = (quantity*unitPrice).toFixed(2);
			var taxRate     = 0;
			var taxAmount   = 0;
			var totalAmount = (quantity*unitPrice).toFixed(2);
			var available   = data['STOCK_QTY'] - quantity_sold;
			var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="0">'
							+'<td style="text-align:center;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>'
							+'<td style="text-align:center;" class="quantity">'+quantity+'</td>'
							+'<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
				if($("input.individual_discount").val()=='Y'){
					$theNewRow	+= '<td class="discount text-center">'+discount+'</td>';
				}
				if($("input.use_taxes").val()=='Y'){
					$theNewRow	+= '<td style="text-align:center;" class="taxRate"  tax-amount="'+taxAmount+'">'+taxRate+'</td>';
				}
				$theNewRow +=''
							+'<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
							+'<td style="text-align:center;">'
								+'<a class="pointer ml-5" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
							+'</td>'
							+'</tr>';
			if(append_row){
				$(".calculations_body").append($theNewRow);
			}else{
				if($("td[data-item-id="+item_id+"]").length){
					$("td[data-item-id="+item_id+"]").parent('tr').replaceWith($theNewRow);
				}else{
					$(".calculations_body").append($theNewRow);
				}
			}
			$(this).calculateColumnTotals();
			calculations_count_check();
		});
	});
	$.fn.quickScan = function(){
		var append_row =($("input.scanner-append").val() == 'Y')?true:false;
		var barcode  = $("input.barcode_input").val();
		$("input.barcode_input").val('');
		if(barcode == ''){
			return false;
		}
		$("input.barcode_input").css({'border':'1px solid #CCCCCC'});
		$.get('db/get-item-details.php',{barcode:barcode},function(data){
			if(data == ''||data==null){
				$("input.barcode_input").css({'border':'1px solid red'});
				return false;
			}
			data = $.parseJSON(data);
			var item_id   = data['ID'];
			var item_name = data['NAME'];
			var quantity  = 1;
			var quantity_sold = 0;
			if($("td[data-item-id="+item_id+"]").length){
				$("td[data-item-id='"+item_id+"']").each(function(i,e){
					quantity_sold = (parseFloat(quantity_sold)||0) + (parseFloat($(this).parent().find(".quantity").text())||0);
				});
			}
			quantity_sold       = (parseFloat(quantity_sold)||0) + (parseFloat(quantity)||0);
			if(append_row == false){
				quantity = quantity_sold;
			}
			var unitPrice       = data['SALE_PRICE'];
			var discount  		= 0;
			var discount_amount = 0;
			var subAmount   = (quantity*unitPrice).toFixed(2);
			var taxRate     = 0;
			var taxAmount   = 0;
			var totalAmount = (quantity*unitPrice).toFixed(2);
			var available   = data['STOCK_QTY'] - quantity_sold;
			var $theNewRow = '<tr class="alt-row calculations transactions dynamix" data-row-id="0">'
							+'<td style="text-align:center;" class="itemName" data-item-id="'+item_id+'">'+item_name+'</td>'
							+'<td style="text-align:center;" class="quantity">'+quantity+'</td>'
							+'<td style="text-align:center;" class="unitPrice" sub-amount="'+subAmount+'">'+unitPrice+'</td>';
				if($("input.individual_discount").val()=='Y'){
					$theNewRow	+= '<td class="discount text-center">'+discount+'</td>';
				}
				if($("input.use_taxes").val()=='Y'){
					$theNewRow	+= '<td style="text-align:center;" class="taxRate"  tax-amount="'+taxAmount+'">'+taxRate+'</td>';
				}
				$theNewRow +=''
							+'<td style="text-align:center;" class="totalAmount">'+totalAmount+'</td>'
							+'<td style="text-align:center;">'
								+'<a class="pointer ml-5" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>'
							+'</td>'
							+'</tr>';
			if(append_row){
				$(".calculations_body").append($theNewRow);
			}else{
				if($("td[data-item-id="+item_id+"]").length){
					$("td[data-item-id="+item_id+"]").parent('tr').replaceWith($theNewRow);
				}else{
					$(".calculations_body").append($theNewRow);
				}
			}
			$(this).calculateColumnTotals();
			calculations_count_check();
			$("input.barcode_input").focus();
		});
	};
	$.fn.calculateColumnTotals = function(){
		var discount_type    = $("input.discount_type:checked").val();
		var transaction_type = $("input[name=radiog_dark]:checked").val();
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.unitPrice","td.price_total");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
		var amountTotal = parseFloat($("td.amountTotal").text())||0;

		var disk = 0;
		var rate = 0;
		var qty  = 0;
		var sub  = 0;
		if(discount_type == 'R'){
			$("td.discount").each(function(){
				disk += parseFloat($(this).text())||0;
			});
		}else{
			var dit = 0;
			$("tr.transactions").each(function(){
				qty  = parseFloat($(this).find("td.quantity").text())||0;
				rate = parseFloat($(this).find("td.unitPrice").text())||0;
				dit  = parseFloat($(this).find("td.discount").text())||0;

				disk +=  parseFloat(((qty*rate*dit) / 100))||0;
			});
		}

		var discount_amount = parseFloat($("input.whole_discount").val())||0;

		if(discount_type == 'P'){
			discount_amount = (amountTotal*discount_amount) / 100;
		}
		amountTotal -= discount_amount;

		$("input.total_discount").val((discount_amount+disk).toFixed(2));

		var charges = parseFloat($("input.inv_charges").val())||0;
		amountTotal += charges;
		$("input.grand_total").val((amountTotal).toFixed(2));

		var received    = parseFloat($("input.received_cash").val())||0;
		var grand_total = $("input.grand_total").val();
		var returnin    = received - grand_total;

		if(transaction_type == 'C'){
			if(received >= grand_total){
				$("input.change_return").val((returnin).toFixed(2));
				$("input.remaining_amount").val("");
			}else{
				$("input.remaining_amount").val("");
				$("input.change_return").val("");
				$("input.received_cash").val("");
			}
		}else{
			if(received < grand_total){
				returnin *= -1;
				$("input.remaining_amount").val((returnin).toFixed(2));
			}else{
				$("input.remaining_amount").val("");
			}
			if(received > grand_total){
				$("input.change_return").val((returnin).toFixed(2));
			}else{
				$("input.change_return").val("");
			}
		}
		var rec_amount = $("input.recovery_amount").val();
		var chg_amount = $("input.change_return").val();

		if(balance_recovered == 'Y'){
			$("input.recovery_amount").val(chg_amount);
			$("input.change_return").val('');
		}else{
			$("input.recovery_amount").val('');
		}
	};
	$.fn.addToThis = function(that_amount){
		var this_amount = parseFloat($(this).val())||0;
		$(this).va(that_amount+this_amount);
	};
	$.fn.calculateRowTotal = function(){
		var taxRate = parseFloat($("input.taxRate").val())||0;
		var taxType = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount  = Math.round((thisVal*thatVal)*100)/100;
		var discountAvail = parseFloat($("input.discount").val())||0;
		var discount_type = $("input.discount_type:checked").val();

		var discountPerCentage = 0;
		amount = Math.round(amount*100)/100;
		if($("input.individual_discount").val()=='Y'){
			if(discount_type == 'R'){
				discountPerCentage = discountAvail;
			}else if(discount_type == 'P'){
				discountAvail = Math.round(discountAvail*100)/100;
				discountPerCentage = amount*(discountAvail/100);
				discountPerCentage = Math.round(discountPerCentage*100)/100;
			}
			$("input.discount").attr('data-amount',discountPerCentage);
			amount -= discountPerCentage;
			amount = Math.round(amount*100)/100;
		}

		var taxAmount = 0;
		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount*(taxRate/100);
			}
		}
		if(taxType == 'I'){
			amount -= taxAmount;
		}else{
			amount += taxAmount;
		}
		$("input.totalAmount").val(amount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
			stockOfBill += parseInt($(this).text())||0;
		});
		stockOfBill += parseInt($("input.quantity").val())||0;
	};

	$.fn.clearPanelValues = function(){
		$(".itemSelector option").prop('selected',false);
		$(".itemSelector").find("option").first().prop('selected',true);
		$(".itemSelector").selectpicker('refresh');

		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.taxRate").val('');
		$("input.subAmount").val('');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
});

var balance_recovered = "";

var letMeGo = function(thisElm){
	$(thisElm).fadeOut(300,function(){
		$(this).html('');
	});
};
var showDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Record is in edit Mode!');
			return false;
		}

		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$("html, body").animate({ scrollTop: 0 }, "fast");
		$(".dodelete").click(function(){
			clickedDel.parent().parent().remove();
			$(this).calculateColumnTotals();
			$(".nodelete").click();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
		});
		$(".close_popup").click(function(){
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('fast');
		});
};
var validatePhone = function(txtPhone) {
	var a = $(txtPhone).val();
	var filter = /^[0-9-+]+$/;
	if(a[0] == 0){
		a = a.substring(1,11);
	}
	if(a[0] != 3){
		return false;
	}
	if(a.length != 10){
		return false;
	}
	if(filter.test(a)) {
		return a;
	}else{
		return false;
	}
};
var sum_tax_amount = function(){
	var tax = 0;
	$("td.taxRate").each(function(){
		tax += parseFloat($(this).attr('tax-amount'))||0;
	});
	$(".whole_tax").val((tax).toFixed(2));
}
var stockOs = function(){
	var thisQty = parseInt($("input.quantity").val())||0;
	var inStock = parseInt($("input.inStock").attr('thestock'))||0;
	var selectedItem = $("select.itemSelector option:selected").val();
	var NewStock = inStock - thisQty;
	if(thisQty <= inStock){
		$("input.inStock").val(NewStock);
	}else{
		//$("input.quantity").val('');
	}
};
var stockOsQuick = function(elm,price,inStock){
	var thisQty = parseInt($(elm).val())||0;
	$(elm).parent().parent().find("td.subAmount").text((thisQty*price).toFixed(2));
	$(elm).parent().parent().find("td.totalAmount").text((thisQty*price).toFixed(2));
};
var editThisRow = function(thisElm){
	var thisRow 	= $(thisElm).parent('td').parent('tr');
	$(".updateMode").removeClass('updateMode');
	thisRow.addClass('updateMode');
	var item_name 	= thisRow.find('td').eq(0).text();
	var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
	var quantity 	= thisRow.find('td').eq(1).text();
	var unitPrice 	= thisRow.find('td').eq(2).text();
	var discount 	= thisRow.find('td').eq(3).text();
	var taxRate 	= parseFloat(thisRow.find('td').eq(4).text())||0;
	var taxAmount   = thisRow.find('td').eq(6).text();
	var totalAmount = thisRow.find('td').eq(7).text();

	$("select.itemSelector option[value='"+item_id+"']").prop('selected',true);
	$("select.itemSelector").selectpicker('refresh');
	$("input.quantity").val(quantity);
	$("input.unitPrice").val(unitPrice);
	$("input.discount").val(discount);

	$("input.taxRate").val(taxRate);
	$("input.taxAmount").val(taxAmount);
	$("input.totalAmount").val(totalAmount);
	$("input.inStock").val('').attr('thestock','');
	$("div.itemSelector button").focus();
};

var saveSale = function(){
	var print_method   = $(".print_method").val();
	var sale_id		   = $(".sale_id").val();
	var saleDate 	   = $("input[name='rDate']").val();
	var billNum  	   = $("input[name='billNum']").val();
	var discount_unit  = parseFloat($(".whole_discount").val())||0;
	var discount_type  = $("input.discount_type:checked").val();
	var total_discount = parseFloat($(".total_discount").val())||0;
	var supplierCode   = $("select.supplierSelector option:selected").val();

	var customer_name  		= $("input.customer_name").val();
	var customer_address  	= $("input.customer_address").val();

	var inv_charges    = parseFloat($("input.inv_charges").val())||0;
	var inv_notes      = $("textarea.inv_notes").val();

	var transaction_type  = $("input[name=radiog_dark]:checked").val();
	var grand_total  	  = parseFloat($("input.grand_total").val())||0;

	var received_cash     = parseFloat($("input.received_cash").val())|0;
	var change_return     = $("input.change_return").val();
	var recovery_amount   = $("input.recovery_amount").val();
	var remaining_amount  = $("input.remaining_amount").val();

	var customer_mobile   = '';

	if(transaction_type == 'C' && grand_total > received_cash){
		displayMessage('Error! Received cash must be equal to bill amount.');
		return false;
	}
	if(supplierCode == ''){
		displayMessage('Please select an Account!');
		return false;
	}
	if($(".customer_mobile").length){
		customer_mobile = $(".customer_mobile").val();
		if($(".customer_mobile").val() != ''){
			if($(".customer_mobile").val().substr(0,1) != 0 || ($(".customer_mobile").val().length > 1 && $(".customer_mobile").val().substr(1,1) != 3) || $(".customer_mobile").val().length != 11){
				displayMessage('Please Enter a valid Moblie Number!');
				return false;
			}
		}
	}
	if($("tr.transactions").length == 0){
		displayMessage('No Transaction Existed, Bill Can not be Saved!');
		return false;
	}
	if(supplierCode == ''){
		displayMessage('Customer Not Selected!');
		return false;
	}
	$(".savePurchase").hide();
	$("#xfade").fadeIn();
	var jSonString = '{';
	$("tr.transactions").each(function(index, element){
		var rowId       = $(this).attr('data-row-id');
		var thisRow     = $(this);
		var item_id 	= parseInt(thisRow.find('td.itemName').attr('data-item-id'))||0;
		var quantity 	= thisRow.find('td.quantity').text();
		var unitPrice 	= thisRow.find('td.unitPrice').text();
		var discount 	= parseFloat(thisRow.find('td.discount').text()) || 0;
		var subAmount 	= parseFloat(thisRow.find('td.unitPrice').attr('sub-amount'))||0;
		var taxRate 	= parseFloat(thisRow.find('td.taxRate').text())||0;
		var taxAmount   = parseFloat(thisRow.find('td.taxRate').attr('tax-amount'))||0;
		var totalAmount = thisRow.find('td.totalAmount').text();
		if(item_id != '' && quantity != 0 && quantity != 0){
			if(index > 0){
				jSonString += ',';
			}
			jSonString += '"'+index+'":';
			jSonString += '{';
			jSonString += '"row_id":"'+rowId+'",'+'"item_id":"'+item_id+'","quantity":"'+quantity+'",';
			jSonString += '"unitPrice":"'+unitPrice+'","discount":"'+discount+'","subAmount":"'+subAmount+'",';
			jSonString += '"taxRate":"'+taxRate+'","taxAmount":"'+taxAmount+'",';
			jSonString += '"totalAmount":"'+totalAmount+'"';
			jSonString += '}';
		}
	});
	jSonString += '}';
	$.post("db/savePosSale.php",{sale_id:sale_id,
							  saleDate:saleDate,
							  customer_mobile:customer_mobile,
							  billNum:billNum,
							  supplierCode:supplierCode,
							  customer_name:customer_name,
							  customer_address:customer_address,
							  inv_charges:inv_charges,
							  inv_notes:inv_notes,
							  bill_amount:grand_total,
							  received_cash:received_cash,
							  balance_recovered:balance_recovered,
							  recovery_amount:recovery_amount,
							  change_return:change_return,
							  remaining_amount:remaining_amount,
							  discount:discount_unit,
							  discount_type:discount_type,
							  total_discount:total_discount,
							  transaction_type:transaction_type,
							  jSonString:jSonString},function(data){
			data = $.parseJSON(data);
			if(data['ID'] > 0){
				var msg_type = '';
				if(sale_id == 0){
					msg_type = "&saved";
				}else{
					msg_type = "&updated";
				}
				var print_opt = '';
				if($(".invoice-type").val()=='S'){
					print_opt = "&print"
				}
				if(print_method=='Y'){
					print_opt = "&print"
				}
				window.location.href = 'sale-panel.php?id='+data['ID']+msg_type+print_opt;
			}else{
				displayMessage(data['MSG']);
				$("#xfade").fadeOut();
			}
	});
};
var barcode_scan_for_sale = function(){
	var barcode = $(".barcode_input").val();
	$(".barcode_input").val('').blur();
	if(barcode == ''){
		$(".barcode_input").focus();
		return false;
	}
	var tr = '';
	$.get('db/scan_item.php',{b_code:barcode},function(data){
		if(data == ''){
			displayMessage('Item Does Not Exist!');
			return false;
		}
		var item_details = $.parseJSON(data);
		if(item_details['OK'] == 'N'){
			displayMessage(item_details['MSG']);
			return false;
		}
		if($("*[data-bi='"+item_details['BI']+"']").length){
			displayMessage('Item Already Scanned!');
			return false;
		}
		tr = '<tr class="alt-row calculations transactions dynamix" data-row-id="0" data-bi="'+item_details['BI']+'">';
		tr += '<td style="text-align:left;" class="itemName" data-item-id="'+item_details['ID']+'">'+item_details['NAME']+'</td>';
		tr += '<td style="text-align:center;" class="quantity">1</td>';
		tr += '<td style="text-align:center;" class="unitPrice">'+item_details['SALE_PRICE']+'</td>';
		tr += '<td style="text-align:center;"><a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a></td>';
		tr += '</tr>';
		$(tr).insertAfter($('.calculations').last());
		tr = '';
		$(this).calculateColumnTotals();
		$(".barcode_input").focus();
	});
};
var check_sms_service = function(){
	$.get('db/check-sms-service.php',{},function(data){
		if(data == 'N'){
			displayMessage('Sms Service Is Not Active!');
			$("input[name='mobile_no']").css({'border':'1px solid red'});
		}else if(data == 'Y'){
			$("input[name='mobile_no']").css({'border':'1px solid #008000'});
		}
	});
};
var save_mobile_sale = function(){
	var sale_id		 = $(".sale_id").val();
	var saleDate = $(".datepicker").val();
	var billNum = $("input[name='billNum']").val();
	var supplier_name = $(".supplier_name").val();
	var supplierCode = $("select.supplierSelector option:selected").val();
	var mobile_no = '';
	if($("input[name='mobile_no']").val() != ''){
		mobile_no = validatePhone("input[name='mobile_no']");
		if(validatePhone("input[name='mobile_no']") == false){
			displayMessage('Please Enter a valid Moblie Number!');
			return false;
		}
	}
	if($("tr.transactions").length == 0){
		displayMessage('No Transaction Existed, Bill Can not be Saved!');
		return false;
	}
	if(supplierCode == ''){
		displayMessage('Customer Not Selected!');
		return false;
	}
	var jSonString = '{';
	$("tr.transactions").each(function(index, element) {
		var rowId       = parseInt($(this).attr('data-row-id'))||0;
		var bi			= parseInt($(this).attr('data-bi'))||0;
		var thisRow     = $(this);
		var item_id 	= parseInt(thisRow.find('td').eq(0).attr('data-item-id'))||0;
		var quantity 	= parseInt(thisRow.find('td').eq(1).text())||0;
		var unitPrice 	= parseInt(thisRow.find('td').eq(2).text())||0;
		var discount 	= 0;
		var subAmount 	= quantity*unitPrice;
		var taxRate 	= 0;
		var taxAmount   = 0;
		var totalAmount = subAmount;
		if(item_id != '' && quantity != 0){
			if(index > 0){
				jSonString += ',';
			}
			jSonString += '"'+index+'":';
			jSonString += '{';
			jSonString += '"row_id":"'+rowId+'",'+'"row_bi":"'+bi+'","item_id":"'+item_id+'","quantity":"'+quantity+'",';
			jSonString += '"unitPrice":"'+unitPrice+'","discount":"'+discount+'","subAmount":"'+subAmount+'",';
			jSonString += '"taxRate":"'+taxRate+'","taxAmount":"'+taxAmount+'",';
			jSonString += '"totalAmount":"'+totalAmount+'"';
			jSonString += '}';
		}
	});

	jSonString += '}';
	$.post("db/saveBarcodeSale.php",{sale_id:sale_id,
									 saleDate:saleDate,
									 billNum:billNum,
									 supplierCode:supplierCode,
									 supplier_name:supplier_name,
									 mobile_no:mobile_no,
									 jSonString:jSonString},function(data){
			data = $.parseJSON(data);
			if(data['ID'] > 0){
				window.location.href = 'sales-invoice.php?id='+data['ID'];
			}else{
				displayMessage(data['MSG']);
			}
	});
};
var makeItCash = function(checker){
	var what = $(checker).is(":checked");
	var this_val = $(checker).val();
	$(".balance-of-customer").hide();
	$(".balance-of-customer input").val(0);
	if(this_val=='A'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").val('').hide();
		getAccountBalance();
		$(".balance-of-customer").show();
	}else if(this_val=='B'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").attr('placeholder','Card Number');
		$("input[name='supplier_name']").show().focus();
	}else if(this_val=='C'){
		$("select.supplierSelector option[value^='010104']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010102']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").attr('placeholder','Customer Name');
		$("input[name='supplier_name']").show().focus();
	}
};
var hidePopUpBox = function(){
	$("#popUpBox").css({'left':'-=500px'});
	$("#popUpBox").css({'transform':'scaleY(0.0)'});
	$("#fade").fadeOut();
	$("#popUpBox").remove();
};
var add_supplier = function(){
	var code_type = $("input[name='radiog_dark']:checked").val();
	if(code_type == 'C'){
		return false;
	}
	var url = '';
	var captions = '';
	if(code_type == 'B'){
		url = 'accounts-management.php'
		captions = 'Bank';
	}else if(code_type == 'A'){
		url = 'customer-detail.php'
		captions = 'Customer';
	}
	$("body").append("<div id='popUpBox'></div>");
	var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New '+captions+' Title:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control categoryName" />';
		formContent += '</p>';
		formContent += '</div>';
	$("#popUpBox").append(formContent);
	$("#fade").fadeIn();
	$("#popUpBox").fadeIn('200').centerThisDiv();
	$(".categoryName").focus();
	$(".categoryName").keyup(function(e){
		var name = $(".categoryName").val();
		if(e.keyCode == 13){
			$(".categoryName").blur();
			if(name != ''){
				$.post(url,{supp_name:name},function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("select.supplierSelector").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
						$("select.supplierSelector option:selected").prop('selected',false);
						$("select.supplierSelector option").last().prop('selected',true);
						$("select.supplierSelector").selectpicker('refresh');
						hidePopUpBox();
					}else if(data['OK'] == 'N'){
						alert('New '+captions+' Could Not be Created!');
					}
				});
			}
		}
	});
};
var getAccountBalance = function(){
	var acc_code = $("select.supplierSelector option:selected").val();
	$.post("db/get-account-balance.php",{supplierAccCode:acc_code},function(data){
		data = $.parseJSON(data);
		$("input.customer-balance").val(data['AMOUNT']);
	});
}
var discount_type_change = function(thiss){
	var this_val = $(thiss).val();
	if($("tr.dynamix").length){
		return false;
	}
	$.get('sale-details.php',{discount:this_val});
};
var recover_balance = function(thiss){
	if($(thiss).is(":checked")){
		balance_recovered = "Y";
	}else{
		balance_recovered = "N";
	}
	$(this).calculateColumnTotals();
}
var sendSmsPopUp = function(){
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel' style='width:280px;'><p class='confirm text-left'>Please Confirm Mobile Number <button id='close-pop' class='pull-right btn btn-xs btn-danger'> <i class='fa fa-times'></i> </button> </p>  <input class='form-control confirmed-mobile pull-left text-center' style='width:150px;margin-left:25px;' value='"+$("input.customer_mobile").val()+"' /> <a class='nodelete btn btn-info'>Send</a></div>");
	centerThisDiv("#popUpDel");
	$("#popUpDel").hide();
	$("#xfade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$("#close-pop").click(function(){
		$("#popUpDel").fadeOut();
		$("#xfade").fadeOut();
	});
	$("#xfade").click(function(){
		$("#popUpDel").fadeOut();
		$("#xfade").fadeOut();
	});
	$(".nodelete").click(function(){
		sendSmsInit();
	});
};
var sendSmsInit = function(){
	$("#popUpDel input[type=text]").remove();
	$("#popUpDel p.confirm").text("Please Wait...");
	var mobile_number = ($("input.confirmed-mobile").length)?$("input.confirmed-mobile").val():"";
	if(mobile_number == ''){
		return;
	}
	var sale_id 	  = $("input.sale_id").val();
	$.post("sale-details.php",{mobile_number:mobile_number,sale_id:sale_id},function(data){
		$("#popUpDel p.confirm").text(data);
		$("#popUpDel").css({"width":"350px"});
		$("#popUpDel").centerThisDiv();
		$("input.confirmed-mobile").val('');
		$("input.confirmed-mobile").hide();
		$(".nodelete").text("OK");
		$(".nodelete").click(function(){
			$("#popUpDel").fadeOut();
			$("#xfade").fadeOut();
		});
	});
};
var calculations_count_check = function(){
	return;
	if($("tbody.calculations_body").find("tr.calculations").length){
		$("tbody.calculations_body").parent().parent().show();
	}else{
		$("tbody.calculations_body").parent().parent().hide();
	}
}
var show_category_list  = function(){
	$("#myCarousel").carousel(0);
}
var show_category_child = function(category_id){
	$(".item-category-list").hide();
	$(".item-category-list[data-cat-id='"+category_id+"']").show();
	$("#myCarousel").carousel(1);
}
