var print_method = '';
var print_opt = '';
$(document).ready(function() {
    use_cartons = $("input.use_cartons").val();
    use_tax = $("input.use_tax").val();
    $(window).keyup(function(e) {
        if (e.keyCode == 27) {
            $(this).clearPanelValues();
            $("div.itemSelector button").focus();
            if ($(".updateMode").length) {
                $(".updateMode").removeClass('updateMode');
            }
        }
    });
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $.fn.barcodeAvailable = function() {
        var $thisElm = $(this);
        var item_id = parseInt($("input[name=itemID]").val()) || 0;
        $(this).on('keyup blur', function(e) {
            $.get('db/compare-barcode.php', { q: $(this).val(), id: item_id }, function(data) {
                if (data > 0) {
                    $("#popUpBox input:submit").prop('disabled', true);
                    $($thisElm).addClass('error');
                } else {
                    $("#popUpBox input:submit").prop('disabled', false);
                    $($thisElm).removeClass('error');
                }
            });
        });
    };
    $.fn.numericOnly = function() {
        $(this).keydown(function(e) {
            if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 ||
                e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 116 ||
                (e.keyCode == 65 && e.ctrlKey === true) ||
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return true;
            } else {
                if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
        });
    };
    $.fn.numericFloatOnly = function() {
        $(this).keydown(function(e) {
            if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 ||
                e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110 ||
                (e.keyCode == 65 && e.ctrlKey === true) ||
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return true;
            } else {
                if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
        });
    };
    $('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
    $('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
    $('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
    $('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
        function() {
            $(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
            $(this).addClass('current'); // Add class "current" to clicked tab
            var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
            $(currentTab).siblings().hide(); // Hide all content divs
            $(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
            return false;
        }
    );
    //Close button:
    $(".close").click(
        function() {
            $(this).parent().fadeTo(400, 0, function() { // Links with the class "close" will close parent
                $(this).slideUp(400);
            });
            return false;
        }
    );
    // Alternating table rows:
    $('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
    // Check all checkboxes when the one in a table head is checked:
    $('.check-all').click(
        function() {
            $(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
        }
    );
    $.fn.sumColumn = function(sumOfFeild, insertToFeild) {
        var sumAll = 0;
        $(sumOfFeild).each(function(index, element) {
            sumAll += parseFloat($(this).text());
        });
        $(insertToFeild).text(sumAll);
    };
    $.fn.sumColumnFloat = function(sumOfFeild, insertToFeild) {
        var sumAll = 0;
        $(sumOfFeild).each(function(index, element) {
            sumAll += parseFloat($(this).text()) || 0;
        });
        sumAll = Math.round(sumAll * 100) / 100;
        $(insertToFeild).text((sumAll).toFixed(2));
    };
    $.fn.multiplyTwoFeilds = function(multiplyToElmVal, writeProductToElm) {
        $(writeProductToElm).val($(this).val() * $(multiplyToElmVal).val());
    };
    $.fn.multiplyTwoFloats = function(multiplyToElmVal, writeProductToElm) {
        $(this).keyup(function(e) {
            var thisVal = parseFloat($(this).val()) || 0;
            var thatVal = parseFloat($(multiplyToElmVal).val()) || 0;
            var productVal = Math.round((thisVal * thatVal) * 100) / 100;
            $(writeProductToElm).val(productVal);
        });
    };
    $.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal, writeProductToElm, writeProductToElmNoTax, taxAmountElm) {
        $(this).keyup(function(e) {
            var taxRate = parseFloat($("input.taxRate").val()) || 0;
            var taxType = ($(".taxType").is(":checked")) ? "I" : "E";
            var thisVal = parseFloat($(this).val()) || 0;
            var thatVal = parseFloat($(multiplyToElmVal).val()) || 0;
            var amount = Math.round((thisVal * thatVal) * 100) / 100;
            if (taxRate > 0) {
                if (taxType == 'I') {
                    taxAmount = amount * (taxRate / 100);
                    amount -= taxAmount;
                } else if (taxType == 'E') {
                    taxAmount = amount * (taxRate / 100);
                }

            } else {
                taxAmount = 0;
            }
            taxAmount = Math.round(taxAmount * 100) / 100;
            amount = Math.round(amount * 100) / 100;
            var finalAmount = Math.round((amount + taxAmount) * 100) / 100;
            $(taxAmountElm).val(taxAmount);
            $(writeProductToElm).val(finalAmount);
            $(writeProductToElmNoTax).val(amount);
        });
    };
    //inventory.php end
    $.fn.setFocusTo = function(Elm) {
        $(this).keydown(function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(Elm).focus();
            }
        });
    };
    $.fn.getItemDetails = function() {
        var item_id = $(".itemSelector option:selected").val();
        if (item_id != '') {
            $.post('db/get-item-details.php', { p_item_id: item_id }, function(data) {
                data = $.parseJSON(data);
                var itemStock  = data['STOCK'];
                var itemPrice  = data['P_PRICE'];
                var qty_carton = data['QTY_CARTON'];
                if (typeof(itemPrice) == 'object') {
                    itemPrice = itemPrice['UNIT_PRICE'];
                }
                if (itemPrice == 0) {
                    itemPrice = '';
                }
                $("input.unitPrice").val(itemPrice);
                $("input.per_carton").val(qty_carton);
                $("input.inStock").attr('thestock', itemStock).val(itemStock);
            });
        }
    };
    $.fn.quickSave = function() {
        var medical_store_mode = $("input.batch_no").length;
        var item_id = $(".itemSelector option:selected").val();
        var item_name = $(".itemSelector option:selected").text();
        if (use_cartons == 'Y') {
            var cartons = parseFloat($("input.cartons").val()) || 0;
            var per_carton = parseFloat($("input.per_carton").val()) || 0;
        }
        var quantity = parseFloat($("input.quantity").val()) || 0;
        var unitPrice = $("input.unitPrice").val();
        var discount = $("input.discount").val();
        var subAmount = $("input.totalAmount").attr('data-sub-amount');
        if (use_tax == 'Y') {
            var taxRate = parseFloat($("input.taxRate").val()) || 0;
            var taxAmount = $("input.taxAmount").val();
        }
        var totalAmount = parseFloat($("input.totalAmount").val()) || 0;
        if (medical_store_mode) {
            var batch_no = ($("input.batch_no").length) ? $("input.batch_no").val() : "";
            var expiry_date = ($("input.expiry_date").length) ? $("input.expiry_date").val() : "";
        }

        $(this).blur();

        if (quantity == 0) {
            return false;
        }
        if (taxRate == '' || taxRate == 0) {
            taxAmount = 0;
        }
        var updateMode = $(".updateMode").length;
        if (item_id != '' && item_name != '' && quantity != '' && unitPrice != '' && subAmount != '' && totalAmount != '') {
            if (updateMode == 0) {
                var the_row = '<tr class="alt-row calculations transactions" data-row-id="0"><td style="text-align:left;" class="itemName" data-item-id="' + item_id + '">' + item_name + '</td>';
                if (use_cartons == 'Y') {
                    the_row += '<td style="text-align:center;" class="cartons">' + cartons + '</td><td style="text-align:center;" class="per_carton">' + per_carton + '</td>';
                }
                the_row += '<td style="text-align:center;" class="quantity">' + quantity + '</td><td style="text-align:center;" class="unitPrice">' + unitPrice + '</td><td  style="text-align:center;" class="discount">' + discount + '</td>';
                if (!medical_store_mode || use_tax == 'Y') {
                    the_row += '<td style="text-align:center;" class="subAmount">' + subAmount + '</td>';
                }
                if (use_tax == 'Y') {
                    the_row += '<td style="text-align:center;" class="taxRate">' + taxRate + '</td><td style="text-align:center;" class="taxAmount">' + taxAmount + '</td>';
                }
                the_row += '<td style="text-align:center;" class="totalAmount" data-sub-amount="' + subAmount + '">' + totalAmount + '</td>';
                if (medical_store_mode) {
                    the_row += '<td style="text-align:center;" class="batch_no">' + batch_no + '</td>';
                    the_row += '<td style="text-align:center;" class="expiry_date">' + expiry_date + '</td>';
                }
                the_row += '<td style="text-align:center;"> - - - </td><td style="text-align:center;"><a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a><a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a></td></tr>';

                $(the_row).insertAfter($(".calculations").last());
            } else if (updateMode == 1) {
                $(".updateMode").find('td.itemName').text(item_name);
                $(".updateMode").find('td.itemName').attr('data-item-id', item_id);
                if (use_cartons == 'Y') {
                    $(".updateMode").find('td.cartons').text(cartons);
                    $(".updateMode").find('td.per_carton').text(per_carton);
                }
                $(".updateMode").find('td.quantity').text(quantity);
                $(".updateMode").find('td.unitPrice').text(unitPrice);
                $(".updateMode").find('td.discount').text(discount);
                $(".updateMode").find('td.subAmount').text(subAmount);
                $(".updateMode").find('td.taxRate').text(taxRate);
                $(".updateMode").find('td.taxAmount').text(taxAmount);
                $(".updateMode").find('td.totalAmount').text(totalAmount);
                $(".updateMode").find('td.totalAmount').attr('data-sub-amount', subAmount);
                $(".updateMode").find('td.batch_no').text(batch_no);
                $(".updateMode").find('td.expiry_date').text(expiry_date);
                $(".updateMode").removeClass('updateMode');
            }
            $(this).clearPanelValues();
            $(this).calculateColumnTotals();
            $("select.itemSelector").parent().find(".dropdown-toggle").focus();
        } else {
            displayMessage('Values Missing!');
        }
    };
    $.fn.calculateColumnTotals = function() {
        var discount_type = $("input.discount_type:checked").val();
        $(this).sumColumnFloat("td.cartons", "td.carton_total");
        $(this).sumColumnFloat("td.quantity", "td.qtyTotal");
        $(this).sumColumnFloat("td.unitPrice", "td.price_total");
        $(this).sumColumnFloat("td.taxAmount", "td.amountTax");
        $(this).sumColumnFloat("td.subAmount", "td.amountSub");
        $(this).sumColumnFloat("td.totalAmount", "td.amountTotal");
        var amountTotal = parseFloat($("td.amountTotal").text()) || 0;

        var tax_amount_total = parseFloat($("td.amountTax").text()) || 0;
        $(".whole_tax").val(tax_amount_total);

        var disk = 0;
        var rate = 0;
        var qty = 0;
        var sub = 0;
        if (discount_type == 'R') {
            $("td.discount").each(function() {
                disk += parseFloat($(this).text()) || 0;
            });
        } else {
            var dit = 0;
            $("tr.transactions").each(function() {
                qty = parseFloat($(this).find("td.quantity").text()) || 0;
                rate = parseFloat($(this).find("td.unitPrice").text()) || 0;
                dit = parseFloat($(this).find("td.discount").text()) || 0;

                disk += parseFloat(((qty * rate * dit) / 100)) || 0;
            });
        }

        var discount_amount = parseFloat($("input.whole_discount").val()) || 0;
        //if(discount_type == 'P'){
        //	discount_amount = (amountTotal*discount_amount) / 100;
        //}
        //$("input.bildiscountamount").val(discount_amount);
        //amountTotal -= discount_amount;

        $("input.total_discount").val((disk).toFixed(2));
        var income_tax = parseFloat($("input.income_tax_percent").val()) || 0;
        income_tax = (amountTotal * income_tax) / 100;
        $("input.income_tax").val((Math.round(income_tax)).toFixed(2));

        var charges = parseFloat($("input.inv_charges").val()) || 0;
        amountTotal += charges;
        amountTotal += income_tax;
        $("input.grand_total").val((Math.round(amountTotal)).toFixed(2));
    };
    $.fn.calculateRowTotal = function() {
        var taxRate = $("input.taxRate").val();
        var taxType = ($(".taxType").is(":checked")) ? "I" : "E";
        var thisVal = parseFloat($("input.quantity").val()) || 0;
        var thatVal = parseFloat($("input.unitPrice").val()) || 0;
        var amount = Math.round((thisVal * thatVal) * 100) / 100;
        var discountAvail = parseFloat($("input.discount").val()) || 0;
        var discount_type = $("input.discount_type:checked").val();
        var discountPerCentage = 0;
        amount = Math.round(amount * 100) / 100;
        if (discount_type == 'R') {
            discountPerCentage = discountAvail;
        } else if (discount_type == 'P') {
            discountAvail = Math.round(discountAvail * 100) / 100;
            discountPerCentage = amount * (discountAvail / 100);
            discountPerCentage = Math.round(discountPerCentage * 100) / 100;
        }
        amount -= discountPerCentage;
        if (taxRate > 0) {
            if (taxType == 'I') {
                taxAmount = amount * (taxRate / 100);
            } else if (taxType == 'E') {
                taxAmount = amount * (taxRate / 100);
            }
        } else {
            taxAmount = 0;
        }
        taxAmount = Math.round(taxAmount * 100) / 100;

        amount = Math.round(amount * 100) / 100;
        var finalAmount = Math.round((amount + taxAmount) * 100) / 100;
        finalAmount = Math.round(finalAmount * 100) / 100;
        if (taxType == 'I') {
            amount -= taxAmount;
            finalAmount -= taxAmount;
        }
        $("input.subAmount").val(Math.round(amount));
        $("input.totalAmount").attr('data-sub-amount', Math.round(amount));

        $("input.taxAmount").val(Math.round(taxAmount));
        $("input.totalAmount").val(Math.round(finalAmount));
    };
    $.fn.updateStockInHand = function() {
        var stockOfBill = 0;
        $("td.quantity").each(function(index, element) {
            stockOfBill += parseFloat($(this).text()) || 0;
        });
        stockOfBill += parseFloat($("input.quantity").val()) || 0;
    };
    $.fn.clearPanelValues = function() {
        $(".itemSelector option").prop('selected', false);
        $(".itemSelector").find("option").first().prop('selected', true);
        $(".itemSelector").selectpicker('refresh');
        $("input.cartons").val('');
        $("input.per_carton").val('');
        $("input.quantity").val('');
        $("input.unitPrice").val('');
        $("input.discount").val('');
        $("input.taxRate").val('');
        $("input.subAmount").val('');
        $("input.taxAmount").val('');
        $("input.totalAmount").val('');
        $("input.batch_no").val('');
        $("input.expiry_date").val('');
        $("input.inStock").val('').attr('thestock', '');
    };
});

var use_cartons = '';
var use_tax = '';
// PreLoad FUNCTION //
//////////////////////
var letMeGo = function(thisElm) {
    $(thisElm).fadeOut(300, function() {
        $(this).html('');
    });
}
var editThisRow = function(thisElm) {
    var thisRow = $(thisElm).parent('td').parent('tr');
    $(".updateMode").removeClass('updateMode');
    thisRow.addClass('updateMode');

    var item_name = thisRow.find('td.itemName').text();
    var item_id = parseInt(thisRow.find('td.itemName').attr('data-item-id')) || 0;
    var cartons = parseFloat(thisRow.find('td.cartons').text()) || 0;
    var per_carton = parseFloat(thisRow.find('td.per_carton').text()) || 0;
    var quantity = parseFloat(thisRow.find('td.quantity').text()) || 0;
    var unitPrice = parseFloat(thisRow.find('td.unitPrice').text()) || 0;
    var discount = parseFloat(thisRow.find('td.discount').text()) || 0;
    var subAmount = parseFloat(thisRow.find('td.totalAmount').attr('data-sub-amount')) || 0;
    var taxRate = parseFloat(thisRow.find('td.taxRate').text()) || 0;
    var taxAmount = parseFloat(thisRow.find('td.taxAmount').text()) || 0;
    var totalAmount = parseFloat(thisRow.find('td.totalAmount').text()) || 0;
    var batch_no = thisRow.find('td.batch_no').text();
    var expiry_date = thisRow.find('td.expiry_date').text();

    $(".itemSelector option[value='" + item_id + "']").prop('selected', true);
    $(".itemSelector").selectpicker('refresh');
    $("input.cartons").val(cartons);
    $("input.per_carton").val(per_carton);
    $("input.quantity").val(quantity);
    $("input.unitPrice").val(unitPrice);
    $("input.discount").val(discount);
    $("input.subAmount").val(subAmount);
    $("input.taxRate").val(taxRate);
    $("input.taxAmount").val(taxAmount);
    $("input.totalAmount").val(totalAmount);
    $("input.totalAmount").attr('data-sub-amount', subAmount);
    $("input.batch_no").val(batch_no);
    $("input.expiry_date").val(expiry_date);
    $("input.inStock").val('').attr('thestock', '');
    $("div.itemSelector button").focus();
};
var stockOs = function() {
    var thisQty = parseFloat($("input.quantity").val()) || 0;
    var inStock = parseFloat($("input.inStock").attr('thestock')) || 0;

    var thisBillQuantity = 0;

    var item_id = parseFloat($("select.itemSelector option:selected").val()) || 0;

    $("td.quantity").each(function(index, element) {
        if (item_id == parseFloat($(this).prev("td.itemName").attr('data-item-id')) || 0) {
            thisBillQuantity += parseFloat($(this).text()) || 0;
        }
    });
    if ($("tr.updateMode").length) {
        thisBillQuantity -= parseFloat($("tr.updateMode td.quantity").text()) || 0;
    }
    var NewStock = thisQty + inStock;
    NewStock += thisBillQuantity;

    $("input.inStock").val(NewStock);
};

var savePurchase = function() {
    var purchase_id       = $(".purchase_id").val();
    var purchaseDate      = $(".datepicker").val();
    var billNum           = $("input[name='billNum']").val();
    var vendorBillNum     = $("input[name='vendorBillNum']").val();
    var po_number         = $("input[name='po_number']").val();
    var gp_num            = $("input[name='gpNum']").val();
    var batch_no          = ($("input[name='batch_no']").length == 1) ? $("input[name='batch_no']").val() : "";
    var inv_notes         = $("textarea.inv_notes").val();
    var supplierCode      = $("select.supplierSelector option:selected").val();
    var supplier_name     = $("input[name='supplier_name']").val();
    var discount          = $("input.bildiscountamount").val();
    var total_discount    = $("input.total_discount").val();
    var charges           = $("input.inv_charges").val();
    var disc_type         = $("input.discount_type:checked").val();
    var income_tax        = parseFloat($("input.income_tax_percent").val()) || 0;
    var unregistered_tax  = $("input.unregistered_tax:checked").length > 0 ? "Y" : "N";
    var subject           = $("input[name=subject]").val();
    var over_discount     = 0;
    if (supplierCode == '') {
        displayMessage('Account Not Selected!');
        return false;
    }
    if($("tr.transactions").length == 0){
        displayMessage('No Transaction Found!');
        return false;
    }
    $(".savePurchase").hide();
    $("#xfade").show();
    var jSonString = {};
    var deleted    = {};
    $("input.delete_rows").each(function(i, e) {
        deleted[i] = $(this).val();
    });
    deleted = JSON.stringify(deleted);
    var medical_store_mode = $("input.batch_no").length;
    $("tr.transactions").each(function(index, element) {
        var rowId = $(this).attr('data-row-id');
        jSonString[index] = {};
        jSonString[index].row_id = rowId;
        jSonString[index].item_id = parseInt($(this).find('td.itemName').attr('data-item-id')) || 0;
        jSonString[index].quantity = parseFloat($(this).find('td.quantity').text()) || 0;
        jSonString[index].cartons = parseFloat($(this).find('td.cartons').text()) || 0;;
        jSonString[index].per_carton = parseFloat($(this).find('td.per_carton').text()) || 0;
        jSonString[index].unitPrice = parseFloat($(this).find('td.unitPrice').text()) || 0;
        jSonString[index].discount = parseFloat($(this).find('td.discount').text()) || 0;
        jSonString[index].subAmount = parseFloat($(this).find('td.totalAmount').attr('data-sub-amount')) || 0;
        jSonString[index].taxRate = parseFloat($(this).find('td.taxRate').text()) || 0;
        jSonString[index].taxAmount = parseFloat($(this).find('td.taxAmount').text()) || 0;
        jSonString[index].totalAmount = parseFloat($(this).find('td.totalAmount').text()) || 0;
        if (medical_store_mode) {
            jSonString[index].batch_no = $(this).find('td.batch_no').text();
            jSonString[index].expiry_date = $(this).find('td.expiry_date').text();
        }
    });
    jSonString = JSON.stringify(jSonString);

    //console.log(jSonString);
    //return;

    $.post("db/savePurchase.php",{
        purchase_id: purchase_id,
        purchaseDate: purchaseDate,
        billNum: billNum,
        vendorBillNum: vendorBillNum,
        po_number: po_number,
        gp_num: gp_num,
        batch_no: batch_no,
        supplierCode: supplierCode,
        supplier_name: supplier_name,
        subject: subject,
        inv_notes: inv_notes,
        discount: discount,
        total_discount: total_discount,
        charges: charges,
        disc_type: disc_type,
        income_tax: income_tax,
        unregistered_tax: unregistered_tax,
        deleted_rows: deleted,
        jSonString: jSonString
    }, function(data) {
        data = $.parseJSON(data);
        if (data['ID'] > 0) {
            var msg_type = '';
            if(purchase_id == 0){
                msg_type = "&saved";
            }else{
                msg_type = "&updated";
            }
            if(print_method == 'Y'){
                print_opt = "&print"
            }
            window.location.href='inventory-details.php?id=' + data['ID'] + msg_type + print_opt;
        }else{
            $("#xfade").hide();
            displayMessage(data['MSG']);
        }
    });
};
var delete_detail_row = function(rowChildElement) {
    var row_id = $(rowChildElement).parent().parent().attr("data-row-id");
    var clickedDel = $(rowChildElement);

    if ($(rowChildElement).parent().parent().hasClass('updateMode')) {
        displayMessage('Row Is In Edit Mode!');
        return false;
    }

    $("#fade").hide();
    $("#popUpDel").remove();
    $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
    $("#popUpDel").hide();
    $("#popUpDel").centerThisDiv();
    $("#fade").fadeIn('slow');
    $("#popUpDel").fadeIn();
    $(".dodelete").click(function() {
        $("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
        $("#popUpDel").children(".dodelete").hide();
        $("#popUpDel").children(".nodelete").text("Close");
        $("div.underTheTable").append('<input class="delete_rows" type="hidden" value="' + row_id + '" />');
        clickedDel.parent().parent().remove();
    });
    $(".nodelete").click(function() {
        $("#fade").fadeOut();
        $("#popUpDel").fadeOut();
    });
    $(".close_popup").click(function() {
        $("#popUpDel").slideUp();
        $("#fade").fadeOut('fast');
    });
};
var makeItCash = function(checker) {
    var what = $(checker).is(":checked");
    if (what) {
        $("select.supplierSelector option[value^='040101']").prop('disabled', true).prop('selected', false);
        $("select.supplierSelector option[value^='010101']").prop('disabled', false).prop('selected', false);
        $("select.supplierSelector").selectpicker('refresh');
        $("input[name='supplier_name']").show().focus();
    } else {
        $("select.supplierSelector option[value^='040101']").prop('disabled', false).prop('selected', false);
        $("select.supplierSelector option[value^='010101']").prop('disabled', true).prop('selected', false);
        $("select.supplierSelector").selectpicker('refresh');
        $("input[name='supplier_name']").val('').hide();
    }
};
var hidePopUpBox = function() {
    $("#popUpBox").css({ 'transform': 'scaleY(0.0)' });
    $("#xfade").fadeOut();
    $("#popUpBox").remove();
};
var add_supplier = function() {
    $("body").append("<div id='popUpBox'></div>");
    var formContent = '';
    formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
    formContent += '<div id="form">';
    formContent += '<p>New Supplier Title:</p>';
    formContent += '<p class="textBoxGo">';
    formContent += '<input type="text" class="form-control categoryName" />';
    formContent += '</p>';
    formContent += '</div>';
    $("#popUpBox").append(formContent);
    $("#xfade").fadeIn();
    $("#popUpBox").fadeIn('200').centerThisDiv();
    $(".categoryName").focus();
    var disabled = '';
    disabled = ($(".trasactionType").is(":checked")) ? "disabled" : "";
    $(".categoryName").keyup(function(e) {
        var name = $(".categoryName").val();
        if (e.keyCode == 13) {
            $(".categoryName").blur();
            if (name != '') {
                $.post('supplier-details.php', { supp_name: name }, function(data) {
                    data = $.parseJSON(data);
                    if (data['OK'] == 'Y') {
                        $("select.supplierSelector").append('<option ' + disabled + ' data-subtext="' + data['ACC_CODE'] + '" value="' + data['ACC_CODE'] + '">' + data['ACC_TITLE'] + '</option>');
                        if (!$(".trasactionType").is(":checked")) {
                            $("select.supplierSelector option:selected").prop('selected', false);
                            $("select.supplierSelector option").last().prop('selected', true);
                        }
                        $("select.supplierSelector").selectpicker('refresh');
                        hidePopUpBox();
                    } else if (data['OK'] == 'N') {
                        displayMessage('New Supplier Could Not be Created!');
                    }
                });
            }
        }
    });
};
var add_new_item_submit = function() {
    var formData = $("#popUpBox form").serializeObject();
    $.post('item-details.php', formData, function(data) {
        hidePopUpBox();
        $(".reload_item").click();
    });
};
var add_new_item = function() {
    $("body").append('<div id="popUpBox"><button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button></div>');
    $("#popUpBox").append('<form action="" method="post"></form>');
    $.get('item-details.php', {}, function(html) {
        var formContent = $(html).find(".popup_content_nostyle").html();
        formContent += $(html).find(".form-submiters").html();
        $("#popUpBox form").append(formContent);
        $("#popUpBox form").append('<input name="addItem" type="hidden" />');
        $("#popUpBox .popuporemov").remove();
        $("#popUpBox .selectpicker").remove();
        $("#popUpBox input:submit").addClass("prop_item_submit");
        $("#popUpBox .prop_item_submit").prop('type', 'button');
        $("#popUpBox .prop_item_submit").click(function() {
            add_new_item_submit();
        });
        $("#popUpBox select").selectpicker();
        $("#popUpBox input.item-barcode").barcodeAvailable();
        $("#xfade").fadeIn();
        $("#popUpBox").fadeIn('200').centerThisDiv();
    });
};
var discount_type_change = function(thiss) {
    var this_val = $(thiss).val();
    if ($("tr.transactions").length) {
        return false;
    }
    $.get('sale-details.php', { discount: this_val });
};
var get_history = function(){
  var item_id = parseInt($("select.itemSelector option:selected").val())||0;
  var code    = '';
  if(!$("input.trasactionType").is(":checked")){
    code    = $("select.supplierSelector option:selected").val();
  }
  if(item_id==0){
    return;
  }
  $("div.itemSelector").popover('destroy');
  var elm = '';
  $.post("?",{get_history:item_id,account_code:code},function(data){
    data = $.parseJSON(data);
    if(data!=null){
      $.each(data,function(i,v){
        if(i>0){
          elm += '<span class="divider"></span>';
        }
        elm += '<p style="font-size: 12px ;"> <b>Date</b> '+v.PURCHASE_DATE+' <b>Cost</b> '+v.UNIT_PRICE+'</p>';
      });
    }else{
      elm += '<span class="divider">No Results.</span>';
    }
    setTimeout(function(){
      $("div.itemSelector").popover({html:true,content:elm});
      $("div.itemSelector").popover('show');
    },300);
  });
};
