$(document).ready(function(){
	var total = 0;
	var discount_type = $(".discount_type").val();
	$.fn.serializeObject = function(){
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};

	$("td.price_td").each(function(){
		total += parseFloat($(this).find('input').val())||0;
	});
	$(".total_amount").val((total).toFixed(2));
	$(".barcode_input").keydown(function(e){
		//e.preventDefault;
		//return false;
	});
	$("table.barcodes_table").on('click','.pointer',function(){
		var item_id = $(this).parent().parent().find('select option:selected').val();
		$("select option[value='"+item_id+"']").parent().change();
		$(this).parent().parent().remove();
		transcationCounter();
	});
	$("table.barcodes_table").on('change','td.item_td select',function(){
		var $tdbox  = $(this).parent('td');
		var items   = {};

		$("table.barcodes_table .item_td").each(function(i,e){
			if($(this).find("select option:selected").val() == '') return;
			items[$(this).find("select option:selected").val()] = $(this).find("select option:selected").val();
		});

		$.each(items,function(index,item_id){
			item_id = parseInt(item_id)||0;
			if(item_id == 0) return;
			$.post('db/get-item-details.php',{get_ps_price:item_id},function(data){
				data = $.parseJSON(data);
				data['STOCK'] = parseFloat(data['STOCK'])||0;
				$(".barcodes_table select").each(function(i,e){
					if($(this).find("option:selected").val() == item_id){
						if($(this).parent().parent().hasClass("dynamic")){
							return;
						}
						data['STOCK']++;

						$(this).parent().parent().find("td.stock_td").find('input').val(data['STOCK']);
						if(data['PURCHASE_PRICE'] != null){
							$(this).parent().parent().find("td.purchase_td").find('input').val(data['PURCHASE_PRICE']);
						}
						if(data['SALE_PRICE'] != null){
							$(this).parent().parent().find("td.sprice_td").find('input').val(data['SALE_PRICE']);
						}
					}
				});
			});
		});
	});
	$("table.barcodes_table").on('keyup','.price_td .form-control',function(e){
		if(e.keyCode == 13){
			$(this).parent().next().children("input").focus();
		}
	});
	$("table.barcodes_table").on('keyup blur change',".purchase_td input,.discount_td input,.tax_td input",function(){
		$("input[readonly]").prop("tabindex",'-1');
		var total = 0;
		var discount_total = 0;
		var tax_total = 0;
		var total_purchase_price = 0;
		var price_td     = 0;
		var purchase_td  = 0;
		var discount_td  = 0;
		var tax_td       = 0;
		var this_discount= 0;
		var taxAmount    = 0;
		$("table.barcodes_table tr").each(function(){
			purchase_td = parseFloat($(this).find('td.purchase_td input').val())||0;
			discount_td = parseFloat($(this).find('td.discount_td input').val())||0;
			tax_td      = parseFloat($(this).find('td.tax_td input').val())||0;

			total_purchase_price += purchase_td;
			price_td        	  = purchase_td;

			if(discount_td > 0){
				this_discount   = purchase_td*(discount_td/100);
				discount_total += this_discount;
				price_td        = purchase_td - this_discount;
			}
			if(tax_td > 0){
				taxAmount    = price_td * (tax_td/100);
				tax_total   += taxAmount;
			}

			var pr_td   = parseFloat(price_td+taxAmount)||0;
			total   += parseFloat(price_td)||0;
			$(this).find('td.price_td input').val((pr_td).toFixed(2));
		});
		$(".total_purchase_price").val((total_purchase_price).toFixed(2));
		$(".total_discount").val((discount_total).toFixed(2));
		$(".total_tax").val((tax_total).toFixed(2));
		$(".total_amount").val((total+tax_total).toFixed(2));
	});
	$(".barcode_input").keyup(function(e){
		//quickScan
		if(e.keyCode == 13){
			$(".barcode_input").blur();
			$exception = $("input.css-checkbox:checked").length == $("input.css-checkbox").length;
			var $thisInputBarcode = $(this);
			var the_code 		  = $thisInputBarcode.val();
			if(the_code == ''){
				return false;
			}else{
				$thisInputBarcode.val('');
			}
			if($("td[data-code='"+the_code+"']").length){
				$("td[data-code='"+the_code+"']").animate({"background-color":"rgba(255,0,0,0.4)"},400,function(){
					$("td[data-code='"+the_code+"']").animate({"background-color":"rgba(255,255,255,1.0)"});
				});
				$(".barcode_input").focus();
				return false;
			}
			var row = new_row_item_object(the_code);
			var get_def = false;
			if($("table.barcodes_table tbody tr").length){
				var item_def     = $("table.barcodes_table tbody tr").last().find("td.item_td select option:selected").val();
				var colour_def 	 = $("table.barcodes_table tbody tr").last().find("td.colour_td input").val();
				var purchase_def = $("table.barcodes_table tbody tr").last().find("td.purchase_td input").val();
				if(show_discount=='Y'){
					var discount_def = $("table.barcodes_table tbody tr").last().find("td.discount_td input").val();
				}
				if(show_tax=='Y'){
					var tax_def      = $("table.barcodes_table tbody tr").last().find("td.tax_td input").val();
				}
				var price_def    = $("table.barcodes_table tbody tr").last().find("td.price_td input").val();
				var sale_def     = $("table.barcodes_table tbody tr").last().find("td.sprice_td input").val();
				get_def      = true;
			}

			$("table.barcodes_table tbody").append(row).find('tr').last().find('td.item_td select').removeClass("item_source").addClass("itemSelect").selectpicker();
			if(get_def){
				$("table.barcodes_table tbody tr").last().find("td.item_td select").selectpicker('val',item_def);
				$("table.barcodes_table tbody tr").last().find("td.colour_td input").val(colour_def);
				$("table.barcodes_table tbody tr").last().find("td.purchase_td input").val(purchase_def);
				if(show_discount=='Y'){
				$("table.barcodes_table tbody tr").last().find("td.discount_td input").val(discount_def);
				}
				if(show_tax=='Y'){
				$("table.barcodes_table tbody tr").last().find("td.tax_td input").val(tax_def);
				}
				$("table.barcodes_table tbody tr").last().find("td.price_td input").val(price_def);
				$("table.barcodes_table tbody tr").last().find("td.sprice_td input").val(sale_def);
			}
			$(".barcode_input").focus();
			transcationCounter();
			$("table.barcodes_table input").change();
		}
	});
	$(".generate_rows").click(function(){
		$(".generate_rows").blur();

		var next_code,last_code,gen_qty,item_def,colour_def,purchase_def,discount_def,tax_def,price_def,sale_def,row;

		last_code    = parseInt($("input.last_code").val())||0;
		next_code    = last_code;
		gen_qty 	 = parseFloat($("tr.barcode_gen").find("td.qty_td input").val())||0;

		item_def     = $("tr.barcode_gen").find("td.item_td select option:selected").val();
		colour_def 	 = $("tr.barcode_gen").find("td.colour_td input").val();
		purchase_def = $("tr.barcode_gen").find("td.purchase_td input").val();
		if(show_discount=='Y'){
		discount_def = $("tr.barcode_gen").find("td.discount_td input").val();
		}
		if(show_tax=='Y'){
		tax_def      = $("tr.barcode_gen").find("td.tax_td input").val();
		}
		price_def    = $("tr.barcode_gen").find("td.price_td input").val();
		sale_def     = $("tr.barcode_gen").find("td.sprice_td input").val();

		for(i=1;i<=gen_qty;i++){
			next_code    = barcode_autoincrement(next_code);
			row = new_row_item_object(next_code);
			$("table.barcodes_table tbody").append(row).find('tr').last().find('td.item_td select').removeClass("item_source").addClass("itemSelect").selectpicker();

			$("table.barcodes_table tbody tr").last().find("td.colour_td input").val(colour_def);
			$("table.barcodes_table tbody tr").last().find("td.item_td select").selectpicker('val',item_def);
			$("table.barcodes_table tbody tr").last().find("td.colour_td input").val(colour_def);
			$("table.barcodes_table tbody tr").last().find("td.purchase_td input").val(purchase_def);
			if(show_discount=='Y'){
			$("table.barcodes_table tbody tr").last().find("td.discount_td input").val(discount_def);
			}
			if(show_tax=='Y'){
			$("table.barcodes_table tbody tr").last().find("td.tax_td input").val(tax_def);
			}
			$("table.barcodes_table tbody tr").last().find("td.price_td input").val(price_def);
			$("table.barcodes_table tbody tr").last().find("td.sprice_td input").val(sale_def);
		}
		$("td.start_code input,input.last_code").val(next_code);

		$("tr.barcode_gen select.itemSelect option").prop('selected',false);
		$("tr.barcode_gen select.itemSelect").selectpicker('refresh');
		$("tr.barcode_gen td.qty_td input").val('');
		$("tr.barcode_gen td.stock_td input").val('');
		$("tr.barcode_gen td.colour_td input").val('');
		$("tr.barcode_gen td.purchase_td input").val('');
		$("tr.barcode_gen td.discount_td input").val('');
		$("tr.barcode_gen td.tax_td input").val('');
		$("tr.barcode_gen td.price_td input").val('');
		$("tr.barcode_gen td.sprice_td input").val('');

		$("tr.barcode_gen div.itemSelect button").focus();
		transcationCounter();
		$("table.barcodes_table input").change();
	});
	$("table.barcodes_table tbody").on('change blur',"div.itemSelect button",function(e){
		var item_id = parseInt($(this).parent().parent().find("select.itemSelect option:selected").val())||0;
		if(item_id > 0){
			$.get('db/get-item-details.php');
		}
	});
	$("table.barcodes_table input").change();
	$(".save_scan").click(function(){
		save_purchase_barcodes();
	});
});
/* Calls */
var makeItCash = function(checker){
	var current_cash = $(".bills-cash-code").val();
	var already      = $("select.supplierSelector option:checked").val();
	var what = $(checker).is(":checked");
	if(what){
		$("select.supplierSelector option[value^='040101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value='"+already+"']").prop('selected',true);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").show().focus();
	}else{
		$("select.supplierSelector option[value^='040101']").prop('disabled',false).prop('selected',false);
		$("select.supplierSelector option[value^='010101']").prop('disabled',true).prop('selected',false);
		$("select.supplierSelector option[value='"+already+"']").prop('selected',true);
		$("select.supplierSelector").selectpicker('refresh');
		$("input[name='supplier_name']").val('').hide();
	}
};
var new_row_item_object = function(barcode){
	var item_selector = $(".item_source_div").html();
	var row = '<tr class="barcode_me" row-id="0">'
					+'<td class="sr_td"></td>'
					+'<td class="barcode_td text-center" data-code="'+barcode+'"><input tabindex="-1"  readonly="readonly" class="form-control text-center" value="'+barcode+'" /></td>'
					+'<td class="item_td">'+item_selector+'</td>'
					+'<td class="stock_td"><input  type="text" readonly value="" class="form-control text-center" /></td>'
					+'<td class="colour_td"><input  type="text"  value="" class="form-control text-center" /></td>'
					+'<td class="purchase_td"><input  type="text"  value="" class="form-control text-center" /></td>';
		if(show_discount=='Y'){
		row += '<td class="discount_td"><input  type="text"  value="" class="form-control text-center" /></td>';
		}
		if(show_tax=='Y'){
		row += '<td class="tax_td"><input  type="text"  value="" class="form-control text-center" /></td>';
		}
		row +=       '<td class="price_td"><input readonly type="text" class="form-control text-center" /></td>'
					+'<td class="sprice_td"><input type="text" class="form-control text-center" /></td>'
					+'<td><a class="pointer"><i class="fa fa-times"></i></a></td>'
				+'</tr>';
	return row;
};
var save_purchase_barcodes = function(){
	$(".barcode_me").last().find(".purchase_td input").blur();
	var scan_purchase_id   = parseInt($(".scan_purchase_id").val())||0;
	var purchase_date 	   = $("input[name=rDate]").val();
	var bill_no 		   = $("input[name=billNum]").val();
	var supplier_name      = $("input[name=supplier_name]").val();
	var inv_notes      	   = $("textarea.inv_notes").val();
	var supplier_acc_code  = $("select.supplierSelector option:selected").val();
	var bill_discount      = parseFloat($("input.total_discount").val())||0;
	var bill_tax      	   = parseFloat($("input.total_tax").val())||0;

	var discount_type = $(".discount_type").val();

	var data_obj = {},data_obj_str = '';
	var validated = true;

	if(supplier_acc_code == ''){
		displayMessage('Account Not Selected!');
		return false;
	}
	if($("tr.barcode_me").length == 0){
		displayMessage('Please, make at least one entry to continue!');
		return false;
	}
	$("tr.barcode_me").each(function(index,element){
		if($(this).find(".barcode_td input").val() == ''){
			var xv_bc = $(this).find(".barcode_td").css('background-color');
			$(this).find(".barcode_td").animate({'background-color':'rgba(255,0,0,0.4)'}).delay(1000).animate({'background-color':xv_bc});
			validated = false;
		}
		var item_id = parseInt($(this).find("select.itemSelect option:selected").val())||0;
		if(item_id == 0){
			validated = false;
		}
		if((parseFloat($(this).find(".price_td input").val())||0) <= 0){
			validated = false;
		}
	});
	if(validated == false){
		displayMessage('Please Completely Fill The Form!');
		return false;
	}
	var breakit 	   = false;
	var sub_total 	   = 0;
	var discount_total = 0;
	var total_bill 	   = 0;
	var total_sub      = 0;
	$("tr.barcode_me").each(function(index,element){
		data_obj[index] = {};
		data_obj[index].row_id	    = parseInt($(this).attr("row-id"))||0;
		data_obj[index].barcode	    = $(this).find(".barcode_td").attr('data-code');
		data_obj[index].item_id		= parseInt($(this).find("select.itemSelect option:selected").val())||0;
		data_obj[index].purchase 	= $(this).find(".purchase_td input").val();
		data_obj[index].colour 		= $(this).find(".colour_td input").val();

		data_obj[index].discount 	= $(this).find(".discount_td input").val()||0;
		discount_total = data_obj[index].purchase*(data_obj[index].discount/100);
		sub_total      = data_obj[index].purchase - discount_total;

		data_obj[index].sub_total   = sub_total;
		data_obj[index].tax    	    = $(this).find(".tax_td input").val()||0;
		data_obj[index].price 		= $(this).find(".price_td input").val();
		data_obj[index].sale_price  = $(this).find(".sprice_td input").val();

		total_sub  += parseFloat(sub_total)||0;
		total_bill += parseFloat(data_obj[index].price)||0;
	});
	$(".save_scan").hide();
	$(".pointer").hide();
	$("#xfade").fadeIn();
	data_obj_str = JSON.stringify(data_obj);
	$.post('db/save_scan_purchase.php',
		{sp_id:scan_purchase_id,
		 purchase_date:purchase_date,
		 bill_no:bill_no,
		 supplier_name:supplier_name,
		 supplier_acc_code:supplier_acc_code,
		 inv_notes:inv_notes,
		 bill_discount:bill_discount,
		 bill_tax:bill_tax,
		 total_bill:total_bill,
		 total_sub:total_sub,
		 jSonString:data_obj_str},
		function(data){
			$("#xfade").fadeOut();
			if(data != ''){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					window.location.href = 'mobile-purchase-details.php?id='+data['ID'];
				}else{
					displayMessage(data['MSG']);
					$(".pointer").show();
					$(".save_scan").show();
				}
			}
	});
};
$(function(){
	$(window).keyup(function(e){
		if(e.keyCode==27){
			hidePopUpBox();
		}
	});
});
var hidePopUpBox = function(){
	$("#popUpBox").css({'left':'-=500px'});
	$("#popUpBox").css({'transform':'scaleY(0.0)'});
	$("#xfade").fadeOut();
	$("#popUpBox").remove();
};
var add_supplier = function(){
	$("body").append("<div id='popUpBox'></div>");
	var formContent  = '';
		formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
		formContent += '<div id="form">';
		formContent += '<p>New Supplier Title:</p>';
		formContent += '<p class="textBoxGo">';
		formContent += '<input type="text" class="form-control categoryName" />';
		formContent += '</p>';
		formContent += '</div>';
	$("#popUpBox").append(formContent);
	$("#xfade").fadeIn();
	$("#popUpBox").fadeIn('200').centerThisDiv();
	$(".categoryName").focus();
	var disabled = '';
	disabled = ($(".trasactionType").is(":checked"))?"disabled":"";
	$(".categoryName").keyup(function(e){
		var name = $(".categoryName").val();
		if(e.keyCode == 13){
			$(".categoryName").blur();
			if(name != ''){
				$.post('supplier-details.php',{supp_name:name},function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("select.supplierSelector").append('<option '+disabled+' data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
						if(!$(".trasactionType").is(":checked")){
							$("select.supplierSelector option:selected").prop('selected',false);
							$("select.supplierSelector option").last().prop('selected',true);
						}
						$("select.supplierSelector").selectpicker('refresh');
						hidePopUpBox();
					}else if(data['OK'] == 'N'){
						alert('New Supplier Could Not be Created!');
					}
				});
			}
		}
	});
};
var transcationCounter = function(){
	$("tr.barcode_me").each(function(i,e){
		var sr = i + 1;
		$(this).find('td.sr_td').text(sr);
	});
}
var add_new_item = function(){
	$("body").append('<div id="popUpBox" class="popUpBox_position"><button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button></div>');
	$("#popUpBox").append('<form action="" method="post"></form>');
	$.get('item-details.php',{},function(html){
		var formContent = $(html).find(".popup_content_nostyle").html();
			formContent+= $(html).find(".form-submiters").html();
		$("#popUpBox form").append('<input name="inv_type" value="B" type="hidden" />');
		$("#popUpBox form").append('<input name="addItem" type="hidden" />');
		$("#popUpBox form").append(formContent);
		$("#popUpBox .popuporemov").remove();
		$("#popUpBox .selectpicker").remove();
		$("#popUpBox .barcode_code_div").hide();
		$("#popUpBox input:submit").addClass("prop_item_submit");
		$("#popUpBox .prop_item_submit").prop('type','button');
		$("#popUpBox .prop_item_submit").click(function(){
			add_new_item_submit();
		});
		$("#popUpBox select").selectpicker();
		$("#xfade").fadeIn();
		$("#popUpBox").fadeIn('200').centerThisDiv();
	});
};
var add_new_item_submit = function(){
	var formData = $("#popUpBox form").serializeObject();
	$.post('item-details.php',formData,function(data){
		hidePopUpBox();
		$(".reload_item").click();
	});
};
var barcode_autoincrement = function(code){
	code = ++code;
	return code;
}
