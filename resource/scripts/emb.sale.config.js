/* global displayToastMessage */
/* global displayMessage */
/* global $ */
var balance_recovered = "";
var stock_check       = 'Y';
var print_method      = '';
var redirect_method   = 'U';
var use_cartons       = '';
$(document).ready(function(){
	$("input.total_discount").prop("readonly",true);
	print_method   = $("input.print_method").val();
	use_cartons    = $("input.use_cartons").val();
	stock_check = $("input.stock_check").val();
	stock_check     = (stock_check == 'Y')?true:false;
	balance_recovered = ($("input.recovered_balance").is(":checked"))?"Y":"N";
	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$.fn.numericFloatOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
					||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
					|| (e.keyCode == 65 && e.ctrlKey === true)
					|| (e.keyCode >= 35 && e.keyCode <= 39)){
						return true;
					}else{
						if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
							e.preventDefault();
						}
					}
				});
			};
			$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
			$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
			$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
			$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
				function() {
					$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
					$(this).addClass('current'); // Add class "current" to clicked tab
					var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
					$(currentTab).siblings().hide(); // Hide all content divs
					$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
					return false;
				}
			);

			//Close button:

			$(".close").click(
				function () {
					$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);
		// Alternating table rows:
		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows
		// Check all checkboxes when the one in a table head is checked:
		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.sumColumn = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseInt($(this).text());
			});
			$(insertToFeild).text(sumAll);
		};
		$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
			var sumAll = 0;
			$(sumOfFeild).each(function(index, element) {
				sumAll += parseFloat($(this).text())||0;
			});
			sumAll = Math.round(sumAll*100)/100;
			$(insertToFeild).text((sumAll).toFixed(2));
		};
		$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
			$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
		};
		$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
			$(this).keyup(function(e){
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var productVal = Math.round((thisVal*thatVal)*100)/100;
				$(writeProductToElm).val(productVal);
			});
		};
		$.fn.deleteMainRow = function(file){
			var idValue    = $(this).attr("do");
			var currentRow = $(this).parent().parent().parent().parent().parent();
			var billNumbr  = currentRow.find('td').first().text();
			$("#xfade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#xfade").fadeIn();
			var win_hi 			= $(window).height()/2;
			var win_width 	= $(window).width()/2;
			win_hi 					= win_hi-$("#popUpDel").height()/2;
			win_width 			= win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel .confirm").text("Are Sure you Want To Delete Bill #"+billNumbr+"?");
			$("#popUpDel").slideDown();
			$(".dodelete").click(function(){
				$.post(file, {cid : idValue}, function(data){
					data = $.parseJSON(data);
					if(data['OK'] == 'Y'){
						$("tr[data-row-id='"+idValue+"']").slideUp();
					}
					$("#popUpDel .confirm").text(data['MSG']);
					$(".nodelete").text('Close');
					$(".dodelete").hide();
				});
			});
			$(".nodelete").click(function(){
				$("#popUpDel").slideUp(function(){
					$(this).remove();
				});
				$("#xfade").fadeOut();
			});
		};

		$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
			$(this).keyup(function(e){
				var taxRate = $("input.taxRate").val();
				var taxType   = ($(".taxType").is(":checked"))?"I":"E";
				var thisVal = parseFloat($(this).val())||0;
				var thatVal = parseFloat($(multiplyToElmVal).val())||0;
				var amount = Math.round((thisVal*thatVal)*100)/100;
				if(taxRate > 0){
					if(taxType == 'I'){
						taxAmount = amount*(taxRate/100);
						amount -= taxAmount;
					}else if(taxType == 'E'){
						taxAmount = amount*(taxRate/100);
					}

				}else{
					taxAmount = 0;
				}
				taxAmount = Math.round(taxAmount*100)/100;
				amount = Math.round(amount*100)/100;
				var finalAmount = Math.round((amount+taxAmount)*100)/100;
				$(taxAmountElm).val(taxAmount);
				$(writeProductToElm).val(finalAmount);
				$(writeProductToElmNoTax).val(amount);
			});
		};
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
		//inventory.php end
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};

		$.fn.getItemDetails = function(){
			var item_id = $(".itemSelector option:selected").val();
			var row_id  = 0;
			$("input.unitPrice").attr('data-title','');
			if(item_id != ''){
				$.post('db/get-item-details.php',{s_item_id:item_id,row_id:0},function(data){
					data = $.parseJSON(data);
					var itemStock = parseInt(data['STOCK'])||0;
					var itemPrice = data['S_PRICE'];
					var itemDisc  = data['DISCOUNT'];
					var itemCosta = data['AVG_COSTA']?data['AVG_COSTA']:0;
					itemCosta     = parseFloat(itemCosta)||0;
					var this_sold = 0;
					$("td[data-type='I'][data-machine-id='"+item_id+"']").each(function(){
						if(!$(this).parent().hasClass("updateMode")){
							this_sold -= parseInt($(this).parent().find("td.quantity").text())||0;
						}
					});
					if($(".updateMode").length){
						this_sold += parseFloat($(".updateMode").find("td.quantity").text())||0;
					}
					itemDisc = (itemDisc > 0)?itemDisc:"";
					itemPrice = (itemPrice > 0)?itemPrice:"";
					if($(".updateMode").length == 0){
						$("input.discount").val(itemDisc);
						$("input.unitPrice").val(itemPrice);
						$("input.unitPrice").attr('data-title',"Avg.Cost : "+(itemCosta).toFixed(2));
					}else{
						$("input.unitPrice").attr('data-title','');
					}
					itemStock += this_sold;
					$("input.inStock").attr('thestock',itemStock).val(itemStock);
					$("input.per_carton").val(data['QTY_CARTON']);
				});
			}
		};
		$.fn.quickSave = function(){
			var machine_id       = $("select.machine_id option:selected").val();
			var machine_rate     = $("select.machine_id option:selected").attr('data-subtext');
			var machine_name     = $("select.machine_id option:selected").text();
			var design_no        = $("input.design_no").val();
			var stitches         = parseFloat($("input.stitches").val())||0;
			var rate        		 = parseFloat($("input.rate").val())||0;
			var amount        	 = parseFloat($("input.amount").val())||0;
			var qty_length       = parseFloat($("input.qty_length").val())||0;
			var final_amount     = parseFloat($("input.final_amount").val())||0;

			var updateMode    = $(".updateMode").length;
			var update_row_id = 0;
			if(updateMode){
				update_row_id = parseInt($(".updateMode").attr('data-row-id'))||0;
			}
			if(machine_id != 0 && amount != 0){
				var $theNewRow   = '<tr class="alt-row transactions" data-row-id="'+update_row_id+'">';
				$theNewRow      += '<td style="text-align:center;" class="machine_id" data-rate="'+machine_rate+'" data-machine-id="'+machine_id+'">'+machine_name+'</td>';
				$theNewRow      += '<td class="text-center design_no">'+design_no+'</td>';
				$theNewRow      += '<td class="text-center stitches">'+stitches+'</td>';
				$theNewRow 			+= '<td class="text-center rate">'+rate+'</td>';
				$theNewRow 			+= '<td class="text-center amount">'+amount+'</td>';
				$theNewRow 			+= '<td class="text-center qty_length">'+qty_length+'</td>';
				$theNewRow 			+= '<td class="text-center final_amount">'+final_amount+'</td>';
				$theNewRow 			+= '<td class="text-center">';
				$theNewRow 			+= '<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>';
				$theNewRow 			+= '<a class="pointer" onclick="showDeleteRowDialogue(this);" title="Delete"><i class="fa fa-times"></i></a>';
				$theNewRow 			+= '</td>';
				$theNewRow 			+= '</tr>';
				if(updateMode == 0){
					$(".transactions_list").append($theNewRow);
				}else if(updateMode == 1){
					$(".updateMode").replaceWith($theNewRow);
				}
				$(this).clearPanelValues();
				$(this).calculateRowTotal();
				$(this).calculateColumnTotals();
				$("div.machine_id button").focus();
			}else{
				displayMessage('Values Missing!');
			}
		};
		$.fn.calculateColumnTotals = function(){
			$(this).sumColumnFloat("td.stitches","th.total_stitches");
			$(this).sumColumnFloat("td.qty_length","th.total_length");
			$(this).sumColumnFloat("td.final_amount","th.total_amount");
		};
		$.fn.addToThis = function(that_amount){
			var this_amount = parseFloat($(this).val())||0;
			$(this).va(that_amount+this_amount);
		};
		$.fn.calculateRowTotal = function(){
			var machine_rate,stitches,stitch_rate,total_amount;
			machine_rate = parseFloat($("select.machine_id option:selected").attr("data-subtext"))||0;
			stitches     = parseFloat($("input.stitches").val())||0;
			stitch_rate  = parseFloat($("input.rate").val())||0;
			total_amount = parseFloat((stitch_rate*machine_rate*stitches)/1000)||0;
			$("input.amount").val((total_amount).toFixed(2));

		};
		$.fn.updateStockInHand = function(){
			var stockOfBill = 0;
			$("td.quantity").each(function(index, element) {
				stockOfBill += parseInt($(this).text())||0;
			});
			stockOfBill += parseInt($("input.quantity").val())||0;
		};

		$.fn.clearPanelValues = function(){
			$("select.machine_id").selectpicker('val','');
			$("input.design_no,input.stitches,input.rate,input.amount").val('');
		};
	});
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var showDeleteRowDialogue = function(rowChildElement){
		var clickedDel 			 = $(rowChildElement);
		var detail_row_id 	 = parseInt($(rowChildElement).parent().parent().attr("data-row-id"))||0;
		if($(rowChildElement).parent().parent().hasClass('updateMode')){
			displayMessage('Record is in edit Mode!');
			return false;
		}
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
			if(detail_row_id>0){
				$("div#form").append('<input type="hidden" value="'+detail_row_id+'" class="deleted_detail_rows" />');
			}
			$(this).calculateColumnTotals();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
		});
		$(".close_popup").click(function(){
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('fast');
		});
	};
	var validatePhone = function(txtPhone) {
		var a = $(txtPhone).val();
		var filter = /^[0-9-+]+$/;
		if(a[0] == 0){
			a = a.substring(1,11);
		}
		if(a[0] != 3){
			return false;
		}
		if(a.length != 10){
			return false;
		}
		if(filter.test(a)) {
			return a;
		}else{
			return false;
		}
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var machine_id 	 = parseInt(thisRow.find('td.machine_id').attr('data-machine-id'))||0;
		var design_no    = thisRow.find('td.design_no').text();
		var stitches     = thisRow.find('td.stitches').text();
		var rate    		 = thisRow.find('td.rate').text();
		var amount	     = thisRow.find('td.amount').text();
		var qty_length	 = thisRow.find('td.qty_length').text();
		var final_amount = thisRow.find('td.final_amount').text();

		$("select.machine_id").selectpicker('val',machine_id);
		$("input.design_no").val(design_no);
		$("input.stitches").val(stitches);
		$("input.rate").val(rate);
		$("input.amount").val(amount);
		$("input.qty_length").val(qty_length);
		$("input.final_amount").val(final_amount);
		$("div.machine_id button").focus();
	};

	var saveSale = function(){
		$("select.account_code").selectpicker("refresh");
		var sale_id		     = $("input.sale_id").val();
		var sale_date 	   = $("input.sale_date").val();
		var bill_no  	     = $("input[name='billNum']").val();
		var lot_no  	     = $("input[name='lot_no']").val();
		var gp_no  	     	 = $("input[name='gp_no']").val();
		var subject		     = ($("input[name='subject']").length)?$("input[name='subject']").val():"";
		var account_code   = $("select.account_code option:selected").val();
		var inv_notes      = $("textarea.inv_notes").val();


		if(account_code == ''){
			displayMessage('Please select an Account!');
			$("div.account_code button").addClass("btn-warning");
			$("div.account_code button").focus();
			return false;
		}

		if($("tr.transactions").length == 0){
			displayMessage('No Transaction Existed, Bill Can not be Saved!');
			$("div.itemSelector button").addClass("btn-warning");
			$("div.itemSelector button").focus();
			return false;
		}

		$(".save_sale").hide();
		$("#xfade").fadeIn();
		var jSonString = {},deleted_ids = {};
		$("tr.transactions").each(function(index, element){
			jSonString[index] = {};
			jSonString[index].row_id       		 = $(this).attr('data-row-id');
			jSonString[index].machine_id       = $(this).find("td.machine_id").attr('data-machine-id');
			jSonString[index].machine_rate     = $(this).find("td.machine_id").attr('data-rate');
			jSonString[index].design_no        = $(this).find("td.design_no").text();
			jSonString[index].stitches         = parseFloat($(this).find("td.stitches").text())||0;
			jSonString[index].rate        		 = parseFloat($(this).find("td.rate").text())||0;
			jSonString[index].amount        	 = parseFloat($(this).find("td.amount").text())||0;
			jSonString[index].qty_length       = parseFloat($(this).find("td.qty_length").text())||0;
			jSonString[index].final_amount     = parseFloat($(this).find("td.final_amount").text())||0;
		});
		$("input.deleted_detail_rows").each(function(index,element){
			deleted_ids[index] = parseInt($(this).val())||0;
		});
		deleted_ids= JSON.stringify(deleted_ids);
		jSonString = JSON.stringify(jSonString);
		$.post("db/saveEmbSale.php",{
			sale_id:sale_id,
			sale_date:sale_date,
			bill_no:bill_no,
			lot_no:lot_no,
			gp_no:gp_no,
			subject:subject,
			account_code:account_code,
			inv_notes:inv_notes,
			deleted_rows:deleted_ids,
			jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					var msg_type = '';
					if(sale_id == 0){
						msg_type = "&saved";
					}else{
						msg_type = "&updated";
					}
					var print_opt = '';
					if($(".invoice-type").val()=='S'){
						print_opt = "&print"
					}else if(print_method=='Y'){
						print_opt = "&print"
					}
					var the_id = "id="+data['ID'];
					if(redirect_method == 'N'){
						the_id = 'method='+data['ID'];
					}
					window.location.href = 'emb-sale-details.php?'+the_id+msg_type+print_opt;
				}else{
					displayMessage(data['MSG']);
					$(".save_sale").show();
					$("#xfade").fadeOut();
				}
			});
		};
		var hidePopUpBox = function(){
			$("#popUpBox").css({'left':'-=500px'});
			$("#popUpBox").css({'transform':'scaleY(0.0)'});
			$("#xfade").fadeOut();
			$("#popUpBox").remove();
		};
		var add_supplier = function(){
			var url,captions,args = {},code_type;
			code_type = 'A';
			url = 'customer-detail.php'
			captions = 'Customer';
			args['supp_name'] = '';
			$("body").append("<div id='popUpBox'></div>");
			var formContent  = '';
			formContent += '<button class="btn btn-danger btn-xs pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>';
			formContent += '<div id="form">';
			formContent += '<p>New '+captions+' Title:</p>';
			formContent += '<p class="textBoxGo">';
			formContent += '<input type="text" class="form-control categoryName" />';
			formContent += '</p>';
			formContent += '</div>';
			$("#popUpBox").append(formContent);
			$("#xfade").fadeIn();
			$("#popUpBox").fadeIn('200').centerThisDiv();
			$(".categoryName").focus();
			$(".categoryName").keyup(function(e){
				var name = $(".categoryName").val();
				if(code_type == 'B'){
					args['title'] = name;
				}else if(code_type == 'A'){
					args['supp_name'] = name;
				}
				if(e.keyCode == 13){
					$(".categoryName").blur();
					if(name != ''){
						$.post(url,args,function(data){
							data = $.parseJSON(data);
							if(data['OK'] == 'Y'){
								$("select.account_code").append('<option data-subtext="'+data['ACC_CODE']+'" value="'+data['ACC_CODE']+'">'+data['ACC_TITLE']+'</option>');
								$("select.account_code option:selected").prop('selected',false);
								$("select.account_code option").last().prop('selected',true);
								$("select.account_code").selectpicker('refresh');
								hidePopUpBox();
							}else if(data['OK'] == 'N'){
								alert('New '+captions+' Could Not be Created!');
							}
						});
					}
				}
			});
		};
