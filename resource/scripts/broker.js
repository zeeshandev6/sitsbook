//delete broker
function deleteBroker(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).attr("value");
        var del = $.post('brokers-management.php', {delete_id:id}, function(data){});
        if(del){
            $(x).parent().parent().remove();
        }
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
