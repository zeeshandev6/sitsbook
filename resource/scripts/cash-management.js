$(function(){
	$.fn.deleteRow = function(idValue,rowToHide){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$.post("db/deleteCheque.php", {cash_id:idValue}, function(data){
				data = $.parseJSON(data);
				$("#popUpDel p.confirm").text(data['message']);
				$(".dodelete").hide();
				$(".nodelete").text('Close');
				if(data['confirmation']){
					rowToHide.hide();
				}
			});
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut('slow');
			$("#popUpDel").fadeOut(function(){
				$("#popUpDel").remove();
			});
		});
	};
	$.fn.escFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==27){
				e.preventDefault();
				$(Elm).focus();
			}
		});
	};
	$.fn.getAccountBalance = function(focusToElement){
		var supplierAccCode = $(this).val();
		$.post('db/get-account-balance.php',{supplierAccCode:supplierAccCode},function(data){
			data = $.parseJSON(data);
			$("input[name='balanceBefore']").val(data['BALANCE']);
			$(document).find(focusToElement).focus();
		});
	};
});