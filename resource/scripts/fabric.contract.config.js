$(document).on('click', 'input[name=save]', function(){
    $(this).prop("disabled",true);

    if($("select[name='supplier_id'] option:selected").val() == ''){
        notifyMe('uploads/logo/default.png','Error!','Supplier not selected.',null);
        $(this).prop("disabled",false);
        return;
    }

    if($("tr.table_row").length == 0){
        notifyMe('uploads/logo/default.png','Error!','No transaction exists.',null);
        $(this).prop("disabled",false);
        return false;
    }
    //table data
    var jsonString;
    var arrReturn   = {};
    var submit_form = true;
    var inputRow,row,row_id,construction,quantity,unit_price,amount;
    $('tr.table_row').each(function(x){
        arrReturn[x] = {};
        inputRow = $(this);
        row_id   = parseInt($(inputRow).attr("data-id"))||0;
        arrReturn[x].row_id       = row_id;
        arrReturn[x].construction = $(inputRow).find('td.construction').text();
        arrReturn[x].variety      = $(inputRow).find('td.variety').text();
        arrReturn[x].thaan        = $(inputRow).find('td.thaan').text();
        arrReturn[x].quantity     = parseFloat($(inputRow).find('td.quantity').text())||0;
        arrReturn[x].unit_price   = parseFloat($(inputRow).find('td.unit_price').text())||0;
        arrReturn[x].amount       = parseFloat($(inputRow).find('td.amount').text())||0;
        if(arrReturn[x][quantity] == 0){
            submit_form = false;
        }
    });
    jsonString = JSON.stringify(arrReturn);
    if(submit_form){
        $('input[name=details]').val(jsonString);
        $('form[name=add-history]').submit();
        $('#myLoading').modal('show');
    }else{
      alert('no submition');
    }
    $(this).prop("disabled",false);
});
$(document).ready(function(){
    $("#pro_table").change(function(){
        calcoltotal();
    });
    var qtyStock = '';
    var stock    = '';
    $($('select#item')).on('change', function(){
        var items_vall  = parseInt($('select#item').val())||0;
        var godown_id   = parseInt($("select[name=location] option:selected").val())||0;
        if(godown_id == 0||items_vall == 0){
            return;
        }
        var items_txtt  = $('select#item option:selected').text();
        var stockInHand = $.post('db/item_stock_qty.php', {itemStockGodown:items_vall,godown_id:godown_id}, function(data){
            stock = $.parseJSON(data);
            qtyStock = stock['stock'];
            $('input.stock_hand,input[name=stock_hand]').val(qtyStock);
        });
    });
    $("#pro_table").change();
});
function calcoltotal(){
    var cartons = 0;
    var qty     = 0;
    $("#pro_table tbody tr").each(function(){
        cartons += (parseFloat($(this).find("td#carton").text())||0);
        qty     += (parseFloat($(this).find("td#qty_receipt").text())||0);
    });
    $("#pro_table tfoot tr td.cartons_total").text(cartons);
    $("#pro_table tfoot tr td.qty_total").text(qty);
}
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).parent().parent().attr("data-id");
        $("form[name=add-history]").append('<input type="hidden" value="'+id+'" name="deleted_rows[]" />');
        $(x).parent().parent().remove();
        x = null;
        $("#pro_table").change();
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
//adding neow row in detail table
$(document).ready(function(){
    $('select').selectpicker();
    $("input.quantity,input.unit_price").on('keyup blur',function(){
      var qty,price,amount;
      qty=parseFloat($("input.quantity").val())||0;
      prc=parseFloat($("input.unit_price").val())||0;
      $("input.amount").val((qty*prc).toFixed(2));
    });
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow,row,row_id,construction,quantity,unit_price,amount,thaan,variety;
        inputRow = $(this).parent().parent();

        construction = $(inputRow).find('input.construction').val();
        quantity     = parseFloat($(inputRow).find('input.quantity').val())||0;
        unit_price   = parseFloat($(inputRow).find('input.unit_price').val())||0;
        amount       = parseFloat($(inputRow).find('input.amount').val())||0;
        thaan        = $(inputRow).find('input.thaan').val();
        variety      = $(inputRow).find('input.variety').val();

        if(amount == 0 || construction == ''){
            notifyMe('uploads/logo/default.png','Error!','Values missing.',null);
            $(this).prop("disabled",false);
            return;
        }

        row = '<tr class="table_row" data-id="0">'
          +'<td class="construction text-left">'+construction+'</td>'
          +'<td class="variety text-center">'+variety+'</td>'
          +'<td class="thaan text-center">'+thaan+'</td>'
          +'<td class="quantity text-center">'+quantity+'</td>'
          +'<td class="unit_price text-center">'+unit_price+'</td>'
          +'<td class="amount text-center">'+amount+'</td>'
          +'<td class="text-center">'
            +'<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
            +'<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>'
          +'</td>'
        +'</tr>';

        if($(".updatemode").length){
          row_id =parseInt($(".updatemode").attr("data-id"))||0;
          $(row).insertAfter(".updatemode");
          $(".updatemode").next("tr").attr("data-id",row_id);
          $(".updatemode").remove();
        }else{
          $('#pro_table').append(row);
        }
        $(inputRow).find("input:text").val('');
        $(inputRow).find("input.construction").focus();
    });//End appending new row
});

//edit table row in update popup
function editMe(elm){
    var thisRow,row,construction,quantity,unit_price,amount,thaan,variety;
    thisRow = $(elm).parent().parent();

    $(".updatemode").removeClass('updatemode');
    $(thisRow).addClass('updatemode');

    construction = $(thisRow).find('td.construction').text();
    quantity     = parseFloat($(thisRow).find('td.quantity').text())||0;
    unit_price   = parseFloat($(thisRow).find('td.unit_price').text())||0;
    amount       = parseFloat($(thisRow).find('td.amount').text())||0;
    thaan        = $(thisRow).find('td.thaan').text();
    variety      = $(thisRow).find('td.variety').text();

    $("tr.inputRow input.construction").val(construction);
    $("tr.inputRow input.quantity").val(quantity);
    $("tr.inputRow input.unit_price").val(unit_price);
    $("tr.inputRow input.amount").val(amount);
    $("tr.inputRow input.thaan").val(thaan);
    $("tr.inputRow input.variety").val(variety);

    $("tr.inputRow").find("input.construction").focus();
}
