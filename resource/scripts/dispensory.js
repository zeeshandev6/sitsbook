$(document).ready(function(){
	$("#saveButton").hide();
	$("#saveButton").click(function(){
		dispensoryProcess();
	});
	
	//For Side
	hideSideBarOnBlur();
	
	//Actions For user
	$(".quickSave").keydown(function(e){
		if(e.keyCode == 13){
			$(this).quickSave();
		}
	});
	$(".quickSave").click(function(){
		$(this).quickSave();
	});
	$(".printPage").click(function(){
		var patient_id = $(".patient_id").val();
		if(patient_id > 0){
			window.location.href = 'print-prescription.php?id='+patient_id;
		}
	});
});

$(window).load(function(){
	initializeEvents();
});

var deleteRow = function(thisElm){
	var type = $(thisElm).attr('data-type');
	var thisRow = $(thisElm).parent('td').parent('tr');
	var row_id = parseInt($(thisElm).attr('data-id')) || 0;
	var message = "Confirm Deletion?";
	if(row_id > 0){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$(".dodelete").hide();
			$.post('db/deletePrescrition.php',{id:row_id,type:type},function(data){
				data = $.parseJSON(data);
				if(data['OK'] == 'Y'){
					thisRow.fadeOut(function(){
						$(this).remove();
					});
					$("#popUpDel .confirm").text("Record Deleted Successfully!");
					$(".nodelete").text('Close');
				}else{
					$("#popUpDel .confirm").text("Error while Deleting Record!");
					$(".nodelete").text('Close');
				}
			});
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	}
};
var initializeEvents = function(){
	$("div.medicineSelector").find(".dropdown-toggle").keyup(function(e){
		if(e.keyCode == 13){
			$(this).parent().parent().parent().find("input.duration").focus();
		}
	});
	$("input.duration").keyup(function(e){
		if(e.keyCode == 13 && $(this).val() != ''){
			$(this).parent().parent().find("input.instructions").focus();
		}
	});
	$("input.instructions").keyup(function(e){
		if(e.keyCode == 13){
			$(this).parent().parent().find("input.quickSave").focus();
		}
	});
};

var toggleSideBar = function(){
	if($(".sideBar").hasClass("sideBarOpen")){
		$(".sideBar").removeClass("sideBarOpen");
		$(".sideBar").addClass("sideBarClose");
	}else{
		$(".sideBar").removeClass("sideBarClose");
		$(".sideBar").addClass("sideBarOpen");
	}
	getPatientsList();
	countPatients();
};
var countPatients = function(){
	var patientsCount = $(".sideBar ul li").length;
	$(".sideBar .confirm .numPatients").text('').text("( "+patientsCount+" )");
};
var getPatientsList = function(){
	$.post('db/getPatientList.php',{liChecked:true},function(data){
		$(".sideBar ul").html(data);
		countPatients();
	});
};
var getPatientsDetails = function(thisElem){
	$(".lightFade").fadeIn();
	idVal = parseInt($(thisElem).attr('data-id'));
	
	$('tr.ajaxRow').remove();
	
	$.post('db/getPrescriptions.php',{check:idVal},function(data){
		$(".docTbody").append(data);
		$("#saveButton").show();
		$(".docTable").show();
	});
	$.post('db/getPatientDetails.php',{id:idVal},function(data){
		var patientDetails = $.parseJSON(data);
		$(".patient_id").val(idVal);
		$("input[name='token']").val(patientDetails['TOKEN_NO']);
		$("input[name='patientName']").val(patientDetails['NAME']);
		$("input[name='age']").val(patientDetails['AGE']);
		if(patientDetails['GENDER'] == 'M'){
			patientDetails['GENDER'] = 'Male';
		}else if(patientDetails['GENDER'] == 'F'){
			patientDetails['GENDER'] = 'Female';
		}else if(patientDetails['GENDER'] == 'C'){
			patientDetails['GENDER'] = 'Child';
		}
		$("input[name='gender']").val(patientDetails['GENDER']);
		var priority = 'Normal';
		if(patientDetails['PRIORITY'] == 'E'){
			priority = 'Emergency';
		}
		$("input[name='priority']").val(priority);
		$("input[name='mobile']").val(patientDetails['MOBILE']);
		$(".bP").val(patientDetails['BP']);
		$(".temp").val(patientDetails['TEMP']);
		$(".doc_fee").val(patientDetails['DOC_FEE']);
		$(".lab_fee").val(patientDetails['LAB_FEE']);
		$(".diagnosis").val(patientDetails['DIAGNOSIS']);
		toggleSideBar();
		$(".lightFade").hide();
		if(patientDetails['BP'] == ''){
			$(".bP").focus();
		}else{
			$("div.medicineSelector").first().find(".dropdown-toggle").focus();
		}
	});
};
var editRow = function(thisElm){
	$(".editMode").removeClass('editMode');
	var $thisRow = $(thisElm).parent().parent();
	var thisType = $(thisElm).attr('data-type');
	var row_id   = $(thisElm).attr('data-id');
	var $thisTypeInput = $('.quickSave[data-type="'+thisType+'"]').parent('td').parent('tr');
	
	var medicineName = $thisRow.find(".medicineTd").text();
	var dosage       = $thisRow.find(".dosageTd").text();
	var duration     = parseInt($thisRow.find(".durationTd").text())||0;
	var instructions = $thisRow.find(".istructionTd").text();
	
	$thisTypeInput.find(".medicineSelector option").each(function(index, element) {
        if($(this).attr('data-name') == medicineName){
			$(this).prop('selected',true).siblings().prop('selected',false);
		}
    });
	$(".medicineSelector").selectpicker("refresh");
	
	dosage = dosage.split('|');
	$thisTypeInput.find(".dosage").each(function(index, element) {
		var currentElem    = $(this);
		var currentElemVal = $(this).val();
		var found = $.inArray(currentElemVal,dosage);
		if(found !== -1){
			currentElem.prop('checked',true);
		}else{
			currentElem.prop('checked',false);
		}
	});
	$thisTypeInput.find(".duration").val(duration);
	$thisTypeInput.find(".instructions").val(instructions);
	
	$thisRow.addClass('editMode');
};

var dispensoryProcess = function(){
	if($(".distribute").length){
		var patient_id = $(".patient_id").val();
		var id_list = '';
		$(".distribute:checked").each(function(index, element) {
			var row_id = 'Go';
			if($(this).val() != 'Go'){
            	row_id = parseInt($(this).val())||0;
			}
			if(index > 0){
				id_list += '|';
			}
			if(row_id != '' && row_id != 0){
				id_list += row_id;
			}
        });
		$.post('db/postDispensory.php',{patient_id:patient_id,id_list:id_list},function(data){
			if(data == ''){
				window.location.href = 'dispensory.php';
			}
		});
	}
};
var checkEmAll = function(thisElm){
	$(".distribute").prop('checked',$(thisElm).prop('checked'));
};

//side bar hide function
var hideSideBarOnBlur=function(){$(window).click(function(e){var container = $(".sideBar");if(!container.is(e.target) && container.has(e.target).length === 0){if(container.hasClass("sideBarOpen")){container.removeClass("sideBarOpen");container.addClass("sideBarClose");}}});};