$(document).on('click', 'input[name=save]', function(){
    $(this).prop("disabled",true);

    if($("tr.table_row").length == 0){
        alert("At least One Item is required!");
        $(this).prop("disabled",false);
        return false;
    }
    //table data
    var jsonString;
    var arrReturn   = {};
    var submit_form = true;
    var inputRow,row,row_id,construction,grey_width,quantity;
    $('tr.table_row').each(function(x){
        arrReturn[x] = {};
        inputRow = $(this);
        row_id   = parseInt($(inputRow).attr("data-id"))||0;
        arrReturn[x].row_id        = row_id;
        arrReturn[x].construction  = $(inputRow).find('td.construction').text();
        arrReturn[x].grey_width    = $(inputRow).find('td.grey_width').text();
        arrReturn[x].thaan         = $(inputRow).find('td.thaan').text();
        arrReturn[x].quantity      = parseFloat($(inputRow).find('td.quantity').text())||0;
        arrReturn[x].rate          = parseFloat($(inputRow).find('td.rate').text())||0;
        arrReturn[x].total_amount  = parseFloat($(inputRow).find('td.total_amount').text())||0;
        arrReturn[x].fab_type      = $(inputRow).find('td.fab_type').text();
        arrReturn[x].design        = $(inputRow).find('td.design').text();
        arrReturn[x].color         = $(inputRow).find('td.color').text();
        if(arrReturn[x][quantity] == 0){
            submit_form = false;
        }
    });
    jsonString = JSON.stringify(arrReturn);
    if(submit_form){
        $('input[name=details]').val(jsonString);
        $('form[name=add-history]').submit();
        $('#myLoading').modal('show');
    }else{
      alert('no submition');
    }
    $(this).prop("disabled",false);
});
$(document).ready(function(){
    $("#pro_table").change(function(){
        calcoltotal();
    });
    var qtyStock = '';
    var stock    = '';
    $($('select#item')).on('change', function(){
        var items_vall  = parseInt($('select#item').val())||0;
        var godown_id   = parseInt($("select[name=location] option:selected").val())||0;
        if(godown_id == 0||items_vall == 0){
            return;
        }
        var items_txtt  = $('select#item option:selected').text();
        var stockInHand = $.post('db/item_stock_qty.php', {itemStockGodown:items_vall,godown_id:godown_id}, function(data){
            stock = $.parseJSON(data);
            qtyStock = stock['stock'];
            $('input.stock_hand,input[name=stock_hand]').val(qtyStock);
        });
    });
    $("#pro_table").change();
});
function calcoltotal(){
    var cartons = 0;
    var qty     = 0;
    $("#pro_table tbody tr").each(function(){
        cartons += (parseFloat($(this).find("td#carton").text())||0);
        qty     += (parseFloat($(this).find("td#qty_receipt").text())||0);
    });
    $("#pro_table tfoot tr td.cartons_total").text(cartons);
    $("#pro_table tfoot tr td.qty_total").text(qty);
}
function deleteMe(x){
    $("#myConfirm").modal('show');
    $(document).on('click', 'button#delete', function(){
        var id = $(x).parent().parent().attr("data-id");
        $("form[name=add-history]").append('<input type="hidden" value="'+id+'" name="deleted_rows[]" />');
        $(x).parent().parent().remove();
        x = null;
        $("#pro_table").change();
    });
    $(document).on('click', 'button#cancell', function(){
        x = null;
    });
}
//adding neow row in detail table
$(document).ready(function(){
    $('select').selectpicker();
    //appending new row
    $('.enter_new_row').on('click', function(){
        var inputRow,row,row_id,construction,grey_width,quantity,fab_type;
        inputRow = $(this).parent().parent();

        construction = $(inputRow).find('input.construction').val();
        grey_width   = $(inputRow).find('input.grey_width').val();
        thaan        = $(inputRow).find('input.thaan').val();
        quantity     = parseFloat($(inputRow).find('input.quantity').val())||0;
        rate         = $(inputRow).find('input.rate').val();
        total_amount = $(inputRow).find('input.total_amount').val();
        fab_type     = $(inputRow).find('input.fab_type').val();
        design       = $(inputRow).find('input.design').val();
        color        = $(inputRow).find('input.color').val();

        row = '<tr class="table_row" data-id="0">'
          +'<td class="construction text-left">'+construction+'</td>'
          +'<td class="grey_width text-left">'+grey_width+'</td>'
          +'<td class="thaan text-center">'+thaan+'</td>'
          +'<td class="quantity text-center">'+quantity+'</td>'
          +'<td class="rate text-center">'+rate+'</td>'
          +'<td class="total_amount text-center">'+total_amount+'</td>'
          +'<td class="fab_type text-center">'+fab_type+'</td>'
          +'<td class="design text-center">'+design+'</td>'
          +'<td class="color text-center">'+color+'</td>'
          +'<td class="text-center">'
            +'<a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a> '
            +'<a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>'
          +'</td>'
        +'</tr>';

        if($(".updatemode").length){
          row_id =parseInt($(".updatemode").attr("data-id"))||0;
          $(row).insertAfter(".updatemode");
          $(".updatemode").next("tr").attr("data-id",row_id);
          $(".updatemode").remove();
        }else{
          $('#pro_table').append(row);
        }

        $(inputRow).find("input:text").val('');
        $(inputRow).find("input.construction").focus();
    });//End appending new row
});

//edit table row in update popup
function editMe(elm){
    var thisRow,row,construction,grey_width,quantity;
    thisRow = $(elm).parent().parent();

    $(".updatemode").removeClass('updatemode');
    $(thisRow).addClass('updatemode');

    construction  = $(thisRow).find('td.construction').text();
    grey_width    = $(thisRow).find('td.grey_width').text();
    thaan         = $(thisRow).find('td.thaan').text();
    quantity      = parseFloat($(thisRow).find('td.quantity').text())||0;
    rate          = parseFloat($(thisRow).find('td.rate').text())||0;
    total_amount  = parseFloat($(thisRow).find('td.total_amount').text())||0;
    fab_type      = $(thisRow).find('td.fab_type').text();
    design        = $(thisRow).find('td.design').text();
    color         = $(thisRow).find('td.color').text();

    $("tr.inputRow input.construction").val(construction);
    $("tr.inputRow input.grey_width").val(grey_width);
    $("tr.inputRow input.thaan").val(thaan);
    $("tr.inputRow input.quantity").val(quantity);
    $("tr.inputRow input.rate").val(rate);
    $("tr.inputRow input.total_amount").val(total_amount);
    $("tr.inputRow input.fab_type").val(fab_type);
    $("tr.inputRow input.design").val(design);
    $("tr.inputRow input.color").val(color);

    $("tr.inputRow").find("input.construction").focus();
}
