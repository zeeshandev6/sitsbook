	$(document).ready(function() {
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					$(Elm).focus();
				}
			});
		};
		$("select.machine_select").change(function(){
			var rpm = parseInt($(this).find("option:selected").attr("data-rpm"))||0;
			$("input[name=machine_rpm]").val(rpm);
			$("input[name=saleRate]").val(parseInt($(this).find("option:selected").attr("data-rate"))||0);
			$("input.stitch_shift_aa,input.stitch_shift_bb").val(rpm*20*60);
		});
		$.fn.clearPanel = function(){
			//$("select.party_code").selectpicker('val','');
			//$("input.farbric_type").val('');
			//$("input.thread_type").val('');
			//$("input.design_no").val('');
			//$("select.product_id").selectpicker('val','');
			$("input.prod_shift_a").val('');
			$("input.prod_shift_b").val('');
			$("input.total_prod").val('');
			$("input.yard").val('');
			$("input.thread_weight").val('');
			$("input.rate").val('');
			$("input.thread_price").val('');
			$("input.total_price").val('');
			$("input.stitch").val('');
			$("input.stitch_a").val('');
			$("input.stitch_b").val('');
			$("input.sale_rate").val('');
			$("input.sale").val('');
			$("input.farbric_type").focus();
		};
		$("input.sale_rate").focus(function(){
			$(this).val($("input[name='saleRate']").val());
		});
	});
	var multiplyTwoInputsOutPutToInput = function($input1,$input2,$output){
		var product = $($input1).val()*$($input2).val();
		$($output).val(Math.round(product,2));
	};
	var addTwoInputsOutPutToInput = function($input1,$input2,$output){
		var product = $($input1).val()+$($input2).val();
		$($output).val(Math.round(product));
	};
	var get_machine_production_main = function(){
		var report_date = $("input[name='report_date']").val();
		var machine_id = $("select[name='machine_id']").val();

		$.get('db/get-machine-production-by-machine-id.php',{report_date:report_date,machine_id:machine_id},function(data){
			if(data == ''){
				return false;
			}
			data = $.parseJSON(data);
			$("textarea[name='comment']").val(data['COMMENT']);
			$("input[name='production_id']").val(data['ID']);
			$("select[name='party_acc_code']").selectpicker('val',data['PARTY_ACC_CODE']);
			$("input[name='expense']").val(data['EXPENSE']);
		});
	};
// delete machine
	$(function(){
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
	});
	var deleteRow = function(rowChildElement){
		var id 					= parseInt($(rowChildElement).attr("do"))||0;
		var record_row  = $(rowChildElement).parent().parent();
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-default'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$(record_row).remove();
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			if(id>0){
				$("div#form").append('<input type="hidden" class="delete_rows" value="'+id+'">');
			}
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
		});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#fade").fadeOut('fast');
		});
	};

//enter on save
$.fn.saveOnEnter = function(){
	var row_id 				= 0;
	var party_code 		= $("select.party_code option:selected").val();
	var party_title 	= $("select.party_code option:selected").text();
	var farbric_type 	= $("input.farbric_type").val();
	var thread_type 	= $("input.thread_type").val();
	var design_no 		= $("input.design_no").val();
	var item_id 			= $("select.product_id option:selected").val();
	var item 					= $("select.product_id option:selected").text();
	var prod_shift_a 	= $("input.prod_shift_a").val();
	var prod_shift_b 	= $("input.prod_shift_b").val();
	var total_pord 		= $("input.total_prod").val();
	var yard 					= $("input.yard").val();
	var thread_weight = $("input.thread_weight").val();
	var rate 					= parseFloat($("input.rate").val())||0;
	var thread_price 	= parseFloat($("input.thread_price").val())||0;
	var total_price 	= parseFloat($("input.total_price").val())||0;
	var stitch 				= $("input.stitch").val();
	var stitch_a 			= parseInt($("input.stitch_a").val())||0;
	var stitch_b 			= parseInt($("input.stitch_b").val())||0;
	var sale_rate 		= parseFloat($("input.sale_rate").val())||0;
	var sale 					= parseFloat($("input.sale").val())||0;
	input_row_calculations();
	if($(".updateRecord").length){
		row_id = parseInt($(".updateRecord").attr("data-row-id"))||0;
	}
	var row = '';
	row += "<tr class='calculations' data-row-id='"+row_id+"'>";
	row += "<td class='party_code' data-acc-code='"+party_code+"' >"+party_title+"</td>";
	row += "<td class='ft' >"+farbric_type+"</td>";
	row += "<td class='tt'>"+thread_type+"</td>";
	row += "<td class='dn'>"+design_no+"</td>";
	row += "<td class='it' data-id='"+item_id+"'>"+item+"</td>";
	row += "<td class='pda'>"+prod_shift_a+"</td>";
	row += "<td class='pdb'>"+prod_shift_b+"</td>";
	row += "<td class='tp'>"+total_pord+"</td>";
	row += "<td class='yr'>"+yard+"</td>";
	row += "<td class='tw'>"+thread_weight+"</td>";
	row += "<td class='ra'>"+rate.toFixed(2)+"</td>";
	row += "<td class='thp'>"+thread_price.toFixed(2)+"</td>";
	row += "<td class='thc'>"+total_price.toFixed(2)+"</td>";
	row += "<td class='st'>"+stitch+"</td>";
	row += "<td class='sta'>"+stitch_a+"</td>";
	row += "<td class='stb'>"+stitch_b+"</td>";
	row += "<td class='sr'>"+sale_rate.toFixed(2)+"</td>";
	row += "<td class='sal'>"+sale.toFixed(2)+"</td>";
	row += "<td>";
	row += '<button class="btn btn-primary btn-xs" onclick="editThisRow(this)"  row-id="0"><i class="fa fa-pencil"></i>';
	row += '<button class="btn btn-danger btn-xs" onclick="deleteRow(this)" do="0"><i class="fa fa-times"></i></button>';
	row += "</td>";
	row += "</tr>";
	if($(".updateRecord").length){
		$(".updateRecord").replaceWith(row);
		$(".updateRecord").removeClass('updateRecord');
	}else{
		$(".appendtbody").append(row);
	}
	$(document).calculateColumnTotals();
	$(this).clearPanel();
};
var editThisRow = function (thisrow){
	$(".updateRecord").removeClass('updateRecord');
	$(thisrow).parent().parent().addClass('updateRecord');
	var party_code					= $(thisrow).parent().parent().find('td.party_code').attr('data-acc-code');
	var f_type 							= $(thisrow).parent().parent().find('td.ft').html();
	var thread_type 				= $(thisrow).parent().parent().find('td.tt').html();
	var design 							= $(thisrow).parent().parent().find('td.dn').html();
	var item 								= $(thisrow).parent().parent().find('td.it').attr('data-id');
	var prod_shiftA 				= $(thisrow).parent().parent().find('td.pda').html();
	var prod_shiftB 				= $(thisrow).parent().parent().find('td.pdb').html();
	var total_prod 					= $(thisrow).parent().parent().find('td.tp').html();
	var yard 								= $(thisrow).parent().parent().find('td.yr').html();
	var thread_weight 			= $(thisrow).parent().parent().find('td.tw').html();
	var rate 								= $(thisrow).parent().parent().find('td.ra').html();
	var thread_price 				= $(thisrow).parent().parent().find('td.thp').html();
	var total_price 				= $(thisrow).parent().parent().find('td.thc').html();
	var stitch 							= $(thisrow).parent().parent().find('td.st').html();
	var stitch_a 						= $(thisrow).parent().parent().find('td.sta').html();
	var stitch_b 						= $(thisrow).parent().parent().find('td.stb').html();
	var sale_rate 					= $(thisrow).parent().parent().find('td.sr').html();
	var sale 								= $(thisrow).parent().parent().find('td.sal').html();

	$("select.party_code").selectpicker('val',party_code);
	$("input.farbric_type").val(f_type);
	$(".thread_type").val(thread_type);
	$(".design_no").val(design);
	$("select.product_id").selectpicker('val',item);
	$(".prod_shift_a").val(prod_shiftA);
	$(".prod_shift_b").val(prod_shiftB);
	$(".total_prod").val(total_prod);
	$(".yard").val(yard);
	$(".thread_weight").val(thread_weight);
	$(".rate").val(rate);
	$(".thread_price").val(thread_price);
	$(".total_price").val(total_price);
	$(".stitch").val(stitch);
	$(".stitch_a").val(stitch_a);
	$(".stitch_b").val(stitch_b);
	$(".sale_rate").val(sale_rate);
	$(".sale").val(sale);
	$("div.party_code button").focus();
}
$.fn.calculateColumnTotals = function(){
	$(this).rowsValue("td.pda","td.fShiftA");
	$(this).rowsValue("td.pdb","td.fShiftB");
	$(this).rowsValue("td.tp","td.ftp");
	$(this).rowsValue("td.yr","td.fyard");
	$(this).rowsValue("td.thp","td.fPriceThread");
	$(this).rowsValue("td.thc","td.fThreadCost");
	$(this).rowsValue("td.sta","td.fstitchSha");
	$(this).rowsValue("td.stb","td.fstitchShb");
	$(this).rowsValue("td.sal","td.fsale");
}
$.fn.calculateIncomeLossTotals = function(){
	$(this).rowsValue("td.incomeLoss","td.incomeLossTotal");
}
$.fn.rowsValue = function(sumOfRows,insertToTotal){
	var sumAll = 0;
	$(sumOfRows).each(function(index, element) {
    sumAll += parseFloat($(this).text());
  });
	$(insertToTotal).text(sumAll.toFixed(2));
};
var input_row_calculations = function(){
	var pShiftA 	= parseInt($("input[name='prod_shiftA']").val()) || 0;
	var pShiftB 	= parseInt($("input[name='prod_shiftB']").val()) || 0;
	var totalProd = pShiftA + pShiftB || 0;
	$("input[name='total_prod']").val(totalProd);
	$("input[name='yard']").val(totalProd*15);
	var total_rate = $("input[name='thread_weight']").val()*$("input[name='rate']").val();
	$("input[name='thread_price']").val((total_rate).toFixed(2));
	$("input[name='total_price']").val(($("input[name='total_prod']").val()*total_rate).toFixed(2));
	var stitches  = parseInt($("input[name='stitch']").val())||0;
	var pShiftA   = parseInt($("input[name='prod_shiftA']").val()) || 0;
	var pShiftB   = parseInt($("input[name='prod_shiftB']").val()) || 0;
	var shift_aa = (parseInt($("input.stitch_shift_aa").val())||0);
	var shift_bb = (parseInt($("input.stitch_shift_bb").val())||0);
	var stitch_a = (pShiftA * stitches);
	var stitch_b = (pShiftB * stitches);
	if(shift_aa != 0 && shift_aa >= stitch_a){
		$("input[name='stitch_a']").val(stitch_a);
	}else{
		$("input[name='stitch_a']").val('');
	}
	if(shift_bb != 0 && shift_bb >= stitch_b){
		$("input[name='stitch_b']").val(stitch_b);
	}else{
		$("input[name='stitch_b']").val('');
	}
	var sale_rate  		= parseFloat($("input.sale_rate").val())||0;
	var total_stitch 	= stitch_a + stitch_b;
	$("input[name='sale']").val(( (parseInt(total_stitch*15*sale_rate/1000)||0) ).toFixed(2));
};
