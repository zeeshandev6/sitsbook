//var $ = $.noConflict(true);
$(document).ready(function(){
		$(".h3_php_error").click(function(){
			$(".h3_php_error").fadeOut('slow');
		});
		var fileSelf = $(".this_file_name").val();
		$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
		$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");

		$("#main-nav li ul").hide();
		$("#main-nav li a.current").parent().find("ul").show();

		$("#main-nav li a.nav-top-item").click(
			function () {
				$(this).parent().siblings().find("ul").slideUp("normal");
				$(this).next().slideToggle("normal");
			}
		);
		$("#main-nav li .nav-top-item").hover(
			function(){
				$(this).stop().animate({ paddingRight: "25px" }, 200);
			},
			function () {
				$(this).stop().animate({ paddingRight: "15px" });
			}
		);

		$(".content-box-header h3").css({ "cursor":"default" });
		$(".closed-box .content-box-content").hide();
		$(".closed-box .content-box-tabs").hide();

		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
		$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

		$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
			function() {
				$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
				$(this).addClass('current'); // Add class "current" to clicked tab
				var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
				$(currentTab).siblings().hide(); // Hide all content divs
				$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
				return false;
			}
		);

    //Close button:

		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);

    // Alternating table rows:

		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:

		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
			}
		);
		$.fn.numericOnly = function(){
			$(this).keydown(function(e){
				if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
				}else{
					if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
						e.preventDefault();
					}
				}
			});
		};
		$(".input_readonly").css({"cursor":"default"});
		$(".input_readonly").focus(function(e){
			$(this).blur();
		});

		$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				showAnim : 'show',
				changeMonth: true,
				changeYear: true,
				yearRange: '2000:+10'
			});
		$.fn.limitChars = function(Num){
			$(this).keydown(function(e){
				var thisVal = $(this).val();
				if(thisVal.length >= Num){
					if(e.keyCode == 8 || e.keyCode == 9 ){
						return true;
					}else{
						$(this).val(thisVal.substr(0,Num));
						e.preventDefault();
						return false;
					}
				}
			});
		};
		$.fn.setFocusTo = function(Elm){
			$(this).keydown(function(e){
				if(e.keyCode==13){
					if($(this).val()==""){
						e.preventDefault();
					}else{
						e.preventDefault();
						$(Elm).focus();
					}
				}
			});
		};
		$.fn.centerThisDiv = function(){
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$(this).height()/2;
			win_width = win_width-$(this).width()/2;
			$(this).css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
		};
});
var deleteRow = function(idValue,file,thisElement){
	var clickedDel = $(this);
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").centerThisDiv();
	$("#popUpDel").hide();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
		$.post(file,{id:idValue},function(data){
			if(data != ''){
				data = $.parseJSON(data);
				$(".dodelete").hide();
				$(".nodelete").text('Close');
				$("#popUpDel .confirm").html(data['MSG']);
				if(data['OK'] == 'Y'){
					$(thisElement).parent('td').parent('tr').hide();
				}
			}
		});
	});
	$(".nodelete").click(function(){
		$("#fade").fadeOut();
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
	$(".close_popup").click(function(){
		$("#fade").fadeOut('fast');
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
};

var deleteRowDouble = function(idValue,file,thisElement){
	var clickedDel = $(this);
	$("#fade").hide();
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>Are You Sure?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
	$("#popUpDel").centerThisDiv();
	$("#popUpDel").hide();
	$("#fade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".dodelete").click(function(){
			$.post(file,{id:idValue},function(data){
				if(data != ''){
					$(".dodelete").hide();
					$(".nodelete").text('Close');
					$("#popUpDel .confirm").html(data);
					$(thisElement).parent('td').parent('tr').hide();
					$('tr[data-id="'+idValue+'"]').remove();
				}
			});
	});
	$(".nodelete").click(function(){
		$("#fade").fadeOut();
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
	$(".close_popup").click(function(){
		$("#fade").fadeOut('fast');
		$("#popUpDel").fadeOut(function(){
			$("#popUpDel").remove();
		});
	});
};
var accCodeStatus = function(thisElm){
	var account_code = $(thisElm).attr('data-code');
	if($(thisElm).children('.fa.fa-square').length){
		$.post('db/accCodeStatus.php',{account_code:account_code,status:'Y'},function(data){
			if(data >= 1){
				$(thisElm).children('.fa.fa-square').removeClass('fa-square').addClass('fa-check-square');
				if(account_code.length == 6){
					$(thisElm).parent("td").parent('tr').nextAll('tr').each(function(index, element) {
                        if($(this).children('td').attr('make-active-code') == account_code){
							$(this).children('td').children('a').children('.fa.fa-square').removeClass('fa-square').addClass('fa-check-square');
						}
                    });;
				}
			}
		});
	};
	if($(thisElm).children('.fa.fa-check-square').length){
		$.post('db/accCodeStatus.php',{account_code:account_code,status:'N'},function(data){
			if(data >= 1){
				$(thisElm).children('.fa.fa-check-square').removeClass('fa-check-square').addClass('fa-square');
				if(account_code.length == 6){
					$(thisElm).parent("td").parent('tr').nextAll('tr').each(function(index, element) {
                        if($(this).children('td').attr('make-active-code') == account_code){
							$(this).children('td').children('a').children('.fa.fa-check-square').removeClass('fa-check-square').addClass('fa-square');
						}
                    });;
				}
			}
		});
	};
};
var accCodeStatusThirdLevelOnly = function(thisElm){
	var account_code = $(thisElm).attr('data-code');
	if($(thisElm).children('.fa.fa-square').length){
		$.post('db/accCodeSettings.php',{account_code:account_code,status:'Y'},function(data){
			if(data >= 1){
				$(thisElm).children('.fa.fa-square').removeClass('fa-square').addClass('fa-check-square');
			}
		});
	}
	if($(thisElm).children('.fa.fa-check-square').length){
		$.post('db/accCodeSettings.php',{account_code:account_code,status:'N'},function(data){
			if(data >= 1){
				$(thisElm).children('.fa.fa-check-square').removeClass('fa-check-square').addClass('fa-square');
			}
		});
	}
};

var centerThisDiv = function(elementSelector){
	var win_hi = $(window).height()/2;
	var win_width = $(window).width()/2;
	var elemHeight = $(elementSelector).height()/2;
	var elemWidth = $(elementSelector).width()/2;
	var posTop = win_hi-elemHeight;
	var posLeft = win_width-elemWidth;
	$(elementSelector).css({
		'position': 'fixed',
		'top': posTop,
		'left': posLeft,
		'margin': '0px'
	});
};
var displayMessage= function(message){
	$("#popUpDel").remove();
	$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
	centerThisDiv("#popUpDel");
	$("#popUpDel").hide();
	$("#xfade").fadeIn('slow');
	$("#popUpDel").fadeIn();
	$(".nodelete,#xfade").click(function(){
		$("#xfade").fadeOut();
		$("#popUpDel").remove();
	});
};

var miniEdit = function(thisElement){
	var $thatElement = $(thisElement).parent().find(".theTitleSpan");
	var title = $(thisElement).parent().find(".theTitleSpan").text();
	var acc_code_id = parseInt($(thisElement).attr('data-id'))||0;
	var $miniText = '<input type="text" class="miniText" value="'+title+'" data-id="'+acc_code_id+'" />';
	$(thisElement).parent().append($miniText);
	$(".miniText").focus();
	$(".miniText").blur(function(){
		miniTextBlurFunction($(this),$thatElement);
	});
	$(".miniText").keyup(function(e){
		if(e.keyCode == 13){
			miniTextBlurFunction($(this),$thatElement);
		}
	});
};
var miniTextBlurFunction = function(thisElm,thatElement){
	var newTitle = $(thisElm).val();
	var this_acc_code = $(thisElm).parent().parent().find('td.acc_code').text();
	var acc_code_id = parseInt($(thisElm).attr('data-id'))||0;
	var prevTitle = $(thatElement).text();
	if(newTitle == prevTitle){
		$(".miniText").remove();
		return false;
	}
	$("#xfade").fadeIn();
	$.post('db/saveAccTitle.php',{acc_id:acc_code_id,new_title:newTitle,acc_code:this_acc_code},function(data){
		if(data != ''){
			data = $.parseJSON(data);
			if(data['OK'] == 'Y'){
				displayMessage(newTitle+' Saved Successfully!');
				$(thatElement).text(newTitle);
			}else{
				displayMessage('Error! Cannot Save Account Title.');
			}
			$(".miniText").remove();
		}else{
			displayMessage('Error! Cannot Save Account Title.');
			$(".miniText").remove();
		}
	});
};
