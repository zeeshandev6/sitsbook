$(document).ready(function() {
    $(".h3_php_error").click(function(){
	$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});

	$(window).keyup(function(e){
		if(e.keyCode == 27){
			$(this).clearPanelValues();
			$("div.itemSelector button").focus();
			if($(".updateMode").length){
				$(".updateMode").removeClass('updateMode');
			}
		}
	});
  $.fn.populateBatchNumberList = function(){
    $(this).find("option").first().nextAll("option").remove();
    var $batchSelect = $(this);
    var item_id      = parseInt($("select.itemSelector option:selected").val())||0;
    $("tr.bill-rows").each(function(i,e){
      if($(this).find("td.itemName[data-item='"+item_id+"']").siblings("td.batch_no_td").text()==''){
        return;
      }
      $batchSelect.append('<option value="'+$(this).find("td.itemName[data-item='"+item_id+"']").siblings("td.batch_no_td").text()+'">'+$(this).find("td.itemName[data-item='"+item_id+"']").siblings("td.batch_no_td").text()+'</option>');
    });
    $batchSelect.selectpicker('refresh');
  };
  $.fn.populateExpiryDateList = function(){
    $(this).find("option").first().nextAll("option").remove();
    var $expirySelect = $(this);
    var item_id       = parseInt($("select.itemSelector option:selected").val())||0;
    var batch_no      = $("select.batch_no option:selected").val();
    $("tr.bill-rows").each(function(i,e){
      if($(this).find("td.itemName[data-item='"+item_id+"'][data-batch-no='"+batch_no+"']").siblings("td.expiry_date_td").text()==''){
        return;
      }
      $expirySelect.append('<option value="'+$(this).find("td.itemName[data-item='"+item_id+"'][data-batch-no='"+batch_no+"']").siblings("td.expiry_date_td").text()+'">'+$(this).find("td.itemName[data-item='"+item_id+"'][data-batch-no='"+batch_no+"']").siblings("td.expiry_date_td").text()+'</option>');
    });
    $expirySelect.selectpicker('refresh');
  };
	$("#main-nav li ul").hide();
	$("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	$("#main-nav li a.nav-top-item").click(
		function () {
			$(this).parent().siblings().find("ul").slideUp("normal");
			$(this).next().slideToggle("normal");
		}
	);
	var fileSelf = $(".this_file_name").val();
		$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
		$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");

	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);
	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	/*$(".content-box-header h3").click(
		function () {
		  $(this).parent().next().toggle();
		  $(this).parent().parent().toggleClass("closed-box");
		  $(this).parent().find(".content-box-tabs").toggle();
		}
	);*/

	// Content box tabs:

	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseInt($(this).text());
        });
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseFloat($(this).text())||0;
        });
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text(sumAll);
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger btn-sm'>Confirm</a><a class='nodelete btn btn-default btn-sm'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$.post(file, {id : idValue}, function(data){
			var supplierName = currentRow.children("td").first().next().next().text();
			if(data==1){
				$("#popUpDel .confirm").text(" "+supplierName+"Contains Information! Do You Really Want To Delete "+supplierName+"?.");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						if(data == 'L'){
							$("#popUpDel .confirm").text(" Stock is Issued To Sheds Cannot Delete This Record! ");
							$(".dodelete").hide();
							$(".nodelete").text('Close');
							$(".nodelete").click(function(){
								$("#popUpDel").slideUp();
								$("#xfade").fadeOut();
							});
						}else{
							currentRow.slideUp();
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut();
						}
					});
				});
			}else{
				$("#popUpDel .confirm").text("Are Sure you Want To Delete "+supplierName+"?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						currentRow.slideUp();
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				});
			}
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.deletePanel = function(file){
		var idValue = $(this).attr("do");
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger btn-sm'>Confirm</a><a class='nodelete btn btn-default btn-sm'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		$("#popUpDel").centerThisDiv();
		$.post(file, {id : idValue}, function(data){
			if(data==1){
				$("#popUpDel .confirm").text("Are you Sure You Want to Delete?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						if(data == 'L'){
							$("#popUpDel .confirm").text(" Not Enough Stock! ");
							$(".dodelete").hide();
							$(".nodelete").text('Close');
							$(".nodelete").click(function(){
								$("#popUpDel").slideUp();
								$("#xfade").fadeOut();
							});
						}else{
							$("#panel-"+idValue).slideUp(function(){
								$(this).remove();
							});
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut();
						}
					});
				});
			}else{
				$("#popUpDel .confirm").text("Are Sure you Want To Delete ?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						$("#panel-"+idValue).slideUp(function(){
							$(this).remove();
						});
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				});
			}
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = parseFloat($("input.taxRate").val())||0;
			var taxType = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
			$(this).calculateRowTotal();
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		if(item_id != ''){
			$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
				var itemPrice = data['P_PRICE'];
				$("input.unitPrice").val(itemPrice);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.getItemReturnDetails = function(){
		var item_id     = parseInt($("select.itemSelector option:selected").val())||0;
		var purchase_id = parseInt($("input.purchase_id").val())||0;
    var batch_no    = $("select.batch_no option:selected").val();
    var expiry_date = $("select.expiry_date option:selected").val();

		if(item_id>0&&purchase_id>0){
			$.post('db/getItemReturnDetails.php',{
          p_item_id:item_id,
          purchase_id:purchase_id,
          batch_no:batch_no,
          expiry_date:expiry_date},
        function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
        // var itemPrice = parseFloat($("tr.bill-rows td.batch_no_td:contains('"+batch_no+"')").siblings("td.expiry_date_td:contains('"+expiry_date+"')").siblings("td[data-unitprice-id="+item_id+"]").first().text())||0;
        $("input.unitPrice").val(data['P_PRICE']);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
				$(".qty_limit").val(data['LIMIT']);
				$(".qty_returned").val(data['RETURNED']);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id     = $("select.itemSelector option:selected").val();
		var item_name   = $("select.itemSelector option:selected").text();
    var batch_no    = ($("select.batch_no").length>0)?$("select.batch_no option:selected").val():"";
    var expiry_date = ($("select.batch_no").length>0)?$("select.expiry_date option:selected").val():"";
		var quantity    = parseInt($("input.quantity").val())||0;
		var unitPrice   = $("input.unitPrice").val();
		var discount    = $("input.discount").val();
		var subAmount   = $("input.subAmount").val();
		var taxRate     = parseFloat($("input.taxRate").val())||0;
		var taxAmount   = $("input.taxAmount").val();
		var totalAmount = $("input.totalAmount").val();
		$(this).blur();
		var updateMode = $(".updateMode").length;
		if(quantity == 0){
			return false;
		}
		if(taxRate == '' || taxRate == 0){
			taxAmount = 0;
		}
    if($("select.batch_no").length>0){
      var input_qty = 0;
      $("td.input_table_td.quantity[data-batch-no='"+batch_no+"'][data-expiry-date='"+expiry_date+"']").each(function(i,e){
         input_qty += parseFloat($(this).text())||0;
      });
      input_qty
    }
		if($("td[data-item-id='"+item_id+"']").length&&updateMode==0&&$("select.batch_no").length==0){
			displayMessage(item_name+" already included in bill!");
			return false;
		}
		if(item_id != '' &&item_name != '' &&quantity != '' &&unitPrice != '' &&subAmount != '' &&totalAmount != ''){
			if(updateMode == 0){
        var tr_row = '<tr class="alt-row calculations transactions" data-row-id="0">';
        tr_row    += '<td class="text-center itemName" data-item-id="'+item_id+'">'+item_name+'</td>';
        if($("select.batch_no").length>0){
          tr_row    += '<td class="text-center batch_no">'+batch_no+'</td>';
          tr_row    += '<td class="text-center expiry_date">'+expiry_date+'</td>';
        }
        tr_row    += '<td class="text-center input_table_td quantity" data-batch-no="'+batch_no+'" data-expiry-date="'+expiry_date+'">'+quantity+'</td>';
        tr_row    += '<td class="text-center unitPrice">'+unitPrice+'</td>';
        tr_row    += '<td class="text-center discountAmount">'+discount+'</td>';
        tr_row    += '<td class="text-center subAmount">'+subAmount+'</td>';
        tr_row    += '<td class="text-center taxRate">'+taxRate+'</td>';
        tr_row    += '<td class="text-center taxAmount">'+taxAmount+'</td>';
        tr_row    += '<td class="text-center totalAmount">'+totalAmount+'</td>';
        tr_row    += '<td class="text-center">- - -</td>';
        tr_row    += '<td class="text-center ">';
        tr_row    += '<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>';
        tr_row    += '<a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a>';
        tr_row    += '</td>';
        tr_row    += '</tr>';
        $(tr_row).insertAfter($("tr.calculations").last());
			}else if(updateMode == 1){
				$(".updateMode").find('td.itemName').text(item_name);
				$(".updateMode").find('td.itemName').attr('data-item-id',item_id);
        if($("select.batch_no").length>0){
        $(".updateMode").find('td.batch_no').text(batch_no);
        $(".updateMode").find('td.expiry_date').text(expiry_date);
        }
				$(".updateMode").find('td.quantity').text(quantity);
				$(".updateMode").find('td.unitPrice').text(unitPrice);
				$(".updateMode").find('td.discountAmount').text(discount);
				$(".updateMode").find('td.subAmount').text(subAmount);
				$(".updateMode").find('td.taxRate').text(taxRate);
				$(".updateMode").find('td.taxAmount').text(taxAmount);
				$(".updateMode").find('td.totalAmount').text(totalAmount);
				$(".updateMode").removeClass('updateMode');
			}
			$(this).clearPanelValues();
			$(this).calculateColumnTotals();
			$("div.itemSelector button").focus();
		}else{
			alert('Values Missing!');
		}
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
	$.fn.calculateRowTotal = function(){
		var taxRate = parseFloat($("input.taxRate").val())||0;
		var taxType = ($(".taxType").is(":checked"))?"I":"E";
		var thisVal = parseFloat($("input.quantity").val())||0;
		var thatVal = parseFloat($("input.unitPrice").val())||0;
		var amount = Math.round((thisVal*thatVal)*100)/100;
		var discount_type = $("input.discount_type").val();
		var discountAvail = parseFloat($("input.discount").val())||0;

		taxAmount = Math.round(taxAmount*100)/100;
		amount    = Math.round(amount*100)/100;
		discountAvail = Math.round(discountAvail*100)/100;

		if(discount_type=='R'){
			discountPerCentage = discountAvail;
		}else{
			discountPerCentage = amount*(discountAvail/100);
			discountPerCentage = Math.round(discountPerCentage*100)/100;
		}
		amount -= discountPerCentage;
		amount = Math.round(amount*100)/100;

		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount*(taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		-= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
            stockOfBill += parseInt($(this).text())||0;
        });
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$("select.itemSelector option").prop('selected',false);
		$("select.itemSelector").selectpicker('refresh');
		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.subAmount").val('');
		$("input.taxRate").val('');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
	$.fn.calculateColumnTotals = function(){
		$(this).sumColumn("td.quantity","td.qtyTotal");
		$(this).sumColumnFloat("td.subAmount","td.amountSub");
		$(this).sumColumnFloat("td.taxAmount","td.amountTax");
		$(this).sumColumnFloat("td.totalAmount","td.amountTotal");
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var editThisRow = function(thisElm){
		var thisRow 	   = $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	 = $("tr.updateMode").find('td.itemName').text();
		var item_id 	   = parseInt($("tr.updateMode").find('td.itemName').attr('data-item-id'))||0;
		var quantity 	   = $("tr.updateMode").find('td.quantity').text();
		var unitPrice 	 = $("tr.updateMode").find('td.unitPrice').text();
		var discount 	   = $("tr.updateMode").find('td.discountAmount').text();
		var subAmount 	 = $("tr.updateMode").find('td.subAmount').text();
		var taxRate 	   = parseFloat($("tr.updateMode").find('td.taxRate').text())||0;
		var taxAmount    = $("tr.updateMode").find('td.taxAmount').text();
		var totalAmount  = $("tr.updateMode").find('td.totalAmount').text();

		if(item_id == 0){
			return false;
		}
		$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
			data          = $.parseJSON(data);
			var itemStock = data['STOCK'];
			$("input.inStock").attr('thestock',itemStock).val(itemStock);
			$(".itemSelector option[value='"+item_id+"']").prop('selected',true);
			$(".itemSelector").selectpicker('refresh');
			$("input.quantity").val(itemStock);
			$("input.quantity").animate({'background-color':'rgba(102,255,102,0.7)'},300,function(){
				$("input.quantity").animate({'background-color':'#FFF'},300).keyup();
			});
			$("input.unitPrice").val(unitPrice);
			$("input.discount").val(discount);
			$("input.subAmount").val(subAmount);
			$("input.taxRate").val(taxRate);
			$("input.taxAmount").val(taxAmount);
			$("input.totalAmount").val(totalAmount);
			stockOs();
			$("input.quantity").focus();
		});
	};
	var stockOs = function(){
		var thisQty     = parseInt($("input.quantity").val())||0;
		var inStock     = parseInt($("input.inStock").attr('thestock'))||0;
		var limit       = parseInt($("input.qty_limit").val())||0;
		var item_id     = parseInt($("select.itemSelector option:selected").val());
    var batch_no    = $("select.batch_no option:selected").val();
		var item_title  = $("select.itemSelector option:selected").text();

		if(limit == 0){
			$("input.quantity").val(limit);
			$("input.inStock").val(inStock);
			return false;
		}
		if(limit < thisQty){
			$(".wel_made tr td[data-item='"+item_id+"']").next("td").animate({'background-color':'rgba(255,0,0,0.6)'},200,function(){
				$(".wel_made tr td[data-item='"+item_id+"']").next("td").animate({'background-color':'rgba(255,255,255,0.6)'},200);
			});
			$("input.quantity").val(limit);
			var NewStock =  inStock - limit;
			$("input.inStock").val(NewStock);
			return false;
		}
		var NewStock =  inStock - thisQty;
		if(NewStock < 0){
			$("input.quantity").val(0);
		}else{
			$("input.inStock").val(NewStock);
		}
	};
	var savePurchase = function(){
		var purchase_id		     = $(".purchase_id").val();
		var purchase_return_id = $(".purchase_return_id").val();
		var purchaseDate  	   = $(".datepicker").val();
		var billNum 		       = $("input.bill_number").val();
		var supplierCode 	     = $("select.supplierSelector option:selected").val();
		var supplier_name 	   = $(".supplier_name").val();
		var po_number 		     = $("input.po_number").val();
		if($("tr.transactions").length == 0){
			displayMessage('No Transactions Made!');
			return false;
		}
		$(".savePurchase").hide();
		var jSonString = {};
		$("tr.transactions").each(function(index, element){
      jSonString[index] = {};
			jSonString[index].row_id      = $(this).attr('data-row-id');
			jSonString[index].item_id 	  = parseInt($(this).find('td.itemName').attr('data-item-id'))||0;
      jSonString[index].batch_no 	  = ($(this).find('td.batch_no').length)?$(this).find('td.batch_no').text():"";
      jSonString[index].expiry_date = ($(this).find('td.expiry_date').length)?$(this).find('td.expiry_date').text():"";
			jSonString[index].quantity 	  = $(this).find('td.quantity').text();
			jSonString[index].unitPrice 	= $(this).find('td.unitPrice').text();
			jSonString[index].discount 	  = parseFloat($(this).find('td.discount').text())||0;
			jSonString[index].subAmount 	= $(this).find('td.subAmount').text();
			jSonString[index].taxRate 	  = $(this).find('td.taxRate').text();
			jSonString[index].taxAmount   = $(this).find('td.taxAmount').text();
			jSonString[index].totalAmount = $(this).find('td.totalAmount').text();
    });
		jSonString = JSON.stringify(jSonString);
		$.post("db/savePurchaseReturn.php",{return_type:'O',
									  purchase_return_id:purchase_return_id,
									  purchase_id:purchase_id,
									  purchaseDate:purchaseDate,
									  billNum:billNum,
									  po_number:po_number,
									  supplierCode:supplierCode,
									  supplier_name:supplier_name,
									  jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					window.location.reload();
				}else{
					displayMessage(data['MSG']);
				}
		});
	};
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};
	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
		});
	};
	var getBillsBySupplierCode = function(){
		var supplierCode = $("select.supplierSelector option:selected").val();
		if(supplierCode == ''){
			$("div.supplierSelector button").focus();
			return false;
		}
		$.post('db/getBillListBySupplerCode.php',{supplierCode:supplierCode},function(data){
			if(data != ''){
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").append(data);
				$("select.billSelector").selectpicker('refresh');
				$("div.billSelector button").focus();
			}else{
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").selectpicker('refresh');
				$("div.supplierSelector button").focus();
			}
		});
	};
	var display_alert = function(message){
		$(".message_div").text(message);
		$(".message_div").fadeIn(1000).delay(1000).fadeOut(1000);
	};
	var getBillDetails = function(){
		var supplier_code  = $("select.supplierSelector option:selected").val();
		var bill_number	   = parseInt($("select.billSelector option:selected").val())||0;
		var purchase_id    = $(".purchase_id").val();
		if(bill_number == 0 && supplier_code == ''){
			return false;
		}
		$.post('db/getPurchaseBill.php',{purchase_id:purchase_id,supplier_code:supplier_code,bill_num:bill_number},function(data){
			$("tr.calculations").first().nextAll(".calculations").remove();
			$(data).insertAfter($("tr.calculations").first());
			$(this).calculateColumnTotals();
			if(purchase_id == 0){
				$("tr.isRedRow").find('td').animate({'background-color':'rgba(255,0,0,0.2)'},300);;
			}
		});
	};
	var get_recent_purchase_returns = function($element){
		var purchase_id   = parseInt($(".purchase_id").val())||0;
    var show_batch_no = $("select.batch_no").length>0?"Y":"N";
		$.get('db/get-recent-inventory-returns.php',{purchase_id:purchase_id,show_batch_no:show_batch_no},function(data){
			$($element).html(data);
		});
	};
