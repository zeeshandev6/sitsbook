<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
  include('common/classes/j-voucher.php');

	//Permission
	if(!in_array('mobile-sales',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	= new ChartOfAccounts();
	$objCustomers			= new Customers();
	$objScanSale   	 	    = new ScanSale();
	$objScanSaleDetails     = new ScanSaleDetails();
	$objItems            	= new Items();
	$objItemCategory        = new itemCategory();
	$objDepartments         = new Departments();
  $objPagination          = new Pagination();
  $objConfigs             = new Configs();
  $objJournalVoucher      = new JournalVoucher();

  if(isset($_POST['status_voucher'])){
      $id          			 = (int)(mysql_real_escape_string($_POST['status_voucher']));
      $status      			 = (mysql_real_escape_string($_POST['status']));
      $bill_number 			 = $objScanSale->getBillNumber($id);
      $voucher_id  			 = $objScanSale->getVoucherId($id);
      $status_voucher_id = $objScanSale->getStatusVoucher($id);

      if($status == 'N'){
          $objJournalVoucher->reverseVoucherDetails($status_voucher_id);
          $objJournalVoucher->deleteJv($status_voucher_id);
          $objScanSale->insertStatusVoucher($id,0);
          $objScanSale->setCollectionStatus($id,$status);
      }else{
          if($voucher_id > 0){
              $voucher_detail_row = $objJournalVoucher->getSaleInvoiceCustomerAmount($voucher_id);

              $objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
              $objJournalVoucher->voucherDate = date('Y-m-d');
              $objJournalVoucher->voucherType = 'SV';

              if($status_voucher_id>0){
                  $objJournalVoucher->reverseVoucherDetails($status_voucher_id);
                  $objJournalVoucher->updateVoucherDate($status_voucher_id);
              }else{
                  $status_voucher_id              = $objJournalVoucher->saveVoucher();
              }

              if($status_voucher_id>0){
                  $objJournalVoucher->voucherId       = $status_voucher_id;

                  $customer_name  = $objAccountCodes->getAccountTitleByCode($voucher_detail_row['ACCOUNT_CODE']);

                  $objJournalVoucher->accountCode     = '0101010001';
                  $objJournalVoucher->accountTitle    = $objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode);
                  $objJournalVoucher->narration       = "Cash collection from ".$customer_name." against invoice # ".$bill_number;
                  $objJournalVoucher->transactionType = 'Dr';
                  $objJournalVoucher->amount          = $voucher_detail_row['AMOUNT'];

                  $objJournalVoucher->saveVoucherDetail();

                  $objJournalVoucher->accountCode     = $voucher_detail_row['ACCOUNT_CODE'];
                  $objJournalVoucher->accountTitle    = $customer_name;
                  $objJournalVoucher->narration       = "Cash collection against invoice # ".$bill_number;
                  $objJournalVoucher->transactionType = 'Cr';

                  $objJournalVoucher->saveVoucherDetail();
                  $objScanSale->insertStatusVoucher($id,$status_voucher_id);
                  $objScanSale->setCollectionStatus($id,$status);
              }
          }
      }
      exit();
  }

  $total = $objPagination->getPerPage();
  $start = 0;

  $suppliersList = $objCustomers->getlist();
  $cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');

  if(isset($_GET['search'])){
      $objScanSale->fromDate      = ($_GET['from_date'] == '')?"":date('Y-m-d',strtotime($_GET['from_date']));
      $objScanSale->toDate        = ($_GET['to_date'] == '')?"":date('Y-m-d',strtotime($_GET['to_date']));
      $objScanSale->cust_acc_code = mysql_real_escape_string($_GET['supplierAccCode']);
      $objScanSale->bill_no       = mysql_real_escape_string($_GET['billNum']);
      $objScanSale->customer_name = mysql_real_escape_string($_GET['supplier_info']);
      $objScanSale->imei          = mysql_real_escape_string($_GET['imei']);
      $objScanSale->collected     = mysql_real_escape_string($_GET['collected']);
  }
  if(isset($_GET['page'])){
      $this_page = $_GET['page'];
      if($this_page>=1){
          $this_page--;
          $start = $this_page * $total;
      }
  }else{
      $start = 0;
      $this_page = 0;
  }

  $list = $objScanSale->search($start, $total);
  $found_records = $objScanSale->found_records;

  $invoice_type= $objConfigs->get_config("SCAN_INVOICE_FORMAT");

  if($invoice_type == 'L_1'){
      $invoice_url = 'mobile-sale-invoice.php?';
  }
  if($invoice_type == 'S_1'){
      $invoice_url = 'mobile-sales-invoice-small.php?';
  }
  if($invoice_type == 'M_1'){
      $invoice_url = 'mobile-sales-invoice-duplicates.php?';
  }
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Scan Sale</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        table td{
            padding: 7px !important;
        }
    </style>

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/scan.sale.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$('select').selectpicker();
    		$("input[name='billNum']").numericOnly();
			$("#datepicker").setFocusTo("input[name='billNum']");
			$("input[name='billNum']").setFocusTo("div.supplierSelector button");
			$("input[name='gpNum']").focus();
			makeItCash(".trasactionType");
			$(".pointer").click(function(){
				$(this).deleteRowDefault('db/deleteScanSale.php');
			});
            $("select[name=collection_status]").change(function(){
                var status = $(this).find("option:selected").val();
                var id     = $(this).attr('data-id');
								var elm    = $(this).parent().parent('td').next('td').find('.pointer');
                $.post("mobile-sale.php",{status_voucher:id,status:status},function(data){
									if(status=='Y'){
										$(elm).hide();
									}else{
										$(elm).show();
									}
                });
            });
            $("input[name='imei']").change(function(){
            	return;
                var imei = $(this).val();
                if(imei == ''){
                    return;
                }
                $("#xfade").fadeIn();
                $.get('mobile-sale.php',{imei:imei},function(data){
                    $("#xfade").fadeOut();
                    data = parseInt(data)||0;
                    if(data == 0){
                        return false;
                    }
                    window.location.href = 'mobile-sale-details.php?id='+data;
                });
            });
            <?php
                if((isset($_GET['tab'])&&$_GET['tab']=='search')){
                    ?>tab('2', '1', '2');<?php
                }
            ?>
    	});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sales Panel - <?php echo $found_records; ?> Records </p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="mobile-sale-details.php"><div class="tab">Details</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1" style="display:block;">
                    <table width="100%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th class="text-center col-xs-1" >Bill#</th>
                               <th class="text-center col-xs-2" >Sale Date</th>
                               <th class="text-center col-xs-5" >Customer Title</th>
                               <th class="text-center col-xs-2" >Status</th>
                               <th class="text-center col-xs-2" >Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
            if(mysql_num_rows($list)){
                while($purchaseRow = mysql_fetch_array($list)){
                    $supplierTitle =     $objAccountCodes->getAccountTitleByCode($purchaseRow['CUST_ACC_CODE']);
?>
                            <tr>
                                <td style="text-align:center"><?php echo $purchaseRow['BILL_NO']; ?></td>
                                <td style="text-align:center"><?php echo date('d-m-Y',strtotime($purchaseRow['SALE_DATE'])); ?></td>
                                <td style="text-align:left"><?php echo $supplierTitle; ?></td>
                                <td class="text-center">
                                    <select name="collection_status" data-id="<?php echo $purchaseRow['ID']; ?>" <?php echo (substr($purchaseRow['CUST_ACC_CODE'],0,6) != '010104')?"disabled":""; ?> >
                                        <option value="Y" <?php echo ($purchaseRow['COLLECT_STATUS']=='Y'||(substr($purchaseRow['CUST_ACC_CODE'],0,6) != '010104'))?"selected":""; ?> >Collected</option>
                                        <option value="N" <?php echo ($purchaseRow['COLLECT_STATUS']=='N'&&(substr($purchaseRow['CUST_ACC_CODE'],0,6) == '010104'))?"selected":""; ?>>Uncollected</option>
                                    </select>
                                </td>
                                <td style="text-align:center">
                                    <a id="view_button" href="mobile-sale-details.php?id=<?php echo $purchaseRow['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a id="view_button" target="_blank" href="<?php echo $invoice_url; ?>&id=<?php echo $purchaseRow['ID']; ?>" title="Print"><i class="fa fa-print"></i></a>
                                    <a id="view_button" target="_blank" href="voucher-print.php?id=<?php echo $purchaseRow['VOUCHER_ID']; ?>" title="Print"><i class="fa fa-print"></i> JV</a>
                                    <?php if(in_array('delete-mobile-sales',$permissionz) || $admin){ ?>
                                    <a do="<?php echo $purchaseRow['ID']; ?>" style="display:<?php echo ($purchaseRow['COLLECT_STATUS']=='Y')?"none":""; ?>;" class="pointer" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a>
                                    <?php } ?>
                                    <a class="button-orange" href="mobile-sale-return-details.php?id=<?php echo $purchaseRow['ID']; ?>" data-toggle="tooltip" title="Return" ><i class="fa fa-reply"></i></a>
                                </td>
                            </tr>
<?php
                }
            }elseif(isset($_GET['search'])){
?>
                            <tr>
                                <th colspan="100" style="text-align:center;">
                                    <?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
                                </th>
                            </tr>
<?php
            }
?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
<?php
                    if(!isset($_GET['search'])){
?>
                            <nav>
                              <ul class="pagination">
<?php
                        $count = $found_records;
                        $total_pages = ceil($count/$total);
                        $i = 1;
                        $thisFileName = $_SERVER['PHP_SELF'];
                        if(isset($this_page) && $this_page>0){
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=1">First</a>
                            </li>
<?php
                        }
                        if(isset($this_page) && $this_page>=1){
                            $prev = $this_page;
?>
                            <li>
                                <a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
                            </li>
<?php
                        }
                        $this_page_act = $this_page;
                        $this_page_act++;
                        while($total_pages>=$i){
                            $left = $this_page_act-5;
                            $right = $this_page_act+5;
                            if($left<=$i && $i<=$right){
                            $current_page = ($i == $this_page_act)?"active":"";
?>
                                <li class="<?php echo $current_page; ?>">
                                    <?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                </li>
<?php
                            }
                            $i++;
                        }
                        $this_page++;
                        if(isset($this_page) && $this_page<$total_pages){
                            $next = $this_page;
                            echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
                        }
                        if(isset($this_page) && $this_page<$total_pages){
?>
                                <li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
                            </ul>
                            </nav>
<?php
                        }
                    }
?>
                    </div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
                <div id="bodyTab2" style="display:none;" >
                    <div id="form">
                    <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

                        <div class="caption">From Date</div>
                        <div class="field">
                            <input type="text" name="from_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">To Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo date('d-m-Y'); ?>" name="to_date" class="form-control datepicker" style="width: 150px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Customer</div>
                        <div class="field" style="width:305px;">
                            <select class="selectpicker form-control"
                                    name='supplierAccCode'
                                    data-style="btn-default"
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($cashAccounts)){
                            while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
                                $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$cash_ina_hand['ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
<?php
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                                $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Bill No</div>
                        <div class="field" style="width:305px;">
                            <input type="text" value="" name="billNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Supplier Info</div>
                        <div class="field" style="width:305px;">
                            <input type="text" value="" name="supplier_info" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Collection Status : </div>
                        <div class="field">
                            <input type="radio" name="collected" value="" id="collect1" class="css-checkbox" checked />
                            <label for="collect1" class="css-label-radio">Both</label>
                            <input type="radio" name="collected" value="Y" id="collect2" class="css-checkbox" />
                            <label for="collect2" class="css-label-radio">Collected</label>
                            <input type="radio" name="collected" value="N" id="collect3" class="css-checkbox" />
                            <label for="collect3" class="css-label-radio">Uncollected</label>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Barcode  </div>
                        <div class="field">
                            <input type="text" value="" name="imei" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
