<?php
	include('common/classes/dashboard.php');
	include('common/classes/items.php');
	include('common/classes/accounts.php');
	include('common/classes/cash_management.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/godown-details.php');
	include('common/classes/j-voucher.php');
	include('common/classes/events.php');
	include('common/classes/tasking.php');
	include('common/classes/tasking-docs.php');

	$objDashboard             = new Dashboard();
	$objItems                 = new Items();
	$objAccounts              = new ChartOfAccounts();
	$objJournalVoucher        = new JournalVoucher();
	$objUserAccounts          = new UserAccounts();
	$objGodownDetails         = new GodownDetails();
	$objCashManagement        = new CashManagement();
	$objMobilePurchaseDetails = new ScanPurchaseDetails();
	$objTasking               = new Tasking();
	$objTaskingDocs           = new TaskingDocs();
	$objEvents                = new Events();
	$objConfigs               = new Configs();

	$userTasks = $objTasking->getUserTasks($user_id);


	$current_designation   = $sessionUserDetails['DESIGNATION_TYPE'];

	$user_designation_type = array();
	$user_designation_type['M'] = 'Order Manager';
	$user_designation_type['O'] = 'Order Taker';
	$user_designation_type['K'] = 'Kitchen';
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-table.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/dashboard.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery.dataTables.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/moltran-core.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/moltran-components.css" type="text/css" media="screen" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.mobile.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-table.js"></script>
    <script type="text/javascript" src="resource/scripts/highcharts.js"></script>
    <script type="text/javascript" src="resource/scripts/highcharttables.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="resource/scripts/chart.js"></script>
		<script type="text/javascript" src="resource/scripts/food.point.config.js"></script>
    <script type="text/javascript">
        $(function(){
            $("select").selectpicker();
        });
        var taskMarkToggle = function(elm){
            var task_id = $(elm).attr("data-task-id");
            var status  = "C";
            if($(elm).hasClass("btn-info")){
                status  = "P";
            }
            $.post("tasking-management.php",{doc_id:task_id,status:status},function(data){
                if(status == 'P'){
                    $(elm).removeClass("btn-info");
                    $(elm).addClass("btn-default");
                }
                if(status == 'C'){
                    $(elm).addClass("btn-info");
                    $(elm).removeClass("btn-default");
                }
            });
        }
    </script>
</head>
<body style="margin-bottom: 0px !important;">
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div style="height:70px;"></div>
<?php
	 //Permission
	if(in_array('dashboard',$permissionz) && $turn_off_operational_forms == 'N'){
?>
	<div class="content-box-top bg-transparent border-none">
		<div class="all_content">
		<div class="col-sm-12">
			<div class="page-title bx-shadow bg-white">
				Dashboard
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="content-box-content border-none">
			<div class="col-sm-6 col-lg-8">
					<div class="mini-stat clearfix bx-shadow bg-info">
							<?php /*<span class="mini-stat-icon profile"><img src="resource/images/user_male.png" alt="user-img" class="thumb-md img-circle invert"  /></span>*/ ?>
							<span class="mini-stat-icon"><i class="fa fa-user text-white"></i></span>
							<div class="mini-stat-info text-right text-white">
									<span class="counter text-white total-orders"><?php echo $usersFullName ?></span>
									<em class="text-white"><?php echo isset($user_designation_type[$current_designation])?$user_designation_type[$current_designation]:"Other User"; ?></em>
							</div>
					</div>
			</div>
			<?php
				if($foodPointMgmtAddon == 'Y'){
			?>
			<div class="col-sm-6 col-lg-4">
					<div class="mini-stat clearfix bx-shadow bg-white btn btn-block btn-default" data-toggle="modal" data-target="#new-order-form">
							<span class="mini-stat-icon bg-white"><i class="fa fa-plus text-white"></i></span>
							<div class="mini-stat-info text-right text-dark">
									<span class="counter text-dark total-orders">Take Order</span>
									Form
							</div>
					</div>
			</div>
			<?php
				}
			?>
			<div class="clear"></div>
<?php
					if($foodPointMgmtAddon == 'Y'){
?>
						<div class="col-xs-12">
							<div class="col-sm-6 col-lg-3">
                  <div class="mini-stat clearfix bx-shadow bg-white btn btn-block" data-toggle="modal" data-target="#orders-received">
                      <span class="mini-stat-icon bg-info"><i class="fa fa-book"></i></span>
                      <div class="mini-stat-info text-right text-dark">
                          <span class="counter text-dark total-orders">15852</span>
                          Orders Received
                      </div>
                  </div>
              </div>
							<div class="col-sm-6 col-lg-3">
                  <div class="mini-stat clearfix bx-shadow bg-white btn btn-block">
                      <span class="mini-stat-icon bg-primary"><i class="fa fa-archive"></i></span>
                      <div class="mini-stat-info text-right text-dark">
                          <span class="counter text-dark total-orders">15852</span>
                          Orders Ready
                      </div>
                  </div>
              </div>
							<div class="col-sm-6 col-lg-3">
                  <div class="mini-stat clearfix bx-shadow bg-white btn btn-block">
                      <span class="mini-stat-icon bg-purple"><i class="fa fa-cutlery"></i></span>
                      <div class="mini-stat-info text-right text-dark">
                          <span class="counter text-dark total-orders">15852</span>
                          Orders Served
                      </div>
                  </div>
              </div>
							<div class="col-sm-6 col-lg-3">
                  <div class="mini-stat clearfix bx-shadow bg-white btn btn-block">
                      <span class="mini-stat-icon bg-pink"><i class="fa fa-dollar"></i></span>
                      <div class="mini-stat-info text-right text-dark">
                          <span class="counter text-dark total-orders">15852</span>
                          Total Orders
                      </div>
                  </div>
              </div>
	            <div class="clear"></div>
						</div>
<?php
					}
?>
<?php
					if($taskingAddon == 'Y'){
?>
            <div class="col-md-6">
                <div class="panel panel-default" id="taskingContainer">
                    <div class="panel-heading">Tasks for today <span class="pull-right mr-10"><?php echo date("d-m-Y"); ?></span> </div>
                    <div class="panel-body">
                        <div id="events">
                            <?php
                                if(mysql_num_rows($userTasks)){
                                    while($taskDetails = mysql_fetch_assoc($userTasks)){
                                        $documents = $objTaskingDocs->getList($taskDetails['ID']);
                            ?>
                                <div class="event col-xs-12" data-row="">
                                    <p class="pull-left col-xs-12">
                                        <span><?php echo $taskDetails['SUBJECT'] ?></span>
                                        <button class="btn btn-<?php echo $taskDetails['TASK_STATUS']=='P'?"default":"info"; ?> btn-sm pull-right ml-5" onclick="taskMarkToggle(this);" data-task-id="<?php echo $taskDetails['ID'] ?>" > <i class="fa fa-check"></i> </button>
                                        <small class="pull-right mr-10 is_time"><?php echo date("d-m-Y H:i A",strtotime($taskDetails['TASK_DATE'])) ?></small>
                                    </p>
                                    <div class="clear"></div>
                                    <small class="col-xs-12 is_desc"><?php echo $taskDetails['DESCRIPTION'] ?></small>
                                    <div class="clear"></div>
<?php
                                if(isset($documents) && mysql_num_rows($documents)){
?>
                                <hr />
                                <ul class="list-group">
<?php
                                    while($document = mysql_fetch_assoc($documents)){
                                        $icondir  = "resource/images/mime-types/";
                                        $iconfile = $icondir.strtolower(pathinfo($document['FILE_NAME'],PATHINFO_EXTENSION)).".png";
?>
                                    <li class="list-group-item">
                                        <div class="col-xs-2">
                                            <img src="<?php echo $iconfile; ?>" style="height:25px;" />
                                        </div>
                                        <div class="col-xs-8">
                                            <?php echo $document['TITLE']; ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <a href="<?php echo "uploads/docs/".date('d-m-Y',strtotime($taskDetails['TASK_DATE']))."/".$document['FILE_NAME']; ?>" download="<?php echo $document['TITLE']; ?>">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </li>
<?php
                                    }
?>
                                </ul>
                            <div class="clear"></div>
<?php
                                }
?>
                                </div>
                            <?php
                                    }
                                }else{
                            ?>
                                <div class="event col-xs-12 text-center">
                                    <div class="alert alert-info mb-0">
                                        <p>No Tasks Available!</p>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
<?php
					}
?>
            <div class="clear"></div>
        </div><!--content-box-content-->
		</div><!--all_content-->
	</div>
  <div class="clear"></div>
	<?php
			if($foodPointMgmtAddon == 'Y'){
	?>
	<div id="orders-received" class="modal fade" role="dialog">
		<div class="modal-dialog modal-xlg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-book text-dark"></i> Current Orders - <?php echo date("d-m-Y");  ?>
						<button type="button" class="btn btn-default btn-sm pull-right" data-dismiss="modal"> <i class="fa fa-times"></i> </button>
					</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="text-center col-xs-1">Order #</th>
								<th class="text-center col-xs-2">Rec.Time</th>
								<th class="text-center col-xs-3">Customer Name</th>
								<th class="text-center col-xs-1">Table #</th>
								<th class="text-center col-xs-2">Amount</th>
								<th class="text-center col-xs-1">Status</th>
							</tr>
						</thead>
						<tbody class="orders-received-list">
						<?php
							for($i=0; $i < 18; $i++){
						 ?>
							<tr>
								<td class="text-center"><?php echo $i; ?></td>
								<td class="text-center"><?php echo date('h:i A'); ?></td>
								<td class="text-left customer_name">Customer <?php echo $i; ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center">

								</td>
							</tr>
						<?php
							}
						?>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="$('#myModal form').submit();"> <i class="fa fa-check"></i>	 </button>
					<button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
				</div>
			</div>
		</div>
	</div><!--orders-received modal-->


	<div id="new-order-form" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-plus text-dark"></i> Take Order
						<button type="button" class="btn btn-default btn-sm pull-right" data-dismiss="modal"> <i class="fa fa-times"></i> </button>
					</h4>
				</div>
				<div class="modal-body">
					<form method="post" action="<?php echo $fileName; ?>">
						<div class="form-group col-xs-6">
							<label for="order-time">Order Time:</label>
							<input type="text" class="form-control" id="order-time" value="<?php echo date("d-m-Y h:i A"); ?>" readonly>
						</div>
						<div class="form-group col-xs-6">
							<label for="table_id_select">Table # : </label>
							<select class="form-control show-tick"  name="table_id" id="table_id_select">
								<option value=""></option>
								<option value="1">Table # 1</option>
								<option value="2">Table # 2</option>
								<option value="3">Table # 3</option>
							</select>
						</div>
						<div class="clear"></div>

						<div class="form-group col-xs-12">
							<label for="customer-name">Customer Name:</label>
							<input type="text" class="form-control" id="customer-name" value="">
						</div>
						<div class="clear"></div>

						<div class="col-xs-12 deals-input-row">
							<table>
								<thead>
									<th class="text-center col-xs-6">Deals</th>
									<th class="text-center col-xs-4">Quantity</th>
									<th class="text-center col-xs-2">Action</th>
								</thead>
								<tbody>
									<tr class="deals-quick-insert">
										<td class="deals_selector_td">
											<select class="form-control show-tick" name="deals_id" id="deals_id_select">
												<option value=""></option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
												<option value="1">Midnight Deal 1</option>
												<option value="2">Midnight Deal 2</option>
												<option value="3">Midnight Deal 3</option>
											</select>
										</td>
										<td>
											<input type="number" class="form-control" name="deal_qty" id="deal_qty" value="" />
										</td>
										<td>
											<button type="button"  class="btn btn-default btn-block" onclick="enter_deal_row(this)" name="deal_enter" id="deal_enter" value=""><i class="fa fa-check"></i></button>
										</td>
									</tr>
								</tbody>
								<tbody class="deals-trows">

								</tbody>
							</table>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</form>
					<div class="clear"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pull-left" onclick="saveOrder();">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
				</div>
			</div>

		</div>
	</div>

	<?php
} // Food Point Management
	?>
<?php
	} //dashboard permission
?>
</body>
</html>
<?php include('conn.close.php'); ?>
