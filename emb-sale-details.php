<?php
  include('common/connection.php');
  include('common/config.php');
  include('common/classes/emb_sale.php');
  include('common/classes/emb_sale_details.php');
  include('common/classes/customers.php');
  include('common/classes/banks.php');
  include('common/classes/machines.php');
  include('common/classes/services.php');
  include('common/classes/itemCategory.php');
  include('common/classes/tax-rates.php');
  include('common/settings/captions.php');

  //Permission
  if(!in_array('sales',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

  $objSale         			 = new EmbroiderySale();
  $objSaleDetails  			 = new EmbroiderySaleDetails();
  $objMachines        	 = new Machines();
  $objItemCategory 			 = new itemCategory();
  $objServices     			 = new Services();
  $objCustomers    			 = new Customers();
  $objBanks        			 = new Banks();
  $objConfigs      			 = new Configs();

  $user_name          = $objConfigs->get_config("USER_NAME");
  $sale_subject       = $objConfigs->get_config('SALE_SUBJECT');

  $customersList       = $objCustomers->getList();
  $newBillNum          = $objSale->genBillNumber();

  $invoice_format      = $objConfigs->get_config('INVOICE_FORMAT');
  $print_meth          = $objConfigs->get_config('DIRECT_PRINT');
  $invoice_format      = explode('_', $invoice_format);
  $invoiceSize         = $invoice_format[0]; // S  L

  if($invoiceSize == 'L'){
    $invoiceFile = 'emb-sales-invoice.php';
  }elseif($invoiceSize == 'M'){
    $invoiceFile = 'emb-sales-invoice-duplicates.php';
  }elseif($invoiceSize == 'S'){
    $invoiceFile = 'emb-sales-invoice-small.php';
  }
  $scanner_toggle = $objConfigs->get_config('SALE_SCANNER');

  $sale_id   = 0;
  $embSaleDetails = NULL;
  $saleDetailList = NULL;

  if(isset($_GET['id'])){
  $sale_id = (int)mysql_real_escape_string($_GET['id']);
  if($sale_id != '' && $sale_id != 0){
    $embSaleDetails  = $objSale->getDetail($sale_id);
    $saleDetailList  = $objSaleDetails->getList($sale_id);
  }else{
    $sale_id      = 0;
  }
  }
?>
<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice # <?php echo $embSaleDetails==NULL?$newBillNum:$embSaleDetails['BILL_NO']; ?></title>
<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css" media="screen" />
<style>
  .caption{
  	padding: 5px !important;
  }
  td.itemName{
  	padding-left: 10px !important;
  }
  td.quantity{
      position: relative;
      cursor: pointer;
  }
  td,th{
      padding: 10px 5px !important;
      border:1px solid #CCC !important;
  }
  input.error{
      background-color: rgba(255,0,0,0.1);
  }
</style>
<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="resource/scripts/configuration.js"></script>
<script type="text/javascript" src="resource/scripts/emb.sale.config.js"></script>
<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
<script type="text/javascript" src="resource/scripts/tab.js"></script>
<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
</head>
<body class="body_menu_close">
<div id="body-wrapper">
  <div id="sidebar">
      <input type="hidden" class="print_method" value="<?php echo $print_meth; ?>" />
      <?php include("common/left_menu.php"); ?>
  </div> <!-- End #sidebar -->
  <div class="content-box-top" >
      <div class="content-box-header">
          <p>Sale Management</p>
          <span id="tabPanel">
              <div class="tabPanel">
<?php
			if(isset($_GET['page'])){
				$page = "&page=".$_GET['page'];
			}else{
				$page = '';
			}
?>
                  <a href="emb-sale.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                  <a href="emb-sale.php?tab=search"><div class="tab">Search</div></a>
                  <div class="tabSelected">Details</div>
              </div>
          </span>
          <div class="clear"></div>
      </div> <!-- End .content-box-header -->
      <div class="content-box-content" style="padding: 5px;">
          <div id="bodyTab1">
              <div id="form" style="margin: 20px auto;" class="col-xs-12 col-md-10">

                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Sale Date</div>
                        <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                            <input type="text" name="sale_date" id="datepicker" value="<?php echo ($embSaleDetails != NULL)?date("d-m-Y",strtotime($embSaleDetails['SALE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker sale_date" style="width:150px" />
                        </div>
                        <div class="clear"></div>
                      </div>

                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Bill#</div>
                        <div class="field" style="width:100px">
                            <input type="text" name="billNum" value="<?php echo ($embSaleDetails != NULL && !isset($_GET['c']))?$embSaleDetails['BILL_NO']:$newBillNum; ?>" class="form-control" style="width:100px" />
                        </div>
                        <div class="clear"></div>
                      </div>

                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <div class="caption" style="width:80px">Account</div>
                        <div class="field" style="width:270px;position:relative;">
                            <a href="#" onclick="add_supplier();" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                            <select class="account_code form-control show-tick"
                                    data-style="btn-default" data-width="230"
                                    data-live-search="true" data-hide-disabled='true' style="border:none;" >
                               <option selected value=""></option>
  <?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
                                $selected = ($embSaleDetails['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
  ?>
                               <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
  <?php
                            }
                        }
                        if(mysql_num_rows($cash_in_hand_list)){
                            while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                                $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                                $selected = '';
                                if($embSaleDetails != NULL){
                                    $selected = ($embSaleDetails['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
                                }
  ?>
                                <option  <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
  <?php
                            }
                        }
  ?>
                            </select>
  				              <input type="hidden" class="sale_id" value="<?php echo (isset($_GET['c']))?0:$sale_id; ?>" />
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Lot No</div>
                        <div class="field"   style="width:100px;margin-left:0px;padding: 0px;">
                            <input type="text" name="lot_no" value="<?php echo $embSaleDetails['LOT_NO']; ?>" class="form-control" />
                        </div>
                      </div>
                      <div class="clear"></div>

                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <?php if($sale_subject == 'Y'){ ?>
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Narration</div>
                        <div class="field"   style="width:150px;margin-left:0px;padding: 0px;">
                            <input type="text" name="subject" value="<?php echo $embSaleDetails['SUBJECT']; ?>" class="form-control" style="width:350px" />
                        </div>
                        <?php } ?>
                      </div>
                      <div class="col-xs-12 col-md-6 col-xs-6">
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Gate Pass</div>
                        <div class="field"   style="width:100px;margin-left:0px;padding: 0px;">
                            <input type="text" name="gp_no" value="<?php echo $embSaleDetails['GP_NO']; ?>" class="form-control" />
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>

                      <table class="table">
                        <thead>
                            <tr>
                               <th class="text-left col-xs-3">
                                    Machine
                                    <a href="machine-details.php" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus"></i></a>
                                    <a href="#" class="reload_item btn btn-default btn-xs pull-right"><i class="fa fa-refresh"></i></a>
                               </th>
                               <th class="text-center col-xs-1">Design Number</th>
                               <th class="text-center col-xs-1">Stitches</th>
                               <th class="text-center col-xs-1">Rate</th>
                               <th class="text-center col-xs-1">Amount</th>
                               <th class="text-center col-xs-1">Qty/Length</th>
                               <th class="text-center col-xs-1">Total</th>
                               <th class="text-center col-xs-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="quickSubmit" style="background:none">
                                <td>
                                    <select class="machine_id show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                                       <option selected value=""></option>
                                       <?php
                                       $machine_list = $objMachines->getList();
                                       $machine_arry = array();
                                       if(mysql_num_rows($machine_list)){
                                         while($machine = mysql_fetch_assoc($machine_list)){
                                           $machine_arry[$machine['ID']] = $machine;
                                           ?>
                                           <option value="<?php echo $machine['ID']; ?>" data-subtext="<?php echo $machine['RATE']; ?>"><?php echo $machine['NAME']; ?></option>
                                           <?php
                                         }
                                       }
                                       ?>
                                    </select>
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center design_no" value="">
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center stitches" value="">
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center rate" value="">
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center amount" value="" />
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center qty_length" value="" />
                                </td>
                                <td>
                                  <input type="text" class="form-control text-center final_amount" value="" readonly />
                                </td>
                                <td class="text-center">
                                  <input type="button" class="insert_button" value="Enter" id="clear_filter" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody class="transactions_list">
<?php
                        if($saleDetailList!=NULL&&mysql_num_rows($saleDetailList)){
                          while($sale_detail = mysql_fetch_assoc($saleDetailList)){
?>
                            <tr class="alt-row transactions" data-row-id="<?php echo $sale_detail['ID']; ?>">
                              <td style="text-align:center;" class="machine_id" data-rate="<?php echo $sale_detail['MACHINE_RATE']; ?>" data-machine-id="<?php echo $sale_detail['MACHINE_ID']; ?>"><?php echo $machine_arry[$sale_detail['MACHINE_ID']]['NAME']; ?></td>
                              <td class="text-center design_no"><?php echo $sale_detail['DESIGN_NO']; ?></td>
                              <td class="text-center stitches"><?php echo $sale_detail['STITCHES']; ?></td>
                              <td class="text-center rate"><?php echo $sale_detail['UNIT_PRICE']; ?></td>
                              <td class="text-center amount"><?php echo $sale_detail['TOTAL_AMOUNT']; ?></td>
                              <td class="text-center qty_length"><?php echo $sale_detail['QTY_LENGTH']; ?></td>
                              <td class="text-center final_amount"><?php echo $sale_detail['FINAL_AMOUNT']; ?></td>
                              <td class="text-center">
                                <a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
                                <a class="pointer" onclick="showDeleteRowDialogue(this);" title="Delete"><i class="fa fa-times"></i></a>
                              </td>
                            </tr>
<?php
                          }
                        }
?>
                        </tbody>
                        <tfoot>
                          <tr class="totals">
                          	<th class='text-right pr-10' >Total</th>
                            <th class="text-center">- - -</th>
                            <th class="total_stitches text-center"></th>
                            <th class="text-center" colspan="2">- - -</th>
                            <th class="total_length   text-center"></th>
                            <th class="total_amount   text-center"></th>
                            <th class="text-center">- - -</th>
                          </tr>
                        </tfoot>
                      </table>
                  <div class="clear" style="height:10px;"></div>

                  <div class="col-xs-5">
                      <div class="panel panel-default">
                          <div class="panel-heading">Notes : </div>
                          <textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $embSaleDetails['NOTES']; ?></textarea>
                      </div>
                  </div>
                  <div class="clear" style="height: 20px;"></div>
                  <div class="underTheTable col-md-5 pull-right" style="text-align: right;padding-right:30px;">
                <?php
                	if($embSaleDetails != NULL){
                ?>
                    <a class="button print_that" target="new" href="<?php echo $invoiceFile; ?>?id=<?php echo $embSaleDetails['ID']; ?>"><i class="fa fa-print"></i> Print</a>
                <?php
					        }
                  if(($sale_id > 0 && in_array('modify-sales',$permissionz)) || $admin == true || $sale_id == 0){
                    if(($embSaleDetails==NULL||(isset($_GET['c'])))){
                ?>
                    <button class="button save_sale"><?php echo ($embSaleDetails==NULL||(isset($_GET['c'])))?"Save &amp; New":""; ?></button>
              <?php } ?>
                    <button class="button save_print_sale"><?php echo ($embSaleDetails==NULL||(isset($_GET['c'])))?"Save":"Update"; ?>  &amp; Print</button>
                <?php
                  }
                ?>
                  <div class="button" onclick="window.location.href='emb-sale-details.php';">New Form</div>
                  <div class="clear"></div>
                </div><!--underTheTable-->
                <div class="clear"></div>
                <div style="margin-top:10px"></div>
		 </div> <!-- End form -->
          </div> <!-- End #tab1 -->
      </div> <!-- End .content-box-content -->
  </div> <!-- End .content-box -->
</div><!--body-wrapper-->
<div id="xfade"></div>
<div id="fade"></div>
<div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
<div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
  $(".reload_item").click(function(){
      $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(data){
           $("select.machine_id").html($(data).find("select.machine_id").html()).selectpicker('refresh');
      });
  });
  $("input.tax_invoice").change(function(){
    var use_tax = $(this).is(":checked")?"Y":"N";
    if(use_tax == 'Y'){
      $("input.registered_tax").prop('checked',true);
    }else{
      $("input.registered_tax").prop('checked',false);
    }
    $.post('<?php echo basename($_SERVER['PHP_SELF']); ?>',{get_tax_invoice_bill:use_tax},function(data){
      data = parseInt(data)||0;
      if(use_tax == 'Y'){
        $("input[name=taxBillNum]").val(data);
      }else{
        $("input[name=taxBillNum]").val('');
      }
    });
  });
$('select').selectpicker();
$(this).calculateColumnTotals();
$("#datepicker").setFocusTo("input[name='billNum']");
$("input[name='billNum']").setFocusTo(".dropdown-toggle:first");
$(".account_code").change(function(){
      var cust_code = $(".account_code option:selected").val();
      if(cust_code != ''){
          $("div.machine_id button").focus();
      }
});
  $("div.machine_id button").keyup(function(e){
    if(e.keyCode == 13 && $('select.machine_id option:selected').val() != ''){
      $("input.design_no").focus();
    }
  });
  $("select.machine_id,input.stitches,input.rate").on('change keyup',function(){
    $(this).calculateRowTotal();
  });
  $("input.qty_length").on("keyup change",function(){
    var qty_length   = parseFloat($("input.qty_length").val())||0;
    var amount       = $("input.amount").val();
    $("input.final_amount").val(Math.round(amount * qty_length));
  });
  $("input.design_no").keydown(function(e){
      if(e.keyCode == 13){
          $("input.stitches").focus();
      }
  });
  $("input.stitches").keydown(function(e){
      if(e.keyCode == 13){
          $("input.rate").focus();
      }
  });
  $("input.rate").keydown(function(e){
      if(e.keyCode == 13){
          $("input.amount").focus();
      }
  });
  $("input.amount").keydown(function(e){
      if(e.keyCode == 13){
          $("input.qty_length").focus();
      }
  });
  $("input.qty_length").keydown(function(e){
      if(e.keyCode == 13){
          $("input.insert_button").focus();
      }
  });
  var qtyE = 0;

  $("input.quantity").keyup(function(e){
      stockOs();
  });
$("input.quantity").keydown(function(e){
if(e.keyCode == 13){
	if(parseInt($(this).val())||0){
		$("input.unitPrice").focus();
	}
}
});
$(".insert_button").keydown(function(e){
  if(e.keyCode == 13){
  	$(this).quickSave();
  }
  if(e.keyCode == 27){
  	$("div.machine_id button").focus();
  }
});
  $(".save_sale").click(function(){
    print_method    = 'N';
    redirect_method = 'N';
    saveSale();
  });
  $("input[name='mobile_no']").focus(function(){
  check_sms_service();
  });

  $("div.machine_id button").focus();
  $(window).keyup(function(e){
      if(e.keyCode == 113){
          window.location.href = 'sale-details.php';
      }
  });
  $("button.save_print_sale").click(function(){
    print_method = 'Y';
    saveSale();
  });
  $(window).keydown(function(e){
      //console.log(e.keyCode);
      if(e.altKey == true&&e.keyCode == 73){
          e.preventDefault();
          $("#form").click();
          $("div.machine_id button").click();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 65){
          e.preventDefault();
          $("#form").click();
          $("div.account_code button").click();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 66){
          e.preventDefault();
          $("#form").click();
          $("input.barcode_input").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 77){
          e.preventDefault();
          $("#form").click();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 78){
          e.preventDefault();
          $("#form").click();
          $("textarea.inv_notes").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 68){
          e.preventDefault();
          $("#form").click();
          $("input.whole_discount").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 79){
          e.preventDefault();
          $("#form").click();
          $("input.inv_charges").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 83){
          e.preventDefault();
          $("#form").click();
          print_method = 'N';
          saveSale();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 80){
          e.preventDefault();
          $("#form").click();
          print_method = 'Y';
          saveSale();
          return false;
      }
  });
<?php
  if(isset($_GET['print'])){
      $inv_idx = isset($_GET['method'])?(int)($_GET['method']):$embSaleDetails['ID'];
?>
  var id  = <?php echo $inv_idx; ?> ;
  var win = window.open("<?php echo $invoiceFile; ?>?id="+id,"_blank");
  if(win){
      win.focus();
  }
<?php
  }
?>
});
<?php
if(isset($_GET['saved'])){
?>
displayMessage('Bill Saved Successfully!');
<?php
}
?>
<?php
if(isset($_GET['updated'])){
?>
displayMessage('Bill Updated Successfully!');
<?php
}
?>

</script>
