<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/inventory-return.php');
	include('common/classes/inventory-return-details.php');
	include('common/classes/dist_sale.php');
	include('common/classes/dist_sale_details.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/dist_sale_return_details.php');
    include('common/classes/demand_list.php');
	include('common/classes/demand_list_detail.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('po-stock-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	   			   = new ChartOfAccounts();
	$objInventory        	   			   = new inventory();
	$objInventoryDetails 	   			   = new inventory_details();
	$objInventoryReturnDetails 			   = new InventoryReturnDetails();
	$objDistributorSale 	   			   = new DistributorSale();
	$objDistributorSaleDetails 	   		   = new DistributorSaleDetails();
	$objDistributorSaleReturn 			   = new DistributorSaleReturn();
	$objDistributorSaleReturnDetails 	   = new DistributorSaleReturnDetails();
    $objDemandList         	   			   = new DemandList();
    $objDemandListDetail   	   			   = new DemandListDetail();
	$objItems              	   			   = new Items();
	$objItemCategory       	   			   = new itemCategory();
	$objTaxRates           	   			   = new TaxRates();
	$objDepartments        	   			   = new Departments();
	$objConfigs 		   	   			   = new Configs();

	$suppliersList   		   			   = $objInventory->getSuppliersList();

	$titleRepo = '';
	$po_number = '';

	$po_stock_report = array();
	if(isset($_GET['search'])){
        $po_number           			    = mysql_real_escape_string($_GET['po_number']);
		$po_stock_report[$po_number] 		= array();

		$purchases 		  = $objInventoryDetails->getListByPoNumber($po_number);
		$purchase_returns = $objInventoryReturnDetails->getListByPoNumber($po_number);
		$sales 			  = $objDistributorSaleDetails->getListByPoNumber($po_number);
		$sale_returns 	  = $objDistributorSaleReturnDetails->getListByPoNumber($po_number);

		if(mysql_num_rows($purchases)){
			while($purchase = mysql_fetch_assoc($purchases)){
				if(!isset($po_stock_report[$po_number][$purchase['ITEM_ID']])){
					$po_stock_report[$po_number][$purchase['ITEM_ID']] = array();
				}
				if(!isset($po_stock_report[$po_number][$purchase['ITEM_ID']]['PURCHASES'])){
					$po_stock_report[$po_number][$purchase['ITEM_ID']]['PURCHASES'] = 0;
				}
				$po_stock_report[$po_number][$purchase['ITEM_ID']]['PURCHASES'] += $purchase['TOTAL_QTY'];
			}
		}
		if(mysql_num_rows($purchase_returns)){
			while($purchase_return = mysql_fetch_assoc($purchase_returns)){
				if(!isset($po_stock_report[$po_number][$purchase_return['ITEM_ID']])){
					$po_stock_report[$po_number][$purchase_return['ITEM_ID']] = array();
				}
				if(!isset($po_stock_report[$po_number][$purchase_return['ITEM_ID']]['PURCHASE_RETURN'])){
					$po_stock_report[$po_number][$purchase_return['ITEM_ID']]['PURCHASE_RETURN'] = 0;
				}
				$po_stock_report[$po_number][$purchase_return['ITEM_ID']]['PURCHASE_RETURN'] += $purchase_return['TOTAL_QTY'];
			}
		}
		if(mysql_num_rows($sales)){
			while($sale = mysql_fetch_assoc($sales)){
				if(!isset($po_stock_report[$po_number][$sale['ITEM_ID']])){
					$po_stock_report[$po_number][$sale['ITEM_ID']] = array();
				}
				if(!isset($po_stock_report[$po_number][$sale['ITEM_ID']]['SALES'])){
					$po_stock_report[$po_number][$sale['ITEM_ID']]['SALES'] = 0;
				}
				$po_stock_report[$po_number][$sale['ITEM_ID']]['SALES'] += $sale['TOTAL_QTY'];
				$po_stock_report[$po_number][$sale['ITEM_ID']]['SALES'] += ($sale['TOTAL_CARTONS']*$sale['QTY_PER_CARTON']);
			}
		}
		if(mysql_num_rows($sale_returns)){
			$po_stock_report[$po_number]['SALE'] = array();
			while($sale_return = mysql_fetch_assoc($sale_returns)){
				if(!isset($po_stock_report[$po_number][$sale_return['ITEM_ID']])){
					$po_stock_report[$po_number][$sale_return['ITEM_ID']] = array();
				}
				if(!isset($po_stock_report[$po_number][$sale_return['ITEM_ID']]['SALE_RETURN'])){
					$po_stock_report[$po_number][$sale_return['ITEM_ID']]['SALE_RETURN'] = 0;
				}
				$po_stock_report[$po_number][$sale_return['ITEM_ID']]['SALE_RETURN'] += $sale_return['TOTAL_QTY'];
				$po_stock_report[$po_number][$sale_return['ITEM_ID']]['SALE_RETURN'] += ($sale_return['TOTAL_CARTONS']*$sale_return['QTY_PER_CARTON']);
			}
		}
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
		$(window).on('load',function(){
			$(".printThis").click(function(){
				if($("div.tablePage").length==0){
					var MaxHeight = 480;
					var RunningHeight = 0;
					var PageNo = 1;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var tableFooter = $(".tableFooter").html();
					var repoDate = $(".repoDate").text();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					$(".tableFooter").remove();
					$(".repoDate").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					$(".newTable").last().append(tableFooter);
					$('table.tableBreak').remove();

			$("div.tablePage").each(function(i,e){
				$(this).prepend($(".pageHeader").first().clone());
			});
			$(".pageHeader").first().remove();
				}
				$(".printTable").printThis({
					debug: false,
					importCSS: false,
					printContainer: true,
					loadCSS: 'resource/css/reports-horizontal.css',
					pageTitle: "Sit Solution",
					removeInline: false,
					printDelay: 500,
					header: null
				});
			});
			if($(".carton_th").length){
				var colspan = $(".tHeader .carton_th").prevAll().length;
			}else{
				var colspan = $(".tHeader th.qty_th").prevAll().length;
			}
			$(".total_tf").attr('colspan',colspan);
			$('select').selectpicker();
		});
	</script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>P.O Stock Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="get" action="">
                        <div class="caption">PO #</div>
                        <div class="field">
                            <input type="text" class="form-control po_number" name="po_number" required />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="btn btn-info"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />

<?php
					if(isset($po_stock_report)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">P.O Stock Report </p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
								P.O # <?php echo $po_number; ?>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						$prevBillNo = '';
						if(count($po_stock_report)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="15%" class="text-center" >Items</th>
                                   <th width="7%"  class="text-center" >Qty.Purchases</th>
								   <th width="7%"  class="text-center" >Purchase.Returns</th>
                                   <th width="7%"  class="text-center" >Qty.Avail.for.Sale</th>
                                   <th width="7%"  class="text-center" >Qty.Sold</th>
								   <th width="7%"  class="text-center" >Sale.Returns</th>
								   <th width="7%"  class="text-center" >Net.Sales</th>
                                   <th width="7%"  class="text-center" >Remaining</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_purchase_qty   	= 0;
								$total_purchase_return  = 0;
								$total_avail_for_sale   = 0;
								$total_sale_qty   		= 0;
								$total_sale_return   	= 0;
								$total_net_sales   		= 0;
								$total_remaining   		= 0;
								foreach($po_stock_report as $po_number => $demand_row){
									foreach($demand_row as $item_id => $forms){
										if(((int)($item_id)) == 0){
											continue;
										}
										$item_name       = $objItems->getItemTitle($item_id);

										$purchase_qty    = isset($forms['PURCHASES'])?         number_format((float)$forms['PURCHASES'],2):0;
										$purchase_return = isset($forms['PURCHASE_RETURN'])?   number_format((float)$forms['PURCHASE_RETURN'],2):0;
										$avail_for_sale  = $purchase_qty-$purchase_return;
										$sale_qty 		 = isset($forms['SALES'])?         	   number_format((float)$forms['SALES'],2):0;
										$sale_return     = isset($forms['SALE_RETURN'])?       number_format((float)$forms['SALE_RETURN'],2):0;
										$net_sales  	 = $sale_qty      - $sale_return;
										$remaining  	 = $avail_for_sale- $net_sales;
?>
                                <tr>
                                    <td class="text-center"><?php  echo $item_name; ?></td>
                                    <td class="text-center"><?php  echo $purchase_qty; ?></td>
									<td class="text-center"><?php  echo $purchase_return; ?></td>
									<td class="text-center"><?php  echo $avail_for_sale; ?></td>
									<td class="text-center"><?php  echo $sale_qty; ?></td>
									<td class="text-center"><?php  echo $sale_return; ?></td>
									<td class="text-center"><?php  echo $net_sales; ?></td>
									<td class="text-center"><?php  echo $remaining; ?></td>
                                </tr>
<?php
										$total_purchase_qty     += $purchase_qty;
										$total_purchase_return  += $purchase_return;
										$total_avail_for_sale   += $avail_for_sale;
										$total_sale_qty     	+= $sale_qty;
										$total_sale_return     	+= $sale_return;
										$total_net_sales 		+= $net_sales;
										$total_remaining     	+= $remaining;
									}
								}
?>

                            </tbody>
<?php
						}//end if
						if(count($po_stock_report)){
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td class="text-center">Total:</td>

								<td class="text-center"><?php echo $total_purchase_qty; ?></td>
								<td class="text-center"><?php echo $total_purchase_return; ?></td>
								<td class="text-center"><?php echo $total_avail_for_sale; ?></td>
								<td class="text-center"><?php echo $total_sale_qty; ?></td>
								<td class="text-center"><?php echo $total_sale_return; ?></td>
								<td class="text-center"><?php echo $total_net_sales; ?></td>
								<td class="text-center"><?php echo $total_remaining; ?></td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
