<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/inventory.php';
	include 'common/classes/inventoryDetails.php';
	include 'common/classes/suppliers.php';
	include 'common/classes/items.php';
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('invoice-purchases',$permissionz) && $admin != true){
			echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
			echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
			echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
			echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
			echo '</div>';
			exit();
	}
	//Permission ---END--

	$objCompanyDetails     = new CompanyDetails();
	$objPurchase           = new inventory();
	$objPurchaseDetails    = new inventory_details();
	$objSuppliers          = new suppliers();
	$objItems  	           = new Items();
	$objJournalVoucher     = new JournalVoucher();
  $objConfigs            = new Configs();

  $currency_type     = $objConfigs->get_config('CURRENCY_TYPE');

	if(isset($_GET['id'])){
		$purchase_id            = mysql_real_escape_string($_GET['id']);
		$purchaseDetails        = mysql_fetch_array($objPurchase->getAny($purchase_id));
		$purchaseDetailList     = $objPurchaseDetails->getList($purchase_id);
		$supplier_balance       = $objJournalVoucher->getInvoiceBalance($purchaseDetails['SUPP_ACC_CODE'],$purchaseDetails['VOUCHER_ID']);
		$supplier_balance_array = $objJournalVoucher->getBalanceType($purchaseDetails['SUPP_ACC_CODE'], $supplier_balance);
	}
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/invoiceStyle.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/invoiceStyle.css",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			//$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100%;background-repeat: no-repeat;">
<?php
	if(isset($purchase_id)){
		if(substr($purchaseDetails['SUPP_ACC_CODE'], 0,6) == '010101'){
			$supplier['ADDRESS'] = '';
			$supplier['CITY'] = '';
			$supplier['SUPP_ACC_TITLE'] 	   = $purchaseDetails['SUPPLIER_NAME'];
		}else{
			$supplier 	   = $objSuppliers->getSupplier($purchaseDetails['SUPP_ACC_CODE']);
		}
		$purchase_date     = date('d-m-Y',strtotime($purchaseDetails['PURCHASE_DATE']));
		$ledgerDate    		 = date('Y-m-d',strtotime($purchase_date));

		$companyLogo   	 	= $objCompanyDetails->getLogo();
		$company  		 		= $objCompanyDetails->getActiveProfile();
?>
<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
        </div><!--headerWrapper-->
    </div><!--header-->
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
        <div class="printThisDiv">
            <div class="invoiceHead" style="width: 720px;margin: 0px auto;">
<?php
				if($companyLogo != ''){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
            		<div style="font-size:26px;padding-bottom: 10px;"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']; ?>
                    <br />
                    Contact : <?php echo $company['CONTACT']; ?>
                    <br />
                    Email : <?php echo $company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
	               <?php echo (substr($purchaseDetails['SUPP_ACC_CODE'], 0,6) == '010101')?"CASH":""; ?> PURCHASE BILL
                </div>
                <div class="clear" style="height: 5px;"></div>
                <div class="infoPanel pull-left">
                	<div class="infoHead">Supplier Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $supplier['SUPP_ACC_TITLE']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $supplier['ADDRESS']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $supplier['CITY']; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right">
                	<div class="infoHead">Bill Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Bill No. <?php echo $purchaseDetails['BILL_NO']; ?></span>
                	<div class="clear"></div>
	                <span class="pull-left" style="padding:0px 0px;font-size:14px;">Date. <?php echo $purchase_date; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear" style="height: 10px;"></div>
            <div class="invoiceBody" style="width: 720px;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="5%">Sr#</th>
                            <th width="25%">Description</th>
                            <th width="5%">Qty</th>
                            <th width="5%">Rate</th>
                            <th width="5%">Disc</th>
                            <th width="10%">SubTotal</th>
                            <th width="5%">Tax@</th>
                            <th width="5%">Tax</th>
                            <th width="15%">Total(<?php echo $currency_type; ?>)</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($purchaseDetailList)){
							while($row = mysql_fetch_array($purchaseDetailList)){
								$itemName = $objItems->getItemTitle($row['ITEM_ID']);
?>
								<tr>
									<td><?php echo $counter; ?></td>
									<td><?php echo $itemName; ?></td>
									<td><?php echo $row['STOCK_QTY']; ?></td>
									<td><?php echo $row['UNIT_PRICE']; ?></td>
                  <td><?php echo $row['PURCHASE_DISCOUNT']; ?></td>
                  <td><?php echo number_format(round($row['SUB_AMOUNT']),2); ?></td>
                  <td><?php echo $row['TAX_RATE']; ?></td>
                  <td><?php echo number_format(round($row['TAX_AMOUNT']),2); ?></td>
                  <td class="text-right"><?php echo number_format(round($row['TOTAL_AMOUNT']),2); ?></td>
								</tr>
<?php
									$quantity    += $row['STOCK_QTY'];
									$subAmount 	 += $row['SUB_AMOUNT'];
									$taxAmount   += $row['TAX_AMOUNT'];
									$totalAmount += $row['TOTAL_AMOUNT'];

									$counter++;
							}
						}
?>
                    </tbody>
                    <tfoot>
                        <tr class="bold_table_tr">
                            <td class="text-center" colspan="2">Bill Total</td>
                            <td class="text-right"><?php echo $quantity; ?></td>
                            <td class="text-center" colspan="2"> - - - </td>
                            <td class="text-center"><?php echo number_format(round($subAmount),2); ?></td>
                            <td class="text-center"> - - - </td>
                            <td class="text-center"><?php echo number_format(round($taxAmount),2); ?></td>
                            <td class="text-right"><?php echo number_format(round($totalAmount),2); ?></td>
                        </tr>
                        <tr style="background-color:  #EEE;">
                            <td colspan="8" class="text-right">Balance : </td>
                        	<td style="text-align: right;padding-right: 10px;">
<?php
						if(substr($purchaseDetails['SUPP_ACC_CODE'], 0,6) != '010101'){
?>
                        		<?php echo number_format($supplier_balance_array['BALANCE'],2)." ".$supplier_balance_array['TYPE']; ?>
<?php
						}else{
                            echo "0.00";
                        }
?>
                        	</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>
                <div class="clear" style="height: 5px;"></div>
                <div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
                	Prepared By : ___________
                </div><!--partyTitle-->
                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
                	Checked By : ___________
                </div><!--partyTitle-->
                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
                	Authorized By : ___________
                </div><!--partyTitle-->
                <div class="clear"></div>
            </div><!--invoiceBody-->
        </div><!--printThisDiv-->
        </div><!--invoiceLeftPrint-->
    </div><!--invoiceContainer-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
