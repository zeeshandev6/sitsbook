<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/inventory-return.php');
	include('common/classes/mobile-purchase.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/j-voucher.php');
	include 'common/classes/cash-book.php';
	include('common/classes/items.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('daily-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     		= new ChartOfAccounts();
	$objSales 			 				= new Sale();
	$objSaleDetails      		= new SaleDetails();
	$objSaleReturn 					= new SaleReturn();
	$objInventory        		= new inventory();
	$objInventoryDetails 		= new inventory_details();
	$objInventoryReturn     = new InventoryReturn();
	$objScanPurchase     	  = new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objScanSale     		    = new ScanSale();
	$objScanSaleDetails     = new ScanSaleDetails();
	$objJournalVoucher   		= new JournalVoucher();
	$objItems            		= new Items();
	$objCashBook 		 				= new cashbook();
	$objCompanyDetails 			= new CompanyDetails();

	if(isset($_POST['report_date'])){
		$report_date 							= date('Y-m-d',strtotime($_POST['report_date']));
		$purchase_report 	  		 	= $objInventory->getDailyPurchases($report_date);
		$purchase_return_report 	= $objInventoryReturn->getDailyPurchaseReturns($report_date);
		$scan_purchase_report 		= $objScanPurchase->getDailyPurchases($report_date);
		$sale_report 							= $objSales->getDailySales($report_date);
		$sale_return_report				= $objSaleReturn->getDailySales($report_date);
		$sale_return_item_cost    = $objSaleReturn->getCostForRepoSummary($report_date);
		$scan_sale_report   			= $objScanSale->getDailySales($report_date);
		$expenseAccounts 					= $objAccountCodes->getLevelFourExpenseList();
		$expenseAccounts_array 		= array();

		if(mysql_num_rows($expenseAccounts)){
			while($accCode = mysql_fetch_array($expenseAccounts)){
				$expenseAccounts_array[] = $accCode['ACC_CODE'];
			}
		}else{
			$expenseAccounts_array = NULL;
		}
		$cashPurchases       = $objCashBook->getCashPurchases($report_date);
		$cashSales       	 	 = $objCashBook->getCashSales($report_date);
		$cashReceipts        = $objCashBook->getCashReceipts($report_date);
		$cashPayments        = $objCashBook->getCashPayments($report_date);
		$bankReceipts        = $objCashBook->getBankReceipts($report_date);
		$bankPayments        = $objCashBook->getBankPayments($report_date);

		$cash_list 					 = array();
		$cash_list[] 				 = '010101';
		$openingBalance      = $objCashBook->getOpeningBalanceSum($report_date,$cash_list);
		$openingBalance      = round($openingBalance,2);
		$openingBalanceType  = ($openingBalance<0)?"Cr":"Dr";

		$party_cheques_bal   = $objJournalVoucher->getLedgerOpeningBalance($report_date,'0101050001');

		$company_details     = $objCompanyDetails->getActiveProfile();
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/daily-report.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$("table.tableBreak").each(function(){
			if($(this).find("tr.entry_row").length == 0 && !$(this).hasClass("profit_loss_table")){
				$(this).remove();
			}
		});
		$(".printThis").click(function(){
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/daily-report.css',
				pageTitle: "Sitsbook.com",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
	});
	</script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Daily Transaction Summary</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="post" action="">
							<div class="caption">Report Date : </div>
							<div class="field" style="width:300px;">
								<input type="text" name="report_date" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px;" />
							</div><!--field-->
							<div class="clear"></div>
							<div class="caption"></div>
							<div class="field">
								<input type="submit" value="report" name="search" class="button"/>
							</div>
							<div class="clear"></div>
						</form>
					</div><!--form-->
					<?php
					if(isset($report_date)){
						?>
						<div class="col-xs-12">
						<span style="float:right;"><button class="button printThis">Print</button></span>
						<div class="clear"></div>
						<div id="bodyTab" class="printTable" style="margin: 0 auto;">
							<div class="pageHeader">
								<div class="logo_aside">
									<h1 class="h1"><?php echo $company_details['NAME']; ?></h1>
									<p class="slogan">
										Daily Summary Report
									</p>
									<p class="underSlogan">
										Report Date : <?php echo date("d-m-Y",strtotime($report_date)); ?>
									</p>
								</div>
								<div class="clear"></div>
							</div>
							<table class="prom tableBreak" style="margin:0; width:100%;" cellspacing="0" align="center">
								<thead class="tHeader">
									<tr>
										<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Cash Receipts</b></td>
									</tr>
									<tr>
										<th width="5%" class="bg-color-x">Voucher#</th>
										<th width="20%" class="bg-color-x">A/c Title</th>
										<th width="35%" class="bg-color-x">Description</th>
										<th width="20%" class="bg-color-x" style="text-align:center">Amount</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$totalAmountReceived = 0;
									if(mysql_num_rows($cashSales)){
										while($row = mysql_fetch_array($cashSales)){
											if($row['VOUCHER_TYPE'] == 'SV'){
												$row['ACCOUNT_CODE']  = '0201010001';
											}else{
												$row['ACCOUNT_CODE']  = '0101060002';
											}
											$row['ACCOUNT_TITLE'] = $objAccountCodes->getAccountTitleByCode($row['ACCOUNT_CODE']);
											?>
											<tr class="entry_row">
												<td  style="padding-left:5px;">JV # <?php echo $row['VOUCHER_NO']; ?></td>
												<td  style="text-align:left"><?php echo $row['ACCOUNT_TITLE']; ?></td>
												<td  style="text-align:left"><?php echo $row['NARRATION']; ?></td>
												<td class=" digits" style="text-align:right;"><?php echo number_format($row['AMOUNT'],2); ?></td>
											</tr>
											<?php
											$totalAmountReceived += $row['AMOUNT'];
										}
									}
									if(mysql_num_rows($cashReceipts)){
										while($row = mysql_fetch_array($cashReceipts)){
											?>
											<tr class="entry_row">
												<td  style="padding-left:5px;">JV # <?php echo $row['VOUCHER_NO']; ?></td>
												<td  style="text-align:left"><?php echo $row['ACCOUNT_TITLE']; ?></td>
												<td  style="text-align:left"><?php echo $row['NARRATION']; ?></td>
												<td class=" digits" style="text-align:right;"><?php echo number_format($row['AMOUNT'],2); ?></td>
											</tr>
											<?php
											$totalAmountReceived += $row['AMOUNT'];
										}
									}
									$bank_payments_array = array();
									if(mysql_num_rows($bankPayments)){
										while($row = mysql_fetch_array($bankPayments)){
											if(!isset($bank_payments_array[$row['VOUCHER_ID']])){
												$bank_payments_array[$row['VOUCHER_ID']] = array();
											}
											if(substr($row['ACCOUNT_CODE'], 0,6) == '010102'){
												$bank_payments_array[$row['VOUCHER_ID']]['BANK'] = $row;
											}elseif(substr($row['ACCOUNT_CODE'], 0,6) == '010101'){
												$bank_payments_array[$row['VOUCHER_ID']]['CASH'][] = $row;
											}
										}
									}
									foreach ($bank_payments_array as $voucher_id => $voucher_rows){
										if(!isset($voucher_rows['CASH'])){
											continue;
										}
										foreach($voucher_rows['CASH'] as $key => $cash_row)
										?>
										<tr class="entry_row">
											<td  style="padding-left:5px;">JV # <?php echo $bank_payments_array[$cash_row['VOUCHER_ID']]['BANK']['VOUCHER_NO']; ?></td>
											<td  style="text-align:left"><?php echo $bank_payments_array[$cash_row['VOUCHER_ID']]['BANK']['ACCOUNT_TITLE']; ?></td>
											<td  style="text-align:left"><?php echo $bank_payments_array[$cash_row['VOUCHER_ID']]['BANK']['NARRATION']; ?></td>
											<td  class="digits" style="text-align:right;"><?php echo number_format($cash_row['AMOUNT'],2); ?></td>
										</tr>
										<?php
										$totalAmountReceived += $cash_row['AMOUNT'];
									}
									?>
								</tr>
								<td  colspan="3"><div style="float: right;">Total Receipts:</div></td>
								<td class=" digits" style="text-align:right; font-weight:bold;">
									<?php echo (isset($totalAmountReceived))?number_format($totalAmountReceived,2):'0'; ?> DR
								</td>
							</tr>
							<tr>
								<td colspan="4" style="font-size:14px;text-align:left;border:none;"><b>Cash Payments</b></td>
							</tr>
							<?php
							$totalAmountPaid = 0;
							if(mysql_num_rows($cashPurchases)){
								while($voucher_detail = mysql_fetch_array($cashPurchases)){
									if($voucher_detail['VOUCHER_TYPE'] == 'SR'){
										$voucher_detail['ACCOUNT_CODE']  = '0201010003';
									}elseif($voucher_detail['VOUCHER_TYPE'] == 'PV'){
										$voucher_detail['ACCOUNT_CODE']  = '0101060001';
									}else{
										$voucher_detail['ACCOUNT_CODE'] = "";
									}
									$voucher_detail['ACCOUNT_TITLE'] = $objAccountCodes->getAccountTitleByCode($voucher_detail['ACCOUNT_CODE']);
									?>
									<tr class="entry_row">
										<td  style="padding-left:5px;">JV # <?php echo $voucher_detail['VOUCHER_NO']; ?></td>
										<td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_TITLE']; ?></td>
										<td  style="text-align:left"><?php echo $voucher_detail['NARRATION']; ?></td>
										<td class="digits" style="text-align:right;"><?php echo number_format($voucher_detail['AMOUNT'],2); ?></td>
									</tr>
									<?php
									$totalAmountPaid += $voucher_detail['AMOUNT'];
								}
							}
							if(mysql_num_rows($cashPayments)){
								while($voucher_detail = mysql_fetch_array($cashPayments)){
									?>
									<tr class="entry_row">
										<td  style="padding-left:5px;">JV # <?php echo $voucher_detail['VOUCHER_NO']; ?></td>
										<td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_TITLE']; ?></td>
										<td  style="text-align:left"><?php echo $voucher_detail['NARRATION']; ?></td>
										<td class="digits" style="text-align:right;"><?php echo number_format($voucher_detail['AMOUNT'],2); ?></td>
									</tr>
									<?php
									$totalAmountPaid += $voucher_detail['AMOUNT'];
								}
							}
							$bank_receipts_array = array();
							if(mysql_num_rows($bankReceipts)){
								while($row = mysql_fetch_array($bankReceipts)){
									if(!isset($bank_receipts_array[$row['VOUCHER_ID']])){
										$bank_receipts_array[$row['VOUCHER_ID']] = array();
									}
									if(substr($row['ACCOUNT_CODE'], 0,6) == '010102'){
										$bank_receipts_array[$row['VOUCHER_ID']]['BANK'] = $row;
									}elseif(substr($row['ACCOUNT_CODE'], 0,6) == '010101'){
										$bank_receipts_array[$row['VOUCHER_ID']]['CASH'][] = $row;
									}
								}
							}
							foreach ($bank_receipts_array as $voucher_id => $voucher_rows){
								if(!isset($voucher_rows['CASH'])){
									continue;
								}
								foreach($voucher_rows['CASH'] as $key => $cash_row)
								?>
								<tr class="entry_row">
									<td  style="padding-left:5px;">JV # <?php echo $bank_receipts_array[$cash_row['VOUCHER_ID']]['BANK']['VOUCHER_NO']; ?></td>
									<td  style="text-align:left"><?php echo $bank_receipts_array[$cash_row['VOUCHER_ID']]['BANK']['ACCOUNT_TITLE']; ?></td>
									<td  style="text-align:left"><?php echo $bank_receipts_array[$cash_row['VOUCHER_ID']]['BANK']['NARRATION']; ?></td>
									<td  class="digits" style="text-align:right;"><?php echo number_format($cash_row['AMOUNT'],2); ?></td>
								</tr>
								<?php
								$totalAmountPaid += $cash_row['AMOUNT'];
							}
							?>
						<tr>
							<td  colspan="3"><div style="float: right;">Total Payments:</div></td>
							<td class=" digits" style="text-align:right; font-weight:bold;">
								<?php echo (isset($totalAmountPaid))?number_format($totalAmountPaid,2):'0.00'; ?> CR
							</td>
						</tr>
				</tbody>
				<tfoot class="tableFooter">
					<?php
					$openingBalanceType = ($openingBalance<0)?"CR":"DR";
					$closingBalance     = ($openingBalance+$totalAmountReceived) - $totalAmountPaid;
					$closing_cash_org   = $closingBalance;

					$closingBalanceType = ($closingBalance<0)?"CR":"DR";
					$closingBalance     = str_replace('-','',$closingBalance);
					?>
				</tr>
				<td  colspan="3"><div  class="cashBookTotals">Opening Balance in Rs. </div></td>
				<td class=" digits" style="text-align:right"><?php echo number_format(str_replace('-','',$openingBalance),2)." ".$openingBalanceType; ?></td>
			</tr>
			<tr>
				<td  colspan="3"><div  class="cashBookTotals">Total Cash Receipt in Rs. </div></td>
				<td class=" digits" style="text-align:right">
					<?php echo (isset($totalAmountReceived))?number_format($totalAmountReceived,2)." DR":"0.00"; ?>
				</td>
			</tr>
			<tr>
				<td  colspan="3"><div  class="cashBookTotals">Total Cash Payment in Rs. </div></td>
				<td class=" digits" style="text-align:right">
					<?php echo (isset($totalAmountPaid))?number_format($totalAmountPaid,2)." CR":"0.00"; ?>
				</td>
			</tr>
			<tr>
				<td  colspan="3"><div  class="cashBookTotals">Total Cash on Hand in Rs. </div></td>
				<td class=" digits" style="text-align:right"><b><?php echo (isset($closingBalance))?number_format($closingBalance,2)." ".$closingBalanceType:"0.00"; ?></b></td>
			</tr>
			<tr>
				<td colspan="3"><div  class="cashBookTotals">Party Cheque Receipt Payments. </div></td>
				<td class=" digits" style="text-align:right"><b><?php echo (isset($party_cheques_bal))?number_format($party_cheques_bal,2):"0.00"; ?></b></td>
			</tr>
			<tr>
				<td colspan="3"><div  class="cashBookTotals">Total Cash &amp; Cheques . </div></td>
				<td class=" digits" style="text-align:right"><b><?php echo (isset($party_cheques_bal))?number_format($party_cheques_bal+$closing_cash_org,2):"0.00"; ?></b></td>
			</tr>
</tfoot>

</table>
<!-- END CASH BOOK-->
<?php
$total_purchase = 0;
?>
<table class="prom tableBreak">
	<thead class="tHeader">
		<tr>
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Purchases</b></td>
		</tr>
		<tr style="background:#EEE;">
			<th width="5%" style="font-size:12px;text-align:center;">Bill #</th>
			<th width="25%" style="font-size:12px;text-align:center">Supplier</th>
			<th width="10%" style="font-size:12px;text-align:center">Cash</th>
			<th width="10%" style="font-size:12px;text-align:center">on A/c</th>
			<th width="10%" style="font-size:12px;text-align:center">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(mysql_num_rows($purchase_report)){
			while($sale_row = mysql_fetch_array($purchase_report)){
				$customer_name = $objAccountCodes->getAccountTitleByCode($sale_row['SUPP_ACC_CODE']);
				$bill_total    = $objInventory->getInventoryAmountSum($sale_row['PURCHASE_ID']);
				$total_purchase   += $bill_total;
				if(substr($sale_row['SUPP_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['SUPPLIER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>
		<?php
		if(mysql_num_rows($scan_purchase_report)){
			while($sale_row = mysql_fetch_array($scan_purchase_report)){
				$customer_name = $objAccountCodes->getAccountTitleByCode($sale_row['SUPP_ACC_CODE']);
				$bill_total    = $objScanPurchaseDetails->get_bill_amount($sale_row['SP_ID']);
				$total_purchase   += $bill_total;
				if(substr($sale_row['SUPP_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['SUPPLIER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>

	</tbody>
	<?php
	if(mysql_num_rows($purchase_report)){
		?>
		<tfoot class="tableFooter">
			<tr>
				<td style="text-align:right;" colspan="4">Total:</td>
				<td style="text-align:right;font-weight:bold;"><?php echo number_format($total_purchase,2); ?></td>
			</tr>
		</tfoot>
		<?php
	}
	?>
</table>
<table class="prom tableBreak">
	<thead class="tHeader">
		<tr>
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Purchase Returns</b></td>
		</tr>
		<tr style="background:#EEE;">
			<th width="5%" style="font-size:12px;text-align:center;">Bill #</th>
			<th width="25%" style="font-size:12px;text-align:center">Supplier</th>
			<th width="10%" style="font-size:12px;text-align:center">Cash</th>
			<th width="10%" style="font-size:12px;text-align:center">on A/c</th>
			<th width="10%" style="font-size:12px;text-align:center">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total_purchase_return = 0;
		if(mysql_num_rows($purchase_return_report)){
			while($sale_row = mysql_fetch_array($purchase_return_report)){
				$customer_name = $objAccountCodes->getAccountTitleByCode($sale_row['SUPP_ACC_CODE']);
				$bill_total    = $objInventoryReturn->getInventoryAmountSum($sale_row['PURCHASE_ID']);
				$total_purchase_return   += $bill_total;
				if(substr($sale_row['SUPP_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['SUPPLIER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>
	</tbody>
	<?php
	if(mysql_num_rows($purchase_report)){
		?>

		<?php
	}
	?>
	<tfoot class="tableFooter">
		<tr>
			<td style="text-align:right;" colspan="4">Total:</td>
			<td style="text-align:right;font-weight:bold;"><?php echo number_format($total_purchase_return,2); ?></td>
		</tr>
		<tr>
			<th style="text-align:right;" colspan="4">Net Purchases:</th>
			<th style="text-align:right;font-weight:bold;"><?php echo number_format(($total_purchase-$total_purchase_return),2); ?></th>
		</tr>
	</tfoot>
</table>
<?php   $net_purchases = $total_purchase-$total_purchase_return; ?>
<table class="prom tableBreak">
	<?php
	$total_sale = 0;
	?>
	<thead class="tHeader">
		<tr>
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Sales</b></td>
		</tr>
		<tr style="background:#EEE;">
			<th width="5%" style="font-size:12px;text-align:center">Bill #</th>
			<th width="25%" style="font-size:12px;text-align:center">Customer</th>
			<th width="10%" style="font-size:12px;text-align:center">Cash</th>
			<th width="10%" style="font-size:12px;text-align:center">on A/c</th>
			<th width="10%" style="font-size:12px;text-align:center">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(mysql_num_rows($sale_report)){
			while($sale_row = mysql_fetch_array($sale_report)){
				$customer_name = $objAccountCodes->getAccountTitleByCode($sale_row['CUST_ACC_CODE']);
				$bill_total    = $objSales->getInventoryAmountSum($sale_row['SALE_ID']);
				$total_sale   += $bill_total;
				if(substr($sale_row['CUST_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['CUSTOMER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>
		<?php
		if(mysql_num_rows($scan_sale_report)){
			while($sale_row = mysql_fetch_array($scan_sale_report)){
				$customer_name = $objAccountCodes->getAccountTitleByCode($sale_row['CUST_ACC_CODE']);
				$bill_total    = $objScanSaleDetails->get_bill_amount($sale_row['SS_ID']);
				//$objScanSaleDetails
				$total_sale   += $bill_total;
				if(substr($sale_row['CUST_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['CUSTOMER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>
	</tbody>
	<?php
	if(mysql_num_rows($sale_report)){
		?>
		<tfoot class="tableFooter">
			<tr>
				<td style="text-align:right;" colspan="4">Total:</td>
				<td style="text-align:right;font-weight:bold;"><?php echo number_format($total_sale,2); ?></td>
			</tr>
		</tfoot>
		<?php
	}
	?>
</table>
<table class="prom tableBreak">
	<thead class="tHeader">
		<tr>
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Sale Returns</b></td>
		</tr>
		<tr style="background:#EEE;">
			<th width="5%" style="font-size:12px;text-align:center;">Bill #</th>
			<th width="25%" style="font-size:12px;text-align:center">Customer</th>
			<th width="10%" style="font-size:12px;text-align:center">Cash</th>
			<th width="10%" style="font-size:12px;text-align:center">on A/c</th>
			<th width="10%" style="font-size:12px;text-align:center">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total_sale_return = 0;
		$total_return_cost = 0;
		if(mysql_num_rows($sale_return_report)){
			while($sale_row = mysql_fetch_array($sale_return_report)){
				$customer_name 				= $objAccountCodes->getAccountTitleByCode($sale_row['CUST_ACC_CODE']);
				$bill_total    				= $objSaleReturn->getInventoryAmountSum($sale_row['SALE_ID']);
				$total_sale_return   += $bill_total;
				if(substr($sale_row['CUST_ACC_CODE'], 0,6)=='010101'){
					$cash_amount = number_format($bill_total,2);
					$accc_amount = '- - -';
				}else{
					$cash_amount = '- - -';
					$accc_amount = number_format($bill_total,2);
				}
				?>
				<tr class="entry_row">
					<td style="text-align:center;"><?php echo $sale_row['BILL_NO']; ?></td>
					<td style="text-align:left;"><?php echo substr($customer_name, 0,16)." - - ".$sale_row['CUSTOMER_NAME']; ?></td>
					<td style="text-align:center;"><?php echo $cash_amount; ?></td>
					<td style="text-align:center;"><?php echo $accc_amount; ?></td>
					<td style="text-align:right;"><?php echo number_format($bill_total,2); ?></td>
				</tr>
				<?php
			}
		}//end Sale num Rows
		?>
	</tbody>
	<tfoot class="tableFooter">
		<tr>
			<td style="text-align:right;" colspan="4">Total:</td>
			<td style="text-align:right;font-weight:bold;"><?php echo number_format($total_sale_return,2); ?></td>
		</tr>
		<tr>
			<th style="text-align:right;" colspan="4">Net Sales:</th>
			<th style="text-align:right;font-weight:bold;"><?php echo number_format(($total_sale-$total_sale_return),2); ?></th>
		</tr>
	</tfoot>
</table>
<?php $net_sales = $total_sale-$total_sale_return; ?>
<table class="prom tableBreak">
	<thead class="tHeader">
		<tr>
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Expenses</b></td>
		</tr>
		<tr style="background:#EEE;">
			<th width="60%" style="font-size:12px;text-align:center">Account Title</th>
			<th width="15%" style="font-size:12px;text-align:center">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$expense_total = 0;
		foreach ($expenseAccounts_array as $key => $expense_code){
			$expense_amount = $objJournalVoucher->getTotalDebits($expense_code,$report_date);
			if($expense_code == '0301040001'){
				$expense_amount-= $sale_return_item_cost;
			}
			if($expense_amount == 0){
				continue;
			}
			$expense_total += $expense_amount;
			?>
			<tr class="entry_row">
				<td style="text-align:left;"><?php echo $objAccountCodes->getAccountTitleByCode($expense_code); ?></td>
				<td style="text-align:right;"><?php echo number_format($expense_amount,2); ?></td>
			</tr>
			<?php
		}
		?>
	</tbody>
	<tfoot class="tableFooter">
		<tr>
			<td style="text-align:right;">Total Expense:</td>
			<td  style="text-align:right;font-weight:bold;"><?php echo number_format($expense_total,2); ?></td>
		</tr>
	</tfoot>
</table>
<table class="prom profit_loss_table tableBreak" style="margin:0; width:100%;" cellspacing="0" align="center">
	<thead class="tHeader">
		<tr class="entry_row">
			<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Profit and Loss</b></td>
		</tr>
		<tr class="entry_row">
			<td width="85%" style="text-align:right;padding-right:10px;">Total Sales</td>
			<td width="15%" style="text-align:right"><?php echo number_format($net_sales,2); ?></td>
		</tr>
		<tr class="entry_row">
			<td style="text-align:right;padding-right:10px;">Total Expenses</td>
			<td style="text-align:right"><?php echo number_format($expense_total,2); ?></td>
		</tr>
		<?php
		$ProfitOrLoss = $net_sales-$expense_total;
		$str_pnl = '';
		if($ProfitOrLoss < 0){
			$str_pnl = "LOSS";
		}else{
			$str_pnl = "PROFIT";
		}
		?>
		<tr class="entry_row">
			<td style="text-align:right;padding-right:10px;">Profit and Loss</td>
			<td style="text-align:right;font-weight:bold;"><?php echo number_format(str_ireplace('-', '', $ProfitOrLoss),2)." $str_pnl"; ?></td>
		</tr>
	</thead>
</table>
<div class="clear"></div>
</div> <!--End bodyTab-->
</div>
<?php
} //end if is generic report
?>
</div> <!-- End bodyTab1 -->
</div> <!-- End .content-box-content -->
</div><!--content-box-->
</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
		?>
		tab('1', '1', '2')
		<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
		?>
		tab('2', '1', '2')
		<?php
	}
?>
</script>
