<?php
  include('common/connection.php');
  include 'common/config.php';
  include('common/classes/sale.php');
  include('common/classes/accounts.php');
  include('common/classes/j-voucher.php');
  include('common/classes/items.php');
  include('common/classes/itemCategory.php');
  include('common/classes/customers.php');
  include('common/classes/suppliers.php');
  include('common/classes/purchase_invoice.php');
  include('common/classes/purchase_invoice_details.php');

  //Permission
  if(!in_array('invoice-purchases',$permissionz) && $admin != true){
      echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
      echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
      echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
      echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
      echo '</div>';
      exit();
  }
  //Permission ---END--

  $objSale                = new Sale();
  $objJournalVoucher      = new JournalVoucher();
  $objAccountCodes        = new ChartOfAccounts();
  $objItems               = new Items();
  $objItemCategory        = new ItemCategory();
  $objCustomers           = new Customers();
  $objSuppliers           = new suppliers();
  $objConfigs             = new Configs();
  $objInvoice             = new PurchaseInvoice();
  $objInvoiceDetails      = new PurchaseInvoiceDetails();

  $total                  = $objConfigs->get_config('PER_PAGE');
  $open_invoice_supplier  = $objConfigs->get_config('OPEN_INVOICE_SUPPLIER');
  $objAccountsList        = $objAccountCodes->getLevelFourList();
  $ac_array               = array();
  $invoice_no             = $objInvoice->genInvoiceNumber();

  $invoice_id = 0;
  $invoice_details = NULL;

  $invoice_id = (isset($_GET['id']))?mysql_real_escape_string($_GET['id']):$invoice_id;

  $invoice_id = (int)($invoice_id);

  if($invoice_id > 0){
      $invoice_details      = $objInvoice->getDetail($invoice_id);
      $invoice_details_list = $objInvoiceDetails->getList($invoice_id);
  }
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <!-- jQuery -->
    <style type="text/css">
        .ui-effects-transfer {
            border: 1px solid #CCC;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/project.expense.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" >
        var refresh_accounts = function(elm_class){
          $.get("?",{},function(html){
            var options = $(html).find('select.'+elm_class).html();
            $("select."+elm_class).html(options);
            $("select."+elm_class).selectpicker("refresh");
          });
        };
        $(document).ready(function() {
            $("select").selectpicker();
            $("*[data-toggle=tooltip]").tooltip();
            $("table#invoice_rows").on("click","tr.invoice_rows button.pointer",function(){
                $(this).parent('td').parent('tr').remove();
                calculations();
            });
            $("input[name=amount_custom]").change(function(){
                var amount      = parseFloat($(this).val())||0;
                var description = $("input[name=desc_custom]").val();
                if(description==''||amount==0){
                    return;
                }
                var row = '<tr class="invoice_rows alt-row" data-row-id="0">'
                         +'<td class="description">'+description+'</td>'
                         +'<td class="text-center amount">'+amount+'</td>'
                         +'<td class="text-center"><button class="pointer"><i class="fa fa-times"></i></button></td>'
                         +'</tr>';
                $("table#invoice_rows tbody").append(row);
                $("input[name=amount_custom]").val('');
                $("input[name=desc_custom]").val('');
                calculations();
                $("input[name=desc_custom]").focus();
            });
            $("#save_invoice").click(function(){
                save_invoice();
            });
        });
        var calculations = function(){
            var invoice_total = 0;
            $("table#invoice_rows tbody tr").each(function(i,e){
                invoice_total += parseFloat($(this).find("td.amount").text())||0;
            });
            $("table#invoice_rows td.invoice_total").text((invoice_total).toFixed(2));
        };
        var save_invoice = function(){
            var invoice_id         = parseInt($("input[name=invoice_id]").val())||0;
            var invoice_no         = parseInt($("input[name=invoice_no]").val())||0;
            var invoice_date       = $("input[name=invoice_date]").val();
            var customer           = $("select[name=customer] option:selected").val();
            var expense_account     = $("select[name=expense_account] option:selected").val();
            var subject            = $("input[name=subject]").val();
            var comments           = $("textarea[name=comments]").val();
            var total_amount       = $("td.invoice_total").text();

            var custom_rows  = {};

            if(($("table#invoice_rows tr.invoice_rows").length) == 0){
                displayMessage("Error! No records selected for invoice.");
                return false;
            }

            if(customer == ''){
                displayMessage("Error! Supplier not selected.");
                return false;
            }
            if(expense_account == ''){
                displayMessage("Error! Expense not selected.");
                return false;
            }

            if(subject == ''){
                displayMessage("Error! Invoice subject is required.");
                return false;
            }

            $("#save_invoice").hide();
            $("table#invoice_rows tr.invoice_rows").each(function(i,e){
                var row_id      = parseInt($(this).attr("data-row-id"))||0;
                var description = $(this).find("td.description").text();
                var amount      = $(this).find("td.amount").text();

                custom_rows[i] = {};
                custom_rows[i].row_id      = row_id;
                custom_rows[i].desc        = description;
                custom_rows[i].amount      = amount;
            });
            custom_rows = JSON.stringify(custom_rows);

            $.post('db/save_purchase_invoice.php',
                {invoice_id:invoice_id,
                 invoice_no:invoice_no,
                 invoice_date:invoice_date,
                 customer:customer,
                 expense_account:expense_account,
                 subject:subject,
                 comments:comments,
                 total_amount:total_amount,
                 custom_rows:custom_rows},
                function(data){
                    $("#xfade").fadeOut();
                    if(data != ''){
                        data = $.parseJSON(data);
                        if(data['ID'] > 0){
                            window.location.href = "invoice-purchase-details.php?id="+data['ID'];
                        }else{
                            displayMessage(data['MSG']);
                            $(".pointer").show();
                        }
                    }
                    $("#save_invoice").show();
            });
        };
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Bill Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="purchase-invoice-list.php?mode=list" class="tab">List</a>
                        <a href="purchase-invoice-list.php?mode=search" class="tab">Search</a>
                        <a href="invoice-purchase-details.php?mode=form" class="tabSelected">Details</a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>" />
                        <div class="clear"></div>
                        <div class="title">Bill Details</div>

                        <div class="caption" style="width:100px;">Bill Date</div>
                        <div class="field" style="width:150px;">
                            <input type="text" name="invoice_date" value="<?php echo ($invoice_details==NULL)?date("d-m-Y"):date('d-m-Y',strtotime($invoice_details['INVOICE_DATE'])); ?>" class="form-control datepicker" />
                        </div>

                        <div class="caption" style="width:100px;">Bill #</div>
                        <div class="field" style="width:130px;">
                            <input type="text" name="invoice_no" value="<?php echo ($invoice_details==NULL)?$invoice_no:$invoice_details['INVOICE_NO']; ?>" class="form-control" readonly />
                        </div>
                        <div class="clear"></div>

                        <div class="caption" style="width:100px;"> Supplier </div>
                        <div class="field" style="width:760px;">
                            <select class="customer form-control show-tick" name="customer"
                                    data-style="btn-default" data-width="410"
                                    data-live-search="true" data-hide-disabled='true' style="border:none;" >
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($objAccountsList)){
                            while($account = mysql_fetch_array($objAccountsList)){
                              if(substr($account['ACC_CODE'],0,2)=='03'||$account['ACC_CODE']=='010112001'){
                                $ac_array[] = $account;
                              }
                              if(substr($account['ACC_CODE'],0,6)!='040101'){
                                continue;
                              }
                              $selected = ($invoice_details['CUSTOMER'] == $account['ACC_CODE'])?"selected":"";
?>
                               <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?>><?php echo $account['ACC_TITLE']; ?></option>
<?php
                        }
                    }
?>
                            </select>
                            <a href='supplier-details.php' target="_blank" class="btn btn-default"><i class="fa fa-plus"></i></a>
                            <button type="button" onclick="refresh_accounts('customer')" class="btn btn-default"><i class="fa fa-refresh"></i></button>
                        </div>
                        <div class="clear"></div>

                        <div class="caption" style="width:100px;"> Expense A/c </div>
                        <div class="field" style="width:760px;">
                            <select class="expense_account form-control show-tick" name="expense_account"
                                    data-style="btn-default" data-width="410"
                                    data-live-search="true" data-hide-disabled='true' style="border:none;" >
                               <option selected value=""></option>
<?php
                        foreach($ac_array as $key => $account){
                                $selected = ($invoice_details['EXPENSE_ACCOUNT'] == $account['ACC_CODE'])?"selected":"";
?>
                               <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?>><?php echo $account['ACC_TITLE']; ?></option>
<?php
                        }
?>
                            </select>
                            <a href='accounts-management.php' target="_blank" class="btn btn-default"><i class="fa fa-plus"></i></a>
                            <button type="button" onclick="refresh_accounts('expense_account')" class="btn btn-default"><i class="fa fa-refresh"></i></button>
                        </div>
                        <div class="clear"></div>

                        <div class="caption" style="width:100px;">Subject</div>
                        <div class="field" style="width:760px;">
                            <input type="text" name="subject" value="<?php echo ($invoice_details==NULL)?"Acquired services":$invoice_details['SUBJECT']; ?>" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption" style="width:100px;">Particulars</div>
                        <div class="field" style="width:760px;">
                            <table class="table" id="invoice_rows">
                                <thead>
                                    <tr>
                                        <th class="text-center col-xs-9">Descrption</th>
                                        <th class="text-center col-xs-2">Amount</th>
                                        <th class="text-center col-xs-1">Action</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center col-xs-9"><input type="text" value="" name="desc_custom" class="form-control desc_custom" /></td>
                                        <td class="text-center col-xs-2"><input type="text" value="" name="amount_custom" class="form-control amount_custom" /></td>
                                        <td class="text-center col-xs-1">
                                            <button class="btn btn-default" onclick="$('input.amount_custom').change();">Enter</button>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $total_amount = 0.00;
                                        if(isset($invoice_details_list)&&mysql_num_rows($invoice_details_list)){
                                            while($row = mysql_fetch_assoc($invoice_details_list)){
                                                ?>
                                                    <tr class="invoice_rows alt-row" data-row-id="0">
                                                        <td class="description"><?php echo $row['DESCRIPTION'] ?></td>
                                                        <td class="text-center amount"><?php echo $row['AMOUNT'] ?></td>
                                                        <td class="text-center"><button class="pointer"><i class="fa fa-times"></i></button></td>
                                            </tr>
                                                <?php
                                                $total_amount+=$row['AMOUNT'];
                                            }
                                        }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="text-right pr-20"> Total </td>
                                        <td class="text-center invoice_total"><?php echo number_format($total_amount,2,'.',''); ?></td>
                                        <td class="text-center">- - -</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    <div class="clear"></div>

                    <div class="caption" style="width:100px;">Comments</div>
                    <div class="field" style="width:760px;">
                        <textarea name="comments"  class="form-control" style="min-height:100px;"><?php echo $invoice_details['NOTES'] ?></textarea>
                    </div>
                    <div class="clear mt-20"></div>

                    <div class="caption" style="width:100px;"></div>
                    <div class="field" style="width:760px;">
                        <button class="btn btn-primary pull-right" id="save_invoice"><?php echo ($invoice_id > 0)?"Update":"Save"; ?></button>
<?php
                    if($invoice_id > 0){
?>
                        <a target="_blank" href="purchase-invoice-print.php?id=<?php echo $invoice_id; ?>"><button class="btn btn-default pull-right mr-10"> <i class="fa fa-print"></i> Print </button></a>
<?php
                    }
?>
                        <a href="invoice-purchase-details.php"><button class="btn btn-default pull-left">New Bill</button></a>
                    </div>

                </div> <!--End bodyTab1-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
