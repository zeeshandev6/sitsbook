<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/gate_pass.php');
	include('common/classes/gate_pass_details.php');

	//Permission
  if(!in_array('gate-pass',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objGatePass 				= new GatePass();
	$objGatePassDetails = new GatePassDetails();

	$total 					= $objConfigs->get_config('PER_PAGE');

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$gate_pass_list = $objGatePass->search($start,$total);
	$found_records  = $objGatePass->found_records;
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" 												 />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" 												 />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" 											 />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" 												 />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" 												 />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" 											 />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  								   />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 								 />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" 								 />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" 						 />
				<link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" 								 />
				<link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" 			 />
				<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

				<style media="screen">
					table th,table td{
						padding: 10px;
					}
				</style>

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Gate Pass Management</p>
                  		<span id = "tabPanel">
                     		<div class = "tabPanel">
                        		<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
														<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        		<a   href="gate-pass-details.php"><div class="tab">New</div></a>
                     		</div>
                  		</span>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
               		<div class="clear"></div>
               		<div id = "bodyTab1">
                        <table style="margin:0 50px;width:90%; margin-top:10px" class="table_pagination">
                            <thead>
                                <tr>
                                   	<th width="5%" style="text-align:center">Sr#</th>
																		<th width="10%" style="text-align:center">GP #</th>
																		<th width="10%" style="text-align:center">EntryDate</th>
																		<th width="10%" style="text-align:center">Lot #</th>
                                   	<th width="10%" style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="body_t">
<?php
						if(isset($gate_pass_list)){
							$counter = 1;
							while($gate_pass_row = mysql_fetch_array($gate_pass_list)){
?>
		                            <tr id="recordPanel">
		                                <td style="text-align:center;"><?php echo $counter; ?></td>
																		<td style="text-align:center;"><?php echo $gate_pass_row['GP_NO']; ?></td>
		                                <td style="text-align:center;"><?php echo date('d-m-Y',strtotime($gate_pass_row['ENTRY_DATE'])); ?></td>
																		<td style="text-align:center;"><?php echo $gate_pass_row['LOT_NO']; ?></td>
		                                <td class="appendable" style="text-align:center;position:relative;">
		                                	<a id="view_button"  href="gate-pass-details.php?id=<?php echo $gate_pass_row['ID']; ?>"><i class="fa fa-pencil"></i></a>
																			<a type="button" do="<?php echo $gate_pass_row['ID']; ?>" onClick="delete_row(this);" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
		                            		</td>
		                            </tr>
<?php
							$counter++;
							}
						}
?>
                            </tbody>
                        </table>
												<div class="col-xs-12 text-center">
													<?php
													if($found_records > $total){
														$get_url = "";
														foreach($_GET as $key => $value){
															$get_url .= ($key == 'page')?"":"&".$key."=".$value;
														}
														?>
														<nav>
															<ul class="pagination">
																<?php
																$count = $found_records;
																$total_pages = ceil($count/$total);
																$i = 1;
																$thisFileName = basename($_SERVER['PHP_SELF']);
																if(isset($this_page) && $this_page>0){
																	?>
																	<li>
																		<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
																	</li>
																	<?php
																}
																if(isset($this_page) && $this_page>=1){
																	$prev = $this_page;
																	?>
																	<li>
																		<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
																	</li>
																	<?php
																}
																$this_page_act = $this_page;
																$this_page_act++;
																while($total_pages>=$i){
																	$left = $this_page_act-5;
																	$right = $this_page_act+5;
																	if($left<=$i && $i<=$right){
																		$current_page = ($i == $this_page_act)?"active":"";
																		?>
																		<li class="<?php echo $current_page; ?>">
																			<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
																		</li>
																		<?php
																	}
																	$i++;
																}
																$this_page++;
																if(isset($this_page) && $this_page<$total_pages){
																	$next = $this_page;
																	?>
																	<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
																	<?php
																}
																if(isset($this_page) && $this_page<$total_pages){
																	?>
																	<li>
																		<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
																	</li>
																</ul>
															</nav>
															<?php
														}
													}
													?>
												</div>
                        <div style = "clear:both; height:20px"></div>
               		</div><!-- End #bodyTab1 -->

               		<div id = "bodyTab2" style="display:none">
                  		<div id = "form">
                     	<form method="post" action="">
                            <div class="caption">Title</div>
                            <div class="field">
                               <input name="accTitle" type="text" class="form-control" />
                            </div>
                            <div style="clear:both; height:5px;"></div>
                            <div class="caption"></div>
                            <div class="field">
                               <input name="customerSearch" type="button" value="Search" class="btn btn-default" />
                            </div>
                            <div class="clear"></div>
                     	</form>
                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab2-->
            	</div><!-- End summer -->
         	</div><!-- End .content-box -->
      	</div><!--body-wrapper-->
        <div id="xfade"></div>
        <div id="fade"></div>
   	</body>
    <script>
		$(document).ready(function() {
			$(document).keyup(function(e){
				if(e.keyCode==27){
					$("#popUpDel,#xfade").fadeOut();
				}
			});
			$("#tab1,#tab2").click(function(){
				$(".h3_php_error").show();
			});
			$(".getAccTitle").keyup(function(){
				var codeCode = $(this).val();
				$.post('db/get-customer-title.php',{title:codeCode},function(data){
					var gotCode = $.parseJSON(data);
					var titles = [];
					$.each(gotCode,function(index,value){
						$.each(value,function(i,v){
							if(i=='CUST_ACC_TITLE'){
								titles.push(v);
							}
						});
					});
					$(".getAccTitle").autocomplete({
							source: titles,
							width: 300,
							scrollHeight:220,
							matchContains: true,
							selectFirst: false,
							minLength: 1
					});
					$.each(titles,function(i,v){
						if(codeCode==v){
							$(".insertAccCode").val(gotCode[0]['CUST_ACC_CODE']);
						}else{
							$(".insertAccCode").val("");
						}
					});
				});
			});
		});
	</script>
    <script>
    	var delete_row = function(clickedDel){
    		clickedDel = $(clickedDel);
			var acc_code = clickedDel.attr("do");
			$("#fade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#popUpDel").centerThisDiv();
			$("#fade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".dodelete").click(function(){
					$.get('db/del-account-code.php', {acc_code : acc_code}, function(data){
						data = $.parseJSON(data);
						$("#popUpDel").children(".confirm").text(data['MSG']);
						$("#popUpDel").children(".dodelete").hide();
						$("#popUpDel").children(".nodelete").text("Close");
						if(data['OK'] == 'Y'){
							clickedDel.parent('td').parent('tr').remove();
						}
					});
				});
			$(".nodelete").click(function(){
				$("#fade").fadeOut();
				$("#popUpDel").fadeOut();
			});
			$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#fade").fadeOut('fast');
			});
		};
<?php
	if(isset($_GET['tab']) && $_GET['tab'] == 'list'){
?>
		tab('1', '1', '2');
<?php
	}elseif(isset($_GET['tab']) && $_GET['tab'] == 'search'){
?>
		tab('2', '1', '2');
<?php
	}
?>
	</script>
</html>
<?php include('conn.close.php'); ?>
