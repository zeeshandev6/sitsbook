<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/mobile-sale-return.php');
	include('common/classes/mobile-sale-return-details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/accounts.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('open-mobile-sale-return',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts 								= new ChartOfAccounts();
	$objMobilePurchaseDetails 	= new ScanPurchaseDetails();
	$objMobileSale 							= new ScanSale();
	$objMobileSaleDetails 			= new ScanSaleDetails();
	$objMobileSaleReturn 				= new ScanSaleReturn();
	$objMobileSaleReturnDetails = new ScanSaleReturnDetails();
	$objItems 									= new Items();
	$objItemCategory 						= new itemCategory();
	$objTaxRates 								= new TaxRates();
	$objDepartments 						= new Departments();

	

	$departmentList  = $objDepartments->getList();
	$taxRateList     = $objTaxRates->getList();
	$ms_id   			= 0;
	$mr_id 				= 0;
	$inventory   	= NULL;
	$items_array = array();
	$ref_url = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'mobile-sale.php';
	if(isset($_GET['sale'])){
    $sale_id      = (int)$_GET['sale'];
    $inventory    = $objMobileSale->getDetail($sale_id);
  }

?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
    	.wel_made td{
    		text-align:  center !important;
    	}
    	.panel{
    		padding: 0px;
    	}
    	hr{
    		margin: 10px;
    	}
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/open.mobile.sale.return.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Scan Sale Return Panel</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="mobile-sale.php?tab=list?open"><div class="tab">List</div></a>
                        <a href="mobile-sale.php?open&tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Return</div>
                        <a href="mobile-sale-details.php"><div class="tab">Sale</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div id="form" style="margin: 20px auto;">
<?php
					if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
?>
                    	<div class="sales-bill" style="display:none;">
	                    	<div class="title">Sale Details </div>
	                        <div class="panel panel-default">
	                        	<div class="panel-heading">
	                        		<div class="caption_simple pull-left">Sale Date :   <?php echo date("d-m-Y",strtotime($inventory['SALE_DATE'])); ?></div>
	                        		<div class="caption_simple pull-left">Supplier :    <?php echo $objAccounts->getAccountTitleByCode($inventory['CUST_ACC_CODE']) ?></div>
	                        		<div class="caption_simple pull-left">Bill Number : <?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?></div>
	                        		<input type="hidden" class="this_supplier" value="<?php echo $inventory['CUST_ACC_CODE']; ?>" />
	                        		<input type="hidden" class="this_bill_num" value="<?php echo $inventory['BILL_NO']; ?>" />
	                            	<div class="clearfix"></div>
	                        	</div>
								<input type="hidden" class="ms_id" value="<?php echo $ms_id; ?>" />
<?php
							$price_total = 0;
?>
		                        <table class="wel_made purch_table">
			                        <thead>
			                            <tr>
			                               <th width="20%"  style="font-size:12px;font-weight:normal;text-align:center">Barcode</th>
			                               <th width="20%"  style="font-size:12px;font-weight:normal;text-align:center">Item Name</th>
			                               <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Price</th>
			                            </tr>
			                        </thead>
			                        <tbody>
			<?php
									while($invRow = mysql_fetch_array($inventoryDetails)){
										$mobile_purchase_details = $objMobilePurchaseDetails->getDetails($invRow['SP_DETAIL_ID']);
										$itemName = $objItems->getItemTitle($mobile_purchase_details['ITEM_ID']);
										$items_array[$mobile_purchase_details['ITEM_ID']] = $objItems->getRecordDetails($mobile_purchase_details['ITEM_ID']);
			?>
			                            <tr class="alt-row <?php echo ($mobile_purchase_details['STOCK_STATUS']!='S')?"redRow":""; ?>"  data-row-id='<?php echo $mobile_purchase_details['ID']; ?>'>
			                            	<td style="text-align:center;" data-purchase-barcode="<?php echo ($mobile_purchase_details['STOCK_STATUS']=='S')?$mobile_purchase_details['BARCODE']:"" ?>"><?php echo $mobile_purchase_details['BARCODE'] ?></td>
			                                <td style="text-align:left;" class="itemName" data-item='<?php echo $mobile_purchase_details['ITEM_ID']; ?>'><?php echo $itemName; ?><?php echo ($mobile_purchase_details['COLOUR'] == '')?"":" - ".$mobile_purchase_details['COLOUR']; ?></td>
			                                <td style="text-align:center;" class="purchase-price"><?php echo $invRow['SALE_PRICE'] ?></td>
			                            </tr>
			<?php
										$price_total += $invRow['SALE_PRICE'];
									}
			?>
			                            <tr class="totals">
		                                	<td style="text-align:right !important;background-color:#EEEEEE;" colspan="2"><b>Total</b></td>
		                                    <td style="text-align:center;background-color:#f5f5f5;"><b><?php echo number_format($price_total,2,'.',''); ?></b></td>
		                                </tr>
			                        </tbody>
		                    	</table>
	                    	</div>
	                    </div>
<?php
					}
?>
						<div class="title">Sale Return Panel</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="caption_simple pull-left">
									Return Date
									<input type="text" name="rDate" value="<?php echo date('d-m-Y'); ?>" class="datepicker input_ghost" style="width:150px;padding-left: 40px;margin-left: -30px;" />
								</div>
								<div class="col-md-2"></div>
								<div class="form-group text-center" style="margin-bottom: 0px;">
									<label class="form-control-label">Scanner : </label>
									<input class="form-control text-center" id="scanner-field" style="width: 400px;" autofocus />
								</div>
		                        <div class="clear"></div>
							</div>
							<div>
								<table class="wel_made purch_table">
		                            <thead>
		                                <tr>
											<th width="5%"  style="text-center" >Invoice #</th>
											<th width="10%"  style="text-center">SaleDate</th>
											<th width="15%"  style="text-center">Barcode</th>
											<th width="25%"  style="text-center">Item Name</th>
											<th width="15%"  style="text-center">Price</th>
											<th width="5%"   style="text-center">Action</th>
		                                </tr>
		                            </thead>
		                            <tbody class="tbody-for-rows"></tbody>
		                            <tfoot>

		                            </tfoot>
		                    	</table>
							</div>
                        </div>
                        <div class="underTheTable">
			                <div class="button pull-right saveSale">Save</div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.supplierSelector').selectpicker();
		$('.itemSelector').selectpicker();
		$('.taxRate').selectpicker();
		$(".billSelector").selectpicker();
		$("tr.redRow td").css({"background-color":"rgba(255,0,0,0.1)"});
		$(document).calculateColumnTotals();
		$("input.bill_number").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.supplierSelector button");
		$("div.supplierSelector button").keyup(function(e){
			var supp = $("select.supplierSelector option:selected").val();
			if(e.keyCode == 13 && supp != ''){
				$(".bill_number").focus();
			}
		});
		$("input.bill_number").setFocusTo("div.itemSelector button");
		$('div.itemSelector button').keyup(function(e){
			if(e.keyCode ==13 && $("select.itemSelector option:selected").val() != ''){
				$(this).getItemReturnDetails();
				$("input.quantity").focus();
			}
		});
		$("input.quantity").on('keyup blur',function(e){
			stockOs();
		});
		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13){
				$("input.unitPrice").focus();
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".taxTd").find(".dropdown-toggle").focus();
			}
		});
		$(".taxTd").find(".dropdown-toggle").keyup(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".addDetailRow").focus();
			}
		});
		$(".taxTd").find(".taxRate").change(function(){
			$(this).calculateRowTotal();
			$(".addDetailRow").focus();
		});
		$("#scanner-field").on('change',function(e){
			$(this).quickSave();
		});
		$(".saveSale").click(function(){
			saveSale();
		});
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Bill Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Bill Updated Successfully!');
<?php
		}
?>

</script>
