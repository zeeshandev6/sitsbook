<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/accounts.php';

	$objJournalVoucher  = new JournalVoucher();
	$objChartOfAccounts = new ChartOfAccounts();
	$objUserAccounts    = new UserAccounts();

	//Permission
	if(!in_array('journal-voucher',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$accountsList = $objJournalVoucher->getAccountsList();
	$jvNum 				= $objJournalVoucher->genJvNumber();

	$jid = 0;
	if(isset($_GET['jid'])){
		$jid 								= (int)mysql_real_escape_string($_GET['jid']);
		$voucher_details 		= $objJournalVoucher->getRecordDetailsVoucher($jid);
		if($voucher_details==NULL){
			$jid = 0;
		}
		$transaction_rows 	= $objJournalVoucher->getVoucherDetailList($jid);
	}
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>SIT Solutions</title>
      <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
      <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
      <link rel="stylesheet" href="resource/css/bootstrap.min.css" />
      <link rel="stylesheet" href="resource/css/bootstrap-select.css" />
      <style type="text/css">
					tr td,tr th{
						padding: 5px !important;
						height: auto !important;
					}
      </style>
      <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
      <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
      <script type="text/javascript" src="resource/scripts/configuration.js"></script>
      <script type="text/javascript" src="resource/scripts/ledger.configuration.js"></script>
      <script type="text/javascript" src="resource/scripts/tab.js"></script>
      <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
      <script type="text/javascript">
				$(function(){
					$("select").selectpicker();
				});
      	var refresh_accounts = function(){
      		$.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(html){
      			var options = $(html).find('select.accCodeSelector').html();
      			$("select.accCodeSelector").html(options);
      			$("select.accCodeSelector").selectpicker("refresh");
      		});
      	};
				var edit_row = function(elm){
					var acc_code,narration,amount_dr,amount_cr;
					$(".updateMode").removeClass("updateMode");
					$(elm).parent().parent().addClass("updateMode");
					acc_code  = $(elm).parent().siblings('td.accCode').text();
					narration = $(elm).parent().siblings('td.narration').text();
					amount_dr = $(elm).parent().siblings('td.debitColumn').text().replace(',','');
					amount_cr = $(elm).parent().siblings('td.creditColumn').text().replace(',','');

					$("select.accCodeSelector option[value='"+acc_code+"']").prop("selected",true).parent().selectpicker('refresh');
					$("input.narration").val(narration);
					$("input.debit").val(amount_dr);
					$("input.credit").val(amount_cr);
					$("div.accCodeSelector").focus();
				};
      </script>
   </head>
   <body>
      <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div><!-- End #sidebar -->
      	<div id = "bodyWrapper">
        	<div class = "content-box-top" style="overflow:visible;">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Journal Voucher</p>
                        <div class="clear"></div>
               		</div><!-- End .content-box-header -->
              		<div id="bodyTab1">
                      <div id="form">
                            <div class = "caption" style="width:100px;">JV#</div>
                            <div class = "field_1">
                               <input class="form-control" name="jvNum" type="text" value="<?php echo (isset($voucher_details))?$voucher_details['VOUCHER_NO']:$jvNum; ?>" />
                            </div>
                            <div class="caption" style="width:210px;">Date</div>
                            <div class="field_1">
                               <input class="form-control datepicker" type="text" name="jvDate" value="<?php echo (isset($voucher_details))?date('d-m-Y',strtotime($voucher_details['VOUCHER_DATE'])):""; ?>" />
                            </div>
                            <div class = "caption" style="width:100px;display: none;">Reference</div>
                            <div class = "field_1" style="width:150px;">
                               <input class="form-control" style="display: none;" type="text" name="jvReference" value="<?php echo (isset($voucher_details))?$voucher_details['REFERENCE']:""; ?>" />
                            </div>
                            <div class="clear"></div>
<?php
			if($distSalesAddon == 'Y'){
                      $sales_man_arr = array();
                      $users_list = $objUserAccounts->getActiveList();
                      if(mysql_num_rows($users_list)){
                        while($user = mysql_fetch_assoc($users_list)){
                          if($user['DESIGNATION_TYPE'] == ''){
                            //$user['DESIGNATION_TYPE'] = 'S';
                            continue;
                          }
                          if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
                            $sales_man_arr[$user['DESIGNATION_TYPE']] = array();
                          }
                          $sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
                        }
                      }
 ?>
			                      <div class="caption" style="width:100px;">OrderTaker</div>
			                      <div class="field" style="width:270px;position:relative;">
			                        <select class="order_taker" name="order_taker_id">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['O'])){
			                            foreach ($sales_man_arr['O'] as $key => $user){
			                                $user_selected = (isset($voucher_details)&&$voucher_details['ORDER_TAKER_ID']==$user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>

			                      <div class="caption" style="width:100px;">Salesman</div>
			                      <div class="field" style="width:270px;position:relative;">
			                        <select class="user_id" name="salesman_id">
			                          <option value=""></option>
			                          <?php
			                          if(isset($sales_man_arr['S'])){
			                            foreach ($sales_man_arr['S'] as $key => $user){
			                                $user_selected = (isset($voucher_details)&&$voucher_details['SALESMAN_ID']==$user['ID'])?"selected":"";
			                                ?>
			                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
			                                <?php
			                            }
			                          }
			                          ?>
			                        </select>
			                      </div>
			                      <div class="clear"></div>
														<hr />

<?php } ?>

                            <div class="panel panel-default"  style="width:100%;margin:0px auto;">
                                  <table cellspacing="0" style="width:100%;">
																		<thead>
                                        <th class="bg-color text-center col-xs-3" style="vertical-align:middle;" colspan="2">
                                        	G/L Account
                                        	<a href="#" onclick="refresh_accounts();" class="btn btn-default btn-xs" ><i class="fa fa-refresh" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                        	<a href="accounts-management.php" target="_blank" class="btn btn-default btn-xs" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                        </th>
                                        <th class="bg-color col-xs-6">Narration</th>
                                        <th class="bg-color col-xs-1">Debit</th>
                                        <th class="bg-color col-xs-1">Credit</th>
                                        <th class="bg-color col-xs-1">Action</th>
                                    </thead>
                                    <tbody id="d">
                                        <tr class="entry_row">
                                            <td colspan="2">
                                                <select class="accCodeSelector show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                                                   <option selected value="0"></option>
<?php
											if(mysql_num_rows($accountsList)){
												while($account = mysql_fetch_array($accountsList)){
?>
                                                   <option value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
<?php
												}
											}
?>
                                                </select>
                                            </td>
                                            <td  ><input type="text" class="form-control narration" /></td>
                                            <td  ><input type="text" transactionType="Dr" class="form-control debit" style="text-align:right;color: #036;" /></td>
                                            <td  ><input type="text" transactionType="Cr" class="form-control credit" style="text-align:right;color: #036;" /></td>
                                            <td  ><input type="button" class="btn btn-default cdr_btn btn-block" value="Enter" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="display:none;height:15px;padding:1px;padding-left:10px;font-size:14px;" class="insertAccTitle"></td>
                                        </tr>
                                    </tbody>
                                     <tbody>
                                      <tr class="transactions" style="display:none;"></tr>
            <?php
                        $debit_total = 0;
                        $credit_total= 0;
                        if(isset($transaction_rows) && mysql_num_rows($transaction_rows)){
                          while($detailRow = mysql_fetch_array($transaction_rows)){
            ?>
                                        <tr class="transactions amountRow" row-id="<?php echo $detailRow['VOUCH_DETAIL_ID'] ?>">
                                          <td style="text-align:right;" class='accCode'><?php echo $detailRow['ACCOUNT_CODE'] ?></td>
                                            <td style="text-align:left;" class='accTitle'><?php echo $detailRow['ACCOUNT_TITLE'] ?></td>
                                            <td style="text-align:left;" class='narration' ><?php echo $detailRow['NARRATION'] ?></td>
                                            <td style="text-align:center;" class="debitColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Dr')?number_format($detailRow['AMOUNT'],2,'.',''):""; ?></td>
                                            <td style="text-align:center;"  class="creditColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Cr')?number_format($detailRow['AMOUNT'],2,'.',''):""; ?></td>
                                            <td style="text-align:center;">
																							<a onclick="edit_row(this);" id="view_button"><i class="fa fa-pencil"></i></a>
																							<a class="pointer" onClick="jvFunctions.deleteRow(this);"><i class="fa fa-times"></i></a>
																						</td>
                                        </tr>
            <?php
                            $debit_total += ($detailRow['TRANSACTION_TYPE'] == 'Dr')?$detailRow['AMOUNT']:0;
                            $credit_total+= ($detailRow['TRANSACTION_TYPE'] == 'Cr')?$detailRow['AMOUNT']:0;
                          }
                        }
            ?>
                          <tr style="background-color:#F8F8F8;height: 30px;">
                                          <td colspan="3" style="text-align:center;"> Total/Difference </td>
                                            <td class="debitTotal" style="text-align:center;color:#042377;" title="Debit"><?php echo number_format($debit_total,2,'.',''); ?></td>
                                            <td class="creditTotal" style="text-align:center;color:#042377;" title="Credit"><?php echo number_format($credit_total,2,'.',''); ?></td>
                                            <td class="drCrDiffer" style="text-align:center;color:#042377;" title="Diffrence"></td>
                                        </tr>
                                     </tbody>
                                  </table>
                                  <div class="underTheTable pull-right" style="padding:10px;margin:10px;">
                                      <input type="hidden" value="<?php echo $jid; ?>" class="jVoucher_id" />
                                      <?php
                                        if(isset($_GET['jid'])){
                                      ?>
                                        <a class="button ml-10 pull-right" target="_blank" href="voucher-print.php?id=<?php echo $jid; ?> "><i class="fa fa-print"></i> Print </a>
                                      <?php
						                            }
						                          ?>
																			<button class="button pull-right save_voucher"><?php echo (isset($_GET['jid']))?"Update":"Save"; ?></button>
																			<input type="button" class="button pull-right mr-10" onclick="window.location.href = 'create-voucher.php';" value="New Voucher" >
                                  </div>
                                  <div class="clear"></div>
                        </div><!--content-box-content-->
                      </div><!--form-->
						<div style="clear:both;"></div>
               		</div>
            	</div>     <!-- End summer -->
         	</div>   <!-- End .content-box-top -->
	</div>  <!--body-wrapper-->
</div>  <!--body-wrapper-->
<div id="fade"></div>
<div id="xfade"></div>
<div id="popUpForm"></div><!--popUpForm-->
</body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
	$(window).load(function(){
		$('.accCodeSelector').selectpicker();
		$("div.accCodeSelector button").focus();
		$(".narration").setFocusTo(".debit");
		$(".debit").setFocusToIfVal(".credit");
		$(".narration").escFocusTo("button.dropdown-toggle");
		$(".debit").escFocusTo(".narration");
		$(".credit").escFocusTo(".debit");
		$(".debit").keyup(function(e){
			if(e.keyCode==13){
				$(this).quickSubmitDrCr();
			}
		});
		$(".credit").keyup(function(e){
			if(e.keyCode==13){
				$(this).quickSubmitDrCr();
			}
		});
        $(".cdr_btn").on('click',function(e){
        $(this).quickSubmitDrCr();
        });
		$("div.accCodeSelector button").keyup(function(e){
            if(e.keyCode == 13){
                if($("select.accCodeSelector option:selected").val() != 0){
                    var account_code = $("select.accCodeSelector option:selected").val();
                    getAccountBalance(account_code);
                    $(".insertAccTitle").fadeIn();
                    $(".narration").focus();
                }
            }
        });
		$(".save_voucher").click(function(){
			$(this).saveJournal();
		});
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                $(".save_voucher").click();
                return false;
            }
        });
		$("#datepicker").setFocusTo("div.accCodeSelector button");
		$(document).showDifference();
		$(".deleteVoucher").click(function(){
			$(this).deleteVoucher();
		});

    });
</script>
