
<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/sale.php';
	include 'common/classes/sale_details.php';
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/mobile-purchase-details.php');
	include 'common/classes/customers.php';
	include 'common/classes/items.php';
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	$objCompanyDetails = new CompanyDetails;
	$objSales 		   = new Sale;
	$objSaleDetails    = new SaleDetails;
	$objScanSale   			= new ScanSale();
	$objScanSaleDetails 	= new ScanSaleDetails();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objCustomers      = new Customers;
	$objItems  	       = new Items;
	$objJournalVoucher = new JournalVoucher;
	$objConfigs   	   = new Configs;

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_noteimg= $objConfigs->get_config('INVOICE_NOTE_IMG');
	$invoice_notetxt= $objConfigs->get_config('INVOICE_NOTE_TXT');
	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
    $use_taxes           = ($use_taxes=='Y')?true:false;

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
    $individual_discount = ($individual_discount=='Y')?true:false;

	$invoice_format = explode('_', $invoice_format);
	if(isset($_GET['id'])){
		$sale_id = mysql_real_escape_string($_GET['id']);
		$saleDetails = $objScanSale->getDetail($sale_id);
		$saleDetailList = $objScanSaleDetails->getList($sale_id);
		$customer_balance = $objJournalVoucher->getInvoiceBalance($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID']);
		$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
	}
	$invoice_num 		 = $invoice_format[1];
?>
<!DOCTYPE html  
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/smallInvoiceStyle.css" type="text/css"  />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printAble").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/smallInvoiceStyle.css",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 400,
			      header: null
			  });
			});
			$(".printThis").click();
			$(window).keyup(function(e){
				if(e.keyCode == 27){
					$(".cancel-button").click();
				}
			});
		});
	</script>
</head>
<body>
<?php
	$balance_os_show = true;
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE']  = "M/s :  CASH SALE ";
			$customer['CUST_ACC_TITLE'] .= ($saleDetails['CUSTOMER_NAME']=='')?"":" - ";
			$customer['CUST_ACC_TITLE']	.=  $saleDetails['CUSTOMER_NAME'];
			$balance_os_show = false;
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
			$customer['CUST_ACC_TITLE'] = "M/s : ".$customer['CUST_ACC_TITLE'];
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$bill_discount = $saleDetails['SALE_DISCOUNT'];
		$total_tax_amount = $saleDetails['SALE_TAX'];
		$companyLogo   = $objCompanyDetails->getLogo($session_branch_id);
		$company  = $objCompanyDetails->getRecordDetails($session_branch_id); 
?>
<div class="invoiceReady panel panel-default">
	<div class="header">
    	<div class="headerWrapper">
            <button class="btn btn-default btn-sm printThis"  title="Print"> <span class="glyphicon glyphicon-print"></span> Print </button>
            <button class="btn btn-default btn-sm cancel-button" onclick="window.close();" title="Print"> <span class="glyphicon glyphicon-remove"></span> Cancel </button>
        </div><!--headerWrapper-->
    </div><!--header-->
    <div class="clear"></div>
    <div class="printAble smallInvoiceSize">
            <div id="form">
            	<div class="title_top q-font" style="font-size:1.5em;font-weight:bold;padding-bottom:10px;"><?php echo $company['NAME']; ?></div>
                <div class="title_top" style="font-size:0.7em;"><?php echo $company['ADDRESS']; ?></div>
                <div class="title_top" style="font-size:0.7em;"><?php echo $company['CONTACT']; ?></div>
                <div class="clear" style="margin-top:0.5em !important;"></div>
                <div class="leftCaption">Counter : <?php echo $fullName['FIRST_NAME']." ".$fullName['LAST_NAME']; ?></div>
                <div class="leftCaption pull-right">Date : <?php echo $sale_date; ?></div>
                <div class="clear"></div>
                <div class="leftCaption pull-right">Time : <?php echo date('h:i A',strtotime($saleDetails['RECORD_TIME'])); ?></div>
                <div class="clear"></div>
                <div class="leftCaption"><?php echo $customer['CUST_ACC_TITLE']; ?></div>
                <div class="leftCaption pull-right">Bill # <?php echo $saleDetails['BILL_NO']; ?></div>
                <div class="clear"></div>
                
                <table class="invoiceTable">
                    <thead>
                        <tr>
                        	<th width="5%" style="text-align:center;">Sr.</th>
                            <th width="65%" style="text-align:left;padding-left:5px;">Items</th>
                            <th width="30%" style="text-align:right;padding-right:5px;">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($saleDetailList)){
							while($row = mysql_fetch_array($saleDetailList)){
								$pd_row   = $objScanPurchaseDetails->getDetails($row['SP_DETAIL_ID']);
								$itemName = $objItems->getItemTitle($pd_row['ITEM_ID']);

?>
								<tr>
									<td class="text-center"><?php echo $counter; ?></td>
									<td style="font-size:12px;"><span style="float:left;margin-left:5px;"><?php echo $itemName; ?></span></td>
                                    <td><span style="float:right;margin-right:5px;"><?php echo number_format($row['SALE_PRICE'],2); ?></span></td>
								</tr>
<?php

									$totalAmount += $row['SALE_PRICE'];
									$counter++;
							}
						}
?>
                    </tbody>
                    <tfoot>
                        <tr>
                        	<td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Bill Total</span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($totalAmount,2); ?></span></td>
                        </tr>
                        <?php if($individual_discount){ ?>
                        <tr>
                            <td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Discount</span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($bill_discount,2); ?></span></td>
                        </tr>
                        <?php } ?>
                        <?php if($use_taxes){ ?>
                        <tr>
                        	<td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Tax</span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($total_tax_amount,2); ?></span></td>
                        </tr>
                        <?php } ?>
                        <tr>
                        	<td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Cash Payment</span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['RECEIVED_CASH'],2); ?></span></td>
                        </tr>

                        <?php
                        	if($saleDetails['CHANGE_RETURNED'] > 0){
                        ?>
                        <tr>
                        	<td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Change Return </span></td>
                            <td><span style="float:right;margin-right:5px;"><?php echo number_format($saleDetails['CHANGE_RETURNED'],2); ?></span></td>
                        </tr>
                        <?php
                        	}elseif(substr($saleDetails['CUST_ACC_CODE'], 0,6) != '010101'){
                        ?>
                        <tr>
                        	<td></td>
                            <td style="text-align:right;border:none !important;"><span style="float:right;margin-right:15px;">Balance <?php echo $cutomer_balance_array['TYPE']=='DR'?"O/s":"Payable"; ?> </span></td>
                            <td><span style="float:right;margin-right:5px;">  <?php echo number_format($cutomer_balance_array['BALANCE'],2); ?></span></td>
                        </tr>
                        <?php
                        	}
                        ?>

                    </tfoot>
                </table>
                <div class="invoice_footer">
	                <div class="clear" style="height: 10px;"></div>

	                <div class="invoice-footer-text" style="height:auto !important;">
		                <?php echo ($invoice_notetxt == '')?'':$invoice_notetxt; ?>
		                <?php if(is_file("uploads/".$invoice_noteimg)){ ?>
	                		<img src="<?php echo "uploads/".$invoice_noteimg; ?>" style="width:100%;" />
	                	<?php } ?>
                	</div>
                	<div class="clear" style="height: 0.6em;"></div>
                	<img src="lib/barcode.php?size=30&text=<?php echo $saleDetails['BILL_NO']; ?>" style="width:auto;height:30px;" />
	                <div class="invoice-developer-info">Developed by SITSOL  +923207448888 </div>
	                <div class="invoice-developer-info"></div>
	        	</div>
                <div class="clear"></div>
            </div><!--form-->
	</div><!--docReportSize-->
<?php
	}
?>
</div><!--invoiceBody-->
</body>
</html>
<?php include('conn.close.php'); ?>