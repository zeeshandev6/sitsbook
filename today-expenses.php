<?php
	include("common/connection.php");
	include 'common/config.php';
	include("common/classes/daily-expenditure-management.php");
	include("common/classes/accounts-code-management.php");
	
	$objDailyExpenditure 	= new DailyExpenditure;
	$objAccCode 			= new AccountCodeManagement; 
	
	$today = date('d-m-Y');
	
	$getExpenseList = $objDailyExpenditure->expenseList();
	
?><!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		td{
			font-size: 16px !important;
		}
	</style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
</head>
  
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>    
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Todays Expenses</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
            
                <div id="bodyTab1" style="display:block;">
                	<div class="printPage">
                        <div class="heading" style="background-color:rgba(90,90,90,1.0);">Posted Entries Dated :  <?php echo date('d-m-Y'); ?></div>
                        <div style="height: 20px;"></div>
                        <table style="width:90%">
                                <thead>
                                    <tr>
                                        <th width="5%">Sr#</th>
                                        <th>Date</th>
                                        <th>Account Title</th>
                                        <th>Detail</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
	if ($getExpenseList != NULL && mysql_num_rows($getExpenseList)){
		$counter = 1;
		$expense_total = 0;
		while($expenseList = mysql_fetch_array($getExpenseList)){
			$category = $objAccCode->getCategory($expenseList['EXPENSE_CODE']);
?>                                
                                
                                    <tr>
                                        <td style="text-align: center;"><?php echo $counter ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($expenseList['EXPENSE_DATE'])); ?></td>
                                        <td><?php echo $category; ?></td>
                                        <td style="text-align:center"><?php echo $expenseList['DETAIL']; ?></td>
                                        <td style="text-align: center;"><?php echo $expenseList['AMOUNT']; ?></td>
                                    </tr>
<?php
		$counter++;
		$expense_total += $expenseList['AMOUNT'];
		}
	}
?>                                
                                    
                                </tbody>
                                <tfoot>
                                	<tr>
                                    	<td colspan="4" style="text-align: right;">Total</td>
                                        <td style="text-align: center;"><?php echo (isset($expense_total))?$expense_total:0; ?></td>
                                    </tr>
                                </tfoot>
                            </table>
                	</div><!--class="printPage"-->
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>     
            </div> <!-- End .content-box-content -->	
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(function(){
		$(window).scroll(function(){
			$('#sidebar').height($(document).height());
			var topOffsetPos = $(window).scrollTop();
			$(".toggleSideBar").css({'top':5+topOffsetPos});
		});
		$.fn.sideBarFunction = function(){
			if($('#sidebar').css('left')=='0px'){
				$(".toggleSideBar").css({'opacity':'0.6'});
				$('#sidebar').animate({'left':'-230px'},200);
				$(".content-box-top").animate({'left':'10px'},200);
				$(".content-box-body").animate({'left':'10px'},200);
				$(".toggleSideBar").children("i.fa-caret-left").removeClass('fa-caret-left').addClass('fa-caret-right');
			}else{
				$('#sidebar').animate({'left':'0'},200);
				$(".toggleSideBar").css({'opacity':'1.0'});
				$(".toggleSideBar").children("i.fa-caret-right").removeClass('fa-caret-right').addClass('fa-caret-left');
				$(".content-box-top").animate({'left':'240px'},200);
				$(".content-box-body").animate({'left':'240px'},200);
			}
		};
		//Call
		$(".toggleSideBar,.toggleSideBar i").on('click',function(){
			$(this).sideBarFunction();
		});
	});
	$(window).load(function(){
		$('#sidebar').height($(document).height());
	});
</script>