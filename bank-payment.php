<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/j-voucher.php';
	include 'common/classes/accounts.php';
	include('common/classes/customers.php');
	include('common/classes/suppliers.php');
	include('common/classes/sms_templates.php');
	include('common/classes/sitsbook_sms_api.php');

	$objAccounts 				= new ChartOfAccounts();
	$objJournalVoucher  = new JournalVoucher();
	$objCustomers 		 	= new Customers();
	$objSuppliers 		 	= new suppliers();
	$objSmsTemplates    = new SmsTemplates();
	$objSitsbookSmsApi  = new SitsbookSmsApi();

	//Permission
	if(!in_array('bank-payment',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--


	$sms_config 					= $objConfigs->get_config('SMS');

	if(isset($_POST['sms_voucher_id'])){
		if($sms_config == 'N'){
			exit();
		}
		$voucher_id 			= (int)mysql_real_escape_string($_POST['sms_voucher_id']);
		$voucher_date 		= $objJournalVoucher->getVoucherDate($voucher_id);
		$voucher_date     = date("d-m-Y",strtotime($voucher_date));
		$bank_account 		= $objJournalVoucher->getBankAccountFromVoucher($voucher_id);

		$accounts_for_sms =  $objJournalVoucher->getVoucherCustomerSuppliersList($voucher_id);

		$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
		$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');

		$mobile_numbers   = array();
		$mobile_acc_codes = array();

		if(mysql_num_rows($accounts_for_sms)){
			while($acc_row = mysql_fetch_assoc($accounts_for_sms)){
				$mobile = '';
				if(substr($acc_row['ACCOUNT_CODE'],0,6) == '010104'){
					$mobile = $objCustomers->getMobileIfActive($acc_row['ACCOUNT_CODE']);
				}
				if(substr($acc_row['ACCOUNT_CODE'],0,6) == '040101'){
					$mobile = $objSuppliers->getMobileIfActive($acc_row['ACCOUNT_CODE']);
				}
				if($mobile!=''){
					$mobile_numbers[$mobile] 		= $acc_row['AMOUNT'];
					$mobile_acc_codes[$mobile] 	= $acc_row['ACCOUNT_TITLE'];
				}
			}
		}
		$sms_template = $objSmsTemplates->get_template(4);
		foreach($mobile_numbers as $mobile_number => $amount){
			$sms_body = $sms_template['SMS_BODY'];

			$sms_body = str_replace("[DATE]",$voucher_date,$sms_body);
			$sms_body = str_replace("[AMOUNT]",$amount,$sms_body);
			$sms_body = str_replace("[BANK_TITLE]",$bank_account,$sms_body);
			$sms_body = str_replace("[ACCOUNT_TITLE]",$mobile_acc_codes[$mobile_number],$sms_body);

			$no_of_sms = 0;
			$acc_id  	 = 0;
			echo $objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
		}
		exit();
	}

	$newJvNumber = $objJournalVoucher->genJvNumber();
	$newCrNumber = $objJournalVoucher->genTypeNumber('BP');
	$voucher_type = 'BP';
	if(isset($_GET['jid'])){
		$jv_id 			= mysql_real_escape_string($_GET['jid']);
		$jVoucher 	    = $objJournalVoucher->getRecordDetailsVoucher($jv_id);
		$voucher_type   = $jVoucher['VOUCHER_TYPE'];
		$jv_detail_list = $objJournalVoucher->getVoucherDetailList($jv_id);
	}
?>
<!DOCTYPE html 
   >
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/bank.payment.configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
          var refresh_accounts = function(){
            $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(html){
              var options = $(html).find('select.accCodeSelector').html();
              var banks   = $(html).find('select.bank_acc_code').html();
              $("select.accCodeSelector").html(options);
              $("select.accCodeSelector").selectpicker("refresh");
              $("select.bank_acc_code").html(banks);
              $("select.bank_acc_code").selectpicker("refresh");
            });
          };
        </script>
   </head>

   <body>
      <div id = "body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->


      	<div id = "bodyWrapper">
        	<div class = "content-box-top" style="overflow:visible;">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p >Bank Payment Panel</p>
                        <div class="clear"></div>
               		</div><!-- End .content-box-header -->
<?php
			if($voucher_type == 'BP'){
?>
              		<div id = "bodyTab1">
                      <div id = "form">
                           <input type="hidden" name="jv_id" class="jv_id" value="<?php echo isset($jVoucher)?$jv_id:"0"; ?>" />
													 <input type="hidden" class="sms-config" value="<?php echo $sms_config; ?>">
                            <div class = "caption" style="width:100px;">BP#</div>
                            <div class = "field_1">
                               <input class="form-control" name="crNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['TYPE_NO']:$newCrNumber; ?>" style="width:160px;" />
                            </div>
                            <div class = "caption" style="margin-left:0px;width:150px;">Voucher#</div>
                            <div class = "field_1">
                               <input class="form-control" name="jvNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['VOUCHER_NO']:$newJvNumber; ?>" style="width:152px;" />
                            </div>
                            <div class = "caption_1" style="margin-left:0;width:50px;line-height:26px;">Date</div>
                            <div class = "field_1">
                               <input class="form-control datepicker" type="text" name="jvDate" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['VOUCHER_DATE'])):''; ?>" style="width: 150px;" />
                            </div>
                            <div class="clear"></div>
                            <div style="display:none;">
                              <div class = "caption" style="width:100px;">Reference</div>
                              <div class = "field_1" style="width:160px;">
                                 <input class="form-control" style="width:160px;" type="text" name="jvReference" value="<?php echo (isset($jVoucher))?$jVoucher['REFERENCE']:''; ?>" />
                              </div>
                              <div class = "caption" style="margin-left:0px;width:150px;">Reference Date</div>
                              <div class = "field_1" style="width:425px">
                                 <input class="form-control datepicker" type="text" name="refDate" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['REF_DATE'])):''; ?>"  style="width: 150px;" />
  							             </div>
                              <div class="clear"></div>
                            </div>
                            <div class = "caption" style="margin-left:0px;width: 120px;">Bank</div>
                            <div class = "field_1" style="width:475px">
                                <select class="bank_acc_code show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                                  <option value=""></option>
<?php
                            $banksList = $objAccounts->getAccountByCatAccCode('010102');
                            if(mysql_num_rows($banksList)){
                                while($account = mysql_fetch_array($banksList)){
?>
                                        <option value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
							</div>
                            <div class="caption bank_acc_balance" style="width: 200px;text-align:left;"></div>
                            <div class="clear"></div>

                            <div style = "clear:both; height:10px;"></div>
                            <fieldset class="panel panel-default" style="width:80%;margin: 0 auto;">
                            	<!--<span class="legend">Journal Voucher</span>-->
                                <table style="width:100%;" align="left" class="voucherDrCrTable">
                                    <thead>
                                        <th class="bg-color">
                                          <span style="float:left;margin:10px;">G/L Account</span>
                                          <a href="#" onclick="refresh_accounts();" class="btn btn-default pull-right" ><i class="fa fa-refresh" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                          <a href="accounts-management.php" target="_blank" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                                        </th>
                                        <th class="bg-color">Narration</th>
                                        <th class="bg-color" style="text-align:center;">Amount</th>
                                        <th class="bg-color" style="text-align:center;">Action</th>
                                    </thead>
                                    <tbody id="d">
                                        <tr class="entry_row">
                                            <td width="21%" style="padding:2px 2px">
                                                <select class="accCodeSelector show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                                                   <option selected value=""></option>
<?php
											$accountsList = $objAccounts->getLevelFourList();
											if(mysql_num_rows($accountsList)){
												while($account = mysql_fetch_array($accountsList)){
?>
                                                   		<option value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
<?php
												}
											}
?>
                                                </select>
                                            </td>
                                            <td width="41%" style="padding:2px 5px"><input type="text" class="form-control narration" /></td>
                                            <td width="15%" style="padding:2px 5px"><input type="text" class="form-control amount" style="text-align:right;" /></td>
                                            <td width="8%"><input type="button" class="btn btn-default cdr_btn btn-block" value="Enter" /></td>
                                            <input type="hidden" value="" class="accountTitle" />
                                            <input type="hidden" value="" class="accountCode" />
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="display:none;height:15px;padding:1px;padding-left:10px;font-size:14px;" class="insertAccTitle"></td>
                                        </tr>
                                    </tbody>
                            	</table>
                                <div class="clear"></div>
                            </fieldset>
                      </div><!--form-->
                      <div class="clear"></div>
                      <div id="form">
                          <div class="panel panel-default" style="width:80%;margin: 0 auto;">
                            <div class="clear"></div>
                            <table cellspacing = "0" align = "center" style="width:100%;">
                               <thead>
                                  <tr>
                                     <th width = "15%" style="text-align:left" class="bg-color">GL/Account Code</th>
                                     <th width = "15%" style="text-align:left" class="bg-color">A/C Title</th>
                                     <th width = "40%" style="text-align:left" class="bg-color">Narrtion</th>
                                     <th width = "10%" style="text-align:center" class="bg-color">Amount</th>
                                     <th width = "10%" style="text-align:center" class="bg-color">Action</th>
                                  </tr>
                               </thead>

                               <tbody>
                                <tr class="transactions" style="display:none;"></tr>

      <?php
                  if(isset($jv_detail_list) && mysql_num_rows($jv_detail_list)){
                    while($jv_row = mysql_fetch_array($jv_detail_list)){
                      if($jv_row['TRANSACTION_TYPE'] == 'Dr'){
                        continue;
                      }
      ?>
                                  <tr class="transactions amountRow">
                                    <td style="text-align:right;"  class="accCode"><?php echo $jv_row['ACCOUNT_CODE']; ?></td>
                                      <td style="text-align:left;"   class="accTitle"><?php echo $jv_row['ACCOUNT_TITLE']; ?></td>
                                      <td style="text-align:left;"   class="narration"><?php echo $jv_row['NARRATION']; ?></td>
                                      <td style="text-align:center;" class="creditColumn"><?php echo $jv_row['AMOUNT']; ?></td>
                                      <td style="text-align:center;"></td>
                                  </tr>
      <?php
                    }
                  }
      ?>

                    <tr style="background-color:#F8F8F8;height: 30px;">
                                    <td colspan="3" style="text-align:right;"> Total: </td>
                                      <td class="creditTotal" style="text-align:center;color:#BD0A0D;"></td>
                                      <td></td>
                                  </tr>
                               </tbody>
                            </table>
      <?php
          if($voucher_type == 'BP'){
      ?>
                            <div class="underTheTable" style="padding:10px;margin-bottom:10px;">
                                <?php
                                  if(!isset($jVoucher)){
                                ?>
                                  <button class="button pull-right saveJournal">Save</button>
                                <?php
                                  }
                                ?>
                                <button class="button mr-10 pull-right" onclick="window.location.href = '<?php echo basename($_SERVER['PHP_SELF']); ?>';">New Voucher</button>
																<div class="clear"></div>
                            </div>
      <?php
          }
      ?>
                            <div style="clear:both"><input type="hidden" class="cashball" /></div>
                  </div><!--content-box-content-->
                      </div>
               		</div>
<?php
			}
?>
            	</div><!-- End summer -->
         	</div>   <!-- End .content-box-top -->
    </div>  <!--body-wrapper-->
</div>
        <div id="fade"></div>
        <div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>
<script>
	$(document).ready(function(){
		$('select.accCodeSelector').selectpicker();
		$("select.bank_acc_code").selectpicker();
		$("input[name='jvReference']").focus();

		getBankBalance($("select.bank_acc_code option:selected").val(),".bank_acc_balance");

		$(".receiptNum,.amount").numericFloatOnly();

		$("input[name='jvReference']").setFocusTo("input[name='refDate']");
		$("input[name='refDate']").setFocusTo("div.accCodeSelector button");

		$("select.bank_acc_code").change(function(){
			var bank_acc_code = $("select.bank_acc_code option:selected").val();
			getBankBalance(bank_acc_code,".bank_acc_balance");
		});

		$("div.bank_acc_code button").click(function(){
			var bank_acc_code = $("select.bank_acc_code option:selected").val();
			getAccountBalance(bank_acc_code,".bank_acc_balance");
		});

		$("div.bank_acc_code button").keyup(function(e){
			if(e.keyCode == 13){
				var bank_acc_code = $("select.bank_acc_code option:selected").val();
				getAccountBalance(bank_acc_code,".bank_acc_balance");
			}
		});

		$('select.accCodeSelector').change(function(){
			if($(this).val() != ''){
				$(".narration").focus();
			}
			var account_code = $('select.accCodeSelector option:selected').val();
			getAccountBalance(account_code,".insertAccTitle");
		});
		$("div.accCodeSelector button").keyup(function(e){
			if(e.keyCode == 13 && $("select.accCodeSelector option:selected").val() != '' ){
				$(".narration").focus();
			}
		});
		$("input.narration").keyup(function(e){
			if(e.keyCode == 13 && $(this).val() != ''){
				$("input.amount").focus();
			}
		});
        $("input.amount").keyup(function(e){
			if(e.keyCode == 13 && $(this).val() != ''){
				$(this).quickSubmitDrCr();
			}
		});
    $(".cdr_btn").on('click',function(){
      $(this).quickSubmitDrCr();
    });
    $(".cdr_btn").on('keydown',function(e){
      if(e.keyCode == 13){
        $(this).quickSubmitDrCr();
      }
    });
		$(".saveJournal").click(function(){
			$(this).saveJournal();
		});
    $(window).keyup(function(e){
        if(e.altKey == true&&e.keyCode == 83){
            e.preventDefault();
            $("#form").click();
            $(".saveJournal").click();
            return false;
        }
    });
<?php
		if($voucher_type != 'BP'){
?>
			displayMessage("Voucher Type Is Not Supported!");
<?php
		}
?>

    });
</script>
