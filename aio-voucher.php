<?php
  include 'common/connection.php';
  include 'common/config.php';
  include 'common/classes/j-voucher.php';
  include 'common/classes/accounts.php';

  $objJournalVoucher = new JournalVoucher();
  $objAccounts       = new ChartOfAccounts();

  //Permission
  if(!in_array('journal-voucher',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource-sp/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource-sp/css/bootstrap.min.css" type="text/css"  />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

  $accountsList = $objJournalVoucher->getAccountsList();
  $jvNum = $objJournalVoucher->genJvNumber();

  if(isset($_GET['is_new'])){
    $type = mysql_real_escape_string($_GET['is_new']);
    $type_number = $objJournalVoucher->genTypeNumber($type);
    $array = array();

    $array['JVNO'] = $jvNum;
    $array['RVNO'] = $type_number;

    echo json_encode($array);
    exit();
  }

  $jid = 0;
  if(isset($_GET['jid'])){
    $jid = $_GET['jid'];
    $jVoucher = mysql_fetch_assoc($objJournalVoucher->getRecordDetails($jid));
    $jVoucherDetailList = $objJournalVoucher->getVoucherDetailList($jid);
  }
?>
<!DOCTYPE html 
>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>SIT Solutions</title>
  <title>SIT Solutions</title>
  <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
  <link rel="stylesheet" href="resource/css/bootstrap.min.css" />
  <link rel="stylesheet" href="resource/css/bootstrap-select.css" />
  <style type="text/css">
  .bg-color{
    font-family: "Arial";
    height: auto !important;
    padding: 10px 5px !important;
    text-align: center;
    background-color: #FFF !important;
  }
  .panel{
    padding-right: 0px;
  }
  table{
    margin-right: 0px;
  }
  .caption{
    line-height: 30px !important;
  }
  </style>
  <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  <script type="text/javascript" src="resource/scripts/configuration.js"></script>
  <script type="text/javascript" src="resource/scripts/aio.voucher.config.js"></script>
  <script type="text/javascript" src="resource/scripts/tab.js"></script>
  <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
  <script type="text/javascript">
  var refresh_accounts = function(){
    $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(html){
      var options = $(html).find('select.accCodeSelector').html();
      $("select.accCodeSelector").html(options);
      $("select.accCodeSelector").selectpicker("refresh");
    });
  };
  $(document).ready(function(){
    $("select").selectpicker();
    $("input.narration").focus(function(){$(this).select()});
  });
  </script>
</head>
<body>
  <div id = "body-wrapper">
    <div id="sidebar">
      <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
    <div id = "bodyWrapper">
      <div class = "content-box-top" style="overflow:visible;">
        <div class = "summery_body">
          <div class = "content-box-header">
            <p style="font-size:15px;">Journal Voucher</p>
            <div class="clear"></div>
          </div><!-- End .content-box-header -->
          <div id = "bodyTab1">
            <div id = "form">

              <div class="caption" style="width:100px;">Voucher Date</div>
              <div class="field" style="width:150px;">
                <input class="form-control datepicker" type="text" name="jvDate" value="<?php echo (isset($jVoucher))?date('d-m-Y',strtotime($jVoucher['VOUCHER_DATE'])):date('d-m-Y'); ?>" />
              </div>
              <div class = "caption" style="width:100px;">Voucher #</div>
              <div class = "field" style="width:150px;">
                <input class="form-control" name="jvNum" type="text" value="<?php echo (isset($jVoucher))?$jVoucher['VOUCHER_NO']:$jvNum; ?>" />
              </div>

              <div class="clear"></div>

              <div class="caption" style="width:100px;">Voucher Type :</div>
              <div class = "field" style="width:150px;">
                <select name="v_type" class="form-control">
                  <option value="JV" <?php echo (isset($jVoucher) && $jVoucher['VOUCHER_TYPE'] == 'JV')?"selected":""; ?> >Journal</option>
                  <option value="CR" <?php echo (isset($jVoucher) && $jVoucher['VOUCHER_TYPE'] == 'CR')?"selected":""; ?>>Cash Receipt</option>
                  <option value="CP" <?php echo (isset($jVoucher) && $jVoucher['VOUCHER_TYPE'] == 'CP')?"selected":""; ?>>Cash Payment</option>
                  <option value="BR" <?php echo (isset($jVoucher) && $jVoucher['VOUCHER_TYPE'] == 'BR')?"selected":""; ?>>Bank Receipt</option>
                  <option value="BP" <?php echo (isset($jVoucher) && $jVoucher['VOUCHER_TYPE'] == 'BP')?"selected":""; ?>>Bank Payment</option>
                </select>
              </div>
              <div class = "caption" style="width:100px;">PO #:</div>
              <div class = "field" style="width:150px;">
                <input class="form-control" type="text" name="pon" value="<?php echo (isset($jVoucher))?$jVoucher['PO_NO']:""; ?>" />
              </div>
              <div class="clear"></div>
              <div class = "caption" style="width:100px;display: none;">Reference</div>
              <div class = "field_1" style="width:150px;">
                <input class="form-control" style="display: none;" type="text" name="jvReference" value="<?php echo (isset($jVoucher))?$jVoucher['REFERENCE']:""; ?>" />
              </div>

              <div style = "clear:both; height:10px;"></div>
              <fieldset class="panel panel-default" style="width:95%;margin:0px auto;">
                <!--<span class="legend">Journal Voucher</span>-->
                <table style="width:100%;" align="left" class="voucherDrCrTable">
                  <thead>
                    <thead>
                      <th class="bg-color">
                        <span style="float:left;margin:10px;">G/L Account</span>
                        <a href="#" onclick="refresh_accounts();" class="btn btn-default btn-sm pull-right" ><i class="fa fa-refresh" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                        <a href="accounts-management.php" style="margin-right:5px;" target="_blank" class="btn btn-default btn-sm pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                      </th>
                      <th class="bg-color">Narration</th>
                      <th class="bg-color" style="text-align:center;">Debit</th>
                      <th class="bg-color" style="text-align:center;">Credit</th>
                    </thead>
                    <tbody id="d">
                      <tr class="entry_row">
                        <td width="21%">
                          <select class="accCodeSelector show-tick form-control" data-style="btn-default" data-live-search="true" style="border:none">
                            <option selected value="0"></option>
                            <?php
                            if(mysql_num_rows($accountsList)){
                              while($account = mysql_fetch_assoc($accountsList)){
                                ?>
                                <option value="<?php echo $account['ACC_CODE']; ?>" data-subtext='<?php echo $account['ACC_CODE']; ?>' ><?php echo $account['ACC_TITLE']; ?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                        </td>
                        <td width="49%" ><input type="text" class="form-control narration" /></td>
                        <td width="15%" ><input type="text" transactionType="Dr" class="form-control debit" style="text-align:right;color: #036;" /></td>
                        <td width="15%" ><input type="text" transactionType="Cr" class="form-control credit" style="text-align:right;color: #036;" /></td>
                      </tr>
                      <tr>
                        <td colspan="4" style="display:none;height:15px;padding:1px;padding-left:10px;font-size:14px;" class="insertAccTitle"></td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="clear"></div>
                </fieldset>
                <div style = "clear:both;height:10px;"></div>
                <div class="panel panel-default"  style="width:95%;margin:0px auto;">
                  <table cellspacing="0" style="width:100%;">
                    <thead>
                      <tr>
                        <th width = "15%" align="left" class="bg-color">GL/Account Code</th>
                        <th width = "15%" align="left" class="bg-color">A/C Title</th>
                        <th width = "40%" align="left" class="bg-color">Narrtion</th>
                        <th width = "10%" style="text-align:center" class="bg-color">Debit</th>
                        <th width = "10%" style="text-align:center" class="bg-color">Credit</th>
                        <th width = "10%" class="bg-color">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="transactions" style="display:none;"></tr>
                      <?php
                      $debit_total = 0;
                      $credit_total= 0;
                      if(isset($jVoucherDetailList) && mysql_num_rows($jVoucherDetailList)){
                        while($detailRow = mysql_fetch_assoc($jVoucherDetailList)){
                          ?>
                          <tr class="transactions amountRow" row-id="<?php echo $detailRow['VOUCH_DETAIL_ID'] ?>">
                            <td style="text-align:right;" class='accCode'><?php echo $detailRow['ACCOUNT_CODE'] ?></td>
                            <td style="text-align:left;" class='accTitle'><?php echo $detailRow['ACCOUNT_TITLE'] ?></td>
                            <td style="text-align:left;" class='narration' ><?php echo $detailRow['NARRATION'] ?></td>
                            <td style="text-align:center;" class="debitColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Dr')?number_format($detailRow['AMOUNT'],2):""; ?></td>
                            <td style="text-align:center;"  class="creditColumn"><?php echo ($detailRow['TRANSACTION_TYPE'] == 'Cr')?number_format($detailRow['AMOUNT'],2):""; ?></td>
                            <td style="text-align:center;"></td>
                          </tr>
                          <?php
                          $debit_total += ($detailRow['TRANSACTION_TYPE'] == 'Dr')?$detailRow['AMOUNT']:0;
                          $credit_total+= ($detailRow['TRANSACTION_TYPE'] == 'Cr')?$detailRow['AMOUNT']:0;
                        }
                      }
                      ?>
                      <tr style="background-color:#F8F8F8;height: 30px;">
                        <td colspan="3" style="text-align:center;"> Total/Difference </td>
                        <td class="debitTotal" style="text-align:center;color:#042377;" title="Debit"><?php echo number_format($debit_total,2); ?></td>
                        <td class="creditTotal" style="text-align:center;color:#042377;" title="Credit"><?php echo number_format($credit_total,2); ?></td>
                        <td class="drCrDiffer" style="text-align:center;color:#042377;" title="Diffrence"></td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="underTheTable pull-right" style="padding:10px;margin:10px;">
                    <input type="hidden" value="<?php echo $jid; ?>" class="jVoucher_id" />
                    <?php
                    if(isset($_GET['jid'])){
                      ?>
                      <a class="button pull-right" target="_blank" href="voucher-print.php?id=<?php echo $jid; ?> "><i class="fa fa-print"></i> Print </a>
                      <?php
                    }else{
                      ?>
                      <button class="button pull-right save_voucher"><?php echo (isset($_GET['jid']))?"Update":"Save"; ?></button>
                      <?php
                    }
                    ?>
                    <input type="button" class="button pull-right mr-10" id="new-form" onclick="window.location.href = 'aio-voucher.php';" value="New Voucher" >
                  </div>
                  <div class="clear"></div>
                </div><!--content-box-content-->
              </div><!--form-->
              <div style="clear:both;"></div>
            </div>
          </div>     <!-- End summer -->
        </div>   <!-- End .content-box-top -->
      </div>  <!--body-wrapper-->
    </div>  <!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm"></div><!--popUpForm-->
  </body>
  </html>

  <script type="text/javascript">
  $(function(){
    $(window).keydown(function(e){
      if(e.altKey == true && e.keyCode == 78){
        e.preventDefault();
        $("#new-form").click();
        return false;
      }
      if(e.altKey == true && e.keyCode == 83){
        e.preventDefault();
        $(".save_voucher").click();
        return false;
      }
    });
  });
  $(window).load(function(){
    $("div.accCodeSelector button").focus();
    $(".narration").setFocusTo(".debit");
    $(".debit").setFocusToIfVal(".credit");
    $(".narration").escFocusTo("button.dropdown-toggle");
    $(".debit").escFocusTo(".narration");
    $(".credit").escFocusTo(".debit");
    $(".debit").keyup(function(e){
      if(e.keyCode==13){
        $(this).quickSubmitDrCr();
      }
    });
    $(".credit").keyup(function(e){
      if(e.keyCode==13){
        $(this).quickSubmitDrCr();
      }
    });
    $("div.accCodeSelector button").keyup(function(e){
      if(e.keyCode ==13 && $("select.accCodeSelector option:selected").val() != 0){
        var account_code = $("select.accCodeSelector option:selected").val();
        var account_title= $("select.accCodeSelector option:selected").text();
        getAccountBalance(account_code);
        $(".insertAccTitle").fadeIn();
        $(".narration").focus();
      }
    });
    $(".save_voucher").click(function(){
      $(this).saveJournal();
    });
    $("#datepicker").setFocusTo("div.accCodeSelector button");
    $(document).showDifference();
    $(".deleteVoucher").click(function(){
      $(this).deleteVoucher();
    });

  });
  </script>
