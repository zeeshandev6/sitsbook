<?php
	require('common/classes/userAccounts.php');
	require('common/classes/md.php');
	require('common/classes/settings.php');

	$objAccounts    	= new UserAccounts();
	$objDiscountConfig  = new DiscountConfig();
	$objConfigs         = new Configs();

	$sessionUserDetails = $objAccounts->getDetails($user_id);
	$session_branch_id  = $sessionUserDetails['BRANCH_ID'];
	$permissionz 		= $objAccounts->getPermissions($user_id);
	$permissionz 		= explode('|',$permissionz);
	$thisFile    		= basename($_SERVER['PHP_SELF']);

	$discountType 		= $objDiscountConfig->get_discount_conf();

	if($sessionUserDetails['CASH_IN_HAND'] == 'Y'){
		$cash_in_hand_acc_code  = $sessionUserDetails['CASH_ACC_CODE'];
		$cash_in_hand_acc_title = "Cash In Hand A/c ".$sessionUserDetails['FIRST_NAME']." ".$sessionUserDetails['LAST_NAME'];
	}else{
		$cash_in_hand_acc_code  = '0101010001';
		$cash_in_hand_acc_title = 'Cash In Hand';
	}
	$allow_other_cash  = $sessionUserDetails['ALLOW_OTHER_CASH'];
	$fullName 		   = $objAccounts->getFullName($user_id);
	$usersFullName 	   = $fullName['FIRST_NAME'].' '.$fullName['LAST_NAME'];
	$cash_in_hand_list = $objAccounts->getFullListCashInHand();
	$cash_in_bank_list = $objAccounts->getAccountByCatAccCode('010102');
	$userNameDiv 	   = '<div class="userNameBanner">'.$usersFullName.'</div>';
?>
