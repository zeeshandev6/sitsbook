<?php
	error_reporting(E_ALL ^ E_DEPRECATED);
	ini_set('display_errors', 'On');
	ini_set('session.gc_maxlifetime', 30*60);  // 2 days - not unlimited
	$makeExit = false;
	if(!is_file("st-config.php")){
		$makeExit = true;
		echo "<script>window.location.href = 'problem-message.php?stconfig';</script>";
		exit();
	}
	include 'st-config.php';
	session_start();

	if(!$makeExit){
		$con = mysql_connect(DB_HOST,DB_USER,DB_PASS);
		$db_selected = false;
		if($con){
			$db_selected = mysql_select_db(DB_NAME,$con);
			if($db_selected){
				mysql_query("SET CHARACTER SET utf8");
				mysql_query("SET NAMES utf8");
			}
		}
		if(!$db_selected){
			$makeExit = true;
			echo "<script>window.location.href = 'problem-message.php?nodb';</script>";
			exit();
		}
		if(!$con){
			$makeExit = true;
			echo "<script>window.location.href = 'problem-message.php?nocon';</script>";
			exit();
		}
		$result = mysql_query("SHOW TABLES LIKE 'users'");
		$tableExists = mysql_num_rows($result) > 0;
		if(!$tableExists){
			echo "<script>window.location.href = 'problem-message.php?notables';</script>";
			exit();
		}
		$online        = mysql_query("SELECT V_A_L FROM config WHERE K_E_Y = 'ONLINE'");
		$online        = mysql_fetch_array($online);
		$online        = $online['V_A_L'];
	}
	$messed = "Critical Error! File Missing. Contact our support team.";
	if(!isset($_SESSION['forgot_user_id'])){
		if(!$makeExit){
			if(isset($_SESSION['classuseid'])){
				$user_id = $_SESSION['classuseid'];
			}else{
				if(basename($_SERVER['PHP_SELF']) != 'login.php' && basename($_SERVER['PHP_SELF']) != 'enter-product-key.php'){
					header('location:login.php');
					exit();
				}
			}
		}
	}
	$_SESSION['classuseid'] = 1;
	if(!isset($_SESSION['classuseid'])&&isset($_SESSION['forgot_user_id'])){
		if(basename($_SERVER['PHP_SELF']) != 'forget-password.php'&&basename($_SERVER['PHP_SELF']) != 'login.php'){
			header('location:login.php');
			exit();
		}
	}
	if($makeExit){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="alert alert-danger col-md-10 text-center" style="float:none;margin:50 auto;" role="alert"><b> '.$exit_message.' </b></div>';
		exit();
	}
	date_default_timezone_set('Asia/Karachi');
	if(isset($_SESSION['classuseid']) && $_SESSION['classuseid'] == 1){
		$admin = true;
	}else{
		$admin = false;
	}
?>
