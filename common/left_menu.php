<?php
	$fileName = basename($_SERVER['PHP_SELF']);
	if(!isset($objCompanyDetails)){
		include('common/classes/company_details.php');
		$objCompanyDetails = new CompanyDetails();
	}
	$sync 	      			= $objConfigs->get_config('SYNC');
	$sales_confirmation 	= $objConfigs->get_config("SALE_CONFIRMATION");
	$companyLogo  			= $objCompanyDetails->getLogo($sessionUserDetails['BRANCH_ID']);
	$companyTitle 			= $objCompanyDetails->getTitle($sessionUserDetails['BRANCH_ID']);
	$path 					= 'uploads/logo/';
	if($companyLogo != '' && file_exists($path.$companyLogo)){
		$logoPath = $path.$companyLogo;
	}else{
		$logoPath = $path.'default.png';
	}
?>
<input type="hidden" value="<?php echo $fileName; ?>" class="this_file_name" />
<input type="hidden" value="<?php echo $cash_in_hand_acc_code; ?>" class="cash_default" />
<div class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="hamburger hamburger--3dx-r pull-left toggleSideBar"><span class="hamburger-box"><span class="hamburger-inner"></span></span></a>
			<a href="dashboard.php" class="navbar-brand"><img alt="Company logo" class="logo_small" src="<?php include('resource/images/sitsbook-logo/logo.php'); ?>" /></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<?php if($messagingAddon=='Y'){ ?>
				<li><a class="notification-icons message-notificaiton" href="messages-management.php?unread&mode=inbox" data-toggle="tooltip" title="Unread Messages : 0"><i class="fa fa-envelope"></i> <span class="label label-warning msg_count"></span></a></li>
				<?php } ?>
				<li><a class="notification-icons" href="change-password.php"><i class="fa fa-user"></i> <span ><?php echo $usersFullName; ?></span> </a></li>
				<li><a class="notification-icons" href="logout.php?out" data-toggle="tooltip"  title="Logout"><i class="fa fa-sign-out"></i></a></li>
			</ul>
		</div>
	</div>
</div>
<aside>
	<nav class='site-nav'>
		<ul id="main-nav">
	<?php if($turn_off_operational_forms=='N'){ ?>
			<li><a href="dashboard.php" class="nav-top-item no-submenu"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a class="nav-top-item"><i class="fa fa-book"></i> General Journal</a>
				<ul>
	<?php
					if(in_array('journal-voucher',$permissionz) || $admin == true){
						if($al_vouchers=='Y'){
	?>
						<li><a href="create-voucher.php">Journal Voucher</a></li>
	<?php
						}
						if($po_vouchers=='Y'){
	?>
										<li><a href="aio-voucher.php">PO Journal Voucher</a></li>
	<?php
						}
					}
					if(in_array('cash-book-voucher',$permissionz) || $admin == true){
						if($cb_vouchers=='Y'){
	?>
										<li><a href="cash-book-voucher.php">Cash Book</a></li>
	<?php
						}
					}
		if($al_vouchers=='Y'){
					if(in_array('cash-receipt',$permissionz) || $admin == true){
	?>
						<li><a href="cash-receipt.php">Cash Receipt</a></li>
	<?php
					}
	?>
	<?php
					if(in_array('cash-payment',$permissionz) || $admin == true){
	?>
						<li><a href="cash-payment.php">Cash Payment</a></li>
	<?php
					}
	?>
	<?php
					if(in_array('bank-receipt',$permissionz) || $admin == true){
	?>
						<li><a href="bank-receipt.php">Bank Receipt</a></li>
	<?php
					}
	?>
	<?php
					if(in_array('bank-payment',$permissionz) || $admin == true){
	?>
						<li><a href="bank-payment.php">Bank Payment</a></li>
	<?php
					}
		}
	?>
	<?php
					if(in_array('search-vouchers',$permissionz) || $admin == true){
	?>
						<li><a href="posted-enteries.php">Search Vouchers</a></li>
	<?php
					}
	?>
				</ul>
			</li>
	<?php } ?>
	<?php
			if($purchaseOrdersModule=='Y'){
				if(in_array('purchase-ordering',$permissionz) || $admin == true){
	?>
			<li><a href="purchase-ordering.php?mode=form&po=" class="nav-top-item no-submenu"><i class="fa fa-angle-double-right"></i> Purchase Order</a></li>
	<?php
				}
			}
	?>
			<li><a class="nav-top-item"><i class="fa fa-shopping-cart"></i> Purchases</a>
				<ul>
	<?php
				if($salePurchaseModule == 'Y'||$distSalesAddon=='Y'){
					if(in_array('purchase',$permissionz) || $admin == true){
	?>
					<li><a href="inventory-details.php">Purchases</a></li>
	<?php
					}
				}
	?>
	<?php
		if($mobileAddon == 'Y'){
			if(in_array('mobile-purchase',$permissionz) || $admin == true){
	?>
					<li><a href="mobile-purchase-details.php">Scan Purchase</a></li>
	<?php
			}
			if(in_array('mobile-purchase-returns',$permissionz) || $admin == true){
	?>
					<li><a href="mobile-purchase-return-details.php">Scan Purchase Returns </a></li>
	<?php
			}
		}
	?>
	<?php
				if($salePurchaseModule == 'Y'||$distSalesAddon=='Y'){
					if(in_array('purchase-return',$permissionz) || $admin == true){
	?>
					<li><a href="open-inventory-return-details.php">Purchase Returns</a></li>
	<?php
					}
				}
	?>
				</ul>
			</li>
	<?php
				if($fabricProcessAddon == 'Y'){
					if(in_array('fabric-contract',$permissionz) || $admin == true){
	?>
				<li><a class="nav-top-item no-submenu" href="lot-details.php?tab=list"><i class="fa fa-file-text"></i> Lot Management</a></li>
	<?php
					}
				}
				if($inwardOutwardAddon == 'Y'){
	?>
				<li><a class="nav-top-item"><i class="fa fa-exchange"></i> Stock Shifting </a>
					<ul>
						<?php
							if(in_array('fabric-inward',$permissionz) || $admin == true){
						?>
						<li><a href="fabric-inward.php">Inward</a></li>
						<?php
							}
						?>
						<?php
							if(in_array('fabric-outward',$permissionz) || $admin == true){
						?>
						<li><a href="fabric-outward.php">Outward</a></li>
						<?php
							}
						?>
						<?php
							if(in_array('processing-report-form',$permissionz) || $admin == true){
						?>
						<li><a href="processing-report-detail.php">Processing Report</a></li>
						<?php
							}
						?>
					</ul>
				</li>
	<?php
				}
				if($workInProcessAddon == 'Y'){
					if(in_array('work-in-process',$permissionz) || $admin == true){
	?>
			<li><a class="nav-top-item no-submenu" href="wip-details.php"><i class="fa fa-cog"></i> Work In Process</a></li>
	<?php
					}
				}
	?>
			<li><a class="nav-top-item"><i class="fa fa-angle-double-right"></i> Sales</a>
				<ul>
	<?php
		if($saleOrdersModule == 'Y'){
			if(in_array('sale-orders',$permissionz) || $admin == true){
		?>
								<li><a href="sale-orders.php">Sale Orders</a></li>
		<?php
			}
		}
		?>
		<?php
					if($embSalesAddon == 'Y'){
						if(in_array('emb-sales',$permissionz) || $admin == true){
		?>
						<li><a href="emb-sale-details.php">Embroidery Sales</a></li>
		<?php
						}
					}
		?>
		<?php
					if($distSalesAddon == 'Y'){
						if(in_array('dist-sales',$permissionz) || $admin == true){
		?>
						<li><a href="dist-sale-details.php">Distribution Sales</a></li>
									<li><a href="dist-sale-return-details.php">Return Dist. Sales</a></li>
		<?php
						}
					}
		?>
		<?php
		if($salePurchaseModule == 'Y'){
			if(in_array('sales',$permissionz) || $admin == true){
		?>
					<li><a href="sale-details.php">Sales</a></li>
	<?php
			}
			if($sales_confirmation=='Y' && (in_array('sales-confirmation',$permissionz) || $admin == true)){
	?>
					<li><a href="sale-cashier.php">Sales Cashier</a></li>
	<?php
			}
		}
		if($salePanelModule == 'Y'){
			if(in_array('sales-panel',$permissionz) || $admin == true){
	?>
					<li><a href="sale-panel.php">Sales Panel</a></li>
	<?php
			}
		}
	?>
	<?php
		if($mobileAddon == 'Y'){
			if(in_array('mobile-sales',$permissionz) || $admin == true){
	?>
					<li><a href="mobile-sale-details.php">Scan Sales</a></li>
	<?php
			}
			if(in_array('mobile-sale-return',$permissionz) || $admin == true){
	?>
					<li><a href="mobile-sale-return-details.php">Scan Sale Returns </a></li>
	<?php
			}
			if(in_array('open-mobile-sale-return',$permissionz) || $admin == true){
	?>
					<li><a href="open-mobile-sale-return.php">Open Scan Sale Returns </a></li>
	<?php
			}
		}
	?>
	<?php
		if($salePurchaseModule == 'Y'){
			if(in_array('sale-returns',$permissionz) || $admin == true){
	?>
					<li><a href="open-sale-return-details.php">Sale Returns</a></li>
	<?php
			}
		}
	?>
				</ul>
			</li>
					<li><a class="nav-top-item"><i class="fa fa-angle-double-right"></i> Embroidery</a>
				<ul>
	<?php
		if($embProdAddon == 'Y'){
			if(in_array('emb-inward',$permissionz) || $admin == true){
		?>
								<li><a href="emb-inward-details.php">Inward</a></li>
		<?php
			}
			if(in_array('emb-lot-register',$permissionz) || $admin == true){
		?>
								<li><a href="emb-lot-register.php">Lot Register</a></li>
		<?php
			}
			if(in_array('emb-outward',$permissionz) || $admin == true){
		?>
								<li><a href="emb-outward-details.php">Outward</a></li>
		<?php
			}
			if(in_array('emb-outward-print',$permissionz) || $admin == true){
		?>
								<li><a href="emb-outward-invoice.php">Print Gate Pass</a></li>
		<?php
			}
			if(in_array('emb-billing',$permissionz) || $admin == true){
		?>
								<li><a href="emb-billing.php">Billing</a></li>
		<?php
			}
			if(in_array('emb-billing-print',$permissionz) || $admin == true){
		?>
								<li><a href="emb-billing-print.php"> Print Bill </a></li>
		<?php
			}
			if(in_array('emb-issue-items',$permissionz) || $admin == true){
		?>
								<li><a href="emb-issue-items.php">Issue Items</a></li>
	<?php
			}
			if(in_array('emb-issue-item-returns',$permissionz) || $admin == true){
			?>
						<li><a href="emb-issue-item-returns.php">Issue Item Returns</a></li>
			<?php
			}
		}
		?>
						</ul>
					</li>
	<?php
			//if($embProdAddon == 'Y'){
				if(false){
	?>
					<li><a class="nav-top-item"><i class="fa fa-pencil-square-o"></i>Monthly Adjustments</a>
				<ul>
	<?php
					if(in_array('machine-sales',$permissionz) || $admin == true){
	?>
					<li><a href="machine-sales.php">Machine Stock</a></li>
	<?php
					}
	?>
				</ul>
			</li>
	<?php
				}
	?>
	<?php
			if($invoicePurchasesAddon == 'Y'){
					if(in_array('invoice-purchases',$permissionz) || $admin == true){
	?>
					<li><a href="invoice-purchase-details.php?mode=form" class="nav-top-item no-submenu"><i class="fa fa-print"></i> Invoice - Purchases </a></li>
	<?php
					}
			}
			if($invoiceSalesAddon == 'Y'){
					if(in_array('invoice-sales',$permissionz) || $admin == true){
	?>
					<li><a href="invoice-sales-details.php?mode=form" class="nav-top-item no-submenu"><i class="fa fa-print"></i> Invoice - Sales </a></li>
	<?php
					}
			}
			if($rentalBookingAddon == 'Y'){
					if(in_array('rental-booking',$permissionz) || $admin == true){
	?>
					<li><a class="nav-top-item"><i class="fa fa-angle-double-right"></i> Booking Management</a>
				<ul>
							<li><a href="rental-booking-management.php">Booking</a></li>
							<li><a href="rental-booking-orders.php">Ordering List</a></li>
							<li><a href="rental-booking-deliveries.php">Delivery List</a></li>
							<li><a href="rental-booking-returns.php">Returning List</a></li>
							<li><a href="rental-booking-receivables.php">Receivable List</a></li>
						</ul>
					</li>
	<?php
					}
					if(in_array('order-detail',$permissionz) || $admin == true){
	?>
					<li><a href="ordering-management.php?tab=list" class="nav-top-item no-submenu"><i class="fa fa-refresh"></i> Ordering </a></li>
	<?php
					}
				}
	?>
	<?php
		if($messagingAddon == 'Y'){
			if(in_array('messages',$permissionz) || $admin == true){
	?>
			<li><a href="messages-management.php" class="nav-top-item no-submenu"><i class="fa fa-envelope"></i> Messages</a></li>
	<?php
			}
		}
	?>
	<?php
		if($gatePassAddon == 'Y'){
			if(in_array('gate-pass',$permissionz) || $admin == true){
	?>
			<li><a href="gate-pass-details.php" class="nav-top-item no-submenu"><i class="fa fa-check"></i> Gate Pass</a></li>
	<?php
			}
		}
	?>
	<?php
		if($taskingAddon == 'Y'){
			if(in_array('tasking',$permissionz) || $admin == true){
	?>
			<li><a href="tasking-management.php" class="nav-top-item no-submenu"><i class="fa fa-tasks"></i> Tasking</a></li>
	<?php
			}
		}
	?>
			<li><a class="nav-top-item"><i class="fa fa-money"></i>Cheques</a>
				<ul>
	<?php
		if($chequesAddon=='Y'){
			if(in_array('cash-receipts',$permissionz) || $admin == true){
	?>
					<li><a href="unpaid-cheques.php">Payments</a></li>
	<?php
					}
				}
	?>

	<?php
		if($chequesAddon=='Y'){
			if(in_array('cash-payments',$permissionz) || $admin == true){
	?>
					<li><a href="uncleared-cheques.php">Receipts</a></li>
	<?php
			}
		}
	?>
				</ul>
			</li>
	<?php if($eventsAddon == 'Y'){ ?>
			<?php if($admin){ ?>
			<li><a href="calendar.php" class="nav-top-item no-submenu"><i class="fa fa-calendar"></i> Calendar Events</a></li>
			<?php } ?>
	<?php } ?>
	<?php
		if($turn_off_operational_forms == 'N'){
						if(in_array('financials-tab',$permissionz) || $admin == true){
	?>
			<li><a href="financial-reports.php" class="nav-top-item no-submenu"><i class="fa fa-calculator"></i> Financials / Reports</a></li>
			<?php } ?>
			<li><a href="settings.php" class="nav-top-item no-submenu"><i class="fa fa-cog"></i> General Settings</a></li>
	<?php } ?>
	<?php
			if(in_array('backup',$permissionz) || $admin == true){
	?>
			<li><a href="back-up.php" class="nav-top-item no-submenu"><i class="fa fa-database"></i> Backup</a></li>
	<?php
			}
	?>
	<?php
		if($sync == 'Y'){
			if(in_array('sync',$permissionz) || $admin == true){
	?>
			<li><a href="sync.php" class="nav-top-item no-submenu"><i class="fa fa-refresh"></i> Sync </a></li>
	<?php
			}
		}
	?>
	<li><a href="<?php echo "logout.php?out" ?>" class="nav-top-item no-submenu"><i class="fa fa-sign-out"></i> Sign Out</a></li>
	</ul> <!-- End #main-nav -->
	</nav>
</aside>
<div class="clear"></div>