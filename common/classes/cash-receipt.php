<?php
	class cashReceipt{
		public $voucherId;
		public $crNum;
		public $jVoucherNum;
		public $voucherDate;
		public $reference;
		public $status;
		public $voucherType;
		public $accountCode;
		public $accountTitle;
		public $narration;
		public $transactionType;
		public $instrumentNum;
		public $amount;
		public $reference_date;
		
		public function getAccountsList(){
			$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) = 9";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($voucherId){
			$query = "SELECT * FROM `voucher` WHERE `VOUCHER_ID` = $voucherId AND CP_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function getListExcluding($voucherId){
			$query = "SELECT * FROM `ledger` WHERE `LEDGER_NO` !=$voucherId AND CP_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function genJvNumber(){
			$query = mysql_query("SELECT MAX(VOUCHER_NO) as LAST_VOUCHER_NO FROM `voucher`");
			if(mysql_num_rows($query)){
				$jv = mysql_fetch_array($query);
				$jv = $jv['LAST_VOUCHER_NO'];
				$jv++;
				$jvNum = $jv;
			}else{
				$jvNum = 1;
			}
			return $jvNum;
		}
		public function genCrNumber(){
			$query = mysql_query("SELECT MAX(TYPE_NO) as LAST_CR_NO FROM `voucher` WHERE VOUCHER_TYPE = 'CR'");
			if(mysql_num_rows($query)){
				$cr = mysql_fetch_array($query);
				$cr = $cr['LAST_CR_NO'];
				$cr++;
				$crNum = $cr;
			}else{
				$crNum = 1;
			}
			return $crNum;
		}
		public function saveVoucher(){
			$query = "INSERT INTO `voucher`(`VOUCHER_NO`, 
											`CR_NO`,
											`VOUCHER_DATE`, 
											`REFERENCE`,
											`REF_DATE`,
											`VOUCHER_STATUS`) 
									VALUES ($this->jVoucherNum,
											 $this->crNum,
											'$this->voucherDate',
											'$this->reference',
											'$this->reference_date',
											'$this->status')";
			$saved = mysql_query($query);
			return mysql_insert_id();
		}
		public function getRecieptDetailList($jvId){
			$query = "SELECT * FROM `voucher_details` WHERE `VOUCHER_ID`= $jvId AND `TRANSACTION_TYPE` = 'Cr'";
			$records = mysql_query($query);
			return $records;
		}
		public function updateVoucher($jvId){
			$query = "UPDATE `voucher` SET `REFERENCE`='$this->reference',
										   `REF_DATE`='$this->reference_date' WHERE `VOUCHER_ID` = $jvId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function saveCashReceiptDetail(){
			$qureyCashAcc = "SELECT `ACC_CODE`,`ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '0101030001'";
			$CashAcc = mysql_fetch_array(mysql_query($qureyCashAcc));
			$cashAccCode = $CashAcc['ACC_CODE'];
			$cashAccTitle = $CashAcc['ACC_TITLE'];
			$CashReceiptDebitNarration = "Cash Received From $this->accountTitle-$this->accountCode Vide Receipt No.$this->instrumentNum";
			if($CashAcc){
				$transactionType = 'Dr';
				$newBalance = $this->getLastBalance($cashAccCode,$this->amount,$transactionType);
				$queryDebit = "INSERT INTO `voucher_details`(`VOUCHER_ID`, 
													`ACCOUNT_CODE`, 
													`ACCOUNT_TITLE`, 
													`NARRATION`, 
													`TRANSACTION_TYPE`, 
													`INSTRUMENT_NO`,
													`AMOUNT`,
													`BALANCE`)
											 VALUES ($this->voucherId,
											 		 '$cashAccCode',
													 '$cashAccTitle',
													 '$CashReceiptDebitNarration',
													 '$transactionType',
													 $this->instrumentNum,
													 $this->amount,
													 $newBalance)";
				$transactionType = 'Cr';
				$newBalance = $this->getLastBalance($this->accountCode,$this->amount,$transactionType);
				$queryCredit = "INSERT INTO `voucher_details`(`VOUCHER_ID`, 
													`ACCOUNT_CODE`, 
													`ACCOUNT_TITLE`, 
													`NARRATION`, 
													`TRANSACTION_TYPE`, 
													`INSTRUMENT_NO`,
													`AMOUNT`,
													`BALANCE`)
											 VALUES ($this->voucherId,
											 		 '$this->accountCode',
													 '$this->accountTitle',
													 '$this->narration',
													 '$transactionType',
													 $this->instrumentNum,
													 $this->amount,
													 $newBalance)";
				$insertedDebit = mysql_query($queryDebit);
				$insertedCredit = mysql_query($queryCredit);
				return $insertedCredit;
			}
		}
	}
?>