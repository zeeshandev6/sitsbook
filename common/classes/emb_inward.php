<?php
	class EmbroideryInward{

		//inward
		public $inward_date;
		public $gate_pass_no;
		public $lot_num;
		public $account_code;

		public $from_date;
		public $to_date;

		//inward Details
		public $inward_id;
		public $inwd_details_id;
		public $prod_id;
		public $quality;
		public $color;
		public $than;
		public $mlength;
		public $measure_id;
		public $total_length;

		//inward
		public function getInwardList($start,$total){
			$today = date('Y-m-d');
			$query = "SELECT * FROM emb_inward LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function getInward($inwd_id){
			$query 		= "SELECT * FROM emb_inward WHERE ID =$inwd_id";
			$records 	= mysql_query($query);
			if(mysql_num_rows($records)){
				return mysql_fetch_assoc($records);
			}
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM emb_inward";
			$records = mysql_query($query);
			return $records;
		}
		public function getNextLotNumber(){
			$query 	 = "SELECT MAX(LOT_NO) AS LAST_NUMBER FROM `emb_inward` ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_assoc($records);
				return $row['LAST_NUMBER'] + 1;
			}
			return 1;
		}
		public function saveInward(){
			$query = "INSERT INTO `emb_inward` (`INWD_DATE`,
																					`GATE_PASS_NO`,
																					`LOT_NO`,
																					`ACCOUNT_CODE`)
	    				           					VALUES ('$this->inward_date',
																					'$this->gate_pass_no',
																					'$this->lot_num',
																					'$this->account_code')";
			mysql_query($query);
			$inserted_id = mysql_insert_id();
			return $inserted_id;
		}
		public function updateInward($inwd_id){
			$query = "UPDATE `emb_inward` SET
																					`INWD_DATE`			 = '$this->inward_date',
																					`GATE_PASS_NO`	 = '$this->gate_pass_no',
																					`LOT_NO`				 = '$this->lot_num',
																					`ACCOUNT_CODE`	 = '$this->account_code' WHERE ID = $inwd_id ";
			$records = mysql_query($query);
			return $records;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM emb_inward ";
			$andFlag = false;

			if($this->account_code != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `ACCOUNT_CODE` = '".$this->account_code."' ";
				$andFlag = true;
			}
			if($this->lot_num != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_NO` = '".$this->lot_num."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY INWD_DATE DESC,LOT_NO DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}

		//inward Details
		public function getInwardDetailList($inward_id){
			$query = "SELECT * FROM emb_inward_details WHERE INWARD_ID = '$inward_id' ";
			return mysql_query($query);
		}
		public function getInwardDetail($id){
			$query = "SELECT * FROM emb_inward_details WHERE ID = '$id' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				return mysql_fetch_assoc($records);
			}
		}
		public function getDetailOfInwardDetail($id){
			$query = "SELECT * FROM emb_inward_details WHERE ID = $id";
			$records = mysql_query($query);
			return $records;
		}

		public function addInwardDetail(){
			$query = "INSERT INTO `emb_inward_details` (`INWARD_ID`,
																								  `PRODUCT_ID`,
																									`QUALITY`,
																									`COLOR`,
																									`THAN`,
																									`MEASURE_ID`,
																									`MLENGTH`,
																									`TOTAL_LENGTH`)
																				  VALUES ('$this->inward_id',
																									'$this->prod_id',
																									'$this->quality',
																									'$this->color',
																									'$this->than',
																									'$this->measure_id',
																									'$this->mlength',
																									'$this->total_length')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function updateInwardDetail($inward_detail_id){
			$query = "UPDATE `emb_inward_details` SET  	`PRODUCT_ID`		= '$this->prod_id',
																									`QUALITY`				= '$this->quality',
																									`COLOR`					= '$this->color',
																									`THAN`					= '$this->than',
																									`MEASURE_ID`		= '$this->measure_id',
																									`MLENGTH`				= '$this->mlength',
																									`TOTAL_LENGTH`	= '$this->total_length' WHERE ID = $inward_detail_id ";
			$result = mysql_query($query);
			return $result;
		}
		public function delete($inward_id){
			$query_inward = "DELETE FROM emb_inward WHERE ID = '$inward_id' ";
			$query_inward_detail = "DELETE FROM emb_inward_details WHERE INWARD_ID = '$inward_id' ";
			$effect = (int)(mysql_query($query_inward));
			$effect+= (int)(mysql_query($query_inward_detail));
			return $effect;
		}
		public function checkInherited($inwd_id){
			$query = "SELECT ID FROM emb_inward_details WHERE INWARD_ID =$inwd_id";
			$execute = mysql_query($query);
			$check = mysql_fetch_array($execute);
			if(!empty($check['ID'])){
				return true;
			}else{
				return false;
			}
		}
		public function deleteDetails($id){
			$query = "DELETE FROM emb_inward_details WHERE ID = $id ";
			return mysql_query($query);
		}
		//inward reports
		public function getReport($fromDate,$toDate){
			$query = "SELECT * FROM `emb_inward` WHERE ";
			$andFlag = false;
			if($fromDate!==""){
				$query .= "`INWD_DATE` >= DATE('".$fromDate."')";
				$andFlag = true;
			}
			if($toDate!==""){
				if($andFlag==true){
					$query .= " AND ";
				}
				$query .= " `INWD_DATE` <= DATE('".$toDate."') ";
			}
			return mysql_query($query);
		}
		public function getDetailsByInwdId($inwd_id){
			$query = "SELECT *,SUM(THAN) as Thans,SUM(TOTAL_LENGTH) as Totals FROM `emb_inward_details` WHERE `INWARD_ID` IN ('$inwd_id') GROUP BY `INWARD_ID`";
			return mysql_query($query);
		}
		public function getThanReport($inwd_id){
			$query = "SELECT SUM(THAN) as Thans,SUM(TOTAL_LENGTH) as Totals FROM `emb_inward_details` WHERE `INWARD_ID` = '$inwd_id'";
			return mysql_query($query);
		}
		public function getCustomersList(){
			$query = "SELECT * FROM `customers` ORDER BY `CUST_ACC_TITLE`";
			return mysql_query($query);
		}
		public function getProductList(){
			$query = "SELECT * FROM `emb_products` ORDER BY `TITLE`";
			return mysql_query($query);
		}
		public function getTotalThaans($inward_id){
			$query = "SELECT SUM(THAN) AS TOTAL_THAAN FROM `emb_inward_details` WHERE INWARD_ID = '$inward_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['TOTAL_THAAN'];
			}
		}

		/* Reporting */

		public function getInwardDetailListByDateRange($fromDate,$toDate){
			$query = "SELECT * FROM `emb_inward` WHERE '' = '' ";
			if($fromDate!==""){
				$query .= " AND DATE(INWD_DATE) >= '$fromDate'";
			}
			if($toDate!==""){
				$query .= " AND DATE(INWD_DATE) <= '$toDate'";
			}
			$query .= " ORDER BY `INWD_DATE`,`ACCOUNT_CODE`";
			$records = mysql_query($query);
			return $records;
		}
		public function getInwardThanReport($fromDate,$toDate){
			$query = "SELECT *,SUM(THAN) as TOTAL_THAN,SUM(TOTAL_LENGTH) as TOTAL_LENGTH FROM `emb_inward_details` INNER JOIN `emb_inward` ON emb_inward.INWARD_ID = emb_inward_details.INWARD_ID ";
			if($fromDate!==""){
				$query .= " AND emb_inward.INWD_DATE >= '$fromDate'";
			}
			if($toDate!==""){
				$query .= " AND emb_inward.INWD_DATE <= '$toDate'";
			}
			$query .= " ORDER BY `INWD_DATE`";
			return mysql_query($query);
		}
		public function getDetailsById($inwd_id){
			$query = "SELECT *  FROM `emb_inward_details` WHERE `INWARD_ID` = $inwd_id ";
			return mysql_query($query);
		}

		public function getsSpecificInward(){
			$query = "SELECT * FROM `emb_inward` ";
			$query .= " INNER JOIN emb_inward_details ON emb_inward.ID = emb_inward_details.INWARD_ID  ";
			$andFlag = false;
			if($this->from_date!==""){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_inward.INWD_DATE >= '$this->from_date'";
				$andFlag = true;
			}
			if($this->to_date!==""){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_inward.INWD_DATE <= '$this->to_date'";
				$andFlag = true;
			}
			if($this->lot_num!==""){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_inward.LOT_NO = '$this->lot_num'";
				$andFlag = true;
			}
			if($this->account_code!==""){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_inward.ACCOUNT_CODE = '$this->account_code'";
				$andFlag = true;
			}
			$query .= " ORDER BY `INWD_DATE`";
			$result =  mysql_query($query);

			return $result;
		}
		public function getSpecificDetailsById($inwd_id){
			$query = "SELECT *  FROM `emb_inward_details`
					  WHERE `INWARD_ID` = $inwd_id";
			return mysql_query($query);
		}

		public function getProductQuality($custAccCode,$lotNum,$prodCode){
			$query = "SELECT emb_inward_details.QUALITY FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.INWARD_ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum'
					  		AND emb_inward_details.PROD_CODE = $prodCode ";

			$stock = mysql_fetch_array(mysql_query($query));
			return $stock['QUALITY'];
		}
		public function getProductQualityQuery($custAccCode,$lotNum,$prodCode){
			$query = "SELECT emb_inward_details.QUALITY FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.INWARD_ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum'
					  		AND emb_inward_details.PROD_CODE = $prodCode ";

			return mysql_query($query);
		}

		public function getProductQualityQueryWholeLot($custAccCode,$lotNum){
			$query = "SELECT emb_inward_details.QUALITY,emb_inward_details.MEASURE_ID FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum'  ";
			$result = mysql_query($query);
			return $result;
		}
		public function getInwardLotStockInHand($custAccCode,$lotNum,$prodCode){
			$query = "SELECT SUM(emb_inward_details.TOTAL_LENGTH) AS STOCK_IN_HAND FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum'
					  		AND emb_inward_details.PRODUCT_ID = '$prodCode' ";
			$result = mysql_query($query);
			$stock  = mysql_fetch_array($result);
			return $stock['STOCK_IN_HAND'];
		}

		public function getInwardLotStockInHandWhloleLot($custAccCode,$lotNum){
			$query = "SELECT SUM(emb_inward_details.TOTAL_LENGTH) AS STOCK_IN_HAND FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum'";
			$record = mysql_query($query);

			if(mysql_num_rows($record)){
				$stock = mysql_fetch_array($record);
				return ($stock['STOCK_IN_HAND'] == '')?0:$stock['STOCK_IN_HAND'];
			}else{
				return 0;
			}
		}

		public function getInwardLotStockInHandWhloleLotByDate($custAccCode,$lotNum,$outward_date){
			$query = "SELECT SUM(emb_inward_details.TOTAL_LENGTH) AS STOCK_IN_HAND FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.ID
					  WHERE emb_inward.ACCOUNT_CODE = '$custAccCode' AND emb_inward.LOT_NO = '$lotNum' ";
			$record = mysql_query($query);

			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['STOCK_IN_HAND'];
			}else{
				return 0;
			}
			$stock = mysql_fetch_array();
			return $stock['STOCK_IN_HAND'];
		}


		public function checkLotNumberExists($lotNum,$customerAccCode){
			$query = "SELECT `LOT_NO` FROM emb_inward WHERE `LOT_NO` ='$lotNum'
					  AND `ACCOUNT_CODE` = '$customerAccCode'  ";
			$record = mysql_query($query);
			$result = mysql_num_rows($record);
			return $result;
		}

		public function getsSpecificInwardJoined(){
			$andFlag = false;
			$query = "SELECT emb_inward.*,emb_inward_details.* FROM `emb_inward` INNER JOIN emb_inward_details ON emb_inward.ID = emb_inward_details.INWARD_ID ";
			$query .= " WHERE ";
			if($this->from_date!==""){
				$query .= " emb_inward.INWD_DATE >= '$this->from_date'";
				$andFlag = true;
			}
			if($this->to_date!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_inward.INWD_DATE <= '$this->to_date'";
				$andFlag = true;
			}
			if($this->account_code != ""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_inward.ACCOUNT_CODE = '$this->account_code'";
				$andFlag = true;
			}
			$query .= " GROUP BY emb_inward.LOT_NO ORDER BY emb_inward.LOT_NO ";
			$result = mysql_query($query);
			return $result;
		}

		public function getInwardClothIn($cutomerAccCode,$lotNum){
			$query = "SELECT SUM(emb_inward_details.TOTAL_LENGTH) AS TOTAL_LENGTH FROM emb_inward
					  INNER JOIN emb_inward_details ON emb_inward.ID = emb_inward_details.INWARD_ID
					  WHERE emb_inward.ACCOUNT_CODE = '$cutomerAccCode' AND emb_inward.LOT_NO = '$lotNum'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['TOTAL_LENGTH'];
			}else{
				return 0;
			}
		}
		public function deleteUnRelatedInwardEntries(){
			$query = "DELETE FROM emb_inward WHERE INWARD_ID NOT IN (SELECT INWARD_ID FROM emb_inward_details)";
			mysql_query($query);
		}

		public function existsInLotRegister($inward_id){
			$query = "SELECT ID FROM emb_lot_register WHERE CUST_ACC_CODE IN (SELECT ACCOUNT_CODE FROM emb_inward WHERE ID = '$inward_id')
						   AND LOT_NO IN (SELECT LOT_NO FROM emb_inward WHERE ID = '$inward_id') ";
			$result = mysql_query($query);

			if(mysql_num_rows($result)){
				return true;
			}else{
				return false;
			}
		}

		public function DetailRowexistsInLotRegister($id){
			$query = "SELECT emb_inward.ACCOUNT_CODE,emb_inward.LOT_NO,emb_inward_details.PROD_CODE FROM emb_inward
					  INNER JOIN emb_inward_details
					  ON emb_inward.INWARD_ID = emb_inward_details.INWARD_ID
					  WHERE ID = $id ";
			$inward_record = mysql_query($query);
			if(mysql_num_rows($inward_record)){
				$inward_record_detail = mysql_fetch_array($inward_record);
				$outward_query = "SELECT ID FROM emb_lot_register
								  WHERE ACCOUNT_CODE = '".$inward_record_detail['ACCOUNT_CODE']."'
								  AND LOT_NO = '".$inward_record_detail['LOT_NO']."' ";
				$outward_record = mysql_query($outward_query);
				if(mysql_num_rows($outward_record)){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		public function getLotLength($customerCode,$lotNumber){
			$query = "SELECT SUM(emb_inward_details.TOTAL_LENGTH) AS TOTALS FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWARD_ID = emb_inward.INWARD_ID
					  WHERE emb_inward.ACCOUNT_CODE = '$customerCode' AND emb_inward.LOT_NO = '$lotNumber' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['TOTALS'];
			}else{
				return 0;
			}
		}
	}
?>
