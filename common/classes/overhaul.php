<?php
	class OverHaul {
		public $started_on;
		public $completed_on;
        public $labour_account;
		public $labour;
		public $overhead;
        public $overhead_account;
		public $misc_expense;
		public $result_item;
		public $result_quantity;
		public $status;
		
		public $from_date;
		public $to_date;
		
        public $process_id;
		public $item_id;
		public $quantity;
		public $gross_cost;
        
        public $found_records;
		
		public function  __isset($property){
			return isset($this->$property);
		}
		
		public function getList(){
			$query = "SELECT * FROM overhaul ";
			return mysql_query($query);
		}
		public function getListBydate($theday){
			$query = "SELECT * FROM overhaul WHERE STARTED_ON = DATE('".$theday."') OR STATUS = 'P' ";
			return mysql_query($query);
		}
		public function getListByDateRange($fromDate,$toDate){
			$query = "SELECT * FROM overhaul WHERE STARTED_ON BETWEEN DATE('".$fromDate."') AND DATE('".$toDate."')";
			return mysql_query($query);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM overhaul WHERE ID = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		
		public function getJobNumber(){
			$query = "SELECT MAX(ID) AS JOB_NO FROM `overhaul`";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ++$row['JOB_NO'];
			}else{
				return 1;
			}
		}

		public function get_voucher_id($overhaul_id){
			$query = "SELECT VOUCHER_ID FROM `overhaul` WHERE ID = '$overhaul_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}

		public function save(){
			$query = "INSERT INTO `overhaul`(`STARTED_ON`, 
										`COMPLETED_ON`,
                                        `LABOUR_CODE`, 
										`LABOUR`, 
										`OVERHEAD_CODE`,
                                        `OVERHEAD`, 
										`MISC_EXPENSE`, 
										`RESULT_ITEM`, 
										`RESULT_QUANTITY`,
										`STATUS`) 
								VALUES ('$this->started_on',
										'$this->completed_on',
                                        '$this->labour_account',
										'$this->labour',
                                        '$this->overhead_account',
										'$this->overhead',
										'$this->misc_expense',
										'$this->result_item',
										'$this->result_quantity',
										'$this->status')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `overhaul` SET `STARTED_ON`  	 = '$this->started_on',
									   `COMPLETED_ON`	 = '$this->completed_on',
									   `LABOUR_CODE`	 = '$this->labour_account',
                                       `LABOUR`		 	 = '$this->labour',
									   `OVERHEAD_CODE`	 = '$this->overhead_account',
                                       `OVERHEAD`	 	 = '$this->overhead',
									   `MISC_EXPENSE`	 = '$this->misc_expense',
									   `RESULT_ITEM` 	 = '$this->result_item',
									   `RESULT_QUANTITY` = '$this->result_quantity' WHERE ID = '$id'";
			return mysql_query($query);
		}

		//Status Changes~~~Completed on Date changes Too
		public function search($start,$total){
			$query = "SELECT SQL_CALC_FOUND_ROWS overhaul.*,SUM(overhaul_details.GROSS_COST) AS TOTAL_COST FROM `overhaul` LEFT OUTER JOIN `overhaul_details` 
					  ON overhaul.ID = overhaul_details.OH_ID ";

			$andFlag = false;

			if(trim($this->from_date) == ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " overhaul.STARTED_ON >= '".$this->from_date."' ";
                $query .= " AND overhaul.COMPLETED_ON <= '".$this->to_date."' ";
				$andFlag = true;
			}
			if($this->result_item != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " overhaul.RESULT_ITEM <= '".$this->result_item."' ";
				$andFlag = true;
			}
            if($this->status != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " overhaul.STATUS <= '".$this->status."' ";
				$andFlag = true;
			}
			if($this->process_id != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " overhaul.ID = '".$this->process_id."' ";
				$andFlag = true;
			}
            $query .= " GROUP BY overhaul_details.OH_ID ORDER BY overhaul.ID ";
            $query .= " LIMIT $start,$total ";
            
            
            $result = mysql_query($query);
            
            $totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function updateStatus($id,$status,$completion_date){
			$query = "UPDATE `overhaul` SET STATUS = '$status' , `COMPLETED_ON` = '$completion_date' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function set_voucher_id($overhaul_id,$voucher_id){
			$query = "UPDATE `overhaul` SET VOUCHER_ID = '$voucher_id' WHERE ID = '$overhaul_id'";
			return mysql_query($query);
		}
		public function set_compltion_voucher_id($overhaul_id,$voucher_id){
			$query = "UPDATE `overhaul` SET CVOUCHER_ID = '$voucher_id' WHERE ID = '$overhaul_id'";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM overhaul WHERE ID = '$id'";
			return mysql_query($query);
		}
	}
?>