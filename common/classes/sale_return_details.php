<?php
	class SaleReturnDetails
	{
		public $sale_id;
		public $item_id;
		public $batch_no;
		public $expiry_date;
		public $cortons;
		public $perCorton;
		public $qtyReceipt;
		public $unitPrice;
		public $saleDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $inStock;

		public function getList($sale_id){
			$query = "SELECT * FROM sale_return_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}

		public function getReturnReplaceItems($sale_invoice_id){
			$query = "SELECT * FROM sale_return_details WHERE SALE_ID IN (SELECT ID FROM sale_return WHERE SALE_ID = '$sale_invoice_id' AND SALE_INVOICE_LINK = 'Y')  ";
			return mysql_query($query);
		}

		public function getDetails($invDetailId){
			$query = "SELECT * FROM `sale_return_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT QUANTITY FROM `sale_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['QUANTITY']=='')?0:$row['QUANTITY'];
			}else{
				return 0;
			}
		}
        public function getAllQuantity($item_id){
			$query = "SELECT SUM(UNIT_PRICE) AS PRICE,SUM(QUANTITY) AS ALL_QTY FROM `sale_return_details` WHERE `ITEM_ID` = '$item_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function getHistoryCostPrice($sale_id,$item_id){
			$item_price = 0;
			if($sale_id == 0){
				$query = "SELECT `PURCHASE_PRICE` FROM `items` WHERE `ID` = '$item_id'";
				$record = mysql_query($query);
				if(mysql_num_rows($record)){
					$row = mysql_fetch_array($record);
					$item_price = ($row['PURCHASE_PRICE'] == '')?0:$row['PURCHASE_PRICE'];
				}
			}else{
				$query  = "SELECT COST_PRICE FROM sale_details WHERE ITEM_ID = '$item_id' AND SALE_ID = '$sale_id' ";
				$result = mysql_query($query);
				if(mysql_num_rows($result)){
					$row = mysql_fetch_array($result);
					$item_price = $row['COST_PRICE'];
				}
			}
			return $item_price;
		}
		public function save(){
			$query = "INSERT INTO `sale_return_details`(`SALE_ID`,
													 `ITEM_ID`,
													 `BATCH_NO`,
											 	 	 `EXPIRY_DATE`,
													 `QUANTITY`,
													 `UNIT_PRICE`,
													 `SALE_DISCOUNT`,
													 `SUB_AMOUNT`,
													 `TAX_RATE`,
													 `TAX_AMOUNT`,
													 `TOTAL_AMOUNT`)
											 VALUES ('$this->sale_id',
												 	     '$this->item_id',
															 '$this->batch_no',
															 '$this->expiry_date',
													     '$this->qtyReceipt',
													     '$this->unitPrice',
													     '$this->saleDiscount',
													     '$this->subTotal',
													     '$this->taxRate',
													     '$this->taxAmount',
													     '$this->totalAmount')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($sale_detail_id){
			$query = "UPDATE `sale_return_details` SET  `ITEM_ID`  				= '$this->item_id',
																									`BATCH_NO` 				= '$this->batch_no',
																									`EXPIRY_DATE` 		= '$this->expiry_date',
																									`QUANTITY` 				= '$this->qtyReceipt' ,
																									`UNIT_PRICE`			= '$this->unitPrice' ,
																									`SALE_DISCOUNT`		= '$this->saleDiscount' ,
																									`SUB_AMOUNT`			= '$this->subTotal' ,
																									`TAX_RATE`				= '$this->taxRate' ,
																									`TAX_AMOUNT` 			= '$this->taxAmount' ,
																									`TOTAL_AMOUNT` 		= '$this->totalAmount'  WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function delete($sale_detail_id){
			$query = "DELETE FROM sale_return_details WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($sale_id){
			$query = "DELETE FROM sale_return_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}
	}
?>
