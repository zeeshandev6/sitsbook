<?php
	class CompanyDetails{
		public $name;
		public $urdu_name;
		public $urdu_address;
		public $address;
		public $contact;
		public $email;
		public $logo_image_file;
		public $slogan;
		public $tax_reg_no;
		public $ntn_reg_no;

		public function getList(){
			$query = "SELECT * FROM company_details ORDER BY NAME ";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM company_details WHERE ID ='$id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getActiveProfile(){
			$query = "SELECT * FROM company_details WHERE PROFILE_ACTIVE = 'Y' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getTitle(){
			$query = "SELECT `NAME` FROM company_details WHERE  `PROFILE_ACTIVE` = 'Y'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['NAME'];
			}else{
				return "";
			}
		}
        public function getLogoById($id){
			$query = "SELECT `LOGO` FROM company_details WHERE `ID` ='$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['LOGO'];
			}else{
				return "";
			}
		}
		public function getLogo(){
			$query = "SELECT `LOGO` FROM company_details WHERE `PROFILE_ACTIVE` = 'Y'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['LOGO'];
			}else{
				return "";
			}
		}
		public function setLogo($id,$logo){
			$query = "UPDATE company_details SET LOGO = '$logo' WHERE ID = '$id'" ;
			return mysql_query($query);
		}
		public function setActive($id,$active){
			$query = "UPDATE company_details SET PROFILE_ACTIVE = '$active' WHERE ID = '$id'" ;
			return mysql_query($query);
		}
        public function setAllInActive(){
			$query = "UPDATE company_details SET PROFILE_ACTIVE = 'N'  " ;
			return mysql_query($query);
		}
		public function save(){
			$query = "INSERT INTO `company_details`(`NAME`,
																							`URDU_NAME`,
																							`URDU_ADDRESS`,
																							`ADDRESS`,
																							`CONTACT`,
																							`EMAIL`,
																							`SLOGAN`,
																							`TAX_REG_NO`,
																							`NTN_REG_NO`)
			 																VALUES ('$this->name',
																							'$this->urdu_name',
																							'$this->urdu_address',
																						  '$this->address',
																							'$this->contact',
																							'$this->email',
																							'$this->slogan',
																							'$this->tax_reg_no',
																							'$this->ntn_reg_no')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `company_details` SET
															 `NAME`						=	'$this->name',
															 `URDU_NAME` 			= '$this->urdu_name',
															 `URDU_ADDRESS` 	= '$this->urdu_address',
															 `ADDRESS`				=	'$this->address',
															 `CONTACT`				=	'$this->contact',
															 `EMAIL`					=	'$this->email',
															 `SLOGAN`     		= '$this->slogan',
															 `TAX_REG_NO` 		= '$this->tax_reg_no',
															 `NTN_REG_NO` 		= '$this->ntn_reg_no' WHERE ID = '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function searchCustomer(){
			$query = "SELECT * FROM `company_details` WHERE";
			if($this->account_code!==""&&$this->account_code!==""){
				$query .= " `CUST_ACC_TITLE` = '$this->account_code' ";
			}
		}

		public function delete($id){
			$query = "DELETE FROM company_details WHERE ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}

	}
?>
