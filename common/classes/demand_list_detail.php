<?php
    class DemandListDetail{
        public $dl_id;
        public $item_id;
        public $quantity;
        public $unit_rate;

        public function getList(){
            $query = " SELECT * FROM demand_list_detail ";
            return mysql_query($query);
        }
        public function getListByDemand($dl_id){
            $query = " SELECT * FROM demand_list_detail WHERE DL_ID = '$dl_id' ";
            return mysql_query($query);
        }
        public function getIdArrayByDemand($dl_id){
            $run    = array();
            $query  = " SELECT ID FROM demand_list_detail WHERE DL_ID = '$dl_id' ";
            $result = mysql_query($query);
            if(mysql_num_rows($result)){
                while($row = mysql_fetch_assoc($result)){
                    $run[] = $row['ID'];
                }
            }
            return $run;
        }
        public function getDetail($id){
            $query  = "SELECT * FROM demand_list_detail WHERE ID = '$id' ";
            $result = mysql_query($query);
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }
        }
        public function save(){
            $query = "INSERT INTO `demand_list_detail`(`DL_ID`, 
                                                       `ITEM_ID`,  
                                                       `QUANTITY`,
                                                       `UNIT_RATE`) 
                                               VALUES ('$this->dl_id',
                                                       '$this->item_id',
                                                       '$this->quantity',
                                                       '$this->unit_rate')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `demand_list_detail` SET `ITEM_ID`     = '$this->item_id',
                                                      `QUANTITY`    = '$this->quantity',
                                                      `UNIT_RATE`   = '$this->unit_rate' WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function deleteList($dl_id){
            $query = "DELETE FROM `demand_list_detail` WHERE DL_ID = '$dl_id' ";
            return mysql_query($query);
        }
        public function delete($id){
            $query = "DELETE FROM `demand_list_detail` WHERE ID = '$id' ";
            return mysql_query($query);
        }
    }
?>