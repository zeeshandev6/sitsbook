<?php
	class cashbook
	{
		public function getCashPurchases($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher_details.TRANSACTION_TYPE = 'Cr' AND SUBSTR(ACCOUNT_CODE,1,6) = '010101'
					  AND (voucher.VOUCHER_TYPE = 'PV' || voucher.VOUCHER_TYPE = 'SR' )
					  AND voucher_details.VOUCHER_DATE = DATE('".$cash_date."') ORDER BY voucher_details.VOUCHER_ID ASC ";
			return mysql_query($query);
		}
		public function getCashSales($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher_details.TRANSACTION_TYPE = 'Dr' AND SUBSTR(ACCOUNT_CODE,1,6) = '010101'
					  AND (voucher.VOUCHER_TYPE = 'SV' || voucher.VOUCHER_TYPE = 'PR' )
					  AND voucher_details.VOUCHER_DATE = DATE('".$cash_date."') ORDER BY voucher_details.VOUCHER_ID ASC ";
			return mysql_query($query);
		}
		public function getCashReceipts($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE (SUBSTR(ACCOUNT_CODE,1,6) != '010101' AND (voucher.VOUCHER_TYPE = 'CR' OR voucher.VOUCHER_TYPE = 'CB'))
					  AND voucher.REVERSAL_ID = 0
					  AND voucher_details.VOUCHER_DATE = '".$cash_date."'
						AND voucher_details.TRANSACTION_TYPE = 'Cr' ORDER BY voucher_details.VOUCHER_ID ASC";
			return mysql_query($query);
		}
		public function getCashPayments($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE (SUBSTR(ACCOUNT_CODE,1,6) != '010101' AND (voucher.VOUCHER_TYPE = 'CP' OR voucher.VOUCHER_TYPE = 'CB'))
					  AND voucher.REVERSAL_ID = 0
					  AND voucher_details.VOUCHER_DATE = DATE('".$cash_date."')
						AND voucher_details.TRANSACTION_TYPE = 'Dr' ORDER BY voucher_details.VOUCHER_ID ASC";
			return mysql_query($query);
		}
		public function getBankReceipts($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher.VOUCHER_TYPE = 'BR'
					  AND voucher.REVERSAL_ID = 0
					  AND voucher_details.VOUCHER_DATE = DATE('".$cash_date."')
					  ORDER BY voucher_details.VOUCHER_ID ASC";
			return mysql_query($query);
		}
		public function getBankPayments($cash_date){
			$query = "SELECT voucher_details.* ,voucher.*
					  FROM voucher_details INNER JOIN voucher ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher.VOUCHER_TYPE = 'BP'
					  AND voucher.REVERSAL_ID = 0
					  AND voucher_details.VOUCHER_DATE = DATE('".$cash_date."')
					  ORDER BY voucher_details.VOUCHER_ID ASC";
			return mysql_query($query);
		}
		public function getVoucherDetailsRowPrev($vouch_detail_id){
			$query = "SELECT voucher.*,voucher_details.*
					  FROM voucher INNER JOIN voucher_details
					  ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID WHERE voucher_details.VOUCH_DETAIL_ID = (SELECT MAX(voucher_details.VOUCH_DETAIL_ID) FROM `voucher_details` WHERE voucher_details.VOUCH_DETAIL_ID < $vouch_detail_id)";
			$record = mysql_query($query);
			return $record;
		}
		public function get_dayz_of_cash(){
			$year   = date('01-01-Y');
			$query  = "SELECT VOUCHER_DATE FROM voucher_details WHERE SUBSTR(ACCOUNT_CODE,1,6) = '010101' GROUP BY VOUCHER_DATE ";
			$result = mysql_query($query);
			$array = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_array($result)){
					$array[] = $row['VOUCHER_DATE'];
				}
			}
			return $array;
		}
		public function getOpeningBalanceSum($endDate,$cash){
			if(!is_array($cash)){
				return false;
			}
			$cashSum = 0;
			foreach($cash as $k => $cashCode){
				$cashCodes = $this->getAccountByCatAccCode($cashCode);
				if(mysql_num_rows($cashCodes)){
					while($cashCodeRow = mysql_fetch_array($cashCodes)){
						$cash = $this->getLedgerOpeningBalance($endDate,$cashCodeRow['ACC_CODE'],'<');
						$cashSum += $cash;
					}
				}
			}
			return $cashSum;
		}
		public function getAccountByCatAccCode($sixDigitCode){
			$query = "SELECT * FROM account_code WHERE SUBSTR(ACC_CODE,1,6) = '$sixDigitCode' AND LENGTH(ACC_CODE) = 9 ORDER BY ACC_CODE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getLedgerOpeningBalance($endDate,$accCode,$operator){
			if(!isset($operator)){
				$operator = '<';
			}
			$query = "SELECT BALANCE
					  FROM voucher_details
					  WHERE VOUCHER_DATE ".$operator." DATE('".$endDate."')
					  AND ACCOUNT_CODE = '$accCode'
					  ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE']==''?0:$cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
	}

?>
