<?php
	class Sale{
		public $user_id;
		public $saleDate;
		public $record_time;
		public $billNum;
		public $source;
		public $supplierAccCode;
		public $supplierAccTitle;
		public $bill_amount;
		public $discount;
		public $discount_type;
		public $sale_discount;
		public $item;
		public $fromDate;
		public $toDate;
		public $mobile_no;
		public $customer_name;
		public $charges;
		public $notes;
		public $subject;
		public $received_cash;
		public $recovered_balance;
		public $recovery_amount;
		public $change_returned;
		public $remaining_amount;

		public $with_tax;
		public $registered_tax;
		public $tax_bill_num;

		public $cat;
		public $account_type;
		public $brand_name;

		public $found_records;

		public function getList(){
			$query = "SELECT * FROM sale ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM sale ";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($bill_num){
			$query = "SELECT BILL_NO FROM sale WHERE BILL_NO = $bill_num ";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM sale  ORDER BY BILL_NO,SALE_DATE DESC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM sale WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}

		public function getAny($id){
			$query = "SELECT * FROM sale WHERE ID = '$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($sale_id){
			$query = "SELECT * FROM sale WHERE ID = '$sale_id' ";
			$record =mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_assoc($record);
			}else{
				return NULL;
			}
		}
		public function getLastBillByAccountCode($report_date,$party_acc_code){
			$query = "SELECT * FROM sale WHERE CUST_ACC_CODE = '$party_acc_code' AND SALE_DATE <= '$report_date' ORDER BY SALE_DATE DESC LIMIT 1 ";
			$record =mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_assoc($record);
			}else{
				return NULL;
			}
		}
		public function getTaxInvoicesListForSerialUpdation($sale_date,$sale_id,$new_tax_bill_no){
			$query  = " SELECT ID,TAX_BILL_NO FROM sale WHERE (SALE_DATE >= '$sale_date' OR (TAX_BILL_NO > $new_tax_bill_no AND SALE_DATE = '$sale_date') ) AND WITH_TAX = 'Y' AND ID != '$sale_id' ORDER BY SALE_DATE ASC ";
			return  mysql_query($query);
		}
		public function updateTaxInvoiceSerialNumber($sale_id,$tax_bill_no){
			$query = " UPDATE sale SET TAX_BILL_NO = '$tax_bill_no' WHERE ID = '$sale_id' ";
			return mysql_query($query);
		}
		public function genBillNumber(){
			$row = NULL;$row_scan = NULL;
			$last_bill_number 		= 1;
			if($this->with_tax=='Y'){
				$end_date    = date('Y-m-d',strtotime($this->saleDate.' + 1 year'));
				$query 		 	 = "SELECT MAX(TAX_BILL_NO) AS S_BILL_NO FROM sale WHERE SALE_DATE >= '".$this->saleDate."' AND SALE_DATE <= '".$end_date."' AND WITH_TAX = 'Y' ";
				$record      = mysql_query($query);
				$row 	       = (mysql_num_rows($record))?mysql_fetch_assoc($record):NULL;
				$last_bill_number = (int)$row['S_BILL_NO'];
			}else{
				$query 		 	 = "SELECT MAX(BILL_NO) AS S_BILL_NO FROM sale ";
				$query_scan  = "SELECT MAX(BILL_NO) AS SS_BILL_NO FROM mobile_sale";
				$record      = mysql_query($query);
				$record_scan = mysql_query($query_scan);
				$row 	       = (mysql_num_rows($record))?mysql_fetch_assoc($record):NULL;
				$row_scan    = (mysql_num_rows($record_scan))?mysql_fetch_assoc($record_scan):NULL;

				if($row != NULL || $row_scan != NULL){
					$records     			= array_merge($row,$row_scan);
					$last_bill_number = max($records);
				}
			}
			return ++$last_bill_number;
		}
		public function save(){
			if(isset($_SESSION['classuseid'])&&$this->user_id==0){
				$user_id = $_SESSION['classuseid'];
			}elseif($this->user_id > 0){
				$user_id = $this->user_id;
			}else{
				return false;
			}
			$query = "INSERT INTO `sale`(`USER_ID`,
										 `SALE_DATE`,
										 `RECORD_TIME`,
										 `BILL_NO`,
										 `CUST_ACC_CODE`,
										 `BILL_AMOUNT`,
										 `DISCOUNT`,
										 `DISCOUNT_TYPE`,
										 `SALE_DISCOUNT`,
										 `MOBILE_NO`,
										 `CUSTOMER_NAME`,
										 `CHARGES`,
										 `NOTES`,
										 `SUBJECT`,
										 `RECEIVED_CASH`,
										 `RECOVERED_BALANCE`,
										 `RECOVERY_AMOUNT`,
										 `CHANGE_RETURNED`,
										 `REMAINING_AMOUNT`,
									 	 `WITH_TAX`,
									 	 `REGISTERED_TAX`,
									 	 `TAX_BILL_NO`)
								  VALUES ('$user_id',
								  		  '$this->saleDate',
								  		  '$this->record_time',
								  		  '$this->billNum',
											  '$this->supplierAccCode',
											  '$this->bill_amount',
											  '$this->discount',
											  '$this->discount_type',
											  '$this->sale_discount',
											  '$this->mobile_no',
											  '$this->customer_name',
											  '$this->charges',
											  '$this->notes',
											  '$this->subject',
											  '$this->received_cash',
											  '$this->recovered_balance',
											  '$this->recovery_amount',
											  '$this->change_returned',
											  '$this->remaining_amount',
												'$this->with_tax',
												'$this->registered_tax',
												'$this->tax_bill_num')";
			mysql_query($query);

			return mysql_insert_id();
		}
		public function update($invId){
			$query = "UPDATE `sale` SET
									`SALE_DATE`		  	  = '$this->saleDate',
									`BILL_NO`		  	  	= '$this->billNum',
									`CUST_ACC_CODE`   	= '$this->supplierAccCode',
									`BILL_AMOUNT`	  	  = '$this->bill_amount',
									`DISCOUNT` 	 	  	  = '$this->discount',
									`MOBILE_NO` 	  	  = '$this->mobile_no',
									`DISCOUNT_TYPE`   	= '$this->discount_type',
									`SALE_DISCOUNT`   	= '$this->sale_discount',
									`CUSTOMER_NAME`   	= '$this->customer_name',
									`CHARGES` 		  	  = '$this->charges',
									`NOTES`     	  	  = '$this->notes',
									`SUBJECT`     	  	= '$this->subject',
									`RECEIVED_CASH`   	= '$this->received_cash',
									`RECOVERED_BALANCE` = '$this->recovered_balance',
									`RECOVERY_AMOUNT`   = '$this->recovery_amount',
									`CHANGE_RETURNED` 	= '$this->change_returned',
									`REMAINING_AMOUNT`  = '$this->remaining_amount',
									`WITH_TAX`  				= '$this->with_tax',
									`REGISTERED_TAX`  	= '$this->registered_tax',
									`TAX_BILL_NO`  			= '$this->tax_bill_num' WHERE `ID` = '$invId' ";
			$updated = mysql_query($query);

			return $updated;
		}
		public function getBranchId($sale_id){
			$query = "SELECT BRANCH_ID FROM users WHERE ID IN (SELECT USER_ID FROM sale WHERE ID = '$sale_id')";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return (int)$row['BRANCH_ID'];
			}else{
				return 0;
			}
		}
		public function getQuantityPerBill($sale_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_QUANTITY FROM sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['TOTAL_QUANTITY'] == '')?0:$row['TOTAL_QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getTotalAmount($sale_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS BILL_AMOUNT FROM sale_details WHERE ID = '$sale_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['BILL_AMOUNT'] == '')?0:$row['BILL_AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSum($sale_id){
			$query = "SELECT (SUM(TOTAL_AMOUNT) - IFNULL((SELECT SUM(TOTAL_AMOUNT) FROM sale_return_details WHERE SALE_ID IN (SELECT ID FROM sale_return WHERE SALE_ID = '$sale_id' AND SALE_INVOICE_LINK = 'Y') ),0) ) AS AMOUNT FROM sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryTaxAmountSum($sale_id){
			$query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['TAX_AMNT']=='')?0:$row['TAX_AMNT'];
			}else{
				return 0;
			}
		}
		public function inventorySearch(){
			$query = "SELECT ID FROM sale WHERE ";
			$query .= "`CUST_ACC_CODE` = '".$this->supplierAccCode."'";
			$query .= " AND `BILL_NO` = ".$this->billNum ;
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function search($start,$per_page){
			$query   = "SELECT SQL_CALC_FOUND_ROWS * FROM sale ";
			$andFlag = false;

			if($this->fromDate!=""){
				$this->fromDate = date('Y-m-d',strtotime($this->fromDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SALE_DATE` >= '".$this->fromDate."'" ;
				$andFlag = true;
			}
			if($this->toDate!=""){
				$this->toDate = date('Y-m-d',strtotime($this->toDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SALE_DATE` <= '".$this->toDate."'";
				$andFlag = true;
			}
			if(trim($this->supplierAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CUST_ACC_CODE` = '".$this->supplierAccCode."' ";
				$andFlag = true;
			}
			if($this->with_tax != "" ){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `WITH_TAX` = '".$this->with_tax."' " ;
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum ;
				$andFlag = true;
			}
			if($this->tax_bill_num!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `TAX_BILL_NO` = ".$this->tax_bill_num ;
				$andFlag = true;
			}
			if($this->user_id != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `USER_ID` = ".$this->user_id ;
				$andFlag = true;
			}
			$query .= " ORDER BY BILL_NO DESC , TAX_BILL_NO DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function getDateByBillAndCode($bill_num,$customer_code){
			$query = "SELECT SALE_DATE FROM `sale` WHERE BILL_NO = '$bill_num' AND CUST_ACC_CODE = '$customer_code'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['SALE_DATE'] == '')?"":date('d-m-Y',strtotime($row['SALE_DATE']));
			}else{
				return '';
			}
		}
		public function getDailySales($report_date){
			$query = "SELECT sale.*,sale_details.*
					  FROM sale INNER JOIN sale_details
					  ON sale.ID = sale_details.SALE_ID
					  WHERE sale.SALE_DATE = '".$report_date."' GROUP BY sale_details.SALE_ID ORDER BY sale_details.SALE_ID ASC";
			return mysql_query($query);
		}
		public function saleReport($report_type){
			if($report_type == 'S'){
				$main_table = 'sale';
				$sub_table  = 'sale_details';
				$query = "SELECT ".$main_table.".*,".$sub_table.".* FROM ".$main_table."
					  INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			}else{
				$main_table = 'sale_return';
				$sub_table  = 'sale_return_details';
				$query = "SELECT ".$main_table.".*, ".$main_table.".SALE_ID AS BILL_ID,".$sub_table.".* FROM ".$main_table."
					  INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			}
			$andFlag = false;
			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE >= DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->toDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE <= DATE('".$this->toDate."')";
				$andFlag = true;
			}
			if(trim($this->supplierAccCode)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE ='".$this->supplierAccCode."'";
				$andFlag = true;
			}
			if($report_type == 'S'){
				if(trim($this->with_tax) != ""){
					$query .= ($andFlag)?"":" WHERE ";
					$query .= ($andFlag)?" AND ":"";
					$query .= " ".$main_table.".WITH_TAX = '".$this->with_tax."'";
					$andFlag = true;
				}
			}

			if(trim($this->account_type)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE LIKE '".$this->account_type."%'";
				$andFlag = true;
			}
			if(trim($this->item) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID = '".$this->item."'";
				$andFlag = true;
			}elseif(trim($this->cat) != ""){
        $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
				$andFlag = true;
      }
			if(trim($this->brand_name) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
				$andFlag = true;
    	}
			if($this->billNum != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".BILL_NO = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY ".$main_table.".SALE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}
		public function fabricSaleReport(){
			$main_table = 'sale';
			$sub_table  = 'sale_details';
			$query  = " SELECT ".$main_table.".*,".$sub_table.".*, item_category.NAME AS CATEGORY_NAME ";
			$query .= " ,SUM($sub_table.CARTONS) AS CARTONS ";
			$query .= " ,SUM($sub_table.CARTONS) AS CARTONS ";
			$query .= " ,SUM($sub_table.QUANTITY) AS QUANTITY ";
			$query .= " ,SUM($sub_table.SUB_AMOUNT) AS SUB_AMOUNT ";
			$query .= " ,SUM($sub_table.TAX_AMOUNT) AS TAX_AMOUNT ";
			$query .= " ,SUM($sub_table.TOTAL_AMOUNT) AS TOTAL_AMOUNT ";

			$query .= " FROM ".$main_table." JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			$query .= " JOIN items ON ".$sub_table.".ITEM_ID = items.ID ";
			$query .= " JOIN item_category ON items.ITEM_CATG_ID  =  item_category.ITEM_CATG_ID ";
			$andFlag = false;
			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE >= DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->toDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE <= DATE('".$this->toDate."')";
				$andFlag = true;
			}
			if(trim($this->supplierAccCode)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE ='".$this->supplierAccCode."'";
				$andFlag = true;
			}
			if(trim($this->account_type)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE LIKE '".$this->account_type."%'";
				$andFlag = true;
			}
			if(trim($this->item) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID = '".$this->item."'";
				$andFlag = true;
			}elseif(trim($this->cat) != ""){
        $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
				$andFlag = true;
      }
			if(trim($this->brand_name) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
				$andFlag = true;
    	}
			if($this->billNum != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".BILL_NO = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " GROUP BY items.ITEM_CATG_ID ORDER BY ".$main_table.".SALE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}

		public function salesmanReport(){
			$main_table = 'sale';
			$sub_table  = 'sale_details';
			$query = "SELECT $main_table.*,$sub_table.*,SUM($sub_table.QUANTITY) AS TOTAL_QTY FROM ".$main_table."
					INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			$andFlag = false;
			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE = DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->supplierAccCode)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE ='".$this->supplierAccCode."'";
				$andFlag = true;
			}

			if(trim($this->user_id)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".USER_ID = '".$this->user_id."'";
				$andFlag = true;
			}

			if(trim($this->account_type)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE LIKE '".$this->account_type."%'";
				$andFlag = true;
			}
			if(trim($this->item) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID = '".$this->item."'";
				$andFlag = true;
			}elseif(trim($this->cat) != ""){
        $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
				$andFlag = true;
      }
			if(trim($this->brand_name) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
				$andFlag = true;
    	}
			if($this->billNum != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".BILL_NO = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " GROUP BY $sub_table.ITEM_ID ORDER BY ".$main_table.".SALE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}

		public function fbr_sale_report(){
			$main_table = 'sale';
			$sub_table  = 'sale_details';
			$query = "SELECT ".$main_table.".*,".$sub_table.".* FROM ".$main_table."
					INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			$andFlag = false;
			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE >= DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->toDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE <= DATE('".$this->toDate."')";
				$andFlag = true;
			}
			if(trim($this->with_tax) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".WITH_TAX = '".$this->with_tax."'";
				$andFlag = true;
			}
			if(trim($this->registered_tax) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".REGISTERED_TAX = '".$this->registered_tax."'";
				$andFlag = true;
			}
			$query .= " ORDER BY ".$main_table.".SALE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}

		public function delete($id){
			$query = "DELETE FROM sale WHERE ID = $id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteUnRelatedInventoryEntries(){
			$query = "DELETE FROM sale WHERE ID NOT IN (SELECT ID FROM sale_details) AND VOUCHER_ID = 0";
			mysql_query($query);
		}
		public function getSaleDetailArrayIdOnly($sale_id){
			$query = "SELECT ID FROM sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_assoc($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getSaleDetailArrayItemOnly($sale_id){
			$query = "SELECT ITEM_ID FROM sale_details WHERE SALE_ID = $sale_id AND SERVICE_ID = 0";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_assoc($record)){
					$returnArray[] = $row['ITEM_ID'];
				}
			}
			return $returnArray;
		}
        public function getSaleDetailArrayServiceOnly($sale_id){
			$query = "SELECT SERVICE_ID FROM sale_details WHERE SALE_ID = $sale_id AND ITEM_ID = 0";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_assoc($record)){
					$returnArray[] = $row['SERVICE_ID'];
				}
			}
			return $returnArray;
		}
		public function get_item_qty_by_bill($sale_id,$item_id){
			$query = "SELECT SUM(sale_details.QUANTITY) AS QTY FROM sale_details
					  INNER JOIN sale ON sale_details.SALE_ID = sale.ID
					  WHERE sale_details.ITEM_ID = '$item_id' AND sale.ID = '$sale_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return (float)$row['QTY'];
			}else{
				return 0;
			}
		}
		public function insertCompanyId($sale_id,$company_id){
			$query = "UPDATE `sale` SET `COMPANY_ID` = $company_id WHERE `ID` = $sale_id ";
			return mysql_query($query);
		}
		public function paymentConfirmation($sale_id,$status){
			$query = "UPDATE `sale` SET `PAYMENT_CONFIRM` = '$status' WHERE `ID` = $sale_id ";
			return mysql_query($query);
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `sale` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function saleTaxReport($report_date){
			$query = " SELECT * FROM sale_details WHERE SALE_ID IN (SELECT ID FROM sale WHERE WITH_TAX = 'Y')";
			$result= mysql_query($query);
			return $result;
		}
		public function saleTaxItemsList($report_date){
			$query = " SELECT ITEM_ID,SUM(QUANTITY) AS TOTAL_QTY  FROM sale_details
					   WHERE SALE_ID IN (SELECT ID FROM sale WHERE WITH_TAX = 'Y' AND SALE_DATE <= '".$report_date."')
					   GROUP BY ITEM_ID ";
			$result= mysql_query($query);
			return $result;
		}
		public function setDiscounts($sale_id){
			$query = "UPDATE `sale` SET
					 `BILL_AMOUNT`	 = '$this->bill_amount',
					 `DISCOUNT` 	 	 = '$this->discount',
					 `DISCOUNT_TYPE` = '$this->discount_type',
					 `SALE_DISCOUNT` = '$this->sale_discount' WHERE `ID` = $invId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function check_in_sale_returns($sale_id){
			$query  = "SELECT * FROM sale_return WHERE SALE_ID = '$sale_id' ";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM sale WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function customer_sale_report(){
			$main_table = 'sale';
			$sub_table  = 'sale_details';
			$query = "SELECT SUM(sale_details.TOTAL_AMOUNT) AS PURCHASES, ".$main_table.".*,".$sub_table.".* FROM ".$main_table."
					  INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			$andFlag = false;
			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE >= DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->toDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE <= DATE('".$this->toDate."')";
				$andFlag = true;
			}
			if(trim($this->mobile_no) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".MOBILE_NO ='".$this->mobile_no."'";
				$andFlag = true;
			}else{
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".MOBILE_NO != '' ";
				$andFlag = true;
			}
			$query .= " GROUP BY sale.MOBILE_NO ORDER BY PURCHASES DESC";
			return  mysql_query($query);
		}
		public function setSmsStatus($inventory_id,$sms_status){
			$query = "UPDATE `sale` SET `SMS_STATUS` = '$sms_status' WHERE `ID` =  $inventory_id ";
			return mysql_query($query);
		}
		public function getCustomerAmountFromVoucher($inventory_id){
			$query = " select AMOUNT FROM voucher_details WHERE SUBSTR(ACCOUNT_CODE,1,6) = '010104' AND VOUCHER_ID = (SELECT VOUCHER_ID FROM sale WHERE ID = $inventory_id) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
	}
?>
