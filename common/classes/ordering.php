<?php
	class Ordering{
		public $rbd_id;
		public $booking_id;
		public $user_id;
		public $title;
		public $order_date;
		public $deliver_date;
		public $order_status;
		public $supplier_id;
		public $total_price;
		public $advance;
		public $remaining;
		public $item_id;
		public $item_type;
		public $quantity;
		public $receiving_date;
		public $comments;
		public $fromOrderDate;
		public $toOrderDate;
		public $limit_start;
		public $design_comments;
		public $order_no;

		public function save(){
			$query = "INSERT INTO `ordering`(
																			`BOOKING_ID`,
																			`RBD_ID`,
																			`USER_ID`,
																			`TITLE`,
																			`ORDER_DATE`,
																			`DELIVERY_DATE`,
																			`ORDER_STATUS`,
																			`SUPPLIER_ID`,
																			`TOTAL_PRICE`,
																			`ADVANCE`,
																			`REMAINING`,
																			`ITEM_ID`,
																			`ITEM_TYPE`,
																			`QUANTITY`,
																			`RECEIVING_DATE`,
																			`COMMENTS`,
																			`DESIGN_COMMENTS`,
																			`ORDER_NO`
																		) VALUES (
																			'$this->booking_id',
																			'$this->rbd_id',
																			'$this->user_id',
																			'$this->title',
																			'$this->order_date',
																			'$this->deliver_date',
																			'$this->order_status',
																			'$this->supplier_id',
																			'$this->total_price',
																			'$this->advance',
																			'$this->remaining',
																			'$this->item_id',
																			'$this->item_type',
																			'$this->quantity',
																			'$this->receiving_date',
																			'$this->comments',
																			'$this->design_comments',
																			'$this->order_no'
																		)";
				mysql_query($query);
				return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `ordering` SET
																	`BOOKING_ID`		  =	'$this->booking_id',
																	`TITLE`					  =	'$this->title',
																	`ORDER_DATE`		  =	'$this->order_date',
																	`DELIVERY_DATE`	  =	'$this->deliver_date',
																	`RECEIVING_DATE`  =	'$this->receiving_date',
																	`SUPPLIER_ID`		  =	'$this->supplier_id',
																	`TOTAL_PRICE`		  =	'$this->total_price',
																	`ADVANCE`				  =	'$this->advance',
																	`REMAINING`			  =	'$this->remaining',
																	`ITEM_ID`  			  =	'$this->item_id',
																	`ITEM_TYPE`  			=	'$this->item_type',
																	`QUANTITY`			  =	'$this->quantity',
																	`COMMENTS`			  =	'$this->comments',
																	`DESIGN_COMMENTS` = '$this->design_comments'  WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function getListByRental($booking_id){
			$query = "SELECT * FROM `ordering` WHERE BOOKING_ID = '$booking_id' ";
			return mysql_query($query);
		}
		public function getBookingNumberByOrderId($order_id){
			$query = "SELECT BOOKING_ORDER_NO FROM `rental_booking`  WHERE ID IN (SELECT BOOKING_ID FROM ordering WHERE ID = '$order_id')";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return (int)$record['BOOKING_ORDER_NO'];
			}
		}
		public function getLastOrderNo(){
			$query = "SELECT ORDER_NO FROM `ordering` ORDER BY ORDER_NO DESC LIMIT 1";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return (int)$record['ORDER_NO'];
			}else{
				return "0";
			}
		}
		public function getDetail($id){
			$query = "SELECT * FROM `ordering` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function searchOrdering(){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM ordering ";
			$andFlag = false;

			if(trim($this->title) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`TITLE` LIKE '%".$this->title."%' ";
				$andFlag = true;
			}

			if(trim($this->fromOrderDate)!=""){
				$this->fromDate = date("Y-m-d 00:00:00)",strtotime($this->fromOrderDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`ORDER_DATE` >= '".$this->fromOrderDate."' ";
				$andFlag = true;
			}
			if(trim($this->toOrderDate)!=""){
				$this->toDate = date("Y-m-d 23:59:00",strtotime($this->toOrderDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`ORDER_DATE` <= '".$this->toOrderDate."' ";
				$andFlag = true;
			}

			if($this->supplier_id!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SUPPLIER_ID` = '$this->supplier_id' ";
				$andFlag = true;
			}

			if($this->item_id!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `ITEM_ID` = '$this->item_id' ";
				$andFlag = true;
			}

			$query .= " ORDER BY ID DESC ";
			if($this->limit_start == ''){
				$this->limit_start = 0;
			}
			$query .= " LIMIT $this->limit_start,$this->limit_end";

			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
    	$this->found_records = $totalRecords['total'];
    	return $result;
		}
		public function orderingReport(){
			$query = "SELECT * FROM ordering ";
			$andFlag = false;

			if(trim($this->title) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`TITLE` LIKE '%".$this->title."%' ";
				$andFlag = true;
			}

			if(trim($this->fromOrderDate)!=""){
				$this->fromOrderDate = date("Y-m-d 00:00:00)",strtotime($this->fromOrderDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`ORDER_DATE` >= '".$this->fromOrderDate."' ";
				$andFlag = true;
			}
			if(trim($this->toOrderDate)!=""){
				$this->toOrderDate = date("Y-m-d 23:59:00",strtotime($this->toOrderDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`ORDER_DATE` <= '".$this->toOrderDate."' ";
				$andFlag = true;
			}

			if($this->supplier_id!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SUPPLIER_ID` = '$this->supplier_id' ";
				$andFlag = true;
			}

			$query .= " ORDER BY ID DESC";
			$result = mysql_query($query);
			return $result;
		}

		public function statusChange($order_id,$status){
			$query = "UPDATE `ordering` SET ORDER_STATUS  = '$status' WHERE ID = '$order_id' ";
			return mysql_query($query);
		}

		public function getVoucherId($id){
			$query = "SELECT VOUCHER_ID FROM `ordering` WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function insertVoucherId($id,$voucherId){
			$query = "UPDATE `ordering` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function getStatusVoucherId($id){
			$query = "SELECT STATUS_VOUCHER FROM `ordering` WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['STATUS_VOUCHER'];
			}else{
				return 0;
			}
		}
		public function insertStatusVoucherId($id,$voucherId){
			$query = "UPDATE `ordering` SET `STATUS_VOUCHER` = $voucherId WHERE `ID` = $id ";
			return mysql_query($query);
		}

		public function delete($id){
			$query = "DELETE FROM `ordering` WHERE ID = '$id'";
			return mysql_query($query);
		}
	}
?>
