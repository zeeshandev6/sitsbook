<?php
	class InventoryReturnDetails{
		public $purchase_id;
		public $item_id;
		public $batch_no;
		public $expiry_date;
		public $cortons;
		public $perCorton;
		public $qtyReceipt;
		public $unitPrice;
		public $purchaseDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $inStock;

		public function getList($purchase_id){
			$query = "SELECT * FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			return mysql_query($query);
		}
		public function getListByPoNumber($po_number){
			$query = "SELECT ITEM_ID,SUM(STOCK_QTY) AS TOTAL_QTY FROM purchase_return_details WHERE PURCHASE_ID IN  (SELECT ID FROM purchase_return WHERE PO_NUMBER = '$po_number') GROUP BY ITEM_ID ";
			return mysql_query($query);
		}
		public function getDetails($invDetailId){
			$query = "SELECT * FROM `purchase_return_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getQtyAgainstPoItem($po_number,$item_id){
			$query = "SELECT SUM(STOCK_QTY) AS TOTAL_QTY FROM `purchase_return_details` WHERE ITEM_ID = '$item_id' AND `PURCHASE_ID` IN (SELECT ID FROM purchase_return WHERE PO_NUMBER = '$po_number') ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_QTY']=='')?0:$row['TOTAL_QTY'];
			}else{
				return 0;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT STOCK_QTY FROM `purchase_return_details` WHERE `ID` = '$invDetailId' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['STOCK_QTY']=='')?0:$row['STOCK_QTY'];
			}else{
				return 0;
			}
		}
    public function getAllQuantity($item_id){
			$query = "SELECT SUM(UNIT_PRICE) AS COST,SUM(STOCK_QTY) AS ALL_QTY FROM `purchase_return_details` WHERE `ITEM_ID` = '$item_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `purchase_return_details`( `PURCHASE_ID`,
																											 `ITEM_ID`,
																											 `BATCH_NO`,
																									 	 	 `EXPIRY_DATE`,
																											 `STOCK_QTY`,
																											 `UNIT_PRICE`,
																											 `PURCHASE_DISCOUNT`,
																											 `SUB_AMOUNT`,
																											 `TAX_RATE`,
																											 `TAX_AMOUNT`,
																											 `TOTAL_AMOUNT`)
																									 VALUES ('$this->purchase_id',
																												 	 '$this->item_id',
																													 '$this->batch_no',
																													 '$this->expiry_date',
																													 '$this->qtyReceipt',
																													 '$this->unitPrice',
																													 '$this->purchaseDiscount',
																													 '$this->subTotal',
																													 '$this->taxRate',
																													 '$this->taxAmount',
																													 '$this->totalAmount')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($purchase_detail_id){
			$query = "UPDATE `purchase_return_details` SET  `ITEM_ID`							= '$this->item_id',
																											`BATCH_NO` 						= '$this->batch_no',
																											`EXPIRY_DATE` 				= '$this->expiry_date',
																											`STOCK_QTY`						= '$this->qtyReceipt',
																											`UNIT_PRICE`					= '$this->unitPrice',
																											`PURCHASE_DISCOUNT`		= '$this->purchaseDiscount',
																											`SUB_AMOUNT`					= '$this->subTotal',
																											`TAX_RATE`						= '$this->taxRate',
																											`TAX_AMOUNT`					= '$this->taxAmount',
																											`TOTAL_AMOUNT`				= '$this->totalAmount' WHERE ID = '$purchase_detail_id' ";
			return mysql_query($query);
		}
		public function delete($purchase_detail_id){
			$query = "DELETE FROM purchase_return_details WHERE ID = $purchase_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($purchase_id){
			$query = "DELETE FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			return mysql_query($query);
		}
	}
?>
