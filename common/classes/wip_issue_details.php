<?php
	class WorkInProcessConsumeDetails {
		public $wip_id;
		public $item_id;
		public $quantity;
		public $unit_cost;
		public $gross_cost;

		public function getList($wip_id){
			$query = "SELECT * FROM wip_issue_details WHERE WIP_ID = '$wip_id'";
			return mysql_query($query);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM wip_issue_details WHERE ID = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function getJobCost($id){
			$query = "SELECT SUM(GROSS_COST) AS TOTAL_COST FROM wip_issue_details WHERE WIP_ID = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_COST'] == '')?0:$row['TOTAL_COST'];
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `wip_issue_details`(`WIP_ID`,
																								`ITEM_ID`,
																								`QUANTITY`,
																								`UNIT_COST`,
																								`GROSS_COST`)
																				VALUES ('$this->wip_id',
																								'$this->item_id',
																								'$this->quantity',
																								'$this->unit_cost',
																								'$this->gross_cost')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `wip_issue_details` SET `ITEM_ID`		= '$this->item_id',
																			   		   `QUANTITY` 	= '$this->quantity',
																							 `UNIT_COST` 	= '$this->unit_cost',
																					   	 `GROSS_COST` = '$this->gross_cost' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function updateStatus($id,$status,$completion_date){
			$query = "UPDATE `wip_issue_details` SET STATUS = '$status' , `COMPLETED_ON` = '$completion_date' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM wip_issue_details WHERE ID = '$id'";
			return mysql_query($query);
		}
	}
?>
