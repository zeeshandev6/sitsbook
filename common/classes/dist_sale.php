<?php
class DistributorSale {
    public $user_id;
    public $order_taker;
    public $saleDate;
    public $order_date;
    public $po_number;
    public $record_time;
    public $billNum;
    public $source;
    public $supplierAccCode;
    public $supplierAccTitle;
    public $bill_amount;
    public $trade_offer;
    public $discount;
    public $discount_type;
    public $sale_discount;
    public $item;
    public $fromDate;
    public $toDate;
    public $mobile_no;
    public $customer_name;
    public $charges;
    public $notes;
    public $subject;
    public $received_cash;
    public $recovered_balance;
    public $recovery_amount;
    public $change_returned;
    public $remaining_amount;
    public $with_tax;
    public $registered_tax;
    public $tax_bill_num;
    public $cat;
    public $account_type;
    public $brand_name;
    public $found_records;

    public $includeBills;


    public $area;
    public $sector;

    public function getList() {
        $query = "SELECT * FROM dist_sale ORDER BY ID DESC";
        $records = mysql_query($query);
        return $records;
    }
    public function countRows() {
        $query = "SELECT count(ID) FROM dist_sale ";
        $records = mysql_query($query);
        return $records;
    }
    public function checkBillNumber($bill_num) {
        $query = "SELECT BILL_NO FROM dist_sale WHERE BILL_NO = $bill_num ";
        $record = mysql_query($query);
        return mysql_num_rows($record);
    }
    public function getPaged($start, $total) {
        $query = "SELECT * FROM dist_sale  ORDER BY BILL_NO,SALE_DATE DESC LIMIT $start,$total";
        $records = mysql_query($query);
        return $records;
    }
    public function getRecordDetails($id) {
        $query = "SELECT * FROM dist_sale WHERE ID = $id ";
        $records = mysql_query($query);
        return $records;
    }
    public function getAny($id) {
        $query = "SELECT * FROM dist_sale WHERE ID = '$id'";
        $records = mysql_query($query);
        return $records;
    }
    public function getDetail($sale_id) {
        $query = "SELECT * FROM dist_sale WHERE ID = '$sale_id' ";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            return mysql_fetch_array($record);
        } else {
            return NULL;
        }
    }
    public function getDetailByBillNumber($bill_num) {
        $query = "SELECT * FROM dist_sale WHERE BILL_NO = $bill_num ";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            return mysql_fetch_array($record);
        } else {
            return NULL;
        }
    }
    public function genBillNumber() {
        $row = NULL;
        $row_scan = NULL;
        $last_bill_number = 1;
        if ($this->with_tax == 'Y') {
            $end_date = date('Y-m-d', strtotime($this->saleDate . ' + 1 year'));
            $query = "SELECT MAX(TAX_BILL_NO) AS S_BILL_NO FROM dist_sale WHERE SALE_DATE >= '" . $this->saleDate . "' AND SALE_DATE <= '" . $end_date . "' AND WITH_TAX = 'Y' ";
            $record = mysql_query($query);
            $row = (mysql_num_rows($record)) ? mysql_fetch_assoc($record) : NULL;
            $last_bill_number = (int)$row['S_BILL_NO'];
        } else {
            $query = "SELECT MAX(BILL_NO) AS S_BILL_NO FROM dist_sale ";
            $query_scan = "SELECT MAX(BILL_NO) AS SS_BILL_NO FROM mobile_sale";
            $record = mysql_query($query);
            $record_scan = mysql_query($query_scan);
            $row = (mysql_num_rows($record)) ? mysql_fetch_assoc($record) : NULL;
            $row_scan = (mysql_num_rows($record_scan)) ? mysql_fetch_assoc($record_scan) : NULL;
            if ($row != NULL || $row_scan != NULL) {
                $records = array_merge($row, $row_scan);
                $last_bill_number = max($records);
            }
        }
        return ++$last_bill_number;
    }
    public function save() {
        if (isset($_SESSION['classuseid']) && $this->user_id == 0) {
            $user_id = $_SESSION['classuseid'];
        } elseif ($this->user_id > 0) {
            $user_id = $this->user_id;
        } else {
            return false;
        }
        $query = "INSERT INTO `dist_sale`(`USER_ID`,
										 `ORDER_TAKER`,
										 `SALE_DATE`,
										 `ORDER_DATE`,
										 `PO_NUMBER`,
										 `RECORD_TIME`,
										 `BILL_NO`,
										 `CUST_ACC_CODE`,
										 `BILL_AMOUNT`,
                                         `TRADE_OFFER`,
										 `DISCOUNT`,
										 `DISCOUNT_TYPE`,
										 `SALE_DISCOUNT`,
										 `MOBILE_NO`,
										 `CUSTOMER_NAME`,
										 `CHARGES`,
										 `NOTES`,
										 `SUBJECT`,
										 `RECEIVED_CASH`,
										 `RECOVERED_BALANCE`,
										 `RECOVERY_AMOUNT`,
										 `CHANGE_RETURNED`,
										 `REMAINING_AMOUNT`,
									 	 `WITH_TAX`,
									 	 `REGISTERED_TAX`,
									 	 `TAX_BILL_NO`)
								  VALUES ('$user_id',
												'$this->order_taker',
									  		  	'$this->saleDate',
												'$this->order_date',
												'$this->po_number',
									  		  	'$this->record_time',
									  		  	'$this->billNum',
												'$this->supplierAccCode',
												'$this->bill_amount',
                                                '$this->trade_offer',
												'$this->discount',
												'$this->discount_type',
												'$this->sale_discount',
												'$this->mobile_no',
												'$this->customer_name',
												'$this->charges',
												'$this->notes',
												'$this->subject',
												'$this->received_cash',
												'$this->recovered_balance',
												'$this->recovery_amount',
												'$this->change_returned',
												'$this->remaining_amount',
												'$this->with_tax',
												'$this->registered_tax',
												'$this->tax_bill_num')";
        mysql_query($query);
        return mysql_insert_id();
    }
    public function update($invId) {
        if (isset($_SESSION['classuseid']) && $this->user_id == 0) {
            $user_id = $_SESSION['classuseid'];
        } elseif ($this->user_id > 0) {
            $user_id = $this->user_id;
        } else {
            return false;
        }
        $query = "UPDATE `dist_sale` SET
									`USER_ID`       		= '$this->user_id',
									`ORDER_TAKER`       	= '$this->order_taker',
									`SALE_DATE`		  	  	= '$this->saleDate',
									`ORDER_DATE`		  	= '$this->order_date',
									`PO_NUMBER`		  		= '$this->po_number',
									`BILL_NO`		  	  	= '$this->billNum',
									`CUST_ACC_CODE`   		= '$this->supplierAccCode',
									`BILL_AMOUNT`	  	  	= '$this->bill_amount',
                                    `TRADE_OFFER` 	 	  	= '$this->trade_offer',
									`DISCOUNT` 	 	  	  	= '$this->discount',
									`MOBILE_NO` 	  	  	= '$this->mobile_no',
									`DISCOUNT_TYPE`   		= '$this->discount_type',
									`SALE_DISCOUNT`   		= '$this->sale_discount',
									`CUSTOMER_NAME`   		= '$this->customer_name',
									`CHARGES` 		  	  	= '$this->charges',
									`NOTES`     	  	  	= '$this->notes',
									`SUBJECT`     	  		= '$this->subject',
									`RECEIVED_CASH`   		= '$this->received_cash',
									`RECOVERED_BALANCE` 	= '$this->recovered_balance',
									`RECOVERY_AMOUNT`   	= '$this->recovery_amount',
									`CHANGE_RETURNED` 		= '$this->change_returned',
									`REMAINING_AMOUNT`  	= '$this->remaining_amount',
									`WITH_TAX`  			= '$this->with_tax',
									`REGISTERED_TAX`  		= '$this->registered_tax',
									`TAX_BILL_NO`  			= '$this->tax_bill_num' WHERE `ID` = '$invId' ";
        $updated = mysql_query($query);
        return $updated;
    }
    public function getBranchId($sale_id) {
        $query = "SELECT BRANCH_ID FROM users WHERE ID IN (SELECT USER_ID FROM dist_sale WHERE ID = '$sale_id')";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return (int)$row['BRANCH_ID'];
        } else {
            return 0;
        }
    }
    public function getQuantityPerBill($sale_id) {
        $query = "SELECT dist_sale_details.ITEM_ID,SUM(dist_sale_details.QUANTITY) AS QTY,SUM(dist_sale_details.CARTONS) AS CARTNS,SUM(dist_sale_details.DOZENS) AS NUM_DOZENS,items.QTY_PER_CARTON AS QTY_CARTON FROM dist_sale_details
								JOIN items ON dist_sale_details.ITEM_ID = items.ID
			 					WHERE dist_sale_details.SALE_ID = '$sale_id' GROUP BY dist_sale_details.ITEM_ID ";
        $record = mysql_query($query);
        $items_array = array();
        if (mysql_num_rows($record)) {
            while ($row = mysql_fetch_array($record)) {
                $crt_qty = (float)($row['CARTNS'] * $row['QTY_CARTON']);
                $dzn_qty = (float)($row['NUM_DOZENS'] * 12);
                if (!isset($items_array[$row['ITEM_ID']])) {
                    $items_array[$row['ITEM_ID']] = 0;
                }
                $items_array[$row['ITEM_ID']] += $row['QTY'];
                $items_array[$row['ITEM_ID']] += $crt_qty;
                $items_array[$row['ITEM_ID']] += $dzn_qty;
            }
        }
        return $items_array;
    }
    public function getTotalAmount($sale_id) {
        $query = "SELECT dist_sale_details.ITEM_ID,SUM(dist_sale_details.TOTAL_AMOUNT) AS BILL_AMOUNT FROM dist_sale_details
								JOIN items ON dist_sale_details.ITEM_ID = items.ID
			 					WHERE dist_sale_details.SALE_ID = '$sale_id' GROUP BY dist_sale_details.ITEM_ID ";
        $record = mysql_query($query);
        $total_amount = 0;
        if (mysql_num_rows($record)) {
            while ($row = mysql_fetch_array($record)) {
                $total_amount+= $row['BILL_AMOUNT'];
            }
        }
        return $total_amount;
    }
    public function getLastBillByAccountCode($report_date, $party_acc_code) {
        $query = "SELECT * FROM dist_sale WHERE CUST_ACC_CODE = '$party_acc_code' AND SALE_DATE <= '$report_date' ORDER BY SALE_DATE DESC LIMIT 1 ";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            return mysql_fetch_assoc($record);
        } else {
            return NULL;
        }
    }
    public function getInventoryAmountSum($sale_id) {
        $query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM dist_sale_details WHERE SALE_ID = $sale_id";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return ($row['AMOUNT'] == '') ? 0 : $row['AMOUNT'];
        } else {
            return 0;
        }
    }
    public function getInventoryTaxAmountSum($sale_id) {
        $query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM dist_sale_details WHERE SALE_ID = $sale_id";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return ($row['TAX_AMNT'] == '') ? 0 : $row['TAX_AMNT'];
        } else {
            return 0;
        }
    }
    public function inventorySearch() {
        $query = "SELECT ID FROM dist_sale WHERE ";
        $query.= "`CUST_ACC_CODE` = '" . $this->supplierAccCode . "'";
        $query.= " AND `BILL_NO` = " . $this->billNum;
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return $row['ID'];
        } else {
            return 0;
        }
    }
    public function search($start, $per_page) {
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM dist_sale ";
        $andFlag = false;
        if ($this->fromDate != "") {
            $this->fromDate = date('Y-m-d', strtotime($this->fromDate));
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `SALE_DATE` >= '" . $this->fromDate . "'";
            $andFlag = true;
        }
        if ($this->toDate != "") {
            $this->toDate = date('Y-m-d', strtotime($this->toDate));
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `SALE_DATE` <= '" . $this->toDate . "'";
            $andFlag = true;
        }
        if (trim($this->supplierAccCode) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `CUST_ACC_CODE` = '" . $this->supplierAccCode . "' ";
            $andFlag = true;
        }
        if ($this->with_tax != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `WITH_TAX` = '" . $this->with_tax . "' ";
            $andFlag = true;
        }
        if ($this->billNum != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `BILL_NO` = '$this->billNum' ";
            $andFlag = true;
        }
        if ($this->po_number != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `PO_NUMBER` = '$this->po_number' ";
            $andFlag = true;
        }
        if ($this->order_taker != "") {
            $this->order_taker = (int)($this->order_taker);
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `ORDER_TAKER` = " . $this->order_taker;
            $andFlag = true;
        }
        if ($this->user_id != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `USER_ID` = " . $this->user_id;
            $andFlag = true;
        }
        if ($this->includeBills != "") {
            $query.= " OR `BILL_NO` IN ( $this->includeBills ) ";
            $andFlag = true;
        }
        $query.= " ORDER BY BILL_NO DESC ";
        $query.= " LIMIT $start,$per_page ";
        $result = mysql_query($query);
        $totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->found_records = $totalRecords['total'];
        return $result;
    }
    public function searchSalemanSales() {
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM dist_sale ";
        $andFlag = false;
        if ($this->fromDate != "") {
            $this->fromDate = date('Y-m-d', strtotime($this->fromDate));
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `SALE_DATE` >= '" . $this->fromDate . "'";
            $andFlag = true;
        }
        if ($this->order_taker != "") {
            $this->order_taker = (int)($this->order_taker);
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `ORDER_TAKER` = " . $this->order_taker;
            $andFlag = true;
        }
        if ($this->user_id != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " `USER_ID` = " . $this->user_id;
            $andFlag = true;
        }
        $query.= " ORDER BY BILL_NO DESC ";
        $result = mysql_query($query);
        $totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->found_records = $totalRecords['total'];
        return $result;
    }
    public function getDateByBillAndCode($bill_num, $customer_code) {
        $query = "SELECT SALE_DATE FROM `dist_sale` WHERE BILL_NO = '$bill_num' AND CUST_ACC_CODE = '$customer_code'";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return ($row['SALE_DATE'] == '') ? "" : date('d-m-Y', strtotime($row['SALE_DATE']));
        } else {
            return '';
        }
    }
    public function getDailySales($report_date) {
        $query = "SELECT dist_sale.*,dist_sale_details.*
					  FROM dist_sale INNER JOIN dist_sale_details
					  ON dist_sale.ID = dist_sale_details.SALE_ID
					  WHERE dist_sale.SALE_DATE = '" . $report_date . "' GROUP BY dist_sale_details.SALE_ID ORDER BY dist_sale_details.SALE_ID ASC";
        return mysql_query($query);
    }
    public function schemeSaleReport(){
        $main_table = 'dist_sale';
        $sub_table = 'dist_sale_details';
        $query = "SELECT " . $main_table . ".*," . $sub_table . ".* FROM " . $main_table . "
                    INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
        $query .= " INNER JOIN customers ON customers.CUST_ACC_CODE = ".$main_table.".CUST_ACC_CODE ";

        $query .= " WHERE ".$sub_table.".UNIT_PRICE = 0 ";

        $andFlag = true;
        if (trim($this->fromDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE >= DATE('" . $this->fromDate . "')";
            $andFlag = true;
        }
        if (trim($this->toDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE <= DATE('" . $this->toDate . "')";
            $andFlag = true;
        }
        if (trim($this->supplierAccCode) !== "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE ='" . $this->supplierAccCode . "'";
            $andFlag = true;
        }
        if (trim($this->user_id) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".USER_ID ='" . $this->user_id . "'";
            $andFlag = true;
        }
        if (trim($this->order_taker) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".ORDER_TAKER = '" . $this->order_taker . "' ";
            $andFlag = true;
        }
        if (trim($this->account_type) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE LIKE '" . $this->account_type . "%'";
            $andFlag = true;
        }

        if (trim($this->area) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.AREA LIKE '" . $this->area . "%'";
            $andFlag = true;
        }

        if (trim($this->sector) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.SECTOR LIKE '" . $this->sector . "%'";
            $andFlag = true;
        }

        if (trim($this->item) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID = '" . $this->item . "'";
            $andFlag = true;
        } elseif (trim($this->cat) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
            $andFlag = true;
        }
        if (trim($this->brand_name) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
            $andFlag = true;
        }
        if ($this->billNum != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".BILL_NO = " . $this->billNum;
            $andFlag = true;
        }
        $query.= " ORDER BY " . $main_table . ".SALE_DATE ASC, " . $main_table . ".BILL_NO ASC ";
        return mysql_query($query);
    }
    public function saleReport($report_type) {
        if ($report_type == 'S') {
            $main_table = 'dist_sale';
            $sub_table = 'dist_sale_details';
            $query = "SELECT " . $main_table . ".*," . $sub_table . ".* FROM " . $main_table . "
                      INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
                      
            $query .= " INNER JOIN customers ON customers.CUST_ACC_CODE = ".$main_table.".CUST_ACC_CODE ";
        } else {
            $main_table = 'dist_sale_return';
            $sub_table = 'dist_sale_return_details';
            $query = "SELECT " . $main_table . ".*, " . $main_table . ".SALE_ID AS BILL_ID," . $sub_table . ".* FROM " . $main_table . "
                      INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
            $query .= " INNER JOIN customers ON customers.CUST_ACC_CODE = ".$main_table.".CUST_ACC_CODE ";
        }
        $andFlag = false;
        if (trim($this->fromDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE >= DATE('" . $this->fromDate . "')";
            $andFlag = true;
        }
        if (trim($this->toDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE <= DATE('" . $this->toDate . "')";
            $andFlag = true;
        }
        if (trim($this->supplierAccCode) !== "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE ='" . $this->supplierAccCode . "'";
            $andFlag = true;
        }
        if (trim($this->user_id) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".USER_ID ='" . $this->user_id . "'";
            $andFlag = true;
        }
        if (trim($this->order_taker) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".ORDER_TAKER = '" . $this->order_taker . "' ";
            $andFlag = true;
        }
        if ($report_type == 'S') {
            if (trim($this->with_tax) != "") {
                $query.= ($andFlag) ? "" : " WHERE ";
                $query.= ($andFlag) ? " AND " : "";
                $query.= " " . $main_table . ".WITH_TAX = '" . $this->with_tax . "'";
                $andFlag = true;
            }
        }
        if (trim($this->account_type) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE LIKE '" . $this->account_type . "%'";
            $andFlag = true;
        }

        if (trim($this->area) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.AREA LIKE '" . $this->area . "%'";
            $andFlag = true;
        }

        if (trim($this->sector) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.SECTOR LIKE '" . $this->sector . "%'";
            $andFlag = true;
        }

        if (trim($this->item) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID = '" . $this->item . "'";
            $andFlag = true;
        } elseif (trim($this->cat) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
            $andFlag = true;
        }
        if (trim($this->brand_name) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
            $andFlag = true;
        }
        if ($this->billNum != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".BILL_NO = " . $this->billNum;
            $andFlag = true;
        }
        $query.= " ORDER BY " . $main_table . ".SALE_DATE ASC, " . $main_table . ".BILL_NO ASC ";
        return mysql_query($query);
    }
    public function saleReportGroupByBill() {
        $main_table = 'dist_sale';
        $sub_table = 'dist_sale_details';
        $query = " SELECT " . $main_table . ".*," . $sub_table . ".*";
        $query.= " ,SUM($sub_table.QUANTITY) AS QUANTITY  ";
        $query.= " ,SUM($sub_table.CARTONS) AS CARTONS  ";
        $query.= " ,SUM($sub_table.TOTAL_AMOUNT) AS TOTAL_AMOUNT  ";
        $query.= " FROM $main_table INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
        $query .= " INNER JOIN customers ON customers.CUST_ACC_CODE = ".$main_table.".CUST_ACC_CODE ";

        $andFlag = false;
        if (trim($this->fromDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE >= DATE('" . $this->fromDate . "')";
            $andFlag = true;
        }
        if (trim($this->toDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE <= DATE('" . $this->toDate . "')";
            $andFlag = true;
        }
        if (trim($this->supplierAccCode) !== "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE ='" . $this->supplierAccCode . "'";
            $andFlag = true;
        }
        if (trim($this->user_id) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".USER_ID ='" . $this->user_id . "'";
            $andFlag = true;
        }
        if (trim($this->order_taker) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".ORDER_TAKER = '" . $this->order_taker . "' ";
            $andFlag = true;
        }
        if (trim($this->account_type) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".CUST_ACC_CODE LIKE '" . $this->account_type . "%'";
            $andFlag = true;
        }
        if (trim($this->item) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID = '" . $this->item . "'";
            $andFlag = true;
        } elseif (trim($this->cat) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
            $andFlag = true;
        }
        if (trim($this->brand_name) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $sub_table . ".ITEM_ID IN ( SELECT ID FROM items WHERE COMPANY = '$this->brand_name' ) ";
            $andFlag = true;
        }
        if ($this->billNum != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".BILL_NO = " . $this->billNum;
            $andFlag = true;
        }
        if (trim($this->area) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.AREA LIKE '" . $this->area . "%'";
            $andFlag = true;
        }

        if (trim($this->sector) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.SECTOR LIKE '" . $this->sector . "%'";
            $andFlag = true;
        }
        if (trim($this->area) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.AREA LIKE '" . $this->area . "%'";
            $andFlag = true;
        }

        if (trim($this->sector) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " customers.SECTOR LIKE '" . $this->sector . "%'";
            $andFlag = true;
        }
        $query.= " GROUP BY " . $main_table . ".BILL_NO ORDER BY " . $main_table . ".SALE_DATE ASC, " . $main_table . ".BILL_NO ASC ";
        return mysql_query($query);
    }
    public function getDistributorBillDiscount($report_date, $order_taker_id, $salesman_id) {
        $query = "SELECT SUM(SALE_DISCOUNT) AS DISCOUNT,SUM(TRADE_OFFER) AS T_OFF FROM dist_sale WHERE SALE_DATE = '$report_date' ";
        if ($order_taker_id > 0) {
            $query.= " AND ORDER_TAKER = " . $order_taker_id . " ";
            $andFlag = true;
        }
        if ($salesman_id > 0) {
            $query.= " AND USER_ID = " . $salesman_id . " ";
            $andFlag = true;
        }
        $result = mysql_query($query);
        if (mysql_num_rows($result)) {
            $row = mysql_fetch_assoc($result);
            $amount = (float)($row['T_OFF']+$row['DISCOUNT']);
            return $amount;
        }
        return NULL;
    }
    public function getItemsList($report_date) {
        $query = "SELECT ITEM_ID FROM dist_sale_details WHERE SALE_ID IN (SELECT ID FROM dist_sale WHERE SALE_DATE = '$report_date'  ";
        if ($this->order_taker != '') {
            $query.= " AND ORDER_TAKER = '$this->order_taker' ";
        }
        if ($this->user_id != '') {
            $query.= " AND USER_ID = '$this->user_id' ";
        }
        $query.= " ) ";
        $result = mysql_query($query);
        $id_arr = array();
        if (mysql_num_rows($result)) {
            while ($row = mysql_fetch_assoc($result)) {
                $id_arr[] = $row['ITEM_ID'];
            }
        }
        return $id_arr;
    }
    public function getItemsListBySaleId($sale_id) {
        $query = "SELECT ITEM_ID FROM dist_sale_details WHERE SALE_ID  = '$sale_id' ";
        $result = mysql_query($query);
        $id_arr = array();
        if (mysql_num_rows($result)) {
            while ($row = mysql_fetch_assoc($result)) {
                $id_arr[] = $row['ITEM_ID'];
            }
        }
        return $id_arr;
    }
    public function distSalesmanReport() {
        $query = " SELECT * FROM dist_sale ";
        $andFlag = false;
        if (trim($this->saleDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " dist_sale.SALE_DATE = '" . $this->saleDate . "' ";
            $andFlag = true;
        }
        if (trim($this->supplierAccCode) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " dist_sale.CUST_ACC_CODE = '" . $this->supplierAccCode . "' ";
            $andFlag = true;
        }
        if (trim($this->user_id) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " dist_sale.USER_ID = '" . $this->user_id . "' ";
            $andFlag = true;
        }
        if (trim($this->order_taker) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " dist_sale.ORDER_TAKER = '" . $this->order_taker . "' ";
            $andFlag = true;
        }
        $query.= " ORDER BY dist_sale.SALE_DATE ASC, dist_sale.BILL_NO ASC ";
        return mysql_query($query);
    }
    public function fbr_sale_report() {
        $main_table = 'dist_sale';
        $sub_table = 'dist_sale_details';
        $query = "SELECT " . $main_table . ".*," . $sub_table . ".* FROM " . $main_table . "
					INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
        $andFlag = false;
        if (trim($this->fromDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE >= DATE('" . $this->fromDate . "')";
            $andFlag = true;
        }
        if (trim($this->toDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE <= DATE('" . $this->toDate . "')";
            $andFlag = true;
        }
        if (trim($this->with_tax) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".WITH_TAX = '" . $this->with_tax . "'";
            $andFlag = true;
        }
        if (trim($this->registered_tax) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".REGISTERED_TAX = '" . $this->registered_tax . "'";
            $andFlag = true;
        }
        $query.= " ORDER BY " . $main_table . ".SALE_DATE ASC, " . $main_table . ".BILL_NO ASC ";
        return mysql_query($query);
    }
    public function delete($id) {
        $query = "DELETE FROM dist_sale WHERE ID = $id LIMIT 1";
        $success = mysql_query($query);
        return $success;
    }
    public function deleteUnRelatedInventoryEntries() {
        $query = "DELETE FROM dist_sale WHERE ID NOT IN (SELECT ID FROM dist_sale_details) AND VOUCHER_ID = 0";
        mysql_query($query);
    }
    public function getSaleDetailArrayIdOnly($sale_id) {
        $query = "SELECT ID FROM dist_sale_details WHERE SALE_ID = $sale_id";
        $record = mysql_query($query);
        $returnArray = array();
        if (mysql_num_rows($record)) {
            while ($row = mysql_fetch_array($record)) {
                $returnArray[] = $row['ID'];
            }
        }
        return $returnArray;
    }
    public function getSaleDetailArrayItemOnly($sale_id) {
        $query = "SELECT ITEM_ID FROM dist_sale_details WHERE SALE_ID = $sale_id AND SERVICE_ID = 0";
        $record = mysql_query($query);
        $returnArray = array();
        if (mysql_num_rows($record)) {
            while ($row = mysql_fetch_array($record)) {
                $returnArray[] = $row['ITEM_ID'];
            }
        }
        return $returnArray;
    }
    public function getSaleDetailArrayServiceOnly($sale_id) {
        $query = "SELECT SERVICE_ID FROM dist_sale_details WHERE SALE_ID = $sale_id AND ITEM_ID = 0";
        $record = mysql_query($query);
        $returnArray = array();
        if (mysql_num_rows($record)) {
            while ($row = mysql_fetch_array($record)) {
                $returnArray[] = $row['SERVICE_ID'];
            }
        }
        return $returnArray;
    }
    public function get_item_qty_by_bill($sale_id, $item_id) {
        $query = "SELECT SUM(CARTONS) AS CARTNS,SUM(DOZENS) AS DZNS,SUM(dist_sale_details.QUANTITY) AS QTY FROM dist_sale_details
					  INNER JOIN dist_sale ON dist_sale_details.SALE_ID = dist_sale.ID
					  WHERE dist_sale_details.ITEM_ID = '$item_id' AND dist_sale.ID = '$sale_id' ";
        $result = mysql_query($query);
        if (mysql_num_rows($result)) {
            $row = mysql_fetch_array($result);
            return $row;
        } else {
            return 0;
        }
    }
    public function insertCompanyId($sale_id, $company_id) {
        $query = "UPDATE `dist_sale` SET `COMPANY_ID` = $company_id WHERE `ID` = $sale_id ";
        return mysql_query($query);
    }
    public function paymentConfirmation($sale_id, $status) {
        $query = "UPDATE `dist_sale` SET `PAYMENT_CONFIRM` = '$status' WHERE `ID` = $sale_id ";
        return mysql_query($query);
    }
    public function insertVoucherId($invId, $voucherId) {
        $query = "UPDATE `dist_sale` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
        return mysql_query($query);
    }
    public function saleTaxReport($report_date) {
        $query = " SELECT * FROM dist_sale_details WHERE SALE_ID IN (SELECT ID FROM dist_sale WHERE WITH_TAX = 'Y')";
        $result = mysql_query($query);
        return $result;
    }
    public function saleTaxItemsList($report_date) {
        $query = " SELECT ITEM_ID,SUM(QUANTITY) AS TOTAL_QTY FROM dist_sale_details WHERE SALE_ID IN (SELECT ID FROM dist_sale WHERE WITH_TAX = 'Y' AND SALE_DATE <= '" . $report_date . "') GROUP BY ITEM_ID ";
        $result = mysql_query($query);
        return $result;
    }
    public function setDiscounts($sale_id) {
        $query = "UPDATE `dist_sale` SET
					 `BILL_AMOUNT`	 = '$this->bill_amount',
					 `DISCOUNT` 	 	 = '$this->discount',
					 `DISCOUNT_TYPE` = '$this->discount_type',
					 `SALE_DISCOUNT` = '$this->sale_discount' WHERE `ID` = $invId";
        $updated = mysql_query($query);
        return $updated;
    }
    public function check_in_sale_returns($sale_id) {
        $query = "SELECT * FROM dist_sale_return WHERE SALE_ID = '$sale_id' ";
        $result = mysql_query($query);
        return mysql_num_rows($result);
    }
    public function getVoucherId($inventory_id) {
        $query = "SELECT VOUCHER_ID FROM dist_sale WHERE ID = $inventory_id";
        $record = mysql_query($query);
        if (mysql_num_rows($record)) {
            $row = mysql_fetch_array($record);
            return $row['VOUCHER_ID'];
        } else {
            return 0;
        }
    }
    public function customer_sale_report() {
        $main_table = 'dist_sale';
        $sub_table = 'dist_sale_details';
        $query = "SELECT SUM(dist_sale_details.TOTAL_AMOUNT) AS PURCHASES, " . $main_table . ".*," . $sub_table . ".* FROM " . $main_table . "
					  INNER JOIN " . $sub_table . " ON " . $main_table . ".ID = " . $sub_table . ".SALE_ID ";
        $andFlag = false;
        if (trim($this->fromDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE >= DATE('" . $this->fromDate . "')";
            $andFlag = true;
        }
        if (trim($this->toDate) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".SALE_DATE <= DATE('" . $this->toDate . "')";
            $andFlag = true;
        }
        if (trim($this->mobile_no) != "") {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".MOBILE_NO ='" . $this->mobile_no . "'";
            $andFlag = true;
        } else {
            $query.= ($andFlag) ? "" : " WHERE ";
            $query.= ($andFlag) ? " AND " : "";
            $query.= " " . $main_table . ".MOBILE_NO != '' ";
            $andFlag = true;
        }
        $query.= " GROUP BY dist_sale.MOBILE_NO ORDER BY PURCHASES DESC";
        return mysql_query($query);
    }
}
?>