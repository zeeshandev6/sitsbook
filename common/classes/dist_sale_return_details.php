<?php
	class DistributorSaleReturnDetails{
		public $sale_id;
		public $item_id;
		public $cortons;
		public $rate_carton;
		public $dozens;
		public $dozen_price;
		public $qtyReceipt;
		public $unitPrice;
		public $saleDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $inStock;

		public function getList($sale_id){
			$query = "SELECT * FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}
		public function getListByPoNumber($po_number){
			$query = "SELECT dist_sale_return_details.ITEM_ID,items.QTY_PER_CARTON,
							 SUM(dist_sale_return_details.QUANTITY) AS TOTAL_QTY,
							 SUM(dist_sale_return_details.CARTONS)  AS TOTAL_CARTONS
							 FROM dist_sale_return_details
							 JOIN items ON items.ID = dist_sale_return_details.ITEM_ID 
							 WHERE dist_sale_return_details.SALE_ID IN  (SELECT ID FROM dist_sale_return WHERE PO_NUMBER = '$po_number') GROUP BY dist_sale_return_details.ITEM_ID ";
			return mysql_query($query);
		}
		public function getDetails($invDetailId){
			$query = "SELECT * FROM `dist_sale_return_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT QUANTITY FROM `dist_sale_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['QUANTITY']=='')?0:$row['QUANTITY'];
			}else{
				return 0;
			}
		}
    public function getAllQuantity($item_id){
			$query = "SELECT SUM(UNIT_PRICE) AS PRICE,SUM(QUANTITY) AS ALL_QTY FROM `dist_sale_return_details` WHERE `ITEM_ID` = '$item_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function getHistoryCostPrice($sale_id,$item_id){
			if($sale_id == 0){
				$query = "SELECT `PURCHASE_PRICE` FROM `items` WHERE `ID` = '$item_id'";
				$record = mysql_query($query);
				if(mysql_num_rows($record)){
					$row = mysql_fetch_array($record);
					return ($row['PURCHASE_PRICE'] == '')?0:$row['PURCHASE_PRICE'];
				}else{
					return 0;
				}
			}else{
				$query  = "SELECT COST_PRICE FROM dist_sale_details WHERE ITEM_ID = '$item_id' AND SALE_ID = '$sale_id' ";
				$result = mysql_query($query);
				if(mysql_num_rows($result)){
					$row = mysql_fetch_array($result);
					return $row['COST_PRICE'];
				}else{
					return 0;
				}
			}
		}
		public function getSaleDetailsByItemsPerDate($report_date,$item_id){
			$query = "SELECT SUM(CARTONS) AS TOTAL_CARTONS,SUM(QUANTITY) AS TOTAL_QTY,RATE_CARTON,UNIT_PRICE FROM dist_sale_return_details WHERE ITEM_ID = '$item_id' AND SALE_ID IN (SELECT ID FROM dist_sale_return WHERE SALE_DATE = '$report_date')";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			return NULL;
		}
		public function save(){
			$query = "INSERT INTO `dist_sale_return_details`(`SALE_ID`,
													 `ITEM_ID`,
													 `CARTONS`,
													 `RATE_CARTON`,
													 `DOZENS`,
													 `DOZEN_PRICE`,
													 `QUANTITY`,
													 `UNIT_PRICE`,
													 `SALE_DISCOUNT`,
													 `SUB_AMOUNT`,
													 `TAX_RATE`,
													 `TAX_AMOUNT`,
													 `TOTAL_AMOUNT`)
											 VALUES ($this->sale_id,
												 	 $this->item_id,
													 $this->cartons,
													 $this->rate_carton,
													 $this->dozens,
													 $this->dozen_price,
													 $this->qtyReceipt,
													 $this->unitPrice,
													 $this->saleDiscount,
													 $this->subTotal,
													 $this->taxRate,
													 $this->taxAmount,
													 $this->totalAmount)";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($sale_detail_id){
			$query = "UPDATE `dist_sale_return_details`
											SET `ITEM_ID`  			= '$this->item_id',
												`CARTONS` 			= '$this->cartons',
												`RATE_CARTON` 		= '$this->rate_carton',
												`DOZENS` 			= '$this->dozens',
												`DOZEN_PRICE` 		= '$this->dozen_price',
												`QUANTITY`			= '$this->qtyReceipt',
												`UNIT_PRICE`		= '$this->unitPrice',
												`SALE_DISCOUNT`		= '$this->saleDiscount',
												`SUB_AMOUNT`		= '$this->subTotal',
												`TAX_RATE`			= '$this->taxRate',
												`TAX_AMOUNT`		= '$this->taxAmount',
												`TOTAL_AMOUNT`		= '$this->totalAmount' WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function delete($sale_detail_id){
			$query = "DELETE FROM dist_sale_return_details WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($sale_id){
			$query = "DELETE FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}
	}
?>
