<?php
    class BranchStock{
        public $item_id;
        public $branch_id;
        public $stock_qty;

        public function getList(){
            $query = "SELECT * FROM branch_stock ";
            return mysql_query($query);
        }
        public function record_exists($branch_id,$item_id){
            $query = "SELECT ID FROM branch_stock WHERE BRANCH_ID = $branch_id AND ITEM_ID = $item_id ";
            return mysql_num_rows(mysql_query($query));
        }
        public function save(){
            $query = "INSERT INTO `branch_stock`( `ITEM_ID`,
                                                  `BRANCH_ID`,
                                                  `STOCK_QTY`)
                                           VALUES ('$this->item_id',
                                                   '$this->branch_id',
                                                   '$this->stock_qty')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `branch_stock` SET `ITEM_ID`   = '$this->item_id',
                                                `BRANCH_ID` = '$this->branch_id',
                                                `STOCK_QTY` = '$this->stock_qty' WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function getBranchStock($branch_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE BRANCH_ID = '$branch_id' ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function getItemCategoryStockGodowns($category_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE  ITEM_ID IN (SELECT ID FROM `items` WHERE ITEM_CATG_ID = $category_id) AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'G') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function getItemCategoryCostGodowns($category_id){
            $query = "SELECT SUM(TOTAL_COST) AS COSTA FROM `branch_stock` WHERE  ITEM_ID IN (SELECT ID FROM `items` WHERE ITEM_CATG_ID = $category_id) AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'G') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['COSTA'];
            }else{
                return 0;
            }
        }
        public function getItemStockGodowns($item_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE  ITEM_ID = '$item_id' AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'G') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function getItemCategoryStockBranches($category_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE  ITEM_ID IN (SELECT ID FROM `items` WHERE ITEM_CATG_ID = $category_id) AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'B') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function getItemCategoryCostBranches($category_id){
            $query = "SELECT SUM(TOTAL_COST) AS COSTA FROM `branch_stock` WHERE  ITEM_ID IN (SELECT ID FROM `items` WHERE ITEM_CATG_ID = $category_id) AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'B') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['COSTA'];
            }else{
                return 0;
            }
        }
        public function getItemStockBranches($item_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE  ITEM_ID = '$item_id' AND BRANCH_ID IN (SELECT ID FROM branches WHERE BRANCH_TYPE = 'B') ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function getItemStock($branch_id,$item_id){
            $query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock` WHERE BRANCH_ID = '$branch_id' AND ITEM_ID = '$item_id' ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row   = mysql_fetch_assoc($result);
                return (float)$row['STOCK'];
            }else{
                return 0;
            }
        }
        public function addStock($item_id,$branch_id,$stock,$total_cost){
            $exists = $this->record_exists($branch_id,$item_id);
            if($exists==0){
                $this->item_id   = $item_id;
                $this->branch_id = $branch_id;
                $this->stock_qty = 0;
                $exists = $this->save();
            }
            if($exists){
                $query  = "UPDATE `branch_stock` SET `STOCK_QTY` = STOCK_QTY + $stock, TOTAL_COST = TOTAL_COST + $total_cost WHERE ITEM_ID = '$item_id' AND BRANCH_ID = '$branch_id'";
                $exists = mysql_query($query);
                if($exists){
                    $this->calculateAvgPrice($item_id,$branch_id);
                }
            }else{
                $exists = false;
            }
            return $exists;
        }
        public function removeStock($item_id,$branch_id,$stock,$total_cost){
            $exists = $this->record_exists($branch_id,$item_id);
            if($exists==0){
                $this->item_id   = $item_id;
                $this->branch_id = $branch_id;
                $this->stock_qty = 0;
                $exists = $this->save();
            }
            if($exists){
                $query = "UPDATE `branch_stock` SET `STOCK_QTY` = STOCK_QTY - $stock, TOTAL_COST = TOTAL_COST - $total_cost WHERE ITEM_ID = $item_id AND BRANCH_ID = $branch_id ";
                $result= mysql_query($query);
                if($result){
                    $this->calculateAvgPrice($item_id,$branch_id);
                }
            }else{
                $exists = false;
            }
            return $exists;
        }
        public function calculateAvgPrice($item_id,$branch_id){
    			$query = "UPDATE branch_stock SET PURCHASE_PRICE = TOTAL_COST/STOCK_QTY WHERE BRANCH_ID = $branch_id AND ITEM_ID = $item_id";
    			return mysql_query($query);
    		}
        public function delete($id){
            $query = "DELETE FROM `branch_stock` WHERE ID = '$id' ";
            return mysql_query($query);
        }
    }
?>
