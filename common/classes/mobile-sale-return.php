<?php
	class ScanSaleReturn{
		public $return_date;
		public $ms_id;
		public $sale_invoice_link;
		public $voucher_id;
		
		public function getList(){
			$query = "SELECT * FROM mobile_sale_return ORDER BY RETURN_DATE ";
			return mysql_query($query);
		}
		public function getReturns($ms_id){
			$query = "SELECT * FROM mobile_sale_return WHERE MS_ID = '$ms_id' ";
			return mysql_query($query);
		}
		public function getDetails($id){
			$query = "SELECT * FROM mobile_sale_return WHERE ID = '$id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `mobile_sale_return`(`RETURN_DATE`, 
														   `MS_ID`,
														   `SALE_INVOICE_LINK`, 
														   `VOUCHER_ID`) 
													VALUES ('$this->return_date',
															'$this->ms_id',
															'$this->sale_invoice_link',
															'$this->voucher_id')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `mobile_sale_return` SET `RETURN_DATE` = '$this->return_date' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function insertVoucherId($mr_id,$voucherId){
			$query = "UPDATE `mobile_sale_return` SET `VOUCHER_ID` = $voucherId WHERE `ID` = '$mr_id'";
			return mysql_query($query);
		}
		public function getVoucherId($mr_id){
			$query = "SELECT VOUCHER_ID FROM mobile_sale_return WHERE ID = '$mr_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function delete($id){
			$query = "DELETE FROM `mobile_sale_return` WHERE ID = '$id'";
			return mysql_query($query);
		}
	}
?>