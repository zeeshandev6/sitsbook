<?php
  class SitsbookSmsApi{

    public $location;
    public $uri;


    public function postSoapRequest($user_name,$admin_sms,$mobile,$sms_body,$no_of_sms,$acc_id){
      $options = array();
    	$options['location'] = ($this->location=='')?$this->getDefaultLocation():$this->location;
    	$options['uri'] 		 = ($this->location=='')?$this->getDefaultUri():$this->location;
    	$api 								 = new SoapClient(NULL, $options);
    	try{
        return $api->process($user_name,$admin_sms,'',$mobile,$sms_body,$no_of_sms,$acc_id);
    	}catch(Exception $e){
    		return false;
    	}
    }

    private function getDefaultLocation(){
      return 'http://sitsol.net/softwares/smsserver/admin/sms-link/soap-server.php';
    }
    private function getDefaultUri(){
      return 'http://sitsol.net/softwares/smsserver/admin/sms-link/';
    }
  }
?>
