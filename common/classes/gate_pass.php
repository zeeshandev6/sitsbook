<?php
	class GatePass{
		public $gp_no;
		public $entry_date;
		public $lot_no;
		public $to_address;

		public $fromDate;
		public $toDate;
		public $design_no;

		public $found_records;

		public function getList(){
			$query = "SELECT * FROM gate_pass ORDER BY ENTRY_DATE";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($id){
			$query 	 = "SELECT * FROM gate_pass WHERE `ID` = '$id' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row;
			}
		}
		public function genGatePassNumber(){
			$query 	 = "SELECT MAX(GP_NO) AS GP_MAX FROM gate_pass  ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				$row['GP_MAX']+=1;
				return $row['GP_MAX'];
			}else{
				return 1;
			}
		}
		public function save(){
			$query = "INSERT INTO `gate_pass`(`GP_NO`,
																				`ENTRY_DATE`,
																				`LOT_NO`,
																				`TO_ADDRESS`)
																VALUES ('$this->gp_no',
																				'$this->entry_date',
																				'$this->lot_no',
																				'$this->to_address')";
			$inserted = mysql_query($query);
			return $inserted;
		}
		public function update($id){
			$query = "UPDATE `gate_pass` SET `GP_NO`  		= '$this->gp_no',
																			 `ENTRY_DATE` = '$this->entry_date',
																			 `LOT_NO` 		= '$this->lot_no',
																			 `TO_ADDRESS` = '$this->to_address' WHERE ID = '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function search($start,$per_page){
			$query   = "SELECT SQL_CALC_FOUND_ROWS * FROM `gate_pass` ";
			$andFlag = false;

			if($this->fromDate != ""){
				$this->fromDate = date('Y-m-d',strtotime($this->fromDate));
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " `ENTRY_DATE` >= '".$this->fromDate."'" ;
				$andFlag = true;
			}
			if($this->toDate!=""){
				$this->toDate = date('Y-m-d',strtotime($this->toDate));
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " `ENTRY_DATE` <= '".$this->toDate."'";
				$andFlag = true;
			}
			if($this->gp_no!=""){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " `GP_NO` = '".$this->gp_no."' " ;
				$andFlag = true;
			}
			$query    .= " ORDER BY ENTRY_DATE DESC ";
			$query    .= " LIMIT $start,$per_page ";
			$result    =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];
			return $result;
		}
		public function report(){
			$query   = "SELECT * FROM `gate_pass` JOIN `gate_pass_details` ON gate_pass_details.GP_ID = gate_pass.ID ";
			$andFlag = false;

			if($this->fromDate != ""){
				$this->fromDate = date('Y-m-d',strtotime($this->fromDate));
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " gate_pass.`ENTRY_DATE` >= '".$this->fromDate."'" ;
				$andFlag = true;
			}
			if($this->toDate != ""){
				$this->toDate = date('Y-m-d',strtotime($this->toDate));
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " gate_pass.`ENTRY_DATE` <= '".$this->toDate."'";
				$andFlag = true;
			}
			if($this->gp_no!=""){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " gate_pass.`GP_NO` = '".$this->gp_no."' " ;
				$andFlag = true;
			}
			if($this->lot_no!=""){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " gate_pass.`LOT_NO` = '".$this->lot_no."' " ;
				$andFlag = true;
			}
			if($this->design_no != ""){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " gate_pass_details.`PARTICULARS` = '".$this->design_no."' " ;
				$andFlag = true;
			}
			$query    .= " ORDER BY gate_pass.ENTRY_DATE DESC ";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM gate_pass WHERE ID ='$id' ";
			return mysql_query($query);
		}
	}
?>
