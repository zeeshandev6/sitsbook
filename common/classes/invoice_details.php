<?php
	class InvoiceDetails{

		public $invoice_id;
		public $description;
		public $quantity;
		public $amount;
		public $purchase_entry;

		public $found_records;
		public $from_date;
		public $to_date;

		public function getList($invoice_id){
			$query = "SELECT * FROM invoice_details WHERE INVOICE_ID = '$invoice_id' ORDER BY ID ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($ped_id){
			$query  = "SELECT * FROM invoice_details WHERE ID = '$ped_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `invoice_details`(		`INVOICE_ID`,
																									`DESCRIPTION`,
																									`QUANTITY`,
																									`AMOUNT`,
																									`PURCHASE_ENTRY`)
																					VALUES ('$this->invoice_id',
																									'$this->description',
																									'$this->quantity',
																									'$this->amount',
																									'$this->purchase_entry')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `invoice_details` SET  `DESCRIPTION` 		= '$this->description',
																							`QUANTITY`    		= '$this->quantity',
																							`AMOUNT`      		= '$this->amount',
																							`PURCHASE_ENTRY`  = '$this->purchase_entry' WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `invoice_details` WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function deleteByMain($invoice_id){
			$query = "DELETE FROM `invoice_details` WHERE INVOICE_ID = '$invoice_id' ";
			return mysql_query($query);
		}
	}
?>
