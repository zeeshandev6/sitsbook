<?php
	class ScanPurchaseDetails
	{
		public $sp_id;
		public $barcode;
		public $item_id;
		public $colour;
		public $purchase_price;
		public $discount;
		public $sub_total;
		public $tax;
		public $price;
		public $sale_price;
		public $stock_status;
		public $expiry_date;

		public function getList($sp_id){
			$query = "SELECT * FROM mobile_purchase_details WHERE SP_ID = $sp_id";
			return mysql_query($query);
		}
		public function getListIDArray($sp_id){
			$query = "SELECT ID FROM mobile_purchase_details WHERE SP_ID = $sp_id";
			$result = mysql_query($query);
			$new_array = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_array($result)){
					$new_array[] = $row['ID'];
				}
			}
			return $new_array;
		}
		public function getLastBarcode(){
			$query = "SELECT BARCODE FROM `mobile_purchase_details` ORDER BY ID DESC LIMIT 1 ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['BARCODE'];
			}else{
				return '';
			}
		}
		public function getDetails($sp_detail_id){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE `ID` = $sp_detail_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailsBySaleOrderItem($item_id,$already_included){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE `ITEM_ID` = '$item_id' AND STOCK_STATUS = 'A' ";
			if(count($already_included)>0){
				if(count($already_included)==1&&$already_included[0]!=''){
					$query .= " AND ID NOT IN (".$already_included[0].") ";
				}else{
					$already_included = implode(',',$already_included);
					if($already_included!=''){
						$query .= " AND ID NOT IN (".$already_included.") ";
					}
				}
			}
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailsByScanSaleDetailId($msd_id){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE `ID` IN (SELECT SP_DETAIL_ID FROM mobile_sale_details WHERE ID = '$msd_id') ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getPurchaseSalePrice($item_id){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE `ITEM_ID` = '$item_id' ORDER BY ID DESC LIMIT 1 ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailsByBarcode($imei){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE `BARCODE` = '$imei' ";
			$record = mysql_query($query);
			return $record;
		}
		public function check_stock_status($sp_id,$status){
			$query = "SELECT STOCK_STATUS FROM mobile_purchase_details WHERE SP_ID = $sp_id AND STOCK_STATUS != '$status' ";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function check_barcode($barcode){
			$query = "SELECT ID FROM `mobile_purchase_details` WHERE BARCODE = '$barcode' ";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function check_barcodeExclude_id($barcode,$id){
			$query = "SELECT ID FROM `mobile_purchase_details` WHERE BARCODE = '$barcode' AND ID != '$id' AND  STOCK_STATUS = 'A'";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function get_details_by_barcode($barcode,$status){
			$query = "SELECT * FROM `mobile_purchase_details` WHERE BARCODE = '$barcode'  ";
			$query .= (isset($status) && $status != '')?" AND STOCK_STATUS = '".$status."' ":"";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `mobile_purchase_details`(`SP_ID`,
															 `BARCODE`,
															 `ITEM_ID`,
															 `COLOUR`,
															 `PURCHASE_PRICE`,
															 `DISCOUNT`,
															 `SUB_TOTAL`,
															 `TAX`,
															 `PRICE`,
															 `SALE_PRICE`,
															 `STOCK_STATUS`,
															 `EXPIRY_DATE`)
													 VALUES ($this->sp_id,
														 	 '$this->barcode',
															 '$this->item_id',
															 '$this->colour',
															 '$this->purchase_price',
															 '$this->discount',
															 '$this->sub_total',
															 '$this->tax',
															 '$this->price',
															 '$this->sale_price',
															 '$this->stock_status',
															 '$this->expiry_date')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($purchase_detail_id){
			$query = "UPDATE `mobile_purchase_details` SET   `BARCODE`    	 = '$this->barcode',
															 `ITEM_ID`    	 =  $this->item_id,
															 `COLOUR`    	 = '$this->colour',
															 `PURCHASE_PRICE`= '$this->purchase_price',
															 `DISCOUNT` 	 = '$this->discount',
															 `SUB_TOTAL` 	 = '$this->sub_total',
															 `TAX`   		 = '$this->tax',
															 `PRICE`   	  	 = '$this->price',
															 `SALE_PRICE` 	 = '$this->sale_price',
															 `STOCK_STATUS`  = '$this->stock_status',
															 `EXPIRY_DATE`   = '$this->expiry_date' WHERE ID = $purchase_detail_id";
			return mysql_query($query);
		}
		public function get_bill_amount($sp_id){
			$query = "SELECT SUM(PRICE) AS TOTAL_AMOUNT FROM `mobile_purchase_details` WHERE SP_ID = '$sp_id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getCostPrice($id){
			$query = "SELECT PRICE FROM `mobile_purchase_details` WHERE ID = '$id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['PRICE'];
			}else{
				return 0;
			}
		}
		public function get_item_stock($item_id){
			$query = "SELECT COUNT(*) AS TOTAL_STOCK FROM `mobile_purchase_details` WHERE ITEM_ID = '$item_id' AND STOCK_STATUS = 'A'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['TOTAL_STOCK'];
			}else{
				return 0;
			}
		}

		public function get_average_purchase_price($item_id){
			$query = "SELECT SUM(PURCHASE_PRICE)/COUNT(*) AS AVG_PRICE FROM `mobile_purchase_details` WHERE ITEM_ID = '$item_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return number_format($row['AVG_PRICE'],2);
			}else{
				return 0;
			}
		}

		public function set_status($msd_id,$status){
			$query = "UPDATE `mobile_purchase_details` SET STOCK_STATUS = '$status' WHERE ID IN (SELECT SP_DETAIL_ID FROM mobile_sale_details WHERE ID = '$msd_id') ";
			return mysql_query($query);
		}
		public function set_status_by_spdid($sp_detail_id,$status){
			$query = "UPDATE `mobile_purchase_details` SET STOCK_STATUS = '$status' WHERE ID = '$sp_detail_id' ";
			return mysql_query($query);
		}
		public function delete($purchase_detail_id){
			$query = "DELETE FROM mobile_purchase_details WHERE ID = $purchase_detail_id";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function deleteInStock($purchase_detail_id){
			$query = "DELETE FROM mobile_purchase_details WHERE ID = '$purchase_detail_id' AND STOCK_STATUS = 'A'";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function deleteCompleteBill($sp_id){
			$query = "DELETE FROM mobile_purchase_details WHERE SP_ID = $sp_id";
			return mysql_query($query);
		}
		public function getTaxAmountById($purchase_detail_id){
			$query = "SELECT ((SUB_TOTAL*TAX)/100) AS TAX_AMOUNT FROM mobile_purchase_details WHERE ID = '$purchase_detail_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return number_format($row['TAX_AMOUNT'],2,'.','');
			}else{
				return 0;
			}
		}
		public function get_item_stock_by_category($category_id){
			$query = "SELECT COUNT(*) AS TOTAL_STOCK FROM `mobile_purchase_details` WHERE ITEM_ID IN  (SELECT ID FROM items WHERE ITEM_CATG_ID = '$category_id') AND STOCK_STATUS = 'A' ";
			$result= mysql_query($query);
			echo mysql_error();
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['TOTAL_STOCK'];
			}else{
				return 0;
			}
		}
		public function getCostPriceExcludeTax($id){
			$query = "SELECT (PRICE - ((SUB_TOTAL*TAX)/100)) AS COST FROM `mobile_purchase_details` WHERE ID = '$id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['COST'];
			}else{
				return 0;
			}
		}
	}
?>
