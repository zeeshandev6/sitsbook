<?php

class ProcessingReportDetails{

    public $sc_id;
    public $pc_id;
    public $pr_id;
    public $quality;
    public $issue;
    public $fresh;
    public $sample;
    public $b_grade;
    public $c_p;
    public $received;
    public $difference;

    public function getList(){
        $query = "SELECT * FROM processing_report_details";
        return mysql_query($query);
    }

    public function getListByPrId($pr_id){
        $query = "SELECT * FROM processing_report_details WHERE PR_ID = '$pr_id' ORDER By ID DESC";
        return mysql_query($query);
    }

    public function getRecordDetailsById($id){
        $query = "SELECT * FROM processing_report_details WHERE ID = '$id'";
        return mysql_query($query);
    }

    public function get_weights_sum($sp_id){
        $query  = "SELECT SUM(NET_WEIGHT) AS NWEIGHT,SUM(GROSS_WEIGHT) AS GWEIGHT  FROM processing_report_details WHERE RP_ID = '$sp_id'";
        $result = mysql_query($query);
        echo mysql_error();
        if(mysql_num_rows($result)){
            return mysql_fetch_assoc($result);
        }else{
            return NULL;
        }
    }

    public function save(){
        $query = "INSERT INTO `processing_report_details`(
                                  `PR_ID`,
                                  `QUALITY`,            `ISSUE`,           `FRESH`,
                                  `SAMPLE`,             `B_GRADE`,         `C_P`,
                                  `RECEIVED`,           `DIFERENCE`

                                  )
                          VALUES ('$this->pr_id',
                                  '$this->quality',     '$this->issue',     '$this->fresh',
                                  '$this->sample',      '$this->b_grade',   '$this->c_p',
                                  '$this->received',    '$this->difference' )";
        return mysql_query($query);
    }

    public function update($id){
        $query = "UPDATE `processing_report_details` SET
                                `QUALITY`        = '$this->quality',        `ISSUE`       = '$this->issue',
                                `FRESH`          = '$this->fresh',
                                `SAMPLE`         = '$this->sample',         `B_GRADE`     = '$this->b_grade',
                                `C_P`            = '$this->c_p',            `RECEIVED`    = '$this->received',
                                `DIFERENCE`      = '$this->difference'       WHERE ID     = '$id'";
        return mysql_query($query);
    }

    public function delete($id){
        $query = "DELETE FROM processing_report_details WHERE ID = '".$id."'";
        return mysql_query($query);
    }
}
?>
