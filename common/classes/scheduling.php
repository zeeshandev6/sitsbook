<?php
	class Scheduling{
		public $customer_id;
		public $item_id;
		public $from_date;
		public $to_date;
		public $product_amount;
		public $rate;
		public $total_amount;
		public $advance;
		public $remaining_amount;
		public $documentation_amount;
		public $installment_tenure;
		public $monthly_installment;
		public $notes;
		public $schedule_status;

		public function get_list(){
			$query = "SELECT * FROM `scheduling` ORDER BY ID DESC ";
			return mysql_query($query);
		}

		public function get_details($id){
			$query  = "SELECT * FROM `scheduling` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}else{
				return NULL;
			}
		}

		public function save(){
			$query = "INSERT INTO `scheduling`(`CUSTOMER_ID`, 
											   `ITEM_ID`, 
											   `FROM_DATE`, 
											   `TO_DATE`, 
											   `PRODUCT_AMOUNT`, 
											   `RATE`, 
											   `TOTAL_PRICE`, 
											   `ADVANCE`, 
											   `REMAINING_AMOUNT`, 
											   `DOCUMENTATION_AMOUNT`, 
											   `INSTALLMENT_TENURE`, 
											   `MONTHLY_INSTALLMENT`, 
											   `NOTES`, 
											   `SCHEDULE_STATUS`) 
										VALUES ('$this->customer_id',
												'$this->item_id',
												'$this->from_date',
												'$this->to_date',
												'$this->product_amount',
												'$this->rate',
												'$this->total_amount',
												'$this->advance',
												'$this->remaining_amount',
												'$this->documentation_amount',
												'$this->installment_tenure',
												'$this->monthly_installment',
												'$this->notes',
												'$this->schedule_status')";
			mysql_query($query);
			return mysql_insert_id();
		}

		public function update($id){
			$query = "UPDATE `scheduling` SET `CUSTOMER_ID` = '$this->customer_id',
											  `ITEM_ID`		= '$this->item_id',
											  `FROM_DATE`	= '$this->from_date',
											  `TO_DATE`		= '$this->to_date',
											  `PRODUCT_AMOUNT` = '$this->product_amount',
											  `RATE`		= '$this->rate',
											  `TOTAL_PRICE`	= '$this->total_amount',
											  `ADVANCE`		= '$this->advance',
											  `REMAINING_AMOUNT` = '$this->remaining_amount',
											  `DOCUMENTATION_AMOUNT` = '$this->documentation_amount',
											  `INSTALLMENT_TENURE` = '$this->installment_tenure',
											  `MONTHLY_INSTALLMENT` = '$this->monthly_installment',
											  `NOTES` = '$this->notes',
											  `SCHEDULE_STATUS` = '$this->schedule_status' WHERE ID = '$id' ";
			return mysql_query($query);
		}

		public function delete($id){
			$query = "DELETE FROM `scheduling` WHERE ID = '$id' LIMIT 1";
			return mysql_query($query);
		}
	}

?>