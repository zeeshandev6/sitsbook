<?php
	class machineSales{
		public $entry_date;
		public $machine_id;
		public $issued_length;
		public $issued_amount;
		public $clothLength;
		public $measure;
		public $unitRate;
		public $amount;

		public function getMachineSalesList(){
			$query = "SELECT * FROM `machine_sales`";
			return mysql_query($query);
		}
		public function getMachineSalePages($start,$end){
			$query = "SELECT * FROM `machine_sales` ORDER BY MACHINE_ID ASC LIMIT $start,$end ";
			return mysql_query($query);
		}
		public function getDetails($id){
			$query = "SELECT * FROM `machine_sales` WHERE `ID` = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailsByMachine($id){
			$query = "SELECT * FROM `machine_sales` WHERE `MACHINE_ID` = $id";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT COUNT(*) AS NUM_ROWS FROM machine_sales";
			$record = mysql_query($query);
			return $record;
		}
		public function getMonthEndMachineSale($thisDate){
			$nextDay = date('d',strtotime($thisDate."+1 days"));
			$query = "SELECT * FROM `machine_sales` WHERE ENTRY_DATE = '$thisDate'";
			$record = mysql_query($query);
			if($nextDay==01&&mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return '';
			}
		}
		public function getMonthEndMachineSaleSingle($thisDate,$machine_id){
			$nextDay = date('d',strtotime($thisDate."+1 days"));
			$query = "SELECT SUM(CLOTH_LENGTH) AS CLOTH_LENGTH ,SUM(AMOUNT) AS AMOUNT FROM `machine_sales` WHERE ENTRY_DATE = '$thisDate' AND MACHINE_ID = '$machine_id'";
			$record = mysql_query($query);
			if($nextDay==01&&mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return '';
			}
		}
		public function getPreviousMonthMachineSale($fromDate){
			$prevDate = date('Y-m-d',strtotime($fromDate."-1 day"));
			$query = "SELECT SUM(CLOTH_LENGTH) AS CLOTH_LENGTH ,SUM(AMOUNT) AS AMOUNT  FROM `machine_sales` WHERE ENTRY_DATE = '$prevDate'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return '';
			}
		}
		public function getPreviousMonthMachineSaleSingle($fromDate,$machine_id){
			$prevDate = date('Y-m-d',strtotime($fromDate."-1 day"));
			$query = "SELECT SUM(CLOTH_LENGTH) AS CLOTH_LENGTH ,SUM(AMOUNT) AS AMOUNT FROM `machine_sales` WHERE ENTRY_DATE = '$prevDate' AND MACHINE_ID = $machine_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return '';
			}
		}
		public function getMachineSalesDetail($id){
			$query = "SELECT * FROM `machine_sales` WHERE `ID` = $id";
			$records = mysql_query($query);
			return mysql_fetch_array($records);
		}
		public function save(){
			$query = "INSERT INTO `machine_sales`(`ENTRY_DATE`,
																					  `MACHINE_ID`,
																					  `CLOTH_LENGTH`,
																					  `MEASURE`,
																					  `EMB_RATE`,
																					  `AMOUNT`,
																					  `ISSUED_LENGTH`,
																					  `ISSUED_AMOUNT`)
																			VALUES ('$this->entry_date',
																							'$this->machine_id',
																							'$this->clothLength',
																							'$this->measure',
																							'$this->unitRate',
																							'$this->amount',
																							'$this->issued_length',
																							'$this->issued_amount')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `machine_sales` SET `ENTRY_DATE`= '$this->entry_date',
													`MACHINE_ID`='$this->machine_id',
													`CLOTH_LENGTH`=$this->clothLength,
													`MEASURE`='$this->measure',
													`EMB_RATE`=$this->unitRate,
													`AMOUNT`=$this->amount,
													`ISSUED_LENGTH`=$this->issued_length,
													`ISSUED_AMOUNT`=$this->issued_amount WHERE `ID` = $id";
			return mysql_query($query);
		}
		public function MachineSaleVoucherExists(){
			$today = date('Y-m-d');
			$query = "SELECT * FROM voucher INNER JOIN voucher_details
					  ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID WHERE `VOUCHER_DATE = '$today'`";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function getMachineList(){
			$query = "SELECT * FROM `machines` WHERE `ACTIVE` = 'Y'";
			return mysql_query($query);
		}
		public function insertVoucherId($machine_sale_id,$voucher_id){
			$query = "UPDATE machine_sales SET VOUCHER_ID = $voucher_id WHERE ID = $machine_sale_id";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function getVoucherId($machine_sale_id){
			$query = "SELECT VOUCHER_ID FROM machine_sales WHERE ID = $machine_sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function search($fromDate,$toDate,$machine_id){
			$query = "SELECT * FROM machine_sales WHERE ";
			$andFlag = false;

			if($fromDate != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " ENTRY_DATE >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " ENTRY_DATE <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			if($machine_id != 0){
				$query .= ($andFlag)?" AND ":"";
				$query .= " MACHINE_ID = ".$machine_id;
				$andFlag = true;
			}
			$records = mysql_query($query);
			return $records;
		}
		public function delete($id){
			$query ="DELETE FROM machine_sales WHERE ID = $id";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function getTotals(){
			$query = "SELECT SUM(CLOTH_LENGTH) AS TOTAL_LENGTH , SUM(AMOUNT) AS TOTAL_AMOUNT FROM machine_sales";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
	}
?>
