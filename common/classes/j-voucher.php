<?php
class JournalVoucher{
	public $voucherId;
	public $jVoucherNum;
	public $voucherDate;
	public $reference;
	public $status;
	public $voucherType;
	public $type_num;
	public $po_number;
	public $order_taker_id;
	public $salesman_id;
	public $accountCode;
	public $accountTitle;
	public $narration;
	public $transactionType;
	public $amount;
	public $balance;
	public $cash_book_flag;
	public $reference_date;

	public $ref_type;
	public $ref_id;

	public function getAccountsList(){
		$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) = 9 ORDER BY ACC_TITLE,ACC_CODE";
		$records = mysql_query($query);
		return $records;
	}
	public function voucherStatus($status,$voucher_id){
		$query = "UPDATE `voucher` SET `VOUCHER_STATUS` = '$status' WHERE `VOUCHER_ID` = '$voucher_id' ";
		mysql_query($query);
	}
	public function getVoucherDate($voucher_id){
		$query = "SELECT `VOUCHER_DATE` FROM `voucher` WHERE `VOUCHER_ID` = '$voucher_id' ";
		$result = mysql_query($query);
		if(mysql_num_rows($result)){
			$balance = mysql_fetch_assoc($result);
			return $balance['VOUCHER_DATE'];
		}else{
			return '00-00-0000';
		}
	}
	public function getBankAccountFromVoucher($voucher_id){
		$query = "SELECT `ACCOUNT_TITLE` FROM `voucher_details` WHERE `VOUCHER_ID` = '$voucher_id' AND SUBSTR(ACCOUNT_CODE,1,6) = '010102' LIMIT 1 ";
		$result = mysql_query($query);
		if(mysql_num_rows($result)){
			$balance = mysql_fetch_assoc($result);
			return $balance['ACCOUNT_TITLE'];
		}else{
			return '';
		}
	}
	public function getAccountsTitle($accCode){
		$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE ACC_CODE = '$accCode'";
		$records = mysql_fetch_assoc(mysql_query($query));
		return $records['ACC_TITLE'];
	}
	public function getRecordDetails($voucherId){
		$query = "SELECT * FROM `voucher` WHERE `VOUCHER_ID` = $voucherId ";
		$records = mysql_query($query);
		return $records;
	}
	public function getRecordDetailsVoucher($voucherId){
		$query = "SELECT * FROM `voucher` WHERE `VOUCHER_ID` = $voucherId ";
		$records = mysql_query($query);
		if(mysql_num_rows($records)){
			$row = mysql_fetch_assoc($records);
			return $row;
		}else{
			return NULL;
		}
	}
	public function checkVoucherByType($typeNum,$type){
		$query = "SELECT * FROM voucher WHERE TYPE_NO = '$typeNum' AND VOUCHER_TYPE = '$type'";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			return false;
		}else{
			return true;
		}
	}

	public function getTransactionAmount($voucher_id){
		$query = "SELECT AMOUNT FROM voucher_details WHERE VOUCHER_ID = $voucher_id";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			$row = mysql_fetch_assoc($record);
			return $row['AMOUNT'];
		}else{
			return NULL;
		}
	}
	public function getSalesmanReceiptAmount($voucher_date,$order_taker_id,$salesman_id){
		$query = "SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM voucher_details WHERE TRANSACTION_TYPE = 'Cr' AND VOUCHER_DATE = '$voucher_date' AND VOUCHER_ID IN (SELECT VOUCHER_ID FROM voucher WHERE ORDER_TAKER_ID = '$order_taker_id' AND SALESMAN_ID = '$salesman_id') ";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			$row = mysql_fetch_assoc($record);
			return (float)$row['TOTAL_AMOUNT'];
		}else{
			return 0;
		}
	}
	public function checkVoucherNumber($voucher_num){
		$query = "SELECT VOUCHER_ID FROM voucher WHERE VOUCHER_NO = $voucher_num";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			$row = mysql_fetch_assoc($record);
			return $row['VOUCHER_ID'];
		}else{
			return 0;
		}
	}



	public function getVoucherDetailDetail($jv_detail_id){
		$query = "SELECT * FROM voucher_details WHERE VOUCH_DETAIL_ID = $jv_detail_id";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			$row = mysql_fetch_assoc($record);
			return $row;
		}else{
			return NULL;
		}
	}

	public function getVoucherDetailColumn($jv_detail_id,$column_name){
		$query = "SELECT $column_name FROM voucher_details WHERE VOUCH_DETAIL_ID = '$jv_detail_id' ";
		$record = mysql_query($query);
		if(mysql_num_rows($record)){
			$row = mysql_fetch_assoc($record);
			return $row[$column_name];
		}else{
			return NULL;
		}
	}
	public function getVoucherDetailColumnList($voucher_id,$column_name,$values_like){
		$values_like = ($values_like!=NULL)?" AND $column_name LIKE '$values_like%' ":"";
		$query = "SELECT * FROM voucher_details WHERE  VOUCHER_ID = '$voucher_id' $values_like  ";
		$record = mysql_query($query);
	}

	public function getListExcluding($voucherId){
		$query = "SELECT * FROM `ledger` WHERE `LEDGER_NO` !=$voucherId AND CR_NO = '0' AND CP_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
		$records = mysql_query($query);
		return $records;
	}
	public function genJvNumber(){
		$query = mysql_query("SELECT MAX(VOUCHER_NO) as LAST_VOUCHER_NO FROM `voucher`");
		if(mysql_num_rows($query)){
			$jv = mysql_fetch_assoc($query);
			$jv = $jv['LAST_VOUCHER_NO'];
			$jv++;
			$jvNum = $jv;
		}else{
			$jvNum = 1;
		}
		return $jvNum;
	}

	public function genTypeNumber($type){
		$query = mysql_query(" SELECT MAX(TYPE_NO) as LAST_TYPE_NO FROM `voucher` WHERE VOUCHER_TYPE = '$type' ");
		if(mysql_num_rows($query)){
			$jv = mysql_fetch_assoc($query);
			$jv = $jv['LAST_TYPE_NO'];
			$jv++;
			$jvNum = $jv;
		}else{
			$jvNum = 1;
		}
		return $jvNum;
	}

	public function saveVoucher(){
		$query = "INSERT INTO `voucher`(`VOUCHER_NO`,
			`VOUCHER_TYPE`,
			`TYPE_NO`,
			`VOUCHER_DATE`,
			`REFERENCE`,
			`REF_DATE`,
			`REF_TYPE`,
			`REF_ID`,
			`PO_NO`,
			`ORDER_TAKER_ID`,
			`SALESMAN_ID`,
			`VOUCHER_STATUS`)
			VALUES ($this->jVoucherNum,
				'$this->voucherType',
				'$this->type_num',
				'$this->voucherDate',
				'$this->reference',
				'$this->reference_date',
				'$this->ref_type',
				'$this->ref_id',
				'$this->po_number',
				'$this->order_taker_id',
				'$this->salesman_id',
				'P')";
				$saved = mysql_query($query);
				return mysql_insert_id();
			}
			public function getVoucherListByType($jv_type){
				$query = "SELECT * FROM `voucher` WHERE `VOUCHER_TYPE`= '$jv_type' ";
				$records = mysql_query($query);
				return $records;
			}
			public function getVoucherListByTypeLimit($jv_type,$limit){
				$query = "SELECT * FROM `voucher` WHERE `VOUCHER_TYPE`= '$jv_type' ORDER BY VOUCHER_DATE DESC LIMIT  ".((int)($limit));
				$records = mysql_query($query);
				return $records;
			}
			public function getVoucherDetailList($jvId){
				$query = "SELECT * FROM `voucher_details` WHERE `VOUCHER_ID`= $jvId";
				$records = mysql_query($query);
				return $records;
			}

			public function getVoucherCustomerSuppliersList($voucher_id){
				$query = "SELECT * FROM `voucher_details` WHERE `VOUCHER_ID`= '$voucher_id' AND ( SUBSTR(ACCOUNT_CODE,1,6) = '010104' OR SUBSTR(ACCOUNT_CODE,1,6) = '040101' )";
				$records = mysql_query($query);
				return $records;
			}

			public function getVoucherDetailListId($jvId){
				$query = "SELECT VOUCH_DETAIL_ID FROM `voucher_details` WHERE `VOUCHER_ID`= $jvId";
				$records = mysql_query($query);
				return $records;
			}

			public function updateVoucher($jvId){
				$query = "UPDATE `voucher` SET `REFERENCE`='$this->reference',
				`REF_DATE`='$this->reference_date',
				`SYNCED`='N' WHERE `VOUCHER_ID` = $jvId";
				$updated = mysql_query($query);
				return $updated;
			}
			public function updateVoucherDate($jvId){
				$query = "UPDATE `voucher` SET `VOUCHER_DATE` = '$this->voucherDate',`SYNCED`='N' WHERE `VOUCHER_ID` = $jvId";
				$updated = mysql_query($query);
				return $updated;
			}
			public function updateVoucherPoNumber($jvId){
				$query = "UPDATE `voucher` SET `PO_NO` = '$this->po_number' , `SYNCED`= 'N' WHERE `VOUCHER_ID` = $jvId";
				$updated = mysql_query($query);
				return $updated;
			}
			public function updateVoucherOrderTakerAndSalesman($jvId){
				$query = "UPDATE `voucher` SET `ORDER_TAKER_ID` = '$this->order_taker_id' , `SALESMAN_ID` = '$this->salesman_id' , `SYNCED`= 'N' WHERE `VOUCHER_ID` = $jvId";
				$updated = mysql_query($query);
				return $updated;
			}
			public function update_narration($vouch_detail_id,$narration){
				$query = "UPDATE `voucher_details` SET `NARRATION` = '$narration' WHERE `VOUCH_DETAIL_ID` = $vouch_detail_id";
				$updated = mysql_query($query);
				return $updated;
			}
			public function updateVoucherType($jvId){
				$query = "UPDATE `voucher` SET `VOUCHER_TYPE` = '$this->voucherType' WHERE `VOUCHER_ID` = $jvId";
				$updated = mysql_query($query);
				return $updated;
			}
			public function getVoucherCounter(){
				$query = "SELECT MAX(COUNTER) AS YOUR_COUNTER FROM voucher_details WHERE ACCOUNT_CODE = '$this->accountCode' AND VOUCHER_DATE = DATE('$this->voucherDate')";
				$record = mysql_query($query);
				if(mysql_num_rows($record)){
					$row = mysql_fetch_assoc($record);
					$yourCounter = ($row['YOUR_COUNTER'] == '')?0:$row['YOUR_COUNTER'];
					return $yourCounter;
				}else{
					return 0;
				}
			}

			public function getPrevBalance($accCode,$voucher_date){
				$query  =  "SELECT BALANCE FROM voucher_details WHERE ACCOUNT_CODE = '$accCode'";
				$query .= "AND VOUCHER_DATE <= DATE('".$voucher_date."') ORDER BY VOUCHER_DATE DESC,COUNTER DESC LIMIT 1 ";
				$result = mysql_query($query);

				if(mysql_num_rows($result)){
					$row = mysql_fetch_assoc($result);
					return $row['BALANCE'];
				}else{
					return 0;
				}
			}

			public function saveVoucherDetail(){
				//get Last Balance
				if($this->amount == 0){
					return false;
				}
				$this->amount = round($this->amount);
				$this->balance = $this->getPrevBalance($this->accountCode,$this->voucherDate);
				//Get New Counter For This Entry
				$newCounter = $this->getVoucherCounter();
				$nextCounter = ++$newCounter;
				$query = "INSERT INTO `voucher_details`(`VOUCHER_ID`,
					`COUNTER`,
					`VOUCHER_DATE`,
					`ACCOUNT_CODE`,
					`ACCOUNT_TITLE`,
					`NARRATION`,
					`TRANSACTION_TYPE`,
					`AMOUNT`,
					`BALANCE`,
					`CASH_BOOK_FLAG`)
					VALUES ('$this->voucherId',
						'$nextCounter',
						'$this->voucherDate',
						'$this->accountCode',
						'$this->accountTitle',
						'$this->narration',
						'$this->transactionType',
						'$this->amount',
						'0',
						'$this->cash_book_flag')";
						// echo $query."-------";
						$inserted = mysql_query($query);
						$voucher_detail_id = mysql_insert_id();
						//set Succeeding Balance Process
						$accCode = $this->accountCode;
						$transactionType = $this->transactionType;
						$VoucherDate = $this->voucherDate;
						$newAmount = $this->amount;
						$balance = $this->balance;
						if($inserted){
							$accountType = substr($accCode,0,2);
							if( $transactionType=='Dr' && ($accountType=='01' || $accountType=='03')){
								$newBalance = $newAmount + $balance;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('+',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
							}elseif( $transactionType=='Cr' && ($accountType=='01' || $accountType=='03') ){
								//add in case : amount And Balance are Credit.
								$newBalance =  $balance - $newAmount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('-',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
							}elseif( $transactionType=='Cr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
								//Less in case : amount is Credit And Balance is Debit.
								$newBalance = $balance + $newAmount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('+',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
							}elseif( $transactionType=='Dr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
								//Less in case : amount is Credit And Balance is Debit.
								$newBalance = $balance - $newAmount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('-',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
							}
						}
						return $voucher_detail_id;
					}

					public function setSucceedingBalance($method,$amount,$accCode,$voucher_detail_id,$voucher_date){

						$query = "SELECT COUNTER FROM voucher_details WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						$voucher = mysql_fetch_assoc(mysql_query($query));
						$thisCounter = $voucher['COUNTER'];
						//update balance if in currentDate

						$query = "UPDATE voucher_details SET BALANCE = BALANCE ".$method.$amount."
						WHERE ACCOUNT_CODE = '".$accCode."' AND VOUCHER_DATE = DATE('".$voucher_date."') AND COUNTER > $thisCounter";
						mysql_query($query);

						$query = "UPDATE voucher_details SET BALANCE = BALANCE ".$method.$amount."
						WHERE ACCOUNT_CODE = '".$accCode."' AND VOUCHER_DATE > DATE('".$voucher_date."')";
						mysql_query($query);
					}

					public function insertEntryBalance($voucher_detail_id,$newBalance){
						$query = "UPDATE voucher_details SET BALANCE = $newBalance WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						mysql_query($query);
					}
					public function updateCustom($voucher_detail_id,$amount){
						$query = "UPDATE voucher_details SET AMOUNT = $amount WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						mysql_query($query);
						return mysql_affected_rows();
					}

					public function updateCustomByAccCode($accCode,$voucher_id,$amount){
						$query = "UPDATE voucher_details SET AMOUNT = $amount WHERE VOUCHER_ID = $voucher_id AND ACCOUNT_CODE = '$accCode' ";
						mysql_query($query);
						return mysql_affected_rows();
					}

					public function setBalance($voucher_detail_id,$balance){
						$query = "UPDATE voucher_details SET BALANCE = $balance WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						mysql_query($query);
						return mysql_affected_rows();
					}

					public function deleteDetails($jvid){
						$query = "DELETE FROM `voucher_details` WHERE `VOUCH_DETAIL_ID` = $jvid ";
						$deleted = mysql_query($query);
						return $deleted;
					}

					public function checkInherited(){
						$query = "";
					}
					public function deleteVoucher($id){
						$query = "DELETE FROM `voucher` WHERE `VOUCHER_ID` = $id";
						$voucher_date = $this->getVoucherDate($id);
						$voucher_detail_list =  $this->getVoucherDetailList($id);
						$voucher_status = $this->getVoucherStatus($id);
						if(mysql_num_rows($voucher_detail_list)){
							if($voucher_status == 'P'){
								while($vouchRow = mysql_fetch_assoc($voucher_detail_list)){
									$voucher_detail_id = $vouchRow['VOUCH_DETAIL_ID'];
									$accCode = $vouchRow['ACCOUNT_CODE'];
									$prevAmount = $vouchRow['AMOUNT'];
									$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$voucher_date);
									$this->deleteDetails($voucher_detail_id);
								}
							}elseif($voucher_status == 'F'){
								while($vouchRow = mysql_fetch_assoc($voucher_detail_list)){
									$voucher_detail_id = $vouchRow['VOUCH_DETAIL_ID'];
									$this->deleteDetails($voucher_detail_id);
								}
							}
						}
						return mysql_query($query);
					}
					public function getSaleInvoiceCustomerAmount($voucher_id){
						$query = "SELECT * FROM `voucher_details` WHERE VOUCHER_ID = '$voucher_id' AND SUBSTR(ACCOUNT_CODE,1,6) = '010104' ";
						$result= mysql_query($query);
						if(mysql_num_rows($result)){
							$row = mysql_fetch_assoc($result);
							return $row;
						}else{
							return 0;
						}
					}
					public function updateTransaction($voucher_detail_id){
						$vouchRow          = $this->getVoucherDetailDetail($voucher_detail_id);
						$accCode 					 = $vouchRow['ACCOUNT_CODE'];
						$VoucherDate 			 = $vouchRow['VOUCHER_DATE'];
						$transactionType   = $vouchRow['TRANSACTION_TYPE'];
						$prevAmount        = $vouchRow['AMOUNT'];
						$accountType       = substr($accCode,0,2);

						if( $transactionType == 'Dr' && ($accountType=='01' || $accountType == '03')){
							$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && ($accountType=='01' || $accountType=='03') ){
							$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Dr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}

						$this->balance = $this->getPrevBalance($this->accountCode,$this->voucherDate);
						$newCounter = $this->getVoucherCounter();
						$nextCounter = ++$newCounter;

						$query = "UPDATE `voucher_details` SET `COUNTER`   			  = '$nextCounter',
																									 `ACCOUNT_CODE`			= '$this->accountCode',
																									 `ACCOUNT_TITLE`		= '$this->accountTitle',
																									 `NARRATION`				= '$this->narration',
																									 `TRANSACTION_TYPE`	= '$this->transactionType',
																									 `AMOUNT`						= '$this->amount',
																									 `BALANCE`					= '$this->balance' WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						$inserted = mysql_query($query);

						//set Succeeding Balance Process
						if($inserted){
							$accountType = substr($this->accountCode,0,2);
							if( $this->transactionType=='Dr' && ($accountType=='01' || $accountType=='03')){
								$newBalance = $this->amount + $this->balance;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('+',$this->amount,$this->accountCode,$voucher_detail_id,$this->voucherDate);
							}elseif( $this->transactionType=='Cr' && ($accountType=='01' || $accountType=='03') ){
								//add in case : amount And Balance are Credit.
								$newBalance =  $this->balance - $this->amount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('-',$this->amount,$this->accountCode,$voucher_detail_id,$this->voucherDate);
							}elseif( $this->transactionType=='Cr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
								//Less in case : amount is Credit And Balance is Debit.
								$newBalance = $this->balance + $this->amount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('+',$this->amount,$this->accountCode,$voucher_detail_id,$this->voucherDate);
							}elseif( $this->transactionType=='Dr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
								//Less in case : amount is Credit And Balance is Debit.
								$newBalance = $this->balance - $this->amount;
								$this->insertEntryBalance($voucher_detail_id,$newBalance);
								$this->setSucceedingBalance('-',$this->amount,$this->accountCode,$voucher_detail_id,$this->voucherDate);
							}
							return 1;
						}else{
							return 0;
						}
					}
					public function deleteTransaction($voucher_detail_id){
						$vouchRow          = $this->getVoucherDetailDetail($voucher_detail_id);
						$voucher_detail_id = $vouchRow['VOUCH_DETAIL_ID'];
						$accCode 					 = $vouchRow['ACCOUNT_CODE'];
						$VoucherDate 			 = $vouchRow['VOUCHER_DATE'];
						$transactionType   = $vouchRow['TRANSACTION_TYPE'];
						$prevAmount        = $vouchRow['AMOUNT'];
						$accountType       = substr($accCode,0,2);

						if( $transactionType == 'Dr' && ($accountType=='01' || $accountType == '03')){
							$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && ($accountType=='01' || $accountType=='03') ){
							$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Dr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}
						$this->deleteDetails($voucher_detail_id);
					}
					public function reverseVoucherDetails($id){
						$voucher_detail_list =  $this->getVoucherDetailList($id);
						$voucher_status = $this->getVoucherStatus($id);
						if(mysql_num_rows($voucher_detail_list)){
							if($voucher_status == 'P'){
								while($vouchRow = mysql_fetch_assoc($voucher_detail_list)){
									$voucher_detail_id = $vouchRow['VOUCH_DETAIL_ID'];
									$accCode = $vouchRow['ACCOUNT_CODE'];
									$VoucherDate = $vouchRow['VOUCHER_DATE'];
									$transactionType = $vouchRow['TRANSACTION_TYPE'];
									$prevAmount = $vouchRow['AMOUNT'];
									$accountType = substr($accCode,0,2);
									if( $transactionType == 'Dr' && ($accountType=='01' || $accountType == '03')){
										$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
									}elseif( $transactionType=='Cr' && ($accountType=='01' || $accountType=='03') ){
										$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
									}elseif( $transactionType=='Cr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
										$this->setSucceedingBalance('-',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
									}elseif( $transactionType=='Dr' && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
										$this->setSucceedingBalance('+',$prevAmount,$accCode,$voucher_detail_id,$VoucherDate);
									}
									$this->deleteDetails($voucher_detail_id);
								}
							}
						}
						return true;
					}

					public function deleteJv($voucher_id){
						$query = "DELETE FROM `voucher` WHERE `VOUCHER_ID` = $voucher_id";
						mysql_query($query);
						return mysql_affected_rows();
					}

					public function deleteCompleteVoucher($voucher_id){
						$query = "DELETE FROM `voucher` WHERE `VOUCHER_ID` = $voucher_id";
						mysql_query($query);
						$query = "DELETE FROM `voucher_details` WHERE `VOUCHER_ID` = $voucher_id";
						$deleted = mysql_query($query);
						return mysql_affected_rows();
					}

					public function getVoucherStatus($voucher_id){
						$query = "SELECT VOUCHER_STATUS FROM voucher WHERE VOUCHER_ID = $voucher_id";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$row = mysql_fetch_assoc($record);
							return $row['VOUCHER_STATUS'];
						}else{
							return NULL;
						}
					}

					/* save Current Balance */
					private function getLastBalance($accCode,$newAmount,$transactionType){
						$query = "SELECT `BALANCE` FROM `voucher_details` WHERE `ACCOUNT_CODE` = '$accCode' ORDER BY `VOUCH_DETAIL_ID` DESC LIMIT 1";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$balance = mysql_fetch_assoc($result);
							$balance = $balance['BALANCE'];
						}else{
							$balance = 0;
						}
						$balanceType = ($balance<0)?"Cr":"Dr";
						if( $transactionType=='Dr' && $balanceType=='Dr' ){
							//add in case : amount And Balance are Debit.
							$newBalance = $newAmount + $balance;
						}elseif( $transactionType=='Cr' && $balanceType=='Cr' ){
							//add in case : amount And Balance are Credit.
							$newBalance =  $balance + $newAmount;
						}elseif( $transactionType=='Cr' && $balanceType=='Dr' ){
							//Less in case : amount is Credit And Balance is Debit.
							$newBalance = $balance - $newAmount;
						}elseif( $transactionType=='Dr' && $balanceType=='Cr' ){
							//Less in case : amount is Credit And Balance is Debit.
							$newBalance = $balance - $newAmount;
						}
						return $newBalance;
					}
					public function getBalance($voucher_detail_id){
						$query = "SELECT BALANCE FROM voucher_details WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$row = mysql_fetch_assoc($record);
							return $row['BALANCE'];
						}else{
							return 0;
						}

					}

					public function deleteUnRelatedVoucherEntries(){
						$query = "DELETE FROM voucher WHERE VOUCHER_ID NOT IN (SELECT VOUCHER_ID FROM voucher_details)";
						mysql_query($query);
					}

					public function setSucceedingBalanceOutward($method,$amount,$accCode,$voucher_detail_id,$voucher_date){
						$query = "UPDATE voucher_details INNER JOIN voucher
						ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
						SET voucher_details.BALANCE = voucher_details.BALANCE".$method.$amount."
						WHERE voucher_details.VOUCH_DETAIL_ID > $voucher_detail_id
						AND voucher_details.ACCOUNT_CODE = '$accCode'
						AND voucher.VOUCHER_STATUS = 'P' ";
						return mysql_query($query);
					}

					public function getLastBalanceNew($accCode,$newAmount,$transactionType,$VoucherDate,$voucher_detail_id){
						$query = "SELECT VOUCHER_ID,COUNTER FROM voucher_details WHERE VOUCH_DETAIL_ID = $voucher_detail_id";
						$voucher = mysql_fetch_assoc(mysql_query($query));
						$voucher_id = $voucher['VOUCHER_ID'];
						$voucher_counter = $voucher['COUNTER'];

						$query = "SELECT BALANCE FROM voucher_details
						WHERE VOUCHER_ID <= $voucher_id AND ACCOUNT_CODE = '$accCode'
						ORDER BY VOUCHER_ID DESC LIMIT 1 ";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$balance = mysql_fetch_assoc($result);
							$balance = $balance['BALANCE'];
						}else{
							$balance = 0;
						}
						$accountType = substr($accCode,0,2);
						if( $transactionType=='Dr' && $accountType=='01' || $accountType=='03'){
							$newBalance = $newAmount + $balance;
							$this->setSucceedingBalance('+',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && $accountType=='01' || $accountType=='03' ){
							//add in case : amount And Balance are Credit.
							$newBalance =  $balance - $newAmount;
							$this->setSucceedingBalance('-',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Cr' && $accountType=='02' || $accountType=='04' || $accountType=='05' ){
							//Less in case : amount is Credit And Balance is Debit.
							$newBalance = $balance + $newAmount;
							$this->setSucceedingBalance('+',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}elseif( $transactionType=='Dr' && $accountType=='02' || $accountType=='04' || $accountType=='05' ){
							//Less in case : amount is Credit And Balance is Debit.
							$newBalance = $balance - $newAmount;
							$this->setSucceedingBalance('-',$newAmount,$accCode,$voucher_detail_id,$VoucherDate);
						}
						return $newBalance;
					}

					public function saveSaleVoucher($entryDate,$costaCode,$costaTitle,$taxAmount,$taxAccCode,$taxAccTitle,$inventoryAccCode,$inventoryAccTitle,$supplierAccCode,$supplierAccTitle,$billNumber,$amount,$costaSales,$saleAccCode,$saleAccTitle,$discountAccCode,$discountAccTitle,$sale_discount,$charges){
						//function body start
						$this->jVoucherNum    = $this->genJvNumber();
						$this->voucherDate    = date('Y-m-d',strtotime($entryDate));
						$this->voucherType    = 'SV';
						$this->reference   	  = 'Sales Voucher';
						$this->reference_date = date('Y-m-d',strtotime($entryDate));
						$this->status	  = 'P';

						$voucher_id = $this->saveVoucher();
						if($voucher_id){
							$this->voucherId    = $voucher_id;

							//!Customer Debit
							$this->accountCode  = $supplierAccCode;
							$this->accountTitle = $supplierAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $amount + $charges;

							$this->saveVoucherDetail();

							$this->accountCode  = $discountAccCode;
							$this->accountTitle = $discountAccTitle;
							$this->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $sale_discount;
							if($sale_discount > 0){
								$this->saveVoucherDetail();
							}

							//Sale Credit
							$this->accountCode  = $saleAccCode;
							$this->accountTitle = $saleAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $amount + $sale_discount - $taxAmount;

							$this->saveVoucherDetail();
							//Sale Tax Credit

							$this->accountCode  = $taxAccCode;
							$this->accountTitle = $taxAccTitle;
							$this->narration    = "Sales Tax Payable against bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $taxAmount;

							if($taxAmount > 0){
								$this->saveVoucherDetail();
							}

							//Charges Credit
							$this->accountCode     = '0201020001';
							$this->accountTitle    = 'Income A/c Other Revenue';
							$this->narration       = "Charged to customer against bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount          = $charges;

							if($charges > 0){
								$this->saveVoucherDetail();
							}

							//CostaSales
							$this->accountCode  = $costaCode;
							$this->accountTitle = $costaTitle;
							$this->narration    = "Cost of Inventory Sold expensed against bill#".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $costaSales;

							if($costaSales > 0){
								$this->saveVoucherDetail();
							}

							//CostaInventory
							$this->accountCode  = $inventoryAccCode;
							$this->accountTitle = $inventoryAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cost of Goods Sold vide invoice no. ".$billNumber:"Cost of Goods Sold to ".$supplierAccTitle." Against Bill# ".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $costaSales;

							$this->saveVoucherDetail();
						}
						return $voucher_id;
					}
					public function updateSaleVoucher($voucher_id,$entryDate,$costaCode,$costaTitle,$taxAmount,$taxAccCode,$taxAccTitle,$inventoryAccCode,$inventoryAccTitle,$supplierAccCode,$supplierAccTitle,$billNumber,$amount,$costaSales,$saleAccCode,$saleAccTitle,$discountAccCode,$discountAccTitle,$sale_discount,$charges){
						if($voucher_id){

							$this->voucherId    = $voucher_id;
							$this->voucherDate  = $entryDate;
							$this->updateVoucherDate($voucher_id);

							//!Customer Debit
							$this->accountCode  = $supplierAccCode;
							$this->accountTitle = $supplierAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $amount + $charges;

							$this->saveVoucherDetail();

							$this->accountCode  = $discountAccCode;
							$this->accountTitle = $discountAccTitle;
							$this->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $sale_discount;
							if($sale_discount > 0){
								$this->saveVoucherDetail();
							}

							//Sale Credit
							$this->accountCode  = $saleAccCode;
							$this->accountTitle = $saleAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $amount + $sale_discount - $taxAmount;

							$this->saveVoucherDetail();
							//Sale Tax Credit

							$this->accountCode  = $taxAccCode;
							$this->accountTitle = $taxAccTitle;
							$this->narration    = "Sales Tax Payable against bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $taxAmount;

							if($taxAmount > 0){
								$this->saveVoucherDetail();
							}

							//Charges Credit

							$this->accountCode     = '0201020001';
							$this->accountTitle    = 'Income A/c Other Revenue';
							$this->narration       = "Charged to customer against bill#".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount          = $charges;

							if($charges > 0){
								$this->saveVoucherDetail();
							}

							//CostaSales
							$this->accountCode  = $costaCode;
							$this->accountTitle = $costaTitle;
							$this->narration    = "Cost of Inventory Sold expensed against bill#".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $costaSales;

							if($costaSales > 0){
								$this->saveVoucherDetail();
							}

							//CostaInventory
							$this->accountCode  = $inventoryAccCode;
							$this->accountTitle = $inventoryAccTitle;
							$this->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cost of Goods Sold vide invoice no. ".$billNumber:"Cost of Goods Sold to ".$supplierAccTitle." Against Bill# ".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $costaSales;

							$this->saveVoucherDetail();
						}
						return $voucher_id;
					}

					public function updateSaleReturnVoucher($voucher_id,$entryDate,$costaCode,$costaTitle,$taxAmount,$taxAccCode,$taxAccTitle,$inventoryAccCode,$inventoryAccTitle,$supplierAccCode,$supplierAccTitle,$billNumber,$amount,$costaSales,$saleAccCode,$saleAccTitle){
						if($voucher_id){
							$this->voucherId    = $voucher_id;

							//Sale Return
							$this->accountCode  = $saleAccCode;
							$this->accountTitle = $saleAccTitle;
							$this->narration    = "Value of Goods Returned By ".$supplierAccTitle." Against Bill# ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $amount - $taxAmount;

							$this->saveVoucherDetail();

							//Sale Tax Credit
							$this->accountCode  = $taxAccCode;
							$this->accountTitle = $taxAccTitle;
							$this->narration    = "Sales Tax Reversed Against Bill# ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $taxAmount;

							if($taxAmount > 0){
								$this->saveVoucherDetail();
							}

							//!Customer Debit
							$this->accountCode  = $supplierAccCode;
							$this->accountTitle = $supplierAccTitle;
							$this->narration    = " Sale Return Against Bill # ".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $amount;

							$this->saveVoucherDetail();

							//CostaInventory
							$this->accountCode  = $inventoryAccCode;
							$this->accountTitle = $inventoryAccTitle;
							$this->narration    = "Cost Of Goods Returned From ".$supplierAccTitle." Against Bill# ".$billNumber;
							$this->transactionType = 'Dr';
							$this->amount       = $costaSales;
							if($costaSales > 0){
								$this->saveVoucherDetail();
							}
							//CostaSales
							$this->accountCode  = $costaCode;
							$this->accountTitle = $costaTitle;
							$this->narration    = "Cost of sale Returned To Inventory against bill# ".$billNumber;
							$this->transactionType = 'Cr';
							$this->amount       = $costaSales;

							if($costaSales > 0){
								$this->saveVoucherDetail();
							}
						}
						return $voucher_id;
					}
					public function ReceiptVoucher($voucher_i_d,$expenditure_date,$expense_amount,$expenseAccCode,$expenseAccTitle,$cashCounterCode,$cashCounterTitle){
						$this->jVoucherNum = $this->genJvNumber();
						$this->voucherType = 'JV';
						$this->type_num    = 0;
						$this->voucherDate = $expenditure_date;
						$this->reference   = 'Fee Income';
						$this->reference_date = $expenditure_date;
						if($voucher_i_d == 0){
							//save Voucher
							$this->voucherId   = $this->saveVoucher();

							$this->accountCode = $cashCounterCode;
							$this->accountTitle= $cashCounterTitle;
							$this->narration   = "Amount Received In Cash On A/c Of ".$expenseAccTitle;
							$this->transactionType = 'Dr';
							$this->amount		= $expense_amount;
							//save Voucher Detail Debit
							$this->saveVoucherDetail();

							$this->accountCode = $expenseAccCode;
							$this->accountTitle= $expenseAccTitle;
							$this->transactionType = 'Cr';
							$this->amount		= $expense_amount;
							$this->narration   = "Amount Received In Cash";
							//save Voucher Detail Credit
							$this->saveVoucherDetail();


						}elseif($voucher_i_d > 0){
							$this->voucherId   = $voucher_i_d;

							//reverse Voucher Balances And Delete Previous voucher Details
							$this->reverseVoucherDetails($this->voucherId);

							$this->voucherDate = $expenditure_date;

							$this->updateVoucherDate($voucher_i_d);

							$this->accountCode = $cashCounterCode;
							$this->accountTitle= $cashCounterTitle;
							$this->transactionType = 'Dr';
							$this->narration   = "Amount Received In Cash On A/c Of ".$expenseAccTitle;
							$this->amount		= $expense_amount;
							//save Voucher Detail Credit
							$this->saveVoucherDetail();

							$this->accountCode = $expenseAccCode;
							$this->accountTitle= $expenseAccTitle;
							$this->transactionType = 'Cr';
							$this->amount		= $expense_amount;
							$this->narration   = "Amount Received In Cash";
							//save Voucher Detail Debit
							$this->saveVoucherDetail();
						}
						return $this->voucherId;
					}
					public function ExpenditureVoucher($voucher_i_d,$expenditure_date,$expense_amount,$expenseAccCode,$expenseAccTitle,$cashCounterCode,$cashCounterTitle){
						$this->jVoucherNum = $this->genJvNumber();
						$this->voucherType = 'JV';
						$this->type_num    = 0;
						$this->voucherDate = $expenditure_date;
						$this->reference   = 'Fee Income';
						$this->reference_date = $expenditure_date;
						if($voucher_i_d == 0){
							//save Voucher
							$this->voucherId   = $this->saveVoucher();

							$this->accountCode = $expenseAccCode;
							$this->accountTitle= $expenseAccTitle;
							$this->transactionType = 'Dr';
							$this->amount		= $expense_amount;
							$this->narration   = "Amount Paid In Cash";
							//save Voucher Detail Credit
							$this->saveVoucherDetail();

							$this->accountCode = $cashCounterCode;
							$this->accountTitle= $cashCounterTitle;
							$this->narration   = "Paid Amount on Account of ".$expenseAccTitle;
							$this->transactionType = 'Cr';
							//save Voucher Detail Debit
							$this->saveVoucherDetail();

						}elseif($voucher_i_d > 0){
							$this->voucherId   = $voucher_i_d;
							//reverse Voucher Balances And Delete Previous voucher Details
							$this->reverseVoucherDetails($this->voucherId);

							$this->accountCode = $expenseAccCode;
							$this->accountTitle= $expenseAccTitle;
							$this->transactionType = 'Dr';
							$this->amount		= $expense_amount;
							$this->narration   = "Amount Paid In Cash";
							//save Voucher Detail Debit
							$this->saveVoucherDetail();

							$this->accountCode = $cashCounterCode;
							$this->accountTitle= $cashCounterTitle;
							$this->transactionType = 'Cr';
							$this->narration   = "Paid Amount on Account of ".$expenseAccTitle;
							//save Voucher Detail Credit
							$this->saveVoucherDetail();
						}
						return $this->voucherId;
					}

					public function checkProfitAndLossEntry($entryDate,$accountCode){
						$query = "SELECT VOUCHER_ID FROM voucher_details WHERE ACCOUNT_CODE = '$accountCode' AND VOUCHER_DATE = DATE('".$entryDate."') ";
						$result = mysql_query($query);
						return mysql_num_rows($result);
					}

					public function getProfitnLoss($accCode,$fromDate,$toDate){
						$fromDate = date('Y-m-d',strtotime($fromDate." -1 day"));
						$toDate   = date('Y-m-d',strtotime($toDate));
						$query = "SELECT BALANCE
						FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$fromDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$openingBalance = mysql_fetch_assoc($record);
							$openingBalance = $openingBalance['BALANCE'];
						}else{
							$openingBalance	= 0;
						}
						$query = "SELECT BALANCE
						FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$toDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$closingBalance = mysql_fetch_assoc($record);
							$closingBalance = $closingBalance['BALANCE'];
						}else{
							$closingBalance	= 0;
						}
						return $closingBalance - $openingBalance;
					}

					public function getProfitnLossAlpha($accCode,$fromDate,$toDate){
						$fromDate = date('Y-m-d',strtotime($fromDate." -1 day"));
						$query = "SELECT BALANCE
						FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$fromDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$openingBalance = mysql_fetch_assoc($record);
							$openingBalance = $openingBalance['BALANCE'];
						}else{
							$openingBalance	= 0;
						}
						$query = "SELECT BALANCE
						FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$toDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$record = mysql_query($query);
						if(mysql_num_rows($record)){
							$closingBalance = mysql_fetch_assoc($record);
							$closingBalance = $closingBalance['BALANCE'];
						}else{
							$closingBalance	= 0;
						}
						return $closingBalance - $openingBalance;
					}

					public function getInvoiceBalance($customer_code,$voucher_id){
						$query = "SELECT `BALANCE` FROM `voucher_details` WHERE `VOUCHER_ID` = '$voucher_id' AND ACCOUNT_CODE = '$customer_code' ORDER BY VOUCH_DETAIL_ID DESC ";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$balance = mysql_fetch_assoc($result);
							return $balance['BALANCE'];
						}else{
							return '0';
						}
					}

					public function getBalanceType($accCode,$balance){
						$accountType = substr($accCode,0,2);
						$type = '';
						if( $balance > 0 && ($accountType=='01' || $accountType=='03')){
							$type = 'DR';
						}elseif( $balance < 0 && ($accountType=='01' || $accountType=='03') ){
							$type = 'CR';
						}elseif( $balance > 0 && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$type = 'CR';
						}elseif( $balance < 0 && ($accountType=='02' || $accountType=='04' || $accountType=='05') ){
							$type = 'DR';
						}

						$balance = str_ireplace('-', '', $balance);

						return array('BALANCE'=>$balance,'TYPE'=>$type);
					}
					public function insertReversalId($this_id,$reversal_id){
						$query = "UPDATE voucher SET REVERSAL_ID = '$reversal_id' WHERE VOUCHER_ID = '$this_id'";
						return mysql_query($query);
					}
					public function getAccountCodeEntriesAsPerDate($day){
						$query = "SELECT ACCOUNT_CODE,ACCOUNT_TITLE FROM voucher_details WHERE VOUCHER_DATE = DATE('".$day."') GROUP BY ACCOUNT_CODE";
						$result = mysql_query($query);
						return $result;
					}
					public function getTotalDebits($account_code,$day){
						$query  = "SELECT SUM(AMOUNT) AS DEBITS FROM voucher_details WHERE ACCOUNT_CODE = '$account_code' AND VOUCHER_ID IN (SELECT VOUCHER_ID FROM voucher WHERE VOUCHER_DATE = '".$day."' AND REVERSAL_ID = 0 ) AND TRANSACTION_TYPE = 'Dr' ";
						$result = mysql_query($query);
						$row 		= mysql_fetch_assoc($result);
						return $row['DEBITS'];
					}
					public function getTotalCredits($account_code,$day){
						$query  = "SELECT SUM(AMOUNT) AS CREDITS FROM voucher_details WHERE ACCOUNT_CODE = '$account_code' AND VOUCHER_ID IN (SELECT VOUCHER_ID FROM voucher WHERE VOUCHER_DATE = '".$day."' AND REVERSAL_ID = 0 ) AND TRANSACTION_TYPE = 'Cr' ";
						$result = mysql_query($query);
						$row 		= mysql_fetch_assoc($result);
						return $row['CREDITS'];
					}
					public function getDayEndBalance($endDate,$accCode){
						$query = "SELECT BALANCE
						FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$cashBalance = mysql_fetch_assoc($result);
							$cashBalance = $cashBalance['BALANCE'];
						}else{
							$cashBalance = 0;
						}
						return $cashBalance;
					}
					public function getOpeningBalanceOfVoucher($customer_code,$voucher_id,$voucher_date){
						$query = "SELECT `BALANCE` FROM `voucher_details` WHERE `VOUCHER_ID` < '$voucher_id' AND ACCOUNT_CODE = '$customer_code' AND VOUCHER_DATE <= '".$voucher_date."' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1 ";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$balance = mysql_fetch_assoc($result);
							return $balance['BALANCE'];
						}else{
							return '0';
						}
					}
					public function getLedgerOpeningBalance($endDate,$accCode){
						$query  = "SELECT BALANCE FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
						$result = mysql_query($query);
						if(mysql_num_rows($result)){
							$cashBalance = mysql_fetch_assoc($result);
							$cashBalance = $cashBalance['BALANCE'];
						}else{
							$cashBalance = 0;
						}
						return $cashBalance;
					}
					public function getLastSpecificTransaction($voucher_date,$party_acc_code,$transaction_type){
						$query = "SELECT voucher_details.* FROM voucher_details JOIN voucher ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID WHERE voucher_details.ACCOUNT_CODE = '$party_acc_code' AND voucher_details.VOUCHER_DATE <= '$voucher_date' AND voucher_details.TRANSACTION_TYPE = '$transaction_type' AND voucher.REVERSAL_ID = 0 AND voucher.VOUCHER_TYPE != 'RV' ORDER BY voucher_details.VOUCHER_DATE DESC,voucher_details.COUNTER DESC LIMIT 1 ";
						$row = mysql_fetch_assoc(mysql_query($query));
						return $row;
					}
					public function getLedgerByVouchersIN($voucher_id_string){
						$query = "SELECT voucher.*,voucher_details.* FROM voucher JOIN voucher_details ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID WHERE voucher.VOUCHER_ID IN ($voucher_id_string) ORDER BY voucher.VOUCHER_DATE ";
						return mysql_query($query);
					}
					public function get_accounts(){
						$query = "SELECT ACCOUNT_CODE FROM voucher_details GROUP BY ACCOUNT_CODE ";
						return mysql_query($query);
					}
					public function get_transactioins($acc_code){
						$query = "SELECT * FROM voucher_details WHERE ACCOUNT_CODE =  '$acc_code' ORDER BY VOUCHER_DATE , COUNTER ";
						return mysql_query($query);
					}
					public function getTransactionSumByRef($account_code,$transaction_type,$ref_id,$ref_type){
						$query = "SELECT SUM(AMOUNT) AS TMNT FROM voucher_details WHERE ACCOUNT_CODE = '$account_code' AND TRANSACTION_TYPE = '$transaction_type' AND VOUCHER_ID IN (SELECT VOUCHER_ID FROM voucher WHERE REF_ID = $ref_id AND REF_TYPE = '$ref_type' AND REVERSAL_ID = 0 )  ";
						$result = mysql_query($query);
						echo mysql_error();
						if(mysql_num_rows($result)){
							$row = mysql_fetch_assoc($result);
							return (float)$row['TMNT'];
						}
						return 0;
					}
				}
?>
