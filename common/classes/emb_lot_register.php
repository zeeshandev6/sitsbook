<?php
	class EmbLotRegister{
		public $lot_date;
		public $outwd_date;
		public $billNum;
		public $customerAccCode;
		public $customerAccTitle;
		public $lotStatus;
		public $fromDate;
		public $toDate;
		public $product_id;
		public $prodctTitle;
		public $lotNum;
		public $quality;
		public $than;
		public $measure;
		public $length;
		public $billing_type;
		public $stitches;
		public $total_laces;
		public $thanLength;
		public $embRate;
		public $embAmount;
		public $stitchRate;
		public $stitchAmount;
		public $stitchAccount;
		public $designNum;
		public $machineNum;
		public $gatePassNumber;
		public $thanOs;
		public $theType;
		public $partial_status;

		public $found_records;

		public function getDetail($lot_detail_id){
			$query = "SELECT * FROM emb_lot_register WHERE ID = '$lot_detail_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getLength($lot_detail_id){
			$query = "SELECT MEASURE_LENGTH FROM emb_lot_register WHERE ID = '$lot_detail_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['MEASURE_LENGTH'];
			}else{
				return 0;
			}
		}
		public function lotProceedable($lot_detail_id){
			$query = "SELECT ID FROM emb_lot_register WHERE ID = '$lot_detail_id' AND (EMB_AMOUNT > 0 OR STITCH_AMOUNT > 0) ";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}

		public function getGroup($lotList){
			$query = "SELECT * FROM emb_lot_register WHERE ID IN ($lotList)";
			$record = mysql_query($query);
			return $record;
		}

		public function getList($status){
			$query = "SELECT * FROM emb_lot_register WHERE LOT_STATUS = '$status'";
			$record = mysql_query($query);
			return $record;
		}

		public function countRows(){
			$query = "SELECT COUNT(ID) AS NUM_ROWS FROM emb_lot_register";
			$record = mysql_query($query);
			return $record;
		}

		public function getListAll($start,$total){
			$query = "SELECT * FROM emb_lot_register ORDER BY LOT_DATE ASC LIMIT $start,$total";
			$record = mysql_query($query);
			return $record;
		}
		public function getListByCustomer($status,$custAccCode){
			$query = "SELECT * FROM emb_lot_register WHERE LOT_STATUS = '$status' AND CUST_ACC_CODE = '$custAccCode'";
			$record = mysql_query($query);
			return $record;
		}
		public function getListByCustomerIncParials($custAccCode){
			$query = "SELECT * FROM emb_lot_register WHERE ( CUST_ACC_CODE = '$custAccCode' AND ID NOT IN  (SELECT LOT_REGISTER_ID FROM outward_details) AND LOT_STATUS = 'C') OR (PARTIAL_STATUS = 'Y' AND CUST_ACC_CODE = '$custAccCode') ";
			$record = mysql_query($query);
			return $record;
		}
		public function getUnderProcessLotLength($customerCode,$lotNumber){
			$query = "SELECT SUM(MEASURE_LENGTH) AS TOTAL_LENGTH FROM emb_lot_register WHERE CUST_ACC_CODE = '$customerCode' AND LOT_NO = $lotNumber AND LOT_STATUS = 'P' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_LENGTH'] == '')?0:$row['TOTAL_LENGTH'];
			}else{
				return 0;
			}
		}
		public function getCompletedLotLength($customerCode,$lotNumber){
			$query = "SELECT SUM(MEASURE_LENGTH) AS TOTAL_LENGTH FROM emb_lot_register WHERE CUST_ACC_CODE = '$customerCode' AND LOT_NO = $lotNumber AND LOT_STATUS = 'C' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_LENGTH'] == '')?0:$row['TOTAL_LENGTH'];
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `emb_lot_register`( `CUST_ACC_CODE`,
																								`LOT_DATE`,
																								`PRODUCT_ID`,
																								`LOT_NO`,
																								`QUALITY`,
																								`MEASURE_ID`,
																								`MEASURE_LENGTH`,
																								`BILLING_TYPE`,
																								`STITCHES`,
																								`TOTAL_LACES`,
																								`EMB_RATE`,
																								`EMB_AMOUNT`,
																								`STITCH_RATE`,
																								`STITCH_AMOUNT`,
																								`STITCH_ACC`,
																								`DESIGN_CODE`,
																								`MACHINE_ID`,
																								`GP_NO`,
																								`LOT_STATUS`,
																								`PARTIAL_STATUS`)
																						VALUES ('$this->customerAccCode',
																										'$this->lot_date',
																										'$this->product_id',
																										'$this->lotNum',
																										'$this->quality',
																										'$this->measure',
																										'$this->length',
																										'$this->billing_type',
																										'$this->stitches',
																										'$this->total_laces',
																										'$this->embRate',
																										'$this->embAmount',
																										'$this->stitchRate',
																										'$this->stitchAmount',
																										'$this->stitchAccount',
																										'$this->designNum',
																										'$this->machineNum',
																										'$this->gatePassNumber',
																										'$this->lotStatus',
																										'$this->partial_status')";
			$inserted = mysql_query($query);
			if($inserted){
				return mysql_insert_id();
			}else{
				return false;
			}
		}
		public function updateLotDetail($lot_detail_id){
			$query = "UPDATE `emb_lot_register` SET `CUST_ACC_CODE`	 	= '$this->customerAccCode',
																					    `PRODUCT_ID`	 		= '$this->product_id',
																					    `LOT_DATE`		 		= '$this->lot_date',
																					    `LOT_NO`		 			= '$this->lotNum',
																					    `QUALITY`		 			= '$this->quality',
																					    `MEASURE_ID`	 		= '$this->measure',
																					    `MEASURE_LENGTH` 	= '$this->length',
																							`BILLING_TYPE`		= '$this->billing_type',
																							`STITCHES`				= '$this->stitches',
																							`TOTAL_LACES`			= '$this->total_laces',
																					    `EMB_RATE`		 		= '$this->embRate',
																					    `EMB_AMOUNT`	 		= '$this->embAmount',
																					    `STITCH_RATE`	 		= '$this->stitchRate',
																					    `STITCH_AMOUNT`	 	= '$this->stitchAmount',
																					    `STITCH_ACC`	 		= '$this->stitchAccount',
																					    `DESIGN_CODE`	 		= '$this->designNum',
																					    `MACHINE_ID`	 		= '$this->machineNum',
																					    `GP_NO`			 			= '$this->gatePassNumber'  WHERE `ID` = '$lot_detail_id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getBothLotsByCust($customerCode){
			$query = "SELECT * FROM emb_lot_register WHERE CUST_ACC_CODE = '$customerCode' AND (LOT_STATUS = 'R' || LOT_STATUS = 'P') ";
			return mysql_query($query);
		}
		public function outwardBill(){
			$query = " SELECT emb_lot_register.*,outward.*  FROM emb_lot_register ";
			$query .= " LEFT JOIN outward ON outward.OUTWD_ID = emb_lot_register.OUTWD_ID  WHERE  ";
			$andFlag = false;
			if($this->fromDate!=''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " outward.OUTWD_DATE >= DATE('".$this->fromDate."') ";
				$andFlag = true;
			}
			if($this->toDate != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " outward.OUTWD_DATE <= DATE('".$this->toDate."') ";
				$andFlag = true;
			}
			if($this->lot_date != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_lot_register.LOT_DATE = DATE('".$this->lot_date."') ";
				$andFlag = true;
			}
			if($this->lotStatus!=''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_lot_register.LOT_STATUS = '$this->lotStatus' ";
				$andFlag = true;
			}
			if($this->designNum!=''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_lot_register.DESIGN_CODE = '$this->designNum' ";
				$andFlag = true;
			}
			if($this->machineNum != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_lot_register.MACHINE_ID = '$this->machineNum' ";
				$andFlag = true;
			}
			if($this->customerAccCode!=''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " outward.CUST_ACC_CODE = '$this->customerAccCode' ";
				$andFlag = true;
			}
			if($this->theType != ''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_lot_register.LOT_STATUS = '$this->theType' ";
				$andFlag = true;
			}
			$query .= ' ORDER BY outward.OUTWD_DATE ASC ';
			return mysql_query($query);
		}
		public function search($start,$total){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM emb_lot_register  ";
			$andFlag = false;
			if($this->fromDate!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE >= DATE('".$this->fromDate."') ";
				$andFlag = true;
			}
			if($this->toDate!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE <= DATE('".$this->toDate."') ";
				$andFlag = true;
			}
			if($this->lot_date!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE = DATE('".$this->lot_date."') ";
				$andFlag = true;
			}
			if($this->lotNum != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " `LOT_NO` = '$this->lotNum' ";
				$andFlag = true;
			}
			if($this->lotStatus!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_STATUS = '$this->lotStatus' ";
				$andFlag = true;
			}
			if($this->designNum!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " DESIGN_CODE = '$this->designNum' ";
				$andFlag = true;
			}
			if($this->machineNum != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " `MACHINE_ID` = '$this->machineNum' ";
				$andFlag = true;
			}
			if($this->customerAccCode!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " CUST_ACC_CODE = '$this->customerAccCode' ";
				$andFlag = true;
			}
			$query .= " ORDER BY LOT_DATE ASC LIMIT $start,$total ";
			$result = mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			//exit();

			return $result;
		}

		public function report(){
			$query = "SELECT * FROM emb_lot_register  ";
			$andFlag = false;
			if($this->fromDate!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE >= DATE('".$this->fromDate."') ";
				$andFlag = true;
			}
			if($this->toDate!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE <= DATE('".$this->toDate."') ";
				$andFlag = true;
			}
			if($this->lot_date!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_DATE = DATE('".$this->lot_date."') ";
				$andFlag = true;
			}
			if($this->lotNum != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " `LOT_NO` = '$this->lotNum' ";
				$andFlag = true;
			}
			if($this->lotStatus != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " LOT_STATUS = '$this->lotStatus' ";
				$andFlag = true;
			}
			if($this->designNum!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " DESIGN_CODE = '$this->designNum' ";
				$andFlag = true;
			}
			if($this->machineNum != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " `MACHINE_ID` = '$this->machineNum' ";
				$andFlag = true;
			}
			if($this->customerAccCode!=''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " CUST_ACC_CODE = '$this->customerAccCode' ";
				$andFlag = true;
			}
			$query .= " ORDER BY LOT_DATE ASC ";
			$result = mysql_query($query);

			return $result;
		}

		public function getOutwardLotQtyDeliveredWholeLot($custAccCode,$lotNum,$lot_date){
			$query = "SELECT SUM(MEASURE_LENGTH) AS QTY_DELIVERED FROM emb_lot_register
					  WHERE CUST_ACC_CODE = '$custAccCode' AND LOT_NO = $lotNum
							AND DATE(LOT_DATE) <= DATE('".$lot_date."') ";
			$stock = mysql_fetch_array(mysql_query($query));
			return $stock['QTY_DELIVERED'];
		}
		public function getLotDetailIdByOutwardDetailId($outward_detail_id){
			$query = "SELECT ID FROM emb_lot_register WHERE OUTWD_DETL_ID = $outward_detail_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function getLotQualityList($customerCode,$lot_num){
			$query = "SELECT QUALITY FROM emb_lot_register
					  WHERE CUST_ACC_CODE = '$customerCode' AND LOT_NO = '$lot_num' ";
			return mysql_query($query);
		}

		public function insert_outaward_detail_id($lot_detail_id,$outward_detail_id){
			$query = "UPDATE emb_lot_register SET OUTWD_DETL_ID = $outward_detail_id WHERE ID = $lot_detail_id";
			return mysql_query($query);
		}

		public function insert_outaward_id($lot_detail_id,$outward_id){
			$query = "UPDATE emb_lot_register SET OUTWD_ID = $outward_id WHERE ID = $lot_detail_id";
			return mysql_query($query);
		}

		public function removeOutwardDetailId($outward_detail_id){
			$query = "UPDATE emb_lot_register SET OUTWD_DETL_ID = 0 WHERE OUTWD_DETL_ID = $outward_detail_id";
			return mysql_query($query);
		}

		public function statusByOutwardDetailId($id,$status){
			$query = "UPDATE emb_lot_register SET LOT_STATUS = '$status' WHERE OUTWD_DETL_ID = $id AND LOT_STATUS != 'R'";
			return mysql_query($query);
		}

		public function status($id,$status){
			$query = "UPDATE emb_lot_register SET LOT_STATUS = '$status' WHERE ID = $id";
			return mysql_query($query);
		}

		public function getLotQuantityIssued($accCode,$lot_num){
			$query  = "SELECT SUM(MEASURE_LENGTH) AS TOTAL_LENGTH FROM emb_lot_register WHERE CUST_ACC_CODE = '$accCode' AND LOT_NO = '$lot_num' ";
			$record = mysql_query($query);
			$row    = mysql_fetch_assoc($record);
			return $row['TOTAL_LENGTH'];
			//$query = "SELECT SUM(MEASURE_LENGTH) AS TOTAL_LENGTH FROM emb_lot_register WHERE CUST_ACC_CODE = '$accCode' AND LOT_NO = '$lot_num' AND EMB_AMOUNT < 0 AND MEASURE_LENGTH < 0";
			//$record2 = mysql_query($query);
			/*
			$row2['TOTAL_LENGTH'] = 0;
			if(mysql_num_rows($record2)){
				$row2 = mysql_fetch_array($record2);
				$row2['TOTAL_LENGTH'] = str_replace('-','',$row2['TOTAL_LENGTH']);
			}
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_LENGTH']=='')?0-$row2['TOTAL_LENGTH']:$row['TOTAL_LENGTH']-$row2['TOTAL_LENGTH'];
			}else{
				return 0 - $row2['TOTAL_LENGTH'];
			}
			*/
		}
		public function getVoucherId($lot_id){
			$query = "SELECT VOUCHER_ID FROM emb_lot_register  WHERE ID = $lot_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function insertVoucherId($lot_id,$voucher_id){
			$query = "UPDATE emb_lot_register SET VOUCHER_ID = $voucher_id WHERE ID = $lot_id";
			return mysql_query($query);
		}
		public function delete($lot_id){
			$query = "DELETE FROM emb_lot_register WHERE ID = $lot_id";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function checkIfClaimIsTransferred($lot_id){
			$query = "SELECT CLAIM_ID FROM outward WHERE CLAIM_ID != '' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$id_list = array();
				while($row = mysql_fetch_array($records)){
					$id_row = explode(',',$row['CLAIM_ID']);
					foreach($id_row as $k=>$v){
						$id_list[] = $v;
					}
				}
				if(in_array($lot_id,$id_list)){
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		}
		public function getClaimedLengthOfMachine($machine_id){
			$query = "SELECT MEASURE_LENGTH,EMB_AMOUNT FROM emb_lot_register
					  WHERE LOT_STATUS = 'R'
					  AND EMB_AMOUNT < 0 AND MACHINE_ID = $machine_id";
			$record = mysql_query($query);
			$returnRow = array();
			$returnRow['CLOTH_LENGTH'] = 0;
			$returnRow['AMOUNT']       = 0;
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					if($row['MEASURE_LENGTH'] < 0){
						$returnRow['CLOTH_LENGTH'] -= $row['MEASURE_LENGTH'];
					}
					$returnRow['AMOUNT'] -= $row['EMB_AMOUNT'];
				}
			}
			return $returnRow;
		}
		public function insertOutwardDate($lot_detail_id,$outward_date){
			$query = "UPDATE emb_lot_register SET `OUTWD_DATE` = DATE('".$outward_date."') WHERE ID = '$lot_detail_id' ";
			return mysql_query($query);
		}
		public function getClaimedLengthOfMachineDateRange($machine_id,$fromDate,$toDate){
			$query = "SELECT MEASURE_LENGTH,EMB_AMOUNT,ID FROM emb_lot_register
					  WHERE LOT_STATUS = 'R'
					  AND EMB_AMOUNT < 0 AND MACHINE_ID = $machine_id
					  AND LOT_DATE >= DATE('".$fromDate."')
					  AND LOT_DATE <= DATE('".$toDate."') ";
			$record = mysql_query($query);
			$returnRow = array();
			$returnRow['CLOTH_LENGTH'] = 0;
			$returnRow['AMOUNT']       = 0;
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					if($this->checkIfClaimIsTransferred($row['ID'])){
						continue;
					}
					if($row['MEASURE_LENGTH'] < 0){
						$returnRow['CLOTH_LENGTH'] -= $row['MEASURE_LENGTH'];
					}
					$returnRow['AMOUNT'] -= $row['EMB_AMOUNT'];
				}
			}
			return $returnRow;
		}
		public function setPartialStatus($lot_register_id,$status){
			$query = "UPDATE `emb_lot_register` SET `PARTIAL_STATUS` = '$status' WHERE `ID` = '$lot_register_id' ";
			return mysql_query($query);
		}
		public function getBillNumberFromOutwardClaim($lot_id){
			$query = "SELECT BILL_NO FROM outward WHERE $lot_id IN (CLAIM_ID)";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['BILL_NO'];
			}else{
				return NULL;
			}
		}
		public function getClaimLotListExcept($customerCode,$calimLotList){
			$query = "SELECT ID,EMB_AMOUNT,LOT_DATE FROM emb_lot_register WHERE CUST_ACC_CODE = '$customerCode' AND LOT_STATUS = 'R' ";
			if($calimLotList != NULL){
				$query .= "AND ID NOT IN (".$calimLotList.")";
			}
			$records = mysql_query($query);
			return $records;
		}
		public function getClaimLengthByMachine($machine_id){
			$query = "SELECT  MEASURE_LENGTH,EMB_AMOUNT FROM emb_lot_register WHERE MACHINE_ID = $machine_id AND  MEASURE_LENGTH < 0 ";
			$records = mysql_query($query);
			$machineSale = array();
			$machineSale['LENGTH'] = 0;
			$machineSale['AMOUNT'] = 0;
			if(mysql_num_rows($records)){
				while($lotRow = mysql_fetch_array($records)){
					$rowlength =  $lotRow['MEASURE_LENGTH'];
					$rowlength = str_replace('-','',$rowlength);
					$lotRow['EMB_AMOUNT'] = str_replace('-','',$lotRow['EMB_AMOUNT']);
					settype($rowlength,'int');
					$machineSale['LENGTH'] += $rowlength;
					$machineSale['AMOUNT'] += $lotRow['EMB_AMOUNT'];
				}
				return $machineSale;

			}else{
				return 0;
			}
		}
	}
?>
