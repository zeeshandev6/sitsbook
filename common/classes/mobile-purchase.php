<?php
	class ScanPurchase
	{
		public $user_id;
		public $scan_date;
		public $bill_no;
		public $supp_acc_code;
		public $supplier_name;
		public $inv_notes;
		public $voucher_id;

		public $categories;
		public $fromDate;
		public $toDate;

		public $num_record;

		/* Reporting */
		public $item_id;
		public $barcode;
		public $state;
		public $imei;

		/* Reporting */
		public function getList(){
			$query = "SELECT * FROM mobile_purchase WHERE  ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(*) FROM mobile_purchase ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM mobile_purchase ORDER BY BILL_NO ASC,SCAN_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($billNum,$supplierCode){
			$query = "SELECT BILL_NO FROM mobile_purchase WHERE BILL_NO = $billNum AND SUPP_ACC_CODE = '$supplierCode'";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM mobile_purchase WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($id){
			$query = "SELECT * FROM mobile_purchase WHERE ID = '$id'  ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function getAny($id){
			$query = "SELECT * FROM mobile_purchase WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function save(){
			$query = "INSERT INTO `mobile_purchase`(`USER_ID`,
											 `SCAN_DATE`,
											 `BILL_NO`,
											 `SUPP_ACC_CODE`,
											 `SUPPLIER_NAME`,
											 `NOTES`)
									  VALUES ('$this->user_id',
									  		  '$this->scan_date',
									  		  '$this->bill_no',
											  '$this->supp_acc_code',
											  '$this->supplier_name',
											  '$this->inv_notes')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($invId){
			$query = "UPDATE `mobile_purchase` SET
									`SCAN_DATE`='$this->scan_date',
									`BILL_NO`='$this->bill_no',
									`SUPP_ACC_CODE`='$this->supp_acc_code',
									`SUPPLIER_NAME` = '$this->supplier_name',
									`NOTES` = '$this->inv_notes' WHERE `ID` = $invId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS mobile_purchase.* FROM mobile_purchase ";
			$andFlag = false;

			if($this->imei != ""){
				$query .= " LEFT JOIN mobile_purchase_details ON mobile_purchase_details.SP_ID = mobile_purchase.ID ";
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase_details.BARCODE = '".$this->imei."' ";
				$andFlag = true;
			}

			if(trim($this->supp_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase.SUPP_ACC_CODE ='".$this->supp_acc_code."' ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase.BILL_NO = '$this->bill_no' ";
				$andFlag = true;
			}
			if($this->supplier_name!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase.SUPPLIER_NAME LIKE '%$this->supplier_name%' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase.SCAN_DATE >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase.SCAN_DATE <= '".$this->toDate."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY mobile_purchase.BILL_NO DESC ";
			$query .= " LIMIT $start,$per_page ";

			$result = mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->num_record = $totalRecords['total'];
			return $result;
		}
		public function getDailyPurchases($report_date){
			$query = "SELECT mobile_purchase.*,mobile_purchase_details.*
					  FROM mobile_purchase INNER JOIN mobile_purchase_details
					  ON mobile_purchase.ID = mobile_purchase_details.SP_ID
					  WHERE mobile_purchase.SCAN_DATE = '".$report_date."' GROUP BY mobile_purchase_details.SP_ID ORDER BY mobile_purchase_details.SP_ID";
			return mysql_query($query);
		}
		public function report(){
			$query = "SELECT * FROM mobile_purchase LEFT OUTER JOIN mobile_purchase_details ON mobile_purchase.ID = mobile_purchase_details.SP_ID  ";
			$andFlag = false;

			if(trim($this->user_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`USER_ID` ='".$this->user_id."' ";
				$andFlag = true;
			}

			if(trim($this->supp_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SUPP_ACC_CODE` ='".$this->supp_acc_code."' ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.ITEM_ID ='".$this->item_id."' ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = '$this->bill_no' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SCAN_DATE` >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SCAN_DATE` <= '".$this->toDate."' ";
				$andFlag = true;
			}
			if(trim($this->state)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.STOCK_STATUS = '".$this->state."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY mobile_purchase.BILL_NO ASC,mobile_purchase_details.BARCODE DESC ";
        	$result = mysql_query($query);
			return $result;
		}

		public function availableStockReport(){
			$query = "SELECT *,SUM(mobile_purchase_details.PRICE) AS TOTAL_PRICE,COUNT(mobile_purchase_details.ITEM_ID) AS QTY FROM mobile_purchase LEFT OUTER JOIN mobile_purchase_details ON mobile_purchase.ID = mobile_purchase_details.SP_ID  ";
			$andFlag = false;

			if(trim($this->user_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`USER_ID` ='".$this->user_id."' ";
				$andFlag = true;
			}

			if(trim($this->supp_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SUPP_ACC_CODE` ='".$this->supp_acc_code."' ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.ITEM_ID ='".$this->item_id."' ";
				$andFlag = true;
			}
			if(trim($this->categories) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.ITEM_ID IN (SELECT ID FROM items WHERE ITEM_CATG_ID IN ($this->categories))  ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = '$this->bill_no' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SCAN_DATE` >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SCAN_DATE` <= '".$this->toDate."' ";
				$andFlag = true;
			}
			if(trim($this->state)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.STOCK_STATUS = '".$this->state."' ";
				$andFlag = true;
			}
			$query .= " GROUP BY mobile_purchase_details.ITEM_ID,mobile_purchase_details.PURCHASE_PRICE ORDER BY mobile_purchase.BILL_NO ASC,mobile_purchase_details.BARCODE DESC ";
			$result = mysql_query($query);
			return $result;
		}

		public function supplierStockReport(){
			$query = "SELECT *,COUNT(mobile_purchase_details.ITEM_ID) AS ITEM_STOCK FROM mobile_purchase LEFT OUTER JOIN mobile_purchase_details ON mobile_purchase.ID = mobile_purchase_details.SP_ID  ";
			$andFlag = false;

			if(trim($this->user_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`USER_ID` ='".$this->user_id."' ";
				$andFlag = true;
			}

			if(trim($this->supp_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SUPP_ACC_CODE` ='".$this->supp_acc_code."' ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.ITEM_ID ='".$this->item_id."' ";
				$andFlag = true;
			}
			if(trim($this->state)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.STOCK_STATUS = '".$this->state."' ";
				$andFlag = true;
			}

			$query .= " GROUP BY mobile_purchase_details.ITEM_ID,mobile_purchase_details.COLOUR ORDER BY mobile_purchase.BILL_NO ASC,mobile_purchase_details.BARCODE DESC  ";
        	$result = mysql_query($query);
			echo mysql_error();
			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM mobile_purchase WHERE ID =$id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `mobile_purchase` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM mobile_purchase WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function getSupplierAmountFromVoucher($inventory_id){
			$query = " SELECT AMOUNT FROM voucher_details WHERE SUBSTR(ACCOUNT_CODE,1,6) = '040101' AND VOUCHER_ID = (SELECT VOUCHER_ID FROM mobile_purchase WHERE ID = $inventory_id) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function setSmsStatus($inventory_id,$sms_status){
			$query = "UPDATE `mobile_purchase` SET `SMS_STATUS` = '$sms_status' WHERE `ID` =  $inventory_id ";
			return mysql_query($query);
		}
		public function validateDate($date){
		    $d = DateTime::createFromFormat('d-m-Y', $date);
		    return $d && $d->format('d-m-Y') == $date;
		}
	}
?>
