<?php
	class UserAccounts{
		public $userType;
		public $userName;
		public $password;
		public $salt;
		public $firstName;
		public $lastName;
		public $designation;
		public $designation_type;
		public $contactNo;
		public $email;
		public $city;
		public $address;
		public $permissions;
		public $joinDate;
		public $active;
		public $branch_id;
		public $cash_acc_code;
		public $cashinhand;
		public $allow_other_cash;

		public function getDetails($user_id){
			$query = "SELECT * FROM users WHERE ID = $user_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row;
			}else{
				return NULL;
			}
		}

		public function getFullName($user_id){
			$query = "SELECT FIRST_NAME,LAST_NAME FROM users WHERE ID = '$user_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getBranchId($user_id){
			$query = "SELECT BRANCH_ID FROM users WHERE ID = '$user_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return (int)$row['BRANCH_ID'];
			}else{
				return 0;
			}
		}

		public function getActiveList(){
			$query = "SELECT * FROM users WHERE ACTIVE = 'Y'";
			return mysql_query($query);
		}
		public function getActiveListByDesType($des_type){
			$query = "SELECT * FROM users WHERE DESIGNATION_TYPE = '$des_type' ACTIVE = 'Y'";
			return mysql_query($query);
		}
		public function getFullList(){
			$query = "SELECT * FROM users ORDER BY ID ASC";
			return mysql_query($query);
		}
		public function getFullListCashInHand(){
			$query = "SELECT * FROM users ORDER BY ID ASC";
			return mysql_query($query);
		}
		public function getAccountByCatAccCode($sixDigitCode){
			$query = "SELECT * FROM account_code WHERE SUBSTR(ACC_CODE,1,6) = '$sixDigitCode' AND LENGTH(ACC_CODE) = 9 ORDER BY ACC_CODE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function save(){
			$userExists = $this->userNameExists($this->userName);
			$query = "INSERT INTO `users`(`USER_TYPE`,
										  `USER_NAME`,
										  `PASSWORDD`,
										  `SALT`,
										  `FIRST_NAME`,
										  `LAST_NAME`,
										  `DESIGNATION`,
											`DESIGNATION_TYPE`,
										  `CONTACT_NO`,
										  `EMAIL`,
										  `CITY`,
										  `ADDRESS`,
										  `JOIN_DATE`,
										  `ACTIVE`,
											`BRANCH_ID`,
										  `CASH_ACC_CODE`,
										  `CASH_IN_HAND`,
											`ALLOW_OTHER_CASH`)
									VALUES ('$this->userType',
											'$this->userName',
											'$this->password',
											'$this->salt',
											'$this->firstName',
											'$this->lastName',
											'$this->designation',
											'$this->designation_type',
											'$this->contactNo',
											'$this->email',
											'$this->city',
											'$this->address',
											'$this->joinDate',
											'$this->active',
											'$this->branch_id',
											'$this->cash_acc_code',
											'$this->cashinhand',
											'$this->allow_other_cash')";
			if($userExists == 0){
				mysql_query($query);
			}
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `users` SET `USER_TYPE`    		= '$this->userType',
																	 `USER_NAME`    		= '$this->userName',
																	 `FIRST_NAME`   		= '$this->firstName',
																	 `LAST_NAME`    		= '$this->lastName',
																	 `CONTACT_NO`   		= '$this->contactNo',
																	 `DESIGNATION`  		= '$this->designation',
																	 `DESIGNATION_TYPE` = '$this->designation_type',
																	 `EMAIL`        		= '$this->email',
																	 `CITY`         		= '$this->city',
																	 `ADDRESS`      		= '$this->address',
																	 `BRANCH_ID`    		= '$this->branch_id',
																	 `CASH_IN_HAND` 		= '$this->cashinhand',
																 	 `ALLOW_OTHER_CASH` = '$this->allow_other_cash' WHERE ID = $id";
			return mysql_query($query);
		}
		public function userStatus($status,$id){
			$query = "UPDATE `users` SET ACTIVE = '$status' WHERE ID = $id";
			return mysql_query($query);
		}
		public function setSecretCode($code,$id){
			$query = "UPDATE `users` SET SECRET_CODE = '$code' WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function checkLogin($user_name,$password){
			$user_name = mysql_real_escape_string($user_name);
			$password = mysql_real_escape_string($password);

			$query = "SELECT PASSWORDD,SALT,ID FROM `users` WHERE USER_NAME = '$user_name' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				$salt = $row['SALT'];
				$prevPassword = $row['PASSWORDD'];
				$hashedSubmit = hash('sha256',$password.$salt);
				if($prevPassword === $hashedSubmit){
					return $row['ID'];
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

		public function checkPassword($password,$id){
			$query = "SELECT PASSWORDD,SALT,ID FROM `users` WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				$salt = $row['SALT'];
				$prevPassword = $row['PASSWORDD'];
				$hashedSubmit = hash('sha256',$password.$salt);
				if($prevPassword == $hashedSubmit){
					return $row['ID'];
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}

		public function userNameExists($userName){
			$query = "SELECT USER_NAME FROM `users` WHERE USER_NAME = '$userName'";
			$num_of_rows = mysql_num_rows(mysql_query($query));
			return $num_of_rows;
		}

		public function userNameExistsUpdate($userName,$this_id){
			$query = "SELECT USER_NAME FROM `users` WHERE USER_NAME = '$userName' AND ID != $this_id";
			$num_of_rows = mysql_num_rows(mysql_query($query));
			return $num_of_rows;
		}
		public function getPermissions($user_id){
			$query = "SELECT PERMISSIONS FROM users WHERE ID = '$user_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['PERMISSIONS'];
			}else{
				return '';
			}
		}
		public function savePermissions($user_id,$permissions){
			$query = "UPDATE users SET PERMISSIONS = '$permissions' WHERE ID = '$user_id'";
			return mysql_query($query);
		}
		public function changePassword($password,$id){
			$salt = $this->getSalt($id);
			if(strlen($salt) == 64 && $password != ''){
				$saltedPassword = $password.$salt;
				$hashedPassword = hash('sha256',$saltedPassword);
				$query = "UPDATE `users` SET `PASSWORDD` = '$hashedPassword' WHERE ID = $id";
				return mysql_query($query);
			}else{
				return false;
			}
		}
		public function buySalt(){
			$random_salt = hash('sha256', uniqid(mt_rand(1, mt_getrandmax()), true));
			return $random_salt;
		}
		public function delete($id){
			$query = "DELETE FROM `users` WHERE ID = $id AND ID != 1";
			return mysql_query($query);
		}
		public function delete_by_acc_code($acc_code){
			$query = "DELETE FROM `users` WHERE CASH_IN_HAND = '$acc_code'";
			return mysql_query($query);
		}
		public function getDetailsByAccCode($acc_code){
			$query = "SELECT * FROM `users` WHERE CASH_IN_HAND = '$acc_code'";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}
		public function getDetailsByContactNumber($number){
			$query = "SELECT * FROM `users` WHERE CONTACT_NO = '$number' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}
		public function getDetailByUserName($username){
			$query = "SELECT * FROM `users` WHERE USER_NAME = '$username' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}
		public function getIdBySecretCode($code){
			$query = "SELECT ID FROM `users` WHERE SECRET_CODE = '$code' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['ID'];
			}else{
				return NULL;
			}
		}
		public function transactions_exist($user_id){
			$query = "  ";
		}
		//PRIVATE FUNCTIONS

		private function getSalt($id){
			$query = "SELECT SALT FROM `users` WHERE ID = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row =  mysql_fetch_assoc($record);
				return $row['SALT'];
			}else{
				return '';
			}
		}
	}
?>
