<?php
	class DistributorSaleReturn{
		public $saleDate;
		public $user_id;
		public $sale_id;
		public $order_taker;
		public $billNum;
		public $po_number;
		public $source;
		public $customerAccCode;
		public $customerAccTitle;
		public $customer_name;
		public $over_discount;
		public $discount_type;
		public $bill_discount;

		public $fromDate;
		public $toDate;
		public $found_records;

		public function getList(){
			$query = "SELECT * FROM dist_sale_return WHERE ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function getListBySaleId($sale_id){
			$query = "SELECT * FROM dist_sale_return WHERE SALE_ID = '$sale_id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getBillNumbers($customerAccountCode){
			$query = "SELECT BILL_NO FROM dist_sale_return WHERE CUST_ACC_CODE = '$customerAccountCode' ";
			return mysql_query($query);
		}

		public function getSaleId($customer_code,$billNum){
			$query = "SELECT ID FROM dist_sale_return WHERE CUST_ACC_CODE = '$customer_code' AND BILL_NO = '$billNum'  ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['ID'] == '')?"0":$row['ID'];
			}else{
				return "0";
			}
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM dist_sale_return ";
			$records = mysql_query($query);
			return $records;
		}
		public function countReturnRows(){
			$query = "SELECT count(ID) FROM dist_sale_return WHERE SALE_ID = 0 ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM dist_sale_return ORDER BY BILL_NO ASC,SALE_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($billNum,$customerCode,$sale_id){
			$query = "SELECT BILL_NO FROM dist_sale_return WHERE BILL_NO = $billNum
													AND CUST_ACC_CODE = '$customerCode'
													AND ID != $sale_id";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM dist_sale_return WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function getBranchId($sale_id){
			$query = "SELECT BRANCH_ID FROM users WHERE ID IN (SELECT USER_ID FROM dist_sale_return WHERE ID = '$sale_id')";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return (int)$row['BRANCH_ID'];
			}else{
				return 0;
			}
		}
		public function getAny($id){
			$query = "SELECT * FROM dist_sale_return WHERE ID = $id";
			$records = mysql_query($query);
			return $records;
		}
		public function quantityReceipt($inv_id){
			$query = "SELECT SUM(QTY_RECEIPT) as totalQty FROM dist_sale_return_details WHERE ID = $inv_id";
			$totalQty = mysql_fetch_array(mysql_query($query));
			return $totalQty['totalQty'];
		}
		public function save(){
			if(((int)$this->user_id) == 0){
				$this->user_id = $_SESSION['classuseid'];
			}
			$query = "INSERT INTO `dist_sale_return`(`USER_ID`,
											 `ORDER_TAKER`,
											 `SALE_ID`,
											 `SALE_DATE`,
											 `BILL_NO`,
											 `PO_NUMBER`,
											 `CUST_ACC_CODE`,
											 `CUSTOMER_NAME`,
											 `DISCOUNT`,
											 `DISCOUNT_TYPE`,
											 `BILL_DISCOUNT`)
									  VALUES ('$this->user_id',
											  '$this->order_taker',
											  '$this->sale_id',
											  '$this->saleDate',
											  '$this->billNum',
											  '$this->po_number',
											  '$this->customerAccCode',
											  '$this->customer_name',
											  '$this->over_discount',
											  '$this->discount_type',
											  '$this->bill_discount')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($invId){
			if(((int)$this->user_id) == 0){
				$this->user_id = $_SESSION['classuseid'];
			}
			$query = "UPDATE `dist_sale_return` SET
									`USER_ID`				= '$this->user_id',
									`ORDER_TAKER`			= '$this->order_taker',
									`SALE_DATE` 			= '$this->saleDate',
									`BILL_NO` 				= '$this->billNum',
									`PO_NUMBER` 			= '$this->po_number',
									`CUST_ACC_CODE` 		= '$this->customerAccCode',
									`DISCOUNT`      		= '$this->over_discount',
									`DISCOUNT_TYPE` 		= '$this->discount_type',
									`BILL_DISCOUNT` 		= '$this->bill_discount'  WHERE `ID` = $invId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getQuantityPerBill($sale_id){
			$query = "SELECT SUM(CARTONS) AS TOTAL_CARTONS,SUM(DOZENS) AS TOTAL_DOZENS,SUM(QUANTITY) AS TOTAL_QUANTITY FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSum($sale_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
        public function getInventoryAmountSumBySaleId($sale_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM dist_sale_return_details WHERE SALE_ID IN (SELECT ID FROM dist_sale_return WHERE SALE_ID = '$sale_id' )  ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSumByBillNumber($bill_no){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM dist_sale_return_details WHERE SALE_ID IN (SELECT ID FROM dist_sale_return WHERE BILL_NO = $bill_no )  ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:((float)$row['AMOUNT']);
			}else{
				return 0;
			}
		}
		public function getInventoryTaxAmountSum($sale_id){
			$query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TAX_AMNT']=='')?0:$row['TAX_AMNT'];
			}else{
				return 0;
			}
		}
		public function getCustomersList(){
			$query = "SELECT * FROM `customers` ORDER BY `CUST_ACC_TITLE` ASC";
			return mysql_query($query);
		}
		public function inventorySearch(){
			$query = "SELECT ID FROM dist_sale_return WHERE  ";
			$query .= "`CUST_ACC_CODE` = '".$this->customerAccCode."'";
			$query .= " AND `BILL_NO` = ".$this->billNum ;
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function searchInventory(){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM dist_sale_return ";
			$andFlag = false;

			if(trim($this->customerAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`CUST_ACC_CODE` ='".$this->customerAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY BILL_NO DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM dist_sale_return ";
			$andFlag = false;

			if($this->sale_id!=''){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= " `SALE_ID` = '".$this->sale_id."' ";
				$andFlag = true;
			}

			if(trim($this->customerAccCode)!=""){
				$query  .= ($andFlag)?"":" WHERE ";
				$query  .= ($andFlag)?" AND ":"";
				$query  .= "`CUST_ACC_CODE` ='".$this->customerAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			if($this->po_number != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `PO_NUMBER` = '$this->po_number' ";
				$andFlag = true;
			}
			$query .= " ORDER BY SALE_DATE DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function getItemsList($report_date){
			$query = "SELECT ITEM_ID FROM dist_sale_return_details WHERE SALE_ID IN (SELECT ID FROM dist_sale_return WHERE SALE_DATE = '$report_date'  ";
			if($this->user_id!=''){
				$query .= " AND USER_ID = '$this->user_id' ";
			}
			$query .= " ) ";
			$result= mysql_query($query);
			$id_arr= array();
			if(mysql_num_rows($result)){
				while($row=mysql_fetch_assoc($result)){
					$id_arr[] = $row['ITEM_ID'];
				}
			}
			return $id_arr;
		}
		public function getItemsListBySaleId($sale_id){
			$query = "SELECT ITEM_ID FROM dist_sale_return_details WHERE SALE_ID  = '$sale_id' ";

			$result= mysql_query($query);
			$id_arr= array();
			if(mysql_num_rows($result)){
				while($row=mysql_fetch_assoc($result)){
					$id_arr[] = $row['ITEM_ID'];
				}
			}
			return $id_arr;
		}
		public function distSalesmanReport(){
			$query  = " SELECT * FROM dist_sale_return ";

			$andFlag = false;
			if(trim($this->saleDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " dist_sale_return.SALE_DATE = '".$this->saleDate."' ";
				$andFlag = true;
			}
			if(trim($this->user_id)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " dist_sale_return.USER_ID = '".$this->user_id."' ";
				$andFlag = true;
			}
			if(trim($this->order_taker)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " dist_sale_return.ORDER_TAKER = '".$this->order_taker."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY dist_sale_return.SALE_DATE ASC, dist_sale_return.BILL_NO ASC ";
			return  mysql_query($query);
		}
		public function getSalesmanRecordList($report_date){
			$query = "SELECT * FROM dist_sale_return WHERE SALE_DATE = '$report_date' ";
			if($this->order_taker != ''){
				$query .= " AND ORDER_TAKER = '$this->order_taker' ";
			}
			if($this->user_id!=''){
				$query .= " AND USER_ID = '$this->user_id' ";
			}
			$result= mysql_query($query);
			$id_arr= array();
			if(mysql_num_rows($result)){
				while($row=mysql_fetch_assoc($result)){
					$id_arr[] = $row;
				}
			}
			return $id_arr;
		}

		public function delete($id){
			$query = "DELETE FROM dist_sale_return WHERE ID =$id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteUnRelatedInventoryEntries(){
			$query = "DELETE FROM dist_sale_return WHERE ID NOT IN (SELECT ID FROM inventory_details) AND VOUCHER_ID = 0";
			mysql_query($query);
		}
		public function getSaleDetailArrayIdOnly($sale_id){
			$query = "SELECT ID FROM dist_sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getSaleDetailArrayItemOnly($sale_id){
			$query = "SELECT dist_sale_return_details.ITEM_ID,items.* FROM dist_sale_return_details 
					  JOIN items ON items.ID = dist_sale_return_details.ITEM_ID WHERE SALE_ID = $sale_id ";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row;
				}
			}
			return $returnArray;
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `dist_sale_return` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function get_item_qty_by_bill($sale_id,$item_id){
			$query = "SELECT SUM(dist_sale_return_details.QUANTITY) AS QTY FROM dist_sale_return_details
					  INNER JOIN dist_sale_return ON dist_sale_return_details.SALE_ID = dist_sale_return.ID
					  WHERE dist_sale_return_details.ITEM_ID = '$item_id' AND dist_sale_return.SALE_ID = '$sale_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['QTY'];
			}else{
				return 0;
			}
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM dist_sale_return WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
	}
?>
