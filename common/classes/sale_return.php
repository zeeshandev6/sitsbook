<?php
	class SaleReturn{
		public $saleDate;
		public $sale_id;
		public $billNum;
		public $source;
		public $customerAccCode;
		public $customerAccTitle;
		public $customer_name;
		public $over_discount;
		public $discount_type;
		public $bill_discount;
		public $registered_tax;
		public $sale_invoice_link;

		public $fromDate;
		public $toDate;
		public $found_records;

		public function getList(){
			$query = "SELECT * FROM sale_return WHERE ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function getListBySaleId($sale_id){
			$query = "SELECT * FROM sale_return WHERE SALE_ID = '$sale_id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getDailySales($report_date){
			$query = "SELECT sale_return.*,sale_return_details.*
					  FROM sale_return INNER JOIN sale_return_details
					  ON sale_return.ID = sale_return_details.SALE_ID
					  WHERE sale_return.SALE_DATE = '".$report_date."' GROUP BY sale_return_details.SALE_ID ORDER BY sale_return_details.SALE_ID ASC";
			return mysql_query($query);
		}
		public function getBillNumbers($customerAccountCode){
			$query = "SELECT BILL_NO FROM sale_return WHERE CUST_ACC_CODE = '$customerAccountCode' ";
			return mysql_query($query);
		}
		public function saleTaxItemsList($report_date){
			$query = " SELECT ITEM_ID,SUM(QUANTITY) AS TOTAL_QTY  FROM sale_return_details
					   WHERE SALE_ID IN (SELECT ID FROM sale_return WHERE REGISTERED_TAX = 'Y' AND SALE_DATE <= '".$report_date."')   GROUP BY ITEM_ID ";
			$result= mysql_query($query);
			return $result;
		}
		public function getSaleId($customer_code,$billNum){
			$query = "SELECT ID FROM sale_return WHERE CUST_ACC_CODE = '$customer_code' AND BILL_NO = '$billNum'  ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['ID'] == '')?"0":$row['ID'];
			}else{
				return "0";
			}
		}
		public function getCostForRepoSummary($sale_return_date){
			$query = "SELECT SUM(AMOUNT) AS COST FROM voucher_details WHERE ACCOUNT_CODE = '0301040001' AND TRANSACTION_TYPE = 'Cr' AND VOUCHER_ID IN (SELECT VOUCHER_ID FROM sale_return WHERE SALE_DATE = '$sale_return_date' ) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['COST'] == '')?"0":$row['COST'];
			}else{
				return "0";
			}
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM sale_return ";
			$records = mysql_query($query);
			return $records;
		}
		public function countReturnRows(){
			$query = "SELECT count(ID) FROM sale_return WHERE SALE_ID = 0 ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM sale_return ORDER BY BILL_NO ASC,SALE_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($billNum,$customerCode,$sale_id){
			$query = "SELECT BILL_NO FROM sale_return WHERE BILL_NO = $billNum
													AND CUST_ACC_CODE = '$customerCode'
													AND ID != $sale_id";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM sale_return WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function getBranchId($sale_id){
			$query = "SELECT BRANCH_ID FROM users WHERE ID IN (SELECT USER_ID FROM sale_return WHERE ID = '$sale_id')";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return (int)$row['BRANCH_ID'];
			}else{
				return 0;
			}
		}
		public function getAny($id){
			$query = "SELECT * FROM sale_return WHERE ID = $id";
			$records = mysql_query($query);
			return $records;
		}
		public function quantityReceipt($inv_id){
			$query = "SELECT SUM(QTY_RECEIPT) as totalQty FROM sale_return_details WHERE ID = $inv_id";
			$totalQty = mysql_fetch_array(mysql_query($query));
			return $totalQty['totalQty'];
		}
		public function save(){
			if(isset($_SESSION['classuseid'])){
				$user_id = $_SESSION['classuseid'];
			}else{
				return false;
			}
			$query = "INSERT INTO `sale_return`(`USER_ID`,
											 `SALE_ID`,
											 `SALE_DATE`,
											 `BILL_NO`,
											 `CUST_ACC_CODE`,
											 `CUSTOMER_NAME`,
											 `DISCOUNT`,
											 `DISCOUNT_TYPE`,
											 `BILL_DISCOUNT`,
											 `REGISTERED_TAX`,
											 `SALE_INVOICE_LINK`)
									  VALUES ('$user_id',
									  		  '$this->sale_id',
									  		  '$this->saleDate',
									  		  '$this->billNum',
											  '$this->customerAccCode',
											  '$this->customer_name',
											  '$this->over_discount',
											  '$this->discount_type',
											  '$this->bill_discount',
											  '$this->registered_tax',
											  '$this->sale_invoice_link')";
			mysql_query($query);
			echo mysql_error();
			return mysql_insert_id();
		}
		public function update($invId){
			$query = "UPDATE `sale_return` SET
									`SALE_DATE` 		= '$this->saleDate',
									`BILL_NO` 		 	= '$this->billNum',
									`CUST_ACC_CODE`  	= '$this->customerAccCode',
									`DISCOUNT`       	= '$this->over_discount',
									`DISCOUNT_TYPE`  	= '$this->discount_type',
									`BILL_DISCOUNT`  	= '$this->bill_discount',
									`REGISTERED_TAX` 	= '$this->registered_tax',
									`SALE_INVOICE_LINK` = '$this->sale_invoice_link'  WHERE `ID` = $invId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getQuantityPerBill($sale_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_QUANTITY FROM sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_QUANTITY'] == '')?0:$row['TOTAL_QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSum($sale_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryTaxAmountSum($sale_id){
			$query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TAX_AMNT']=='')?0:$row['TAX_AMNT'];
			}else{
				return 0;
			}
		}
		public function getCustomersList(){
			$query = "SELECT * FROM `customers` ORDER BY `CUST_ACC_TITLE` ASC";
			return mysql_query($query);
		}
		public function inventorySearch(){
			$query = "SELECT ID FROM sale_return WHERE  ";
			$query .= "`CUST_ACC_CODE` = '".$this->customerAccCode."'";
			$query .= " AND `BILL_NO` = ".$this->billNum ;
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function searchInventory(){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM sale_return ";
			$andFlag = false;

			if(trim($this->customerAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`CUST_ACC_CODE` ='".$this->customerAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY BILL_NO DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
      $this->found_records = $totalRecords['total'];

			return $result;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM sale_return ";
			$andFlag = false;

			if(trim($this->customerAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`CUST_ACC_CODE` ='".$this->customerAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY SALE_DATE DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM sale_return WHERE ID =$id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteUnRelatedInventoryEntries(){
			$query = "DELETE FROM sale_return WHERE ID NOT IN (SELECT ID FROM inventory_details) AND VOUCHER_ID = 0";
			mysql_query($query);
		}
		public function getSaleDetailArrayIdOnly($sale_id){
			$query = "SELECT ID FROM sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getSaleDetailArrayItemOnly($sale_id){
			$query = "SELECT ITEM_ID FROM sale_return_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ITEM_ID'];
				}
			}
			return $returnArray;
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `sale_return` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function get_item_qty_by_bill($sale_id,$item_id){
			$query = "SELECT SUM(sale_return_details.QUANTITY) AS QTY FROM sale_return_details
					  INNER JOIN sale_return ON sale_return_details.SALE_ID = sale_return.ID
					  WHERE sale_return_details.ITEM_ID = '$item_id' AND sale_return.SALE_ID = '$sale_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['QTY'];
			}else{
				return 0;
			}
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM sale_return WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
	}
?>
