<?php
  class FabricContractDetails{
    public $fabric_id;
    public $construction;
    public $grey_width;
    public $quantity;
    public $unit_price;
    public $total_amount;
    public $thaan;
    public $variety;
    public $from_party;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;

    public $found_records;

    public function getList(){
      $query = "SELECT * FROM fabric_contract_details ORDER BY ID DESC ";
      return mysql_query($query);
    }
    public function getListById($id){
      $query = "SELECT * FROM fabric_contract_details WHERE FABRIC_ID = $id";
      return mysql_query($query);
    }
    public function getListByLotNo($lot_no){
      $query = "SELECT * FROM fabric_contract_details WHERE FABRIC_ID IN (SELECT ID FROM fabric_contracts WHERE LOT_NO = $lot_no AND VOUCHER_ID > 0 ) ";
      return mysql_query($query);
    }
    public function save(){
      $query = "INSERT INTO `fabric_contract_details`(
        FABRIC_ID,
        CONSTRUCTION,
        GREY_WIDTH,
        QUANTITY,
        UNIT_PRICE,
        TOTAL_AMOUNT,
        THAAN,
        VARIETY,
        FROM_PARTY
      )
      VALUES(
        '$this->fabric_id',
        '$this->construction',
        '$this->grey_width',
        '$this->quantity',
        '$this->unit_price',
        '$this->total_amount',
        '$this->thaan',
        '$this->variety',
        '$this->from_party'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }
    public function update($id){
      $query = "UPDATE `fabric_contract_details` SET
      `CONSTRUCTION`     = '$this->construction',
      `GREY_WIDTH`       = '$this->grey_width',
      `QUANTITY`         = '$this->quantity',
      `UNIT_PRICE`       = '$this->unit_price',
      `TOTAL_AMOUNT`     = '$this->total_amount',
      `THAAN`            = '$this->thaan',
      `VARIETY`          = '$this->variety',
      `FROM_PARTY`       = '$this->from_party' WHERE ID = '$id' ";
      return mysql_query($query);
    }
    public function getTotalMeters($lot_no){
      $query  = "SELECT SUM(QUANTITY) AS TOTAL_METER FROM `fabric_contract_details` WHERE FABRIC_ID IN (SELECT ID FROM `fabric_contracts` WHERE LOT_NO = '$lot_no') ";
      $result = mysql_query($query);
      $row    = mysql_fetch_assoc($result);
      return $row['TOTAL_METER'];
    }
    public function getTotalMetersIssued($lot_no,$dyeing_lot_no){
      $query  = "SELECT SUM(QUANTITY) AS TOTAL_METER FROM `fabric_contract_details` WHERE FABRIC_ID IN (SELECT ID FROM `fabric_contracts` WHERE LOT_NO = '$lot_no' AND DYEING_UNIT_LOT_NO = '$dyeing_lot_no') ";
      $result = mysql_query($query);
      $row    = mysql_fetch_assoc($result);
      return $row['TOTAL_METER'];
    }
    public function deleteContract($id){
      $query = "DELETE FROM fabric_contract_details WHERE fabric_ID = '".$id."'";
      return mysql_query($query);
    }
    public function delete($id){
      $query = "DELETE FROM fabric_contract_details WHERE ID = '".$id."'";
      return mysql_query($query);
    }
    public function insertVoucherId($contract_id,$voucher_id){
      $query = "UPDATE fabric_contract_details SET VOUCHER_ID = '$voucher_id' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function get_voucher_id($contract_id){
      $query = "SELECT VOUCHER_ID FROM fabric_contract_details WHERE ID = '$contract_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
