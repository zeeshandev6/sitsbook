<?php
  class EmbroideryContractDetails{
    public $fabric_id;
    public $construction;
    public $grey_width;
    public $thaan;
    public $quantity;
    public $rate;
    public $total_amount;
    public $fab_type;
    public $design;
    public $color;
    public $from_source;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;

    public $found_records;

    public function getList(){
      $query = "SELECT * FROM embroidery_contract_details ORDER BY ID DESC ";
      return mysql_query($query);
    }
    public function getListById($id){
      $query = "SELECT * FROM embroidery_contract_details WHERE FABRIC_ID = $id";
      return mysql_query($query);
    }
    public function save(){
      $query = "INSERT INTO `embroidery_contract_details`(
        FABRIC_ID,
        CONSTRUCTION,
        GREY_WIDTH,
        THAAN,
        QUANTITY,
        RATE,
        TOTAL_AMOUNT,
        FAB_TYPE,
        DESIGN,
        COLOR,
        FROM_SOURCE
      )
      VALUES(
        '$this->fabric_id',
        '$this->construction',
        '$this->grey_width',
        '$this->thaan',
        '$this->quantity',
        '$this->rate',
        '$this->total_amount',
        '$this->fab_type',
        '$this->design',
        '$this->color',
        '$this->from_source'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }
    public function update($id){
      $query = "UPDATE `embroidery_contract_details` SET
      `CONSTRUCTION`     = '$this->construction',
      `GREY_WIDTH`       = '$this->grey_width',
      `THAAN`            = '$this->thaan',
      `QUANTITY`         = '$this->quantity',
      `RATE`             = '$this->rate',
      `TOTAL_AMOUNT`     = '$this->total_amount',
      `FAB_TYPE`         = '$this->fab_type',
      `DESIGN`           = '$this->design',
      `COLOR`            = '$this->color',
      `FROM_SOURCE`      = '$this->from_source' WHERE ID = '$id'";
      return mysql_query($query);
    }
    public function deleteContract($id){
      $query = "DELETE FROM embroidery_contract_details WHERE fabric_ID = '".$id."'";
      return mysql_query($query);
    }
    public function delete($id){
      $query = "DELETE FROM embroidery_contract_details WHERE ID = '".$id."'";
      return mysql_query($query);
    }
    public function getSumAmountByLotNumber($lot_no){
      $query  = " SELECT SUM(TOTAL_AMOUNT) AS SUM_AMOUNT FROM `embroidery_contract_details` WHERE FABRIC_ID IN ( SELECT ID FROM `embroidery_contracts` WHERE LOT_NO = '$lot_no' ) ";
      $result = mysql_query($query);
      $row    = mysql_fetch_assoc($result);
      return  $row['SUM_AMOUNT'];
    }
    public function insertVoucherId($contract_id,$voucher_id){
      $query = "UPDATE embroidery_contract_details SET VOUCHER_ID = '$voucher_id' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function getListByLotNo($lot_no){
      $query = "SELECT * FROM embroidery_contract_details WHERE FABRIC_ID IN (SELECT ID FROM embroidery_contracts WHERE LOT_NO = '$lot_no' AND VOUCHER_ID > 0 ) ";
      return mysql_query($query);
    }
    public function get_voucher_id($contract_id){
      $query = "SELECT VOUCHER_ID FROM embroidery_contract_details WHERE ID = '$contract_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
