<?php
  class PackingContracts{
    public $lot_no;
    public $user_id;
    public $supplier_id;
    public $contract_date;
    public $delivery_date;
    public $broker_id;
    public $broker_commission;
    public $measure;
    public $rate;
    public $payment_schedule;
    public $dyeing_unit_lot_no;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;
    public $variety;

    public $found_records;

    public function getList(){
      $query = "SELECT * FROM packing_contracts ORDER BY ID DESC ";
      return mysql_query($query);
    }

    public function getListById($id){
      $query = "SELECT * FROM packing_contracts WHERE ID = $id";
      return mysql_query($query);
    }

    public function getListByLotNo($scid){
      $query = "SELECT * FROM packing_contracts WHERE LOT_NO = '$scid'";
      return mysql_query($query);
    }

    public function getListByIdScid($accid, $scid){
      $query = "SELECT * FROM packing_contracts WHERE USER_ID = '$accid' AND LOT_NO = '$scid'";
      return mysql_query($query);
    }

    public function save(){
      $query = "INSERT INTO `packing_contracts`(
        LOT_NO,
        USER_ID,
        SUPPLIER_ID,
        CONTRACT_DATE,
        DELIVERY_DATE,
        BROKER_ID,
        BROKER_COMMISSION,
        MEASURE,
        RATE,
        PAYMENT_SCHEDULE,
        DYEING_UNIT_LOT_NO
      )
      VALUES(
        '$this->lot_no',
        '$this->user_id',
        '$this->supplier_id',
        '$this->contract_date',
        '$this->delivery_date',
        '$this->broker_id',
        '$this->broker_commission',
        '$this->measure',
        '$this->rate',
        '$this->payment_schedule',
        '$this->dyeing_unit_lot_no'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }

    public function update($id){
      $query = "UPDATE `packing_contracts` SET
      `LOT_NO`                = '$this->lot_no',
      `USER_ID`               = '$this->user_id',
      `SUPPLIER_ID`           = '$this->supplier_id',
      `BROKER_ID`             = '$this->broker_id',
      `CONTRACT_DATE`         = '$this->contract_date',
      `DELIVERY_DATE`         = '$this->delivery_date',
      `BROKER_COMMISSION`     = '$this->broker_commission',
      `MEASURE`               = '$this->measure',
      `RATE`                  = '$this->rate',
      `PAYMENT_SCHEDULE`      = '$this->payment_schedule',
      `DYEING_UNIT_LOT_NO`    = '$this->dyeing_unit_lot_no' WHERE ID = '$id'";
      return mysql_query($query);
    }

    public function delete($id){
      $query = "DELETE FROM packing_contracts WHERE ID = '".$id."'";
      return mysql_query($query);
    }

    public function search($from_date, $todate, $supplier, $broker, $start, $pagelimit){
      $query = "SELECT SQL_CALC_FOUND_ROWS
      packing_contracts.ID as FABRIC_ID,
      packing_contracts.*,

      brokers.ACCOUNT_CODE as BROKER_ACCOUNT_CODE,
      brokers.ACCOUNT_TITLE as BROKER_ACCOUNT_TITLE,

      suppliers.SUPP_ACC_CODE as SUPPLIER_CODE,
      suppliers.SUPP_ACC_TITLE as SUPPLIER_TITLE

      FROM packing_contracts
      LEFT JOIN suppliers    ON packing_contracts.SUPPLIER_ID   = suppliers.SUPP_ACC_CODE
      LEFT JOIN brokers      ON packing_contracts.BROKER_ID     = brokers.ACCOUNT_CODE
      ";
      $andFlag = false;
      if($from_date!=""){
        $from_date = date('Y-m-d',strtotime($from_date));
        $query    .= ($andFlag)?"":" WHERE ";
        $query    .= ($andFlag)?" AND ":" ";
        $query    .= " packing_contracts.CONTRACT_DATE >= '".$from_date."'";
        $andFlag   = true;
      }

      if($todate!=""){
        $todate  = date('Y-m-d',strtotime($todate));
        $query  .= ($andFlag)?"":" WHERE ";
        $query  .= ($andFlag)?" AND ":" ";
        $query  .= " packing_contracts.CONTRACT_DATE <= '".$todate."'";
        $andFlag = true;
      }

      if($supplier!=""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  suppliers.SUPP_ACC_CODE = '$supplier' ";
        $andFlag = true;
      }

      if($this->lot_no != ""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  packing_contracts.LOT_NO = '$this->lot_no' ";
        $andFlag = true;
      }

      if($this->sc_no!=""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  packing_contracts.SC_NO = '$this->sc_no' ";
        $andFlag = true;
      }

      if($broker!=""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  brokers.ACCOUNT_CODE = '$broker' ";
        $andFlag = true;
      }

      $query .= " ORDER BY packing_contracts.ID DESC LIMIT $start,$pagelimit";
      $result = mysql_query($query);

      //exit();

      //getting total numer or reords that matched against search
      $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
      $this->found_records = $totalRecords['total'];

      return $result;
    }
    public function report(){
      $query = "SELECT packing_contracts.ID as fabricId, packing_contracts.*,packing_contract_details.*,
      suppliers.SUPP_ID,
      suppliers.SUPP_ACC_TITLE,
      brokers.ID,
      brokers.FIRST_NAME as brokerFirstName,
      brokers.LAST_NAME as brokerLastName,
      users.ID,
      users.FIRST_NAME as userFirstName,
      users.LAST_NAME as userLastName,
      users.ACTIVE
      FROM packing_contracts
      LEFT JOIN packing_contract_details ON packing_contracts.ID  = packing_contract_details.FABRIC_ID
      LEFT JOIN suppliers      ON packing_contracts.SUPPLIER_ID   = suppliers.SUPP_ID
      LEFT JOIN brokers        ON packing_contracts.BROKER_ID     = brokers.ID
      LEFT JOIN users          ON packing_contracts.USER_ID       = users.ID ";
      $andFlag = false;

      if($this->from_date !=""){
        $this->from_date = date('Y-m-d',strtotime($this->from_date));
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= " packing_contracts.CONTRACT_DATE >= '".$this->from_date."'";
        $andFlag = true;
      }
      if($this->to_date != ""){
        $this->to_date = date('Y-m-d',strtotime($this->to_date));
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= " packing_contracts.CONTRACT_DATE <= '".$this->to_date."'";
        $andFlag = true;
      }
      if($this->lot_no != ""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= " packing_contracts.LOT_NO = '$this->lot_no' ";
        $andFlag = true;
      }
      if($this->supplier_id != ""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  packing_contracts.SUPPLIER_ID = '$this->supplier_id' ";
        $andFlag = true;
      }
      if($this->broker_id != ""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  packing_contracts.BROKER_ID = '$this->broker_id' ";
        $andFlag = true;
      }
      if($this->variety != ""){
        $query .= ($andFlag)?"":" WHERE ";
        $query .= ($andFlag)?" AND ":" ";
        $query .= "  packing_contract_details.VARIETY = '$this->variety' ";
        $andFlag = true;
      }

      $result = mysql_query($query);
      return $result;
    }
    public function getDetail($fabric_id){
      $query = "SELECT * FROM packing_contracts WHERE ID = '$fabric_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row;
      }else{
        return 0;
      }
    }
    public function insertVoucherId($lot_no,$voucher_id){
      $query = "UPDATE packing_contracts SET VOUCHER_ID = '$voucher_id' WHERE LOT_NO = '$lot_no'";
      return mysql_query($query);
    }
    public function getVoucherId($lot_no){
      $query = "SELECT VOUCHER_ID FROM packing_contracts WHERE LOT_NO = '$lot_no' LIMIT 1";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
    public function getSupplierIdByLotNumber($lot_no){
      $query = "SELECT SUPPLIER_ID FROM packing_contracts WHERE LOT_NO = '$lot_no' LIMIT 1";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['SUPPLIER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
