<?php
	class InventoryReturn
	{
		public $purchaseDate;
		public $purchase_id;
		public $billNum;
		public $source;
		public $supplierAccCode;
		public $supplierAccTitle;
		public $supplier_name;
		public $income_tax;
		public $unregistered_tax;
		public $po_number;
		public $fromDate;
		public $toDate;
		public $found_records;

		public function getList(){
			$query = "SELECT * FROM purchase_return WHERE ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function getListByPurchaseId($purchase_id){
			$query = "SELECT * FROM purchase_return WHERE PURCHASE_ID = '$purchase_id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getDailyPurchaseReturns($report_date){
			$query = "SELECT purchase_return.*,purchase_return_details.*
					  FROM purchase_return INNER JOIN purchase_return_details
					  ON purchase_return.ID = purchase_return_details.PURCHASE_ID
					  WHERE purchase_return.PURCHASE_DATE = '".$report_date."' GROUP BY purchase_return_details.PURCHASE_ID ORDER BY purchase_return.ID ASC";
			return mysql_query($query);
		}
		public function getBillNumbers($supplierAccountCode){
			$query = "SELECT BILL_NO FROM purchase_return WHERE SUPP_ACC_CODE = '$supplierAccountCode' ";
			return mysql_query($query);
		}

		public function getPurchaseId($supplier_code,$billNum){
			$query = "SELECT ID FROM purchase_return WHERE SUPP_ACC_CODE = '$supplier_code' AND BILL_NO = '$billNum'  ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['ID'] == '')?"0":$row['ID'];
			}else{
				return "0";
			}
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM purchase_return ";
			$records = mysql_query($query);
			return $records;
		}
		public function countReturnRows(){
			$query = "SELECT count(ID) FROM purchase_return WHERE PURCHASE_ID = 0 ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM purchase_return ORDER BY BILL_NO ASC,PURCHASE_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($billNum,$supplierCode,$purchase_id){
			$query = "SELECT BILL_NO FROM purchase_return WHERE BILL_NO = $billNum
													AND SUPP_ACC_CODE = '$supplierCode'
													AND ID != $purchase_id";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM purchase_return WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function getAny($id){
			$query = "SELECT * FROM purchase_return WHERE ID = $id";
			$records = mysql_query($query);
			return $records;
		}
		public function quantityReceipt($inv_id){
			$query = "SELECT SUM(QTY_RECEIPT) as totalQty FROM purchase_return_details WHERE ID = $inv_id";
			$totalQty = mysql_fetch_array(mysql_query($query));
			return $totalQty['totalQty'];
		}
		public function save(){
			$query = "INSERT INTO `purchase_return`( `PURCHASE_ID`,
													 `PURCHASE_DATE`,
													 `BILL_NO`,
													 `SUPP_ACC_CODE`,
													 `SUPPLIER_NAME`,
												 	 `INCOME_TAX`,
													 `UNREGISTERED_TAX`,
													 `PO_NUMBER`,
													 `USER_ID`)
											  VALUES ('$this->purchase_id',
											  		  '$this->purchaseDate',
											  		  '$this->billNum',
													  '$this->supplierAccCode',
													  '$this->supplier_name',
													  '$this->income_tax',
													  '$this->unregistered_tax',
													  '$this->po_number',
													  ".$_SESSION['classuseid'].")";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `purchase_return` SET
									`PURCHASE_DATE`     = '$this->purchaseDate',
									`BILL_NO` 		    = '$this->billNum',
									`SUPP_ACC_CODE`     = '$this->supplierAccCode',
									`INCOME_TAX`	    = '$this->income_tax',
									`UNREGISTERED_TAX`	= '$this->unregistered_tax',
									`PO_NUMBER`	 	    = '$this->po_number' WHERE `ID` = '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getQuantityPerBill($purchase_id){
			$query = "SELECT SUM(STOCK_QTY) AS TOTAL_QUANTITY FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_QUANTITY'] == '')?0:$row['TOTAL_QUANTITY'];
			}else{
				return 0;
			}
		}
		public function purchaseTaxItemsList($report_date){
			$query = " SELECT ITEM_ID,SUM(STOCK_QTY) AS TOTAL_QTY, (SUM(UNIT_PRICE)/SUM(STOCK_QTY)) AS AVG_COST FROM purchase_return_details WHERE PURCHASE_ID IN (SELECT ID FROM purchase_return WHERE UNREGISTERED_TAX != 'N' AND PURCHASE_DATE <= '".$report_date."') GROUP BY ITEM_ID ";
			$result= mysql_query($query);
			return $result;
		}
		public function getInventoryAmountSum($purchase_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryTaxAmountSum($purchase_id){
			$query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TAX_AMNT']=='')?0:$row['TAX_AMNT'];
			}else{
				return 0;
			}
		}
		public function getSuppliersList(){
			$query = "SELECT * FROM `suppliers` ORDER BY `SUPP_ACC_TITLE` ASC";
			return mysql_query($query);
		}
		public function inventorySearch(){
			$query = "SELECT ID FROM purchase_return WHERE  ";
			$query .= "`SUPP_ACC_CODE` = '".$this->supplierAccCode."'";
			$query .= " AND `BILL_NO` = ".$this->billNum ;
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function searchInventory(){
			$query = "SELECT * FROM purchase_return ";
			$andFlag = false;

			if(trim($this->supplierAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SUPP_ACC_CODE` ='".$this->supplierAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY PURCHASE_DATE DESC ";
			return  mysql_query($query);
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM purchase_return ";
			$andFlag = false;

			if(trim($this->supplierAccCode)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SUPP_ACC_CODE` ='".$this->supplierAccCode."'";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = ".$this->billNum;
				$andFlag = true;
			}
			if($this->po_number != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `PO_NUMBER` = '$this->po_number' ";
				$andFlag = true;
			}
			$query .= " ORDER BY PURCHASE_DATE DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM purchase_return WHERE ID =$id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteUnRelatedInventoryEntries(){
			$query = "DELETE FROM purchase_return WHERE ID NOT IN (SELECT ID FROM inventory_details) AND VOUCHER_ID = 0";
			mysql_query($query);
		}
		public function getPurchaseDetailArrayIdOnly($purchase_id){
			$query = "SELECT ID FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getPurchaseDetailArrayItemOnly($purchase_id){
			$query = "SELECT ITEM_ID FROM purchase_return_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ITEM_ID'];
				}
			}
			return $returnArray;
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `purchase_return` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function get_item_qty_by_bill($purchase_id,$item_id){
			$query = "SELECT purchase_return_details.STOCK_QTY FROM purchase_return_details
					  INNER JOIN purchase_return ON purchase_return_details.PURCHASE_ID = purchase_return.ID
					  WHERE purchase_return_details.ITEM_ID = '$item_id' AND purchase_return.PURCHASE_ID = '$purchase_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['STOCK_QTY'];
			}else{
				return 0;
			}
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM purchase_return WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
	}
?>
