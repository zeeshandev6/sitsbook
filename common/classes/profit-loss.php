<?php
	class ProfitLossAccount{
		public function getProfitAndLossVoucherId($fromDate,$toDate){
			$query = "SELECT voucher.*,voucher_details.* FROM voucher 
					  INNER JOIN voucher_details ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID
					  WHERE voucher_details.ACCOUNT_CODE = '0501020001' 
					  AND voucher.VOUCHER_DATE >= DATE('".$fromDate."') 
					  AND voucher.VOUCHER_DATE <= DATE('".$toDate."') 
					  AND voucher.VOUCHER_STATUS = 'P' ";
			$record = mysql_query($query);
			return $record;
		}
		
		public function getProfitAndLossRevenue($voucher_id){
			$query = "SELECT * FROM voucher_details WHERE VOUCHER_ID = $voucher_id AND TRANSACTION_TYPE = 'Dr' AND ACCOUNT_CODE != '0501020001'";
			$record = mysql_query($query);
			return $record;
		}
		
		public function getProfitAndLossExpense($voucher_id){
			$query = "SELECT * FROM voucher_details WHERE VOUCHER_ID = $voucher_id AND TRANSACTION_TYPE = 'Cr' AND ACCOUNT_CODE != '0501020001'";
			$record = mysql_query($query);
			return $record;
		}
		
		public function getOutwardProcessedLots($toDate){
			$query = "SELECT SUM(outward_details.EMB_AMOUNT) as TOTAL_AMOUNT FROM outward_details  
					  INNER JOIN outward ON outward.OUTWD_ID = outward_details.OUTWD_ID 
					  WHERE outward.BILL_NO = 0 AND outward.LOT_STATUS = 'C' 
					  AND DATE(outward.LOT_DATE) <= DATE('".$toDate."')";
					  
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['TOTAL_AMOUNT'];
			}else{
				return NULL;
			}
		}
		
		public function getOutwardDetailsAmountByLotDate($toDate){
			$query = "SELECT SUM(outward_details.EMB_AMOUNT) as TOTAL_AMOUNT FROM outward_details  
					  INNER JOIN outward ON outward.OUTWD_ID = outward_details.OUTWD_ID 
					  WHERE DATE(outward_details.LOT_DATE) <= DATE('".$toDate."')";
					  
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['TOTAL_AMOUNT'];
			}else{
				return NULL;
			}
		}
	}
?>