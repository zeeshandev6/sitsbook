<?php
    class StockTransfer{
        public $entry_date;
        public $notes;

        //search-params
        public $from_date;
        public $to_date;
        public $found_records;
        
        public function getList(){
            $query = "SELECT * FROM `stock_transfer` ";
            return mysql_query($query);
        }
        public function getDetails($id){
            $query = "SELECT * FROM stock_transfer WHERE ID = '$id' ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }else{
                return NULL;
            }
        }
        public function save(){
            $query = "INSERT INTO `stock_transfer`(`ENTRY_DATE`, 
                                             `NOTES`) 
                                     VALUES ('$this->entry_date',
                                             '$this->notes')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `stock_transfer` SET `ENTRY_DATE`  = '$this->entry_date',
                                                  `NOTES`       = '$this->notes'  WHERE ID = '$id' ";
            return mysql_query($query);
        }

        public function search($start,$per_page){
            $query = "SELECT SQL_CALC_FOUND_ROWS * FROM stock_transfer ";
            $andFlag = false;
            
            if($this->from_date!=""){
                $this->from_date = date('Y-m-d',strtotime($this->from_date));
                $query .= ($andFlag)?"":" WHERE ";
                $query .= ($andFlag)?" AND ":"";
                $query .= " `ENTRY_DATE` >= '".$this->from_date."'" ;
                $andFlag = true;
            }
            if($this->to_date!=""){
                $this->to_date = date('Y-m-d',strtotime($this->to_date));
                $query .= ($andFlag)?"":" WHERE ";
                $query .= ($andFlag)?" AND ":"";
                $query .= " `ENTRY_DATE` <= '".$this->to_date."'";
                $andFlag = true;
            }
            $query .= " ORDER BY ID DESC ";
            $query .= " LIMIT $start,$per_page ";
            $result =  mysql_query($query);

            $totalRecords        = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
            $this->found_records = $totalRecords['total'];

            return $result;
        }
        public function delete($id){
            $query = "DELETE FROM `stock_transfer` WHERE ID = '$id' ";
            return mysql_query($query);
        }
    }
?>