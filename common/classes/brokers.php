<?php

class Brokers{

    public $account_code;
    public $account_title;
    public $first_name;
    public $last_name;
    public $broker_type;
    public $broker_locale;
    public $mobiles;
    public $phones;
    public $email;
    public $country;
    public $city;
    public $address;
    public $postcode;
    public $totalMatchRecords;

    public function getList(){
        $query = "SELECT * FROM brokers ORDER BY ACCOUNT_TITLE";
        return mysql_query($query);
    }

    public function getListByType($type){
        $query = "SELECT * FROM brokers WHERE BROKER_TYPE = '$type' ";
        return mysql_query($query);
    }
    public function getListByLocale($type){
        if($type == ''){
            return $this->getList();
        }else{
            $query = "SELECT * FROM brokers WHERE BROKER_LOCALE = '$type' ";
            return mysql_query($query);
        }
    }

    public function save(){
        $query = "INSERT INTO brokers(  ACCOUNT_CODE,           ACCOUNT_TITLE,
                                        FIRST_NAME,             LAST_NAME,              BROKER_TYPE,
                                        BROKER_LOCALE,
                                        MOBILES,                PHONES,                 EMAILS,
                                        COUNTRY,                CITY,                   ADDRESS,
                                        POSTCODE
                                        )
                                VALUES( '$this->account_code',  '$this->account_title',
                                        '$this->first_name',    '$this->last_name',     '$this->broker_type',
                                        '$this->broker_locale',
                                        '$this->mobiles',       '$this->phones',        '$this->email',
                                        '$this->country',       '$this->city',          '$this->address',
                                        '$this->postcode'
                                       )";
        mysql_query($query);
        return mysql_insert_id();
    }

    public function update($id){
        $query = "UPDATE `brokers` SET  `ACCOUNT_TITLE` = '$this->account_title',
                                        `FIRST_NAME`    = '$this->first_name',
                                        `LAST_NAME`     = '$this->last_name',
                                        `BROKER_TYPE`   = '$this->broker_type',
                                        `BROKER_LOCALE` = '$this->broker_locale',
                                        `MOBILES`       = '$this->mobiles',
                                        `PHONES`        = '$this->phones',
                                        `EMAILS`        = '$this->email',
                                        `COUNTRY`       = '$this->country',
                                        `CITY`          = '$this->city',
                                        `ADDRESS`       = '$this->address',
                                        `POSTCODE`      = '$this->postcode' WHERE `ID` = '$id'";
        return mysql_query($query);
    }
    public function delete($id){
        $query = "DELETE FROM brokers WHERE ID = '".$id."'";
        return mysql_query($query);
    }
    public function getRecordDetailsByID($id){
        $query = "SELECT * FROM brokers WHERE ID ='$id'";
        $records = mysql_query($query);
        if(mysql_num_rows($records)){
            return mysql_fetch_assoc($records);
        }
    }
    public function getRecordDetailsByCode($code){
        $query = "SELECT * FROM brokers WHERE ACCOUNT_CODE ='$code'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array;
        }
    }
    public function getTitleByCode($code){
        $query = "SELECT ACCOUNT_TITLE FROM brokers WHERE ACCOUNT_CODE ='$code'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array['ACCOUNT_TITLE'];
        }
    }
    public function getTitle($id){
        $query = "SELECT ACCOUNT_TITLE FROM brokers WHERE ID ='$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array['ACCOUNT_TITLE'];
        }else{
            return '';
        }
    }

    public function searchBrokers($start,$pageLimit){
        $query = "SELECT SQL_CALC_FOUND_ROWS    brokers.*  FROM brokers ";
        $andFlag = false;

        if(($this->first_name)!=""){
        	$query .= ($andFlag)?"  ":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " brokers.FIRST_NAME LIKE '%".$this->first_name."%'";
            $andFlag = true;
        }

        if(($this->last_name)!=""){
        	$query .= ($andFlag)?"  ":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " brokers.LAST_NAME LIKE '%".$this->last_name."%'";
            $andFlag = true;
        }
        if(($this->phones)!=""){
        	$query .= ($andFlag)?"  ":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " brokers.PHONES LIKE '%".$this->phones."%'";
            $andFlag = true;
        }
        if(($this->mobiles)!=""){
        	$query .= ($andFlag)?"  ":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " brokers.MOBILES LIKE '%".$this->mobiles."%'";
            $andFlag = true;
        }
        if(($this->city)!=""){
        	$query .= ($andFlag)?"  ":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " brokers.CITY LIKE '%".$this->city."%'";
            $andFlag = true;
        }


        $query .= " LIMIT $start,$pageLimit";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];

        return $result;
    }
}
?>
