<?php
	class SaleDetails{
		public $sale_id;
		public $service_id;
		public $item_id;
		public $item_description;
		public $cartons;
		public $per_carton;
		public $qtyReceipt;
		public $unitPrice;
		public $costPrice;
		public $saleDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $batch_no;
		public $expiry_date;
		public $inStock;
		public $barcode_id;
		public $returned;

		public function getList($sale_id){
			$query = "SELECT * FROM sale_details WHERE SALE_ID = $sale_id ";
			return mysql_query($query);
		}
		public function getListMergeByCategory($sale_id){
			$query = "SELECT sale_details.*,
											item_category.NAME AS CATEGORY_NAME,
											items.ITEM_CATG_ID,
											SUM(sale_details.QUANTITY) AS QTY,
											SUM(sale_details.CARTONS) AS CARTON,
											SUM(sale_details.TOTAL_AMOUNT) AS AMNT  FROM sale_details
								LEFT JOIN items ON sale_details.ITEM_ID = items.ID
								LEFT JOIN item_category ON item_category.ITEM_CATG_ID = items.ITEM_CATG_ID WHERE sale_details.SALE_ID = $sale_id ";
			$query .= " GROUP BY items.ITEM_CATG_ID ";
			$result= mysql_query($query);
			echo mysql_error();
			return $result;
		}
		public function getItemHistory($item_id,$account_code,$limit){
			$query = "SELECT sale.SALE_DATE,sale_details.UNIT_PRICE,sale_details.SALE_DISCOUNT
								FROM sale
								INNER JOIN sale_details ON sale.ID   = sale_details.SALE_ID
								WHERE sale_details.ITEM_ID = '$item_id' ";
			if($account_code!=''){
				$query .= " AND sale.CUST_ACC_CODE = '$account_code' ";
			}
			$query .= " ORDER BY sale.SALE_DATE DESC LIMIT $limit ";
			$result= mysql_query($query);
			$rows  = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_assoc($result)){
					$row['SALE_DATE'] = date('d-m-Y',strtotime($row['SALE_DATE']));
					$rows[]  = $row;
				}
				return $rows;
			}
			return NULL;
		}
		public function getAverageCostByItemCategory($item_category_id,$sale_id){
			$query  = "SELECT COUNT(SALE_ID) AS NUMOROWS,SUM(UNIT_PRICE) AS PRICE_SUM FROM sale_details WHERE SALE_ID = '$sale_id' AND  ITEM_ID IN (SELECT ID FROM items WHERE ITEM_CATG_ID = '$item_category_id') ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				$til = ($row['NUMOROWS']==0)?0:$row['PRICE_SUM']/$row['NUMOROWS'];
				return $til;
			}
		}
		public function getDetails($invDetailId){
			$query = "SELECT * FROM `sale_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getQty($sale_detail_id){
			$query = "SELECT QUANTITY FROM `sale_details` WHERE `ID` = $sale_detail_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return (int)$row['QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT QUANTITY FROM `inventory_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['QUANTITY']=='')?0:$row['QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getTotalAmount($sale_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM `sale_details` WHERE `SALE_ID` = '$sale_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `sale_details`(`SALE_ID`,
																					 `ITEM_ID`,
								                           `SERVICE_ID`,
																					 `ITEM_DESCRIPTION`,
								                           `CARTONS`,
								                           `PER_CARTON`,
																					 `QUANTITY`,
																					 `UNIT_PRICE`,
																					 `COST_PRICE`,
																					 `SALE_DISCOUNT`,
																					 `SUB_AMOUNT`,
																					 `TAX_RATE`,
																					 `TAX_AMOUNT`,
																					 `TOTAL_AMOUNT`,
																					 `BATCH_NO`,
																			 	 	 `EXPIRY_DATE`,
																					 `BARCODE_ID`,
																					 `RETURNED`)
																			 VALUES ($this->sale_id,
																				 	'$this->item_id',
                                          '$this->service_id',
																					'$this->item_description',
                                          '$this->cartons',
                                          '$this->per_carton',
																					'$this->qtyReceipt',
																					'$this->unitPrice',
																					'$this->costPrice',
																					'$this->saleDiscount',
																					'$this->subTotal',
																					'$this->taxRate',
																					'$this->taxAmount',
																					'$this->totalAmount',
																					'$this->batch_no',
																					'$this->expiry_date',
																					'$this->barcode_id',
																					'$this->returned')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($sale_detail_id){
			$query = "UPDATE `sale_details` SET `ITEM_ID`      					= '$this->item_id',
                                          `SERVICE_ID`   					= '$this->service_id',
																					`ITEM_DESCRIPTION`      = '$this->item_description',
                                          `CARTONS`      					= '$this->cartons',
                                          `PER_CARTON`   					= '$this->per_carton',
																					`QUANTITY`     					= '$this->qtyReceipt',
																					`UNIT_PRICE`   					= '$this->unitPrice',
																					`SALE_DISCOUNT`					= '$this->saleDiscount',
																					`SUB_AMOUNT`   					= '$this->subTotal',
																					`TAX_RATE`     					= '$this->taxRate',
																					`TAX_AMOUNT`   					= '$this->taxAmount',
																					`TOTAL_AMOUNT` 					= '$this->totalAmount',
																					`BATCH_NO` 		 					= '$this->batch_no',
																					`EXPIRY_DATE`  					= '$this->expiry_date',
																					`RETURNED`     					= '$this->returned' WHERE ID = '$sale_detail_id' ";
			return mysql_query($query);
        }
		public function getSalesDiscountPerBill($sale_id){
			$query = "SELECT (QUANTITY*UNIT_PRICE*SALE_DISCOUNT)/100 AS DISCOUNT_PER_CENT FROM `sale_details` WHERE SALE_ID = $sale_id";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return ($row['DISCOUNT_PER_CENT'] == '')?0:$row['DISCOUNT_PER_CENT'];
			}else{
				return 0;
			}
		}
		public function getDetailsByBarcodeId($barcode_id){
			$query = "SELECT * FROM `sale` INNER JOIN sale_details ON sale_details.SALE_ID = sale.ID WHERE sale_details.BARCODE_ID = '$barcode_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function delete($sale_detail_id){
			$query = "DELETE FROM sale_details WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($sale_id){
			$query = "DELETE FROM sale_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}
	}
?>
