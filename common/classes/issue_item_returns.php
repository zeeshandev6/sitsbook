<?php
	class IssueItemReturns{
		public $user_id;
		public $issue_date;
		public $po_number;
		public $charged_to;
		public $notes;
		public $found_records;

		public $from_date;
		public $to_date;
		public $shed_id;
		public $machine_id;
		public $item_id;

		public function getList(){
			$query = "SELECT * FROM issue_item_returns ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($issue_id){
			$query = "SELECT * FROM issue_item_returns WHERE ID = '$issue_id' ";
			$record =mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function save(){
			if(isset($_SESSION['classuseid'])){
				$user_id = $_SESSION['classuseid'];
			}else{
				return false;
			}
			$query = "INSERT INTO `issue_item_returns` (`USER_ID`,
																					`ISSUE_DATE`,
																					`PO_NUMBER`,
																					`CHARGED_TO`,
																					`NOTES`)
																			VALUES ('$user_id',
																							'$this->issue_date',
																							'$this->po_number',
																							'$this->charged_to',
																							'$this->notes')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($issue_id){
			$query = "UPDATE `issue_item_returns` SET `ISSUE_DATE`  = '$this->issue_date',
										      							`PO_NUMBER`   = '$this->po_number',
											  								`CHARGED_TO`  = '$this->charged_to',
											  								`NOTES`   		= '$this->notes' WHERE `ID` = '$issue_id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function insertVoucherId($issue_id,$voucherId){
			$query = "UPDATE `issue_item_returns` SET `VOUCHER_ID` = $voucherId WHERE `ID` = '$issue_id' ";
			return mysql_query($query);
		}
		public function getVoucherId($issue_id){
			$query = "SELECT VOUCHER_ID FROM `issue_item_returns` WHERE ID = '$issue_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function delete($id){
			$query = "DELETE FROM `issue_item_returns` WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function getPages($start,$limit){
			$query = "SELECT SQL_CALC_FOUND_ROWS issue_item_returns.* FROM issue_item_returns JOIN issue_item_return_details ON issue_item_return_details.ISSUE_ID = issue_item_returns.ID ";
			$andFlag = false;
			if(trim($this->from_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_returns.ISSUE_DATE >= '".$this->from_date."' ";
				$andFlag = true;
			}
			if(trim($this->to_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_returns.ISSUE_DATE <= '".$this->to_date."' ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_return_details.ITEM_ID = '".$this->item_id."' ";
				$andFlag = true;
			}
			if(trim($this->charged_to) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_returns.CHARGED_TO = '".$this->charged_to."' ";
				$andFlag = true;
			}
			$query .= " GROUP BY issue_item_return_details.ISSUE_ID LIMIT $start,$limit ";
			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];
			return $result;
		}
		public function getReport(){
			$query = "SELECT * FROM issue_item_return_details LEFT JOIN issue_item_returns ON issue_item_return_details.ISSUE_ID = issue_item_returns.ID ";
			$andFlag = false;
			if(trim($this->from_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_returns.ISSUE_DATE >= '".$this->from_date."' ";
				$andFlag = true;
			}
			if(trim($this->to_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_returns.ISSUE_DATE <= '".$this->to_date."' ";
				$andFlag = true;
			}
			if(trim($this->machine_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_return_details.MACHINE_ID = '".$this->machine_id."' ";
				$andFlag = true;
			}
			if(trim($this->shed_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_return_details.MACHINE_ID IN  (SELECT ID FROM machines WHERE SHED_ID = '".$this->shed_id."') ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_return_details.ITEM_ID = '".$this->item_id."' ";
				$andFlag = true;
			}
			$result = mysql_query($query);
			return $result;
		}
	}
?>
