<?php
class ProcessingReport{

    public $user_id;
    public $lot_no;
    public $report_date;
    public $supplier_id;
    public $invoice_no;
    public $comments;

    public $start;
    public $per_page;

    public $user_type;

    public $fromdate;
    public $todate;
    public $pon;
    public $quality;

    public $totalMatchRecords;

    public function getList(){
        $query = "SELECT * FROM processing_report ";
        return mysql_query($query);
    }

    public function getRecordDetailsById($id){
        $query = "SELECT * FROM processing_report WHERE ID = '$id' ";
        $result= mysql_query($query);
        if(mysql_num_rows($result)){
            return mysql_fetch_assoc($result);
        }else{
            return NULL;
        }
    }

    public function save(){
        $query = "INSERT INTO `processing_report`(
                              `LOT_NO`,                `USER_ID`,
                              `REPORT_DATE`,          `SUPPLIER_ID`,
                              `COMMENTS`)
                      VALUES ('$this->lot_no',         '$this->user_id',
                              '$this->report_date',   '$this->supplier_id',
                              '$this->comments'
                              )";
        return mysql_query($query);
    }

    public function update($id){
        $query = "UPDATE `processing_report` SET
                            `LOT_NO`       = '$this->lot_no',
                            `REPORT_DATE`  = '$this->report_date',        `SUPPLIER_ID` = '$this->supplier_id',
                            `COMMENTS`     = '$this->comments'             WHERE ID = '$id'";
        return mysql_query($query);
    }


    public function delete($id){
        $query = "DELETE FROM processing_report WHERE ID = '".$id."'";
        return mysql_query($query);
    }


    public function search(){
        $query = "SELECT SQL_CALC_FOUND_ROWS
                    processing_report.ID as rollingId,
                    processing_report.*

                FROM processing_report ";
        $andFlag = false;

        if(($this->fromdate)!=""){
            $this->fromdate = date('Y-m-d',strtotime($this->fromdate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.REPORT_DATE >= '".$this->fromdate."'";
            $andFlag = true;
        }

        if(($this->todate)!=""){
            $this->todate = date('Y-m-d',strtotime($this->todate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.REPORT_DATE <= '".$this->todate."'";
            $andFlag = true;
        }

        if(($this->supplier_id)!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.SUPPLIER_ID ='".$this->supplier_id."'";
            $andFlag = true;
        }

        if(($this->lot_no)!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.LOT_NO ='".$this->lot_no."'";
            $andFlag = true;
        }

        $query .= " LIMIT $this->start,$this->per_page";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];

        return $result;
    }

    public function report(){
        $query = "SELECT SQL_CALC_FOUND_ROWS
                    processing_report.ID as process_report_id,
                    processing_report.*,
                    processing_report_details.*
                FROM processing_report
                LEFT JOIN processing_report_details ON processing_report_details.PR_ID = processing_report.ID ";
        $andFlag = false;

        if($this->lot_no != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.LOT_NO = '".$this->lot_no."'";
            $andFlag = true;
        }

        if($this->quality != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report_details.QUALITY LIKE '".$this->quality."%'";
            $andFlag = true;
        }
        if($this->supplier_id!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.SUPPLIER_ID ='".$this->supplier_id."'";
            $andFlag = true;
        }

        if($this->fromdate != ""){
            $this->fromdate = date('Y-m-d',strtotime($this->fromdate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.REPORT_DATE >= '".$this->fromdate."'";
            $andFlag = true;
        }

        if($this->todate != ""){
            $this->todate = date('Y-m-d',strtotime($this->todate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= "  processing_report.REPORT_DATE <= '".$this->todate."'";
            $andFlag = true;
        }
        $result = mysql_query($query);
        return $result;
    }
}
?>
