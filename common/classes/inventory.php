<?php
	class inventory{
		public $user_id;
		public $purchaseDate;
		public $billNum;
		public $vendorBillNum;
		public $po_number;
		public $gpNum;
		public $batch_no;
		public $source;
		public $supplierAccCode;
		public $supplierAccTitle;
		public $supplier_name;
		public $inv_notes;
		public $charges;
		public $discount;
		public $total_discount;
		public $discount_type;
		public $income_tax;
		public $item;
		public $subject;

		public $fromDate;
		public $toDate;

		public $with_tax;
		public $unregistered_tax;

		public $limit_start;
		public $limit_end;
		public $cat;
		public $found_records;

		public function getList(){
			$query = "SELECT * FROM purchase WHERE  ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(*) FROM purchase ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM purchase ORDER BY BILL_NO ASC,PURCHASE_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function genBillNumber(){
			$query 		 = "SELECT MAX(BILL_NO) AS S_BILL_NO FROM purchase";
			$record      = mysql_query($query);
			$row 	     = (mysql_num_rows($record))?mysql_fetch_assoc($record):NULL;
			if($row == NULL){
				$last_bill_number = 0;
			}else{
				$last_bill_number = max($row);
			}
			return ++$last_bill_number;
		}
		public function checkBillNumber($billNum,$supplierCode){
			$query = "SELECT BILL_NO FROM purchase WHERE BILL_NO = '$billNum' AND SUPP_ACC_CODE = '$supplierCode'";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM purchase WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($id){
			$query = "SELECT * FROM purchase WHERE ID = '$id'  ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}
		public function getAny($id){
			$query = "SELECT * FROM purchase WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function quantityReceipt($inv_id){
			$query = "SELECT SUM(QTY_RECEIPT) as totalQty FROM purchase_details WHERE ID = $inv_id";
			$totalQty = mysql_fetch_assoc(mysql_query($query));
			return $totalQty['totalQty'];
		}
		public function save(){
			$user_id = $_SESSION['classuseid'];
			$query = "INSERT INTO `purchase`( `USER_ID`,
																			  `PURCHASE_DATE`,
																			  `GP_NO`,
																			  `BATCH_NO`,
																			  `BILL_NO`,
																			  `VENDOR_BILL_NO`,
																			  `PO_NUMBER`,
																			  `SUPP_ACC_CODE`,
																			  `SUPPLIER_NAME`,
																			  `NOTES`,
																			  `CHARGES`,
																			  `DISCOUNT`,
																			  `TOTAL_DISCOUNT`,
																			  `DISCOUNT_TYPE`,
																			  `INCOME_TAX`,
																			  `UNREGISTERED_TAX`,
																			  `SUBJECT`)
																VALUES ('$user_id',
																				'$this->purchaseDate',
																				'$this->gpNum',
																				'$this->batch_no',
																				'$this->billNum',
																				'$this->vendorBillNum',
																				'$this->po_number',
																				'$this->supplierAccCode',
																				'$this->supplier_name',
																				'$this->inv_notes',
																				'$this->charges',
																				'$this->discount',
																				'$this->total_discount',
																				'$this->discount_type',
																				'$this->income_tax',
																				'$this->unregistered_tax',
																				'$this->subject')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($invId){
			$query = "UPDATE `purchase` SET
									`PURCHASE_DATE`	 	= '$this->purchaseDate',
									`GP_NO`	 			= '$this->gpNum',
									`BATCH_NO` 			= '$this->batch_no',
									`BILL_NO`			= '$this->billNum',
									`VENDOR_BILL_NO`	= '$this->vendorBillNum',
									`PO_NUMBER`			= '$this->po_number',
									`SUPP_ACC_CODE`		= '$this->supplierAccCode',
									`SUPPLIER_NAME` 	= '$this->supplier_name',
									`NOTES` 			= '$this->inv_notes',
									`CHARGES` 			= '$this->charges',
									`DISCOUNT` 			= '$this->discount',
									`TOTAL_DISCOUNT`	= '$this->total_discount',
									`DISCOUNT_TYPE` 	= '$this->discount_type',
									`INCOME_TAX`        = '$this->income_tax',
									`UNREGISTERED_TAX`  = '$this->unregistered_tax',
									`SUBJECT` 			= '$this->subject' WHERE `ID` = $invId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getQuantityPerBill($purchase_id){
			$query = "SELECT SUM(STOCK_QTY) AS TOTAL_QUANTITY FROM purchase_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['TOTAL_QUANTITY'] == '')?0:$row['TOTAL_QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSum($purchase_id){
			$query = "SELECT SUM(TOTAL_AMOUNT) AS AMOUNT FROM purchase_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function getInventoryTaxAmountSum($purchase_id){
			$query = "SELECT SUM(TAX_AMOUNT) AS TAX_AMNT FROM purchase_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['TAX_AMNT']=='')?0:$row['TAX_AMNT'];
			}else{
				return 0;
			}
		}
		public function getSuppliersList(){
			$query = "SELECT * FROM `suppliers` ORDER BY `SUPP_ACC_TITLE` ASC";
			return mysql_query($query);
		}
		public function inventorySearch(){
			$query = "SELECT ID FROM purchase WHERE  ";
			$query .= "`SUPP_ACC_CODE` = '".$this->supplierAccCode."'";
			$query .= " AND `BILL_NO` = ".$this->billNum ;
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['ID'];
			}else{
				return 0;
			}
		}
		public function searchInventory(){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM purchase ";
			$andFlag = false;

			if(trim($this->supplierAccCode) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`SUPP_ACC_CODE` ='".$this->supplierAccCode."' ";
				$andFlag = true;
			}
			if($this->billNum!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`BILL_NO` = '$this->billNum' ";
				$andFlag = true;
			}
			if($this->po_number != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`PO_NUMBER` = '$this->po_number' ";
				$andFlag = true;
			}
			if($this->vendorBillNum != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`VENDOR_BILL_NO` = '$this->vendorBillNum' ";
				$andFlag = true;
			}
			if(trim($this->with_tax) == "Y"){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.INCOME_TAX > 0 ";
				$andFlag = true;
			}
			if(trim($this->with_tax) == "N"){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.INCOME_TAX = 0 ";
				$andFlag = true;
			}
			if(trim($this->unregistered_tax)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.UNREGISTERED_TAX = '".$this->unregistered_tax."' ";
				$andFlag = true;
			}
			if($this->user_id != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`USER_ID` = '$this->user_id' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$this->fromDate = date("Y-m-d",strtotime($this->fromDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`PURCHASE_DATE` >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$this->toDate = date("Y-m-d",strtotime($this->toDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " purchase.`PURCHASE_DATE` <= '".$this->toDate."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY purchase.BILL_NO DESC,purchase.PURCHASE_DATE DESC ";
			if($this->limit_start == ''){
				$this->limit_start = 0;
			}
			$query .= " LIMIT $this->limit_start,$this->limit_end";

			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];
        	return $result;
		}
		public function getDailyPurchases($report_date){
			$query = "SELECT purchase.*,purchase_details.*
					  FROM purchase INNER JOIN purchase_details
					  ON purchase.ID = purchase_details.PURCHASE_ID
					  WHERE purchase.PURCHASE_DATE = '".$report_date."' GROUP BY purchase_details.PURCHASE_ID ORDER BY purchase.ID ASC";
			return mysql_query($query);
		}
		public function purchaseReport($report_type){
			if($report_type == 'P'){
				$main_table = 'purchase';
				$sub_table = 'purchase_details';
			}else{
				$main_table = 'purchase_return';
				$sub_table = 'purchase_return_details';
			}
			$query = "SELECT ".$main_table.".*,".$sub_table.".* FROM ".$main_table."
					  INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".PURCHASE_ID ";
			$andFlag = false;

      if($this->user_id != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".USER_ID = ".$this->user_id;
				$andFlag = true;
			}

			if(trim($this->fromDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".PURCHASE_DATE >= DATE('".$this->fromDate."')";
				$andFlag = true;
			}
			if(trim($this->toDate) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".PURCHASE_DATE <= DATE('".$this->toDate."')";
				$andFlag = true;
			}
			if(trim($this->supplierAccCode)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SUPP_ACC_CODE ='".$this->supplierAccCode."'";
				$andFlag = true;
			}
			if($report_type == 'P'){
				if(trim($this->with_tax) == "Y"){
					$query .= ($andFlag)?"":" WHERE ";
					$query .= ($andFlag)?" AND ":"";
					$query .= " ".$main_table.".INCOME_TAX > 0 ";
					$andFlag = true;
				}
				if(trim($this->with_tax) == "N"){
					$query .= ($andFlag)?"":" WHERE ";
					$query .= ($andFlag)?" AND ":"";
					$query .= " ".$main_table.".INCOME_TAX = 0 ";
					$andFlag = true;
				}
				if(trim($this->unregistered_tax)!=""){
					$query .= ($andFlag)?"":" WHERE ";
					$query .= ($andFlag)?" AND ":"";
					$query .= " ".$main_table.".UNREGISTERED_TAX = '".$this->unregistered_tax."' ";
					$andFlag = true;
				}
			}
			if(trim($this->item) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID = '".$this->item."'";
				$andFlag = true;
			}elseif(trim($this->cat) != ""){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".ITEM_ID IN ( SELECT ID FROM items WHERE ITEM_CATG_ID = '$this->cat' ) ";
				$andFlag = true;
            }
			if($this->billNum != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".BILL_NO = ".$this->billNum;
				$andFlag = true;
			}
			$query .= " ORDER BY ".$main_table.".PURCHASE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}
		public function purchaseTaxItemsList($report_date){
			$query = " SELECT ITEM_ID,SUM(STOCK_QTY) AS TOTAL_QTY, (SUM(UNIT_PRICE)/SUM(STOCK_QTY)) AS AVG_COST FROM purchase_details WHERE PURCHASE_ID IN (SELECT ID FROM purchase WHERE UNREGISTERED_TAX != 'Y' AND PURCHASE_DATE <= '".$report_date."') GROUP BY ITEM_ID ";
			$result= mysql_query($query);
			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM purchase WHERE ID = $id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteUnRelatedInventoryEntries(){
			$query = "DELETE FROM purchase WHERE ID NOT IN (SELECT ID FROM inventory_details) AND VOUCHER_ID = 0";
			mysql_query($query);
		}
		public function getPurchaseDetailArrayIdOnly($purchase_id){
			$query = "SELECT ID FROM purchase_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_assoc($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getPurchaseDetailArrayItemOnly($purchase_id){
			$query = "SELECT ITEM_ID FROM purchase_details WHERE PURCHASE_ID = $purchase_id";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_assoc($record)){
					$returnArray[] = $row['ITEM_ID'];
				}
			}
			return $returnArray;
		}
		public function getDateByBillAndCode($bill_num,$supplier_code){
			$query = "SELECT PURCHASE_DATE FROM `purchase` WHERE BILL_NO = '$bill_num' AND SUPP_ACC_CODE = '$supplier_code'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return ($row['PURCHASE_DATE'] == '')?"":date('d-m-Y',strtotime($row['PURCHASE_DATE']));
			}else{
				return '';
			}
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `purchase` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function setSmsStatus($inventory_id,$sms_status){
			$query = "UPDATE `purchase` SET `SMS_STATUS` = '$sms_status' WHERE `ID` =  $inventory_id ";
			return mysql_query($query);
		}
		public function get_item_qty_by_bill($purchase_id,$item_id){
			$query = "SELECT purchase_details.STOCK_QTY FROM purchase_details
					  INNER JOIN purchase ON purchase_details.PURCHASE_ID = purchase.ID WHERE ITEM_ID = '$item_id' AND purchase_details.PURCHASE_ID = '$purchase_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['STOCK_QTY'];
			}else{
				return 0;
			}
		}
		public function check_in_purchase_returns($purchase_id){
			$query = "SELECT * FROM purchase_return WHERE PURCHASE_ID = '$purchase_id' ";
			$result= mysql_query($query);
			return mysql_num_rows($result);
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM purchase WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}

		public function getSupplierAmountFromVoucher($inventory_id){
			$query = " SELECT AMOUNT FROM voucher_details WHERE SUBSTR(ACCOUNT_CODE,1,6) = '040101' AND VOUCHER_ID = (SELECT VOUCHER_ID FROM purchase WHERE ID = $inventory_id) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
	}
?>
