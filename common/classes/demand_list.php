<?php
    class DemandList{
        public $po_number;
        public $supplier_acc_code;
        public $entry_date;
        public $required_date;

        //search params
        public $from_date;
        public $to_date;
        public $item_id;
        //po_number
        //supplier_acc_code

        public $found_records;

        public function getList(){
            $query = " SELECT * FROM demand_list ";
            return mysql_query($query);
        }
        public function getDetail($id){
            $query  = "SELECT * FROM demand_list WHERE ID = '$id' ";
            $result = mysql_query($query);
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }
        }
        public function save(){
            $query = "INSERT INTO `demand_list`(`PO_NUMBER`,
                                                `SUPPLIER_ACC_CODE`,
                                                `ENTRY_DATE`,
                                                `REQUIRED_DATE`)
                                        VALUES ('$this->po_number',
                                                '$this->supplier_acc_code',
                                                '$this->entry_date',
                                                '$this->required_date')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `demand_list` SET `PO_NUMBER`             = '$this->po_number',
                                               `SUPPLIER_ACC_CODE`     = '$this->supplier_acc_code',
                                               `ENTRY_DATE`            = '$this->entry_date',
                                               `REQUIRED_DATE`         = '$this->required_date' WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function setStatus($dl_id,$status){
            $query = "UPDATE `demand_list` SET ISSUE_STATUS = '$status' WHERE ID  = '$dl_id'  ";
            return mysql_query($query);
        }
        public function delete($id){
            $query = "DELETE FROM `demand_list` WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function demand_report(){
            $query   = " SELECT demand_list.*,demand_list_detail.*,suppliers.SUPP_ACC_TITLE AS SUPPLIER_ACC_TITLE, items.NAME AS ITEM_TITLE  FROM demand_list 
                         JOIN demand_list_detail ON demand_list_detail.DL_ID = demand_list.ID  
                         JOIN suppliers ON suppliers.SUPP_ACC_CODE           = demand_list.SUPPLIER_ACC_CODE
                         JOIN items     ON items.ID                          = demand_list_detail.ITEM_ID ";
            $andFlag = false;
            if($this->po_number != ''){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.PO_NUMBER = '".$this->po_number."' " ;
				$andFlag = true;
            }
            if($this->supplier_acc_code != ''){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.SUPPLIER_ACC_CODE = '".$this->supplier_acc_code."' " ;
				$andFlag = true;
            }
            if($this->item_id != ''){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list_detail.ITEM_ID = '".$this->item_id."' " ;
				$andFlag = true;
            }
            $query .= " GROUP BY demand_list.PO_NUMBER,demand_list_detail.ITEM_ID ";
            return mysql_query($query);
        }
        public function search($start,$per_page){
			$query   = " SELECT SQL_CALC_FOUND_ROWS * FROM demand_list  ";
			$andFlag =   false;

            if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.ENTRY_DATE >= '".$this->from_date."'" ;
				$andFlag = true;
			}

            if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.ENTRY_DATE >= '".$this->from_date."'" ;
				$andFlag = true;
			}

            if($this->po_number != ''){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.PO_NUMBER >= '".$this->po_number."'" ;
				$andFlag = true;
            }
            if($this->supplier_acc_code != ''){
                $query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.SUPPLIER_ACC_CODE = '".$this->supplier_acc_code."'" ;
				$andFlag = true;
            }
			if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.ENTRY_DATE >= '".$this->from_date."'" ;
				$andFlag = true;
			}
			if($this->to_date!=""){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " demand_list.ENTRY_DATE <= '".$this->to_date."'";
				$andFlag = true;
			}
			$query 			    .= " ORDER BY demand_list.ENTRY_DATE DESC ";
			$query 			    .= " LIMIT $start,$per_page ";
			$result 			 = mysql_query($query);
			$totalRecords 		 = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        	$this->found_records = $totalRecords['total'];
			return $result;
		}
    }
?>
