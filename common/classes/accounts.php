<?php
	class ChartOfAccounts{
		public $account_code;
		public $account_title;
		public $level;

		public $new_account_code;

		public function addAccount(){
			$query = "";
		}

		public function getList(){
			$query = "SELECT * FROM account_code ORDER BY ACC_CODE";
			return mysql_query($query);
		}
		public function getGeneralList(){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) = 2  ORDER BY ACC_CODE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getActiveList(){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) = 10 ORDER BY ACC_TITLE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getMainTitle($accCode){
			$query = "SELECT ACC_TITLE FROM account_code WHERE ACC_CODE = SUBSTR('".$accCode."',1,2)";
			$record = mysql_query($query);
			if(mysql_query($query)){
				$row = mysql_fetch_array($record);
				return $row['ACC_TITLE'];
			}else{
				return "";
			}
		}
		public function getPriorityList(){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) = 10 ORDER BY PRIORITY DESC";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
        public function getLevelTwoListByGeneral($firstLevel){
			$query = "SELECT ACC_CODE FROM account_code WHERE SUBSTR(ACC_CODE,1,2) = '$firstLevel' AND LENGTH(ACC_CODE) = 4";
			$records = mysql_query($query);
			return $records;
		}
		public function getLevelFourByGeneral($general){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) > 6 AND ACC_CODE LIKE '".$general."%' ORDER BY ACC_TITLE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getLevelFourList(){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) = 10 ORDER BY ACC_TITLE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getLevelFourExpenseList(){
			$query = "SELECT * FROM account_code WHERE LENGTH(ACC_CODE) = 10 AND SUBSTR(ACC_CODE,1,2) = '03' ORDER BY ACC_CODE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function checkLevelActive($accCode){
			$query = "SELECT ACTIVE FROM account_code WHERE ACC_CODE = SUBSTR('$accCode',1,6) AND LENGTH(ACC_CODE) = 6";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['ACTIVE'] == '')?"N":$row['ACTIVE'];
			}else{
				return 'N';
			}
		}
		public function checkInherited($code){
			if(strlen($code)<7){
				$a = strlen($code);
				$select = "SELECT ACC_CODE FROM account_code WHERE LENGTH(ACC_CODE) > ".$a." AND ACC_CODE LIKE '".$code."%'";
				return mysql_num_rows(mysql_query($select));
			}else{
				return false;
			}
		}
		public function getAccountTitleByCode($accCode){
			$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '$accCode'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ACC_TITLE'];
			}else{
				return '';
			}
		}
		public function getLevelThreeList($secondLevel){
			$query = "SELECT ACC_CODE FROM account_code WHERE SUBSTR(ACC_CODE,1,4) = '$secondLevel' AND LENGTH(ACC_CODE) = 6";
			$records = mysql_query($query);
			return $records;
		}
		public function getLevelThreeListByGeneral($firstLevel){
			$query = "SELECT ACC_CODE,ACC_TITLE FROM account_code WHERE SUBSTR(ACC_CODE,1,2) = '$firstLevel' AND LENGTH(ACC_CODE) = 6";
			$records = mysql_query($query);
			return $records;
		}
		public function getLevelThreeListByMain($secondLevel){
			$query = "SELECT ACC_CODE FROM account_code WHERE SUBSTR(ACC_CODE,1,4) = '$secondLevel' AND LENGTH(ACC_CODE) = 6";
			$records = mysql_query($query);
			return $records;
		}
		public function getAccountByCatAccCode($sixDigitCode){
			$query = "SELECT * FROM account_code WHERE SUBSTR(ACC_CODE,1,6) = '$sixDigitCode' AND LENGTH(ACC_CODE) = 10 ORDER BY ACC_TITLE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getAccountsByGeneral($general){
			$query = "SELECT * FROM account_code WHERE ACC_CODE LIKE '".$general."%' ORDER BY ACC_TITLE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function getAccountCodeById($lastInsertId){
			$query = "SELECT `ACC_CODE_ID`,`ACC_CODE`,`ACC_TITLE` FROM `account_code` WHERE `ACC_CODE_ID` = $lastInsertId";
			$accountDetail = mysql_query($query);
			return $accountDetail;
		}
		public function creatCustomer($accountCode,$accountTitle){
			$query = "INSERT INTO `customers`(`CUST_ACC_CODE`, `CUST_ACC_TITLE`) VALUES ('$accountCode','$accountTitle') ";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function deleteCustomer($accountCode){
			$query = "DELETE FROM `customers` WHERE `CUST_ACC_CODE` = '$accountCode'";
			return mysql_query($query);
		}
		public function creatSupplier($accountCode,$accountTitle){
			$query = "INSERT INTO `suppliers`(`SUPP_ACC_CODE`, `SUPP_ACC_TITLE`) VALUES ('$accountCode','$accountTitle') ";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function deleteSupplier($accountCode){
			$query = "DELETE FROM `suppliers` WHERE `SUPP_ACC_CODE` = '$accountCode'";
			return mysql_query($query);
		}
		public function creatItem($accountCode,$accountTitle){
			$query = "INSERT INTO `items`(`ITEM_ACC_CODE`, `ITEM_ACC_TITLE`,`ACTIVE`) VALUES ('$accountCode','$accountTitle','Y') ";
			return mysql_query($query);
		}
		public function genMachineNumber(){
			$query = "SELECT MACHINE_NO FROM machines WHERE MACHINE_NO != 0 AND MACHINE_NO != '0' AND MACHINE_NO != '' ORDER BY MACHINE_ID DESC LIMIT 1";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				$machineNum = $row['MACHINE_NO']+1;
				return $machineNum;
			}else{
				return 1;
			}

		}
		public function creatMachine($accountCode,$accountTitle){
			$newMachineNumber = $this->genMachineNumber();
			$query = "INSERT INTO `machines`(`MACHINE_NO`,`MACHINE_ACC_CODE`, `MACHINE_TITLE`,`ACTIVE`) VALUES ('$newMachineNumber' ,'$accountCode','$accountTitle','Y') ";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function deleteMachine($accountCode,$accountTitle){
			$query = "DELETE FROM `machines` MACHINE_ACC_CODE = '$accountCode'";
			return mysql_query($query);
		}
		public function deleteItem($accountCode){
			$query = "DELETE FROM `items` WHERE `ITEM_ACC_CODE` = '$accountCode'";
			return mysql_query($query);
		}
		public function creatBank($accountCode,$accountTitle){
			$query = "INSERT INTO `banks`(`BANK_ACC_CODE`, `BANK_ACC_TITLE`) VALUES ('$accountCode','$accountTitle') ";
			return mysql_query($query);
		}
		public function deleteBank($accountCode){
			$query = "DELETE FROM `banks` WHERE `ITEM_ACC_CODE` = '$accountCode'";
			return mysql_query($query);
		}
		public function getRecordDetails($code){
			$query = "SELECT `ACC_TITLE` FROM account_code WHERE `ACC_CODE` =$code";
			$record = mysql_query($query);
			return $record;
		}
		public function getDetail($id){
			$query = "SELECT * FROM account_code WHERE `ACC_CODE_ID` = '$id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function addGeneral($title){
			 $select = "SELECT MAX(ACC_CODE) FROM account_code WHERE LENGTH(ACC_CODE) = 2";
			 $record = mysql_fetch_array(mysql_query($select));
			 $last_general = $record['MAX(ACC_CODE)'];
			 if($last_general==""){

				$new_general_code = '01'; //$new_general_code
			 }else{
			 	$last_general_one = substr($last_general,0,1);
			 	$last_general_two = substr($last_general,1,1);
				 if($last_general_two !== 10){
					 $last_general_two++;
				 }else{
					 $last_general_one = substr($last_general_one,0,1);
					 $last_general_one++;
					 $last_general_two++;
					 $last_general_two = substr($last_general_two,1,1);
				 }
				 $new_general_code = $last_general_one.$last_general_two;//$new_general_code
			 }
			 $query = "INSERT INTO `account_code`(`ACC_CODE`, `ACC_TITLE`)
							              VALUES ('".$new_general_code."','".$title."')";
			 $inserted = mysql_query($query);
			 $this->new_account_code = $new_general_code;
			 return $inserted;
		}
		public function addMain($title,$general){
			 $select = "SELECT MAX(ACC_CODE) FROM account_code WHERE LENGTH(ACC_CODE) = 4 AND SUBSTR(ACC_CODE,1,2) LIKE '%$general%'";
			 $record = mysql_fetch_array(mysql_query($select));
			 $last_main = $record['MAX(ACC_CODE)'];
			 if($last_main==""){
				$new_main_code = $general.'01'; //$new_main_code
			 }else{
			 	$last_main_one = substr($last_main,0,2);
				$last_main_two = substr($last_main,2,1);
				$last_main_three = substr($last_main,3,1);
				if($last_main_three == 10){
					$last_main_one = substr($last_main,0,2);
					$last_main_two = substr($last_main,2,2);
					$last_main_two++;
					$last_main_two = substr($last_main_two,0,1).'0';
					$new_main_code = $last_main_one.$last_main_two; //$new_main_code
				}else{
					$last_main_three++;
					$new_main_code = $last_main_one.$last_main_two.$last_main_three; //$new_main_code
				}
			 }
			 $query = "INSERT INTO `account_code`(`ACC_CODE`, `ACC_TITLE`)
							              VALUES ('".$new_main_code."','".$title."')";
			 $inserted = mysql_query($query);
			 $this->new_account_code = $new_main_code;
			 return $inserted;
		}
		public function addSubMain($title,$main){
			 $select = "SELECT MAX(ACC_CODE) FROM account_code WHERE LENGTH(ACC_CODE) = 6 AND SUBSTR(ACC_CODE,1,4) LIKE '%$main%'";
			 $record = mysql_fetch_array(mysql_query($select));
			 $last_subMain = $record['MAX(ACC_CODE)'];
			 if($last_subMain==""){
				 $new_subMain_code = $main.'01';  //$new_subMain_code
			 }else{
				 if(substr($last_subMain,5,1) == 10){
					 $submain_fifth = substr($last_subMain,4,1);
					 $submain_sixth = substr($last_subMain,5,1);
					 $submain_sixth++;
					 $submain_sixth = substr($submain_sixth,1,1);
					 $submain_fifth++;
					 $new_subMain_code = $main.$submain_fifth.$submain_sixth;//$new_subMain_code
				 }else{
					 $submain_sixth = substr($last_subMain,5,1);
					 $submain_five = substr($last_subMain,0,5);
					 $submain_sixth++;
					 $new_subMain_code = $submain_five.$submain_sixth;//$new_subMain_code
				 }
			 }
			 $query = "INSERT INTO `account_code`(`ACC_CODE`, `ACC_TITLE`)
							              VALUES ('".$new_subMain_code."','".$title."')";
			 $inserted = mysql_query($query);
			 $this->new_account_code = $new_subMain_code;
			 return $inserted;
		}
		    public function addSubMainChild($title, $submain) {
			$new_subMainChild = '';
			$select = "SELECT MAX(ACC_CODE) FROM account_code WHERE LENGTH(ACC_CODE) = 10 AND SUBSTR(ACC_CODE,1,6) LIKE '%$submain%'";
			$record = mysql_fetch_array(mysql_query($select));
			$last_subMainChild = $record['MAX(ACC_CODE)'];
			if ($last_subMainChild == "") {
			    $new_subMainChild = $submain . '0001';
			} else {
			    $new_subMainChild = (int)substr($last_subMainChild, 6, 4);
			    $new_subMainChild++;
			    while(strlen($new_subMainChild)!=4){
			      $new_subMainChild = '0'.$new_subMainChild;
			    }
			}
			$new_subMainChild = $submain.$new_subMainChild;
			$this->account_code = $new_subMainChild;
			$query = "INSERT INTO `account_code`(`ACC_CODE`, `ACC_TITLE`) VALUES ('" . $new_subMainChild . "','" . $title . "')";
			$insertion = mysql_query($query);
			$this->new_account_code = $new_subMainChild;
			return mysql_insert_id();
		    }

		public function accountCheckInBooks($accCode){
			$query = "SELECT * FROM `voucher_details` WHERE `ACCOUNT_CODE` = '$accCode'";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function accountStatus($accCode,$status){
			if(strlen($accCode) == 6){
				$query = "UPDATE account_code SET `ACTIVE` = '$status' WHERE SUBSTR(ACC_CODE,1,6) = '$accCode'";
				mysql_query($query);
				return mysql_affected_rows();
			}elseif(strlen($accCode) > 6){
				//$query = "UPDATE account_code SET `ACTIVE` = '$status' WHERE ACC_CODE = '$accCode' ";
			}
		}
		public function updateTitle($acc_id,$acc_title){
			$query = "UPDATE account_code SET `ACC_TITLE` = '$acc_title' WHERE ACC_CODE_ID = '$acc_id'";
			return mysql_query($query);
		}
		public function updateTitleByCode($acc_code,$acc_title){
			$query = "UPDATE account_code SET `ACC_TITLE` = '$acc_title' WHERE ACC_CODE = '$acc_code'";
			return mysql_query($query);

		}
		public function increasePriority($accountCode){
			$query = "UPDATE account_code SET PRIORITY = PRIORITY+1 WHERE ACC_CODE = '$accountCode'";
			mysql_query($query);
		}
		public function get_array_of_account_codes($third_level){
			$return = array();
			if(mysql_num_rows($third_level)){
				while($revenue = mysql_fetch_array($third_level)){
					 $return[] = $revenue['ACC_CODE'];
				}
			}
			return $return;
		}
		public function deleteAccCode($acc_code){
			$query = "DELETE FROM account_code WHERE ACC_CODE = '$acc_code' ";
			return mysql_query($query);
		}
	}
?>
