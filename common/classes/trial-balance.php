<?php
	class TrialBalance{
		public function getVoucherListByDate($fromDate,$toDate){
			$query = "SELECT `VOUCHER_ID` FROM `voucher` WHERE ";
			$andFlag = false;
			if($fromDate!==''){
				//$query .= ($andFlag)?" AND ":"";
				
			}
		}
		public function getThirdLevelAccounts(){
			$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) = 6";
			return mysql_query($query);
		}
		public function getFourthLevelAccounts(){
			$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) > 6 ORDER BY ACC_CODE";
			return mysql_query($query);
		}
		public function getChildAccountsOf($thirdLevelAccCode){
			$parentAccLength = strlen($thirdLevelAccCode);
			$parentAccCode   = $thirdLevelAccCode;
			$query = "SELECT * FROM `account_code` WHERE SUBSTR(ACC_CODE,1,$parentAccLength) = '$thirdLevelAccCode' 
													 AND LENGTH(ACC_CODE) > $parentAccLength";
			return mysql_query($query);
		}
		public function getLastBalanceList($thirdLevelCode){
			$query = "SELECT BALANCE AS LAST_BALANCE FROM voucher_details 
			   		  WHERE LENGTH(ACCOUNT_CODE) = 9 AND SUBSTR(ACCOUNT_CODE,1,6) = '$thirdLevelCode' 
					  ORDER BY VOUCH_DETAIL_ID DESC LIMIT 1";
			$records = mysql_query($query);
			return $records;
		}
		public function getAccountTitle($AccCode){
			$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '$AccCode'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['ACC_TITLE'];
			}else{
				return '';
			}
		}
		public function getLedgerOpeningBalance($endDate,$accCode){
			$query = "SELECT BALANCE 
					  FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance['BALANCE'] = round($cashBalance['BALANCE']*100)/100;
				 $cashBalance = $cashBalance['BALANCE'];
			}else{
				 $cashBalance = 0;
			}
			return $cashBalance;
		}
		
		public function getTrialOpeningBalance($endDate,$accCode){
			$query = "SELECT BALANCE 
					  FROM voucher_details WHERE VOUCHER_DATE < DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
		public function getAccountDebitsSum($accCode,$fromDate,$toDate){
			$query = "SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM voucher_details 
					  WHERE ACCOUNT_CODE = '".$accCode."'
					  AND VOUCHER_DATE >= DATE('".$fromDate."') 
					  AND VOUCHER_DATE <= DATE('".$toDate."') 
					  AND TRANSACTION_TYPE = 'Dr'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_AMOUNT']=='')?0:$row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}
		
		public function getAccountCreditsSum($accCode,$fromDate,$toDate){
			$query = "SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM voucher_details 
					  WHERE ACCOUNT_CODE = '".$accCode."'
					  AND VOUCHER_DATE >= DATE('".$fromDate."') 
					  AND VOUCHER_DATE <= DATE('".$toDate."') 
					  AND TRANSACTION_TYPE = 'Cr'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_AMOUNT']=='')?0:$row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}
		
		/* BALANCE SHEET*/
		
		public function getStockTakingAmount($toDate){
			$firstDayNextMonth = date('01-m-Y',strtotime($toDate+"1 month"));
			$endOfThisMonth  = date('Y-m-d',strtotime($firstDayNextMonth."-1 day"));
			$query = "SELECT AMOUNT FROM shed_stock WHERE DATE(STOCK_TAKING_DATE) = DATE('".$endOfThisMonth."')";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
		
	}
?>