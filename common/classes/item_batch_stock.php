<?php
    class ItemBatchStock{
        public $item_id;
        public $batch_no;
        public $expiry_date;
        public $stock;
        public $active_status;

        public $from_date;
        public $to_date;

        public $found_records;

        public function getList(){
          $query = "SELECT * FROM item_batch_stock ORDER BY ITEM_ID,BATCH_NO,EXPIRY_DATE ";
          return mysql_query($query);
        }
        public function record_exists($item_id,$batch_no,$expiry_date){
          $query = "SELECT ID FROM item_batch_stock WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE = '$expiry_date' ";
          return mysql_num_rows(mysql_query($query));
        }
        public function save(){
          $query = "INSERT INTO `item_batch_stock`( `ITEM_ID`,
                                                    `BATCH_NO`,
                                                    `EXPIRY_DATE`,
                                                    `STOCK`,
                                                    `ACTIVE_STATUS`)
                                            VALUES( '$this->item_id',
                                                    '$this->batch_no',
                                                    '$this->expiry_date',
                                                    '$this->stock',
                                                    '$this->active_status')";
          mysql_query($query);
          return mysql_insert_id();
        }
        public function setBatchStatus($record_id,$status){
          $query = "UPDATE item_batch_stock SET ACTIVE_STATUS = '$status' WHERE ID = $record_id ";
          return mysql_query($query);
        }
        public function addStock($item_id,$batch_no,$expiry_date,$stock){
          if($batch_no==''){
            return;
          }
          $exists = $this->record_exists($item_id,$batch_no,$expiry_date);
          if($exists==0){
              $this->item_id       = $item_id;
              $this->batch_no      = $batch_no;
              $this->expiry_date   = $expiry_date;
              $this->stock         = 0;
              $this->active_status = 'N';
              $exists              = $this->save();
          }
          if($exists){
              $query  = "UPDATE `item_batch_stock` SET `STOCK` = STOCK + $stock WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE = '$expiry_date' ";
              $exists = mysql_query($query);
          }else{
              $exists = false;
          }
          return $exists;
        }
        public function removeStock($item_id,$batch_no,$expiry_date,$stock){
          if($batch_no==''){
            return;
          }
          $exists = $this->record_exists($item_id,$batch_no,$expiry_date);
          if($exists==0){
              $this->item_id     = $item_id;
              $this->batch_no    = $batch_no;
              $this->expiry_date = $expiry_date;
              $this->stock       = 0;
              $exists            = $this->save();
          }
          if($exists){
              $query  = "UPDATE `item_batch_stock` SET `STOCK` = STOCK - $stock WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE = '$expiry_date' ";
              $exists = mysql_query($query);
          }else{
              $exists = false;
          }
          return $exists;
        }
        public function getItemBatchRemainingStock($item_id,$batch_no,$expiry_date){
          $query = "SELECT SUM(STOCK) AS TOTAL_STOCK FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE > '$expiry_date' ";
          $result= mysql_query($query);
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              return (float)$row['TOTAL_STOCK'];
          }else{
              return 0;
          }
        }
        public function getItemBatchDetail($item_id,$batch_no,$expiry_date){
          $query = "SELECT * FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE > '$expiry_date' ";
          $result= mysql_query($query);
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              return $row;
          }else{
              return NULL;
          }
        }
        public function getItemBatchCurrentStock($item_id,$batch_no,$expiry_date){
          $query = "SELECT SUM(STOCK) AS TOTAL_STOCK FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE = '$expiry_date' ";
          $result= mysql_query($query);
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              return (float)$row['TOTAL_STOCK'];
          }else{
              return 0;
          }
        }
        public function getActiveBatchNumber($item_id,$expiry_date){
          $query = "SELECT * FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' AND EXPIRY_DATE > '$expiry_date' AND ACTIVE_STATUS = 'Y' AND STOCK > 0 ORDER BY EXPIRY_DATE ASC,BATCH_NO ASC LIMIT 1 ";
          $result= mysql_query($query);
          if(mysql_num_rows($result)==0){
            $query = str_replace(" AND ACTIVE_STATUS = 'Y' ","",$query);
            $result= mysql_query($query);
          }
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              if($row['STOCK']==0){
                $query = str_replace(" AND ACTIVE_STATUS = 'Y' ","",$query);
                $result= mysql_query($query);
                if(mysql_num_rows($result)){
                  $row = mysql_fetch_assoc($result);
                }
              }
              return $row;
            }else{
              return NULL;
          }
        }
        public function getItemBatchStock($item_id,$batch_no,$expiry_date){
          $query = "SELECT SUM(STOCK) AS TOTAL_STOCK FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' AND BATCH_NO = '$batch_no' AND EXPIRY_DATE = '$expiry_date' ";
          $result= mysql_query($query);
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              return (float)$row['TOTAL_STOCK'];
          }else{
              return 0;
          }
        }
        public function getItemStock($item_id){
          $query  = "SELECT SUM(STOCK) AS STOCK FROM `item_batch_stock` WHERE ITEM_ID = '$item_id' ";
          $result = mysql_query($query);
          if(mysql_num_rows($result)){
              $row = mysql_fetch_assoc($result);
              return (float)$row['TOTAL_STOCK'];
          }else{
              return 0;
          }
        }
        public function search(){
    			$query   = "SELECT SQL_CALC_FOUND_ROWS * FROM item_batch_stock ";
    			$andFlag = false;

    			if($this->from_date!=""){
    				$this->from_date = date('Y-m-d',strtotime($this->from_date));
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `EXPIRY_DATE` >= '".$this->from_date."'" ;
    				$andFlag = true;
    			}
    			if($this->to_date!=""){
    				$this->to_date = date('Y-m-d',strtotime($this->to_date));
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `EXPIRY_DATE` <= '".$this->to_date."'";
    				$andFlag = true;
    			}
          if($this->item_id!=""){
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `ITEM_ID` = '".$this->item_id."'";
    				$andFlag = true;
    			}
          if($this->batch_no!=""){
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `BATCH_NO` = '".$this->batch_no."'";
    				$andFlag = true;
    			}
          if($this->stock != ""){
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `STOCK` != '".$this->stock."'";
    				$andFlag = true;
    			}
          if($this->active_status!=""){
    				$query .= ($andFlag)?"":" WHERE ";
    				$query .= ($andFlag)?" AND ":"";
    				$query .= " `ACTIVE_STATUS` = '".$this->active_status."'";
    				$andFlag = true;
    			}

    			$query .= " ORDER BY ITEM_ID,EXPIRY_DATE ASC ";
    			$result =  mysql_query($query);

    			$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
    			$this->found_records = $totalRecords['total'];

    			return $result;
    		}
        public function delete($id){
          $query = "DELETE FROM `item_batch_stock` WHERE ID = '$id' ";
          return mysql_query($query);
        }
    }
?>
