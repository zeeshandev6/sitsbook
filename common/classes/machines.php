<?php
class Machines{
    public $title;
    public $shed_id;
    public $machine_no;
    public $rate;
    public $machine_rpm;
    public $description;

    public function getList(){
        $query = "SELECT * FROM machines ORDER BY NAME ";
        return mysql_query($query);
    }
    public function getMachineListByShed($shed_id){
        $query = "SELECT * FROM machines WHERE SHED_ID = '$shed_id' ";
        return mysql_query($query);
    }
    public function save(){
        $query = "INSERT INTO machines(  NAME,
                                         SHED_ID,
                                         MACHINE_NO,
                                         RATE,
                                         MACHINE_RPM,
                                         DESCRIPTION)
                                VALUES( '$this->title',
                                        '$this->shed_id',
                                        '$this->machine_no',
                                        '$this->rate',
                                        '$this->machine_rpm',
                                        '$this->description' )";
        mysql_query($query);
        return mysql_insert_id();
    }
    public function update($id){
        $query = "UPDATE `machines` SET `NAME`         = '$this->title',
                                        `SHED_ID`      = '$this->shed_id',
                                        `MACHINE_NO`   = '$this->machine_no',
                                        `RATE`         = '$this->rate',
                                        `MACHINE_RPM`  = '$this->machine_rpm',
                                        `DESCRIPTION`  = '$this->description' WHERE `ID` = '$id'";
        return mysql_query($query);
    }
    public function delete($id){
        $query = "DELETE FROM machines WHERE ID = '$id' ";
        return mysql_query($query);
    }
    public function countMachineByShed($shed_id){
        $query = "SELECT COUNT(NAME) AS TOTAL_MACHINES FROM `machines` WHERE SHED_ID = '$shed_id' ";
        $result= mysql_query($query);
        if(mysql_num_rows($result)){
            $result = mysql_fetch_assoc($result);
            return $result['TOTAL_MACHINES'];
        }
    }
    public function getShedAccCode($machine_id){
        $query = "SELECT SHED_ACC_CODE FROM `shed` WHERE SHED_ID IN (SELECT SHED_ID FROM `machines` WHERE ID = '$machine_id' ) ";
        $result= mysql_query($query);
        if(mysql_num_rows($result)){
            $result = mysql_fetch_assoc($result);
            return $result['SHED_ACC_CODE'];
        }
    }
    public function getRecordDetailsValue($id,$column){
        $query = "SELECT $column FROM machines WHERE ID ='$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array[$column];
        }
    }
    public function getRecordDetailsByID($id){
        $query = "SELECT * FROM machines WHERE ID ='$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array;
        }
    }
	  public function getName($id){
        $query = "SELECT NAME FROM machines WHERE ID = '$id' ";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array['NAME'];
        }else{
        	return '';
        }
    }
    public function search($start,$pageLimit){
        $query = "SELECT SQL_CALC_FOUND_ROWS machines.* FROM machines ";
        if( ($this->title)!="" || ($this->status)!=""){
            $query .= " WHERE ";
        }
        $andFlag = false;
        if(($this->title)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " machines.NAME LIKE '%".$this->title."%'";
            $andFlag = true;
        }
        $query .= " LIMIT $start, $pageLimit";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];
        return $result;
    }
}
?>
