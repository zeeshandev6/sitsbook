<?php
	class Sheds{
		public $shed_acc_code;
		public $shed_title;
		public $location;
		public $description;
		public $email;
		public $phones;

		public function getList(){
			$query = "SELECT * FROM shed ORDER BY SHED_TITLE";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM shed ORDER BY SHED_TITLE LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(SHED_ID) FROM shed";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM shed WHERE SHED_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getAccCode($id){
			$query = "SELECT SHED_ACC_CODE FROM shed WHERE SHED_ID = $id";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['SHED_ACC_CODE'];
			}else{
				return NULL;
			}
		}
		public function getImage($id){
			$query = "SELECT `UPLOAD_IMAGE` FROM shed WHERE SHED_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getTitle($id){
			$query = "SELECT `SHED_TITLE` FROM shed WHERE SHED_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getTitleStr($id){
			$query = "SELECT `SHED_TITLE` FROM shed WHERE SHED_ID ='$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['SHED_TITLE'];
			}else{
				return '';
			}
		}
		public function getShedTitleById($id){
			$query = "SELECT `SHED_TITLE` FROM shed WHERE SHED_ID ='$id'";
			$records = mysql_fetch_array(mysql_query($query));
			return $records['SHED_TITLE'];
		}
		public function save(){
			$query = "INSERT INTO `shed`(`SHED_ACC_CODE`,
			 							 `SHED_TITLE`,
			 							 `LOCATION`,
			 							 `DESCRIPTION`,
			 							 `EMAIL`,
			 							 `PHONES`)
								  VALUES ('$this->shed_acc_code',
										  '$this->shed_title',
										  '$this->location',
										  '$this->description',
										  '$this->email',
										  '$this->phones')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `shed` SET `SHED_ACC_CODE` = '$this->shed_acc_code',
										`SHED_TITLE`    = '$this->shed_title',
										`LOCATION`      = '$this->location',
										`DESCRIPTION`   = '$this->description',
										`EMAIL`    		= '$this->email',
										`PHONES` 	    = '$this->phones' WHERE SHED_ID = '$id'";
			return mysql_query($query);
		}
		public function search(){
			$query = "SELECT * FROM shed WHERE ";
			if($this->shedName!==""){
				$query .= "`SHED_TITLE` like '%".$this->shedName."%' ";
			}
			if($this->shedContactNum!==""){
				$query .= ( $this->shedEmail!=="" || $this->shedName!=="" )?" AND ":"";
				$query .= "`PHONES` LIKE '%".$this->shedContactNum."%' ";
			}
			$records = mysql_query($query);
			return $records;
		}
		public function delete($id){
			$query = "DELETE FROM shed WHERE SHED_ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}

		public function get_shed_id_by_code($accountCode){
			$query = "SELECT SHED_ID FROM shed WHERE SHED_ACC_CODE = '$accountCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['SHED_ID'];
			}else{
				return '';
			}
		}

		public function deleteShedAccCode($accountCode){
			$queryImg = "SELECT UPLOAD_IMAGE AS IMAGE FROM shed WHERE SHED_ACC_CODE = '$accountCode'";
			$imageRecord = mysql_query($queryImg);
			$image = (mysql_num_rows($imageRecord))?mysql_fetch_array($imageRecord):NULL;
			if($image['IMAGE'] != '' && file_exists('../uploads/sheds/'.$image['IMAGE'])){
				unlink('../uploads/sheds/'.$image['IMAGE']);
			}
			$query = "DELETE FROM `shed` WHERE  SHED_ACC_CODE = '$accountCode'";
			return mysql_query($query);
		}
		public function status($id,$status){
			$query = "UPDATE shed SET ACTIVE = '$status' WHERE SHED_ID ='$id'";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
