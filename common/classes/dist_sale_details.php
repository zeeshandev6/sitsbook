<?php
	class DistributorSaleDetails{
		public $sale_id;
		public $service_id;
		public $item_id;
		public $cartons;
		public $carton_price;
		public $rate_carton;
		public $dozens;
		public $dozen_price;
		public $qtyReceipt;
		public $unitPrice;
		public $costPrice;
		public $saleDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $inStock;
		public $barcode_id;

		public function getList($sale_id){
			$query = "SELECT *FROM dist_sale_details WHERE SALE_ID = $sale_id ";
			return mysql_query($query);
		}
		public function getListByPoNumber($po_number){
			$query = "SELECT dist_sale_details.ITEM_ID,items.QTY_PER_CARTON,
							 SUM(dist_sale_details.QUANTITY) AS TOTAL_QTY,
							 SUM(dist_sale_details.CARTONS) AS TOTAL_CARTONS
							 FROM dist_sale_details
							 JOIN items ON items.ID = dist_sale_details.ITEM_ID
							 WHERE dist_sale_details.SALE_ID IN  (SELECT ID FROM dist_sale WHERE PO_NUMBER = '$po_number') GROUP BY ITEM_ID ";
			return mysql_query($query);
		}
		public function getListMergeByCategory($sale_id){
			$query = "SELECT dist_sale_details.*,
											item_category.NAME AS CATEGORY_NAME,
											items.ITEM_CATG_ID,
											SUM(dist_sale_details.QUANTITY) AS QTY,
											SUM(dist_sale_details.CARTONS) AS CARTON,
											SUM(dist_sale_details.TOTAL_AMOUNT) AS AMNT  FROM dist_sale_details
								LEFT JOIN items ON dist_sale_details.ITEM_ID = items.ID
								LEFT JOIN item_category ON item_category.ITEM_CATG_ID = items.ITEM_CATG_ID WHERE dist_sale_details.SALE_ID = $sale_id ";
			$query .= " GROUP BY items.ITEM_CATG_ID ";
			$result= mysql_query($query);
			return $result;
		}
		public function getAverageCostByItemCategory($item_category_id,$sale_id){
			$query  = "SELECT COUNT(SALE_ID) AS NUMOROWS,SUM(UNIT_PRICE) AS PRICE_SUM FROM dist_sale_details WHERE SALE_ID = '$sale_id' AND  ITEM_ID IN (SELECT ID FROM items WHERE ITEM_CATG_ID = '$item_category_id') ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				$til = $row['PRICE_SUM']/$row['NUMOROWS'];
				return $til;
			}
		}
		public function getDetails($invDetailId){
			$query = "SELECT * FROM `dist_sale_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getQty($sale_detail_id){
			$query = "SELECT QUANTITY FROM `dist_sale_details` WHERE `ID` = $sale_detail_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return (int)$row['QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT QUANTITY FROM `inventory_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['QUANTITY']=='')?0:$row['QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getSaleDetailsByItemsPerDate($report_date,$order_taker,$user_id,$item_id){
			$query = "SELECT SUM(CARTONS) AS TOTAL_CARTONS,SUM(QUANTITY) AS TOTAL_QTY,RATE_CARTON,UNIT_PRICE FROM dist_sale_details WHERE ITEM_ID = '$item_id' AND SALE_ID IN (SELECT ID FROM dist_sale WHERE SALE_DATE = '$report_date' ";
			if($order_taker != ''){
				$query .= " AND ORDER_TAKER = '$order_taker' ";
			}
			if($user_id!=''){
				$query .= " AND USER_ID = '$user_id' ";
			}
			$query .= " ) ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			return NULL;
		}
		public function getAllQuantity($item_id){
			$query = "SELECT SUM(UNIT_PRICE) AS PRICE,SUM(COST_PRICE) AS COST,SUM(QUANTITY) AS ALL_QTY FROM `dist_sale_details` WHERE `ITEM_ID` = '$item_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `dist_sale_details`(`SALE_ID`,
													  `ITEM_ID`,
													  `SERVICE_ID`,
													  `CARTONS`,
													  `RATE_CARTON`,
													  `DOZENS`,
													  `DOZEN_PRICE`,
													  `QUANTITY`,
													  `UNIT_PRICE`,
													  `COST_PRICE`,
													  `SALE_DISCOUNT`,
													  `SUB_AMOUNT`,
													  `TAX_RATE`,
													  `TAX_AMOUNT`,
													  `TOTAL_AMOUNT`,
													  `BARCODE_ID`)
											  VALUES ('$this->sale_id',
											  		  '$this->item_id',
                                          			  '$this->service_id',
                                          			  '$this->cartons',
                                          			  '$this->rate_carton',
													  '$this->dozens',
													  '$this->dozen_price',
													  '$this->qtyReceipt',
													  '$this->unitPrice',
													  '$this->costPrice',
													  '$this->saleDiscount',
													  '$this->subTotal',
													  '$this->taxRate',
													  '$this->taxAmount',
													  '$this->totalAmount',
													  '$this->barcode_id')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($sale_detail_id){
			$query = "UPDATE `dist_sale_details` SET  `ITEM_ID`      = '$this->item_id',
			                                          `SERVICE_ID`   = '$this->service_id',
			                                          `CARTONS`      = '$this->cartons',
			                                          `RATE_CARTON`  = '$this->rate_carton',
													  `DOZENS` 		 = '$this->dozens',
													  `DOZEN_PRICE`  = '$this->dozen_price',
													  `QUANTITY`     = '$this->qtyReceipt',
													  `UNIT_PRICE`   = '$this->unitPrice',
													  `SALE_DISCOUNT`= '$this->saleDiscount',
													  `SUB_AMOUNT`   = '$this->subTotal',
													  `TAX_RATE`     = '$this->taxRate',
													  `TAX_AMOUNT`   = '$this->taxAmount',
													  `TOTAL_AMOUNT` = '$this->totalAmount' WHERE ID = $sale_detail_id";
			$result = mysql_query($query);
			return $result;
    }
		public function getSalesDiscountPerBill($sale_id){
			$query = "SELECT (QUANTITY*UNIT_PRICE*SALE_DISCOUNT)/100 AS DISCOUNT_PER_CENT FROM `dist_sale_details` WHERE SALE_ID = $sale_id";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return ($row['DISCOUNT_PER_CENT'] == '')?0:$row['DISCOUNT_PER_CENT'];
			}else{
				return 0;
			}
		}
		public function getDetailsByBarcodeId($barcode_id){
			$query = "SELECT * FROM `sale` INNER JOIN dist_sale_details ON dist_sale_details.SALE_ID = sale.ID WHERE dist_sale_details.BARCODE_ID = '$barcode_id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function delete($sale_detail_id){
			$query = "DELETE FROM dist_sale_details WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($sale_id){
			$query = "DELETE FROM dist_sale_details WHERE SALE_ID = $sale_id";
			return mysql_query($query);
		}
	}
?>
