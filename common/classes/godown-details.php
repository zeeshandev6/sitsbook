<?php
	class GodownDetails{
		public $godown_id;
		public $item_id;
		public $quantity;

		public function getList($godown_id){
			$query = "SELECT * FROM `godown_details` WHERE GODOWN_ID = '$godown_id' ORDER BY TITLE";
			return mysql_query($query);
		}
		public function getDetails($item_id,$godown_id){
			$query  = "SELECT * FROM `godown_details` WHERE ITEM_ID = '$item_id' AND GODOWN_ID = '$godown_id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `godown_details`(`GODOWN_ID`, 
										   `ITEM_ID`, 
										   `QUANTITY`) 
							       VALUES ('$this->godown_id',
							       		   '$this->item_id',
							       		   '$this->quantity')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `godown_details` SET `ITEM_ID`= '$this->item_id' ,
										  `QUANTITY`= '$this->quantity'  WHERE ID = '$id";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `godown_details` WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function addStock($item,$godown,$quantity){
			$query = "UPDATE `godown_details` SET QUANTITY = QUANTITY + $quantity WHERE GODOWN_ID = $godown AND ITEM_ID = $item";
			return mysql_query($query);
		}
		public function removeStock($item,$godown,$quantity){
			$query = "UPDATE `godown_details` SET QUANTITY = QUANTITY - $quantity WHERE GODOWN_ID = $godown AND ITEM_ID = $item";
			return mysql_query($query);
		}
		public function getGodownStock($godown_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_STOCK FROM godown_details WHERE GODOWN_ID = '$godown_id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return ($row['TOTAL_STOCK']=='')?0:$row['TOTAL_STOCK'];
			}else{
				return 0;
			}
		}
		public function getItemStockInGodown($godown_id,$item_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_STOCK FROM godown_details WHERE GODOWN_ID = '$godown_id' AND ITEM_ID = '$item_id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return ($row['TOTAL_STOCK']=='')?0:$row['TOTAL_STOCK'];
			}else{
				return 0;
			}
		}
		public function getStockSum($item_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_STOCK FROM godown_details WHERE ITEM_ID = '$item_id'";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return ($row['TOTAL_STOCK']=='')?0:$row['TOTAL_STOCK'];
			}else{
				return 0;
			}
		}
		public function getItemStockAllGodownArray($item_id){
			$query = "SELECT QUANTITY FROM `godown_details` WHERE ITEM_ID = '$item_id' ";
			$result = mysql_query($query);
			$return = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_assoc($result)){
					if($row['QUANTITY'] == 0){
						continue;
					}
					$return[] = $row['QUANTITY'];
				}
			}
			return $return;
		}
	}
?>