<?php
	class EmbProducts{
		public $title;
		public $code;
		public $description;

		public $totalMatchRecords;

		public function getList(){
			$query = "SELECT * FROM emb_products ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetails($id){
			$query = "SELECT * FROM emb_products WHERE ID ='$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				return mysql_fetch_assoc($records);
			}
		}
		public function checkCode($prodCode){
			$query = "SELECT `PROD_CODE` FROM `emb_products` WHERE `PROD_CODE` = $prodCode";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return false;
			}else{
				return true;
			}
		}
		public function countRows(){
			$query = "SELECT count(TITLE) FROM emb_products";
			$records = mysql_query($query);
			return $records;
		}
		public function newProductCode(){
			$query = "SELECT MAX(PROD_CODE) as PCODE FROM `emb_products`";
			$code = mysql_fetch_array(mysql_query($query));
			$lastCode = ++$code['PCODE'];
			if(strlen($lastCode)==1){
				$lastCode = "00".$lastCode;
			}elseif(strlen($lastCode)==2){
				$lastCode = "0".$lastCode;
			}
			return $lastCode;
		}
		public function save(){
			$query = "INSERT INTO `emb_products`(`TITLE`, `PROD_CODE`, `DESCRIPTION`)
			VALUES('$this->title','$this->code','$this->description')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `emb_products` SET
												`PROD_CODE`	  = '$this->code',
												`TITLE`		  = '$this->title',
												`DESCRIPTION` = '$this->description' WHERE ID ='$id'";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getTitle($id){
			$query 	 = "SELECT TITLE FROM emb_products WHERE ID = '$id' ";
			$result  = mysql_query($query);
			$records = mysql_fetch_array($result);
			return $records['TITLE'];
		}
		public function delete($id){
			$query 	 = "DELETE FROM emb_products WHERE ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}

		public function search($start,$pageLimit){
			$query = "SELECT SQL_CALC_FOUND_ROWS emb_products.* FROM emb_products ";
			$andFlag = false;
			if($this->title!=""){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_products.TITLE LIKE '%".$this->title."%'";
				$andFlag = true;
			}
			$query .= " LIMIT $start, $pageLimit";
			$result = mysql_query($query);

			//getting total numer or reords that matched against search
			$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->totalMatchRecords = $totalRecords['total'];
			return $result;
		}
	}
?>
