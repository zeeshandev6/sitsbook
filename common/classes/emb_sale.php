<?php
	class EmbroiderySale{
		public $user_id;
		public $company_id;
		public $sale_date;
		public $gp_no;
		public $bill_no;
		public $account_code;
		public $customer_name;
		public $bill_amount;
		public $charges;
		public $total_amount;
		public $lot_no;
		public $notes;
		public $subject;
		public $voucher_id;
		public $record_time;
		public $payment_confirm;

		public $from_date;
		public $to_date;
		public $design_no;

		public function getDetail($sale_id){
			$query = "SELECT * FROM emb_sale WHERE ID = '$sale_id' ";
			$record =mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function countRows(){
			$query = "SELECT count(ID) FROM sale ";
			$records = mysql_query($query);
			return $records;
		}
		public function save(){
			$query = "INSERT INTO `emb_sale`(`USER_ID`,
																			 `COMPANY_ID`,
																			 `SALE_DATE`,
																			 `GP_NO`,
																			 `BILL_NO`,
																			 `CUST_ACC_CODE`,
																			 `CUSTOMER_NAME`,
																			 `BILL_AMOUNT`,
																			 `CHARGES`,
																			 `TOTAL_AMOUNT`,
																			 `LOT_NO`,
																			 `NOTES`,
																			 `SUBJECT`,
																			 `VOUCHER_ID`,
																			 `RECORD_TIME`,
																			 `PAYMENT_CONFIRM`)
														   VALUES ('$this->user_id',
																			 '$this->company_id',
																			 '$this->sale_date',
																			 '$this->gp_no',
																			 '$this->bill_no',
																			 '$this->account_code',
																			 '$this->customer_name',
																			 '$this->bill_amount',
																			 '$this->charges',
																			 '$this->total_amount',
																			 '$this->lot_no',
																			 '$this->notes',
																			 '$this->subject',
																			 '$this->voucher_id',
																			 '$this->record_time',
																			 '$this->payment_confirm') ";
			mysql_query($query);
			return mysql_insert_id();
		}

		public function update($id){
			$query = "UPDATE `emb_sale` SET `COMPANY_ID`  		= '$this->company_id',
																			`SALE_DATE`				= '$this->sale_date',
																			`GP_NO`						= '$this->gp_no',
																			`BILL_NO`					= '$this->bill_no',
																			`CUST_ACC_CODE`		= '$this->account_code',
																			`CUSTOMER_NAME`		= '$this->customer_name',
																			`BILL_AMOUNT`			= '$this->bill_amount',
																			`CHARGES`					= '$this->charges',
																			`TOTAL_AMOUNT`		= '$this->total_amount',
																			`LOT_NO`					= '$this->lot_no',
																			`NOTES`						= '$this->notes'  ,
																			`SUBJECT`					= '$this->subject'  ,
																			`RECORD_TIME`			= '$this->record_time'   WHERE ID = '$id' ";
		  return mysql_query($query);
		}
		public function insertCompanyId($sale_id,$company_id){
			$query = "UPDATE `emb_sale` SET `COMPANY_ID` = $company_id WHERE `ID` = $sale_id ";
			return mysql_query($query);
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `emb_sale` SET `VOUCHER_ID` = '$voucherId' WHERE `ID` = '$invId' ";
			return mysql_query($query);
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM emb_sale WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return (int)$row['VOUCHER_ID'];
			}
		}
		public function genBillNumber(){
			$row = NULL;$row_scan = NULL;
			$last_bill_number 		= 1;

			$query 		 	 = "SELECT MAX(BILL_NO) AS S_BILL_NO FROM emb_sale ";
			$query_scan  = "SELECT MAX(BILL_NO) AS SS_BILL_NO FROM mobile_sale";
			$record      = mysql_query($query);
			$record_scan = mysql_query($query_scan);
			$row 	       = (mysql_num_rows($record))?mysql_fetch_assoc($record):NULL;
			$row_scan    = (mysql_num_rows($record_scan))?mysql_fetch_assoc($record_scan):NULL;

			if($row != NULL || $row_scan != NULL){
				$records     			= array_merge($row,$row_scan);
				$last_bill_number = max($records);
			}

			return ++$last_bill_number;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM emb_sale ";
			$andFlag = false;

			if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SALE_DATE` >= '".$this->from_date."'" ;
				$andFlag = true;
			}
			if($this->to_date!=""){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `SALE_DATE` <= '".$this->to_date."'";
				$andFlag = true;
			}
			if(trim($this->account_code)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CUST_ACC_CODE` = '".$this->account_code."' ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = '$this->bill_no' " ;
				$andFlag = true;
			}
			if($this->user_id != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `USER_ID` = '$this->user_id' " ;
				$andFlag = true;
			}
			$query  .= " ORDER BY BILL_NO DESC ";
			$query  .= " LIMIT $start,$per_page ";
			$result  =  mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function saleReport(){
			$main_table = 'emb_sale';
			$sub_table  = 'emb_sale_details';
			$query = "SELECT ".$main_table.".*,".$sub_table.".* FROM ".$main_table." INNER JOIN ".$sub_table." ON ".$main_table.".ID = ".$sub_table.".SALE_ID ";
			$andFlag = false;

			if(trim($this->from_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE >= DATE('".$this->from_date."')";
				$andFlag = true;
			}
			if(trim($this->to_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".SALE_DATE <= DATE('".$this->to_date."')";
				$andFlag = true;
			}
			if(trim($this->account_code)!==""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".CUST_ACC_CODE ='".$this->account_code."'";
				$andFlag = true;
			}
			if(trim($this->machine_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".MACHINE_ID = '".$this->machine_id."'";
				$andFlag = true;
			}
			if(trim($this->gp_no) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".GP_NO = '".$this->gp_no."'";
				$andFlag = true;
			}
			if(trim($this->lot_no) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".LOT_NO = '".$this->lot_no."'";
				$andFlag = true;
			}
			if(trim($this->bill_no) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$main_table.".BILL_NO = '".$this->bill_no."'";
				$andFlag = true;
			}
			if(trim($this->design_no) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ".$sub_table.".DESIGN_NO = '".$this->design_no."'";
				$andFlag = true;
			}
			$query .= " ORDER BY ".$main_table.".SALE_DATE ASC, ".$main_table.".BILL_NO ASC ";
			return  mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM emb_sale WHERE ID = $id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
