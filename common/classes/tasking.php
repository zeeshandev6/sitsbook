<?php

class Tasking{

    public $to_user_id;
	public $from_user_id;
    public $task_date;
    public $stages;
    public $subject;
    public $description;
    public $user_id;
    public $totalMatchRecords;

    public function getList(){
        $query = "SELECT * FROM tasking";
        return mysql_query($query);
    }

    public function getUserTasks($user_id){
        $query = "SELECT * FROM tasking WHERE TO_USER = '$user_id'";
        return mysql_query($query);
    }

    public function getUserTasksByStatus($user_id,$status){
        $query = "SELECT * FROM tasking WHERE TO_USER = '$user_id' AND TASK_STATUS = '$status'";
        return mysql_query($query);
    }

    public function getDetails($id){
        $query = "SELECT * FROM tasking WHERE ID = '$id'";
        $result= mysql_query($query);
        if(mysql_num_rows($result)){
            $row = mysql_fetch_assoc($result);
            return $row;
        }
    }
    public function getListByIdScid($scid, $stage){
        $query = "SELECT * FROM tasking WHERE SC_ID = '$scid' AND STAGE = '$stage'";
        return mysql_query($query);
    }
    public function getCountByDate($today){
        $query  = "SELECT COUNT(*) AS TOTAL FROM tasking WHERE TASK_DATE = '$today'";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function getCountUnRead(){
        $query  = "SELECT COUNT(*) AS TOTAL FROM tasking WHERE TASK_STATUS = 'N' ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function getListByIdScidStage($accid, $scid, $stage){
        $query = "SELECT * FROM tasking WHERE USER_ID = '$accid' AND SC_ID = '$scid' AND STAGE = '$stage'";
        return mysql_query($query);
    }
    public function save(){
        $query = "INSERT INTO tasking(
                                        FROM_USER, 
                                        TO_USER, 
                                        TASK_DATE, 
                                        SUBJECT, 
                                        DESCRIPTION,
                                        TASK_STATUS
                                       )
                                VALUES( '$this->from_user_id',
                                		'$this->to_user_id', 
                                		'$this->task_date', 
                                        '$this->subject', 
                                        '$this->description',
                                        'P'
                                       )";
		mysql_query($query);
		return mysql_insert_id();
    }
    public function update($id){
        $query = "UPDATE `tasking` SET
                         `TO_USER`      = '$this->to_user_id',
                         `SUBJECT`      = '$this->subject',
                         `DESCRIPTION`  = '$this->description' WHERE ID = '$id'";
        return mysql_query($query);
    }
    public function delete($id){
        $query = "DELETE FROM tasking WHERE ID = '".$id."'";
        return mysql_query($query);
    }
	public function updateTaskingtatus($id,$status){
		$query = "UPDATE `tasking` SET `TASK_STATUS` = '$status' WHERE ID = '$id'";
		return mysql_query($query);
	}
    public function getListById($id){
        $query = "SELECT * FROM tasking WHERE ID = '$id'";
        $result = mysql_query($query);
        return mysql_fetch_assoc($result);
    }
    //searchOld not in use
    public function searchOld($from_date, $todate,  $subject, $start, $end){
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM tasking ";
        if(($from_date)!="" || ($todate)!="" || ($subject)!="" )
        {
            $query .= " WHERE ";
        }
        $andFlag = false;

        if(($from_date)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE >= '".$from_date."'";
            $andFlag = true;
        }

        if(($todate)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE <= '".$todate."'";
            $andFlag = true;
        }

        if(($subject)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " SUBJECT LIKE '%".$subject."%'";
        }

        $query .= " ORDER BY TASK_DATE ASC LIMIT $start,$end";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];
        return $result;
    }
    //search in use
    public function search($from,$to,$from_date, $todate, $subject){
        $query = "SELECT *  FROM tasking ";
        $andFlag = false;
        
        if($from!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " FROM_USER = '".$from."'";
            $andFlag = true;
        }
        if($to!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER = '".$to."'";
            $andFlag = true;
        }
        if($from_date != ""){
            $from_date = date('Y-m-d 00:00:00',strtotime($from_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE >= '".$from_date."'";
            $andFlag = true;
        }
        if($todate != ""){
            $todate = date('Y-m-d 23:59:59',strtotime($todate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE <= '".$todate."'";
            $andFlag = true;
        }

        if($subject!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " SUBJECT LIKE '%".$subject."%'";
        }
        $query .= " ORDER BY TASK_DATE ASC ";
        $result = mysql_query($query);
        
        //getting total numer or reords that matched against search
        //$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        //$this->totalMatchRecords = $totalRecords['total'];
        return $result;
    }

    public function get_counting_todays($user_id,$today){
        $query = "SELECT COUNT(*) AS TOTAL  FROM tasking ";
        $andFlag = false;

        if($user_id!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER = '".$user_id."'";
            $andFlag = true;
        }
        if($today != ""){
            $today = date('Y-m-d',strtotime($today));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE = '".$today."'";
            $andFlag = true;
        }

        $query .= " AND TASK_STATUS = 'N' ORDER BY ID ASC ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function get_message_count_specific($from,$to,$from_date,$to_date,$status){
        $query = "SELECT COUNT(*) AS TOTAL  FROM tasking ";
        $andFlag = false;

        if($from!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " FROM_USER = '".$from."'";
            $andFlag = true;
        }
        if($to != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER = '".$to."'";
            $andFlag = true;
        }
        if($from_date != ""){
            $from_date = date('Y-m-d 00:00:00',strtotime($from_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE >= '".$from_date."'";
            $andFlag = true;
        }
        if($to_date != ""){
            $to_date = date('Y-m-d 23:59:59',strtotime($to_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_DATE <= '".$to_date."'";
            $andFlag = true;
        }
        if($status != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TASK_STATUS = '".$status."' ";
            $andFlag = true;
        }
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return ($row['TOTAL']=='')?"0":$row['TOTAL'];
    }
}
?>