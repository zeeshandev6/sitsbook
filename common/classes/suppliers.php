<?php
	class suppliers
	{
		public $account_title;
		public $account_code;
		public $urdu_title;
		public $contact_person;
		public $email;
		public $mobile;
		public $phones;
		public $city;
		public $address;
		public $tax_reg_no;
		public $ntn_reg_no;
		public $cnic_no;
		public $send_sms;

		public function getList(){
			$query = "SELECT * FROM suppliers ORDER BY SUPP_ACC_TITLE";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM suppliers WHERE SUPP_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getMobileIfActive($accCode){
			$query = "SELECT `MOBILE` FROM `suppliers` WHERE `SUPP_ACC_CODE` ='$accCode' AND SEND_SMS = 'Y' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['MOBILE'];
			}else{
				return "";
			}
		}
		public function getTitle($accCode){
			$query = "SELECT `SUPP_ACC_TITLE` FROM suppliers WHERE `SUPP_ACC_CODE` ='$accCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['SUPP_ACC_TITLE'];
			}else{
				return "";
			}
		}
		public function setTitle($accCode,$title){
			$query = "UPDATE suppliers SET SUPP_ACC_TITLE = '$title' WHERE SUPP_ACC_CODE = '$accCode'";
			return mysql_query($query);
		}

		public function getSupplier($accCode){
			$query = "SELECT * FROM suppliers WHERE `SUPP_ACC_CODE` ='$accCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row;
			}else{
				return NULL;
			}
		}

		public function save(){
			$query = "INSERT INTO `suppliers`(`SUPP_ACC_TITLE`,
											  `SUPP_ACC_CODE`,
												`URDU_TITLE`,
											  `CONT_PERSON`,
											  `EMAIL`,
											  `MOBILE`,
											  `PHONES`,
											  `CITY`,
											  `ADDRESS`,
												`TAX_REG_NO`,
												`NTN_REG_NO`,
												`CNIC_NO`,
												`SEND_SMS`)
									  VALUES ('$this->account_title',
									  		  	'$this->account_code',
														'$this->urdu_title',
													  '$this->contact_person',
													  '$this->email',
													  '$this->mobile',
													  '$this->phones',
													  '$this->city',
													  '$this->address',
														'$this->tax_reg_no',
														'$this->ntn_reg_no',
														'$this->cnic_no',
														'$this->send_sms')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($supp_id){
			$query = "UPDATE `suppliers` SET
					 `SUPP_ACC_TITLE` = '$this->account_title',
					 `URDU_TITLE`	  	= '$this->urdu_title',
					 `CONT_PERSON`	  = '$this->contact_person',
					 `EMAIL`				  = '$this->email',
					 `MOBILE`				  = '$this->mobile',
					 `PHONES`				  = '$this->phones',
					 `CITY`					  = '$this->city',
					 `ADDRESS`			  = '$this->address',
					 `TAX_REG_NO`		  = '$this->tax_reg_no',
					 `NTN_REG_NO`		  = '$this->ntn_reg_no',
					 `CNIC_NO`		 	  = '$this->cnic_no',
					 `SEND_SMS`       = '$this->send_sms' WHERE SUPP_ID = '$supp_id'  ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function search($term){
			$query = "select * from suppliers where `SUPP_ACC_TITLE` LIKE '%".$term."%'";
			$records = mysql_query($query);
			return $records;
		}
		public function supplier_report(){
			$query = "SELECT * FROM SUPPLIERS WHERE `SUPP_ACC_TITLE` LIKE '%".$term."%' ";
			$records = mysql_query($query);
			return $records;
		}
		public function getSupplierBalance($suppAccCode){
			$query = "SELECT `BALANCE` FROM `voucher_details` WHERE `ACCOUNT_CODE` = '$suppAccCode' ORDER BY `VOUCH_DETAIL_ID` DESC";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$data =  mysql_fetch_array($result);
				$balance = $data['BALANCE'];
			}else{
				$balance = 0;
			}
			$account_type = substr($suppAccCode,0,2);
			if($balance >= 0 && ($account_type == '01' || $account_type == '03')){
				$BalanceType = 'DR';
			}elseif($balance < 0 && ($account_type == '01' || $account_type == '03')){
				$BalanceType = 'CR';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $balance >= 0){
				$BalanceType = 'CR';
			}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $balance < 0){
				$BalanceType = 'DR';
			}

			return str_replace('-','',$balance)." ".$BalanceType;
		}
		public function delete($id){
			$query = "DELETE FROM suppliers WHERE SUPP_ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function delete_code($id){
			$query = "DELETE FROM suppliers WHERE SUPP_ACC_CODE ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
