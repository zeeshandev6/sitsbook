<?php

class MeasureType {

    public $title;
    public $status;
    public $description;
    public $totalMatchRecords;

    public function getList(){
        $query = "SELECT * FROM measure";
        return mysql_query($query);
    }

    public function save(){
        $query = "INSERT INTO measure(  NAME,
                                        DESCRIPTIONS,
                                        ACTIVE)
                              VALUES(  '$this->title',
                                       '$this->description',
                                       '$this->status')";
        mysql_query($query);
        return mysql_insert_id();
    }

    public function update($id){
        $query = "UPDATE `measure` SET `NAME`         ='$this->title',
                                       `DESCRIPTIONS`  ='$this->description',
                                       `ACTIVE`        ='$this->status' WHERE `ID` = '$id'";
        return mysql_query($query);
    }


    public function delete($id){
        $query = "DELETE FROM measure WHERE ID = '".$id."'";
        return mysql_query($query);
    }

    public function getRecordDetailsByID($id){
        $query = "SELECT * FROM measure WHERE ID ='$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array;
        }
    }
	public function getName($id){
        $query = "SELECT NAME FROM measure WHERE ID ='$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array['NAME'];
        }else{
        	return '';
        }
    }
    public function searchMeasure_Type($start,$pageLimit){
        $query = "SELECT SQL_CALC_FOUND_ROWS measure.ID,
                                        measure.NAME,             measure.ACTIVE,
                                        measure.DESCRIPTIONS FROM measure";

        if( ($this->title)!="" || ($this->status)!="")
        {
            $query .= " WHERE ";
        }
        $andFlag = false;

        if(($this->title)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " measure.NAME LIKE '%".$this->title."%'";
            $andFlag = true;
        }
        if(($this->status)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " measure.ACTIVE LIKE '%".$this->status."%'";
            $andFlag = true;
        }
        $query .= " LIMIT $start,$pageLimit";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];

        return $result;
    }

    public function updateStatus($id){
        $query = "UPDATE `measure` SET `ACTIVE`='$this->status' WHERE `ID` = '$id'";
        return mysql_query($query);
    }
}
?>
