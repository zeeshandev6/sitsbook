<?php
    class StockTransferDetail{
        public $transfer_id;
        public $item_id;
        public $from_branch;
        public $to_branch;
        public $quantity;
        public $unit_cost;
        public $total_cost;

        public function getList($transfer_id){
            $query = "SELECT * FROM `stock_transfer_detail` WHERE TRANSFER_ID = '$transfer_id' ";
            return mysql_query($query);
        }
        public function getRowIdArray($transfer_id){
            $query  = "SELECT ID FROM `stock_transfer_detail` WHERE TRANSFER_ID = '$transfer_id' ";
            $result = mysql_query($query);
            $array  = array();
            if(mysql_num_rows($result)){
                while($row = mysql_fetch_assoc($result)){
                    $array[] = $row['ID'];
                }
            }
            return $array;
        }
        public function getDetail($id){
            $query  = "SELECT * FROM `stock_transfer_detail` WHERE ID = '$id' ";
            $result = mysql_query($query);
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }
        }
        public function save(){
            $query = "INSERT INTO `stock_transfer_detail`(`TRANSFER_ID`, 
                                             `ITEM_ID`, 
                                             `FROM_BRANCH`, 
                                             `TO_BRANCH`,                                              
                                             `QUANTITY`,
                                             `UNIT_COST`,
                                             `TOTAL_COST`) 
                                     VALUES ('$this->transfer_id',
                                             '$this->item_id',
                                             '$this->from_branch',
                                             '$this->to_branch',
                                             '$this->quantity',
                                             '$this->unit_cost',
                                             '$this->total_cost')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `stock_transfer_detail` SET `ITEM_ID`     = '$this->item_id',
                                                         `FROM_BRANCH` = '$this->from_branch',
                                                         `TO_BRANCH`   = '$this->to_branch',
                                                         `QUANTITY`    = '$this->quantity',
                                                         `UNIT_COST`   = '$this->unit_cost',
                                                         `TOTAL_COST`  = '$this->total_cost' WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function delete($id){
            $query = "DELETE FROM `stock_transfer_detail` WHERE ID = '$id' ";
            return mysql_query($query);
        }
    }
?>