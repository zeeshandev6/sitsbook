<?php
	class Design{
		public $title;
		public $code;
		public $description;
		public $image;
		
		public function getList(){
			$query = "SELECT * FROM design ORDER BY DESIGN_ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM design ORDER BY DESIGN_ID DESC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(TITLE) FROM design";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM design WHERE DESIGN_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function getImageByCode($design_code){
			$query = "SELECT IMAGE FROM design WHERE DESIGN_CODE ='$design_code' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['IMAGE'];
			}else{
				return NULL;
			}
		}
		public function newDesignCode(){
			$query = "SELECT MAX(DESIGN_CODE) FROM `design`";
			$code = mysql_fetch_array(mysql_query($query));
			$lastCode = $code[0];
			return ++$lastCode;
		}
		public function addDesign(){
			$query = "INSERT INTO `design`(`TITLE`, `DESIGN_CODE`, `IMAGE`, `DESCRIPTION`) 
			VALUES('$this->title','$this->code','$this->image','$this->description')";
			$inserted = mysql_query($query);
			return $inserted;
		}
		public function update($id){
			$query = "UPDATE `design` SET 
					 `DESIGN_CODE`='$this->code',
					 `TITLE`='$this->title',
					 `IMAGE`='$this->image',
					 `DESCRIPTION`='$this->description' WHERE DESIGN_ID ='$id'";
			$updated = mysql_query($query);
			return $updated;
		}
		public function search($term){
			$query = "select * from design where `TITLE` like '%".$term."%'";
			$records = mysql_query($query);
			return $records;
		}
		public function delete($id){
			$query = "DELETE FROM design WHERE DESIGN_ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>