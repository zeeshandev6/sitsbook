<?php
	class ScanSale{
		public $user_id;
		public $sale_date;
		public $record_time;
		public $bill_no;
		public $cust_acc_code;
		public $customer_name;
		public $merch_rate;
		public $voucher_id;
		public $fromDate;
		public $toDate;
		public $num_record;
		public $customer_mobile;

		public $sale_discount;
		public $sale_tax;

		public $total_amount;
		public $received_cash;
		public $delivery_charges;
		public $change_returned;
		public $collected;

		public $found_records;

		public $item_id;
		public $imei;

		public function getList(){
			$query = "SELECT * FROM mobile_sale WHERE  ORDER BY ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function countRows(){
			$query = "SELECT count(*) FROM mobile_sale ";
			$records = mysql_query($query);
			return $records;
		}
		public function getPaged($start,$total){
			$query = "SELECT * FROM mobile_sale ORDER BY BILL_NO ASC,SALE_DATE ASC LIMIT $start,$total";
			$records = mysql_query($query);
			return $records;
		}
		public function checkBillNumber($billNum,$supplierCode){
			$query = "SELECT BILL_NO FROM mobile_sale WHERE BILL_NO = $billNum AND CUST_ACC_CODE = '$supplierCode'";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM mobile_sale WHERE ID = $id ";
			$records = mysql_query($query);
			return $records;
		}
		public function genBillNumber(){
			$query 		 	 = "SELECT MAX(BILL_NO) AS S_BILL_NO FROM sale";
			$query_scan  = "SELECT MAX(BILL_NO) AS SS_BILL_NO FROM mobile_sale";
			$record      = mysql_query($query);
			$record_scan = mysql_query($query_scan);
			$row 	     	 = (mysql_num_rows($record))?mysql_fetch_assoc($record):NULL;
			$row_scan    = (mysql_num_rows($record_scan))?mysql_fetch_assoc($record_scan):NULL;
			if($row == NULL && $row_scan == NULL){
				$last_bill_number = 1;
			}else{
				$records     = array_merge($row,$row_scan);
				$last_bill_number = max($records);
			}
			return ++$last_bill_number;
		}
		public function getDetail($id){
			$query = "SELECT * FROM mobile_sale WHERE ID = '$id'  ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `mobile_sale`( `USER_ID`,
																					 `SALE_DATE`,
																					 `RECORD_TIME`,
																					 `BILL_NO`,
																					 `CUST_ACC_CODE`,
																					 `CUSTOMER_NAME`,
																					 `MERCH_RATE`,
																					 `MOBILE`,
																					 `SALE_DISCOUNT`,
																					 `SALE_TAX`,
																					 `TOTAL_AMOUNT`,
																					 `RECEIVED_CASH`,
																					 `RECOVERED_BALANCE`,
																					 `RECOVERY_AMOUNT`,
																					 `CHANGE_RETURNED`,
																					 `REMAINING_AMOUNT`,
																				 	 `DELIVERY_CHARGES`)
																			  VALUES ('$this->user_id',
																				  		  '$this->sale_date',
																				  		  '$this->record_time',
																				  		  '$this->bill_no',
																							  '$this->cust_acc_code',
																							  '$this->customer_name',
																								'$this->merch_rate',
																							  '$this->customer_mobile',
																							  '$this->sale_discount',
																							  '$this->sale_tax',
																							  '$this->total_amount',
																							  '$this->received_cash',
																							  '$this->recovered_balance',
																							  '$this->recovery_amount',
																							  '$this->change_returned',
																							  '$this->remaining_amount',
																								'$this->delivery_charges')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($invId){
			$query = "UPDATE  `mobile_sale` SET
												`SALE_DATE`  		  		= '$this->sale_date',
												`CUST_ACC_CODE`       = '$this->cust_acc_code',
												`CUSTOMER_NAME`       = '$this->customer_name',
												`MERCH_RATE`       		= '$this->merch_rate',
												`MOBILE` 		      		= '$this->customer_mobile',
												`SALE_DISCOUNT`       = '$this->sale_discount',
												`SALE_TAX`       	  	= '$this->sale_tax',
												`TOTAL_AMOUNT`        = '$this->total_amount',
												`RECEIVED_CASH`       = '$this->received_cash',
												`RECOVERED_BALANCE`   = '$this->recovered_balance',
												`RECOVERY_AMOUNT`     = '$this->recovery_amount',
												`CHANGE_RETURNED`     = '$this->change_returned',
												`REMAINING_AMOUNT`    = '$this->remaining_amount',
												`DELIVERY_CHARGES`    = '$this->delivery_charges' WHERE `ID` = '$invId' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function search($start,$per_page){
			$query = "SELECT SQL_CALC_FOUND_ROWS mobile_sale.* FROM mobile_sale ";
			$andFlag = false;

			if($this->imei != ""){
				$query .= " JOIN mobile_sale_details ON mobile_sale_details.SS_ID = mobile_sale.ID ";
				$query .= " JOIN mobile_purchase_details ON mobile_purchase_details.ID = mobile_sale_details.SP_DETAIL_ID ";
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_purchase_details.BARCODE = '$this->imei' ";
				$andFlag = true;
			}
			if(trim($this->cust_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.CUST_ACC_CODE ='".$this->cust_acc_code."' ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.BILL_NO = '$this->bill_no' ";
				$andFlag = true;
			}
			if($this->collected != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.COLLECT_STATUS = '$this->collected' ";
				$andFlag = true;
			}
			if($this->customer_name != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.CUSTOMER_NAME LIKE '%$this->customer_name%' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.SALE_DATE >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " mobile_sale.SALE_DATE <= '".$this->toDate."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY mobile_sale.BILL_NO DESC ";
			$query .= " LIMIT $start,$per_page ";
			$result = mysql_query($query);

			$totalRecords 			 = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function getDailySales($report_date){
			$query = "SELECT mobile_sale.*,mobile_sale_details.*
					  FROM mobile_sale INNER JOIN mobile_sale_details
					  ON mobile_sale.ID = mobile_sale_details.SS_ID
					  WHERE mobile_sale.SALE_DATE = '".$report_date."' GROUP BY mobile_sale_details.SS_ID ORDER BY mobile_sale_details.SS_ID ASC";
			return mysql_query($query);
		}
		public function report(){
			$query = "SELECT *,mobile_sale_details.SALE_PRICE AS customer_price,mobile_sale_details.TAX as CTAX,
					  mobile_sale_details.ID as MSDID,
					  mobile_sale_details.DISCOUNT as CDISCOUNT  FROM mobile_sale
					  INNER JOIN mobile_sale_details
					  ON mobile_sale.ID = mobile_sale_details.SS_ID
					  INNER JOIN mobile_purchase_details
					  ON mobile_sale_details.SP_DETAIL_ID = mobile_purchase_details.ID ";
			$andFlag = false;

			if(trim($this->user_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`mobile_sale`.`USER_ID` ='".$this->user_id."' ";
				$andFlag = true;
			}
			if(trim($this->cust_acc_code) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_sale.CUST_ACC_CODE ='".$this->cust_acc_code."' ";
				$andFlag = true;
			}
			if(trim($this->item_id) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "mobile_purchase_details.ITEM_ID ='".$this->item_id."' ";
				$andFlag = true;
			}
			if($this->bill_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BILL_NO` = '$this->bill_no' ";
				$andFlag = true;
			}
			if(trim($this->fromDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SALE_DATE` >= '".$this->fromDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDate)!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`SALE_DATE` <= '".$this->toDate."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY BILL_NO ASC,SALE_DATE DESC ";
        	$result = mysql_query($query);
			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM mobile_sale WHERE ID = $id LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
		public function setCollectionStatus($invId,$status){
			$query = "UPDATE `mobile_sale` SET `COLLECT_STATUS` = '$status' WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function insertVoucherId($invId,$voucherId){
			$query = "UPDATE `mobile_sale` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function insertStatusVoucher($invId,$voucherId){
			$query = "UPDATE `mobile_sale` SET `STATUS_VOUCHER` = $voucherId WHERE `ID` = $invId";
			return mysql_query($query);
		}
		public function getBillNumber($sale_id){
			$query = "SELECT BILL_NO FROM mobile_sale WHERE ID = '$sale_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['BILL_NO'];
			}else{
				return 0;
			}
		}
		public function getVoucherId($inventory_id){
			$query = "SELECT VOUCHER_ID FROM mobile_sale WHERE ID = $inventory_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function getStatusVoucher($id){
			$query = "SELECT STATUS_VOUCHER FROM mobile_sale WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['STATUS_VOUCHER'];
			}else{
				return 0;
			}
		}
		public function setSmsStatus($id,$sms_status){
			$query = "UPDATE `mobile_sale` SET `SMS_STATUS` = '$sms_status' WHERE `ID` =  $id ";
			return mysql_query($query);
		}
		public function getCustomerAmountFromVoucher($id){
			$query = " select AMOUNT FROM voucher_details WHERE SUBSTR(ACCOUNT_CODE,1,6) = '010104' AND VOUCHER_ID = (SELECT VOUCHER_ID FROM mobile_sale WHERE ID = $id ) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function validateDate($date){
		    $d = DateTime::createFromFormat('d-m-Y', $date);
		    return $d && $d->format('d-m-Y') == $date;
		}
	}
?>
