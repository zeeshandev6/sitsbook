<?php
	class StockHistory{

		public $po_no;
		public $type;
		public $date;
		public $party_name;
		public $gp_no;
		public $ref_no;
		public $vehicle_no;
		public $through;
		public $lot_no;
		public $vehicle_type;
		public $location;
		public $io_status;
		public $notes;
		public $account_id;

		public $pon;
		public $item_id;
		public $from_date;
		public $to_date;

		public $found_records;

		/**
		 * @return id, id of records inserted or error.
		 *
		 * @param  of var different types set against user form through object.
		 */
		public function save(){
			$query = "INSERT INTO `stock_history`(
										`PO_NO`, 						`TYPEE`, 					`INWARD_DATE`,
										`PARTY_CODE`,
										`LOT_NO`, 					`REFERENCE_NO`,
										`GPNO`, 						`VEHICLE_NO`, 		`THROUGH`,
										`VEHICLE_TYPE`, 		`LOCATION`, 			`IO_STATUS`,
										`NOTES`
										)
								VALUES (
										'$this->po_no',			'$this->type',			'$this->date',
										'$this->party_name',
										'$this->lot_no', 		'$this->ref_no',
										'$this->gp_no',			'$this->vehicle_no',	'$this->through',
										'$this->vehicle_type',	'$this->location',	'$this->io_status',
										'$this->notes'
										)";
			$this->recId = mysql_query($query);
			return $this->recId;
		}
		public function get_lot_list_for_perchi($scid,$stage){
			$query = "SELECT LOT_NO FROM `stock_history` WHERE PO_NO = '$scid' AND TYPEE = '$stage' ";
			return mysql_query($query);
		}
		public function getList(){
			$query = "SELECT * FROM `stock_history` ORDER BY ID DESC ";
			return mysql_query($query);
		}

		public function get_list_dated($today,$type){
			$query  = "SELECT COUNT(*) AS TOTAL FROM `stock_history` WHERE INWARD_DATE = '".$today."' AND IO_STATUS = '$type' ";
			$result = mysql_query($query);
			$row 	= mysql_fetch_assoc($result);
			return $row['TOTAL'];
		}
		public function getListById($id){
			$query = "SELECT * FROM stock_history WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function getListByType($type){
			$query = "SELECT * FROM stock_history WHERE TYPEE = '$type'";
			return mysql_query($query);
		}
		public function getInwardContractlist($type,$status,$po_no){
			$query = "SELECT * FROM stock_history  ";
			$andFlag = false;
			if($type != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " TYPEE = '$type' ";
				$andFlag = true;
			}
			if($status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " IO_STATUS = '$status' ";
				$andFlag = true;
			}
			if($this->ref_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " REFERENCE_NO = '$this->ref_no' ";
				$andFlag = true;
			}

			if($po_no > 0){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " PO_NO = '$po_no'";
			}
			$query .= " ORDER BY ID DESC ";
			return mysql_query($query);
		}
		public function getInwardContractDetailedlist($type,$status,$lot_no){
			$query = "SELECT * FROM stock_history INNER JOIN stock_history_detail ON stock_history_detail.STH_ID = stock_history.ID ";
			$andFlag = false;
			if($type != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.TYPEE = '$type' ";
				$andFlag = true;
			}
			if($status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.IO_STATUS = '$status' ";
				$andFlag = true;
			}
			if($this->ref_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.REFERENCE_NO = '$this->ref_no' ";
				$andFlag = true;
			}

			if($lot_no > 0){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.LOT_NO = '$lot_no'";
			}
			$query .= " ORDER BY stock_history.ID DESC ";
			return mysql_query($query);
		}
		public function getPages($start,$total){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM stock_history
					  INNER JOIN stock_history_detail ON stock_history.ID    = stock_history_detail.STH_ID  ";
			$andFlag = false;

			if($this->from_date != ''){
				$this->from_date = date("Y-m-d",strtotime($this->from_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE >= '$this->from_date' ";
				$andFlag = true;
			}

			if($this->to_date != ''){
				$this->to_date = date("Y-m-d",strtotime($this->to_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE <= '$this->to_date' ";
				$andFlag = true;
			}

			if($this->type != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.TYPEE = '$this->type' ";
				$andFlag = true;
			}
			if($this->io_status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.IO_STATUS = '$this->io_status' ";
				$andFlag = true;
			}
			if($this->po_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.PO_NO = '$this->po_no' ";
				$andFlag = true;
			}
			if($this->party_name != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.PARTY_CODE LIKE '$this->party_name%' ";
				$andFlag = true;
			}
			if($this->ref_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.REFERENCE_NO = '$this->ref_no' ";
				$andFlag = true;
			}
			if($this->through != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.THROUGH = '$this->through' ";
				$andFlag = true;
			}

			if($this->vehicle_type != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.VEHICLE_TYPE = '$this->vehicle_type' ";
				$andFlag = true;
			}

			if($this->vehicle_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.VEHICLE_NO = '$this->vehicle_no' ";
				$andFlag = true;
			}

			if($this->gp_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.GPNO = '$this->gp_no' ";
				$andFlag = true;
			}

			$query .= " GROUP BY stock_history.ID ORDER BY stock_history.ID ASC LIMIT $start,$total  ";
			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $result;
		}
		public function report(){
			$query = "SELECT * FROM stock_history
				      INNER JOIN stock_history_detail ON stock_history.ID    = stock_history_detail.STH_ID ";
			$andFlag = false;
			if($this->from_date != ''){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE >= '$this->from_date'";
				$andFlag = true;
			}
			if($this->to_date != ''){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE <= '$this->to_date'";
				$andFlag = true;
			}
			if($this->type != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.TYPEE = '$this->type' ";
				$andFlag = true;
			}
			if($this->item_id != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history_detail.ITEM = '$this->item_id' ";
				$andFlag = true;
			}
			if($this->ref_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.REFERENCE_NO = '$this->ref_no' ";
				$andFlag = true;
			}
			if($this->io_status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.IO_STATUS = '$this->io_status' ";
				$andFlag = true;
			}
			$query .= " ORDER BY stock_history.INWARD_DATE , stock_history.TYPEE ";
			$result = mysql_query($query);
			return $result;
		}
		public function getStockReport(){
			$query = "SELECT * FROM stock_history
				      INNER JOIN stock_history_detail ON stock_history.ID    = stock_history_detail.STH_ID ";
			$andFlag = false;
			if($this->from_date != ''){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE >= '$this->from_date'";
				$andFlag = true;
			}
			if($this->to_date != ''){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE <= '$this->to_date'";
				$andFlag = true;
			}
			if($this->lot_no != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.LOT_NO = '$this->lot_no' ";
				$andFlag = true;
			}
			if($this->item_id != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history_detail.ITEM = '$this->item_id' ";
				$andFlag = true;
			}

			$query .= $andFlag?" ":" WHERE ";
			$query .= $andFlag?" AND ":" ";
			$query .= " stock_history.IO_STATUS = 'I' ";
			$andFlag = true;

			$query .= " ORDER BY stock_history.INWARD_DATE, stock_history.LOT_NO  ";
			$result = mysql_query($query);
			return $result;
		}
		public function movedItemsReport(){
			$str   = ($this->po_no=="")?"":"WHERE PO_NO = '".$this->pon."'";
			$query = "SELECT stock_history_detail.ITEM FROM stock_history_detail ".$str." GROUP BY stock_history_detail.ITEM ";
			$result = mysql_query($query);
			return $result;
		}
		public function getOpeningStockSum($item_id,$start_date,$io_status){
			$query = "SELECT SUM(stock_history_detail.QUANTITY) AS STOCK_IN FROM stock_history
				      INNER JOIN stock_history_detail ON stock_history.ID    = stock_history_detail.STH_ID ";
			$andFlag = false;

			if($item_id != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history_detail.ITEM = '$item_id' ";
				$andFlag = true;
			}
			if($start_date != ''){
				$start_date = date('Y-m-d',strtotime($start_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE < '$start_date'";
				$andFlag = true;
			}
			if($io_status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.IO_STATUS = '$io_status'";
				$andFlag = true;
			}
			$stock_in = mysql_fetch_assoc(mysql_query($query));
			$stock = (float)$stock_in['STOCK_IN'];

			return  $stock;
		}
		public function getStockSum($item_id,$from_date,$to_date,$io_status){
			$query = "SELECT SUM(stock_history_detail.QUANTITY) AS STOCK_IN FROM stock_history
				      INNER JOIN stock_history_detail ON stock_history.ID    = stock_history_detail.STH_ID ";
			$andFlag = false;

			if($item_id != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history_detail.ITEM = '$item_id' ";
				$andFlag = true;
			}
			if($from_date != ''){
				$from_date = date('Y-m-d',strtotime($from_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE >= '$from_date'";
				$andFlag = true;
			}
			if($to_date != ''){
				$to_date = date('Y-m-d',strtotime($to_date));
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.INWARD_DATE <= '$to_date'";
				$andFlag = true;
			}
			if($io_status != ''){
				$query .= $andFlag?" ":" WHERE ";
				$query .= $andFlag?" AND ":" ";
				$query .= " stock_history.IO_STATUS = '$io_status'";
				$andFlag = true;
			}
			$stock_in = mysql_fetch_assoc(mysql_query($query));
			$stock = (float)$stock_in['STOCK_IN'];

			return  $stock;
		}
		public function getListByStatus($status){
			$query = "SELECT * FROM stock_history WHERE IO_STATUS = '$status'";
			return mysql_query($query);
		}
		public function getDetail($id){
			$query  = "SELECT * FROM stock_history WHERE ID = $id";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}

		/**
		 * @return true if update query executed else false.
		 *
		 * @method update function in Stock History for main table receiving id as parameter.
		 */
		public function update($id){
			$query = "UPDATE `stock_history` SET
										`PO_NO`					= '$this->po_no',
										`TYPEE`					= '$this->type',
										`PARTY_CODE`		= '$this->party_name',
										`LOT_NO`				= '$this->lot_no',
										`INWARD_DATE`		= '$this->date',
										`GPNO`					= '$this->gp_no',
										`REFERENCE_NO`  = '$this->ref_no',
										`VEHICLE_NO`		= '$this->vehicle_no',
										`THROUGH`				= '$this->through',
										`VEHICLE_TYPE`	= '$this->vehicle_type',
										`NOTES`					= '$this->notes',
										`LOCATION`			= '$this->location' WHERE ID = $id ";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `stock_history` WHERE ID = $id";
			return mysql_query($query);
		}
		public function getNewLotNumber($scid){
			$query = "SELECT MAX(LOT_NO) AS NEW_LOT_NO FROM stock_history WHERE PO_NO = '$scid' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				$lot_number = (int)$row['NEW_LOT_NO'];
				return ++$lot_number;
			}else{
				return 1;
			}
		}
		public function get_terry_towel_report($report_Date,$item_category_id){
			$category = ($item_category_id != '')?" AND stock_history_detail.ITEM IN (SELECT ID FROM items WHERE ITEM_CATG_ID = '$item_category_id') ":"";
			$po_no    = ($this->po_no > 0)?" AND stock_history.PO_NO = '$this->po_no' ":"";
			$query 	  = "SELECT * FROM stock_history_detail LEFT JOIN stock_history ON stock_history_detail.STH_ID = stock_history.ID WHERE  stock_history.INWARD_DATE <= '$report_Date' ".$po_no." ".$category." GROUP BY stock_history.PO_NO,stock_history.LOCATION,stock_history_detail.ITEM  ";
			$result   = mysql_query($query);
			return $result;
		}
		public function get_stock_for_terry_towel($po_no,$godown_id,$type,$report_date,$item_id){
			$typee 		= "  AND stock_history.IO_STATUS = '$type'  ";
			$po_no 		= "  stock_history.PO_NO = '$po_no' AND     ";
			$query 		= "SELECT SUM(QUANTITY) AS ITEM_STOCK FROM stock_history_detail JOIN stock_history ON stock_history_detail.STH_ID = stock_history.ID WHERE $po_no stock_history.LOCATION = '$godown_id' AND stock_history.INWARD_DATE <= '$report_date' $typee AND  stock_history_detail.ITEM = '$item_id' ";
			$result  	= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['ITEM_STOCK'];
			}else{
				return 0;
			}
		}
		public function get_prev_stock_for_terry_towel_report($item_id,$godown_id,$type,$report_date){
			$query = "SELECT SUM(QUANTITY) AS ITEM_STOCK FROM stock_history_detail JOIN stock_history ON stock_history_detail.STH_ID = stock_history.ID WHERE stock_history.INWARD_DATE < '$report_date' AND stock_history.IO_STATUS = '$type' AND stock_history_detail.ITEM = '$item_id' AND stock_history.LOCATION = '$godown_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row['ITEM_STOCK'];
			}else{
				return 0;
			}
		}
	}

	class StockHistoryDetail extends StockHistory {

		public $historyId;
		public $item;
		public $item_type;
		public $unit;
		public $qty_per_unit;
		public $quantity;
		public $suits;
		public $unit_cost;
		public $total_cost;
		public $remarks;


		/**
		 * @return id-of-records-inserted.
		 *
		 * @method save function in Stock History Detail for child table receiving id as parameter.
		 */
		public function save(){
			$query = "INSERT INTO `stock_history_detail`(`STH_ID`,
														  `ITEM`,
															`ITEM_TYPE`,
														  `UNIT`,
														  `QTY_PER_UNIT`,
														  `QUANTITY`,
															`SUITS`,
														  `UNIT_COST`,
														  `TOTAL_COST`)
												  VALUES ('$this->historyId',
												  		  '$this->item',
																'$this->item_type',
												  		  '$this->unit',
												  		  '$this->qty_per_unit',
												  		  '$this->quantity',
																'$this->suits',
															  '$this->unit_cost',
															  '$this->total_cost')";
			return mysql_query($query);
		}

		/**
		 * @return true if update query executed else false.
		 *
		 * @method update function in Stock History Detail for child table receiving id as parameter.
		 */
		public function update($id){
			$query = " UPDATE `stock_history_detail` SET
									`ITEM`					= '$this->item',
									`ITEM_TYPE`			= '$this->item_type',
									`UNIT`					= '$this->unit',
									`QTY_PER_UNIT`	= '$this->qty_per_unit',
									`QUANTITY`		  = '$this->quantity',
									`SUITS`		  		= '$this->suits',
									`UNIT_COST`		  = '$this->unit_cost',
									`TOTAL_COST`	  = '$this->total_cost' WHERE ID = $id";
			return mysql_query($query);
		}
		public function getRecordDetailsById($id){
			$query = "SELECT * FROM `stock_history_detail` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_assoc($result);
			}else{
				return NULL;
			}
		}
		public function get_grey_fabric_inward($item_id,$last_date,$type){
			$query  = " SELECT SUM(QUANTITY)  AS TOTAL_QTY FROM `stock_history_detail`
					    JOIN `stock_history` ON stock_history.ID = stock_history_detail.STH_ID
					    WHERE stock_history_detail.ITEM    	   	 = '$item_id'
					    AND   stock_history.INWARD_DATE 		  	<= '$last_date'
					    AND   stock_history.IO_STATUS 		       = '$type' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$result = mysql_fetch_assoc($result);
				return (float)$result['TOTAL_QTY'];
			}else{
				return 0;
			}
		}
		public function getListByHisId($id){
			$query = "SELECT * FROM stock_history_detail WHERE STH_ID = '$id' ";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `stock_history_detail` WHERE ID = $id ";
			return mysql_query($query);
		}
		public function delete_io($sth_id){
			$query = "DELETE FROM `stock_history_detail` WHERE STH_ID = $sth_id ";
			return mysql_query($query);
		}
	}
?>
