<?php
	class inventory_details{
		public $purchase_id;
		public $item_id;
		public $cartons;
		public $per_corton;
		public $qtyReceipt;
		public $unitPrice;
		public $purchaseDiscount;
		public $subTotal;
		public $taxRate;
		public $taxAmount;
		public $totalAmount;
		public $batch_no;
		public $expiry_date;
		public $inStock;

		public function getList($purchase_id){
			$query = "SELECT * FROM purchase_details WHERE PURCHASE_ID = '$purchase_id' ";
			return mysql_query($query);
		}
		public function getDetails($invDetailId){
			$query = "SELECT * FROM `purchase_details` WHERE `ID` = '$invDetailId' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getItemHistory($item_id,$account_code,$limit){
			$query = "SELECT purchase.PURCHASE_DATE,items.NAME,purchase_details.UNIT_PRICE,purchase_details.PURCHASE_DISCOUNT
								FROM purchase
								INNER JOIN purchase_details ON purchase.ID   = purchase_details.PURCHASE_ID
								INNER JOIN items ON purchase_details.ITEM_ID = items.ID
								WHERE purchase_details.ITEM_ID = '$item_id' ";
			if($account_code!=''){
				$query .= " AND purchase.SUPP_ACC_CODE = '$account_code' ";
			}
			$query .= " ORDER BY purchase.PURCHASE_DATE DESC LIMIT $limit ";
			$result= mysql_query($query);
			$rows  = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_assoc($result)){
					$row['PURCHASE_DATE'] = date('d-m-Y',strtotime($row['PURCHASE_DATE']));
					$rows[]  = $row;
				}
				return $rows;
			}
			return NULL;
		}
		public function getListByPoNumber($po_number){
			$query = "SELECT ITEM_ID,SUM(STOCK_QTY) AS TOTAL_QTY FROM purchase_details WHERE PURCHASE_ID IN  (SELECT ID FROM purchase WHERE PO_NUMBER = '$po_number') GROUP BY ITEM_ID ";
			return mysql_query($query);
		}
		public function getLastPurchasedItemRecord($item_id){
			$query = "SELECT * FROM `purchase_details` WHERE `ITEM_ID` = '$item_id' ORDER BY ID DESC LIMIT 1 ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				$query = "SELECT PURCHASE_PRICE FROM `items` WHERE `ID` = '$item_id' ";
				$record = mysql_query($query);
				if(mysql_num_rows($record)){
					$row = mysql_fetch_array($record);
					return $row['PURCHASE_PRICE'];
				}
			}
			return NULL;
		}
		public function getQtyAgainstPoItem($po_number,$item_id){
			$query = "SELECT SUM(STOCK_QTY) AS TOTAL_QTY FROM `purchase_details` WHERE ITEM_ID = '$item_id' AND `PURCHASE_ID` IN (SELECT ID FROM purchase WHERE PO_NUMBER = '$po_number') ";
			$record = mysql_query($query);
			echo mysql_error();
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_QTY']=='')?0:$row['TOTAL_QTY'];
			}else{
				return 0;
			}
		}
		public function getQuantity($invDetailId){
			$query = "SELECT STOCK_QTY FROM `purchase_details` WHERE `ID` = $invDetailId";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['STOCK_QTY']=='')?0:$row['STOCK_QTY'];
			}else{
				return 0;
			}
		}
        public function getAllQuantity($item_id){
			$query = "SELECT SUM(UNIT_PRICE) AS COST,SUM(STOCK_QTY) AS ALL_QTY FROM `purchase_details` WHERE `ITEM_ID` = '$item_id' ";
			$record = mysql_query($query);
            echo mysql_error();
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return 0;
			}
		}
		public function save(){
			$query = "INSERT INTO `purchase_details`	(`PURCHASE_ID`,
																								 `ITEM_ID`,
																								 `CARTONS`,
																								 `PER_CARTON`,
																								 `STOCK_QTY`,
																								 `UNIT_PRICE`,
																								 `PURCHASE_DISCOUNT`,
																								 `SUB_AMOUNT`,
																								 `TAX_RATE`,
																								 `TAX_AMOUNT`,
																								 `TOTAL_AMOUNT`,
																							 	 `BATCH_NO`,
																							 	 `EXPIRY_DATE`)
																						 VALUES ('$this->purchase_id',
																									 	 '$this->item_id',
													                           '$this->cartons',
													                           '$this->per_carton',
																										 '$this->qtyReceipt',
																										 '$this->unitPrice',
																										 '$this->purchaseDiscount',
																										 '$this->subTotal',
																										 '$this->taxRate',
																										 '$this->taxAmount',
																										 '$this->totalAmount',
																									 	 '$this->batch_no',
																									 	 '$this->expiry_date')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($purchase_detail_id){
			$query = "UPDATE `purchase_details` SET `ITEM_ID`           = '$this->item_id',
																							`CARTONS`           = '$this->cartons',
										                          `PER_CARTON`        = '$this->per_carton',
										                          `STOCK_QTY`         = '$this->qtyReceipt',
																							`UNIT_PRICE`        = '$this->unitPrice',
																							`PURCHASE_DISCOUNT` = '$this->purchaseDiscount',
																							`SUB_AMOUNT`        = '$this->subTotal',
																							`TAX_RATE`          = '$this->taxRate',
																							`TAX_AMOUNT`        = '$this->taxAmount',
																							`TOTAL_AMOUNT`      = '$this->totalAmount',
																							`BATCH_NO`        	= '$this->batch_no',
																							`EXPIRY_DATE`       = '$this->expiry_date' WHERE ID = '$purchase_detail_id' ";
			return mysql_query($query);
		}
		public function delete($purchase_detail_id){
			$query = "DELETE FROM purchase_details WHERE ID = '$purchase_detail_id' ";
			return mysql_query($query);
		}
		public function deleteCompleteBill($purchase_id){
			$query = "DELETE FROM purchase_details WHERE PURCHASE_ID = '$purchase_id' ";
			return mysql_query($query);
		}
	}
?>
