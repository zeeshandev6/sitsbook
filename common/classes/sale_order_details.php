<?php
class SaleOrderDetails{
    public $order_id;
    public $order_no;
    public $customer_name;
    public $product_id;
    public $product_title;
    public $quantity;
    public $amount;
    public $email;
    public $mobile;

    public function getList(){
        $query = "SELECT * FROM `sale_order_details` ";
        return mysql_query($query);
    }
    public function getListByDate($day){
        $query = "SELECT * FROM `sale_order_details` WHERE ORDER_DATE = '$day' ";
        return mysql_query($query);
    }
    public function getListByOrderId($order_id){
        $query = "SELECT * FROM `sale_order_details` WHERE ORDER_ID = '$order_id' ";
        return mysql_query($query);
    }
    public function getListByOrderNumber($order_no){
        $query = "SELECT * FROM `sale_order_details` WHERE ORDER_NO = '$order_no'";
        return mysql_query($query);
    }
    public function getOrderNumbersByOrderId($order_id){
        $query = "SELECT ORDER_NO FROM `sale_order_details` WHERE ORDER_ID = '$order_id' ";
        $result= mysql_query($query);
        $array = array();
        if(mysql_num_rows($result)){
          while($row = mysql_fetch_assoc($result)){
            $array[] = $row['ORDER_NO'];
          }
        }
        return $array;
    }
    public function save(){
        $query = "INSERT INTO `sale_order_details` (`ORDER_ID`,
                                            `ORDER_NO`,
                                            `CUSTOMER_NAME`,
                                            `PRODUCT_ID`,
                                            `PRODUCT_TITLE`,
                                            `QUANTITY`,
                                            `AMOUNT`,
                                            `EMAIL`,
                                            `MOBILE`)
                                    VALUES( '$this->order_id',
                                            '$this->order_no',
                                            '$this->customer_name',
                                            '$this->product_id',
                                            '$this->product_title',
                                            '$this->quantity',
                                            '$this->amount',
                                            '$this->email',
                                            '$this->mobile')";
        mysql_query($query);
        return mysql_insert_id();
    }
    public function update($id){
        $query = "UPDATE `sale_order_details` SET  `ORDER_NO`      = '$this->order_no',
                                                   `CUSTOMER_NAME` = '$this->customer_name',
                                                   `PRODUCT_ID`    = '$this->product_id',
                                                   `PRODUCT_TITLE` = '$this->product_title',
                                                   `QUANTITY`      = '$this->quantity',
                                                   `AMOUNT`        = '$this->amount',
                                                   `EMAIL`         = '$this->email',
                                                   `MOBILE`        = '$this->mobile' WHERE `ID` = '$id'";
        return mysql_query($query);
    }
    public function set_product_id($order_id,$product_id){
        $query = "UPDATE `sale_order_details` SET  `PRODUCT_ID`    = '$product_id' WHERE `ID` = '$order_id' ";
        return mysql_query($query);
    }
    public function delete($id){
        $query = "DELETE FROM sale_order_details WHERE ID = '".$id."'";
        return mysql_query($query);
    }
    public function deleteFullOrder($id){
        $query = "DELETE FROM sale_order_details WHERE ORDER_ID = '".$id."'";
        return mysql_query($query);
    }
    public function getDetail($id){
        $query = "SELECT * FROM sale_order_details WHERE ID = '$id'";
        $records = mysql_query($query);
        $row = mysql_num_rows($records);
        if($row > 0){
            $array = mysql_fetch_assoc($records);
            return $array;
        }
    }
}
?>
