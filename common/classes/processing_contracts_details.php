<?php
  class ProcessingContractDetails{
    public $fabric_id;
    public $fabric_type;
    public $thaan;
    public $construction;
    public $grey_width;
    public $quantity;
    public $unit_price;
    public $total_amount;
    public $process;
    public $color;
    public $from_party;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;

    public $found_records;

    public function getList(){
      $query = "SELECT * FROM processing_contract_details ORDER BY ID DESC ";
      return mysql_query($query);
    }
    public function getListById($id){
      $query = "SELECT * FROM processing_contract_details WHERE FABRIC_ID = $id";
      return mysql_query($query);
    }
    public function getListByLotNo($lot_no){
      $query = "SELECT * FROM processing_contract_details WHERE FABRIC_ID IN (SELECT ID FROM processing_contracts WHERE LOT_NO = $lot_no  AND VOUCHER_ID > 0) ";
      return mysql_query($query);
    }
    public function save(){
      $query = "INSERT INTO `processing_contract_details`(
        FABRIC_ID,
        FABRIC_TYPE,
        THAAN,
        QUANTITY,
        UNIT_PRICE,
        TOTAL_AMOUNT,
        PROCESS,
        COLOR,
        FROM_PARTY
      )
      VALUES(
        '$this->fabric_id',
        '$this->fabric_type',
        '$this->thaan',
        '$this->quantity',
        '$this->unit_price',
        '$this->total_amount',
        '$this->process',
        '$this->color',
        '$this->from_party'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }
    public function update($id){
      $query = "UPDATE `processing_contract_details` SET
      `FABRIC_TYPE`      = '$this->fabric_type',
      `THAAN`            = '$this->thaan',
      `QUANTITY`         = '$this->quantity',
      `UNIT_PRICE`       = '$this->unit_price',
      `TOTAL_AMOUNT`     = '$this->total_amount',
      `PROCESS`          = '$this->process',
      `COLOR`            = '$this->color',
      `FROM_PARTY`       = '$this->from_party' WHERE ID = '$id'";
      return mysql_query($query);
    }
    public function deleteContract($id){
      $query = "DELETE FROM processing_contract_details WHERE FABRIC_ID = '".$id."'";
      return mysql_query($query);
    }
    public function delete($id){
      $query = "DELETE FROM processing_contract_details WHERE ID = '".$id."'";
      return mysql_query($query);
    }
    public function insertVoucherId($contract_id,$voucher_id){
      $query = "UPDATE processing_contract_details SET VOUCHER_ID = '$voucher_id' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function get_voucher_id($contract_id){
      $query = "SELECT VOUCHER_ID FROM processing_contract_details WHERE ID = '$contract_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
