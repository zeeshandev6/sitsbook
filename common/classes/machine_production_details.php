<?php
	class MachineProductionDetails{

		public $mp_id;
		public $party_acc_code;
		public $fabric_type;
		public $thread_type;
		public $design_no;
		public $item;
		public $stitch;
		public $shift_a;
		public $shift_b;
		public $tp;
		public $yard;
		public $at_rate;
		public $sale;
		public $thread_weight;
		public $rate;
		public $thread_price;
		public $total_price;
		public $stitch_a;
		public $stitch_b;
		public $total_stitch;

		public function getList($mp_id){
			$query = "SELECT * FROM `machine_production_details` WHERE MP_ID = '$mp_id' ";
			$result= mysql_query($query);
			return $result;
		}
		public function getDetails($id){
			$query = "SELECT * FROM `machine_production_details` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getPrevMachineProductionPnL($report_date){
			$query = "SELECT SUM(machine_production_details.SALE) AS SALE_ROW,
											 SUM(machine_production_details.TOTAL_PRICE) AS THREAD_EXPENSE_TOTAL,
			 								 SUM(machine_production.EXPENSE) AS EXPENSE_TOTAL FROM machine_production_details
								JOIN machine_production ON machine_production_details.MP_ID = machine_production.ID
			 					WHERE machine_production.REPORT_DATE < '$report_date' ";
			$result = mysql_query($query);
			$row    = mysql_fetch_assoc($result);
			return $row['SALE_ROW'] - ($row['THREAD_EXPENSE_TOTAL']+$row['EXPENSE_TOTAL']);
		}
		public function save(){
			$query = "INSERT INTO `machine_production_details`(`MP_ID`,
																 `PARTY_ACC_CODE`,
															   `FABRIC_TYPE`,
															   `THREAD_TYPE`,
															   `DESIGN_NO`,
															   `ITEM`,
															   `STITCH`,
															   `SHIFT_A`,
															   `SHIFT_B`,
															   `TP`,
															   `YARD`,
															   `AT_RATE`,
															   `SALE`,
															   `THREAD_WEIGHT`,
															   `RATE`,
															   `THREAD_PRICE`,
															   `TOTAL_PRICE`,
															   `STITCH_A`,
															   `STITCH_B`)
														VALUES ('$this->mp_id',
																	'$this->party_acc_code',
															   	'$this->fabric_type',
															   	'$this->thread_type',
															   	'$this->design_no',
																	'$this->item',
																	'$this->stitch',
																	'$this->shift_a',
																	'$this->shift_b',
																	'$this->tp',
																	'$this->yard',
																	'$this->at_rate',
																	'$this->sale',
																	'$this->thread_weight',
																	'$this->rate',
																	'$this->thread_price',
																	'$this->total_price',
																	'$this->stitch_a',
																	'$this->stitch_b') ";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `machine_production_details` SET `MP_ID` = '$this->mp_id',
																 							 `PARTY_ACC_CODE` = '$this->party_acc_code',
															   					 		 `FABRIC_TYPE` 		= '$this->fabric_type',
																						   `THREAD_TYPE`		= '$this->thread_type',
																						   `DESIGN_NO`			= '$this->design_no',
																						   `ITEM`						= '$this->item',
																						   `STITCH`					= '$this->stitch',
																						   `SHIFT_A`				= '$this->shift_a',
																						   `SHIFT_B`				= '$this->shift_b',
																						   `TP`							= '$this->tp',
																						   `YARD`						= '$this->yard',
																						   `AT_RATE`				= '$this->at_rate',
																						   `SALE`						= '$this->sale',
																						   `THREAD_WEIGHT`	= '$this->thread_weight',
																						   `RATE`						= '$this->rate',
																						   `THREAD_PRICE`		= '$this->thread_price',
																						   `TOTAL_PRICE`		= '$this->total_price',
																						   `STITCH_A`				= '$this->stitch_a',
																						   `STITCH_B`				= '$this->stitch_b' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function delete_report($mp_id){
			$query = "DELETE FROM `machine_production_details` WHERE MP_ID = '$mp_id' ";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `machine_production_details` WHERE ID = '$id' ";
			return mysql_query($query);
		}
	}
?>
