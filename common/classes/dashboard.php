<?php
	class Dashboard{

		public function getSales($today,$salesAccCode){

			$monthly = date('Y-m-01',strtotime($today));
			$yearly  = date('Y-01-01',strtotime($today));

			//today
			$queryToday = "SELECT SUM(AMOUNT) AS SALES FROM voucher_details
						   WHERE VOUCHER_DATE = DATE('".$today."') AND ACCOUNT_CODE = '".$salesAccCode."' AND TRANSACTION_TYPE = 'Cr' ";
			$todaysRecord = mysql_query($queryToday);
			if(mysql_num_rows($todaysRecord)){
				$todayRow     = mysql_fetch_array($todaysRecord);
				$todaySale	  = $todayRow['SALES']==''?0:$todayRow['SALES'];
			}else{
				$todaySale    = 0;
			}

			//monthly
			$queryMonthly = "SELECT SUM(AMOUNT) AS SALES FROM voucher_details
						   WHERE VOUCHER_DATE <= DATE('".$today."') AND VOUCHER_DATE >= DATE('".$monthly."')
						   AND ACCOUNT_CODE = '".$salesAccCode."'  AND TRANSACTION_TYPE = 'Cr' ";
			$monthlyRecord = mysql_query($queryMonthly);
			if(mysql_num_rows($monthlyRecord)){
				$monthlyRow     = mysql_fetch_array($monthlyRecord);
				$monthlySale	    = $monthlyRow['SALES']==''?0:$monthlyRow['SALES'];
			}else{
				$monthlySale    = 0;
			}

			//monthly
			$queryMonthly = "SELECT SUM(AMOUNT) AS SALES FROM voucher_details
						   WHERE VOUCHER_DATE <= DATE('".$today."') AND VOUCHER_DATE >= DATE('".$monthly."')
						   AND ACCOUNT_CODE = '0201010003'  AND TRANSACTION_TYPE = 'Dr' ";
			$monthlyRecord = mysql_query($queryMonthly);
			if(mysql_num_rows($monthlyRecord)){
				$monthlyRow     = mysql_fetch_array($monthlyRecord);
				$monthlySaleReturn	    = $monthlyRow['SALES']==''?0:$monthlyRow['SALES'];
			}else{
				$monthlySaleReturn    = 0;
			}

			$monthlySale -= $monthlySaleReturn;

			//yearly
			$queryYearly = "SELECT SUM(AMOUNT) AS SALES FROM voucher_details
						   WHERE VOUCHER_DATE <= DATE('".$today."') AND VOUCHER_DATE >= DATE('".$yearly."')
						   AND ( ACCOUNT_CODE = '".$salesAccCode."' )  AND TRANSACTION_TYPE = 'Cr' ";
			$yearlyRecord = mysql_query($queryYearly);
			if(mysql_num_rows($yearlyRecord)){
				$yearlyRow     = mysql_fetch_array($yearlyRecord);
				$yearlySale	    = $yearlyRow['SALES'] == ''?0:$yearlyRow['SALES'];
			}else{
				$yearlySale    = 0;
			}

			//yearly
			$queryYearly = "SELECT SUM(AMOUNT) AS SALES FROM voucher_details
						   WHERE VOUCHER_DATE <= DATE('".$today."') AND VOUCHER_DATE >= DATE('".$yearly."')
						   AND ( ACCOUNT_CODE = '0201010003' )  AND TRANSACTION_TYPE = 'Dr' ";
			$yearlyRecord = mysql_query($queryYearly);
			if(mysql_num_rows($yearlyRecord)){
				$yearlyRow     = mysql_fetch_array($yearlyRecord);
				$yearlySaleReturn	    = $yearlyRow['SALES'] == ''?0:$yearlyRow['SALES'];
			}else{
				$yearlySaleReturn    = 0;
			}

			$yearlySale -= $yearlySaleReturn;

			$todaySale = round($todaySale*100)/100;
			$monthlySale = round($monthlySale*100)/100;
			$yearlySale = round($yearlySale*100)/100;

			$salesArray = array('TODAY'=>$todaySale,'MONTHLY'=>$monthlySale,'YEARLY'=>$yearlySale);
			return $salesArray;
		}
		public function getTodaysPurchases($today,$purchaseAccCode){
			$queryToday = "SELECT SUM(AMOUNT) AS PURCHASES FROM voucher_details
						   WHERE VOUCHER_DATE = DATE('".$today."') AND ( ACCOUNT_CODE = '".$purchaseAccCode."' OR ACCOUNT_CODE = '0101080001' ) AND TRANSACTION_TYPE = 'Dr' ";
			$todaysRecord = mysql_query($queryToday);
			if(mysql_num_rows($todaysRecord)){
				$todayRow     = mysql_fetch_array($todaysRecord);
				$todayPurchase	  = $todayRow['PURCHASES']==''?0:$todayRow['PURCHASES'];
			}else{
				$todayPurchase    = 0;
			}
			return $todayPurchase;
		}
		public function getThirdLevelTotal($fromDate,$toDate,$sixDigitCode,$fromOp){
			$accountReceivables = $this->getAccountByCatAccCode($sixDigitCode);
			$total_amount = 0;
			if(mysql_num_rows($accountReceivables)){
				while($row = mysql_fetch_array($accountReceivables)){
					$total_amount += $this->getLedgerOpeningBalance($toDate, $row['ACC_CODE'], $fromOp);
				}
			}
			return $total_amount;
		}
		public function sumAccountsDebits($today,$sixDigitCode,$to_opt){
			$accountReceivables = $this->getAccountByCatAccCode($sixDigitCode);
			$total_amount = 0;
			if(mysql_num_rows($accountReceivables)){
				while($row = mysql_fetch_array($accountReceivables)){
					$total_amount += $this->getAccountDebitsSum($row['ACC_CODE'],'',$today,'',$to_opt);
				}
			}
			return $total_amount;
		}
        public function sumAccountsCredits($today,$sixDigitCode,$to_opt){
			$accountReceivables = $this->getAccountByCatAccCode($sixDigitCode);
			$total_amount = 0;
			if(mysql_num_rows($accountReceivables)){
				while($row = mysql_fetch_array($accountReceivables)){
					$total_amount += $this->getAccountCreditsSum($row['ACC_CODE'],'',$today,'=',$to_opt);
				}
			}
			return $total_amount;
		}
		public function getAccountByCatAccCode($sixDigitCode){
			$query = "SELECT * FROM account_code WHERE SUBSTR(ACC_CODE,1,6) = '$sixDigitCode' AND LENGTH(ACC_CODE) = 9 ORDER BY ACC_CODE";
			$accountsList = mysql_query($query);
			return $accountsList;
		}
		public function calculatePerCentage($largeValue,$smallValue){
			if($largeValue == 0||$smallValue == 0){
				return 0;
			}
			return ($smallValue/$largeValue)*100;
		}

		public function cashChartWeekly($numberOfWeeks){
			$cash  = array();
			$cash[]= '010101';$cash[]= '010102';
			$endOfLastWeek = $this->getEndOfLastWeek();
			$cashWeekly = array();
			while($numberOfWeeks >= 1){
				foreach($cash as $k => $cashCode){
					$cashCodes = $this->getAccountByCatAccCode($cashCode);
					if(mysql_num_rows($cashCodes)){
						while($cashCodeRow = mysql_fetch_array($cashCodes)){
							$weeklyBalance = $this->getLedgerOpeningBalance($endOfLastWeek,$cashCodeRow['ACC_CODE'],'<=');
							if(!isset($cashWeekly[$endOfLastWeek])){
								$cashWeekly[$endOfLastWeek] = 0;
							}
							$cashWeekly[$endOfLastWeek] += $weeklyBalance;
						}
					}
				}
				--$numberOfWeeks;
				$endOfLastWeek = date('Y-m-d',strtotime($endOfLastWeek." - 1 week"));
			}
			$cashWeekly = array_reverse($cashWeekly);
			return $cashWeekly;
		}
		public function trends_chart($today,$revenues,$expenses,$equities){
			$bars_data = array();
			$current_month = date('m',strtotime($today));
			for($i=1;$i<=$current_month;$i++){
				if($i < 10){
					$i = '0'.$i;
				}
				$month_start_date = date('Y-'.$i.'-01');
				$month_end_date   = date('Y-m-d',strtotime($month_start_date." +1 month"));
				$month_end_date   = date('Y-m-d',strtotime($month_end_date." -1 day"));
				$repo_month = date('M-Y',strtotime($month_start_date));
				$bars_data[$repo_month] = array();
				$revenueTotal  = 0;
				foreach($revenues as $key=>$rev_acc){
					$profitOrLoss  =  $this->getAccountCreditsSum($rev_acc, $month_start_date, $month_end_date, '>=', '<=');
					$revenueTotal += $profitOrLoss;
				}
				$less          =  $this->getAccountDebitsSum('0201010003', $month_start_date, $month_end_date, '>=', '<=');
				$revenueTotal -= $less;
				$bars_data[$repo_month]['REVENUE'] = $revenueTotal;
				$expenseTotal  = 0;
				foreach($expenses as $key=>$exp_acc){
					$main_acc = substr($exp_acc, 0,6);
					$profitOrLoss = $this->getAccountDebitsSum($exp_acc, $month_start_date, $month_end_date, '>=', '<=');
					$less 		  = $this->getAccountCreditsSum($exp_acc, $month_start_date, $month_end_date, '>=', '<=');
					$profitOrLoss-= $less;
					$expenseTotal += $profitOrLoss;
				}
				$bars_data[$repo_month]['EXPENSE'] = $expenseTotal;
				$equityTotal  = 0;
				foreach($equities as $key=>$eq_acc){
					$main_acc = substr($eq_acc, 0,6);
					$ledger_open  = $this->getLedgerOpeningBalance($month_start_date,$eq_acc,'<=');
					$ledger_close = $this->getLedgerOpeningBalance($month_end_date,$eq_acc,'<=');
					$eq_average   = ($ledger_open+$ledger_close)/2;
					$exrev 		  = ($bars_data[$repo_month]['REVENUE'] - $bars_data[$repo_month]['EXPENSE']);
					if($exrev == 0){
						continue;
					}
					if($eq_average == 0){
						continue;
					}
					$eq_average  = $exrev/$eq_average;
					$eq_average  = $eq_average*100;
					$equityTotal += $eq_average;
				}
				$bars_data[$repo_month]['RETURN'] = $equityTotal;
			}//end for
			return $bars_data;
		}
		public function getEndOfLastWeek(){
			return date('Y-m-d',strtotime('last monday'));
		}

		public function getExpensesSummaryChartMonthly($exp_accounts,$expense_month){
			$data_return   = array();

			$start_date    = date('Y-'.$expense_month.'-01');
			$end_date      = date('Y-'.$expense_month.'-t',strtotime(date('Y-'.$expense_month."-d")));
			$data_return = array();
			foreach($exp_accounts as $key=>$exp_acc){
				$main_acc = substr($exp_acc, 0,6);
				$expense_amount      = $this->getAccountDebitsSum($exp_acc, $start_date, $end_date, '>=', '<=');
				$expense_amount_less = $this->getAccountCreditsSum($exp_acc, $start_date, $end_date, '>=', '<=');
				$expense_amount     -= $expense_amount_less;
				if(isset($data_return[$main_acc])){
					$data_return[$main_acc] += $expense_amount;
				}else{
					$data_return[$main_acc] = $expense_amount;
				}
			}
			return $data_return;
		}

		public function getLedgerOpeningBalance($endDate,$accCode,$operator){
			if(!isset($operator)){
				$operator = '<';
			}
			$query = "SELECT BALANCE
					  FROM voucher_details
					  WHERE VOUCHER_DATE ".$operator." DATE('".$endDate."')
					  AND ACCOUNT_CODE = '$accCode'
					  ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE']==''?0:$cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}

		public function getAccountHeadBalance($endDate,$headType){
			$query = "SELECT SUM(BALANCE) AS HEAD_BALANCE,SUBSTR(ACCOUNT_CODE,1,6) AS HEAD_TITLE
					  FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND SUBSTR(ACCOUNT_CODE,1,2) = '$headType'
					  AND LENGTH(ACCOUNT_CODE) = 9
					  ORDER BY VOUCHER_DATE DESC, COUNTER DESC GROUP BY SUBSTR(ACCOUNT_CODE,1,6) ";
		     $result = mysql_query($query);
			 return $result;
		}

		public function getAccountDebitsSum($accCode,$fromDate,$toDate,$fromOp,$toOp){
			$query = "SELECT SUM(voucher_details.AMOUNT) AS TOTAL_AMOUNT FROM voucher_details
								JOIN voucher ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID
			 					WHERE voucher_details.ACCOUNT_CODE = '".$accCode."'";
			if($fromDate != ''){
				$query .= " AND voucher_details.VOUCHER_DATE ".$fromOp." '".$fromDate."' ";
			}
			$query .= " AND voucher_details.VOUCHER_DATE ".$toOp." '".$toDate."' AND voucher_details.TRANSACTION_TYPE = 'Dr' AND voucher.VOUCHER_TYPE != 'PL' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_AMOUNT']=='')?0:$row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}

		/*
		public function getAccountCreditsSum($accCode,$fromDate,$toDate,$fromOp,$toOp){
			$query = "SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM voucher_details WHERE ACCOUNT_CODE = '".$accCode."' ";
			if($fromDate != ''){
				$query .= "AND VOUCHER_DATE ".$fromOp." DATE('".$fromDate."') ";
			}
			$query .= " AND VOUCHER_DATE ".$toOp." DATE('".$toDate."') AND TRANSACTION_TYPE = 'Cr' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_AMOUNT']=='')?0:$row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}
		*/

		public function getAccountCreditsSum($accCode,$fromDate,$toDate,$fromOp,$toOp){
			$query = "SELECT SUM(voucher_details.AMOUNT) AS TOTAL_AMOUNT FROM voucher_details
								JOIN voucher ON voucher.VOUCHER_ID = voucher_details.VOUCHER_ID
			 					WHERE voucher_details.ACCOUNT_CODE = '".$accCode."'";
			if($fromDate != ''){
				$query .= " AND voucher_details.VOUCHER_DATE ".$fromOp." '".$fromDate."' ";
			}
			$query .= " AND voucher_details.VOUCHER_DATE ".$toOp." '".$toDate."' AND voucher_details.TRANSACTION_TYPE = 'Cr' AND voucher.VOUCHER_TYPE != 'PL' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_AMOUNT']=='')?0:$row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}

		public function getLedgerOpeningBalanceLimit($endDate,$tillDate,$accCode){
			$query = "SELECT BALANCE
					  FROM voucher_details
					  WHERE VOUCHER_DATE <= DATE('".$endDate."')
					  AND VOUCHER_DATE > DATE('".$tillDate."')
					  AND ACCOUNT_CODE = '$accCode'
					  ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);

			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE']==''?0:$cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
	}
?>
