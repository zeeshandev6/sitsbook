<?php

class Messages{

    public $to_user_id;
    public $from_user_id;
    public $notification_date;
    public $stages;
    public $subject;
    public $descriptions;
    public $user_id;
    public $totalMatchRecords;

    public $msg_status;
    public $unread;

    public function getList(){
        $query = "SELECT * FROM messages";
        return mysql_query($query);
    }
    public function getListByIdScid($scid, $stage){
        $query = "SELECT * FROM messages WHERE SC_ID = '$scid' AND STAGE = '$stage'";
        return mysql_query($query);
    }
    public function getCountByDate($today){
        $query  = "SELECT COUNT(*) AS TOTAL FROM messages WHERE MSG_DATE = '$today'";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function getCountUnRead(){
        $query  = "SELECT COUNT(*) AS TOTAL FROM messages WHERE MSG_STATUS = 'N' ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function getListByIdScidStage($accid, $scid, $stage){
        $query = "SELECT * FROM messages WHERE USER_ID = '$accid' AND SC_ID = '$scid' AND STAGE = '$stage'";
        return mysql_query($query);
    }
    public function save(){
        $query = "INSERT INTO messages(
                                        FROM_USER_ID,
                                        TO_USER_ID,
                                        MSG_DATE,
                                        SUBJECT,
                                        DESCRIPTIONS,
                                        MSG_STATUS
                                       )
                                VALUES( '$this->from_user_id',
                                		'$this->to_user_id',
                                		'$this->notification_date',
                                        '$this->subject',
                                        '$this->descriptions',
                                        'N'
                                       )";
		  mysql_query($query);
		  return mysql_insert_id();
    }
    public function update($id){
        $query = "UPDATE `messages` SET
                                            `TO_USER_ID`='$this->to_user_id',
                                            `SUBJECT`='$this->subject',
                                            `DESCRIPTIONS`='$this->descriptions' WHERE ID = '$id'";
        return mysql_query($query);
    }
    public function delete($id){
        $query = "DELETE FROM messages WHERE ID = '".$id."'";
        return mysql_query($query);
    }
  	public function updateMessageStatus($id,$status){
  		$query = "UPDATE `messages` SET `MSG_STATUS` = 'Y' WHERE ID = '$id'";
  		return mysql_query($query);
  	}
    public function getListById($id){
        $query = "SELECT * FROM messages WHERE ID = '$id'";
        $result = mysql_query($query);
        return mysql_fetch_assoc($result);
    }
    //searchOld not in use
    public function searchOld($from_date, $todate,  $subject, $start, $end){
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM messages ";
        if(($from_date)!="" || ($todate)!="" || ($subject)!="" )
        {
            $query .= " WHERE ";
        }
        $andFlag = false;

        if(($from_date)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE >= '".$from_date."'";
            $andFlag = true;
        }

        if(($todate)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE <= '".$todate."'";
            $andFlag = true;
        }

        if(($subject)!=""){
            $query .= ($andFlag)?" AND ":" ";
            $query .= " SUBJECT LIKE '%".$subject."%'";
        }

        $query .= " ORDER BY MSG_DATE ASC LIMIT $start,$end";
        $result = mysql_query($query);

        //getting total numer or reords that matched against search
        $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
        $this->totalMatchRecords = $totalRecords['total'];
        return $result;
    }

    //search in use
    public function search($from,$to,$from_date, $todate, $subject){
        $query = "SELECT *  FROM messages ";
        $andFlag = false;

        if($from!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " FROM_USER_ID = '".$from."'";
            $andFlag = true;
        }
        if($to!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER_ID = '".$to."'";
            $andFlag = true;
        }
        if($this->msg_status!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_STATUS = '".$this->msg_status."'";
            $andFlag = true;
        }
        if($from_date != ""){
            $from_date = date('Y-m-d 00:00:00',strtotime($from_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE >= '".$from_date."'";
            $andFlag = true;
        }
        if($todate != ""){
            $todate = date('Y-m-d 23:59:59',strtotime($todate));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE <= '".$todate."'";
            $andFlag = true;
        }

        if($subject!=""){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " SUBJECT LIKE '%".$subject."%'";
        }

        if($this->unread){
        	$query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" OR ":" ";
            $query .= " MSG_STATUS = 'N'";
            $andFlag = true;
        }
        $query .= " ORDER BY MSG_DATE ASC ";
        $result = mysql_query($query);
        return $result;
    }

    public function get_counting_todays($user_id,$today){
        $query = "SELECT COUNT(*) AS TOTAL  FROM messages ";
        $andFlag = false;

        if($user_id!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER_ID = '".$user_id."'";
            $andFlag = true;
        }
        if($today != ""){
            $today = date('Y-m-d',strtotime($today));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE = '".$today."'";
            $andFlag = true;
        }

        $query .= " AND MSG_STATUS = 'N' ORDER BY ID ASC ";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row['TOTAL'];
    }
    public function get_message_count_specific($from,$to,$from_date,$to_date,$status){
        $query = "SELECT COUNT(*) AS TOTAL  FROM messages ";
        $andFlag = false;

        if($from!=""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " FROM_USER_ID = '".$from."'";
            $andFlag = true;
        }
        if($to != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " TO_USER_ID = '".$to."'";
            $andFlag = true;
        }
        if($from_date != ""){
            $from_date = date('Y-m-d 00:00:00',strtotime($from_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE >= '".$from_date."'";
            $andFlag = true;
        }
        if($to_date != ""){
            $to_date = date('Y-m-d 23:59:59',strtotime($to_date));
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_DATE <= '".$to_date."'";
            $andFlag = true;
        }
        if($status != ""){
            $query .= ($andFlag)?"":" WHERE ";
            $query .= ($andFlag)?" AND ":" ";
            $query .= " MSG_STATUS = '".$status."' ";
            $andFlag = true;
        }
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return ($row['TOTAL']=='')?"0":$row['TOTAL'];
    }
}
?>
