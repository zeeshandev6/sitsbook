<?php
	class ledgerReport{
		public $voucherId;
		public $jVoucherNum;
		public $voucherDate;
		public $reference;
		public $status;
		public $voucherType;
		public $accountCode;
		public $accountTitle;
		public $narration;
		public $transactionType;
		public $amount;
		public $fromDate;
		public $toDate;
		public $reference_date;

		public $po_number;

		public function getAccountsList(){
			$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) = 10 ORDER BY `ACC_TITLE`";
			$records = mysql_query($query);
			return $records;
		}
		public function getVoucherIdListByAccCode($accCode){
			$query = "SELECT `VOUCHER_ID` FROM `voucher_details` WHERE `ACCOUNT_CODE` = '$accCode'";
			$records = mysql_query($query);
			return $records;
		}
		public function getVoucherDetailListByDateRange($fromDate,$toDate,$accCode){
			$query = "SELECT voucher_details.*,voucher.* FROM `voucher_details`
					  INNER JOIN `voucher` ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher_details.ACCOUNT_CODE = '$accCode' AND REVERSAL_ID = 0 ";
			if($fromDate!==""){
				$query .= " AND voucher.VOUCHER_DATE >= '".$fromDate."'";
			}
			if($toDate!==""){
				$query .= " AND voucher.VOUCHER_DATE <= '".$toDate."'";
			}

			if($this->po_number!=""){
				$query .= " AND voucher.PO_NO = '".$this->po_number."'";
			}

			$query .= " ORDER BY voucher_details.VOUCHER_DATE,voucher_details.COUNTER ASC";
			$records = mysql_query($query);
			return $records;
		}
		public function getVoucherDetailListByPO(){
			$query 		= " SELECT voucher_details.*,voucher.* FROM `voucher_details`
							  		INNER JOIN `voucher` ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
							  		WHERE  REVERSAL_ID = 0 AND voucher.PO_NO = '".$this->po_number."'
										AND voucher_details.VOUCHER_ID NOT IN (SELECT VOUCHER_ID FROM lot_details WHERE LOT_NO = '".$this->po_number."')  ";
			if($this->transactionType!=''){
				$query .= " AND voucher_details.TRANSACTION_TYPE = '$this->transactionType' ";
			}
			$query   .= " ORDER BY voucher_details.VOUCHER_DATE,voucher_details.VOUCH_DETAIL_ID ASC ";
			$records  = mysql_query($query);
			return $records;
		}
		public function getVoucherDebitSumByPO(){
			$query = "SELECT SUM(voucher_details.AMOUNT) AS TOTAL_DEBIT FROM `voucher_details`
					  		INNER JOIN `voucher` ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  		WHERE  REVERSAL_ID = 0 AND voucher.PO_NO = '".$this->po_number."' ";
			$query .= " AND voucher_details.TRANSACTION_TYPE = 'Dr' ";
			$query .= " ORDER BY voucher_details.VOUCHER_DATE,voucher_details.VOUCH_DETAIL_ID ASC";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_assoc($records);
				return $row['TOTAL_DEBIT'];
			}else{
				return 0;
			}
		}
		public function getVoucherDetailListByDateRangeMultiCodes($fromDate,$toDate,$accCodes){
			$query = "SELECT voucher_details.*,voucher.* FROM `voucher_details`
					  INNER JOIN `voucher` ON voucher_details.VOUCHER_ID = voucher.VOUCHER_ID
					  WHERE voucher_details.ACCOUNT_CODE IN ($accCodes) ";
			if($fromDate!==""){
				$query .= " AND voucher.VOUCHER_DATE >= '".$fromDate."'";
			}
			if($toDate!==""){
				$query .= " AND voucher.VOUCHER_DATE <= '".$toDate."'";
			}
			$query .= " ORDER BY voucher_details.VOUCHER_DATE,voucher_details.VOUCH_DETAIL_ID ASC";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($voucherId){
			$query = "SELECT * FROM `voucher` WHERE `VOUCHER_ID` = $voucherId AND CR_NO = '0' AND CP_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function getListExcluding($voucherId){
			$query = "SELECT * FROM `ledger` WHERE `LEDGER_NO` !=$voucherId AND CR_NO = '0' AND CP_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function getVoucherDetailList($jvId){
			$query = "SELECT * FROM `voucher_details` WHERE `VOUCHER_ID`= $jvId";
			$records = mysql_query($query);
			return $records;
		}
		public function updateVoucher($jvId){
			$query = "UPDATE `voucher` SET `VOUCHER_DATE`='$this->voucherDate',
										   `REFERENCE`='$this->reference',
										   `REF_DATE`='$this->reference_date' WHERE `VOUCHER_ID` = $jvId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function deleteDetails($jvid){
			$query = "DELETE FROM `voucher_details` WHERE `VOUCH_DETAIL_ID` = $jvid";
			$deleted = mysql_query($query);
			return mysql_affected_rows();
		}
		public function checkInherited(){
			$query = "";
		}
        public function is_reversal_vouhcer($voucher_id){
			$query  = "SELECT COUNT(*) AS RECS FROM voucher WHERE REVERSAL_ID = '$voucher_id' ";
			$result = mysql_query($query);
			$rows   = mysql_fetch_assoc($result);
			return (int)$rows['RECS'];
		}
		public function getLedgerOpeningBalance($endDate,$accCode){
			 $query = "SELECT BALANCE
					  FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
		public function getLastBalance($endDate,$accCode){
			$query = "SELECT BALANCE
					  FROM voucher_details WHERE VOUCHER_DATE <= DATE('".$endDate."') AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}

		public function getInvoiceId($voucher_id){
			$query = "SELECT ID FROM invoice WHERE VOUCHER_ID = '$voucher_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return (int)$row['ID'];
			}else{
				return 0;
			}
		}
		public function getSaleId($voucher_id){
			$query = "SELECT ID FROM sale WHERE VOUCHER_ID = '$voucher_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['ID'];
			}else{
				return 0;
			}
		}
    public function getPurchaseId($voucher_id){
			$query = "SELECT ID FROM `purchase` WHERE VOUCHER_ID = '$voucher_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['ID'];
			}else{
				return 0;
			}
		}

		public function getInvoiceOpeningBalance($accCode,$outward_date,$voucher_id){
			$query = "SELECT BALANCE FROM voucher_details  WHERE `ACCOUNT_CODE` = '$accCode'
					  AND VOUCHER_DATE <= DATE('".$outward_date."') AND VOUCHER_ID < $voucher_id ORDER BY VOUCHER_ID DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
	}
?>
