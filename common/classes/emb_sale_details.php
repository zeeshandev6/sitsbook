<?php
	class EmbroiderySaleDetails{
		public $sale_id;
		public $machine_id;
		public $machine_rate;
		public $design_no;
		public $stitches;
		public $unit_price;
		public $total_amount;
		public $qty_length;
		public $final_amount;

		public function getList($sale_id){
			$query = "SELECT * FROM emb_sale_details WHERE SALE_ID = $sale_id ";
			return mysql_query($query);
		}
		public function getMachineIdArrayPerBill($sale_id){
			$query = "SELECT MACHINE_ID FROM emb_sale_details WHERE SALE_ID = $sale_id ";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['MACHINE_ID'];
				}
			}
			return $returnArray;
		}
		public function getStitchesPerBill($sale_id){
			$query = "SELECT SUM(STITCHES) AS TOTAL_STITCHES FROM emb_sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_STITCHES'] == '')?0:$row['TOTAL_STITCHES'];
			}else{
				return 0;
			}
		}
		public function getInventoryAmountSum($sale_id){
			$query = "SELECT SUM(FINAL_AMOUNT) AS AMOUNT FROM emb_sale_details WHERE SALE_ID = $sale_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}

		public function save(){
			$query = "INSERT INTO `emb_sale_details`(`SALE_ID`,
																							 `MACHINE_ID`,
																							 `MACHINE_RATE`,
																							 `DESIGN_NO`,
																							 `STITCHES`,
																							 `UNIT_PRICE`,
																							 `TOTAL_AMOUNT`,
																						 	 `QTY_LENGTH`,
																							 `FINAL_AMOUNT`
																						 )
																		  	VALUES (
																								'$this->sale_id',
																								'$this->machine_id',
																								'$this->machine_rate',
																								'$this->design_no',
																								'$this->stitches',
																								'$this->unit_price',
																								'$this->total_amount',
																								'$this->qty_length',
																								'$this->final_amount'
																				)";
				mysql_query($query);
				return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `emb_sale_details` SET `MACHINE_ID`   = '$this->machine_id',
																							`MACHINE_RATE` = '$this->machine_rate',
																							`DESIGN_NO`    = '$this->design_no',
																							`STITCHES`     = '$this->stitches',
																							`UNIT_PRICE`   = '$this->unit_price',
																							`TOTAL_AMOUNT` = '$this->total_amount',
																							`QTY_LENGTH` 	 = '$this->qty_length',
																							`FINAL_AMOUNT` = '$this->final_amount'  WHERE ID = '$id' ";
				return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM `emb_sale_details` WHERE ID = $id";
			$success = mysql_query($query);
			return $success;
		}
		public function deleteCompleteBill($id){
			$query = "DELETE FROM `emb_sale_details` WHERE `SALE_ID` = $id";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
