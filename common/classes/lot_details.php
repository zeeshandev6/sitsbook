<?php
  class LotDetails{
    public $lot_no;
    public $title;
    public $start_date;
    public $end_date;
    public $lot_status;
    public $stages;
    public $description;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;

    public $lot_status_not;
    public $found_records;

    public function getList(){
      $query = "SELECT * FROM `lot_details` ORDER BY ID DESC ";
      return mysql_query($query);
    }

    public function getListById($id){
      $query = "SELECT * FROM `lot_details` WHERE ID = $id ";
      return mysql_query($query);
    }

    public function getIdByLotNumber($lot_no){
      $query = "SELECT ID FROM `lot_details` WHERE LOT_NO = '$lot_no' ";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['ID'];
      }else{
        return 0;
      }
    }
    public function check_in_use($lot_no){
      $query1 = "SELECT LOT_NO  FROM fabric_contracts     WHERE LOT_NO = '$lot_no' ";
      $result1 = mysql_num_rows(mysql_query($query1));
      $query2 = "SELECT LOT_NO  FROM processing_contracts WHERE LOT_NO = '$lot_no' ";
      $result2 = mysql_num_rows(mysql_query($query2));
      $query3 = "SELECT LOT_NO  FROM packing_contracts    WHERE LOT_NO = '$lot_no' ";
      $result3 = mysql_num_rows(mysql_query($query3));
      $query4 = "SELECT LOT_NO  FROM embroidery_contracts WHERE LOT_NO = '$lot_no' ";
      $result4 = mysql_num_rows(mysql_query($query4));

      return $result1+$result2+$result3+$result4;
    }

    public function getListByIdScid($accid, $scid){
      $query = "SELECT * FROM `lot_details` WHERE USER_ID = '$accid' AND LOT_NO = '$scid'";
      return mysql_query($query);
    }

    public function save(){
      $query = "INSERT INTO `lot_details`(
        LOT_NO,
        TITLE,
        START_DATE,
        END_DATE,
        LOT_STATUS,
        STAGES,
        DESCRIPTION
      )
      VALUES(
        '$this->lot_no',
        '$this->title',
        '$this->start_date',
        '$this->end_date',
        '$this->lot_status',
        '$this->stages',
        '$this->description'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }

    public function update($id){
      $query = "UPDATE `lot_details` SET
      `LOT_NO`       = '$this->lot_no',
      `TITLE`        = '$this->title',
      `START_DATE`   = '$this->start_date',
      `END_DATE`     = '$this->end_date',
      `LOT_STATUS`   = '$this->lot_status',
      `STAGES`       = '$this->stages',
      `DESCRIPTION`  = '$this->description' WHERE ID = '$id'";
      return mysql_query($query);
    }

    public function delete($id){
      $query = "DELETE FROM `lot_details` WHERE ID = $id ";
      return mysql_query($query);
    }

    public function search($start,$pagelimit){
      $query = "SELECT SQL_CALC_FOUND_ROWS lot_details.* FROM lot_details
      ";
      $andFlag = false;

      if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `START_DATE` >= '".$this->from_date."'" ;
				$andFlag = true;
			}
			if($this->to_date!=""){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `START_DATE` <= '".$this->to_date."'";
				$andFlag = true;
			}
      if($this->title!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `TITLE` LIKE '".$this->title."%' ";
				$andFlag = true;
			}
      if($this->lot_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_NO` = '".$this->lot_no."' ";
				$andFlag = true;
			}
      if($this->lot_status!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_STATUS` = '".$this->lot_status."' ";
				$andFlag = true;
			}
      if($this->lot_status_not!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_STATUS` != '".$this->lot_status_not."' ";
				$andFlag = true;
			}
      if($this->stages!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `STAGES` IN ($this->stages)  ";
				$andFlag = true;
			}

      $query .= " ORDER BY lot_details.ID DESC LIMIT $start,$pagelimit ";
      $result = mysql_query($query);

      //getting total numer or reords that matched against search
      $totalRecords = (mysql_fetch_assoc(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
      $this->found_records = $totalRecords['total'];

      return $result;
    }
    public function report(){
      $query = "SELECT * FROM lot_details
      ";
      $andFlag = false;

      if($this->from_date!=""){
				$this->from_date = date('Y-m-d',strtotime($this->from_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `START_DATE` >= '".$this->from_date."'" ;
				$andFlag = true;
			}
			if($this->to_date!=""){
				$this->to_date = date('Y-m-d',strtotime($this->to_date));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `START_DATE` <= '".$this->to_date."'";
				$andFlag = true;
			}
      if($this->title!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `TITLE` LIKE '".$this->title."%' ";
				$andFlag = true;
			}
      if($this->lot_no!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_NO` = '".$this->lot_no."' ";
				$andFlag = true;
			}
      if($this->lot_status!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_STATUS` = '".$this->lot_status."' ";
				$andFlag = true;
			}
      if($this->lot_status_not!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `LOT_STATUS` != '".$this->lot_status_not."' ";
				$andFlag = true;
			}
      if($this->stages!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `STAGES` IN ($this->stages)  ";
				$andFlag = true;
			}

      $query .= " ORDER BY lot_details.ID DESC ";
      $result = mysql_query($query);


      return $result;
    }
    public function getDetail($lot_id){
      $query = "SELECT * FROM `lot_details` WHERE ID = '$lot_id' ";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row;
      }else{
        return 0;
      }
    }
    public function getTotalCostByLotNumber($lot_number){
      $query = "(SELECT VOUCHER_ID FROM embroidery_contracts WHERE LOT_NO = '$lot_number' )
                UNION
                (SELECT VOUCHER_ID FROM fabric_contracts WHERE LOT_NO = '$lot_number')
                UNION
                (SELECT VOUCHER_ID FROM packing_contracts WHERE LOT_NO = '$lot_number')
                UNION
                (SELECT VOUCHER_ID FROM processing_contracts WHERE LOT_NO = '$lot_number') ";
      $result = mysql_query($query);
      $voucher_id_arr = array();
      if(mysql_num_rows($result)){
        while($row = mysql_fetch_assoc($result)){
          $voucher_id_arr[] = $row['VOUCHER_ID'];
        }
      }
      $voucher_id_arr = array_unique($voucher_id_arr);
      $total_cost = 0;
      foreach ($voucher_id_arr as $key => $voucher_id) {
        $cq   = " SELECT SUM(AMOUNT) AS TOTAL_COST FROM voucher_details WHERE VOUCHER_ID = '$voucher_id' AND ACCOUNT_CODE = '0101060003' AND TRANSACTION_TYPE = 'Dr' ";
        $cost = mysql_fetch_assoc(mysql_query($cq));
        $total_cost += (float)$cost['TOTAL_COST'];
      }
      return $total_cost;
    }
    public function changeStatus($contract_id,$status){
      $query = "UPDATE `lot_details` SET LOT_STATUS = '$status' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function insertVoucherId($contract_id,$voucher_id){
      $query = "UPDATE `lot_details` SET VOUCHER_ID = '$voucher_id' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function get_voucher_id($contract_id){
      $query = "SELECT VOUCHER_ID FROM `lot_details` WHERE ID = '$contract_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
