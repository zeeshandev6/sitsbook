<?php
	class stockReports{
		public function getInventoryList($partyCode,$itemCode,$fromDate,$toDate){
			$query = "SELECT inventory.*,inventory_details.* FROM inventory 
			INNER JOIN inventory_details ON inventory.INV_ID = inventory_details.INV_ID WHERE";
			$andFlag = false;
			if($partyCode!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " inventory.SUPP_ACC_CODE = '$partyCode' ";
				$andFlag = true;
			}
			if($itemCode!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " inventory_details.ITEM_ID = '$itemCode' ";
				$andFlag = true;
			}
			if($fromDate!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) <= DATE('".$fromDate."') ";
				$andFlag = true;
			}
		}
		public function getInventoryListByItem($itemAccCode,$fromDate,$toDate,$shedId){
			$query = "SELECT issue_item.*,issue_item_details.* FROM issue_item_details 
					  INNER JOIN issue_item ON issue_item.ID = issue_item_details.ISSUE_ID 
					  WHERE ";
			$andFlag = false;
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item.ISSUE_DATE >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($itemAccCode!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_details.ITEM_ID = '$itemAccCode' ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item.ISSUE_DATE <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			$query .= " AND issue_item.SHED_ID = $shedId GROUP BY issue_item_details.ITEM_ID";
			return mysql_query($query);
		}
		public function getInventoryListItemWise($fromDate,$toDate){
			$query = "SELECT inventory.*,inventory_details.* FROM inventory_details 
					  INNER JOIN inventory ON inventory.INV_ID = inventory_details.INV_ID 
					  WHERE ";
			$andFlag = false;
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			return mysql_query($query);
		}
		
		public function getInventoryListShedWise($shedId,$fromDate,$toDate){
			$query = "SELECT issue_item.*,issue_item_details.* FROM issue_item_details 
					  INNER JOIN issue_item ON issue_item.ID = issue_item_details.ISSUE_ID  
					  WHERE ";
			$andFlag = false;
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item.ISSUE_DATE >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item.ISSUE_DATE <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			$query .= " AND issue_item.SHED_ID = $shedId ";
			$query .= " GROUP BY  issue_item_details.ITEM_ID ";
			$result = mysql_query($query);
			echo mysql_error();
			return $result;
		}
		
		
		public function getItemOpeningStock($itemCode,$fromDate){
			//getItemStockPurchased till fromDate
			$stockPurchaseQuery = "SELECT inventory_details.QTY_RECEIPT FROM inventory_details 
									INNER JOIN inventory ON inventory.INV_ID = inventory_details.INV_ID 
									WHERE inventory_details.ITEM_ID = '$itemCode' AND 
									DATE(inventory.RECEIPT_DATE) < DATE('".$fromDate."')";
			$stockPurchasedResource = mysql_query($stockPurchaseQuery);
			$stockPurchased = 0;
			while($row = mysql_fetch_array($stockPurchasedResource)){
				$stockPurchased += $row['QTY_RECEIPT'];
			}
			//getItemStockIssued till fromDate
			$stockIssueQuery = "SELECT issue_item_details.QTY_ISSUED FROM issue_item_details 
								INNER JOIN issue_item ON issue_item.ID = issue_item_details.ISSUE_ID 
								WHERE issue_item_details.ITEM_ID = '$itemCode' AND 
								DATE(issue_item.ISSUE_DATE) < DATE('".$fromDate."')";
			$stockIssuedResource = mysql_query($stockIssueQuery);
			$stockIssued = 0;
			while($row = mysql_fetch_array($stockIssuedResource)){
				$stockIssued += $row['QTY_ISSUED'];
			}
			//Calculate Opening Stock
			$openingStock = $stockPurchased - $stockIssued;
			return $openingStock;
		}
		
		
		public function getOpeningStockByShed($itemCode,$fromDate,$shed_Id){
			$query = "SELECT `REM_QUANTITY` FROM `shed_stock` 
					  WHERE `ITEM_ID` = '$itemCode' AND 
					  DATE(STOCK_TAKING_DATE) < DATE('".$fromDate."') AND 
					  `SHED_ID` = $shed_Id";
			$resource = mysql_query($query);
			if(mysql_num_rows($resource)){
				$quantity = mysql_fetch_array($resource);
				$quantity = $quantity['REM_QUANTITY'];
			}else{
				$quantity = 0;
			}
			return $quantity;
		}
		public function getClosingStockByShed($itemCode,$toDate,$shed_Id){
			$query = "SELECT `REM_QUANTITY` FROM `shed_stock` 
					  WHERE `ITEM_ID` = '$itemCode' AND 
					  DATE(STOCK_TAKING_DATE) >= DATE('".$toDate."') AND 
					  `SHED_ID` = $shed_Id";
			$resource = mysql_query($query);
			if(mysql_num_rows($resource)){
				$quantity = mysql_fetch_array($resource);
				$quantity = $quantity['REM_QUANTITY'];
			}else{
				$quantity = 0;
			}
			return $quantity;
		}
		
		public function getItemStockPuchased($itemCode,$fromDate,$toDate){
			$query = "SELECT inventory_details.QTY_RECEIPT FROM inventory 
					  INNER JOIN inventory_details ON inventory_details.INV_ID = inventory.INV_ID 
					  WHERE ";
			$andFlag = false;
			if($itemCode!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " inventory_details.ITEM_ID = '$itemCode' ";
				$andFlag = true;
			}
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(inventory.RECEIPT_DATE) <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			$stockPurchased = 0;
			$stockQuery = mysql_query($query);
			while($row = mysql_fetch_array($stockQuery)){
				$stockPurchased += $row['QTY_RECEIPT'];
			}
			return $stockPurchased;
		}
		
		
		public function getItemStockIssued($itemCode,$fromDate,$toDate){
			$query = "SELECT issue_item_details.QTY_ISSUED FROM issue_item 
					  INNER JOIN issue_item_details ON issue_item_details.ISSUE_ID = issue_item.ID 
					  WHERE ";
			$andFlag = false;
			if($itemCode!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_details.ITEM_ID = '$itemCode' ";
				$andFlag = true;
			}
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			$stockIssued = 0;
			$stockQuery = mysql_query($query);
			while($row = mysql_fetch_array($stockQuery)){
				$stockIssued += $row['QTY_ISSUED'];
			}
			return $stockIssued;
		}
		
		public function getItemStockIssuedToShed($itemCode,$fromDate,$toDate,$shedId){
			$query = "SELECT issue_item_details.QTY_ISSUED FROM issue_item 
					  INNER JOIN issue_item_details ON issue_item_details.ISSUE_ID = issue_item.ID 
					  WHERE ";
			$andFlag = false;
			if($itemCode!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item_details.ITEM_ID = '$itemCode' ";
				$andFlag = true;
			}
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			if($shedId!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " issue_item.SHED_ID = $shedId ";
				$andFlag = true;
			}
			$stockIssued = 0;
			$stockQuery = mysql_query($query);
			while($row = mysql_fetch_array($stockQuery)){
				$stockIssued += $row['QTY_ISSUED'];
			}
			return $stockIssued;
		}
		
		public function getLatestPriceOfItem($itemCode){
			$query = "SELECT `UNIT_PRICE` FROM `inventory_details` 
					  WHERE `ITEM_ID` = '$itemCode' ORDER BY `INV_DETAIL_ID` DESC LIMIT 1";
			$price = mysql_fetch_array(mysql_query($query));
			return $price['UNIT_PRICE'];
		}
		public function getSupplierList(){
			$query = "SELECT * FROM `suppliers`";
			return mysql_query($query);
		}
		public function getItemList(){
			$query = "SELECT * FROM `items`";
			return mysql_query($query);
		}
		public function getAccountTitle($accCode){
			$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '$accCode'";
			$title = mysql_fetch_array(mysql_query($query));
			return $title['ACC_TITLE'];
		}
		public function getShedTitle($shedId){
			$query = "SELECT `SHED_TITLE` FROM `shed` WHERE `SHED_ID` = $shedId";
			$title = mysql_fetch_array(mysql_query($query));
			return $title['SHED_TITLE'];
		}
		
		public function getShedList($fromDate,$toDate){
			$query = "SELECT issue_item.SHED_ID FROM issue_item 
					  INNER JOIN issue_item_details ON issue_item.ID = issue_item_details.ISSUE_ID  
					  WHERE ";
			$andFlag = false;
			if($fromDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) >= DATE('".$fromDate."') ";
				$andFlag = true;
			}
			if($toDate!==''){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(issue_item.ISSUE_DATE) <= DATE('".$toDate."') ";
				$andFlag = true;
			}
			$query .= " GROUP BY `SHED_ID` ";
			return mysql_query($query);
		}
		
	}
?>