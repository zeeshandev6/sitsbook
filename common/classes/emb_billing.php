<?php
	class EmbBilling{

		public $outwd_id;
		public $outwd_date;
		public $billNum;
		public $cloth;
		public $yarn;
		public $transport;
		public $gate_pass_number;
		public $customerAccCode;
		public $customerAccTitle;
		public $third_party_code;

		public $fromDate;
		public $toDate;

		public $outwd_detail_id;
		public $outward_lot_id;
		public $product_id;
		public $lotNum;
		public $quality;
		public $measure;
		public $length;
		public $billing_type;
		public $stitches;
		public $total_laces;
		public $embRate;
		public $embAmount;
		public $stitchRate;
		public $stitchAmount;
		public $stitchAccount;
		public $designNum;
		public $machineNum;
		public $gatePassNumber;
		public $thanOs;
		public $remarks;

		public $found_records;

		public function getList(){
			$query = "SELECT * FROM `emb_billing` WHERE VOUCHER_ID != 0";
			$records = mysql_query($query);
			return $records;
		}
		public function getMonthList($dayt){
			$thisMonthStart = date('Y-m-01',strtotime($dayt));
			$query = "SELECT * FROM `emb_billing` WHERE VOUCHER_ID != 0 AND OUTWD_DATE <= DATE('".$dayt."') AND OUTWD_DATE >= DATE('".$thisMonthStart."')";
			$records = mysql_query($query);
			return $records;
		}
		public function getListByDate($date){
			$query = "SELECT * FROM `emb_billing` WHERE OUTWD_DATE = DATE('".$date."') AND BILL_NO != 0";
			$records = mysql_query($query);
			return $records;
		}


		public function getUnpostedList(){
			$query = "SELECT * FROM `emb_billing` WHERE   `VOUCHER_ID` = 0";
			$records = mysql_query($query);
			return $records;
		}
		public function checkClaimString($outwd_id){
			$query = "SELECT CLAIM_ID FROM emb_billing WHERE OUTWD_ID = '$outwd_id' AND CLAIM_ID  != ''";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return 'Y';
			}else{
				return 'N';
			}
		}
		public function getPaged($start,$total){
			$query 	 = "SELECT SQL_CALC_FOUND_ROWS * FROM `emb_billing` JOIN emb_billing_details ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID  ORDER BY emb_billing.OUTWD_DATE ASC, emb_billing.BILL_NO ASC LIMIT $start,$total";
			$records = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];
			return $records;
		}
		public function getSpecificRecord($id){
			$query = "SELECT * FROM `emb_billing` WHERE `OUTWD_ID` = $id";
			$record = mysql_query($query);
			return $record;
		}
		public function getSpecificCustomerOutward($id){
			$query = "SELECT * FROM `emb_billing` WHERE `OUTWD_ID` = $id AND  LOT_STATUS = 'C'";
			$record = mysql_query($query);
			return $record;
		}
		public function getOutwardDate($id){
			$query = "SELECT `OUTWD_DATE` FROM `emb_billing` WHERE `OUTWD_ID` = $id AND LOT_STATUS = 'C'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['OUTWD_DATE'];
			}else{
				return '';
			}
			return $record;
		}
		public function getLotDate($id){
			$query = "SELECT emb_billing.LOT_DATE FROM `emb_billing` INNER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID  WHERE emb_billing_details.OUTWD_DETL_ID = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['LOT_DATE'];
			}else{
				return '';
			}
			return $record;
		}
		public function getBillNumber($id){
			$query = "SELECT `BILL_NO` FROM `emb_billing` WHERE `OUTWD_ID` = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['BILL_NO'];
			}else{
				return 0;
			}
		}
		public function countRows(){
			$query = "SELECT count(OUTWD_ID) FROM emb_billing";
			$records = mysql_query($query);
			return $records;
		}

		public function getAmountSum($outwd_id){
			$query = "SELECT SUM(EMB_AMOUNT+STITCH_AMOUNT) AS AMOUNT FROM emb_billing_details WHERE OUTWD_ID = '$outwd_id' ";
			$record =mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}

		public function getSummaryById($outwd_id){
			$query = "SELECT *,PRODUCT_ID,LOT_NO,MEASURE_ID
							 QUALITY,SUM(MEASURE_LENGTH) AS LENGTH_TOTAL,
							 EMB_RATE,SUM(EMB_AMOUNT) AS TOTAL_AMOUNT,
							 DESIGN_CODE,MACHINE_ID,GP_NO
					  FROM emb_billing_details WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}

		public function getProductList(){
			$query = "SELECT * FROM `products` ORDER BY `PROD_CODE`";
			return mysql_query($query);
		}
		public function genLastBillNum(){
			$query = "SELECT MAX(BILL_NO) as maxBillNum FROM `emb_billing` WHERE LOT_STATUS = 'C'";
			$query = mysql_query($query);
			if(mysql_num_rows($query)){
				$lastBillNum = mysql_fetch_array($query);
				$billNum = $lastBillNum['maxBillNum'];
				$billNum++;
			}else{
				$billNum = 1;
			}
			return $billNum;
		}
		public function getVoucherId($outwd_id){
			$query = "SELECT VOUCHER_ID FROM emb_billing WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return '';
			}
		}
		public function getRecordIdByVoucherId($voucher_id){
			$query = "SELECT OUTWD_ID FROM emb_billing WHERE VOUCHER_ID = '$voucher_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['OUTWD_ID'];
			}else{
				return '';
			}
		}
		public function save(){
			$query = "INSERT INTO `emb_billing` (`OUTWD_DATE`, `BILL_NO`, `GP_NO`,`CLOTH`, `YARN`, `TRANSPORT`,`CUST_ACC_CODE`, `CUST_ACC_TITLE`,`THIRD_PARTY_CODE`,`LOT_STATUS`)
								    VALUES ('$this->outwd_date','$this->billNum', '$this->gate_pass_number','$this->cloth', '$this->yarn', '$this->transport', '$this->customerAccCode','$this->customerAccTitle','$this->third_party_code','C')";
			$inserted = mysql_query($query);
			return $inserted;
		}
		public function update($id){
			$query = "UPDATE `emb_billing` SET
							 `OUTWD_DATE` 			= '$this->outwd_date',
							 `BILL_NO`					= '$this->billNum',
							 `GP_NO`						= '$this->gate_pass_number',
							 `CLOTH`						=	'$this->cloth',
							 `YARN`							=	'$this->yarn',
							 `TRANSPORT` 				= '$this->transport',
							 `CUST_ACC_CODE`		=	'$this->customerAccCode',
							 `CUST_ACC_TITLE`		=	'$this->customerAccTitle',
							 `THIRD_PARTY_CODE` = '$this->third_party_code' WHERE `OUTWD_ID`= '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function delete($outwardId){
			$query_outward 				= "DELETE FROM `emb_billing` WHERE `OUTWD_ID` =$outwardId";
			$query_inward_detail 	= "DELETE FROM `emb_billing_details` WHERE `OUTWD_ID` =$outwardId";
			mysql_query($query_outward);
			$effect = mysql_affected_rows();
			mysql_query($query_inward_detail);
			return $effect;
		}
		//outward Details

		public function getOutwardDetailListByDateRangeShedReport($fromDate,$toDate){
			$andFlag = false;
			$query = "SELECT DESIGN_CODE,MACHINE_ID,SUM(MEASURE_LENGTH) AS MEASURE_LENGTH,SUM(EMB_AMOUNT) AS EMB_AMOUNT FROM `emb_billing` ";
			if($this->machineNum!==""){
				$query .= " INNER JOIN `emb_billing_details` ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID";;
			}
			$query .= " WHERE ";
			if($fromDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) >= '$fromDate'";
				$andFlag = true;
			}
			if($toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) <= '$toDate'";
				$andFlag = true;
			}
			if($this->machineNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.MACHINE_ID = $this->machineNum";
				$andFlag = true;
			}
			$query .= "  GROUP BY MACHINE_ID ORDER BY `CUST_ACC_CODE`,`OUTWD_DATE`";

			$records = mysql_query($query);
			return $records;
		}

		public function getMachineSaleFromOutward($fromDate,$toDate,$machine_id){
			$andFlag = false;
			$query = "SELECT SUM(MEASURE_LENGTH) AS CLOTH_LENGTH,SUM(EMB_AMOUNT) AS EMB_AMOUNT FROM `emb_billing` ";
			if($this->machineNum!==""){
				$query .= " INNER JOIN `emb_billing_details` ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID";;
			}
			$query .= " WHERE ";
			if($fromDate!=""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) >= '$fromDate'";
				$andFlag = true;
			}
			if($toDate!=""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) <= '$toDate'";
				$andFlag = true;
			}
			if($machine_id != ""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.MACHINE_ID = '$machine_id' ";
				$andFlag = true;
			}
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				return mysql_fetch_array($records);
			}else{
				return NULL;
			}
		}


		public function getOutwardDetails($outwardId){
			$query = "SELECT * FROM `emb_billing_details` WHERE `OUTWD_ID` = $outwardId";
			$records = mysql_query($query);
			return $records;
		}
		public function deleteDetailsWhole($outwardId){
			$query = "DELETE FROM `emb_billing_details` WHERE `OUTWD_ID` = $outwardId";
			$result = mysql_query($query);
			return $result;
		}

		public function deleteDetailsExcept($id_group){
			$query = "DELETE FROM `emb_billing_details` WHERE `OUTWD_DETL_ID` NOT IN (".$id_group.")";
			$result = mysql_query($query);
			return $result;
		}

		public function getSpecificOutwardDetail($outwardDetailId){
			$query = "SELECT * FROM `emb_billing_details` WHERE `OUTWD_DETL_ID` = '$outwardDetailId' ";
			$records = mysql_query($query);
			return $records;
		}
		public function saveOutwardDetails(){
			$query = "INSERT INTO `emb_billing_details`(`OUTWD_ID`,
													`OUTWARD_LOT_ID`,
													`PRODUCT_ID`,
													`LOT_NO`,
													`QUALITY`,
													`MEASURE_ID`,
													`BILLING_TYPE`,
													`STITCHES`,
													`TOTAL_LACES`,
													`MEASURE_LENGTH`,
													`EMB_RATE`,
													`EMB_AMOUNT`,
													`STITCH_RATE`,
													`STITCH_AMOUNT`,
													`STITCH_ACC`,
													`DESIGN_CODE`,
													`MACHINE_ID`,
													`GP_NO`,
													`REMARKS`)
											 VALUES ( $this->outwd_id,
												 	 '$this->outward_lot_id',
													 '$this->product_id',
													 '$this->lotNum',
													 '$this->quality',
													 '$this->measure',
													 '$this->billing_type',
													 '$this->stitches',
													 '$this->total_laces',
													 '$this->length',
													 '$this->embRate',
													 '$this->embAmount',
													 '$this->stitchRate',
													 '$this->stitchAmount',
													 '$this->stitchAccount',
													 '$this->designNum',
													 '$this->machineNum',
													 '$this->gatePassNumber',
													 '$this->remarks')";
			$inserted = mysql_query($query);
			if($inserted){
				return mysql_insert_id();
			}else{
				return false;
			}
		}
		public function updateOutwardDetail($outwardDetailsId){
			$query = "UPDATE `emb_billing_details` SET
												  `PRODUCT_ID` 			= '$this->product_id',
												  `QUALITY` 				= '$this->quality',
												  `MEASURE_ID` 		= '$this->measure',
												  `MEASURE_LENGTH` 	= '$this->length',
													`BILLING_TYPE` 		= '$this->billing_type',
													`STITCHES` 				= '$this->stitches',
													`TOTAL_LACES` 		= '$this->total_laces',
												  `EMB_RATE` 				= '$this->embRate',
												  `EMB_AMOUNT` 			= '$this->embAmount',
												  `STITCH_RATE` 		= '$this->stitchRate',
												  `STITCH_AMOUNT` 	= '$this->stitchAmount',
												  `STITCH_ACC` 			= '$this->stitchAccount',
												  `DESIGN_CODE` 		= '$this->designNum',
												  `MACHINE_ID` 			= '$this->machineNum',
												  `GP_NO` 					= '$this->gatePassNumber',
												  `REMARKS` 				= '$this->remarks' WHERE `OUTWD_DETL_ID` = $outwardDetailsId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function checkDuplication($product_id){
			// not in use
			$query = "SELECT `PRODUCT_ID` FROM `emb_billing_details` WHERE `PROD_CODE` = '$product_id'";
			$existance = mysql_num_rows(mysql_query($query));
			return $existance;
		}
		public function deleteDetails($outwd_detail_id){
			$query = "DELETE FROM `emb_billing_details` WHERE `OUTWD_DETL_ID` = '$outwd_detail_id' ";
			return mysql_query($query);
		}
		public function getInwardId($account_code,$lot_no){
			$query  = "SELECT `ID` FROM `emb_inward` WHERE `ACCOUNT_CODE` = '$account_code' AND `LOT_NO` = '$lot_no' ";
			$result = mysql_query($query);
			return mysql_fetch_array($result);
		}
		public function getLotDetailsFromInward($inwdId){
			$query = "SELECT `QUALITY`,SUM(TOTAL) as T_TOTAL FROM `emb_inward_details` WHERE `INWD_ID` = $inwdId GROUP BY `QUALITY` ";
			return mysql_query($query);
		}
		public function getLotLengthFromInward($inwdId){
			$getLength = "SELECT SUM(TOTAL) as T_TOTAL FROM `emb_inward_details` WHERE `INWD_ID` = $inwdId";
			return mysql_fetch_array(mysql_query($getLength));
		}
		public function getOutwardId($customerAccCode){
			$getOutwdId = "SELECT `OUTWD_ID` FROM `emb_billing` WHERE `CUST_ACC_CODE` = '$customerAccCode' WHERE `LOT_STATUS` = 'C'";
			$owId = mysql_fetch_array(mysql_query($getOutwdId));
			return $owId['OUTWD_ID'];
		}

		public function getDetailsIds($outwd_id){
			$query = "SELECT OUTWD_DETL_ID FROM emb_billing_details WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			return $record;
		}

		public function getMeasureLength($outwd_id,$lotNum){
			$query = "SELECT `MEASURE_LENGTH` FROM `emb_billing_details` WHERE `OUTWD_ID` = '$outwd_id' AND `LOT_NO` = '$lotNum'";
			$length = mysql_query($query);
			return $length;
		}
		public function checkInherited($outwd_id){
			$query = "SELECT `OUTWD_DETL_ID` FROM `emb_billing_details` WHERE `OUTWD_ID` = '$outwd_id' ";
			$execute = mysql_query($query);
			$check = mysql_fetch_array($execute);
			if(!empty($check['OUTWD_DETL_ID'])){
				return true;
			}else{
				return false;
			}
		}
		public function checkBillDuplication($billNum,$outwd_id){
			$query = "SELECT BILL_NO FROM emb_billing WHERE BILL_NO  = '$billNum' AND OUTWD_ID != '$outwd_id' ";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function getCustomersList(){
			$query = "SELECT * FROM `customers` ORDER BY `CUST_ACC_TITLE`";
			return mysql_query($query);
		}

		public function getOutwardDetailListByDateRange($fromDate,$toDate){
			$andFlag = false;
			$query = "SELECT * FROM emb_billing ";

			$query .= " LEFT OUTER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID ";

			$query .= " WHERE ";

			if($fromDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(emb_billing.OUTWD_DATE) >= '$fromDate'";
				$andFlag = true;
			}
			if($toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(emb_billing.OUTWD_DATE) <= '$toDate'";
				$andFlag = true;
			}
			if($this->machineNum != ""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.MACHINE_ID = $this->machineNum";
				$andFlag = true;
			}
			if($this->customerAccCode != ""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing.CUST_ACC_CODE = $this->customerAccCode";
				$andFlag = true;
			}
			$query .= " ORDER BY emb_billing.OUTWD_DATE ASC, emb_billing.BILL_NO ASC";

			$records = mysql_query($query);
			return $records;
		}
		public function getOutwardDetailListByDateRangeGroupMachines($fromDate,$toDate){
			$andFlag = false;
			$query = "SELECT * FROM `emb_billing` ";
			if($this->machineNum!==""){
				$query .= " INNER JOIN `emb_billing_details` ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID";;
			}
			$query .= " WHERE ";
			if($fromDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) >= '$fromDate'";
				$andFlag = true;
			}
			if($toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) <= '$toDate'";
				$andFlag = true;
			}
			if($this->machineNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.MACHINE_ID = $this->machineNum";
				$andFlag = true;
			}
			$query .= ($andFlag)?" AND ":" ";
			$query .= " emb_billing.LOT_STATUS = 'C' ";
			$andFlag = true;

			$query .= " GROUP BY emb_billing_details.MACHINE_ID ORDER BY `CUST_ACC_CODE`,`OUTWD_DATE`";

			$records = mysql_query($query);
			return $records;
		}
		public function getOutwardDetailListIfMachine($fromDate,$toDate){
			$andFlag = false;
			$query = "SELECT * FROM `emb_billing` ";
			$query .= " WHERE ";
			if($fromDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) >= '$fromDate'";
				$andFlag = true;
			}
			if($toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " DATE(OUTWD_DATE) <= '$toDate'";
				$andFlag = true;
			}
			$query .= ($andFlag)?" AND ":" ";
			$query .= " `LOT_STATUS` = 'C' ";
			$andFlag = true;

			$query .= " ORDER BY `CUST_ACC_CODE`,`OUTWD_DATE`";

			$records = mysql_query($query);
			return $records;
		}
		public function getDetailsById($outwd_id){
			$query = "SELECT * FROM `emb_billing_details`
					  WHERE `OUTWD_ID` = '$outwd_id' ";
			return mysql_query($query);
		}
		public function getDetailsByIdIfMachine($outwd_id){
			$query = "SELECT * FROM `emb_billing_details`
					  WHERE `OUTWD_ID` = '$outwd_id' AND `MACHINE_ID` = $this->machineNum";
			return mysql_query($query);
		}

		public function getDetailsByIdIfMachineClub($outwd_id){
			$query = "SELECT DESIGN_CODE,MACHINE_ID,SUM(MEASURE_LENGTH) AS MEASURE_LENGTH,SUM(EMB_AMOUNT) AS EMB_AMOUNT FROM `emb_billing_details`
					  WHERE `OUTWD_ID` = '$outwd_id' AND `MACHINE_ID` = $this->machineNum GROUP BY MACHINE_ID";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function getSpecificOutward(){
			$andFlag = false;
			$query = "SELECT * FROM `emb_billing` INNER JOIN `emb_billing_details` ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID ";
			$query .= " WHERE ";
			if($this->fromDate!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_billing.OUTWD_DATE >= '$this->fromDate'";
				$andFlag = true;
			}
			if($this->toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing.OUTWD_DATE <= '$this->toDate'";
				$andFlag = true;
			}
			if($this->lotNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.LOT_NO = '$this->lotNum'";
				$andFlag = true;
			}
			if($this->designNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.DESIGN_CODE = '$this->designNum'";
				$andFlag = true;
			}
			if($this->customerAccCode!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing.CUST_ACC_CODE = '$this->customerAccCode'";
				$andFlag = true;
			}
			$query .= ($andFlag)?" AND ":" ";
			$query .= " emb_billing.LOT_STATUS = 'C' ";
			$andFlag = true;

			$query .= " GROUP BY emb_billing.OUTWD_ID ORDER BY `CUST_ACC_CODE`,`LOT_NO`,`BILL_NO`";
			return  mysql_query($query);
		}

		public function getSpecificOutwardIncBill(){
			$andFlag = false;
			$query = "SELECT * FROM `emb_billing` INNER JOIN `emb_billing_details` ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID ";
			$query .= " WHERE ";
			if($this->fromDate!==""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " emb_billing.OUTWD_DATE >= '$this->fromDate'";
				$andFlag = true;
			}
			if($this->toDate!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing.OUTWD_DATE <= '$this->toDate'";
				$andFlag = true;
			}
			if($this->lotNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.LOT_NO = '$this->lotNum'";
				$andFlag = true;
			}
			if($this->designNum!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing_details.DESIGN_CODE = '$this->designNum'";
				$andFlag = true;
			}
			if($this->customerAccCode!==""){
				$query .= ($andFlag)?" AND ":" ";
				$query .= " emb_billing.CUST_ACC_CODE = '$this->customerAccCode'";
				$andFlag = true;
			}
			$query .= ($andFlag)?" AND ":" ";
			$query .= " emb_billing.BILL_NO != 0 ";
			$andFlag = true;

			$query .= " ORDER BY `BILL_NO`,`LOT_NO`";
			return  mysql_query($query);
		}

		public function getSpecificDetailsById($outwd_id){
			$query = "SELECT *  FROM `emb_billing_details` WHERE `OUTWD_ID` = '$outwd_id' ";
			return mysql_query($query);
		}
		public function getPartiallySentLotLength($customer_acc_code,$lot_number){
			$query = "SELECT SUM(MEASURE_LENGTH) AS PARTIAL_LENGTH FROM emb_billing_details
								WHERE PARTIAL_STATUS = 'Y' AND OUTWD_ID IN (SELECT OUTWD_ID FROM emb_billing WHERE CUST_ACC_CODE = '$customer_acc_code') AND LOT_NO = '$lot_number'
								AND OUTWD_DETL_ID IN (SELECT OUTWD_DETL_ID FROM emb_lot_register ) ";
			$result = mysql_query($query);
			$row    = mysql_fetch_assoc($result);
			return $row['PARTIAL_LENGTH'];
		}
		public function getLengthIfPartial($outward_lot_id){
			$query  = "SELECT SUM(MEASURE_LENGTH) AS PARTIAL_LENGTH,SUM(TOTAL_LACES) AS TOTAL_PRODUCTION FROM emb_billing_details WHERE OUTWARD_LOT_ID IN  (SELECT ID FROM emb_lot_register WHERE ID = '$outward_lot_id' AND PARTIAL_STATUS = 'Y') ";
			$result = mysql_query($query);
			$row    = mysql_fetch_assoc($result);
			return $row;
		}
		public function insertVoucherId($outwardId,$voucherId){
			$query = "UPDATE `emb_billing` SET `VOUCHER_ID` = $voucherId WHERE `OUTWD_ID` = $outwardId";
			return mysql_query($query);
		}
		public function getAccountTitle($accCode){
			$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '$accCode'";
			$title = mysql_fetch_array(mysql_query($query));
			return $title['ACC_TITLE'];
		}

		/* Invoice Function */

		public function searchByBillNum($billNum){
			if($billNum!==''){
				$query = "SELECT `OUTWD_ID` FROM `emb_billing` WHERE `BILL_NO` = $billNum";
				$result = mysql_query($query);
				if(mysql_num_rows($result)){
					$id = mysql_fetch_array($result);
					$returnVal = $id['OUTWD_ID'];
				}else{
					$returnVal = null;
				}

			}else{
				$returnVal = null;
			}
			return $returnVal;
		}

		public function getInvoice($id){

			$query = "SELECT *,`emb_billing`.GP_NO AS GP_NUMBER FROM `emb_billing`
					  INNER JOIN `emb_billing_details`
					  ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.OUTWD_ID = $id";
			return mysql_query($query);
		}
		public function getCutomer($accCode){
			$query = "SELECT * FROM `customers` WHERE `CUST_ACC_CODE` = '$accCode'";
			return mysql_fetch_array(mysql_query($query));
		}

		public function getCutomerOfOutward($outwd_id){
			$query = "SELECT CUST_ACC_CODE FROM emb_billing WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['CUST_ACC_CODE'];
			}else{
				return '';
			}
		}
		public function getThanOutStanding($lotNum,$custAccCode){
			$query = "SELECT emb_inward_details.TOTAL FROM emb_inward_details
					  INNER JOIN emb_inward ON emb_inward_details.INWD_ID = emb_inward.ID
					  WHERE emb_inward.LOT_NO = '$lotNum' AND emb_inward.ACCOUNT_CODE = '$custAccCode' ";
			$resource = mysql_query($query);
			$total = mysql_fetch_array($resource);
			return $total['TOTAL'];
		}
		public function getLotQualityList($customerCode,$lotNum){
			$query = "SELECT QUALITY FROM emb_inward_details INNER JOIN emb_inward ON emb_inward.ID = emb_inward_details.INWARD_ID
					  WHERE emb_inward.ACCOUNT_CODE = '$customerCode' AND emb_inward.LOT_NO = '$lotNum' ";
			return mysql_query($query);
		}
		public function getOutwardLotQtyDelivered($custAccCode,$lotNum,$prodCode,$outwardDate){
			$query = "SELECT SUM(emb_billing_details.MEASURE_LENGTH) AS QTY_DELIVERED FROM emb_billing_details
					  INNER JOIN emb_billing ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$custAccCode' AND emb_billing_details.LOT_NO = $lotNum
					  		AND emb_billing_details.PRODUCT_ID = '$prodCode'
							AND DATE(emb_billing.OUTWD_DATE) <= DATE('".$outwardDate."')
							AND emb_billing.LOT_STATUS = 'C'";
			$result = mysql_query($query);
			$stock  = mysql_fetch_array($result);
			return $stock['QTY_DELIVERED'];
		}
		public function getOutwardLotQtyDeliveredWholeLotPrevId($custAccCode,$lotNum,$outwardDate,$outward_detail_id){
			$query = "SELECT SUM(emb_billing_details.MEASURE_LENGTH) AS QTY_DELIVERED FROM emb_billing_details
					  INNER JOIN emb_billing ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$custAccCode' AND emb_billing_details.LOT_NO = '$lotNum'  AND emb_billing_details.OUTWD_DETL_ID <= '$outward_detail_id' ";
			$stock = mysql_fetch_array(mysql_query($query));
			return $stock['QTY_DELIVERED'];
		}
		public function getOutwardLotQtyDeliveredWholeLot($custAccCode,$lotNum,$outwardDate){
			$query = "SELECT SUM(emb_billing_details.MEASURE_LENGTH) AS QTY_DELIVERED FROM emb_billing_details
					  INNER JOIN emb_billing ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$custAccCode' AND emb_billing_details.LOT_NO = $lotNum
							AND DATE(emb_billing.OUTWD_DATE) <= DATE('".$outwardDate."') ";
			$stock = mysql_fetch_array(mysql_query($query));
			return $stock['QTY_DELIVERED'];
		}

		public function getPlainThansOutward($cutomerAccCode,$lotNum){
			$query = "SELECT SUM(MEASURE_LENGTH) AS TOTAL FROM emb_billing_details
								WHERE OUTWD_ID IN (SELECT OUTWD_ID FROM emb_billing WHERE CUST_ACC_CODE = '$cutomerAccCode')
								AND OUTWD_DETL_ID NOT IN (SELECT OUTWD_DETL_ID FROM emb_lot_register WHERE CUST_ACC_CODE = '$cutomerAccCode' AND LOT_NO = '$lotNum')
								AND LOT_NO = '$lotNum' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				$row['TOTAL'] = ($row['TOTAL']=='')?0:$row['TOTAL'];
				return $row['TOTAL'];
			}else{
				return 0;
			}
		}
		public function getOutwardClothOut($cutomerAccCode,$lotNum){
			$query = "SELECT SUM(emb_billing_details.MEASURE_LENGTH) AS TOTAL FROM emb_billing
					  INNER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$cutomerAccCode'
					  AND emb_billing_details.LOT_NO = $lotNum
					  AND emb_billing.LOT_STATUS = 'C'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				$row['TOTAL'] = ($row['TOTAL']=='')?0:$row['TOTAL'];
				return $row['TOTAL'];
			}else{
				return 0;
			}
		}
		public function getDesignCodes($cutomerAccCode,$lotNum){
			$query = "SELECT emb_billing_details.DESIGN_CODE FROM  emb_billing_details
					  INNER JOIN emb_billing ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$cutomerAccCode'
					  AND emb_billing_details.LOT_NO = $lotNum
					  AND emb_billing.LOT_STATUS = 'C' GROUP BY emb_billing_details.DESIGN_CODE ";
			$records = mysql_query($query);
			$designs = "";
			if(mysql_num_rows($records)){
				while($design = mysql_fetch_array($records)){
					 $designs .= $design['DESIGN_CODE'].", ";
				}
			}else{
				$designs = "";
			}
			return $designs;
		}


		public function changeOutwardDetailParentIdAndBillStatus($outwd_id,$outward_detail_id){
			$query = "UPDATE emb_billing_details SET OUTWD_ID = '$outwd_id' , BILL_STATUS = 'Y' WHERE OUTWD_DETL_ID = $outward_detail_id ";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function changeOutwardDetailParentIdAndBillStatus2($outwd_id,$outward_detail_id){
			$query = "UPDATE emb_billing_details SET OUTWD_ID = '$outwd_id' , BILL_STATUS = 'N' WHERE OUTWD_DETL_ID = $outward_detail_id ";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function issueBillNumber($outwd_id){
			$billNum = $this->genLastBillNum();
			$query = "UPDATE emb_billing SET `BILL_NO` = $billNum WHERE OUTWD_ID = '$outwd_id' ";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function getCompletedLotsByCustomer($accCode){
			$query = "SELECT emb_billing.*,emb_billing_details.* FROM emb_billing
					  INNER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.CUST_ACC_CODE = '$accCode' AND emb_billing.LOT_STATUS = 'C'
					  AND emb_billing_details.BILL_STATUS != 'Y'";
			$record = mysql_query($query);
			return $record;
		}

		public function deleteUnRelatedOutwardEntries(){
			$query = "DELETE FROM emb_billing WHERE OUTWD_ID NOT IN (SELECT OUTWD_ID FROM emb_billing_details) AND VOUCHER_ID = 0 AND CLAIM_ID = ''";
			mysql_query($query);
		}
		public function carryLotDateToOutwardDetails($outward_detail_id){
			$lot_date = $this->getLotDate($outward_detail_id);
			$query = "UPDATE emb_billing_details SET LOT_DATE = '$lot_date' WHERE OUTWD_DETL_ID = $outward_detail_id";
			mysql_query($query);
			return mysql_affected_rows();
		}

		public function getMahcineSaleAlreadyAccounted($fromDate){
			$query = "SELECT SUM(MEASURE_LENGTH) AS CLOTH_LENGTH , SUM(EMB_AMOUNT) AS AMOUNT FROM emb_billing
			 		  INNER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.OUTWD_DATE >= DATE('".$fromDate."')
					  AND emb_billing_details.LOT_DATE < DATE('".$fromDate."')
					  AND emb_billing.BILL_NO != 0";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}

		public function get_outward_detail_id_list($outwd_id){
			$query = "SELECT OUTWD_DETL_ID FROM emb_billing_details WHERE OUTWD_ID = '$outwd_id' ";
			return mysql_query($query);
		}

		public function getMahcineSaleAlreadyFetchLoop($fromDate){
			$query = "SELECT MEASURE_LENGTH AS CLOTH_LENGTH , EMB_AMOUNT AS AMOUNT FROM emb_billing
			 		  INNER JOIN emb_billing_details ON emb_billing.OUTWD_ID = emb_billing_details.OUTWD_ID
					  WHERE emb_billing.OUTWD_DATE >= DATE('".$fromDate."')
					  AND emb_billing_details.LOT_DATE < DATE('".$fromDate."')
					  AND emb_billing.BILL_NO != 0";
			return $record = mysql_query($query);
		}
		public function insertClaimIdz($claim_id_string,$outwd_id){
			$query = "UPDATE emb_billing SET CLAIM_ID = '$claim_id_string' WHERE OUTWD_ID  = '$outwd_id' ";
		 	return mysql_query($query);
		}

		public function search($start,$total){
			$query   = "SELECT SQL_CALC_FOUND_ROWS *
									FROM emb_billing
									JOIN emb_billing_details
									ON emb_billing_details.OUTWD_ID = emb_billing.OUTWD_ID  ";
			$andFlag = false;
			if($this->fromDate != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " emb_billing.OUTWD_DATE >= DATE('".$this->fromDate."') ";
				$andFlag = true;
			}
			if($this->toDate != ''){
				$query  .= ($andFlag)?" AND ":" WHERE ";
				$query  .= " emb_billing.OUTWD_DATE <= DATE('".$this->toDate."') ";
				$andFlag = true;
			}
			if($this->customerAccCode!=''){
				$query  .= ($andFlag)?" AND ":" WHERE ";
				$query  .= " emb_billing.CUST_ACC_CODE = '".$this->customerAccCode."' ";
				$andFlag = true;
			}
			if($this->machineNum != ''){
				$query  .= ($andFlag)?" AND ":" WHERE ";
				$query  .= " emb_billing_details.MACHINE_ID = '".$this->machineNum."' ";
				$andFlag = true;
			}
			if($this->billNum != ''){
				$query  .= ($andFlag)?" AND ":" WHERE ";
				$query  .= " emb_billing.BILL_NO = '".$this->billNum."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY emb_billing.OUTWD_DATE ASC LIMIT $start,$total ";
			$result = mysql_query($query);

			$totalRecords = mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')"));
			$this->found_records = $totalRecords['total'];

			return $result;
		}

		public function getClaimIdArray($outwd_id){
			$query = "SELECT CLAIM_ID FROM `emb_billing` WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				if($row['CLAIM_ID']){
					$id_array = explode(',',$row['CLAIM_ID']);
					return $id_array;
				}else{
					return NULL;
				}
			}else{
				return NULL;
			}
		}
		public function getDesignsList($outwd_id){
			$query = "SELECT DESIGN_CODE FROM `emb_billing_details` WHERE OUTWD_ID = '$outwd_id' GROUP BY DESIGN_CODE ";
			$result = mysql_query($query);
			return $result;
		}
		public function getRecordByBillNumber($gp_no){
			$query 	= "SELECT * FROM `emb_billing` WHERE `BILL_NO` = '$gp_no' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_assoc($record);
			}else{
				return 0;
			}
		}
		public function getLastBill($report_date,$party_code){
			$query = "SELECT * FROM `emb_billing` JOIN sale ON sale.CUST_ACC_CODE = emb_billing.CUST_ACC_CODE AND sale.SALE_DATE != emb_billing.OUTWD_DATE WHERE ( sale.CUST_ACC_CODE = '$party_code' AND sale.SALE_DATE <= '$report_date' ) OR (emb_billing.CUST_ACC_CODE = '$party_code' AND emb_billing.OUTWD_DATE <= '$report_date') ORDER BY emb_billing.OUTWD_DATE DESC,sale.SALE_DATE DESC LIMIT 1";
			$row   = mysql_fetch_assoc(mysql_query($query));
			return $row;
		}
		public function getMachineIdAmountList($outwd_id){
			$query = "SELECT MACHINE_ID,EMB_AMOUNT FROM `emb_billing_details` WHERE OUTWD_ID = '$outwd_id' ";
			$record = mysql_query($query);
			return $record;
		}
	}
?>
