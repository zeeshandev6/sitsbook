<?php
	class CashPayment{
		public $voucherId;
		public $cpNum;
		public $jVoucherNum;
		public $voucherDate;
		public $reference;
		public $status;
		public $voucherType;
		public $accountCode;
		public $accountTitle;
		public $narration;
		public $transactionType;
		public $instrumentNum;
		public $amount;
		public $reference_date;
		
		public function getAccountsList(){
			$query = "SELECT * FROM `account_code` WHERE LENGTH(ACC_CODE) = 9";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($voucherId){
			$query = "SELECT * FROM `voucher` WHERE `VOUCHER_ID` = $voucherId AND CR_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function getListExcluding($voucherId){
			$query = "SELECT * FROM `ledger` WHERE `LEDGER_NO` !=$voucherId AND CR_NO = '0' AND BR_NO = '0' AND BP_NO = '0'";
			$records = mysql_query($query);
			return $records;
		}
		public function genJvNumber(){
			$query = mysql_query("SELECT MAX(VOUCHER_NO) as LAST_VOUCHER_NO FROM `voucher`");
			if(mysql_num_rows($query)){
				$jv = mysql_fetch_array($query);
				$jv = $jv['LAST_VOUCHER_NO'];
				$jv++;
				$jvNum = $jv;
			}else{
				$jvNum = 1;
			}
			return $jvNum;
		}
		public function genCpNumber(){
			$query = mysql_query("SELECT MAX(TYPE_NO) as LAST_CP_NO FROM `voucher` WHERE VOUCHER_TYPE = 'CP'");
			if(mysql_num_rows($query)){
				$cp = mysql_fetch_array($query);
				$cp = $cp['LAST_CP_NO'];
				$cp++;
				$cpNum = $cp;
			}else{
				$cpNum = 1;
			}
			return $cpNum;
		}
		public function saveVoucher(){
			$query = "INSERT INTO `voucher`(`VOUCHER_NO`, 
											`CP_NO`,
											`VOUCHER_DATE`, 
											`REFERENCE`,
											`REF_DATE`,
											`VOUCHER_STATUS`) 
									VALUES ($this->jVoucherNum,
											 $this->cpNum,
											'$this->voucherDate',
											'$this->reference',
											'$this->reference_date',
											'$this->status')";
			$saved = mysql_query($query);
			return mysql_insert_id();
		}
		public function getPaymentDetailList($jvId){
			$query = "SELECT * FROM `voucher_details` WHERE `VOUCHER_ID`= $jvId AND `TRANSACTION_TYPE` = 'Dr'";
			$records = mysql_query($query);
			return $records;
		}
		public function updateVoucher($jvId){
			$query = "UPDATE `voucher` SET `REFERENCE`='$this->reference',
										   `REF_DATE`='$this->reference_date' WHERE `VOUCHER_ID` = $jvId";
			$updated = mysql_query($query);
			return $updated;
		}
		public function saveCashPaymentDetail(){
			$qureyCashAcc = "SELECT `ACC_CODE`,`ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '0101030001'";
			$CashAcc = mysql_fetch_array(mysql_query($qureyCashAcc));
			$cashAccCode = $CashAcc['ACC_CODE'];
			$cashAccTitle = $CashAcc['ACC_TITLE'];
			$CashPaymentCreditNarration = "Cash Paid To $this->accountTitle-$this->accountCode Vide Instrument No.$this->instrumentNum";
			if($CashAcc){
				$transactionType = 'Cr';
				$newBalance = $this->getLastBalance($cashAccCode,$this->amount,$transactionType);
				$queryDebit = "INSERT INTO `voucher_details`(`VOUCHER_ID`, 
													`ACCOUNT_CODE`, 
													`ACCOUNT_TITLE`, 
													`NARRATION`, 
													`TRANSACTION_TYPE`, 
													`INSTRUMENT_NO`,
													`AMOUNT`,
													`BALANCE`)
											 VALUES ($this->voucherId,
											 		 '$cashAccCode',
													 '$cashAccTitle',
													 '$CashPaymentCreditNarration',
													 '$transactionType',
													 $this->instrumentNum,
													 $this->amount,
													 $newBalance)";
				$transactionType = 'Dr';
				$newBalance = $this->getLastBalance($this->accountCode,$this->amount,$transactionType);
				$queryCredit = "INSERT INTO `voucher_details`(`VOUCHER_ID`, 
													`ACCOUNT_CODE`, 
													`ACCOUNT_TITLE`, 
													`NARRATION`, 
													`TRANSACTION_TYPE`, 
													`INSTRUMENT_NO`,
													`AMOUNT`,
													`BALANCE`)
											 VALUES ($this->voucherId,
											 		 '$this->accountCode',
													 '$this->accountTitle',
													 '$this->narration',
													 '$transactionType',
													 $this->instrumentNum,
													 $this->amount,
													 $newBalance)";
				$insertedCredit = mysql_query($queryCredit);
				$insertedDebit = mysql_query($queryDebit);
				return $insertedCredit;
			}
		}
		public function deleteDetails($jvid){
			$queryCr = "DELETE FROM `voucher_details` WHERE `VOUCH_DETAIL_ID` = $jvid";
			$deleted = mysql_query($queryCr);
			return mysql_affected_rows();
		}
		public function checkInherited(){
			$query = "";
		}
		/* save Current Balance */
		private function getLastBalance($accCode,$newAmount,$transactionType){
			$query = "SELECT `BALANCE` FROM `voucher_details` WHERE `ACCOUNT_CODE` = '$accCode' ORDER BY `VOUCH_DETAIL_ID` DESC LIMIT 1";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$balance = mysql_fetch_array($result);
				$balance = $balance['BALANCE'];
			}else{
				$balance = 0;
			}
			$balanceType = ($balance<0)?"Cr":"Dr";
			if( $transactionType=='Dr' && $balanceType=='Dr' ){
				//add in case : amount And Balance are Debit.
				$newBalance = $newAmount + $balance;
			}elseif( $transactionType=='Cr' && $balanceType=='Cr' ){
				//add in case : amount And Balance are Credit.
				$newBalance =  $balance - $newAmount;
			}elseif( $transactionType=='Cr' && $balanceType=='Dr' ){
				//Less in case : amount is Credit And Balance is Debit.
				$newBalance = $balance - $newAmount;
			}elseif( $transactionType=='Dr' && $balanceType=='Cr' ){
				//Less in case : amount is Credit And Balance is Debit.
				$newBalance = $balance - $newAmount;
			}
			return $newBalance;
		}
	}
?>