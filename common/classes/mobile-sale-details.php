<?php
	class ScanSaleDetails{
		public $ss_id;
		public $sp_id;
		public $orig_price;
		public $discount;
		public $tax;
		public $price;
		public $sale_order_no;

		public function getList($sp_id){
			$query = "SELECT * FROM mobile_sale_details WHERE SS_ID = $sp_id";
			return mysql_query($query);
		}
		public function getListIDArray($sp_id){
			$query = "SELECT ID FROM mobile_sale_details WHERE SS_ID = '$sp_id'";
			$result= mysql_query($query);
			$new_array = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_array($result)){
					$new_array[] = $row['ID'];
				}
			}
			return $new_array;
		}
		public function getSSIDByBarcode($imei){
			$query = "SELECT mobile_sale_details.SS_ID FROM `mobile_sale_details` LEFT JOIN `mobile_purchase_details` ON mobile_purchase_details.ID = mobile_sale_details.SP_DETAIL_ID WHERE mobile_purchase_details.BARCODE = '$imei' AND mobile_sale_details.ID NOT IN  (SELECT MSDID FROM mobile_sale_return_details) ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['SS_ID'];
			}else{
				return 0;
			}
		}
		public function checkForReturnedAgainstCurrentBill($mobile_sale_detail_id){
			$query = "SELECT ID FROM `mobile_sale_return_details`  WHERE MSDID = '$mobile_sale_detail_id' ";
			$result= mysql_query($query);
			return mysql_num_rows($result);
		}
		public function getDetails($msd_id){
			$query = "SELECT * FROM `mobile_sale_details` WHERE `ID` = $msd_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function order_numbers_used($order_numbers){
			$query = "SELECT mobile_sale_details.*,mobile_sale.BILL_NO FROM `mobile_sale_details` JOIN `mobile_sale` ON mobile_sale.ID = mobile_sale_details.SS_ID WHERE `SALE_ORDER_NO` IN ($order_numbers) ";
			$record = mysql_query($query);
			return mysql_num_rows($record);
		}
		public function order_number_placed($order_number_exists){
			$query = "SELECT mobile_sale_details.*,mobile_sale.BILL_NO FROM `mobile_sale_details` JOIN `mobile_sale` ON mobile_sale.ID = mobile_sale_details.SS_ID WHERE `SALE_ORDER_NO` = $order_number_exists ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}
		}
		public function getLastSale($barcode){
			$query = " SELECT mobile_sale.*,mobile_sale_details.*,mobile_sale_details.ID as MSDID FROM mobile_sale JOIN mobile_sale_details ON mobile_sale.ID = mobile_sale_details.SS_ID  WHERE mobile_sale_details.ID IN (SELECT mobile_sale_details.ID FROM `mobile_sale_details` LEFT JOIN `mobile_purchase_details` ON mobile_purchase_details.ID = mobile_sale_details.SP_DETAIL_ID WHERE mobile_purchase_details.BARCODE = '$barcode') ORDER BY mobile_sale_details.ID DESC LIMIT 1 ";
			$record = mysql_query($query);
			echo mysql_error();
			if(mysql_num_rows($record)){
				$row = mysql_fetch_assoc($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailsBySpDetailID($sp_detail_id){
			$query = "SELECT * FROM `mobile_sale_details` WHERE `SP_DETAIL_ID` = $sp_detail_id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `mobile_sale_details`(`SS_ID`,
														`SP_DETAIL_ID`,
														`PRICE`,
														`DISCOUNT`,
														`TAX`,
														`SALE_PRICE`,
														`SALE_ORDER_NO`)
												 VALUES( $this->ss_id,
														 		'$this->sp_id',
														 		'$this->orig_price',
														 		'$this->discount',
														 		'$this->tax',
														 		'$this->price',
																'$this->sale_order_no')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `mobile_sale_details` SET `PRICE`      = '$this->orig_price',
																							   `DISCOUNT`   = '$this->discount',
																							   `TAX`   			= '$this->tax',
																							   `SALE_PRICE` = '$this->price' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function get_bill_amount($sp_id){
			$query = "SELECT SUM(SALE_PRICE) AS TOTAL_AMOUNT FROM `mobile_sale_details` WHERE SS_ID = '$sp_id'";
			$result = mysql_query($query);
			echo mysql_error();
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['TOTAL_AMOUNT'];
			}else{
				return 0;
			}
		}

		public function stock_status_is_not($sp_id,$status){
			$query = "SELECT STOCK_STATUS FROM mobile_purchase_details WHERE ID = '$sp_id' AND STOCK_STATUS != '$status' ";
			$result = mysql_query($query);
			return mysql_num_rows($result);
		}
		public function delete($sale_detail_id){
			$query = "DELETE FROM mobile_sale_details WHERE ID = $sale_detail_id";
			return mysql_query($query);
		}
		public function deleteCompleteBill($sp_id){
			$query = "DELETE FROM mobile_sale_details WHERE SS_ID = $sp_id";
			return mysql_query($query);
		}
		public function getTaxAmountById($sale_detail_id){
			$query = "SELECT ((SUB_TOTAL*TAX)/100) AS TAX_AMOUNT FROM mobile_sale_details WHERE ID = '$sale_detail_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return number_format($row['TAX_AMOUNT'],2,'.','');
			}else{
				return 0;
			}
		}
	}
?>
