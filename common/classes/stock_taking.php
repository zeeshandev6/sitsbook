<?php
	class stockTaking
	{
		public $entry_date;
		public $shed_id;
		public $item_id;
		public $unitRate;
		public $quantity;
		public $amount;

		public $found_records;

		public $fromDate;
		public $toDate;

		public function getStockTakingList(){
			$query = "SELECT * FROM `shed_stock`";
			return mysql_query($query);
		}
		public function getStockTakingPages($start,$end){
			$query = "SELECT * FROM `shed_stock` LIMIT $start,$end";
			return mysql_query($query);
		}
		public function getDetails($id){
			$query = "SELECT * FROM `shed_stock` WHERE ID = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}

		public function countRows(){
			$query = "SELECT COUNT(*) AS NUM_ROWS FROM shed_stock";
			$record = mysql_query($query);
			return $record;
		}

		public function save(){
			$query = "INSERT INTO `shed_stock`(`STOCK_TAKING_DATE`,
																			   `SHED_ID`,
																			   `ITEM_ID`,
																			   `REM_QUANTITY`,
																			   `UNIT_RATE`,
																			   `AMOUNT`)
																VALUES ( '$this->entry_date',
																				 '$this->shed_id',
																				 '$this->item_id',
																				  $this->quantity,
																			 	  $this->unitRate,
																		 		  $this->amount)";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}


		public function update($id){
			$query = "UPDATE `shed_stock` SET `STOCK_TAKING_DATE`	= '$this->entry_date',
																			  `SHED_ID`					 	=	'$this->shed_id',
																			  `ITEM_ID`						=	'$this->item_id',
																			  `REM_QUANTITY`			=	'$this->quantity',
																			  `UNIT_RATE`					=	'$this->unitRate',
																			  `AMOUNT`						=	'$this->amount' WHERE `ID` = $id";
																				echo $query;
			return mysql_query($query);
		}


		public function search($start,$total){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM `shed_stock` ";
			$andFlag = false;

			if($this->fromDate != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " STOCK_TAKING_DATE >= DATE('".$this->fromDate."') ";
				$andFlag = true;
			}

			if($this->toDate != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " STOCK_TAKING_DATE <= DATE('".$this->toDate."') ";
				$andFlag = true;
			}

			if($this->item_id != ''){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " ITEM_ID = '".$this->item_id."' ";
				$andFlag = true;
			}

			if($this->shed_id != 0){
				$query .= ($andFlag)?" AND ":" WHERE ";
				$query .= " SHED_ID = '".$this->shed_id."' ";
				$andFlag = true;
			}
			$query .= " ORDER BY STOCK_TAKING_DATE ASC  LIMIT $start,$total ";
			$records = mysql_query($query);

			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];

			return $records;
		}

		public function getAmountSumByMonth($itemAccCode,$endDate){
			$query = "SELECT SUM(AMOUNT) AS COST FROM shed_stock WHERE STOCK_TAKING_DATE = DATE('".$endDate."') AND ITEM_ID = '$itemAccCode' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['COST']==''?0:$row['COST'];
			}else{
				return 0;
			}
		}

		public function getShedList(){
			$query = "SELECT * FROM `shed` WHERE `ACTIVE` = 'Y'";
			$records = mysql_query($query);
			return $records;
		}
		public function getIssueItemList(){
			$query = "SELECT * FROM `items` WHERE `ACTIVE` = 'Y' ORDER BY ITEM_ID DESC";
			$records = mysql_query($query);
			return $records;
		}
		public function delete($id){
			$query = "DELETE FROM `shed_stock` WHERE `ID` = $id LIMIT 1";
			return mysql_query($query);
		}
		public function insertVoucherId($id,$voucher_id){
			$query = "UPDATE shed_stock SET VOUCHER_ID = $voucher_id WHERE ID = $id ";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function getVoucherId($id){
			$query = "SELECT VOUCHER_ID FROM shed_stock WHERE ID = $id";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return NULL;
			}
		}
		public function getTotals(){
			$query = "SELECT SUM(REM_QUANTITY) AS TOTAL_LENGTH, SUM(AMOUNT) AS TOTAL_AMOUNT FROM shed_stock";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row;
			}else{
				return NULL;
			}
		}
	}
?>
