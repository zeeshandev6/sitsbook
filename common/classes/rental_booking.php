<?php
	class RentalBooking{
		public $user_id;
		public $title;
		public $description;
		public $booking_time;
		public $try_time;
		public $delivery_time;
		public $returning_time;
		public $booking_status;
		public $client_name;
		public $client_mobile;
		public $client_phone;
		public $address;
		public $total_price;
		public $advance;
		public $remaining;
		public $fromBookingDate;
		public $toBookingDate;
		public $fromDeliveryDate;
		public $toDeliveryDate;
		public $clinet_name ;
		public $clinet_mobile;
		public $limit_start;
		public $booking_order_no;

		public $received_by;
		public $received_by_date;
		public $received_by_notes;
		public $deliver_status;

		public $return_by;
		public $return_by_date;
		public $return_by_notes;
		public $return_status;

		public $rental_booking_id;
		public $item_id;
		public $service_id;
		public $quantity;
		public $return_date;
		public $delivereddate;
		public $avail_status;
		public $rent_status;


		public function save(){
			$query = "INSERT INTO `rental_booking`(
																						`USER_ID`,
																						`TITLE`,
																						`DESCRIPTION`,
																						`BOOKING_DATE_TIME`,
																						`TRY_DATE_TIME`,
																						`DELIVERY_DATE_TIME`,
																						`RETURNING_DATE_TIME`,
																						`BOOKING_STATUS`,
																						`CLIENT_NAME`,
																						`CLIENT_MOBILE`,
																						`CLIENT_PHONE`,
																						`ADDRESS`,
																						`TOTAL_PRICE`,
																						`ADVANCE`,
																						`REMAINING`,
																						`BOOKING_ORDER_NO`
																					) VALUES (
																						'$this->user_id',
																						'$this->title',
																						'$this->description',
																						'$this->booking_time',
																						'$this->try_time',
																						'$this->delivery_time',
																						'$this->returning_time',
																						'$this->booking_status',
																						'$this->client_name',
																						'$this->client_mobile',
																						'$this->clinet_phone',
																						'$this->address',
																						'$this->total_price',
																						'$this->advance',
																						'$this->remaining',
																						'$this->booking_order_no'
																					)";
			mysql_query($query);
			return mysql_insert_id();
		}

		public function updateMain($rental_id){
			$query = "UPDATE `rental_booking` SET `TITLE`						   = '$this->title',
																						`DESCRIPTION` 		   = '$this->description',
																						`BOOKING_DATE_TIME`  = '$this->booking_time',
																						`TRY_DATE_TIME`		   = '$this->try_time',
																						`DELIVERY_DATE_TIME` = '$this->delivery_time',
																						`RETURNING_DATE_TIME`= '$this->returning_time',
																						`CLIENT_NAME`				 = '$this->client_name',
																						`CLIENT_MOBILE`			 = '$this->client_mobile',
																						`CLIENT_PHONE` 			 = '$this->clinet_phone',
																						`ADDRESS` 					 = '$this->address',
																						`TOTAL_PRICE` 			 = '$this->total_price',
																						`ADVANCE` 					 = '$this->advance',
																						`REMAINING`					 = '$this->remaining',
																						`BOOKING_ORDER_NO`   = '$this->booking_order_no' WHERE ID = '$rental_id' ";
			return mysql_query($query);
		}

		public function saveDetail(){
			$query = "INSERT INTO `rental_booking_detail`(
																										`RENTAL_BOOKING_ID`,
																										`ITEM_ID`,
																										`SERVICE_ID`,
																										`RETURNING_DATE`,
																										`QUANTITY`,
																										`DELIVERED_DATE`,
																										`ORDERING_STATUS`,
																										`RENT_STATUS`
																									) VALUES (
																										'$this->rental_booking_id',
																										'$this->item_id',
																										'$this->service_id',
																										'$this->return_date',
																										'$this->quantity',
																										'$this->delivereddate',
																										'$this->avail_status',
																										'$this->rent_status'
																									)";
				mysql_query($query);
				return mysql_insert_id();
		}

		public function update($id){
			$query= "UPDATE `rental_booking_detail` SET `ITEM_ID` 			  = '$this->item_id',
																									`SERVICE_ID` 		  = '$this->service_id',
																									`QUANTITY` 			  = '$this->quantity',
																									`RETURNING_DATE`  = '$this->return_date',
																									`DELIVERED_DATE`  = '$this->delivereddate',
																									`ORDERING_STATUS` = '$this->avail_status',
																									`RENT_STATUS` 		= '$this->rent_status' WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function updateDatesStatus($id){
			$query= "UPDATE `rental_booking` SET
																				`DELIVERY_DATE_TIME`='$this->delivery_time' WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function getItemTitlesByBookingId($rental_id){
			$query = "SELECT NAME FROM `items` WHERE ID IN (SELECT ITEM_ID FROM rental_booking_detail WHERE ITEM_ID > 0 AND RENTAL_BOOKING_ID = $rental_id) ";
			return mysql_query($query);
		}
		public function getServiceTitlesByBookingId($rental_id){
			$query = "SELECT TITLE FROM `services` WHERE ID IN (SELECT SERVICE_ID FROM rental_booking_detail WHERE SERVICE_ID > 0 AND RENTAL_BOOKING_ID = $rental_id) ";
			return mysql_query($query);
		}
		public function updateStatus($id){
			$query= "UPDATE `rental_booking` SET `BOOKING_STATUS` = '$this->booking_status' WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function getDetailByNumber($booking_no){
			$query = "SELECT * FROM `rental_booking` WHERE BOOKING_ORDER_NO = '$booking_no' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function getDetail($id){
			$query = "SELECT * FROM `rental_booking` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function getDetailDetail($id){
			$query = "SELECT * FROM `rental_booking_detail` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				return mysql_fetch_array($result);
			}else{
				return NULL;
			}
		}
		public function getRentalTitle($id){
			$query = "SELECT TITLE FROM `rental_booking` WHERE ID = '$id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return $record['TITLE'];
			}else{
				return NULL;
			}
		}
		public function delete($id){
			$query = "DELETE FROM `rental_booking` WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function deleteDetail($id){
			$query = "DELETE FROM `rental_booking_detail` WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function getRentalDetailListByRentalId($rental_id){
			$query = "SELECT * FROM `rental_booking_detail` WHERE RENTAL_BOOKING_ID = $rental_id";
			return mysql_query($query);
		}
		public function getListByRentalIdOrderOnly($rental_id){
			$query = "SELECT * FROM `rental_booking_detail` WHERE ORDERING_STATUS = 'Y' AND RENTAL_BOOKING_ID = '$rental_id' AND ID NOT IN (SELECT RD_ID FROM ordering_detail) ";
			return mysql_query($query);
		}
		public function delateRentalDetailListByRentalId($rental_id){
			$query = "DELETE FROM `rental_booking_detail` WHERE RENTAL_BOOKING_ID = '$rental_id'";
			return mysql_query($query);
		}
		public function getOrderPendingList(){
			$query = "SELECT * FROM `rental_booking_detail` WHERE ORDERING_STATUS = 'Y' AND ID NOT IN (SELECT RBD_ID FROM ordering) ";
			return mysql_query($query);
		}
		public function searchRental(){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM rental_booking ";
			$andFlag = false;

			if(trim($this->title) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`TITLE` LIKE '%".$this->title."%' ";
				$andFlag = true;
			}

			if(trim($this->fromBookingDate)!=""){
				$this->fromDate = date("Y-m-d 00:00:00)",strtotime($this->fromBookingDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`BOOKING_DATE_TIME` >= '".$this->fromBookingDate."' ";
				$andFlag = true;
			}
			if(trim($this->toBookingDate)!=""){
				$this->toDate = date("Y-m-d 23:59:00",strtotime($this->toBookingDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`BOOKING_DATE_TIME` <= '".$this->toBookingDate."' ";
				$andFlag = true;
			}

			if(trim($this->fromDeliveryDate)!=""){
				$this->fromDate = date("Y-m-d 00:00:00)",strtotime($this->fromDeliveryDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`DELIVERY_DATE_TIME` >= '".$this->fromDeliveryDate."' ";
				$andFlag = true;
			}
			if(trim($this->toDeliveryDate)!=""){
				$this->toDate = date("Y-m-d 23:59:00",strtotime($this->toDeliveryDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`DELIVERY_DATE_TIME` <= '".$this->toDeliveryDate."' ";
				$andFlag = true;
			}
			if($this->booking_status!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BOOKING_STATUS` = '$this->booking_status' ";
				$andFlag = true;
			}

			if($this->clinet_name!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CLIENT_NAME` = '$this->clinet_name' ";
				$andFlag = true;
			}
			if($this->clinet_mobile!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CLIENT_MOBILE` = '$this->clinet_mobile' ";
				$andFlag = true;
			}

			$query .= " ORDER BY ID DESC";
			if($this->limit_start == ''){
				$this->limit_start = 0;
			}
			$query .= " LIMIT $this->limit_start,$this->limit_end";

			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
    	$this->found_records = $totalRecords['total'];
    	return $result;
		}

		public function getRentalReport(){
			$query = "SELECT * FROM rental_booking ";
			$andFlag = false;

			if(trim($this->fromBookingDate)!=""){
				$this->fromDate = date("Y-m-d 00:00:00)",strtotime($this->fromBookingDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`BOOKING_DATE_TIME` >= '".$this->fromBookingDate."' ";
				$andFlag = true;
			}
			if(trim($this->toBookingDate)!=""){
				$this->toDate = date("Y-m-d 23:59:00",strtotime($this->toBookingDate));
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`BOOKING_DATE_TIME` <= '".$this->toBookingDate."' ";
				$andFlag = true;
			}

			if(trim($this->title) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "`TITLE` LIKE '%".$this->title."%' ";
				$andFlag = true;
			}

			if($this->booking_status!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `BOOKING_STATUS` = '$this->booking_status' ";
				$andFlag = true;
			}
			if($this->client_name!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CLIENT_NAME` = '$this->client_name' ";
				$andFlag = true;
			}
			if($this->client_mobile!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CLIENT_MOBILE` = '$this->client_mobile' ";
				$andFlag = true;
			}
			if($this->client_phone!=""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CLIENT_PHONE` = '$this->client_phone' ";
				$andFlag = true;
			}

			$query .= " ORDER BY ID DESC";

			$result = mysql_query($query);
			//$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
    	//$this->found_records = $totalRecords['total'];
    	return $result;
		}
		public function getVoucherId($id){
			$query = "SELECT VOUCHER_ID FROM `rental_booking` WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function insertVoucherId($id,$voucherId){
			$query = "UPDATE `rental_booking` SET `VOUCHER_ID` = $voucherId WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function setOrderCompleteStatus($id,$status){
			$query = "UPDATE `rental_booking_detail` SET `ORDER_COMPLETED` = '$status' WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function setOrderDeliverStatus($id,$status){
			$query = "UPDATE `rental_booking_detail` SET `DELIVER_STATUS` = '$status' WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function setOrderDeliverTime($id,$time){
			$query = "UPDATE `rental_booking_detail` SET `DELIVERED_DATE` = '$time' WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function deliverOrderUpdate($id){
			$query = "UPDATE `rental_booking_detail` SET `RECEIVED_BY`		   = '$this->received_by',
			 																						 `RECEIVED_BY_DATE`  = '$this->received_by_date',
																									 `RECEIVED_BY_NOTES` = '$this->received_by_notes',
																									 `DELIVER_STATUS`    = '$this->deliver_status'  WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function returnOrderUpdate($id){
			$query = "UPDATE `rental_booking_detail` SET `RETURN_BY`		   = '$this->return_by',
			 																						 `RETURN_BY_DATE`  = '$this->return_by_date',
																									 `RETURN_BY_NOTES` = '$this->return_by_notes',
																									 `RETURN_STATUS`   = '$this->return_status'  WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function setOrderReturnStatus($id,$status){
			$query = "UPDATE `rental_booking_detail` SET `RETURN_STATUS` = '$status' WHERE `ID` = $id  ";
			return mysql_query($query);
		}
		public function getCompletedOrders(){
			$query = "SELECT * FROM `rental_booking_detail` WHERE ORDER_COMPLETED = 'Y' ORDER BY ID DESC ";
			return mysql_query($query);
		}
		public function getDeliveredOrders(){
			$query = "SELECT * FROM `rental_booking_detail` WHERE DELIVER_STATUS = 'Y' AND RETURN_STATUS = 'N' AND RENT_STATUS = 'Y' ORDER BY ID DESC ";
			return mysql_query($query);
		}
		public function getUndeliveredCompletedOrders(){
			$query = "SELECT * FROM `rental_booking_detail` WHERE ( (ORDER_COMPLETED = 'Y' AND ORDERING_STATUS = 'Y') OR ORDERING_STATUS = 'N' ) AND DELIVER_STATUS != 'Y' ORDER BY ID DESC ";
			return mysql_query($query);
		}
		public function getReturnVoucherId($id){
			$query = "SELECT RETURN_VOUCHER_ID FROM `rental_booking` WHERE ID = $id ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['RETURN_VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function insertReturnVoucherId($id,$voucherId){
			$query = "UPDATE `rental_booking` SET `RETURN_VOUCHER_ID` = $voucherId WHERE `ID` = $id ";
			return mysql_query($query);
		}
		public function getBookingTotalCost($rental_booking_id){
			$query = "SELECT AMOUNT FROM `voucher_details` WHERE VOUCHER_ID IN  (SELECT VOUCHER_ID FROM rental_booking WHERE ID = '$rental_booking_id') AND ACCOUNT_CODE = '0101060003' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return (float)$record['AMOUNT'];
			}else{
				return "0";
			}
		}
		public function getBookingOrderNumber($id){
			$query = "SELECT BOOKING_ORDER_NO FROM `rental_booking` WHERE ID = $id ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return (int)$record['BOOKING_ORDER_NO'];
			}else{
				return "0";
			}
		}
		public function getListBookingOrder(){
			$query = "SELECT BOOKING_ORDER_NO FROM `rental_booking` ORDER BY BOOKING_ORDER_NO DESC LIMIT 1";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$record = mysql_fetch_array($result);
				return (int)$record['BOOKING_ORDER_NO'];
			}else{
				return "0";
			}
		}

	}
?>
