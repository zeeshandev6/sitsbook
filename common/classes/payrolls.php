<?php
	class Payrolls
	{
		public $account_title;
		public $account_code;
		public $contact_person;
		public $wage_type;
		public $wage_amount;
		public $email;
		public $mobile;
		public $phones;
		public $city;
		public $address;  //9 values

		public function getList(){
			$query = "SELECT * FROM payrolls ORDER BY CUST_ACC_TITLE";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM payrolls WHERE CUST_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}

		public function getTitle($accCode){
			$query = "SELECT `CUST_ACC_TITLE` FROM payrolls WHERE `CUST_ACC_CODE` ='$accCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['CUST_ACC_TITLE'];
			}else{
				return "";
			}
		}
		public function setTitle($accCode,$title){
			$query = "UPDATE payrolls SET CUST_ACC_TITLE = '$title' WHERE CUST_ACC_CODE = '$accCode'";
			return mysql_query($query);
		}
		public function save(){
			$query = "INSERT INTO `payrolls`(`CUST_ACC_TITLE`,
											  `CUST_ACC_CODE`,
											  `CONT_PERSON`,
											  `WAGE_TYPE`,
											  `WAGE_AMOUNT`,
											  `EMAIL`,
											  `MOBILE`,
											  `PHONES`,
											  `CITY`,
											  `ADDRESS`)
									  VALUES ('$this->account_title',
									  		  '$this->account_code',
									  		  '$this->contact_person',
									  		  '$this->wage_type',
									  		  '$this->wage_amount',
									  		  '$this->email',
									  		  '$this->mobile',
									  		  '$this->phones',
									  		  '$this->city',
									  		  '$this->address')";
			$check = "SELECT `CUST_ACC_CODE` FROM `payrolls` WHERE `CUST_ACC_CODE` = '$this->account_code'";
			$exists = mysql_num_rows(mysql_query($check));
			if($exists==0){
				mysql_query($query);
				return mysql_insert_id();
			}else{
				return false;
			}
		}
		public function update($id){
			$query = "UPDATE `payrolls` SET
					 `CUST_ACC_CODE`='$this->account_code',
					 `CUST_ACC_TITLE`='$this->account_title',
					 `CONT_PERSON`='$this->contact_person',
					 `WAGE_TYPE`='$this->wage_type',
					 `WAGE_AMOUNT`='$this->wage_amount',
					 `EMAIL`='$this->email',
					 `MOBILE`='$this->mobile',
					 `PHONES`='$this->phones',
					 `CITY`='$this->city',
					 `ADDRESS`='$this->address'
					  WHERE CUST_ID=$id";
			$updated = mysql_query($query);
			return $updated;
		}
		public function searchCustomer(){
			$query = "SELECT * FROM `payrolls` WHERE";
			if($this->account_code!==""&&$this->account_code!==""){
				$query .= " `CUST_ACC_TITLE` = '$this->account_code' ";
			}
		}
		public function search($term){
			$query = "select * from payrolls where `CUST_ACC_TITLE` like '%".$term."%'";
			$records = mysql_query($query);
			return $records;
		}
		public function delete($id){
			$query = "DELETE FROM payrolls WHERE CUST_ID ='$id' LIMIT 1";
			$success = mysql_num_rows(mysql_query($query));
			return $success;
		}
		public function delete_code($code){
			$query = "DELETE FROM payrolls WHERE CUST_ACC_CODE ='$code' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
