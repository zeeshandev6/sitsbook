<?php
	class CashManagement{
		public $cashMgmtId;
		public $today;
		public $type;
		public $customer_code;
		public $partyAccTitle;
		public $bank_acc_code;
		public $chequeDate;
		public $chequeNum;
		public $amount;
		public $bankReceipt;   //un-used- removing cause errors in some places
		public $memo;
		public $status;
		public $post;

		public $held_status;

		public function getSupplierList(){
			$query = "SELECT * FROM `suppliers`";
			$records = mysql_query($query);
			return $records;
		}

		public function get_autocomplete_values($value,$column){
			$query = "SELECT $column,ID FROM cash_management WHERE $column LIKE '%$value%' LIMIT 5";
			$result= mysql_query($query);
			$rows = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_assoc($result)){
					$rows[$row['ID']] = array();
					$rows[$row['ID']]['names'] = $row[$column];
				}
			}
			return $rows;
		}

		public function getPaymentChequeListToday(){
			$today = date('Y-m-d');
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'P'
					  AND DATE(CHEQUE_DATE) = DATE('".$today."') ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}
		public function getUnPaidChequeListToday(){
			$today = date('Y-m-d');
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'P'
					  AND DATE(CHEQUE_DATE) = DATE('".$today."') AND STATUS = 'O' ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}
		public function getPostDatedChequesAmount($party_acc_code,$current_date,$cheque_type){
			$query = "SELECT SUM(AMOUNT) AS TOTAL_AMOUNT FROM cash_management WHERE CHEQUE_DATE > '$current_date' AND PARTY_ACC_CODE = '$party_acc_code' AND CHEQUE_TYPE = '$cheque_type' ";
			$row   = mysql_fetch_assoc(mysql_query($query));
			return $row['TOTAL_AMOUNT'];
		}
		public function getPaymentChequeListNext(){
			$today = date('Y-m-d');
			$nextTwoDays = date('Y-m-d',strtotime($today."+3 days"));
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'P'
					  AND DATE(CHEQUE_DATE) >= DATE('".$today."')
					  AND DATE(CHEQUE_DATE) <= DATE('".$nextTwoDays."') ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}


		public function getReceiptChequeListToday(){
			$today = date('Y-m-d');
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'R'
					  AND DATE(CHEQUE_DATE) = DATE('".$today."') ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}

		public function getUnclearedChequeListToday(){
			$today = date('Y-m-d');
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'R'
					  AND DATE(CHEQUE_DATE) = DATE('".$today."') AND STATUS = 'O' ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}

		public function getReceiptChequeListNext(){
			$today = date('Y-m-d');
			$nextTwoDays = date('Y-m-d',strtotime($today."+3 days"));
			$query = "SELECT * FROM `cash_management` WHERE `CHEQUE_TYPE` = 'R'
					  AND DATE(CHEQUE_DATE) >= DATE('".$today."')
					  AND DATE(CHEQUE_DATE) <= DATE('".$nextTwoDays."') ORDER BY TODAY,CHEQUE_DATE ";
			$result = mysql_query($query);
			return $result;
		}

		public function changeChequeStatus($id,$status,$today){
			$query = "UPDATE `cash_management` SET STATUS = '$status', STATUS_DATE = '$today' WHERE ID = $id";
			mysql_query($query);
			return mysql_affected_rows();
		}

		public function changePostStatus($id,$status){
			$query = "UPDATE `cash_management` SET POST = '$status' WHERE ID = $id";
			mysql_query($query);
			return mysql_affected_rows();
		}

		public function getAccountTitle($accCode){
			$query = "SELECT `ACC_TITLE` FROM `account_code` WHERE `ACC_CODE` = '$accCode'";
			$result = mysql_fetch_array(mysql_query($query));
			return $result['ACC_TITLE'];
		}

		public function getDetails($id){
			$query = "SELECT * FROM `cash_management` WHERE `ID` = '$id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				return  mysql_fetch_array($result);
			}
		}

		public function save(){
			$query = "INSERT INTO `cash_management`(`TODAY`,
																							`CHEQUE_TYPE`,
																							`PARTY_ACC_CODE`,
																							`BANK_ACC_CODE`,
																							`CHEQUE_NO`,
																							`CHEQUE_DATE`,
																							`AMOUNT`,
																							`MEMO`,
																							`STATUS`,
																							`POST`)
																					VALUES ('$this->today',
																									'$this->type',
																									'$this->customer_code',
																									'$this->bank_acc_code',
																									'$this->chequeNum',
																									'$this->chequeDate',
																							 		'$this->amount',
																									'$this->memo',
																									'$this->status',
																									'$this->post')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($cheque_id){
			$query = "UPDATE `cash_management` SET `TODAY`           = '$this->today',
																						 `CHEQUE_TYPE`	   = '$this->type',
																						 `PARTY_ACC_CODE`	 = '$this->customer_code',
																						 `BANK_ACC_CODE`	 = '$this->bank_acc_code',
																						 `CHEQUE_NO`       = '$this->chequeNum',
																						 `CHEQUE_DATE`     = '$this->chequeDate',
																						 `AMOUNT`          = '$this->amount',
																						 `MEMO`            = '$this->memo',
																						 `STATUS`          = '$this->status',
																						 `POST`            = '$this->post' WHERE ID = '$cheque_id' ";
			return mysql_query($query);
		}
		public function delete($cash_id){
			$query = "DELETE FROM cash_management WHERE ID = $cash_id";
			mysql_query($query);
			return mysql_affected_rows();
		}
		public function search($fromDate,$toDate,$AccCode,$status,$type){
			$query = "SELECT * FROM `cash_management` WHERE ";
			$andFlag = false;
			if($fromDate!=""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(TODAY) >= DATE('".$fromDate."')  ";
				$andFlag = true;
			}
			if($toDate!=""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " DATE(TODAY) <= DATE('".$toDate."')  ";
				$andFlag = true;
			}
			if($AccCode!=""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " `PARTY_ACC_CODE` = '$AccCode'  ";
				$andFlag = true;
			}
			if($status!=""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " `STATUS` = '$status'  ";
				$andFlag = true;
			}
			if($this->held_status != ''){
				if($this->held_status == "H"){
					$query .= ($andFlag)?" AND ":"";
					$query .= " `STATUS` = 'O'  ";
					$andFlag = true;
				}
				if($this->held_status == "O"){
					$query .= ($andFlag)?" AND ":"";
					$query .= " `STATUS` != 'O'  ";
					$andFlag = true;
				}
			}
			if($type!=""){
				$query .= ($andFlag)?" AND ":"";
				$query .= " `CHEQUE_TYPE` = '$type'  ";
				$andFlag = true;
			}
			$query .= " ORDER BY TODAY,CHEQUE_DATE  ";
			return mysql_query($query);
		}
	}
?>
