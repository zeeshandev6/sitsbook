<?php
  class PackingContractDetails{
    public $fabric_id;
    public $construction;
    public $variety;
    public $thaan;
    public $quantity;
    public $rate;
    public $amount;

    public $from_date;
    public $to_date;
    public $pon;
    public $sc_no;

    public $found_records;

    public function getList(){
      $query = "SELECT * FROM packing_contract_details ORDER BY ID DESC ";
      return mysql_query($query);
    }
    public function getListById($id){
      $query = "SELECT * FROM packing_contract_details WHERE FABRIC_ID = $id";
      return mysql_query($query);
    }
    public function getListByLotNo($lot_no){
      $query = "SELECT * FROM packing_contract_details WHERE FABRIC_ID IN (SELECT ID FROM packing_contracts WHERE LOT_NO = $lot_no AND VOUCHER_ID > 0) ";
      return mysql_query($query);
    }
    public function save(){
      $query = "INSERT INTO `packing_contract_details`(
        FABRIC_ID,
        CONSTRUCTION,
        VARIETY,
        THAAN,
        QUANTITY,
        RATE,
        AMOUNT
      )
      VALUES(
        '$this->fabric_id',
        '$this->construction',
        '$this->variety',
        '$this->thaan',
        '$this->quantity',
        '$this->rate',
        '$this->amount'
      )";
      mysql_query($query);
      return mysql_insert_id();
    }
    public function update($id){
      $query = "UPDATE `packing_contract_details` SET
      `CONSTRUCTION`     = '$this->construction',
      `VARIETY`          = '$this->variety',
      `THAAN`            = '$this->thaan',
      `QUANTITY`         = '$this->quantity',
      `RATE`             = '$this->rate',
      `AMOUNT`           = '$this->amount' WHERE ID = '$id'";
      return mysql_query($query);
    }
    public function getSumAmountByLotNumber($lot_no){
      $query  = " SELECT SUM(AMOUNT) AS SUM_AMOUNT FROM `packing_contract_details` WHERE FABRIC_ID IN ( SELECT ID FROM `packing_contracts` WHERE LOT_NO = '$lot_no' ) ";
      $result = mysql_query($query);
      $row    = mysql_fetch_assoc($result);
      return  $row['SUM_AMOUNT'];
    }
    public function deleteContract($id){
      $query = "DELETE FROM packing_contract_details WHERE fabric_ID = '".$id."'";
      return mysql_query($query);
    }
    public function delete($id){
      $query = "DELETE FROM packing_contract_details WHERE ID = '".$id."'";
      return mysql_query($query);
    }
    public function insertVoucherId($contract_id,$voucher_id){
      $query = "UPDATE packing_contract_details SET VOUCHER_ID = '$voucher_id' WHERE ID = '$contract_id'";
      return mysql_query($query);
    }
    public function get_voucher_id($contract_id){
      $query = "SELECT VOUCHER_ID FROM packing_contract_details WHERE ID = '$contract_id'";
      $result = mysql_query($query);
      if(mysql_num_rows($result)){
        $row = mysql_fetch_assoc($result);
        return $row['VOUCHER_ID'];
      }else{
        return 0;
      }
    }
  }
?>
