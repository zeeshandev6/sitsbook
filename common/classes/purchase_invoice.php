<?php
	class PurchaseInvoice{
		public $invoice_no;
		public $invoice_date;
		public $supplier;
		public $expense_account;
		public $customer;
		public $subject;
		public $notes;
		public $total_amount;

		public $found_records;

		public $from_date;
		public $to_date;

		public function getList(){
			$query = "SELECT * FROM  `purchase_invoice`  ORDER BY ID DESC ";
			$records = mysql_query($query);
			return $records;
		}
		public function genInvoiceNumber(){
			$query = "SELECT MAX(INVOICE_NO) AS LAST_INVOICE_NO FROM   `purchase_invoice`   ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				$row['LAST_INVOICE_NO'] = (int)($row['LAST_INVOICE_NO']);
				$invoice_no = ++$row['LAST_INVOICE_NO'];
				return $invoice_no;
			}else{
				return 1;
			}
		}
		public function getDetail($pro_services_invoice_id){
			$query  = "SELECT * FROM   `purchase_invoice`   WHERE ID = '$pro_services_invoice_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function getVoucherId($pro_services_invoice_id){
			$query  = "SELECT VOUCHER_ID FROM  `purchase_invoice`  WHERE ID = '$pro_services_invoice_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return (int)$row['VOUCHER_ID'];
			}else{
				return 0;
			}
		}
		public function setVoucherId($pro_services_invoice_id,$voucher_id){
			$query  = "UPDATE  `purchase_invoice`  SET VOUCHER_ID = '$voucher_id' WHERE ID = '$pro_services_invoice_id' ";
			return mysql_query($query);
		}
		public function save(){
			$query = "INSERT INTO   `purchase_invoice`  ( `INVOICE_NO`,
																					 `INVOICE_DATE`,
																					 `SUPPLIER`,
																					 `EXPENSE_ACCOUNT`,
																					 `CUSTOMER`,
																					 `SUBJECT`,
																					 `NOTES`,
																					 `TOTAL_AMOUNT`)
																	VALUES ( '$this->invoice_no',
																					 '$this->invoice_date',
																					 '$this->supplier',
																					 '$this->expense_account',
																					 '$this->customer',
																					 '$this->subject',
																					 '$this->notes',
																					 '$this->total_amount')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($invoice_id){
			$query = "UPDATE   `purchase_invoice`   SET  `INVOICE_DATE`  		= '$this->invoice_date',
																					`SUPPLIER`     	 		= '$this->supplier',
																					`CUSTOMER`     	 		= '$this->customer',
																					`EXPENSE_ACCOUNT` 	= '$this->expense_account',
																					`SUBJECT`      			= '$this->subject',
																					`NOTES` 	   	 			= '$this->notes',
																					`TOTAL_AMOUNT` 			= '$this->total_amount' WHERE `ID` = '$invoice_id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function delete($id){
			$query = "DELETE FROM   `purchase_invoice`   WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function getPages($start,$limit){
			$query = "SELECT SQL_CALC_FOUND_ROWS   `purchase_invoice`.* FROM   `purchase_invoice`    ";
			$andFlag = false;
			if(trim($this->from_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "   `purchase_invoice`.INVOICE_DATE >= '".$this->from_date."' ";
				$andFlag = true;
			}
			if(trim($this->to_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "   `purchase_invoice`.INVOICE_DATE <= '".$this->to_date."' ";
				$andFlag = true;
			}
			if(trim($this->customer) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "   `purchase_invoice`.CUSTOMER = '".$this->customer."' ";
				$andFlag = true;
			}
			if(trim($this->expense_account) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "   `purchase_invoice`.EXPENSE_ACCOUNT = '".$this->expense_account."' ";
				$andFlag = true;
			}
			if((int)($this->invoice_no) > 0){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= "   `purchase_invoice`.INVOICE_NO = '".$this->invoice_no."' ";
				$andFlag = true;
			}
			$query .= " LIMIT $start,$limit ";
			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
      $this->found_records = $totalRecords['total'];

			return $result;
		}
		public function getReport(){
			$query = "SELECT * FROM `purchase_invoice` LEFT JOIN `purchase_invoice_details` ON `purchase_invoice_details`.INVOICE_ID = `purchase_invoice`.ID ";
			$andFlag = false;
			if(trim($this->from_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `purchase_invoice`.INVOICE_DATE >= '".$this->from_date."' ";
				$andFlag = true;
			}
			if(trim($this->to_date) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `purchase_invoice`.INVOICE_DATE <= '".$this->to_date."' ";
				$andFlag = true;
			}
			if(trim($this->customer) != ""){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `purchase_invoice`.CUSTOMER = '".$this->customer."' ";
				$andFlag = true;
			}
			if((int)($this->invoice_no) > 0){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " `purchase_invoice`.INVOICE_NO = '".$this->invoice_no."' ";
				$andFlag = true;
			}
			$result = mysql_query($query);
			return $result;
		}
	}
?>
