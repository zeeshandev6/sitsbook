<?php
	class AgingReportSummary{
		public function  getCustomerVouchersList($customerAccCode,$today){
			$query = "SELECT VOUCHER_ID FROM voucher_details WHERE ACCOUNT_CODE = '$customerAccCode' AND VOUCHER_DATE = DATE('".$today."')";
			$records = mysql_query($query);
			return $records;
		}
		
		public function openingBalanceX($endDate,$accCode){
			$query = "SELECT BALANCE FROM voucher_details 
						WHERE VOUCHER_DATE <= DATE('".$endDate."') 
							AND ACCOUNT_CODE = '$accCode' ORDER BY VOUCHER_DATE DESC, COUNTER DESC LIMIT 1";
		     $result = mysql_query($query);
			 if(mysql_num_rows($result)){
				 $cashBalance = mysql_fetch_array($result);
				 $cashBalance = $cashBalance['BALANCE'];
			 }else{
				 $cashBalance = 0;
			 }
			 return $cashBalance;
		}
		
		public function getCustomerDebitsForPeriod($customer_code,$fromDate,$toDate){
			$from = $fromDate;
			$to   = date('Y-m-d',strtotime($toDate." -1 days"));
			//$to = $toDate;
			$query = "SELECT SUM(AMOUNT) AS DEBITS FROM voucher_details WHERE ACCOUNT_CODE = '$customer_code' ";
			if($fromDate != ''){
				$query .= " AND VOUCHER_DATE >= DATE('".$from."')  ";
			}
			if($toDate != ''){
				$query .= "AND VOUCHER_DATE <= DATE('".$to."') AND TRANSACTION_TYPE = 'Dr' ";
			}
			$record = mysql_query($query);
			$row = mysql_fetch_array($record);
			return ($row['DEBITS'] == NULL)?'0':$row['DEBITS'];
		}
        public function getCustomerCreditsForPeriod($customer_code,$fromDate,$toDate){
			$from = $fromDate;
			$to   = date('Y-m-d',strtotime($toDate." -1 days"));
			//$to = $toDate;
			$query = "SELECT SUM(AMOUNT) AS CREDITS FROM voucher_details WHERE ACCOUNT_CODE = '$customer_code' ";
			if($fromDate != ''){
				$query .= " AND VOUCHER_DATE >= DATE('".$from."')  ";
			}
			if($toDate != ''){
				$query .= "AND VOUCHER_DATE <= DATE('".$to."') AND TRANSACTION_TYPE = 'Cr' ";
			}
			$record = mysql_query($query);
			$row = mysql_fetch_array($record);
			return ($row['CREDITS'] == NULL)?'0':$row['CREDITS'];
		}
	}
	
?>

