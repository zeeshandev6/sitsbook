<?php
	class MachineProduction{

		public $report_date;
		public $machine_id;
		public $machine_rpm;
		public $party_acc_code;
		public $expense;
		public $comment;
		public $stitch_shift_a;
		public $commander_shift_a;
		public $stitch_shift_b;
		public $commander_shift_b;
		public $niddle_setting;
		public $sale_rate;

		public function getDetails($mp_id){
			$query = "SELECT * FROM machine_production WHERE ID = '$mp_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getReport($report_date,$machine_id){
			$query = "SELECT * FROM machine_production WHERE REPORT_DATE = '$report_date' AND MACHINE_ID = '$machine_id' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getReportList($report_date){
			$query = "SELECT * FROM machine_production WHERE REPORT_DATE = '$report_date'  ";
			return mysql_query($query);
		}
		public function getMachineProductionList($report_date,$machine_id){
			$query = "SELECT * FROM machine_production JOIN machine_production_details ON machine_production.ID = machine_production_details.MP_ID WHERE machine_production.REPORT_DATE = '$report_date' AND machine_production.MACHINE_ID = '$machine_id' ";
			return mysql_query($query);
		}
		public function getDetailsByMachineId($machine_id,$report_date){
			$query = "SELECT * FROM machine_production WHERE MACHINE_ID = '$machine_id' AND REPORT_DATE = '$report_date' ";
			$result= mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_assoc($result);
				return $row;
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `machine_production`( `REPORT_DATE`,
																							   	`MACHINE_ID`,
																									`MACHINE_RPM`,
																							   	`PARTY_ACC_CODE`,
																							   	`EXPENSE`,
																							   	`COMMENT`,
																									`STITCH_SHIFT_A`,
																									`COMMANDER_SHIFT_A`,
																									`STITCH_SHIFT_B`,
																									`COMMANDER_SHIFT_B`,
																									`NIDDLE_SETTING`,
																									`SALE_RATE`
																							   )
																					   VALUES ('$this->report_date',
																						   		   '$this->machine_id',
																										 '$this->machine_rpm',
																						   		   '$this->party_acc_code',
																						   		   '$this->expense',
																						   		   '$this->comment',
																						   		   '$this->stitch_shift_a',
																										 '$this->commander_shift_a',
																						   		   '$this->stitch_shift_b',
																										 '$this->commander_shift_b',
																						   		   '$this->niddle_setting',
																						   		   '$this->sale_rate')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `machine_production` SET `REPORT_DATE`				= '$this->report_date',
																					   	  `MACHINE_ID`				= '$this->machine_id',
																								`MACHINE_RPM`				= '$this->machine_rpm',
																					   	  `PARTY_ACC_CODE` 		= '$this->party_acc_code',
																					   	  `EXPENSE`						= '$this->expense',
																								`COMMENT` 					= '$this->comment',
																								`STITCH_SHIFT_A` 		= '$this->stitch_shift_a',
																								`COMMANDER_SHIFT_A` 	= '$this->commander_shift_a',
																								`STITCH_SHIFT_B` 		= '$this->stitch_shift_b',
																								`COMMANDER_SHIFT_B` 	= '$this->commander_shift_b',
																								`NIDDLE_SETTING` 		= '$this->niddle_setting',
																								`SALE_RATE` 				= '$this->sale_rate' WHERE ID = '$id' ";
			$result = mysql_query($query);
			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM `machine_production` WHERE ID = '$id' ";
			return mysql_query($query);
		}
	}
?>
