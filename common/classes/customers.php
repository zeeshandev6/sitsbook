<?php
	class Customers{
		public $account_title;
		public $account_code;
		public $urdu_title;
		public $contact_person;
		public $channel;
		public $email;
		public $facebook_link;
		public $mobile;
		public $whatsapp_mobile;
		public $phones;
		public $city;
		public $area;
		public $sector;
		public $address;
		public $tax_reg_no;
		public $ntn_reg_no;
		public $cnic_no;
		public $send_sms;

		public $found_records;

		public function getList(){
			$query = "SELECT * FROM customers ORDER BY CUST_ACC_TITLE";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM customers WHERE CUST_ID ='$id'";
			$records = mysql_query($query);
			return $records;
		}
		public function checkByColumnXvalue($column,$value){
			$value = trim($value);
			$query = "SELECT CUST_ACC_CODE FROM customers WHERE $column LIKE '$value' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_assoc($records);
				return ($row['CUST_ACC_CODE']=='')?NULL:$row['CUST_ACC_CODE'];
			}else{
				return NULL;
			}
		}

		public function getTitle($accCode){
			$query = "SELECT `CUST_ACC_TITLE` FROM customers WHERE `CUST_ACC_CODE` ='$accCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['CUST_ACC_TITLE'];
			}else{
				return "";
			}
		}
		public function getMobileIfActive($accCode){
			$query = "SELECT `MOBILE` FROM customers WHERE `CUST_ACC_CODE` ='$accCode' AND SEND_SMS = 'Y' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['MOBILE'];
			}else{
				return "";
			}
		}
		public function setTitle($accCode,$title){
			$query = "UPDATE customers SET CUST_ACC_TITLE = '$title' WHERE CUST_ACC_CODE = '$accCode'";
			return mysql_query($query);
		}

		public function getCustomer($accCode){
			$query = "SELECT * FROM customers WHERE `CUST_ACC_CODE` ='$accCode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row;
			}else{
				return NULL;
			}
		}
		public function get_cities_array(){
			$query = "SELECT CITY,CUST_ACC_CODE FROM customers GROUP BY CITY ORDER BY CITY,CUST_ACC_TITLE";
			$result = mysql_query($query);
			$cities = array();
			if(mysql_num_rows($result)){
				while($row = mysql_fetch_array($result)){
					$cities[$row['CUST_ACC_CODE']]=$row['CITY'];
				}
			}
			return $cities;
		}
		public function get_sector_array(){
			$query = "SELECT SECTOR FROM customers WHERE TRIM(SECTOR) != '' GROUP BY SECTOR ORDER BY SECTOR ";
			$result = mysql_query($query);
			$cities = array();
			if(mysql_num_rows($result)){
				while($row  = mysql_fetch_array($result)){
					$cities[] = $row['SECTOR'];
				}
			}
			return $cities;
		}
		public function get_area_array(){
			$query = "SELECT AREA FROM customers WHERE TRIM(AREA) != '' GROUP BY AREA ORDER BY AREA ";
			$result = mysql_query($query);
			$cities = array();
			if(mysql_num_rows($result)){
				while($row  = mysql_fetch_array($result)){
					$cities[] = $row['AREA'];
				}
			}
			return $cities;
		}
		public function updateAreaSectorByNameGroup($dcol,$dbefore,$dafter){
			$query = "UPDATE customers SET $dcol = '$dafter' WHERE $dcol = '$dbefore' ";
			return  mysql_query($query);
		}
		public function addCustomer(){
			$query = "INSERT INTO `customers`(`CUST_ACC_TITLE`,
				 																`CUST_ACC_CODE`,
																				`URDU_TITLE`,
																				`CONT_PERSON`,
																				`CHANNEL`,
																				`EMAIL`,
																				`FACEBOOK_LINK`,
																				`MOBILE`,
																				`WHATSAPP_MOBILE`,
																				`PHONES`,
																				`CITY`,
																				`ADDRESS`,
																				`AREA`,
																				`SECTOR`,
																			    `TAX_REG_NO`,
																				`NTN_REG_NO`,
																				`CNIC_NO`,
																				`SEND_SMS`)
																VALUES ('$this->account_title',
																			  '$this->account_code',
																				'$this->urdu_title',
																				'$this->contact_person',
																				'$this->channel',
																				'$this->email',
																				'$this->facebook_link',
																				'$this->mobile',
																				'$this->whatsapp_mobile',
																				'$this->phones',
																				'$this->city',
																				'$this->address',
																				'$this->area',
																				'$this->sector',
																				'$this->tax_reg_no',
																				'$this->ntn_reg_no',
																				'$this->cnic_no',
																				'$this->send_sms')";
			$check = "SELECT `CUST_ACC_CODE` FROM `customers` WHERE `CUST_ACC_CODE` = '$this->account_code'";
			$exists = mysql_num_rows(mysql_query($check));
			if($exists==0){
				mysql_query($query);
				return mysql_insert_id();
			}else{
				return false;
			}
		}
		public function update($id){
			$query = "UPDATE `customers` SET
					 `CUST_ACC_CODE`	 = '$this->account_code',
					 `CUST_ACC_TITLE`	 = '$this->account_title',
					 `URDU_TITLE`	 		 = '$this->urdu_title',
					 `CONT_PERSON`		 = '$this->contact_person',
					 `CHANNEL` 				 = '$this->channel',
					 `EMAIL`					 = '$this->email',
					 `FACEBOOK_LINK` 	 = '$this->facebook_link',
					 `MOBILE`					 = '$this->mobile',
					 `WHATSAPP_MOBILE` = '$this->whatsapp_mobile',
					 `PHONES`					 = '$this->phones',
					 `CITY`						 = '$this->city',
					 `ADDRESS`			   = '$this->address',
					 `AREA`			   		 = '$this->area',
					 `SECTOR`			   	 = '$this->sector',
					 `TAX_REG_NO`			 = '$this->tax_reg_no',
					 `NTN_REG_NO`			 = '$this->ntn_reg_no',
					 `CNIC_NO`			   = '$this->cnic_no',
					 `SEND_SMS`			   = '$this->send_sms'  WHERE CUST_ID = $id ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function update_column($column,$value,$customer_id){
			$query = "UPDATE `customers` SET $column = '".$value."' WHERE CUST_ID = '".$customer_id."' ";
			return mysql_query($query);
		}
		public function searchCustomer(){
			$query = "SELECT * FROM `customers` WHERE";
			if($this->account_code!==""&&$this->account_code!==""){
				$query .= " `CUST_ACC_TITLE` = '$this->account_code' ";
			}
		}
		public function search($start=0,$per_page=30){
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM customers ";
			$andFlag = false;

			if($this->account_title != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " CUST_ACC_TITLE LIKE '%$this->account_title%' ";
				$andFlag = true;
			}
			if($this->mobile != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " MOBILE LIKE '$this->mobile%' ";
				$andFlag = true;
			}
			if($this->area != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " AREA LIKE '%$this->area%' ";
				$andFlag = true;
			}
			if($this->sector != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " SECTOR LIKE '%$this->sector%' ";
				$andFlag = true;
			}
			if($this->city != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " CITY LIKE '%$this->city%' ";
				$andFlag = true;
			}
			$query .= " ORDER BY CUST_ACC_TITLE ";
			$query .= " LIMIT $start,$per_page ";
			$result = mysql_query($query);
			$totalRecords = (mysql_fetch_array(mysql_query("(SELECT FOUND_ROWS() AS 'total')")));
			$this->found_records = $totalRecords['total'];
			return $result;
		}
		public function delete($id){
			$query = "DELETE FROM customers WHERE CUST_ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}

		public function delete_code($code){
			$query = "DELETE FROM customers WHERE CUST_ACC_CODE ='$code' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
