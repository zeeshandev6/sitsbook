<?php
	class Items{
		public $barcode;
		public $name;
		public $urdu_name;
		public $hs_code;
		public $company_name;
		public $measure;
		public $itemCatId;
		public $qty_per_carton;
		public $rate_carton;
		public $carton_dozen;
		public $is_carton;
		public $is_dozen;
		public $dozen_rate;
		public $stockQty;
		public $unit_length;
		public $minQty;
		public $purchasePrice;
		public $salePrice;
		public $active;
		public $item_discount;
		public $department;
		public $comment;
		public $inv_type;
		public $fromStock;
		public $toStock;

		public $from_sale_price;
		public $to_sale_price;

		public function save(){
			if($this->name == ''){
				return false;
			}
			$query = "INSERT INTO `items`(`ITEM_BARCODE`,
										  `NAME`,
										  `URDU_NAME`,
										  `HS_CODE`,
										  `COMPANY`,
										  `MEASURE`,
										  `QTY_PER_CARTON`,
										  `CARTON_DOZEN`,
										  `IS_CARTON`,
										  `IS_DOZEN`,
										  `DOZEN_RATE`,
										  `RATE_CARTON`,
										  `ITEM_CATG_ID`,
										  `MIN_QTY`,
										  `PURCHASE_PRICE`,
										  `SALE_PRICE`,
										  `ACTIVE`,
										  `ITEM_DISCOUNT`,
										  `COMMENT`,
										  `INV_TYPE`,
										  `UNIT_LENGTH`)
								   VALUES('$this->barcode',
								   		  '$this->name',
										  '$this->urdu_name',
										  '$this->hs_code',
								   		  '$this->company_name',
								   		  '$this->measure',
										  '$this->qty_per_carton',
										  '$this->carton_dozen',
										  '$this->is_carton',
										  '$this->is_dozen',
										  '$this->dozen_rate',
										  '$this->rate_carton',
								   		  '$this->itemCatId',
								   		  '$this->minQty',
										  	'$this->purchasePrice',
										  	'$this->salePrice',
										  	'$this->active',
										  	'$this->item_discount',
										  	'$this->comment',
										  	'$this->inv_type',
										  	'$this->unit_length')";
			$inserted = mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			if($this->name == ''){
				return false;
			}
			$query = "UPDATE `items` SET
					 `ITEM_BARCODE`		= '$this->barcode',
					 `NAME`				= '$this->name',
					 `URDU_NAME`		= '$this->urdu_name',
					 `HS_CODE`			= '$this->hs_code',
					 `COMPANY` 			= '$this->company_name',
					 `MEASURE` 			= '$this->measure',
					 `QTY_PER_CARTON`   = '$this->qty_per_carton',
					 `CARTON_DOZEN`    	= '$this->carton_dozen',
					 `IS_CARTON`    	= '$this->is_carton',
					 `IS_DOZEN`    		= '$this->is_dozen',
					 `DOZEN_RATE`    	= '$this->dozen_rate',
					 `RATE_CARTON`   	= '$this->rate_carton',
					 `ITEM_CATG_ID`		= '$this->itemCatId',
					 `PURCHASE_PRICE`	= '$this->purchasePrice',
					 `SALE_PRICE`		= '$this->salePrice',
					 `MIN_QTY`	 		= '$this->minQty',
					 `ITEM_DISCOUNT`	= '$this->item_discount',
					 `COMMENT`   		= '$this->comment',
					 `UNIT_LENGTH`      = '$this->unit_length' ";
			if($this->inv_type != ''){
				$query 	 .= ", `INV_TYPE`  = '$this->inv_type' ";
			}
			$query   .= " WHERE ID = '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function sumCategoryTotalCost($cateogry_id){
			$query  = "SELECT SUM(TOTAL_COST) AS COSTA FROM items WHERE ITEM_CATG_ID = '$cateogry_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['COSTA'];
			}else{
				return 0;
			}
		}
		public function sumCategoryTotalStock($cateogry_id){
			$query  = "SELECT SUM(STOCK_QTY) AS STOCK FROM items WHERE ITEM_CATG_ID = '$cateogry_id' ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return $row['STOCK'];
			}else{
				return 0;
			}
		}
		public function getListByDepartment($dep_id){
			$query = "SELECT * FROM items WHERE DEPARTMENT = $dep_id AND ACTIVE = 'Y' ORDER BY DEPARTMENT ASC, NAME ASC";
			$records = mysql_query($query);
			return $records;
		}
		public function getListAll(){
			$query = "SELECT * FROM items ORDER BY NAME ASC";
			$records = mysql_query($query);
			return $records;
		}
		public function getCategoryList(){
			$query = "SELECT `ITEM_CATG_ID` FROM items GROUP BY ITEM_CATG_ID";
			$records = mysql_query($query);
			return $records;
		}
		public function getCompanyNameList(){
			$query = "SELECT `COMPANY` FROM items WHERE COMPANY != '' GROUP BY COMPANY";
			$records = mysql_query($query);
			return $records;
		}
		public function getCategoryListFull(){
			$query = "SELECT `ITEM_CATG_ID` FROM items GROUP BY ITEM_CATG_ID";
			$records = mysql_query($query);
			return $records;
		}
		public function getListByCategory($category_id){
			$query = "SELECT * FROM items WHERE ITEM_CATG_ID = $category_id ORDER BY NAME";
			$records = mysql_query($query);
			return $records;
		}
		public function getList($department){
			$query = "SELECT * FROM items WHERE DEPARTMENT = $department AND ACTIVE = 'Y' ORDER BY NAME";
			$records = mysql_query($query);
			return $records;
		}
		public function getActiveList(){
			$query = "SELECT * FROM items WHERE ACTIVE = 'Y' ORDER BY NAME ASC ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDashboardStockPosition(){
			$query = "SELECT * FROM items WHERE ACTIVE = 'Y' AND (INV_TYPE = 'N' AND  MIN_QTY > STOCK_QTY)  ORDER BY NAME DESC ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDemandReport($company_name){
			$query = "SELECT * FROM items WHERE ACTIVE = 'Y' AND (INV_TYPE = 'N' AND STOCK_QTY < MIN_QTY ) AND MIN_QTY != 0  ";
			if($company_name != ''){
				$query.= " AND COMPANY = '$company_name' ";
			}
			$query.= "ORDER BY NAME DESC ";
			$records = mysql_query($query);
			return $records;
		}
		public function getActiveListCatagorically($category_id){
			$query = "SELECT * FROM items WHERE ITEM_CATG_ID = $category_id AND ACTIVE = 'Y' ORDER BY NAME ASC ";
			$records = mysql_query($query);
			return $records;
		}
		public function getRecordDetails($id){
			$query = "SELECT * FROM items WHERE ID = '$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_assoc($records);
				return $row;
			}else{
				return NULL;
			}
		}
		public function getDetailByBarcode($barcode){
			$query = "SELECT * FROM items WHERE ITEM_BARCODE = '$barcode'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row;
			}else{
				return NULL;
			}
		}
		public function newBarcode(){
			$query = "SELECT ITEM_BARCODE FROM items ORDER ITEM_BARCODE DESC LIMIT 1 ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$records = mysql_fetch_assoc($records);
				$barcode = $records['ITEM_BARCODE'];
				return $barcode;
			}else{
				return 0000;
			}
		}
		public function barcodeAvailabilityCheck($barcode,$item_id){
			$query = "SELECT * FROM items WHERE ITEM_BARCODE = '$barcode' AND ID != '$item_id'";
			$records = mysql_query($query);
			return mysql_num_rows($records);
		}
		public function getItemQtyPerCarton($id){
			$query = "SELECT `QTY_PER_CARTON` FROM `items` WHERE ID = '$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['QTY_PER_CARTON'];
			}else{
				return '';
			}
		}
		public function getItemTitle($id){
			$query = "SELECT `NAME` FROM `items` WHERE ID = '$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['NAME'];
			}else{
				return '';
			}
		}
		public function getItemMeasure($id){
			$query = "SELECT `MEASURE` FROM `items` WHERE ID = '$id'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['MEASURE'];
			}else{
				return '';
			}
		}
		public function getItemPurchasePriceByTitle($name){
			$query = "SELECT `PURCHASE_PRICE` FROM `items` WHERE NAME LIKE '$name'";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				$row = mysql_fetch_array($records);
				return $row['PURCHASE_PRICE'];
			}else{
				return '';
			}
		}
		public function getStock($id){
			$query = "SELECT `STOCK_QTY` FROM `items` WHERE `ID` = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['STOCK_QTY'] == '')?0:$row['STOCK_QTY'];
			}else{
				return 0;
			}
		}
		public function getStockByItemName($name){
			$query = "SELECT `STOCK_QTY` FROM `items` WHERE `NAME` LIKE '$name'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['STOCK_QTY'] != '')?$row['STOCK_QTY']:0;
			}else{
				return 0;
			}
		}
		public function addStock($item_id,$quantity){
			$query = "UPDATE items SET STOCK_QTY  = STOCK_QTY + $quantity WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function updateStock($item_id,$quantity){
			$query = "UPDATE items SET STOCK_QTY  = '$quantity' WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function updateStockValue($item_id,$quantity,$totalCost){
			$query = "UPDATE items SET STOCK_QTY  = '$quantity', TOTAL_COST = '$totalCost' WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function stock_entry($item_id,$quantity,$totalCost,$price){
			$query = "UPDATE items SET STOCK_QTY  = '$quantity', TOTAL_COST = '$totalCost',PURCHASE_PRICE = '$price' WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function addStockValue($item_id,$quantity,$totalCost){
			if($item_id == 0){
				return false;
			}
			$query = "UPDATE items SET STOCK_QTY  = STOCK_QTY + $quantity, TOTAL_COST = TOTAL_COST + $totalCost WHERE ID = $item_id";
			$result =mysql_query($query);
			$this->calculateAvgPrice($item_id);
			return $result;
		}
		public function calculateAvgPrice($item_id){
			$check = "SELECT TOTAL_COST / STOCK_QTY AS CHECK_PRICE FROM items WHERE ID = $item_id ";
			$result = mysql_fetch_array(mysql_query($check));
			if($result['CHECK_PRICE']>0){
				$query = "UPDATE items SET PURCHASE_PRICE = TOTAL_COST/STOCK_QTY WHERE ID = $item_id";
				mysql_query($query);
			}
			$query = "UPDATE items SET TOTAL_COST = 0 WHERE STOCK_QTY = 0 ";
			mysql_query($query);
			return true;
		}
		public function removeStockValue($item_id,$quantity,$totalCost){
			if($item_id == 0){
				return false;
			}
			$query  = "UPDATE items SET STOCK_QTY  = STOCK_QTY - $quantity, TOTAL_COST = TOTAL_COST - $totalCost WHERE ID = $item_id ";
			$result = mysql_query($query);
			$this->calculateAvgPrice($item_id);
			return $result;
		}
		public function removeStock($item_id,$quantity){
			$query = "UPDATE items SET STOCK_QTY  = STOCK_QTY - $quantity WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function addStockByName($itemName,$quantity){
			$query = "UPDATE items SET STOCK_QTY  = STOCK_QTY + $quantity WHERE NAME = '$itemName'";
			return mysql_query($query);
		}
		public function removeStockByName($itemName,$quantity){
			$query = "UPDATE items SET STOCK_QTY  = STOCK_QTY - $quantity WHERE NAME = '$itemName'";
			return mysql_query($query);
		}
		public function setImage($item_id,$image_name){
			$query = "UPDATE items SET IMAGE  = '$image_name' WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function getImage($id){
			$query = "SELECT `IMAGE` FROM `items` WHERE `ID` = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return $row['IMAGE'];
			}else{
				return '';
			}
		}
		public function getPurchasePrice($id){
			$query = "SELECT `PURCHASE_PRICE` FROM `items` WHERE `ID` = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['PURCHASE_PRICE'] == '')?0:$row['PURCHASE_PRICE'];
			}else{
				return 0;
			}
		}
		public function getAveragePrice($id){
			$query = "SELECT `TOTAL_COST`,`STOCK_QTY` FROM `items`  WHERE `ID` = '$id'";
			$record = mysql_query($query);

			$godown_query = "SELECT SUM(STOCK_QTY) AS STOCK FROM `branch_stock`  WHERE `ITEM_ID` = '$id' ";
			$godown_record = mysql_query($godown_query);
			$godown_items = 0;
			if(mysql_num_rows($godown_record)){
				$row = mysql_fetch_array($godown_record);
				$godown_items = (float)$row['STOCK'];
			}

			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				$row['STOCK_QTY'] +=  $godown_items;
				if($row['STOCK_QTY'] > 0 && $row['TOTAL_COST'] > 0){
					$average_price = $row['TOTAL_COST']/$row['STOCK_QTY'];
				}else{
					$average_price = $this->getPurchasePrice($id);
				}
				return round($average_price,2);
			}else{
				return 0;
			}
		}
		public function getSalePrice($id){
			$query = "SELECT `SALE_PRICE` FROM `items` WHERE `ID` = '$id'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['SALE_PRICE'] == '')?0:$row['SALE_PRICE'];
			}else{
				return 0;
			}
		}

		public function getBrandNameList(){
			$query = "SELECT `COMPANY` FROM `items` WHERE COMPANY != ''  GROUP BY COMPANY ORDER BY COMPANY";
			return mysql_query($query);
		}

		public function getSalePriceByName($name){
			$query = "SELECT `SALE_PRICE` FROM `items` WHERE `NAME` LIKE '$name'";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['SALE_PRICE'] == '')?0:$row['SALE_PRICE'];
			}else{
				return 0;
			}
		}

		public function beingUsed($item_id){
			$query = "SELECT COUNT(ITEM_ID) AS PURCHASED_ITEMS
						   FROM purchase_details WHERE ITEM_ID = $item_id ";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				if($row['PURCHASED_ITEMS']>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		public function getMaxItemID(){
			$query = "SELECT MAX(ID) AS ITEM_ID FROM items LIMIT 1";
			$result = mysql_query($query);
			if(mysql_num_rows($result)){
				$row = mysql_fetch_array($result);
				return (int)$row['ITEM_ID'];
			}else{
				return 0;
			}
		}
		public function getNextAutoIncrement(){
			$result = mysql_query("SHOW TABLE STATUS LIKE 'items'");
			$row    = mysql_fetch_array($result);
			return $row['Auto_increment'];
		}
		public function search(){
			$query = "SELECT * FROM items ";
			$andFlag = false;

			if($this->name != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " NAME LIKE '%$this->name%' ";
				$andFlag = true;
			}
			if($this->barcode != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ITEM_BARCODE LIKE '$this->barcode%' ";
				$andFlag = true;
			}
			if($this->company_name != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " COMPANY LIKE '$this->company_name%' ";
				$andFlag = true;
			}
			if($this->measure != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " MEASURE = '$this->measure' ";
				$andFlag = true;
			}
			if($this->inv_type != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " INV_TYPE = '$this->inv_type' ";
				$andFlag = true;
			}
			if($this->itemCatId != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " ITEM_CATG_ID = '$this->itemCatId' ";
				$andFlag = true;
			}
			if($this->from_sale_price != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " SALE_PRICE >= '$this->from_sale_price' ";
				$andFlag = true;
			}
			if($this->to_sale_price != ''){
				$query .= ($andFlag)?"":" WHERE ";
				$query .= ($andFlag)?" AND ":"";
				$query .= " SALE_PRICE <= '$this->to_sale_price' ";
				$andFlag = true;
			}
			$query .= " ORDER BY NAME ";
			return mysql_query($query);
		}
		public function update_single_column($id,$column,$value){
			$query = "UPDATE items SET $column = '$value' WHERE ID = '$id'";
			return mysql_query($query);
		}
		public function updateItemStatus($item_id,$status){
			$query = "UPDATE items SET ACTIVE = '$status' WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function addScanCost($item_id,$cost){
			$query = "UPDATE items SET TOTAL_COST = TOTAL_COST + $cost WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function removeScanCost($item_id,$cost){
			$query = "UPDATE items SET TOTAL_COST = TOTAL_COST - $cost WHERE ID = $item_id";
			return mysql_query($query);
		}
		public function delete($id){
			$query = "DELETE FROM items WHERE ID ='$id' LIMIT 1";
			$success = mysql_query($query);
			return $success;
		}
	}
?>
