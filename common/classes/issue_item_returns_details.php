<?php
	class IssueItemReturnDetails{
		public $issue_id;
		public $item_id;
		public $machine_id;
		public $quantity;
		public $cost_price;
		public $total_cost;

		public function getList($issue_id){
			$query = "SELECT * FROM issue_item_details WHERE ISSUE_ID = '$issue_id' ";
			$records = mysql_query($query);
			return $records;
		}
		public function getListForReturn($issue_usage,$issued_to){
			$query = "SELECT * FROM issue_item_details JOIN issue_item ON issue_item.ID = issue_item_details.ISSUE_ID WHERE issue_item.ISSUE_USAGE = '$issue_usage' AND issue_item.CHARGED_TO = '$issued_to' ";
			$records = mysql_query($query);
			return $records;
		}
		public function getDetail($issue_id){
			$query  = "SELECT * FROM issue_item_details WHERE ID = '$issue_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				return mysql_fetch_array($record);
			}else{
				return NULL;
			}
		}
		public function save(){
			$query = "INSERT INTO `issue_item_details` (`ISSUE_ID`,
														`ITEM_ID`,
														`MACHINE_ID`,
														`QUANTITY`,
														`COST_PRICE`,
														`TOTAL_COST`)
												VALUES ('$this->issue_id',
														'$this->item_id',
														'$this->machine_id',
														'$this->quantity',
														'$this->cost_price',
														'$this->total_cost')";
			mysql_query($query);
			return mysql_insert_id();
		}
		public function update($id){
			$query = "UPDATE `issue_item_details` SET `ITEM_ID`    = '$this->item_id',
													  `MACHINE_ID` = '$this->machine_id',
													  `QUANTITY`   = '$this->quantity',
													  `COST_PRICE` = '$this->cost_price',
													  `TOTAL_COST` = '$this->total_cost' WHERE ID = '$id' ";
			$updated = mysql_query($query);
			return $updated;
		}
		public function getTotalQuantity($issue_id){
			$query = "SELECT SUM(QUANTITY) AS TOTAL_QUANTITY FROM issue_item_details WHERE ISSUE_ID = '$issue_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['TOTAL_QUANTITY'] == '')?0:$row['TOTAL_QUANTITY'];
			}else{
				return 0;
			}
		}
		public function getListArrayIds($issue_id){
			$query = "SELECT ID FROM issue_item_details WHERE ISSUE_ID = '$issue_id' ";
			$record = mysql_query($query);
			$returnArray = array();
			if(mysql_num_rows($record)){
				while($row = mysql_fetch_array($record)){
					$returnArray[] = $row['ID'];
				}
			}
			return $returnArray;
		}
		public function getTotalAmount($issue_id){
			$query = "SELECT SUM(TOTAL_COST) AS AMOUNT FROM issue_item_details WHERE ISSUE_ID = '$issue_id' ";
			$record = mysql_query($query);
			if(mysql_num_rows($record)){
				$row = mysql_fetch_array($record);
				return ($row['AMOUNT'] == '')?0:$row['AMOUNT'];
			}else{
				return 0;
			}
		}
		public function delete($id){
			$query = "DELETE FROM `issue_item_details` WHERE ID = '$id' ";
			return mysql_query($query);
		}
		public function deleteCompleteByIssueId($issue_id){
			$query = "DELETE FROM `issue_item_details` WHERE ISSUE_ID = '$issue_id' ";
			return mysql_query($query);
		}
	}
?>
