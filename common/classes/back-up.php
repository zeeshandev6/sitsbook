<?php
	class Backup{

		public $query;

		public function makeBackUp()
		{
			$tables = '*';
			//get all of the tables
			if($tables == '*')
			{
				$tables = array();
				$result = mysql_query('SHOW TABLES');
				while($row = mysql_fetch_row($result))
				{
					$tables[] = $row[0];
				}
			}
			else
			{
				$tables = is_array($tables) ? $tables : explode(',',$tables);
			}
			$return = '';
			//cycle through
			foreach($tables as $table){
				$result = mysql_query('SELECT * FROM '.$table);
				$num_fields = mysql_num_fields($result);

				$return.= 'DROP TABLE IF EXISTS '.$table.';';
				$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
				$return.= "\n".$row2[1].";\n";

				for ($i = 0; $i < $num_fields; $i++)
				{
					while($row = mysql_fetch_row($result))
					{
						$return.= 'INSERT INTO '.$table.' VALUES(';
						for($j=0; $j<$num_fields; $j++)
						{
							$row[$j] = addslashes($row[$j]);
							$row[$j] = preg_replace("/\n/","\\n",$row[$j]);
							if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
							if ($j<($num_fields-1)) { $return.= ','; }
						}
						$return.= ");\n";
					}
				}
			}

			//save file
			$sql_file_name = 'backups/db-backup-'.date('d-M-Y').'.sql';
			$handle = fopen($sql_file_name,'w+');
			fwrite($handle,$return);
			fclose($handle);
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($sql_file_name));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($sql_file_name));
			readfile($sql_file_name);
			exit;
		}

		public function restoreFile($filename){
			$file = fopen($filename,'r');
			$query = '';
			$this->query = '';
			$success = false;
			while(!feof($file)){
				$line = fgets($file);
				if(substr(trim($line),-1,1) == ';'){
					$query .= " ".$line." ";
					if($query != ''){
						$result  = mysql_query($query);
						$success+= ((int)$result==0)?1:0;
						if(!$result){
							$this->query .= $query."\n";
						}
					}
					$query = '';
				}else{
					$query .= " ".$line." ";
				}
			}
			return $success;
		}

		public function restoreFileProgressBar($filename){
			$file 		   = fopen($filename,'r');
			$read_file     = fread($file, filesize($filename));
			$num_queries   = explode(";", $read_file);
			$num_queries   = count($num_queries);
			$current_query = 1;
			$query = '';
			$success = false;
			while(!feof($file)){
				$line  	  = fgets($file);
				$progress = intval(($current_query/$num_queries) * 100);
				if(substr(trim($line),-1,1) == ';'){
					$query .= " ".$line." ";
					if($query != ''){
						$success = mysql_query($query);
						$current_query++;
					}
					$query = '';
				}else{
					$query .= " ".$line." ";
				}
			}
			return $success;
		}
		public function getFloatingVoucherList(){
			$query = "SELECT * FROM `voucher` WHERE `VOUCHER_STATUS` = 'F' ";
			$records = mysql_query($query);
			if(mysql_num_rows($records)){
				return false;
			}else{
				return true;
			}
		}
		public function getUnpostedEntriesList(){
			$queryOutward = "SELECT * FROM outward WHERE `VOUCHER_ID` = 0";
			$queryInventory = "SELECT * FROM inventory WHERE `VOUCHER_ID` = 0";
			$queryIssueItem = "SELECT * FROM issue_item WHERE `VOUCHER_ID` = 0";

			$outward = mysql_query($queryOutward);
			$inventory = mysql_query($queryInventory);
			$issueItem = mysql_query($queryIssueItem);

			if(mysql_num_rows($outward)||mysql_num_rows($inventory)||mysql_num_rows($issueItem)){
				return false;
			}else{
				return true;
			}
		}
	}
?>
