<?php
    class Branches{
        public $title;
        public $address;
        public $active;
        public $branch_type;

        public function getList(){
            $query = "SELECT * FROM branches ";
            return mysql_query($query);
        }
        public function getListByType($type){
            $query = "SELECT * FROM branches WHERE `BRANCH_TYPE` = '$type' ";
            return mysql_query($query);
        }
        public function getListSpecialOrdering(){
            $query = "SELECT * FROM branches ORDER BY BRANCH_TYPE DESC,TITLE ASC";
            return mysql_query($query);
        }
        public function getDetails($id){
            $query = "SELECT * FROM branches WHERE ID = '$id' ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }else{
                return NULL;
            }
        }
        public function getTitle($id){
            $query = "SELECT TITLE FROM branches WHERE ID = '$id' ";
            $result= mysql_query($query);
            if(mysql_num_rows($result)){
                $row = mysql_fetch_assoc($result);
                return $row['TITLE'];
            }else{
                return $id;
            }
        }
        public function save(){
            $query = "INSERT INTO `branches`(`TITLE`,
                                             `ADDRESS`,
                                             `ACTIVE`,
                                             `BRANCH_TYPE`)
                                     VALUES ('$this->title',
                                             '$this->address',
                                             '$this->active',
                                             '$this->branch_type')";
            mysql_query($query);
            return mysql_insert_id();
        }
        public function update($id){
            $query = "UPDATE `branches` SET `TITLE`        = '$this->title',
                                            `ADDRESS`      = '$this->address',
                                            `BRANCH_TYPE`  = '$this->branch_type' WHERE ID = '$id' ";
            return mysql_query($query);
        }
        public function setBranchStatus($id,$status){
            $query = "UPDATE `branches` SET `ACTIVE`     = '$status' WHERE ID = '$id' ";
        }
        public function delete($id){
            $query = "DELETE FROM `branches` WHERE ID = '$id' ";
            return mysql_query($query);
        }
    }
?>
