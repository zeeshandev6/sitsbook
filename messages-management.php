<?php
    $out_buffer = ob_start();
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/messages.php');
    include ('common/classes/message-docs.php');
    include ('common/classes/customers.php');

	//Permission
	if(!in_array('messages',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

  $objMessages        = new Messages();
  $objCustomer        = new Customers();
  $objMessageDocs     = new MessageDocs();

  $mode 	  = "";
  $loadPage = true;

	if(!is_dir("uploads")){
		mkdir("uploads",0777);
	}

	if(!is_dir("uploads/docs/")){
		mkdir("uploads/docs/",0777);
	}

	$dir      = "uploads/docs/";

	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}else{
		exit(header('location:'.$_SERVER['PHP_SELF'].'?mode=inbox'));
	}
  if(isset($_POST['save'])){
  	  $notification_id      			      = 0;
	    $objMessages->from_user_id 		    = $_SESSION['classuseid'];
      $objMessages->to_user_id 		      = $_POST['to_user'];
      $objMessages->notification_date   = date('Y-m-d H:i:s');
      $objMessages->subject             = $_POST['subject'];
      $objMessages->descriptions        = $_POST['about'];

  		if($notification_id == 0){
  			$notification_id = $objMessages->save();
  			if($notification_id){
  				$action = 'S';
  			}else{
  				$action = 'E';
  			}
  		}
		  $datedFolder = $dir.date('d-m-Y')."/";
  		if(!is_dir($datedFolder) && count($_FILES['docs']) > 0){
  			mkdir($datedFolder,0777);
  		}
  		if($notification_id){
  			$fcount = 0;
  			foreach($_FILES['docs']['name'] as $filename){
  				$tmp     = $_FILES['docs']['tmp_name'][$fcount];
  				$fcount += 1;
  				$title   = basename($filename);
  				$ext     = pathinfo($filename,PATHINFO_EXTENSION);
  				$target_filename = $notification_id.md5(microtime().rand(0,300)).".".$ext;
  				$target  = $datedFolder.$target_filename;
  				move_uploaded_file($tmp,$target);

  				$objMessageDocs->msg_id 	= $notification_id;
  				$objMessageDocs->title  	= $title;
  				$objMessageDocs->file_name  = $target_filename;

  				$objMessageDocs->save();
  			}
  		}
      if($action == 'S'){
          exit(header('location: '.$_SERVER['PHP_SELF'].'?mode=compose&action=added'));
      }else{
      	exit(header('location: '.$_SERVER['PHP_SELF'].'?mode=compose&action=error'));
      }
    }

    $objMessages->unread = false;
  	if(isset($_POST['search'])){
  		$from_date = $_POST['from-date'];
  		$todate    = $_POST['to-date'];
  		$subject   = $_POST['subject'];
  	}else{
  		$from_date  = date('d-m-Y',strtotime("-2 days"));
  		$todate     = date('d-m-Y');
  		$subject    = '';
  	}
  	if($mode == 'inbox'){
  		$from = '';
  		$to   = $_SESSION['classuseid'];
  	}elseif($mode == 'sent'){
  		$from = $_SESSION['classuseid'];
  		$to   = '';
  	}
    if(isset($_GET['unread'])){
      $objMessages->unread = 'true';
    }
  	if($mode != 'compose'){
  		$messageList = $objMessages->search($from,$to,$from_date, $todate, $subject);
  	}
  	$userList  		    = $objAccounts->getActiveList();
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/deletebyid.js"></script>
        <script type="text/javascript" src="resource/libs/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
              $("select.to_user").selectpicker();
              $("div.to_user button").focus();
              tinymce.init({
                  selector: "textarea",
                  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
              });
              $(".unread").attr({"data-toggle":"tooltip","data-placement":"top","title":"Unread Message"});
              $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p> <i class="fa fa-envelope"></i> Messages</p>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>
                <div class="col-md-12 col-xs-12">
                	<div role="tabpanel">
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs"  role="tablist">
					    <li role="presentation" class="<?php echo $mode=='inbox'?"active":""; ?>"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?mode=inbox"> <i class="fa fa-inbox"></i> Inbox</a></li>
					    <li role="presentation" class="<?php echo $mode=='sent'?"active":""; ?>"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?mode=sent"> <i class="fa fa-send"></i> Sent</a></li>
					    <li role="presentation" class="<?php echo $mode=='compose'?"active":""; ?>"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?mode=compose"> <i class="fa fa-envelope"></i> Compose</a></li>
					  </ul>
					  <!-- Tab panes -->
<?php
				if($mode!='compose'){
?>
					  <div class="col-md-12 col-xs-12">
					  	<div style="height: 10px;"></div>
	                	<div class="panel panel-default">
	                		<form method="post" action="messages-management.php?mode=<?php echo $mode; ?>">
		                		<div id="form">
			                		<div class="caption" style="width: 100px;padding: 5px;">From Date</div>
			                		<div class="field" style="width: 150px;">
			                			<input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="from-date" style="width: 150px;" />
			                		</div>
			                		<div class="caption" style="width: 100px;padding: 5px;">To Date</div>
			                		<div class="field" style="width: 150px;">
			                			<input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="to-date" style="width: 150px;" />
			                		</div>
			                		<div class="clearfix"></div>

			                		<div class="caption" style="width: 100px;padding: 5px;">Subject</div>
			                		<div class="field" style="width: 430px;">
			                			<input type="text" class="form-control" name="subject" style="width: 430px;" />
			                		</div>
			                		<div class="clearfix"></div>

			                		<div class="caption" style="width: 100px;padding: 5px;"></div>
			                		<div class="field" style="width: 150px;">
			                			<input type="submit" name="search" value="Search" class="button" />
			                		</div>
			                		<div class="clearfix"></div>
		                		</div>
	                		</form>
	                		<div class="clearfix"></div>
	                	</div>
	                </div>
	                <div class="clearfix"></div>
<?php
				}
?>
					<div class="col-md-12">
					<div class="clear" style="height:20px;">&nbsp;</div>
					<div class="panel panel-default">
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane fade in active" id="home">
<?php
				if($mode!='compose'){
?>
					    	<table cellspacing="0" width="100%" class="table-hover" >
		                        <thead>
		                        <tr>
		                            <th width="10%" style="text-align:center">Date</th>
		                            <th width="12%" style="text-align:center"><?php echo $mode=='inbox'?"From":"To"; ?></th>
		                            <th width="23%" style="text-align:center">Subject</th>
		                            <th width="5%" style="text-align:center">Action</th>
		                        </tr>
		                        </thead>

		                        <tbody>
		                        <?php
		                        if(mysql_num_rows($messageList)){
		                            while($notificationDetails = mysql_fetch_assoc($messageList)){
		                            	if($mode=='inbox'){
		                            		$sender_detail = $objAccounts->getDetails($notificationDetails['FROM_USER_ID']);
		                            	}else{
		                            		$sender_detail = $objAccounts->getDetails($notificationDetails['TO_USER_ID']);
		                            	}
										if($notificationDetails['MSG_STATUS'] != 'Y'){
											$message_read_class = 'unread';
										}else{
											$message_read_class = '';
										}
								?>
		                                <tr class="<?php echo $message_read_class; ?>">
		                                    <td style="text-align:center"><?php echo date('d-m-Y', strtotime($notificationDetails['MSG_DATE'])); ?></td>
		                                    <td style="text-align:left; padding-left: 20px;"><?php echo $sender_detail['FIRST_NAME']." ".$sender_detail['LAST_NAME']; ?></td>
		                                    <td style="text-align:left; padding-left: 20px;"><?php echo $notificationDetails['SUBJECT']; ?></td>
		                                    <td style="text-align:center">
		                                        <a target="_blank" href="view-notification.php?id=<?php echo $notificationDetails['ID']; ?>" class="button mark-read btn-xs">View</a>
		                                    </td>
		                                </tr>
		                            <?php
		                            }
		                        }else{
		                            ?>
		                            <tr id="recordPanel">
		                                <td style="text-align:center" colspan="6"> No Record Found!</td>
		                            </tr>
		                        <?php
		                        }
		                        ?>
		                        </tbody>
		                    </table>
<?php
				}else{
					if($loadPage==true){
						if(isset($_GET['action'])){
?>
							<div class="col-xs-12 col-md-12">
								<?php
			                        if($_GET['action']=='updated'){
			                            ?>
			                            <div id="msg" class="alert alert-success">
			                                <button type="button" class="close">&times;</button>
			                                <span style="font-weight: bold;">Success!</span> Message Updated.
			                            </div>
			                        <?php
			                        }elseif($_GET['action']=='added'){
			                            ?>
			                            <div id="msg" class="alert alert-success">
			                                <button type="button" class="close">&times;</button>
			                                <span style="font-weight: bold;">Success!</span> Message Sent.
			                            </div>
			                        <?php
			                        }
			                    ?>
							</div>
						<?php
						}
	                ?>
						<form method="post" action="" enctype="multipart/form-data">
                            <div id="form">
                                <div class="caption">User Name :</div>
                                <div class="field">
                                    <select required name="to_user" class="to_user form-control show-tick" data-live-search="true">
                                    	<option value=""></option>
<?php
									if(mysql_num_rows($userList)){
										while($user = mysql_fetch_assoc($userList)){
											if($_SESSION['classuseid']==$user['ID']){
												continue;
											}
?>
											<option value="<?php echo $user['ID']; ?>" data-subtext="<?php echo $user['DESIGNATION']; ?>" ><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']; ?></option>
<?php
										}
									}
?>
                                    </select>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Date :</div>
                                <div class="field">
                                    <input type="text" readonly="readonly" class="form-control date" name="date" value="<?php if(isset($date)){ echo $date; }else{ echo date('d-m-Y'); } ?>" style=" width:57%" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Subject :</div>
                                <div class="field" style="width:690px;">
                                    <input type="text" class="form-control" name="subject" value="<?php if(isset($_GET['id'])){ echo $subject; } ?>" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption">Description :</div>
                                <div class="field" style="width:700px;">
                                    <textarea name="about"><?php if(isset($_GET['id'])){ echo $about; } ?></textarea>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Attachments :</div>
                                <div class="field" style="width:700px;">
                                    <input type="file" class="btn btn-default" name="docs[]" multiple />
                                </div>
                                <div class="clear"></div>

                                <div class="caption"></div>
                                <div class="field">
                                <?php
                                if(!isset($_GET['id'])){
                                    ?>
                                    <button type="submit" name="save"   value="Send"     class="btn btn-primary"> <i class="fa fa-send"></i> Send </button>
                                <?php
                                }else{
                                    ?>
                                    <input type="submit" name="save"   value="Update"   class="btn btn-default" />
                                    <input type="button" name="print"  value="Print"    class="btn btn-default" />
                                    <input type="button" name="new"    value="New Form" class="btn btn-default" onclick="window.location.href='add-notification-details.php';" />
                                <?php
                                }
                                ?>
                                </div>
                                <div style = "clear:both; height:20px"></div>
                            </div>
                        </form>
<?php
					}//Load Page if true end
				}
?>
					    </div>
					  </div>
					  </div>
					  </div>
					</div>
        </div>
                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Notification</div>

                            <div class="caption">From Date :</div>
                            <div class="field">
                                <input type="text" id="datepicker" name="fromdate" class="form-control date" style=" width:57%" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">To Date :</div>
                            <div class="field">
                                <input type="text" name="todate" class="form-control date datepicker" style=" width:57%" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"> Subject :</div>
                            <div class="field">
                                <input type="text" name="subject" class="form-control" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input type="submit" class="button" name="search" value="Search" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab2-->
                <div class="clear"></div>

                <!-- Delete confirmation popup -->
                <div id="myConfirm" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>     <!-- End summer -->
        </div>   <!-- End .content-box-top -->
    </div>
    </body>
    <script type="text/javascript">
      $(function(){
      <?php if($mode != "sent"){ echo '
        $(".mark-read").click(function(){
          $(this).parent().parent().removeClass("unread").attr({"data-toggle":"","data-placement":"","title":""}).tooltip("destroy");
        }); ';
       } ?>
       });
    </script>
    </html>
<?php include('conn.close.php'); ?>
