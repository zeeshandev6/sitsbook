<?php
    $out_buffer = ob_start();
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/measure.php');

    $objMeasures  = new Measures();

    $id = 0;

    if(isset($_POST['title'])){
      $id                         = (int)$_POST['id'];
      $objMeasures->name          = ucfirst($_POST['title']);
      $objMeasures->description   = ucfirst($_POST['description']);
      $objMeasures->status        = (isset($_POST['status']))?"Y":"N";

      if($id ==0){
        $id = $objMeasures->save();
        $action = 'added';
      }else{
        $objMeasures->update($id);
        $action = 'added';
      }
      header('location: add-measure-type-details.php?id='.$id.'&action='.$action);
      exit();
    }
    if(isset($_GET['id'])){
      $id = (int)(mysql_real_escape_string($_GET['id']));
      $array = $objMeasures->getRecordDetailsByID($id);
      $id             =   $array['ID'];
      $title          =   $array['NAME'];
      $status         =   $array['ACTIVE'];
      $description    =   $array['DESCRIPTIONS'];
    }
?>
  <!DOCTYPE html>

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p ><B>Measure Type Details</B></p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="measure-type-management.php"><div class="tab">List</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <?php
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type updated.
                        </div>
                    <?php
                    }elseif($_GET['action']=='added'){
                        ?>
                        <div id="msg" class="alert alert-success">
                            <button type="button" class="close">&times;</button>
                            <span style="font-weight: bold;">Success!</span> Measure Type added.
                        </div>
                    <?php
                    }
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" name="cust" class="form-horizontal">
                        <input type="hidden" name="measure_type" value="<?php if(isset($_GET['id'])){ echo 'update';} else { echo 'add'; } ?>" />
                        <input type="hidden" name="id" value="<?php if(isset($_GET['id'])){ echo $id;} ?>" />
                        <div id="form">
                          <div class="form-group">
                            <label class="control-label col-sm-2">Title :</label>
                            <div class="col-sm-10">
                              <input class="form-control" name="title" value="<?php if(isset($title)){ echo $title;} ?>" required="required"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2">Description :</label>
                            <div class="col-sm-10">
                              <textarea style="min-height:60px;" class="form-control" name="description"><?php if(isset($description)){ echo $description;}  ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <?php
                                if(!isset($_GET['id'])){
                              ?>
                                <input type="submit" name="save" value="Save" class="btn btn-primary" />
                              <?php
                                }elseif(isset($_GET['id'])){
                              ?>
                                <input type="submit" name="save" value="Update" id="updated" class="btn btn-primary" />
                                <button class="btn btn-default pull-right" name="measure_type" value="" >Add New</button>
                              <?php
                                }
                              ?>
                            </div>
                          </div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>
    </div><!--bodyWrapper-->
    </body>
    </html>
<?php include('conn.close.php'); ?>
