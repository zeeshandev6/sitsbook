<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
    include('common/classes/inventory-return.php');
	include('common/classes/inventory-return-details.php');
    include('common/classes/sale.php');
	include('common/classes/sale_details.php');
    include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

    if(!isset($_GET['0'])){
        exit();
    }

	$objAccountCodes           = new ChartOfAccounts();
	$objPurchase               = new inventory();
	$objPurchaseDetails        = new inventory_details();
    $objPurchaseReturn         = new InventoryReturn();
	$objPurchaseReturnDetails  = new InventoryReturnDetails();
    $objSale                   = new Sale();
	$objSaleDetails            = new SaleDetails();
    $objSaleReturn             = new SaleReturn();
	$objSaleReturnDetails      = new SaleReturnDetails();
	$objItems                  = new Items();
	$objItemCategory           = new itemCategory();
	$objTaxRates               = new TaxRates();
	$objDepartments            = new Departments();
	$objConfigs 		       = new Configs();

	$itemsCategoryList = $objItemCategory->getList();
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
        .btn-success{
            opacity:0.8;
        }
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 884;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){

					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Sitsbook.com",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('.itemSelector').selectpicker();
			$('.supplierSelector').selectpicker();
			$("select[name=user_id]").selectpicker();
			$('#repoType').selectpicker();
		});
        $(window).load(function(){
            $("tr.alt-row").removeClass('alt-row');
        });
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Analyse Stock Report [Developer]</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th class="col-xs-1">Sr. #</th>
                                   <th class="col-xs-4">Item Name</th>
                                   <th class="col-xs-1">Purchases</th>
                                   <th class="col-xs-1">P.Return</th>
                                   <th class="col-xs-1">Sales</th>
                                   <th class="col-xs-1">S.Return</th>
                                   <th class="col-xs-1">Net (Sales - Purchases)</th>
                                   <th class="col-xs-1">Stock</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                    $counter = 1;
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
							$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
                            if(mysql_num_rows($itemList)){
?>
								<tr>
                                    <th class="text-left pl-10" colspan="8"><?php echo $ItemCat['NAME']; ?></th>
                                </tr>
<?php

								while($theItem = mysql_fetch_array($itemList)){
                                    $item_id          = (int)$theItem['ID'];
                                    $stock            = $objItems->getStock($item_id);

                                    $purchases        = $objPurchaseDetails->getAllQuantity($item_id);
                                    $purchase_returns = $objPurchaseReturnDetails->getAllQuantity($item_id);
                                    $sales            = $objSaleDetails->getAllQuantity($item_id);
                                    $sale_returns     = $objSaleReturnDetails->getAllQuantity($item_id);

                                    $p_qty     = ($purchases['ALL_QTY'] + $sale_returns['ALL_QTY']) - $purchase_returns['ALL_QTY'];
                                    $s_qty     = ($sales['ALL_QTY']);

                                    $net       = (float)($p_qty - $s_qty);

                                    if($net == $stock){
                                        //continue;
                                    }
?>
                                    <tr class="<?php echo ($net == $stock)?"btn-success":""; ?>">
                                        <td class="text-center"><?php echo $counter; ?></td>
                                        <td class="text-left pl-10"><?php echo $theItem['NAME']; ?></td>
                                        <td class="text-center"><?php echo $purchases['ALL_QTY']; ?></td>
                                        <td class="text-center"><?php echo $purchase_returns['ALL_QTY']; ?></td>
                                        <td class="text-center"><?php echo $sales['ALL_QTY']; ?></td>
                                        <td class="text-center"><?php echo $sale_returns['ALL_QTY']; ?></td>
                                        <td class="text-center"><?php echo $net; ?></td>
                                        <td class="text-center"><?php echo $stock; ?></td>
                                    </tr>
<?php
                                    $counter++;
								}
							}
?>
<?php
                        }
                    }
?>
                            </tbody>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
