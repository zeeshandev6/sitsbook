<?php
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/items.php');
    include ('common/classes/item_category.php');

    $objItems         = new Items();
    $objItemCategory  = new ItemCategory();

    if(isset($_POST['delete_id'])){
      $delete_id =  mysql_real_escape_string($_POST['delete_id']);
      if(mysql_num_rows($objItems->getListByCategory($delete_id)) > 0){
        echo "N";
        exit();
      }
      $deleted   = $objItemCategory->delete($delete_id);
      echo "Y";
      exit();
    }


    $total = $objConfigs->get_config("PER_PAGE");
?>
    <!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("a.pointer").click(function(){
                    var row = $(this).parent().parent();
                    $("#confirmation").modal('show');
                    var delete_id = $(this).attr('value');
                    $("#confirmation #delete").click(function(){
                        $.post('item-category-management.php',{delete_id:delete_id},function(data){
                          if(data.trim()=='Y'){
                            displayMessage("Item category deleted successfully. ");
                            $(row).remove();
                          }else{
                            displayMessage("Error! Cannot delete Item category. ");
                          }
                        });
                    });
                });
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
<?php
        $mode = "";
        if(!isset($_GET['search']))
        {
            $path = "item-category-management.php?title=&search=Search";
            if(isset($_GET['tab'])){
                $path .= "&tab=search";
            }
            echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        }

        if(isset($_GET['search'])){
            $objItemCategory->name           = mysql_real_escape_string($_GET['title']);
            if(isset($_GET['page'])){
          		$this_page = $_GET['page'];
          		if($this_page>=1){
          			$this_page--;
          			$start = $this_page * $total;
          		}
          	}else{
          		$start = 0;
          		$this_page = 0;
          	}
            $category_list    = $objItemCategory->searchItemCategory($start,$total);
            $row              = mysql_num_rows($category_list);

            $found_records       = $objItemCategory->totalMatchRecords;
            $pageCount           = ceil($found_records/$total);
        }else{
            $category_list = $objItemCategory->getList();
            $row              = mysql_num_rows($category_list);
        }
?>
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p ><B>Item Category Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$found_records; } ?></span> </p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                                <div class="tab" id="tab2" onclick="tab('2','1','2');">Search</div>
                                <a href="add-item-category.php"><div class="tab">New</div></a>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>
                <div id = "bodyTab1">
                  <div class="col-xs-12">
                    <table class="table table-hover"  >
                        <thead>
                        <tr>
                            <th class="text-center" style="width:10%;">Sr No.</th>
                            <th class="text-center" style="width:20%;">Title</th>
                            <th class="text-center" style="width:10%;">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if($row>0){
                            $counter = 1;
                            while($itemCategoryDetails = mysql_fetch_assoc($category_list)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="itemCategory-list text-center"><?php echo $itemCategoryDetails['NAME']; ?></td>
                                    <td style="text-align:center">
                                        <a id="view_button" href="add-item-category.php?id=<?php echo $itemCategoryDetails['ITEM_CATG_ID']; ?>">View</a>
                                        <a class="pointer" value="<?php echo $itemCategoryDetails['ITEM_CATG_ID']; ?>"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            <?php
                                $counter++;
                            }
                        }
                        else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="col-xs-12 text-center">
      								<?php
      								if($found_records > $total){
      									$get_url = "";
      									foreach($_GET as $key => $value){
      										$get_url .= ($key == 'page')?"":"&".$key."=".$value;
      									}
      									?>
      									<nav>
      										<ul class="pagination">
      											<?php
      											$count = $found_records;
      											$total_pages = ceil($count/$total);
      											$i = 1;
      											$thisFileName = basename($_SERVER['PHP_SELF']);
      											if(isset($this_page) && $this_page>0){
      												?>
      												<li>
      													<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
      												</li>
      												<?php
      											}
      											if(isset($this_page) && $this_page>=1){
      												$prev = $this_page;
      												?>
      												<li>
      													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
      												</li>
      												<?php
      											}
      											$this_page_act = $this_page;
      											$this_page_act++;
      											while($total_pages>=$i){
      												$left = $this_page_act-5;
      												$right = $this_page_act+5;
      												if($left<=$i && $i<=$right){
      													$current_page = ($i == $this_page_act)?"active":"";
      													?>
      													<li class="<?php echo $current_page; ?>">
      														<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
      													</li>
      													<?php
      												}
      												$i++;
      											}
      											$this_page++;
      											if(isset($this_page) && $this_page<$total_pages){
      												$next = $this_page;
      												?>
      												<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
      												<?php
      											}
      											if(isset($this_page) && $this_page<$total_pages){
      												?>
      												<li>
      													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
      												</li>
      											</ul>
      										</nav>
      										<?php
      									}
      								}
      								?>
      							</div>
                  </div>
                </div> <!--bodyTab1-->
                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="" class="form-horizontal">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-sm-2">Category Title :</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2"></label>
                          <div class="col-sm-10">
                            <input type="submit" name="search" value="Search" class="btn btn-default" />
                          </div>
                        </div>
                      </div>
                    </form>
                </div> <!--bodyTab1-->
                <div class="clear" style="height:30px"></div>

            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
        <div id="confirmation" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div><!--bodyWrapper-->
    </body>
</html>
<?php include('conn.close.php'); ?>
