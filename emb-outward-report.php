<?php
	include 'common/connection.php';
    include 'common/config.php';
	include 'common/classes/accounts.php';
	include 'common/classes/emb_outward.php';
	include 'common/classes/machines.php';
	include 'common/classes/measure.php';
	include 'common/classes/emb_stitch_account.php';
	include 'common/classes/machine_sales.php';
	include 'common/classes/emb_inward.php';
	include 'common/classes/emb_lot_register.php';

	$objChartOfAccounts    = new ChartOfAccounts();
	$objOutwards           = new outward();
	$objInward             = new EmbroideryInward();
	$objMachine            = new machines();
	$objMeasure            = new Measures();
	$objStitch             = new EmbStitchAccount();
	$objMachineSales       = new machineSales();
	$objLotRegisterDetails = new EmbLotRegister();

	$firstDayOfThisMonth = date('01-m-Y');

	if(isset($_POST['search'])){
		$fromDate   = ($_POST['fromDate']=="")?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$toDate     = ($_POST['toDate']=="")?"":date('Y-m-d',strtotime($_POST['toDate']));

		$objOutwards->machineNum = mysql_real_escape_string($_POST['machineNum']);

		$firstDayOfMonth = date('d',strtotime($fromDate));
		if(strtotime($toDate)<=strtotime(date('Y-m-d'))){
			if($fromDate==''){
				$thisYear = date('Y');
				$firstJanuary = '01-01';
				$firstJanuaryThisYear = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
				$fromDate = $firstJanuaryThisYear;
			}

			$objLotRegisterDetails->fromDate	=  $fromDate;
			$objLotRegisterDetails->toDate  	=  $toDate;
			$objLotRegisterDetails->machineNum  =  $objOutwards->machineNum;
			$objLotRegisterDetails->theType = 'R';

			$claimReport = $objLotRegisterDetails->outwardBill();
			$report      = $objOutwards->getOutwardDetailListByDateRange($fromDate,$toDate);
		}else{
			$message = 'Ending Date Can not Exceed Current Date!';
		}
	}
	$machineList = $objMachine->getList();
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
    <script  type="text/javascript">
		$(document).ready(function(){
            $("select").selectpicker();
			$("table.tableBreak").tablesorter();
			$("#orderBill").click();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Outward Reports</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="outward-report.php"><div class="tabSelected">Generic</div></a>
                        <a href="specific-outward-report.php"><div class="tab">Specific</div></a>
                        <a href="cloth-stock-report.php"><div class="tab">Cloth Register</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form" style="width: 900px; margin: 0 auto;">
<?php
					if(isset($message)){
?>
						<div class="caption"></div><div class="redColor"><?php echo $message; ?></div>
<?php
					}
?>
                    <form method="post" action="">

                        <div class="title" style="font-size:20px;font-weight:normal; margin-bottom:20px">Outward Cloth Delivered Report</div>

                        <div class="caption">Start Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($fromDate))?date('d-m-Y',strtotime($fromDate)):$firstDayOfThisMonth; ?>" name="fromDate" class="form-control datepicker"  style="width:145px;"/>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">End Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($toDate))?date('d-m-Y',strtotime($toDate)):date('d-m-Y'); ?>" name="toDate" class="form-control datepicker" style="width:145px;" />
                        </div>
                        <div class="clear"></div>
                        <div class="caption">Machine Number</div>
                        <div class="field" style="">
                            <select name="machineNum" class="form-control">
                            <option value=""></option>
<?php
						if(mysql_num_rows($machineList)){
							while($machine = mysql_fetch_array($machineList)){
                                print_r($machine);
?>
								<option value="<?php echo $machine['ID']; ?>"><?php echo $machine['MACHINE_NO']; ?> - <?php echo $machine['NAME']; ?></option>
<?php
							}
						}
?>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
				<?php
                    if(!isset($report)){
                        echo "";
                    }else{
                ?>
        	<div class="content-box-content">
	            <span style="float:right;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printTable">
                	<div style="text-align:left;margin-bottom:5px;" class="pageHeader">
                        <p style="font-size:18px;padding:0;"><?php echo ($objOutwards->machineNum=='')?"Outward Cloth Delivery Report":"Outward Cloth Processed Report Machine No. ".$objOutwards->machineNum; ?></p>
                        <p>From Period <?php echo ($fromDate=="")?"":date('d-m-Y',strtotime($fromDate)); ?> to Period <?php echo date('d-m-Y',strtotime($toDate)); ?><span style="float:right;font-size:14px" class="repoDate">Report generated on: <?php echo date('d-m-Y'); ?></span></p>
                    </div>
                    <table class="tableBreak">
                        <thead class="tHeader">
                            <tr style="background:#EEE;">
                               <th width="5%" style="font-size:12px;text-align:center" id="orderBill">Bill#</th>
                               <th width="9%" style="font-size:12px;text-align:center">OutwardDate</th>
															 <th width="10%" class="text-center">3rd Party</th>
                               <th width="20%" style="font-size:12px;text-align:center">CustomerTitle</th>
                               <th width="4%" style="font-size:12px;text-align:center">Lot#</th>
                               <th width="6%" style="font-size:12px;text-align:center">Quality</th>
                               <th width="8%" style="font-size:12px;text-align:center">Design</th>
                               <th width="6%" style="font-size:12px;text-align:center">Measure</th>
                               <th width="6%" style="font-size:12px;text-align:center">Length</th>
                               <th width="5%" style="font-size:12px;text-align:center">Emb@</th>
                               <th width="5%" style="font-size:12px;text-align:center">StockO/s</th>
                               <th width="9%" style="font-size:12px;text-align:center">Amount</th>
<?php
									if($objOutwards->machineNum==""){
?>
                               <th width="8%" style="font-size:12px;text-align:center">Machine #</th>
<?php
									}
?>
															 <th width="4%" style="font-size:12px;text-align:center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
					if(mysql_num_rows($report)){
						$lastCustCode = "";
						$newMLength = 0;
						$embAmountTotal = 0;
						$prevOutward = 0;
						while($detailRow = mysql_fetch_array($report)){
							if($detailRow['OUTWD_DETL_ID'] == NULL){
								continue;
							}
							$measureMent = $objMeasure->getName($detailRow['MEASURE_ID']);
							$stockInward = $objInward->getInwardLotStockInHandWhloleLot($detailRow['CUST_ACC_CODE'],$detailRow['LOT_NO']);
							$outwardStockDelivered = $objOutwards->getOutwardLotQtyDeliveredWholeLotPrevId($detailRow['CUST_ACC_CODE'],$detailRow['LOT_NO'],$detailRow['OUTWD_DATE'],$detailRow['OUTWD_DETL_ID']);
							$stockOs = $stockInward - $outwardStockDelivered;
							$lot_billed  = $objOutwards->lotBilled($detailRow['OUTWD_DETL_ID']);
?>
                            <tr id="recordPanel" class="alt-row">
                            	<td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['BILL_NO']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo date('d-m-Y',strtotime($detailRow['OUTWD_DATE'])); ?></td>
																<td ><?php echo $objChartOfAccounts->getAccountTitleByCode($detailRow['THIRD_PARTY_CODE']) ?></td>
                                <td class='outwardRepoTd' style="text-align:left;"><?php echo substr($detailRow['CUST_ACC_TITLE'],0,15); ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['LOT_NO']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['QUALITY']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php  echo $detailRow['DESIGN_CODE']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php  echo $measureMent; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['MEASURE_LENGTH']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['EMB_RATE']; ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $stockOs;  ?></td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $detailRow['EMB_AMOUNT']; ?></td>
<?php
									if($objOutwards->machineNum==""){
?>
								<td class='outwardRepoTd' style="text-align:center;"><?php  echo $objMachine->getRecordDetailsValue($detailRow['MACHINE_ID'],'MACHINE_NO')."-".$objMachine->getRecordDetailsValue($detailRow['MACHINE_ID'],'NAME'); ?></td>
<?php
									}
?>
																<td style="text-align:center;">
																	<?php echo $lot_billed?"Billed":"Completed"; ?>
																</td>
                            </tr>
<?php
							$lastCustCode = $detailRow['CUST_ACC_CODE'];
							$newMLength  += $detailRow['MEASURE_LENGTH'];
							$embAmountTotal  += $detailRow['EMB_AMOUNT'];
						}
					}
					if(mysql_num_rows($claimReport)){
						while($claimRow = mysql_fetch_array($claimReport)){
							$measureMent = $objMeasure->getTitle($claimRow['MEASURE_ID']);
							$stockInward = $objInward->getInwardLotStockInHandWhloleLot($claimRow['CUST_ACC_CODE'],$claimRow['LOT_NO']);
							$customerName = $objOutwards->getAccountTitle($claimRow['CUST_ACC_CODE']);
							$stockOs = " - - - ";
?>
								<tr id="recordPanel"  class="alt-row hasTdReds">
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['BILL_NO']; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo date('d-m-Y',strtotime($claimRow['OUTWD_DATE'])); ?></td>
                                    <td class='outwardRepoTd' style="text-align:left;"><?php echo substr($customerName,0,15); ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['LOT_NO']; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['QUALITY']; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center"><?php echo $claimRow['DESIGN_CODE']; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php  echo $measureMent; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['MEASURE_LENGTH'] ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['EMB_RATE']; ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $stockOs;  ?></td>
                                    <td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['EMB_AMOUNT']; ?></td>
<?php
									if($objOutwards->machineNum==""){
?>
									<td class='outwardRepoTd' style="text-align:center;"><?php echo $claimRow['ID']; ?></td>
<?php
									}
?>
	                            </tr>
<?php
								if($claimRow['EMB_RATE'] < 0 && $claimRow['MEASURE_LENGTH'] > 0){
									$embAmountTotal += $claimRow['EMB_AMOUNT'];
								}elseif($claimRow['MEASURE_LENGTH'] < 0 && $claimRow['EMB_RATE'] > 0){
									$newMLength += $claimRow['MEASURE_LENGTH'];
									$embAmountTotal += $claimRow['EMB_AMOUNT'];
								}elseif($claimRow['EMB_RATE'] < 0 ){
									$embAmountTotal += $claimRow['EMB_AMOUNT'];
								}
						}
					}
?>
                        </tbody>
<?php
							if(!isset($newMLength)&&!isset($embAmountTotal)){
								$newMLength = 0;
								$embAmountTotal = 0;
							}
?>
                        <tfoot class="tableFooter">
<?php
							//If Single Machine Is Selected Hide Adjustment Calculations
							if(isset($objOutwards->machineNum)){
?>
                            <tr>
                                <td class='outwardRepoTd' style="text-align:right; font-weight:bold" colspan="<?php echo ($objOutwards->machineNum!=="")?"7":"7"; ?>">Total:</td>
                                <td class='outwardRepoTd' style="text-align:center;" ><?php echo (isset($newMLength))?$newMLength:0; ?></td>
<?php
								if($objOutwards->machineNum==""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
							if($objOutwards->machineNum!=""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;" ><?php echo (isset($embAmountTotal))?$embAmountTotal:0; ?></td>
<?php
							}else{
?>
                                <td class='outwardRepoTd' style="text-align:center;" ><?php echo (isset($embAmountTotal))?$embAmountTotal:0; ?></td>
<?php
							}
?>
<?php
								if($objOutwards->machineNum==""){
?>
								<td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                            </tr>

<?php
							}
?>
<?php
							//If All Machinee Are Selected  Hide Adjustment Calculations
							if(isset($objOutwards->machineNum)&&$objOutwards->machineNum!==''){
								$machineSale = $objMachineSales->getMonthEndMachineSaleSingle($toDate,$objOutwards->machineNum);
								$prevMachineSales = $objMachineSales->getPreviousMonthMachineSaleSingle($fromDate,$objOutwards->machineNum);
								//$claimed = $objLotRegisterDetails->getClaimedLengthOfMachineDateRange($objOutwards->machineNum,$fromDate,$toDate);
								if($machineSale==''){
									unset($machineSale);
									$machineSale = array();
									$machineSale['CLOTH_LENGTH'] = 0;
									$machineSale['AMOUNT'] =  0;
								}
									$newMLength     += $machineSale['CLOTH_LENGTH'];
									$embAmountTotal += $machineSale['AMOUNT'];
?>
                            <tr>
                                <td class='outwardRepoTd' style="text-align:right; font-weight:bold" colspan="<?php echo ($objOutwards->machineNum!="")?"7":"8"; ?>">Current Month Adjustment</td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $machineSale['CLOTH_LENGTH']; ?></td>
<?php
								if($objOutwards->machineNum==""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $machineSale['AMOUNT']; ?></td>
                            </tr>

                            <tr>
                                <td class='outwardRepoTd' style="text-align:right; font-weight:bold" colspan="<?php echo ($objOutwards->machineNum!=="")?"7":"8"; ?>">Available Machine Sale</td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $newMLength; ?></td>
<?php
								if($objOutwards->machineNum==""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;border-top:1px solid #000;"><?php echo $embAmountTotal; ?></td>
                            </tr>
<?php
								if($prevMachineSales==''){
									unset($prevMachineSales);
									$prevMachineSales = array();
									$prevMachineSales['CLOTH_LENGTH'] = 0;
									$prevMachineSales['AMOUNT']=  0;
								}
									$newMLength    -= $prevMachineSales['CLOTH_LENGTH'];
									$embAmountTotal-= $prevMachineSales['AMOUNT'];
?>
							<tr>
                                <td class='outwardRepoTd' style="text-align:right; font-weight:bold" colspan="<?php echo ($objOutwards->machineNum!=="")?"7":"8"; ?>">Previous Month Adjustment</td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo ($prevMachineSales['CLOTH_LENGTH']=='')?"0":$prevMachineSales['CLOTH_LENGTH']; ?></td>
<?php
								if($objOutwards->machineNum==""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo ($prevMachineSales['AMOUNT']=='')?"0":$prevMachineSales['AMOUNT']; ?></td>
                            </tr>
                            <tr>
                                <td class='outwardRepoTd' style="text-align:right; font-weight:bold" colspan="<?php echo ($objOutwards->machineNum!=="")?"7":"8"; ?>">Net Machine Sale</td>
                                <td class='outwardRepoTd' style="text-align:center;"><?php echo $newMLength; ?></td>
<?php
								if($objOutwards->machineNum==""){
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
<?php
								}
?>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;"> - - - </td>
                                <td class='outwardRepoTd' style="text-align:center;border-top:1px solid #000;"><?php echo $embAmountTotal; ?></td>
                            </tr>
<?php
							}
?>
                        </tfoot>
                    </table>
                    <div class="clear"></div>
                </div> <!--End bodyTab-->
                <div style="height:0px;clear:both"></div>
                <?php
					}
				?>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script>
	$(window).load(function(){
		$(".hasTdReds").find('td').css({'color':'red'});
		$(".printThis").click(function(){
			var MaxHeight = 814;
			var RunningHeight = 0;
			var PageNo = 1;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight = 814;
				}else{
					MaxHeight = 914;
				}

				if (RunningHeight + $(this).height() > MaxHeight) {
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate    = $(".repoDate").text();
			//remove previous thead/tfoot/ReportDate
			$(".tHeader").remove();
			$(".repoDate").remove();
			$(".tableFooter").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:12px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();
			$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Emque Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			});
		});

	});
</script>
