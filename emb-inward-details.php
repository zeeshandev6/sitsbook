<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/emb_inward.php');
	include('common/classes/emb_products.php');
	include('common/classes/measure.php');
	include('common/classes/customers.php');

	//Permission
	if(!in_array('emb-inward',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objInwards 	= new EmbroideryInward();
	$objMeasure 	= new Measures();
	$objProducts 	= new EmbProducts();
	$objCustomers 	= new Customers();

	$measureList 		= $objMeasure->getList();
	$customersList 		= $objCustomers->getList();
	$productList 		= $objInwards->getProductList();

	$last_lot_number    = $objInwards->getNextLotNumber();

	$id = 0;
	if(isset($_GET['id'])){
		$id  	  						= (int)(mysql_real_escape_string($_GET['id']));
		$inward 						= $objInwards->getInward($id);
		$inwardDetailsList 	= $objInwards->getInwardDetailList($id);
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
		<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
    <!-- jQuery -->
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
  	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
  	<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
  	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript">
		$(function(){

		});
		var reload_dropdown = function(elm){
			$("button#reload_product").prop('disabled',true);
			$.get("?",{},function(html){
				$(elm).html($(html).find(elm).html());
				$(elm).selectpicker('refresh');
				$("button#reload_product").prop('disabled',false);
			});
		};
		</script>
</head>
<body>
    <div id="body-wrapper">
			<div id="sidebar">
				<?php include("common/left_menu.php") ?>
			</div> <!-- End #sidebar -->
    	<div class="content-box-top">
      	<div class="content-box-header">
          	<p>Cloth Receipt Management</p>
              <span id="tabPanel">
                  <div class="tabPanel">
                      <a href="emb-inward.php?tab=list"><div class="tab">List</div></a>
                      <a href="emb-inward.php?tab=search"><div class="tab">Search</div></a>
                      <div class="tabSelected" id="tab1">Details</div>
                  </div>
              </span>
          </div><!--content-box-header-->
          <div class="content-box-content">
          	<div id="form">
			  		  <input type="hidden" class="inward_id" value="<?php echo $id; ?>">
                      <div class="caption">Inward Date</div>
                      <div class="field" style="width:150px">
                          <input type="text" name="inward_date" value="<?php echo (isset($inward))?date("d-m-Y",strtotime($inward['INWD_DATE'])):date("d-m-Y"); ?>" class="form-control datepicker" style="width:145px" />
                      </div>
											<div class="caption" style="width:310px;">Lot No</div>
                      <div class="field" style="width:150px">
                          <input type="text" name="lot_num" value="<?php echo (isset($inward))?$inward['LOT_NO']:$last_lot_number; ?>" class="form-control" style="width:145px" autofocus />
                      </div>
											<div class="clear"></div>

                      <div class="caption">Customer</div>
                      <div class="field">
                          <select class="selectpicker show-tick form-control account_code" data-live-search="true" name="account_code">
                             <option value=""></option>
<?php
                      if(mysql_num_rows($customersList)){
                          while($account = mysql_fetch_array($customersList)){
														$selected = (isset($inward)&&$inward['ACCOUNT_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                             <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                          }
                      }
?>
                          </select>
                      </div>
											<div class="caption" style="width: 80px;">
												<a href="customer-detail.php" target="_blank" class="btn btn-default btn-sm pull-left"><i class="fa fa-plus"></i></a>
												<a href="#" onclick="reload_dropdown('select.account_code');" class="btn btn-default btn-sm pull-left ml-5"><i class="fa fa-refresh"></i></a>
											</div>

											<div class="caption" style="width:100px">Gate Pass</div>
                      <div class="field" style="width:150px">
                          <input type="text" name="gate_pass_no" value="<?php echo (isset($inward))?$inward['GATE_PASS_NO']:""; ?>" class="form-control" style="width:145px" autofocus />
                      </div>
											<div class="clear"></div>
											<hr />
											<div class="clear"></div>

											<table class="table_input">
													<thead>
															<tr style="background:#EEE;">
																 <th class="col-xs-3 text-center">
																	 Product
																	 <a href="emb-product-details.php" target="_blank" class="btn btn-default btn-xs pull-right ml-5"><i class="fa fa-plus"></i></a>
																	 <button type="button"  onclick="reload_dropdown('select.product_id')" id="reload_product" class="btn btn-default btn-xs pull-right"><i class="fa fa-refresh"></i></button>
																 </th>
																 <th class="col-xs-1 text-center">Fab.Quality</th>
																 <th class="col-xs-1 text-center">Color</th>
																 <th class="col-xs-1 text-center">Than</th>
																 <th class="col-xs-1 text-center">Measure</th>
																 <th class="col-xs-1 text-center">Length</th>
																 <th class="col-xs-1 text-center">Than Length</th>
																 <th class="col-xs-1 text-center">Action</th>
															</tr>
													</thead>
													<tbody>
															<tr class="quickSubmit">
																	<td >
																			<select class="product_id show-tick form-control" data-live-search="true">
																				 <option selected value="0"></option>
					<?php
																	if(mysql_num_rows($productList)){
																			while($account = mysql_fetch_array($productList)){
																					$selected = "";
					?>
																				 <option data-subtext="<?php echo $account['PROD_CODE']; ?>" value="<?php echo $account['ID']; ?>" <?php echo $selected; ?> ><?php echo $account['TITLE']; ?></option>
					<?php
																			}
																	}
					?>
																			</select>
																	</td>
																	<td style="padding:0; line-height:0;text-align:center;">
																		<input type="text" class="form-control text-center quality" />
																	</td>
																	<td style="padding:0; line-height:0;text-align:center;">
																		<input type="text" class="form-control color" value="White" />
																	</td>
																	<td style="padding:0; line-height:0">
																		<input type="text" class="form-control text-center thaan" />
																	</td>
																	<td>
																		<select  class="measure_id form-control" >
																			<option value=""></option>
																		<?php
																		if(mysql_num_rows($measureList)){
																			while($row = mysql_fetch_array($measureList)){
																		?>
																			<option value="<?php  echo $row['ID']; ?>"><?php  echo $row['NAME']; ?></option>
																		<?php
																			}
																		}
																		?>
																			</select>
																	</td>
																	<td>
																		<input type="text" class="length text-center form-control" value="1" />
																	</td>
																	<td>
																		<input type="text" class="total_length text-center form-control"  readonly="readonly" />
																	</td>
																	<td>
																		<input type="button" class="add_detail_row button" value="Enter" />
																	</td>
															</tr>
													</tbody>
													<tbody class="transactions">
<?php
			if(isset($inwardDetailsList) && mysql_num_rows($inwardDetailsList)){
				while($row = mysql_fetch_array($inwardDetailsList)){
					$measureName  = $objMeasure->getName($row['MEASURE_ID']);
					$product_name = $objProducts->getTitle($row['PRODUCT_ID']);
?>
															<tr class="transcation" data-row-id="<?php echo $row['ID'];?>">
																	<td class="text-left 	  product_id" data-id="<?php echo $row['PRODUCT_ID']; ?>"><?php echo $product_name; ?></td>
																	<td class="text-center  quality"><?php echo $row['QUALITY']; ?></td>
																	<td class="text-center  color"><?php echo $row['COLOR']; ?></td>
																	<td class="text-center  thaan"><?php echo $row['THAN']; ?></td>
																	<td class="text-center  measure_id" data-id="<?php echo $row['MEASURE_ID']; ?>"><?php echo $measureName; ?></td>
																	<td class="text-center  length"><?php echo $row['MLENGTH']; ?></td>
																	<td class="text-center  total_length"><?php echo $row['TOTAL_LENGTH']; ?></td>
																	<td class="text-center">
																		<a type="button"   id="view_button" onclick="edit_row(this);" value="O" title="Update" ><i class="fa fa-pencil"></i></a>
																		<a class="pointer" onclick="delete_row(this);" title="Delete"><i class="fa fa-times"></i></a>
																	</td>
															</tr>
<?php
				}
			}
?>
													</tbody>
													<tfoot>
														<tr class="totals">
																<td class="text-right"  colspan="3">TOTAL</td>
																<td class="text-center  total_thaan"></td>
																<td class="text-center" colspan="2">- - -</td>
																<td class="text-center  length_total"></td>
																<td class="text-center" >- - -</td>
														</tr>
													</tfoot>
											</table>
											<div class="clear mt-10"></div>
											<?php if(((in_array('emb-inward-modify',$permissionz) || $admin == true) && $id > 0) || $id == 0 ){ ?>
											<input type="button" class="button pull-right save_inward"  value="<?php echo $id>0?"Update":"Save"; ?>">
											<?php }?>
											<a href="emb-inward-details.php" class="button pull-left">New Form</a>
          	</div>
          </div><!--content-box-content-->
      </div><!--content-box-->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="popUpForm2" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    		<div id="form">
	      	<p>New Customer Title:</p>
	        <p class="textBoxGo">
	        	<input type="text" value="" name="customerTitle" class="input_size newCustomerTitle" />
	            <i class="fa fa-arrow-right"></i>
	        </p>
        </div>
    </div>
    <div id="popUpForm3" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
                <p>New Product Title:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="productTitle" class="input_size newProductTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div><!--popUpForm-->
</body>
</html>
<script>
	$(document).ready(function(){
			$('select').selectpicker();
			$("input.add_detail_row").click(function(){
				quick_save();
			});
			$("input.save_inward").click(function(){
				save_inward();
			});
			$("input.thaan,input.length").on("keyup change",function(){
				inputRowCalculation();
			});
			$("input[name=lot_num]").keydown(function(e){
				if(e.keyCode == 13){
					$("div.account_code button").focus();
				}
			});
			$("div.account_code button").keydown(function(e){
				if(e.keyCode == 13){
					$("div.product_id button").focus();
				}
			});
			$("div.product_id button").keydown(function(e){
				if(e.keyCode == 13){
					$("input.quality").focus();
				}
			});
			$("input.quality").keydown(function(e){
				if(e.keyCode == 13){
					$("input.color").focus();
				}
			});
			$("input.color").keydown(function(e){
				if(e.keyCode == 13){
					$("input.thaan").focus();
				}
			});
			$("input.thaan").keydown(function(e){
				if(e.keyCode == 13){
					$("div.measure_id button").focus();
				}
			});
			$("div.measure_id button").keydown(function(e){
				if(e.keyCode == 13){
					$("input.length").focus();
				}
			});
			$("input.length").keydown(function(e){
				if(e.keyCode == 13){
					$("input.add_detail_row").focus();
				}
			});
		});
</script>
