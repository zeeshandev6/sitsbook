<?php
	include('msgs.php');
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/suppliers.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');
	include('common/classes/sms_templates.php');
	include('common/classes/sitsbook_sms_api.php');

	//Permission
	if(!in_array('purchase',$permissionz) && $admin != true){
		linkBootstrap();
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts  = new ChartOfAccounts();
	$objInventory        = new inventory();
	$objInventoryDetails = new inventory_details();
	$objItems            = new Items();
	$objItemCategory     = new itemCategory();
	$objTaxRates         = new TaxRates();
	$objDepartments      = new Departments();
	$objSuppliers      	 = new suppliers();
	$objSmsTemplates     = new SmsTemplates();
	$objSitsbookSmsApi   = new SitsbookSmsApi();
	$objConfigs          = new Configs();

	$sms_config    		 = $objConfigs->get_config('SMS');

	if(isset($_POST['send_sms_by_id'])){
		if($sms_config == 'N'){
			exit();
		}
		$purchase_id 			= (int)mysql_real_escape_string($_POST['send_sms_by_id']);
		$purchase_details = $objInventory->getDetail($purchase_id);

		if(substr($purchase_details['SUPP_ACC_CODE'],0,6) == '040101'){
			$mobile_number = $objSuppliers->getMobileIfActive($purchase_details['SUPP_ACC_CODE']);
		}
		if($mobile_number==''){
			echo "Error! Mobile number not available.";
			exit();
		}

		$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
		$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');

		$purchase_date 		 = date("d-m-Y",strtotime($purchase_details['PURCHASE_DATE']));
		$account_code      = $purchase_details['SUPP_ACC_CODE'];
		$amount 					 = $objInventory->getSupplierAmountFromVoucher($purchase_id);
		$account_title 		 = $objChartOfAccounts->getAccountTitleByCode($purchase_details['SUPP_ACC_CODE']);

		$sms_template = $objSmsTemplates->get_template(5);
		$sms_body = $sms_template['SMS_BODY'];

		$sms_body = str_replace("[DATE]",$purchase_date,$sms_body);
		$sms_body = str_replace("[AMOUNT]",$amount,$sms_body);
		$sms_body = str_replace("[ACCOUNT_TITLE]",$account_title,$sms_body);
		$sms_body = str_replace("[PURCHASE_TYPE]",'A/c',$sms_body);

		$no_of_sms   = 0;
		$acc_id  	   = 0;
		$sent_status = $objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
		if(stripos($sent_status,'Successfully')!==false){
			$objInventory->setSmsStatus($purchase_id,'Y');
		}
		echo $sent_status;
		exit();
	}

	if(isset($_POST['get_history'])){
		$item_id 			 = (int)mysql_real_escape_string($_POST['get_history']);
		$account_code  = mysql_real_escape_string($_POST['account_code']);
		$rows    			 = $objInventoryDetails->getItemHistory($item_id,$account_code,4);
		echo json_encode($rows);
		exit();
	}

	$inventory         = NULL;
	$suppliersList     = $objInventory->getSuppliersList();
	$itemsCategoryList = $objItemCategory->getList();
	$taxRateList       = $objTaxRates->getList();

	$currency_type     = $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons       = $objConfigs->get_config('USE_CARTONS');
	$use_batch_no      = $objConfigs->get_config('USE_BATCH_NO');
	$use_tax    			 = $objConfigs->get_config('SHOW_TAX');
	$use_income_tax    = $objConfigs->get_config('INCOME_TAX');
	$purchase_subject  = $objConfigs->get_config('PURCHASE_SUBJECT');

	$last_purchase_hint        = $objConfigs->get_config('LAST_PURCHASE_HINT');

	$use_cartons_flag  = ($use_cartons == 'Y')?true:false;

	$invoice_format = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format = explode('_', $invoice_format);
	$invoiceSize 		= $invoice_format[0]; // S  L

	if($invoiceSize == 'L'){
		$invoiceFile = 'purchase-invoice.php';
	}elseif($invoiceSize == 'M'){
		$invoiceFile = 'purchase-invoice.php';
	}elseif($invoiceSize == 'S'){
		$invoiceFile = 'purchase-bill-small.php';
	}

	$purchase_id = 0;
	if(isset($_GET['id'])){
		$purchase_id = mysql_real_escape_string($_GET['id']);
		if($purchase_id != '' && $purchase_id != 0){
			$inventory 				= mysql_fetch_array($objInventory->getRecordDetails($purchase_id));
			$inventoryDetails = $objInventoryDetails->getList($purchase_id);
			$discountType     = $inventory['DISCOUNT_TYPE'];
		}else{
			$purchase_id = 0;
		}
	}
?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		div.tabPanel .tab,div.tabPanel .tabSelected{
			-webkit-box-sizing: content-box !important;
			-moz-box-sizing: content-box !important;
			box-sizing: content-box !important;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
		td,th{
			padding: 10px 5px !important;
			border:1px solid #CCC !important;
		}
		.caption{
			padding: 5px !important;
		}
		td.itemName{
			padding-left: 10px !important;
		}
		td.quantity{
			position: relative;
			cursor: pointer;
		}
		input.error{
			border-color: red !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/inventory.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<?php include("common/left_menu.php") ?>
	</div> <!-- End #sidebar -->
	<div class="content-box-top">
		<div class = "summery_body">
			<div class="content-box-header">
				<p>Purchase Management</p>
				<span id="tabPanel">
					<div class="tabPanel">
						<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
						?>
						<a href="inventory.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
						<a href="inventory.php?tab=search<?php echo $page; ?>"><div class="tab">Search</div></a>
						<div class="tabSelected">Details</div>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content" style="padding: 5px;" >
				<div id="bodyTab1">
					<div id="form" style="margin: 20px auto;">
						<div class="myCat"></div>

						<div class="pull-right" style="margin-right:25px;padding:5px;">
							<div class="pull-left" style="margin-right:25px;">Discount Type : </div>
							<input id="cmn-toggle-discount" name="disc_type" value="P" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" <?php echo ($discountType == 'P')?"checked":""; ?>  >
							<label for="cmn-toggle-discount" class="css-label-radio">Percentage</label>
							<input id="cmn-toggle-discount-amount" name="disc_type" value="R" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" <?php echo ($discountType == 'R')?"checked":""; ?>  >
							<label for="cmn-toggle-discount-amount" class="css-label-radio">Amount</label>
						</div>
						<div class="clear"></div>

						<div class="caption" style="width:120px;margin-left:0px;padding: 0px;">Purchase Date</div>
						<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
							<input type="text" name="rDate" value="<?php echo (isset($inventory))?date("d-m-Y",strtotime($inventory['PURCHASE_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
						</div>

						<div class="caption" style="width:135px;">Gate Pass #</div>
						<div class="field" style="width:100px">
							<input type="text" name="gpNum" value="<?php echo (isset($inventory))?$inventory['GP_NO']:""; ?>" class="form-control" style="width:100px" />
						</div>
						<?php
						if($use_batch_no=='Y'){
							?>
							<div class="caption">Batch #</div>
							<div class="field">
								<input type="text" name="batch_no" value="<?php echo (isset($inventory))?$inventory['BATCH_NO']:""; ?>" class="form-control" style="width:100px" />
							</div>
							<?php
						}
						?>

						<div class="pull-left">
							<div class="caption" style="width:120px;margin-left:0px;padding: 0px;">Bill#</div>
							<div class="field" style="width: 150px;">
								<input type="text" name="billNum" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:$objInventory->genBillNumber(); ?>" class="form-control" style="width:100px" />
							</div>
						</div>
						<div class="clear"></div>

						<?php if($show_vendor_bill_no=='Y'){ ?>
						<div class="caption" style="width:120px;margin-left:0px;">Vendor Bill #</div>
						<div class="field" style="width:150px">
							<input type="text" name="vendorBillNum" value="<?php echo (isset($inventory))?$inventory['VENDOR_BILL_NO']:""; ?>" class="form-control" />
						</div>
						<?php } ?>
						<?php
							if($purchaseOrdersModule == 'Y'){
						?>
						<div class="pull-left">
							<div class="caption" style="width:100px;">PO #</div>
							<div class="field" style="width: 150px;">
								<input type="text" name="po_number" value="<?php echo (isset($inventory))?$inventory['PO_NUMBER']:""; ?>" class="form-control" style="width:100px" />
							</div>
						</div>
						<?php
							}
						?>
						<div class="pull-left <?php echo ($saleTaxModule=='Y')?"":"hide"; ?>" style="margin-right:25px;padding-left:20px;padding-top: 10px;">
							<div class="caption" style="width:100px;margin-left:0px;padding: 0px;"></div>
							<div class="field" style="width: 150px;">
								<input id="cmn-toggle-unreg" value="Y" class="css-checkbox unregistered_tax" type="checkbox" <?php echo ($inventory['UNREGISTERED_TAX'] == 'Y')?"checked":""; ?> />
								<label for="cmn-toggle-unreg" class="css-label" style="margin-top:5px;margin-right: 20px;">Unregistered(FBR)</label>
							</div>
						</div>
						<div class="clear"></div>

						<div class="pull-left">
								<div class="caption" style="width:120px;margin-left:0px;">Account</div>
								<div class="field" style="width:270px;position:relative;">
									<a href="#" onclick="add_supplier();" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
									<select class="supplierSelector form-control "
									data-style="btn-default"
									data-live-search="true" data-hide-disabled='true' style="border:none" data-width="230" >
									<option selected value=""></option>
									<?php
									if(mysql_num_rows($suppliersList)){
										while($account = mysql_fetch_array($suppliersList)){
											$selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
											$disabled = (substr($inventory['SUPP_ACC_CODE'], 0,6)=='010101')?"disabled":"";
											?>
											<option <?php echo $selected; ?> <?php echo $disabled; ?> data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
											<?php
										}
									}
									?>
									<?php
									if(mysql_num_rows($cash_in_hand_list)){
										while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
											$cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
											if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']&&$allow_other_cash!='Y'){
												continue;
											}
											if($inventory != NULL){
												$disabled = (substr($inventory['SUPP_ACC_CODE'], 0,6)=='040101')?"disabled":"";
											}else{
												$disabled = 'disabled';
											}
											if($inventory != NULL){
												$selected = ($inventory['SUPP_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
											}else{
												$selected = "";
											}
											?>
											<option <?php echo $selected; ?> <?php echo $disabled; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
											<?php
										}
									}
									?>
								</select>
								<input type="hidden" class="purchase_id" value="<?php echo $purchase_id; ?>" />
								<input type="hidden" class="sms-config" value="<?php echo $sms_config; ?>" />
								<input type="hidden" class="use_cartons" value="<?php echo $use_cartons; ?>">
								<input type="hidden" class="use_tax" value="<?php echo $use_tax; ?>">
							</div>
						</div>

						<div class="clear"></div>

						<div class="pull-left">
							<div class="caption" style="width:120px;margin-left:0px;">
								<input type="checkbox" id="mor1" <?php echo (isset($inventory) && substr($inventory['SUPP_ACC_CODE'],0,6)== '010101')?"checked=\"checked\"":""; ?> onchange="makeItCash(this);" class="trasactionType css-checkbox" value="0101010001" title="Cash In Hand" />
								<label id="mor1" for="mor1" class="css-label" title="Ctrl+Space"><small>Cash</small></label>
							</div>
							<div class="field" style="width:270px;position:relative;">
								<input type="text" name="supplier_name" value="<?php echo (isset($inventory))?$inventory['SUPPLIER_NAME']:""; ?>" class="form-control" style="width:190px;" placeholder="Supplier Name" />
							</div>
						</div>

						<?php if($purchase_subject == 'Y'){ ?>
						<div class="caption" style="width:120px;margin-left:0px;">Narration</div>
						<div class="field" style="width:150px;margin-left:0px;padding: 0px;">
								<input type="text" name="subject" value="<?php echo $inventory['SUBJECT']; ?>" class="form-control" style="width:350px" />
						</div>
						<?php } ?>
						<div class="clear"></div>
						<div class="clear" style="height:30px;"></div>
					<div class="col-xs-12" style="padding:0px !important;">
					<table class="table min-1024">
						<thead>
							<tr>
								<th width="10%" style="font-size:12px;font-weight:normal;text-align:left">
									Item
									<a href="item-details.php" target="_blank"><i class="fa fa-plus pull-right" style="color:#06C;font-size:18px;margin-right:10px;"></i></a>
									<a href="#" class="reload_item"><i class="fa fa-refresh pull-right" style="color:#06C;font-size:18px;margin-right:10px;"></i></a>
								</th>
									<?php if($use_cartons_flag){ ?>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Cartons</th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">PerCarton</th>
									<?php } ?>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Unit Cost</th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Discount <?=($discountType == 'R')?".Rs":"%";?></th>
									<?php if($medicalStoreAddon=='N'||$use_tax=='Y'){ ?>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
									<?php } ?>
									<?php if($use_tax=='Y'){ ?>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
									<?php } ?>
									<th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Total Amount(<?php echo $currency_type; ?>) </th>
									<?php if($medicalStoreAddon=='Y'){ ?>
									<th width="5%" style="font-size:12px;font-weight:normal;text-align:center">Batch#</th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">ExpiryDate</th>
									<?php } ?>
									<th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">StockInHand</th>
									<th width="6%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
								</tr>
							</thead>
							<input type="hidden" class="discount_type" value="<?php echo $discountType; ?>" />
							<tbody>
								<tr class="quickSubmit" style="background:none">
									<td>
										<select class="itemSelector show-tick form-control"
										data-style="btn btn-default"
										data-live-search="true" style="border:none">
										<option selected value=""></option>
										<?php
										if(mysql_num_rows($itemsCategoryList)){
											while($ItemCat = mysql_fetch_array($itemsCategoryList)){
												$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
												?>
												<optgroup label="<?php echo $ItemCat['NAME']; ?>">
													<?php
													if(mysql_num_rows($itemList)){
														while($theItem = mysql_fetch_array($itemList)){
															if($theItem['ACTIVE'] == 'N'){
																continue;
															}
															if($theItem['INV_TYPE'] == 'B'){
																continue;
															}
															?>
															<option value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
															<?php
														}
													}
													?>
												</optgroup>
												<?php
											}
										}
										?>
									</select>
								</td>
								<?php if($use_cartons_flag){ ?>
									<td>
										<input type="text"  class="cartons text-center form-control"/>
									</td>
									<td>
										<input type="text"  class="per_carton text-center form-control" readonly/>
									</td>
									<?php } ?>
									<td>
										<input type="text"  class="quantity text-center form-control"/>
									</td>
									<td>
										<input type="text" class="unitPrice text-center form-control"/>
									</td>
									<td>
										<input type="text" class="discount text-center form-control"/>
									</td>
									<?php if($medicalStoreAddon=='N'||$use_tax=='Y'){ ?>
									<td>
										<input type="text"  readonly  value="0" class="subAmount text-center form-control"/>
									</td>
									<?php } ?>
									<?php if($use_tax=='Y'){ ?>
									<td style="padding:0; line-height:0;" class="taxTd">
										<input class="form-control text-center taxRate" value="" />
									</td>
									<td style="padding:0; line-height:0;">
										<input type="text"  readonly  value="0" class="taxAmount text-center form-control"/>
									</td>
									<?php } ?>
									<td>
										<input type="text"  readonly value="0" class="totalAmount text-center form-control"/>
									</td>
									<?php if($medicalStoreAddon=='Y'){ ?>
									<td class="batch_td">
										<input class="form-control text-center batch_no" value="" />
									</td>
									<td class="expiry_date">
										<input class="form-control text-left expiry_date date-pick" style="width: 100% !important;background-image: none !important;" value="" />
									</td>
									<?php } ?>
									<td>
										<input type="text" class="inStock text-center form-control" theStock="0" readonly />
									</td>
									<td><input type="button" class="insert_row" value="Enter" id="clear_filter" /></td>
								</tr>
							</tbody>
							<tbody>
								<!--doNotRemoveThisLine-->
								<tr style="display:none;" class="calculations"></tr>
								<!--doNotRemoveThisLine-->
								<?php
								if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
									while($invRow = mysql_fetch_array($inventoryDetails)){
										$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
										?>
										<tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
											<td style="text-align:left;"
											class="itemName"
											data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
											<?php echo $itemName; ?>
										</td>
										<?php if($use_cartons_flag){ ?>
											<td style="text-align:center;"  class="cartons"><?php echo $invRow['CARTONS'] ?></td>
											<td style="text-align:center;"  class="per_carton"><?php echo $invRow['PER_CARTON'] ?></td>
											<?php } ?>
											<td style="text-align:center;"  class="quantity"><?php echo $invRow['STOCK_QTY'] ?></td>
											<td style="text-align:center;"  class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
											<td style="text-align:center;"  class="discount"><?php echo $invRow['PURCHASE_DISCOUNT'] ?></td>
											<?php if($medicalStoreAddon=='N'||$use_tax=='Y'){ ?>
											<td style="text-align:center;"  class="subAmount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
											<?php } ?>
											<?php if($use_tax=='Y'){ ?>
											<td style="text-align:center;"  class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
											<td style="text-align:center;"  class="taxAmount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
											<?php } ?>
											<td style="text-align:center;"  class="totalAmount" data-sub-amount="<?php echo $invRow['SUB_AMOUNT']; ?>"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
											<?php if($medicalStoreAddon=='Y'){ ?>
											<td style="text-align:center;"  class="batch_no"><?php echo $invRow['BATCH_NO'] ?></td>
											<td style="text-align:center;"  class="expiry_date"><?php echo date('d-m-Y',strtotime($invRow['EXPIRY_DATE'])) ?></td>
											<?php } ?>
											<td style="text-align:center;"> - - - </td>
											<td style="text-align:center;">
												<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
												<a class="pointer" onclick="delete_detail_row(this);" title="Delete"><i class="fa fa-times"></i></a>
											</td>
										</tr>
										<?php
									}
								}
								?>
								<tr class="totals">
									<td style="text-align:center;background-color:#EEEEEE;">Total</td>
									<?php if($use_cartons_flag){ ?>
										<td style="text-align:center;background-color:#f5f5f5;" class="carton_total"></td>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<?php } ?>
										<td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<td style="text-align:center;background-color:#f5f5f5;" class="discountAmount"></td>
										<?php if($medicalStoreAddon=='N'||$use_tax=='Y'){ ?>
										<td style="text-align:center;background-color:#f5f5f5;" class="amountSub"></td>
										<?php } ?>
										<?php if($use_tax=='Y'){ ?>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<td style="text-align:center;background-color:#f5f5f5;" class="amountTax"></td>
										<?php } ?>
										<td style="text-align:center;background-color:#f5f5f5;" class="amountTotal"></td>
										<?php if($medicalStoreAddon=='Y'){ ?>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<?php } ?>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
										<td style="text-align:center;background-color:#EEE;">- - -</td>
									</tr>
								</tbody>
							</table>
							</div>
							<div style="height:10px;"></div>
							<div class="panel panel-default pull-left" style="width:550px;">
								<div class="panel-heading">Notes : </div>
								<textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $inventory['NOTES']; ?></textarea>
							</div>
							<div class="pull-right" style="padding: 0em 1em;width:450px;">
								<div class="pull-right">
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Discount :</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right discount_dom"  value="" placeholder="applies to all items" />
									</div>
									<div class="clear"></div>
									<div class="caption hide" style="padding: 5px;width: 180px;height: 30px;">Discount :</div>
									<div class="caption hide" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right whole_discount"  value="<?php echo $inventory['DISCOUNT']; ?>" />
									</div>
									<div class="clear"></div>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Total Discount :</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right total_discount"  value="<?php echo $inventory['TOTAL_DISCOUNT']; ?>" />
									</div>
									<div class="clear"></div>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Tax Amount :</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right whole_tax" readonly  value="" />
									</div>
									<div class="clear"></div>

									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right inv_charges"  value="<?php echo $inventory['CHARGES']; ?>" />
									</div>
									<div class="clear"></div>
	<?php 						if($use_income_tax == 'Y'){ 	?>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Income Tax Percent</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right income_tax_percent"  value="<?php echo $inventory['INCOME_TAX']; ?>" />
									</div>
									<div class="clear"></div>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Income Tax Amount</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right income_tax"  value="" readonly />
									</div>
									<div class="clear"></div>
	<?php 						} 														?>
									<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
									<div class="caption" style="width: 170px;margin-left:0px;">
										<input type="text" class="form-control text-right grand_total" readonly="readonly" />
									</div>
									<input type="hidden" value="" class="bildiscountamount"  />
									<div class="clear"></div>
								</div>
							</div>
							<div class="clear"></div>

							<div style="height:30px;"></div>
							<div class="clear"></div>
							<div class="hide">
								<div class="popover-content">

								</div>
							</div>
							<div class="underTheTable col-md-4 pull-right text-center">
								<?php if(($purchase_id > 0 && in_array('modify-purchase',$permissionz)) || $admin == true || $purchase_id == 0){ ?>
									<div class="button pull-right ml-10 savePurchase"><?php echo ($purchase_id > 0)?"Update":"Save"; ?></div>
									<?php } ?>
									<?php
									if($purchase_id > 0){
										?>
										<a class="button ml-10 pull-right" target="_blank" href="<?php echo $invoiceFile; ?>?id=<?php echo $purchase_id; ?>"><i class="fa fa-print"></i> Print</a>
									<?php
										if($sms_config=='Y'){
									 	?>
											<button class="button pull-left" onclick="send_sms_form(this,<?php echo $purchase_id ?>);"> <i class="fa fa-envelope"></i> Send SMS</button>
										<?php
										}
									}
									?>
									<div class="button ml-10 pull-right new_form_button" onclick="window.location.href='inventory-details.php';">New Form</div>
								</div><!--underTheTable-->
								<div class="clear"></div>
								<div style="margin-top:10px"></div>
							</div> <!-- End form -->
						</div> <!-- End #tab1 -->
					</div> <!-- End .content-box-content -->
				</div> <!-- End .summary_body -->
			</div> <!-- End .content-box -->
		</div><!--body-wrapper-->
		<div id="xfade" onclick="hidePopUpBox();" ></div>
		<div id="fade"></div>
		<div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
		</div>
		<div id="popUpForm" class="popUpFormStyle"></div>
	</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	setTimeout(function(){
			$("div.itemSelector").attr({'data-original-title':'Purchase History','data-placement':'bottom'})
		}
	,400);
	$(document).calculateColumnTotals();
	$("input[name='billNum']").numericOnly();
	$("input[name='gpNum']").numericOnly();
	$("input.quantity").numericFloatOnly();
	$("input.unitPrice").numericFloatOnly();
	$("input.discount").numericFloatOnly();
	$("#datepicker").setFocusTo("input[name='billNum']");
	$("input[name='gpNum']").setFocusTo("input[name='billNum']");
	$("input[name='billNum']").setFocusTo("input[name='vendorBillNum']");
	$("input[name='vendorBillNum']").setFocusTo("div.supplierSelector button");
	$("input[name='gpNum']").focus();

	$(document).calculateColumnTotals();

	// $("input.date-pick").datepicker({
	// 	dateFormat:'dd-mm-yy',
	// 	showAnim: 'show',
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	yearRange: '2000:+10',
	// 	onSelect: function(date) {
	//   	$("input.insert_row").focus();
	//   }
	// });

	$("select.supplierSelector").change(function(){
		if($("input[name=subject]").length){
			$("input[name=subject]").focus();
		}else{
			$("div.itemSelector button").focus();
		}
	});
	$("input[name=subject]").keydown(function(e){
		if(e.keyCode==13){
			$("div.itemSelector button").focus();
		}
	});
	$('select.itemSelector').change(function(){
		$(this).getItemDetails();
		<?php if($last_purchase_hint=='Y'){ ?>
		get_history();
		<?php } ?>
	});
	$('div.itemSelector button').keydown(function(e){
		if(e.keyCode == 13){
			$("div.itemSelector").popover('destroy');
		}
		if(e.keyCode == 13 && $('.itemSelector option:selected').val() != ''){
			if($(".updateMode").length == 0){
				$(this).getItemDetails();
			}
			if($("input.cartons").length){
				$("input.cartons").focus();
			}else{
				$("input.quantity").focus();
			}
		}
	});
	$("input.cartons,input.per_carton").keyup(function(){
		var cartons    = parseFloat($("input.cartons").val())||0;
		var per_carton = parseFloat($("input.per_carton").val())||0;
		$("input.quantity").val(cartons*per_carton);
	});
	$("input.cartons").keydown(function(e){
		if(e.keyCode == 13){
			//$("input.per_carton").focus();
			$("input.quantity").focus();
		}
	});
	$("input.per_carton").keydown(function(e){
		if(e.keyCode == 13){
			$("input.quantity").focus();
		}
	});
	$(document).keydown(function(e){
		if(e.keyCode == 32 && e.ctrlKey){
			$(".trasactionType").click();
			$(".customer_mobile").focus();
		}
	});
	$("input.quantity").setFocusTo("input.unitPrice");
	$("input.quantity").keyup(function(){
		stockOs();
	});
	$("input.unitPrice").keydown(function(e){
		if(e.keyCode == 13){
			$("input.discount").focus();
		}
	});
	$("input.discount").keydown(function(e){
		if(e.keyCode == 13){
			$(this).calculateRowTotal();
			if($("td.taxTd").find("input.taxRate").length){
				$("td.taxTd").find("input.taxRate").focus();
			}else if($("input.batch_no").length){
					$("input.batch_no").focus();
			 }else{
				 $("input.insert_row").focus();
			 }
		}
	});
	$("input.batch_no").keydown(function(e){
		if(e.keyCode==13){
			$("input.expiry_date").focus();
		}
	});
	$("input.expiry_date").keydown(function(e){
		if(e.keyCode==13){
			$("input.insert_row").focus();
		}
	});
	$("td.taxTd").find("input.taxRate").keydown(function(e){
		if(e.keyCode == 13){
			$(this).calculateRowTotal();
			if($("input.batch_no").length>0){
				$("input.batch_no").focus();
			}else{
				$("input.insert_row").focus();
			}
		}
	});
	$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
	$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");
	$("input.insert_row").keydown(function(e){
		if(e.keyCode == 13){
			$(this).quickSave();
		}
	});
	$("input.insert_row").click(function(){
		//$(this).quickSave();
	});
	$(".savePurchase").click(function(){
		savePurchase();
	});
	$(".reload_item").click(function(){
		$.get('inventory-details.php',{},function(data){
			$("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
		});
	});
	$(".whole_discount").on('keyup change',function(){
		$(this).calculateColumnTotals();
	});
	$(".discount_dom").on('blur change',function(){
		var item_discount,total_sub_amount,dominate_discount,discount_type;
		$(this).calculateColumnTotals();
		discount_type = $("input.discount_type:checked").val();
		total_sub_amount  = parseFloat($("td.amountSub").text())||0;
		dominate_discount = $(this).val();
		if(dominate_discount == ''||discount_type!='R'){
			return;
		}
		$("tr.transactions").each(function(){
			var taxRate = $(this).find("td.taxRate").text();
			var taxType = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).find("td.quantity").text())||0;
			var thatVal = parseFloat($(this).find("td.unitPrice").text())||0;
			var amount = thisVal*thatVal;
			var discountAvail = parseFloat($(this).find("td.discount").text())||0;

			item_discount = (amount*dominate_discount)/total_sub_amount;
			$(this).find("td.discount").text((item_discount).toFixed(0));

			var discountPerCentage = 0;
			if(discount_type == 'R'){
				discountPerCentage = discountAvail;
			}else if(discount_type == 'P'){
				discountPerCentage = amount*(discountAvail/100);
			}
			amount -= discountPerCentage;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount * (taxRate/100);
				}else if(taxType == 'E'){
					taxAmount = amount * (taxRate/100);
				}
			}else{
				taxAmount = 0;
			}
			var finalAmount = amount+taxAmount;
			if(taxType == 'I'){
				amount      -= taxAmount;
				finalAmount -= taxAmount;
			}
			$(this).find("td.subAmount").text((amount).toFixed(2));
			$(this).find("td.taxAmount").text((taxAmount).toFixed(2));
			$(this).find("td.totalAmount").text((finalAmount).toFixed(2));
		});
		$(this).calculateColumnTotals();
	});
	$("input.inv_charges").on('change keyup',function(e){
		$(this).calculateColumnTotals();
	});
	$("input.income_tax_percent").on('change keyup',function(e){
		$(this).calculateColumnTotals();
	});
	$(window).keydown(function(e){
		if(e.altKey == true&&e.keyCode == 73){
			e.preventDefault();
			$("#form").click();
			$("div.itemSelector button").click();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 65){
			e.preventDefault();
			$("#form").click();
			$("div.supplierSelector button").click();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 66){
			e.preventDefault();
			$("#form").click();
			$("input.barcode_input").focus();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 67){
			e.preventDefault();
			$("#form").click();
			$("input.supplier_name").focus();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 77){
			e.preventDefault();
			$("#form").click();
			$("input.customer_mobile").focus();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 68){
			e.preventDefault();
			$("#form").click();
			$("input.whole_discount").focus();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 79){
			e.preventDefault();
			$("#form").click();
			$("input.inv_charges").focus();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 83){
			e.preventDefault();
			$("#form").click();
			savePurchase();
			return false;
		}
		if(e.altKey == true&&e.keyCode == 80){
				e.preventDefault();
				$("#form").click();
				print_method = 'Y';
				savePurchase();
				return false;
		}
		if(e.altKey == true&&e.keyCode == 78){
				e.preventDefault();
				$(".new_form_button").click();
				return false;
		}
	});
<?php
  if(isset($_GET['print'])){
      $inv_idx = isset($_GET['method'])?(int)($_GET['method']):$inventory['ID'];
?>
  var id  = <?php echo $inv_idx; ?> ;
  var win = window.open("<?php echo $invoiceFile; ?>?id="+id,"_blank");
  if(win){
      win.focus();
  }
<?php
  }
?>
});
<?php
if(isset($_GET['saved'])){
	?>
	displayMessage('Bill Saved Successfully!');
	<?php
}
?>
<?php
if(isset($_GET['updated'])){
	?>
	displayMessage('Bill Updated Successfully!');
	<?php
}
?>

</script>
