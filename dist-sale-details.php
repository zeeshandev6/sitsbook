<?php
  include('common/connection.php');
  include('common/config.php');
  include('common/classes/dist_sale.php');
  include('common/classes/dist_sale_details.php');
  include('common/classes/sale_orders.php');
  include('common/classes/sale_order_details.php');
  include('common/classes/customers.php');
  include('common/classes/banks.php');
  include('common/classes/items.php');
  include('common/classes/services.php');
  include('common/classes/itemCategory.php');
  include('common/classes/tax-rates.php');

  //Permission
  if(!in_array('sales',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

  $objSale         			 = new DistributorSale();
  $objSaleDetails  			 = new DistributorSaleDetails();
  $objSaleOrders         = new SaleOrders();
  $objSaleOrderDetails   = new SaleOrderDetails();
  $objItems        			 = new Items();
  $objItemCategory 			 = new itemCategory();
  $objServices     			 = new Services();
  $objCustomers    			 = new Customers();
  $objBanks        			 = new Banks();
  $objTaxRates     			 = new TaxRates();
  $objConfigs      			 = new Configs();

  $user_name          = $objConfigs->get_config("USER_NAME");
  $stock_check        = $objConfigs->get_config("STOCK_CHECK");
  $sale_subject       = $objConfigs->get_config('SALE_SUBJECT');
  $cash_sales         = $objConfigs->get_config('CASH_SALES');
  $use_cartons        = $objConfigs->get_config('USE_CARTONS');
  $tooltip_cost_price = $objConfigs->get_config('TOOLTIP_COST_PRICE');
  $tax_invoice_date   = $objConfigs->get_config('TAX_INVOICE_START_DATE');
  $salesman_specific  = $objConfigs->get_config('SPECIFIC_SALESMAN');

  $order_date_session  = (isset($_SESSION['OSALE_DATE']))?$_SESSION['OSALE_DATE']:date('d-m-Y');
  $date_session        = (isset($_SESSION['SALE_DATE']))?$_SESSION['SALE_DATE']:date('d-m-Y');
  $order_taker_session = (isset($_SESSION['ORDER_TAKER_ID']))?$_SESSION['ORDER_TAKER_ID']:'';
  $salesman_id_session = (isset($_SESSION['SALESMAN_ID']))?$_SESSION['SALESMAN_ID']:'';

  if(isset($_POST['get_tax_invoice_bill'])){
    $objSale->with_tax   = mysql_real_escape_string($_POST['get_tax_invoice_bill']);
    $objSale->saleDate   = date('Y-m-d',strtotime($tax_invoice_date));
    echo $objSale->genBillNumber();
    exit();
  }

  $_SESSION['TOOLTIP_COST_PRICE'] = $tooltip_cost_price;
  $use_cartons_flag = ($use_cartons == 'Y')?true:false;
  if(isset($_GET['discount'])){
    $objConfigs->set_config($_GET['discount'],'DISCOUNT');
    exit();
  }
  $sms_config = $objConfigs->get_config('SMS');
  if(isset($_POST['mobile_number'])){
    if($sms_config == 'N'){
      echo $sms_config;
      exit();
    }

    $invoice_msg   = $objConfigs->get_config('INVOICE_MSG');
    $mobile_number = $_POST['mobile_number'];
    $sale_id       = $_POST['sale_id'];

    $saleDetails   = mysql_fetch_assoc($objSale->getRecordDetails($sale_id));
    $saleItems     = $objSaleDetails->getList($sale_id);

    $sms_search_vars = array();
    $sms_search_vars[] = '[INVOICE]';
    $sms_search_vars[] = '[DATE]';
    $sms_search_vars[] = '[AMOUNT]';

    $sms_replace_vars   = array();
    $sms_replace_vars[] = $saleDetails['BILL_NO'];
    $sms_replace_vars[] = date('d-m-Y',strtotime($saleDetails['SALE_DATE']));
    $sms_replace_vars[] = $saleDetails['BILL_AMOUNT'];

    $invoice_msg = str_replace($sms_search_vars, $sms_replace_vars, $invoice_msg);

    $options = array('location' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/soap-server.php',
    'uri' => 'http://sitsol.net/softwares/smsserver/admin/sms-link/');
    $api = new SoapClient(NULL, $options);
    try{
      echo $api->process($user_name,"N","",$mobile_number,$invoice_msg,0,0);
    }catch(Exception $e){
      echo "N";
    }
    exit();
  }

  $customersList       = $objCustomers->getList();
  $itemsCategoryList   = $objItemCategory->getList();
  $servicesList        = $objServices->getList();
  $taxRateList         = $objTaxRates->getList();
  $newBillNum          = $objSale->genBillNumber();
  $individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
  $currency_type       = $objConfigs->get_config('CURRENCY_TYPE');
  $individual_discount = ($individual_discount=='Y')?true:false;
  $use_taxes           = $objConfigs->get_config('SHOW_TAX');
  $use_taxes           = ($use_taxes=='Y')?true:false;

  $invoice_format      = $objConfigs->get_config('INVOICE_FORMAT');
  $print_meth          = $objConfigs->get_config('DIRECT_PRINT');
  $sale_scanner_append = $objConfigs->get_config('SALE_SCANNER_APPEND');
  $cash_at_bank_list   = $objBanks->getBanksListByType('M');
  $invoice_format      = explode('_', $invoice_format);
  $invoiceSize         = $invoice_format[0]; // S  L

  if($invoiceSize == 'L'){
    $invoiceFile = 'dist-sales-invoice.php';
  }elseif($invoiceSize == 'M'){
    $invoiceFile = 'dist-sales-invoice-duplicates.php';
  }elseif($invoiceSize == 'S'){
    $invoiceFile = 'dist-sales-invoice-small.php';
  }
  $scanner_toggle = $objConfigs->get_config('SALE_SCANNER');

  $sale_id   = 0;
  $inventory = NULL;

  if(isset($_GET['bill_num_param'])){
    $bn = (int)$_GET['bill_num_param'];
    $inventory = $objSale->getDetailByBillNumber($bn);
    $sale_id = (int)$inventory['ID'];
    $discountType = ($inventory['DISCOUNT_TYPE'] == '')?$discountType:$inventory['DISCOUNT_TYPE'];
    if($inventory['WITH_TAX']=="Y"){
        $invoiceFile = 'sales-tax-invoice.php';
    }
  }elseif(isset($_GET['id'])){
    $sale_id = mysql_real_escape_string($_GET['id']);
    if($sale_id != '' && $sale_id != 0){
      $inventory    = mysql_fetch_array($objSale->getRecordDetails($sale_id));
      $saleDetails  = $objSaleDetails->getList($sale_id);
      $discountType = ($inventory['DISCOUNT_TYPE'] == '')?$discountType:$inventory['DISCOUNT_TYPE'];
      if($inventory['WITH_TAX']=="Y"){
        $invoiceFile = 'sales-tax-invoice.php';
      }
    }else{
      $sale_id      = 0;
    }
  }
?>
<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice # <?php echo $inventory==NULL?$newBillNum:$inventory['BILL_NO']; ?></title>
<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css" media="screen" />
<style>
  .caption{
  	padding: 5px !important;
  }
  td.itemName{
  	padding-left: 10px !important;
  }
  td.quantity{
      position: relative;
      /*cursor: pointer;*/
  }
  td,th{
      padding: 10px 5px !important;
      border:1px solid #CCC !important;
  }
  input.error{
      background-color: rgba(255,0,0,0.1);
  }
</style>
<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="resource/scripts/configuration.js"></script>
<script type="text/javascript" src="resource/scripts/dist.sale.config.js"></script>
<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
<script type="text/javascript" src="resource/scripts/tab.js"></script>
<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
</head>
<body class="body_menu_close">
<input type="hidden" class="invoice-type"   value="<?php echo $invoiceSize; ?>">
<input type="hidden" class="scanner-append" value="<?php echo $sale_scanner_append; ?>">
<input type="hidden" class="stock_check"    value="<?php echo $stock_check; ?>">
<input type="hidden" class="use_cartons"    value="<?php echo $use_cartons; ?>">
<div id="body-wrapper">
  <div id="sidebar">
      <input type="hidden" class="print_method" value="<?php echo $print_meth; ?>" />
      <?php include("common/left_menu.php"); ?>
  </div> <!-- End #sidebar -->
  <div class="content-box-top" >
      <div class="content-box-header">
          <p>Sale Management</p>
          <span id="tabPanel">
              <div class="tabPanel">
                  <?php
                  			if(isset($_GET['page'])){
                  				$page = "&page=".$_GET['page'];
                  			}else{
                  				$page = '';
                  			}
                  ?>
                  <a href="dist-sale.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                  <a href="dist-sale.php?tab=search"><div class="tab">Search</div></a>
                  <div class="tabSelected">Details</div>
              </div>
          </span>
          <div class="clear"></div>
      </div> <!-- End .content-box-header -->
      <div class="content-box-content" style="padding: 5px;">
          <div id="bodyTab1">
              <div id="form" style="margin: 20px auto;">
                      <div class="pull-right" style="margin-right:25px;padding:5px;">
                          <div class="pull-left" style="margin-right:25px;">Discount Type : </div>
                          <input id="cmn-toggle-discount" name="disc_type" value="P" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" <?php echo ($discountType == 'P')?"checked":""; ?>  >
                          <label for="cmn-toggle-discount" class="css-label-radio">Percentage</label>
                          <input id="cmn-toggle-discount-amount" name="disc_type" value="R" class="css-checkbox discount_type" onclick="return discount_type_change(this);" type="radio" <?php echo ($discountType == 'R')?"checked":""; ?>  >
                          <label for="cmn-toggle-discount-amount" class="css-label-radio">Amount</label>
                      </div>
                      <div class="clear"></div>

                      <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Order Date</div>
                      <div class="field"   style="width:150px;margin-left:0px;padding: 0px;">
                          <input type="text" name="order_date" value="<?php echo ($inventory != NULL)?date("d-m-Y",strtotime($inventory['ORDER_DATE'])):date('d-m-Y',strtotime($order_date_session)); ?>" class="form-control datepicker" style="width:150px" />
                      </div>
                      <div class="caption" style="width:120px;margin-left:0px;padding: 0px;">Execution Date</div>
                      <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                          <input type="text" name="rDate" value="<?php echo ($inventory != NULL)?date("d-m-Y",strtotime($inventory['SALE_DATE'])):date('d-m-Y',strtotime($date_session)); ?>" class="form-control datepicker" style="width:150px" />
                      </div>
                      <div class="caption" style="width:70px;">Bill#</div>
                      <div class="field" style="width:100px">
                          <input type="text" name="billNum" value="<?php echo ($inventory != NULL && !isset($_GET['c']))?$inventory['BILL_NO']:$newBillNum; ?>" class="form-control" style="width:100px" />
                      </div>

                      <div class="caption" style="width:70px;">PO#</div>
                      <div class="field" style="width:100px">
                        <input type="text" name="po_number" value="<?php echo ($inventory != NULL)?$inventory['PO_NUMBER']:""; ?>" class="form-control" style="width:150px" />
                      </div>

                      <div class="hide">
                        <div class="field text-right" style="width:150px;">
                          <input id="cmn-toggle-taxinvoice" value="Y" class="css-checkbox tax_invoice" type="checkbox" <?php echo ($inventory['WITH_TAX'] == 'Y')?"checked":""; ?> />
                          <label for="cmn-toggle-taxinvoice" class="css-label" style="margin-top:5px;margin-right:-25px;">TAX Invoice</label>
                        </div>
                        <div class="field text-right" style="width:150px;">
                          <input id="cmn-toggle-registeredtax" value="Y" class="css-checkbox registered_tax" type="checkbox" <?php echo ($inventory['REGISTERED_TAX'] == 'Y')?"checked":""; ?> />
                          <label for="cmn-toggle-registeredtax" class="css-label" style="margin-top:5px;margin-right:-25px;">Registered</label>
                        </div>
                        <div class="caption" style="width:150px;">TaxInvoiceSerial#</div>
                        <div class="field" style="width:100px">
                            <input type="text" name="taxBillNum" value="<?php echo ($inventory != NULL && !isset($_GET['c']))?$inventory['TAX_BILL_NO']:''; ?>" class="form-control" style="width:100px" />
                        </div>
                        <div class="clear"></div>
                      </div>
                      <div class="clear"></div>
                      <hr />
<?php
                    if($salesman_specific=='Y'){
                      $sales_man_arr = array();
                      $users_list = $objAccounts->getActiveList();
                      if(mysql_num_rows($users_list)){
                        while($user = mysql_fetch_assoc($users_list)){
                          if($user['DESIGNATION_TYPE'] == ''){
                            //$user['DESIGNATION_TYPE'] = 'S';
                            continue;
                          }
                          if(!isset($sales_man_arr[$user['DESIGNATION_TYPE']])){
                            $sales_man_arr[$user['DESIGNATION_TYPE']] = array();
                          }
                          $sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
                        }
                      }
 ?>
                      <div class="caption" style="width:80px">OrderTaker</div>
                      <div class="field" style="width:270px;position:relative;">
                        <select class="order_taker" name="order_taker">
                          <option value=""></option>
                          <?php
                          if(isset($sales_man_arr['O'])){
                            foreach ($sales_man_arr['O'] as $key => $user){
                                $user_selected = $user['ID']==$order_taker_session?"selected":"";
                                $user_selected = ($inventory!=NULL&&$inventory['ORDER_TAKER']==$user['ID'])?"selected":$user_selected;
                                ?>
                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
                                <?php
                            }
                          }
                          ?>
                        </select>
                      </div>

                      <div class="caption" style="width:80px">Salesman</div>
                      <div class="field" style="width:270px;position:relative;">
                        <select class="user_id" name="salesman">
                          <option value=""></option>
                          <?php
                          if(isset($sales_man_arr['S'])){
                            foreach ($sales_man_arr['S'] as $key => $user){
                                $user_selected = $user['ID']==$salesman_id_session?"selected":"";
                                $user_selected = ($inventory!=NULL&&$inventory['USER_ID']==$user['ID'])?"selected":$user_selected;
                                ?>
                                <option <?php echo $user_selected; ?> value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME']." ".$user['LAST_NAME']."(".$user['USER_NAME'].")"; ?></option>
                                <?php
                            }
                          }
                          ?>
                        </select>
                      </div>

                      <div class="caption" style="width:80px">Sectors</div>
                      <div class="field" style="width:270px;position:relative;">
                        <select class="sector_selector form-control" name="sector_selector" data-live-search="true">
                          <option value=""></option>
                        </select>
                      </div>
                      <div class="clear"></div>
<?php               } ?>
                      <div class="caption" style="width:80px">Account</div>
                      <div class="field" style="width:270px;position:relative;">
                          <a href="#" data-toggle="modal" data-target="#addCustomerModal" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></a>
                          <select class="supplierSelector form-control show-tick"
                                  data-style="btn-default" data-width="230"
                                  data-live-search="true" data-hide-disabled='true' style="border:none;" >
                             <option selected value=""></option>
<?php
                      if(mysql_num_rows($customersList)){
                          while($account = mysql_fetch_array($customersList)){
                              if($inventory != NULL){
                                  $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":"";
                                  $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
                              }else{
                                  $disabled = "disabled";
                              }
                              $selected = ($inventory['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                             <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" data-sector="<?php echo $account['SECTOR']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?> ><?php echo  $account['CUST_ACC_TITLE']; ?></option>
<?php
                          }
                      }
                      if(mysql_num_rows($cash_in_hand_list)){
                          while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                              $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                              if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
                                  continue;
                              }
                              $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
                              $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"disabled":$disabled;
                              if($inventory != NULL){
                                  $selected = ($inventory['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
                              }else{
                                  $selected = ($cash_in_hand_acc_code==$cash_rows['CASH_ACC_CODE'])?"selected":"";
                              }
?>
                              <option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
<?php
                          }
                      }
                      if(mysql_num_rows($cash_at_bank_list)&&$admin){
                          while($cash_rows = mysql_fetch_array($cash_at_bank_list)){
                              if($inventory != NULL){
                                  $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"disabled":"";
                                  $disabled = (substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')?"disabled":$disabled;
                              }else{
                                  $disabled = "disabled";
                              }
                              $selected = ($inventory['CUST_ACC_CODE']==$cash_rows['ACCOUNT_CODE'])?"selected=\"selected\"":"";
?>
                              <option <?php echo $disabled; ?> <?php echo $selected; ?> data-subtext="<?php echo $cash_rows['ACCOUNT_CODE']; ?>" value="<?php echo $cash_rows['ACCOUNT_CODE']; ?>"><?php echo $cash_rows['ACCOUNT_TITLE']; ?></option>
<?php
                          }
                      }
?>
                          </select>
				<input type="hidden" class="sale_id" value="<?php echo (isset($_GET['c']))?0:$sale_id; ?>" />
                      <input type="hidden" class="individual_discount" value="<?php echo ($individual_discount)?"Y":"N"; ?>" />
                      <input type="hidden" class="use_taxes" value="<?php echo ($use_taxes)?"Y":"N"; ?>" />
                      </div>
                      <div class="caption" style="width: 550px;">
                    	<div class="pull-left" style="padding-top:5px;">
                              <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="A" id="radio1" <?php echo ((substr($inventory['CUST_ACC_CODE'], 0,6)=='010104') || ($inventory == NULL && $cash_sales == 'N'))?"checked":''; ?> class="css-checkbox" />
                              <label for="radio1" class="css-label-radio radGroup1">Account</label>
                              <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="C" id="radio2" <?php echo ((substr($inventory['CUST_ACC_CODE'], 0,6)=='010101')|| ($inventory == NULL && $cash_sales == 'Y'))?"checked":''; ?> class="css-checkbox" />
                              <label for="radio2" class="css-label-radio radGroup1">Cash</label>
                              <input type="radio" name="radiog_dark" onchange="makeItCash(this);" value="B" id="radio3" <?php echo (substr($inventory['CUST_ACC_CODE'], 0,6)=='010102')?"checked":''; ?> class="css-checkbox" />
                              <label for="radio3" class="css-label-radio radGroup1">Card</label>
                          </div>
                        <div class="pull-left" style="margin-left:10px;">
                                <input type="text" name="supplier_name"  value="<?php echo ($inventory != NULL)?$inventory['CUSTOMER_NAME']:"Walk in Customer"; ?>" class="form-control supplier_name" style="width:190px;text-align:center;display:<?php echo (substr($inventory['CUST_ACC_CODE'], 0,6)=='010104')?"none":""; ?>;" placeholder="Customer Name" />
                        </div>
                          <?php if($sms_config == 'Y'){ ?>
                              <div class="pull-left" style="margin-left:10px;width:125px;">
                                  <input type="text" class="customer_mobile form-control text-center" value="<?php echo ($inventory != NULL)?$inventory['MOBILE_NO']:""; ?>" placeholder="Mobile Number" maxlength="11" />
                              </div>
                          <?php } ?>
                    </div>
                      <div class="clear"></div>
                      <?php if($sale_subject == 'Y'){ ?>
                      <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Narration</div>
                      <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                          <input type="text" name="subject" value="<?php echo $inventory['SUBJECT']; ?>" class="form-control" style="width:350px" />
                      </div>
                      <?php } ?>
                      <div class="clear"  style="height:30px;"></div>
                      <div class="panel panel-default" style="padding:0em;border:none;box-shadow:none;display:<?php echo ($scanner_toggle=='Y')?"":"none"; ?>">
                              <div id="form" class="scanner_div" style="text-align: center;margin:0px;;">
                                  <input type="text" class="barcode_input form-control" value="" placeholder="Scan Barcode" style="height:50px;font-size:26px;">
                              </div> <!-- End form -->
                          <div class="clear"></div>
                      </div>
                      <table class="prom">
                      <thead>
                          <tr>
                             <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">
                                  Item
                                  <a href="#" class="reload_item"><i class="fa fa-refresh pull-right" style="color:#06C;font-size:18px;margin-right:10px;"></i></a>
                             </th>
                             <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Cartons</th>
                             <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Rate</th>
                             <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Dozens</th>
                             <th width="7%"  style="font-size:12px;font-weight:normal;text-align:center">Doz.Rate</th>
                             <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Peice</th>
                             <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                             <?php if($individual_discount){ ?>
                             <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Discount</th>
                             <?php } ?>
                             <?php if($use_taxes){ ?>
                             <th width="7%" style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                             <?php } ?>
                             <th width="7%" style="font-size:12px;font-weight:normal;text-align:center">Total Amount(<?php echo $currency_type; ?>)</th>
                             <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">StockInHand</th>
                             <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="quickSubmit" style="background:none">
                              <td>
                                  <select class="itemSelector show-tick form-control"
                                          data-style="btn-default"
                                          data-live-search="true" style="border:none">
                                     <option selected value=""></option>
<?php
                            if(mysql_num_rows($itemsCategoryList)){
                                  while($ItemCat = mysql_fetch_array($itemsCategoryList)){
                                    $itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
									                     <optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
                        								if(mysql_num_rows($itemList)){
                        									while($theItem = mysql_fetch_array($itemList)){
                        										if($theItem['ACTIVE'] == 'N'){
                        											continue;
                        										}
                        										if($theItem['INV_TYPE'] == 'B'){
                        											continue;
                        										}
?>
                                          <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
									                       }
								                       }
?>
									                   </optgroup>
<?php
                                  }
                              }
?>
<?php
                              if(mysql_num_rows($servicesList)){
                                  ?><optgroup label="Services"><?php
                                  while($srv = mysql_fetch_assoc($servicesList)){
?>
                                      <option data-type="S" value="<?php echo $srv['ID']; ?>"><?php echo $srv['TITLE']; ?></option>
<?php
                                  }
                                  ?></optgroup><?php
                              }
?>
                                  </select>
                              </td>
                              <td>
                                  <input type="text" class="cartons form-control text-center"/>
                                  <input type="hidden" class="qty_carton" value="" />
                              </td>
                              <td>
                                  <input type="text"   class="rate_carton form-control text-center"/>
                                  <input type="hidden" class="carton_sub_amount" />
                              </td>
                              <td>
                                  <input type="text" class="dozen form-control text-center"/>
                                  <input type="hidden" class="dozen_size" value="12" />
                              </td>
                              <td>
                                  <input type="text"   class="dozen_price form-control text-center"/>
                                  <input type="hidden" class="dozen_sub_amount" />
                              </td>
                              <td>
                                  <input type="text" class="quantity form-control text-center"/>
                              </td>
                              <td>
                                  <input type="text" class="unitPrice form-control text-center" data-toggle="focus" />
                              </td>
                              <?php if($individual_discount){ ?>
                              <td>
                                  <input type="text" class="discount form-control text-center" value="" />
                              </td>
                              <?php } ?>
                              <?php if($use_taxes){ ?>
                              <td class="taxTd">
                              	<input type="text" class="form-control taxRate text-center" value=""  />
                              </td>
                              <?php } ?>
                              <td>
                                  <input type="text"  readonly value="0" class="totalAmount form-control text-center"/>
                              </td>
                              <td>
                                  <input type="text" class="inStock form-control text-center" theStock="0" readonly />
                              </td>
                              <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                          </tr>
                      </tbody>
                      <tbody>
                      	<!--doNotRemoveThisLine-->
                              <tr style="display:none;" class="calculations"></tr>
                          <!--doNotRemoveThisLine-->
<?php
                  $whole_tax = 0;
			if(isset($saleDetails) && mysql_num_rows($saleDetails)){
				while($invRow = mysql_fetch_array($saleDetails)){
                          if($invRow['SERVICE_ID'] > 0){
                              $item_id  = $invRow['SERVICE_ID'];
                              $itemName = $objServices->getTitle($item_id);
                              $item_type= 'S';
                          }else{
                              $item_id    = $invRow['ITEM_ID'];
                              $itemDetail = $objItems->getRecordDetails($invRow['ITEM_ID']);
                              $itemName   = $itemDetail['NAME'];
                              $item_type  = 'I';
                          }
?>
                          <tr class="alt-row calculations transactions dynamix" data-row-id='<?php echo $invRow['ID']; ?>'>
                              <td style="text-align:left;" class="itemName"data-type='<?php echo $item_type; ?>' data-item-id='<?php echo $item_id; ?>'><?php echo $itemName; ?></td>
                              <td style="text-align:center;" class="cartons" data-qty="<?php echo $itemDetail['QTY_PER_CARTON'] ?>"><?php echo $invRow['CARTONS'] ?></td>
                              <td style="text-align:center;" class="rate_carton"><?php echo $invRow['RATE_CARTON'] ?></td>
                              <td style="text-align:center;" class="dozen"><?php echo $invRow['DOZENS'] ?></td>
                              <td style="text-align:center;" class="dozen_price"><?php echo $invRow['DOZEN_PRICE'] ?></td>
                              <td style="text-align:center;" class="quantity"><?php echo $invRow['QUANTITY'] ?></td>
                              <td style="text-align:center;" sub-amount="<?php echo $invRow['SUB_AMOUNT'] ?>" class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                              <?php if($individual_discount){ ?>
                              <td style="text-align:center;" class="discount" data-discount-amount="<?php echo $invRow['SALE_DISCOUNT']; ?>"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                              <?php } ?>
                              <?php if($use_taxes){ ?>
                              <td style="text-align:center;" tax-amount="<?php echo $invRow['TAX_AMOUNT'] ?>" class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                              <?php } ?>
                              <td style="text-align:center;" class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                              <td style="text-align:center;"> - - - </td>
                              <td style="text-align:center;">
                                 <a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
                                 <a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>
                              </td>
                          </tr>
<?php
                          $whole_tax += $invRow['TAX_AMOUNT'];
				}
			}
?>
                          <tr class="totals">
                              	<td style="text-align:center;background-color:#EEEEEE;">Total</td>
                                  <td style="text-align:center;background-color:#f5f5f5;" class="carton_total"></td>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <td style="text-align:center;background-color:#f5f5f5;" class="dozen_total"></td>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <?php if($individual_discount){ ?>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <?php } ?>
                                  <?php if($use_taxes){ ?>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <?php } ?>
                                  <td style="text-align:center;background-color:#f5f5f5;" class="amountTotal"></td>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                                  <td style="text-align:center;background-color:#EEE;">- - -</td>
                              </tr>
                      </tbody>
                  </table>
                  <div class="clear" style="height:10px;"></div>
                  <div class="col-xs-5">
                      <div class="panel panel-default">
                          <div class="panel-heading">Notes : </div>
                          <textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $inventory['NOTES']; ?></textarea>
                      </div>
                  </div>
                  <div class="pull-right" style="padding: 0em 1em;width:auto;">
                      <div class="pull-right">
                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Discount :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right discount_dom" placeholder="apply to all items"  value="" />
                          </div>
                          <div class="clear"></div>

                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Trade Off :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right trade_offer" placeholder=""  value="<?php echo $inventory['TRADE_OFFER'];?>" />
                          </div>
                          <div class="clear"></div>

                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Total Discount :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right total_discount"  value="<?php echo $inventory['SALE_DISCOUNT']; ?>" />
                          </div>
                          <div class="clear"></div>
                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Tax Amount :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right whole_tax" readonly  value="<?php echo isset($whole_tax)?$whole_tax:0; ?>" />
                          </div>
                          <div class="clear"></div>
                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right inv_charges"  value="<?php echo $inventory['CHARGES']; ?>" />
                          </div>
                          <div class="clear"></div>

                      	<div class="caption" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
                      	<div class="caption" style="width: 150px;margin-left:0px;">
                      		<input type="text" class="form-control text-right grand_total" readonly="readonly" />
                      	</div>
                      	<div class="clear"></div>

                          <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Tendered :</div>
                          <div class="caption" style="width: 150px;margin-left:0px;">
                              <input type="text" class="form-control text-right received_cash" value="<?php echo $inventory['RECEIVED_CASH']; ?>" />
                          </div>
                          <div class="clear"></div>

                          <div class="balance-of-customer">
                              <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Balance Receivable :</div>
                              <div class="caption" style="width: 150px;margin-left:0px;">
                                  <input type="text" class="form-control customer-balance text-right" readonly />
                              </div>
                              <div class="clear"></div>
                          </div>

                          <div class="return-to-customer">
                              <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Change Given :</div>
                              <div class="caption" style="width: 150px;margin-left:0px;">
                                  <input type="text" class="form-control text-right change_return" value="<?php echo $inventory['CHANGE_RETURNED']; ?>" readonly />
                              </div>
                              <div class="clear"></div>
                          </div>

                          <div class="recovered-from-customer">
                              <div class="caption" style="padding: 5px;width: 180px;height: 30px;">
                                  Balance Recovered :
                                  <input id="cmn-toggle-recovered" value="X" class="css-checkbox recovered_balance" onclick="return recover_balance(this);" type="checkbox" <?php echo ($inventory['RECOVERED_BALANCE'] == 'Y')?"checked":""; ?> />
                                  <label for="cmn-toggle-recovered" class="css-label" style="margin-top:5px;margin-right:-25px;"></label>
                              </div>
                              <div class="caption" style="width: 150px;margin-left:0px;">
                                  <input type="text" class="form-control text-right recovery_amount" value="<?php echo $inventory['RECOVERY_AMOUNT']; ?>" readonly />
                              </div>
                              <div class="clear"></div>
                          </div>

                          <div class="remains-to-customer" style="display:<?php echo ((substr($inventory['CUST_ACC_CODE'], 0,6)!='010101')||$inventory==NULL)?"":"none"; ?>;">
                              <div class="caption" style="padding: 5px;width: 180px;height: 30px;">Amount Receivable :</div>
                              <div class="caption" style="width: 150px;margin-left:0px;">
                                  <input type="text" class="form-control text-right remaining_amount" value="<?php echo $inventory['REMAINING_AMOUNT']; ?>" readonly />
                              </div>
                              <div class="clear"></div>

                          </div>
                      </div>
                  </div>
                  <div class="clear" style="height: 20px;"></div>
                  <div class="underTheTable col-md-5 pull-right" style="text-align: right;padding-right:30px;">
                      <?php
                          if(($sale_id > 0 && in_array('modify-sales',$permissionz)) || $admin == true || $sale_id == 0){
                      ?>
                          <?php if($sms_config == 'Y'&&$sale_id > 0){ ?>
                              <div class="button btn-sm" onclick="sendSmsPopUp();" > <i class="fa fa-commenting-o" style="font-size:1.8em;"></i></div>
                          <?php
                              }
                          ?>
                      <?php
                          }
                      ?>
                <?php
                	if($inventory != NULL){
                ?>
                    <a class="button print_that" target="new" href="<?php echo $invoiceFile; ?>?id=<?php echo $inventory['ID']; ?>"><i class="fa fa-print"></i> Print</a>
                <?php
					        }
                  if(($sale_id > 0 && in_array('modify-sales',$permissionz)) || $admin == true || $sale_id == 0){
                    if(($inventory==NULL||(isset($_GET['c'])))){
                ?>
                    <button class="button save_sale"><?php echo ($inventory==NULL||(isset($_GET['c'])))?"Save &amp; New":""; ?></button>
              <?php } ?>
                    <button class="button save_sale_only"><?php echo ($inventory==NULL||(isset($_GET['c'])))?"Save":"Update"; ?></button>
                    <button class="button save_print_sale"><?php echo ($inventory==NULL||(isset($_GET['c'])))?"Save":"Update"; ?>  &amp; Print</button>
                <?php
                  }
                ?>
                  <div class="button" onclick="window.location.href='dist-sale-details.php';">New Form</div>
                  <div class="clear"></div>
                </div><!--underTheTable-->
                <div class="clear"></div>
                <div style="margin-top:10px"></div>
		 </div> <!-- End form -->
          </div> <!-- End #tab1 -->
      </div> <!-- End .content-box-content -->
  </div> <!-- End .content-box -->
</div><!--body-wrapper-->
<div id="xfade"></div>
<div id="fade"></div>
<!-- Modal -->
<div id="addCustomerModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Customer</h4>
      </div>
      <div class="modal-body">
        <form action="/action_page.php">
            <div class="form-group">
                <label for="customer_name">Customer Title:</label>
                <input type="text" class="form-control" id="customer_name" name="customer_name">
            </div>
            <div class="form-group">
                <label for="sectors">Sectors:</label>
                <select class="modal_sectors" id="sectors">
                    <option value=""></option>
                    <?php 
                        $sectors_array = $objCustomers->get_sector_array();
                        foreach ($sectors_array as $key => $sector_name) {
                    ?>
                    <option value="<?=$sector_name;?>"><?=$sector_name;?></option>
                    <?php 
                        }
                    ?>
                </select>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right addNewCustomer">Save</button>
      </div>
    </div>
  </div>
</div>
<div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
<div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
  $(".customer_mobile").bind('keyup blur',function(e){
      if($(this).val().substr(0,1) != 0 || ($(this).val().length > 1 && $(this).val().substr(1,1) != 3)){
      	$(".customer_mobile").val('');
      }
  });

  if($("select.supplierSelector option[data-sector]").length){
    $("select.sector_selector").selectpicker();
    $("select.supplierSelector option[data-sector]").each(function(){
      if($("select.sector_selector option[value='"+$(this).attr('data-sector')+"']").length){
        return;
      }
      $(this).clone().appendTo("select.sector_selector");
      $("select.sector_selector option").last().val($(this).attr('data-sector')).text($(this).attr('data-sector'));
    });
    $("select.sector_selector option[data-sector='"+$("select.supplierSelector option:selected").attr('data-sector')+"']").prop("selected",true);
    $("select.sector_selector option").attr('data-subtext','');
    $("select.sector_selector option").prop('disabled',false);
    $("select.sector_selector").selectpicker('refresh');
    $("select.sector_selector").change(function(){
      if($(this).find("option:selected").val() == ''){
        makeItCash($("input[name=radiog_dark]:checked"));
      }else{
        $("select.supplierSelector option[data-sector='"+$(this).find("option:selected").val()+"']").prop("disabled",false);
        $("select.supplierSelector option[data-sector!='"+$(this).find("option:selected").val()+"']").prop("disabled",true)
        $("select.supplierSelector").selectpicker('refresh');
      }
    });
  }else{
    $("select.sector_selector").remove();
  }
  $(".reload_item").click(function(){
      $.get("<?php echo basename($_SERVER['PHP_SELF']); ?>",{},function(data){
           $("select.itemSelector").html($(data).find("select.itemSelector").html()).selectpicker('refresh');
      });
  });
  $(window).load(function(){
    <?php if(isset($_GET['method'])){ ?>
        var method_id = <?php echo (int)$_GET['method']; ?>;
        $.get("?",{id:method_id},function(html_data){
            $("select.supplierSelector").selectpicker('val',$(html_data).find("select.supplierSelector option:selected").val());
            $("select.sector_selector").selectpicker("val",'');
            $("select.sector_selector option[data-sector='"+$("select.supplierSelector option:selected").attr("data-sector")+"']").prop("selected",true);
            $("select.sector_selector").selectpicker("refresh");
            $("select.sector_selector").trigger("change");
            $("select.supplierSelector").selectpicker("val",'');
        })
    <?php } ?>
  });
  $("input.tax_invoice").change(function(){
    var use_tax = $(this).is(":checked")?"Y":"N";
    if(use_tax == 'Y'){
      $("input.registered_tax").prop('checked',true);
    }else{
      $("input.registered_tax").prop('checked',false);
    }
    $.post('<?php echo basename($_SERVER['PHP_SELF']); ?>',{get_tax_invoice_bill:use_tax},function(data){
      data = parseInt(data)||0;
      if(use_tax == 'Y'){
        $("input[name=taxBillNum]").val(data);
      }else{
        $("input[name=taxBillNum]").val('');
      }
    });
  });
$('select').selectpicker();
$(this).calculateColumnTotals();
  $("label#mor1").tooltipster({
      theme: 'tooltipster-light'
  });
  $("input[name=radiog_dark]").change(function(){
      if($(this).val() == 'C'){
          $("div.remains-to-customer").hide();
      }else{
          $("div.remains-to-customer").show();
      }
  });
  $("input.received_cash").change(function(){
      $(this).calculateColumnTotals();
  });
  makeItCash($("input[name=radiog_dark]:checked"));
$("input[name='billNum']").numericOnly();
$("input.quantity").numericOnly();
$("input[name='mobile_no']").numericOnly();
$(".customer_mobile").numericOnly();
$("input.unitPrice").numericFloatOnly();
$("input.discount").numericFloatOnly();
$("input.order_date").setFocusTo("input.sale_date");
$("input.sale_date").setFocusTo("input[name='billNum']");
$("input[name='billNum']").setFocusTo(".dropdown-toggle:first");
$(".supplierSelector").change(function(){
      var cust_code = $(".supplierSelector option:selected").val();
      if(cust_code != ''){
          $.get('db/get-customer-details.php',{acc_code:cust_code},function(data){
              if(data != ''){
                  data = $.parseJSON(data);
                  if($(".customer_mobile").length){
                  	$(".customer_mobile").val(data['MOBILE']).focus();
                  }
              }
          });
          $("div.supplierSelector button").removeClass("btn-warning");
      }
});
$("div.supplierSelector").keydown(function(e){
    if(e.keyCode == 13 && $("select.supplierSelector").val()!=''){
        $("div.itemSelector button").focus();
    }
});
  $(".customer_mobile").setFocusTo("div.itemSelector button");
$("div.itemSelector button").keyup(function(e){
      if(e.keyCode == 13 &&  $('select.itemSelector option:selected').attr('data-type') == 'S'){
          $("input.unitPrice").focus();
      }else if(e.keyCode == 13 && $('select.itemSelector option:selected').val() != ''){
          if($("input.cartons").length){
              $("input.cartons").focus();
          }else{
              $("input.quantity").focus();
          }
          $("div.itemSelector button").removeClass("btn-warning");
}
});
$("input.cartons,input.rate_carton").keyup(function(){
    var cartons    = parseFloat($("input.cartons").val())||0;
    var rate_carton = parseFloat($("input.rate_carton").val())||0;
    $("input.carton_sub_amount").val(cartons*rate_carton);
    stockOs();
});
$("input.dozen,input.dozen_price").keyup(function(){
    var dozen    = parseFloat($("input.dozen").val())||0;
    var dozen_price = parseFloat($("input.dozen_price").val())||0;
    $("input.dozen_sub_amount").val(dozen*dozen_price);
    stockOs();
});
$("input.cartons").keydown(function(e){
    if(e.keyCode == 13){
        $("input.rate_carton").focus();
    }
});
$("input.rate_carton").keydown(function(e){
    if(e.keyCode == 13){
        $("input.dozen").focus();
    }
});
$("input.dozen").keydown(function(e){
    if(e.keyCode == 13){
        $("input.dozen_price").focus();
    }
});
$("input.dozen_price").keydown(function(e){
    if(e.keyCode == 13){
        $("input.quantity").focus();
    }
});
$(document).keydown(function(e){
    if(e.keyCode == 32 && e.ctrlKey){
        $(".trasactionType").click();
        $(".supplier_name").focus();
    }
});
$("input.barcode_input").on('blur keyup',function(e){
    if(e.type == 'blur'||e.keyCode == 13){
        $(this).quickScan();
    }
});
$("div.itemSelector button").blur(function(e){
if($('select.itemSelector option:selected').val() != ''
          && $('select.itemSelector option:selected').attr('data-type') != 'S'){
	$(this).getItemDetails();
}
});
var qtyE = 0;
$("input.quantity").keyup(function(e){
    stockOs();
});
$("input.quantity").keydown(function(e){
if(e.keyCode == 13){
	$("input.unitPrice").focus();
}
});
$("tr.quickSubmit input[type=text]").on('change keyup',function(e){
    $(this).calculateRowTotal();
});
$("input.unitPrice").keydown(function(e){
  if(e.keyCode==67){
    if($("input.unitPrice[data-original-title]").length){
      $("input.unitPrice").tooltip('destroy');
      $("input.unitPrice").removeAttr('data-original-title');
    }else{
      $("input.unitPrice").attr('title',$("input.unitPrice").attr('data-title'));
      $("input.unitPrice").tooltip();
      $("input.unitPrice").trigger('focus');
    }
  }
if(e.keyCode == 13){
          if($("input.discount").length){
              $("input.discount").focus();
          }else{
              if($("input.taxRate").length){
                  $("input.taxRate").focus();
              }else{
                  $(this).calculateRowTotal();
                  $(".addDetailRow").focus();
              }

          }
}
});
  $("table").on('dblclick',"td.quantity",function(){
      return;
      var thisElm   = $(this);
      var item_type = $(this).parent().find("[data-item-id]").attr("data-type");
      var thisQty   = parseInt($(this).text())||0;
      var item_id   = parseInt($(this).parent().find("[data-item-id]").attr("data-item-id"))||0;
      var itemPrice = parseFloat($(this).parent().find("td.unitPrice").text())||0;
      if(item_type == 'S'){
          parseInt($(this).parent().find("td[data-item-id]").effect("highlight"));
          return;
      }
      $.post('db/get-item-details.php',{s_item_id:item_id},function(data){
          data = $.parseJSON(data);
          var itemStock = parseInt(data['STOCK'])||0;
          var this_sold = 0;
          $("td[data-item-id='"+item_id+"']").each(function(){
              this_sold += parseInt($(this).parent().find("td.quantity").text())||0;
          });
          itemStock += thisQty;
          itemStock -= this_sold;
          $(thisElm).append('<input class="form-control input-nested" value="'+thisQty+'" />');
          $(thisElm).find(".input-nested").focus();
          $(thisElm).find(".input-nested").on('keyup blur',function(e){
              stockOsQuick($(this),itemPrice,itemStock);
              var newQty = parseInt($(this).val())||0;
              if(e.keyCode == 13||e.type == 'blur'){
                  if(newQty!=0){
                      $(this).parent().text(newQty);
                      $(this).calculateColumnTotals();
                  }
              }
          });
      });
  });
  $("input.inv_charges").on('change keyup',function(e){
      $(this).calculateColumnTotals();
  });
    $("input.discount").keydown(function(e){
        if(e.keyCode == 13){
            $(this).calculateRowTotal();
            if($("input.taxRate").length){
                    $("input.taxRate").focus();
                }else{
                    $(".addDetailRow").focus();
                }
        }
    });
    $("input.taxRate").keydown(function(e){
        if(e.keyCode == 13){
            $(this).calculateRowTotal();
            $(".addDetailRow").focus();
        }
    });
    $(".taxTd").find(".taxRate").change(function(){
        $(this).calculateRowTotal();
        $(".addDetailRow").focus();
    });
$(".addDetailRow").keydown(function(e){
if(e.keyCode == 13){
	$(this).quickSave();
}
if(e.keyCode == 27){
	$(".taxTd").find(".dropdown-toggle").focus();
}
});
$(".addDetailRow").dblclick(function(e){
e.preventDefault;
});
$(".save_sale").click(function(){
  print_method    = 'N';
  redirect_method = 'N';
  saveSale();
});
$(".save_print_sale").click(function(){
  if($("input[name=radiog_dark]:checked").val() == 'C'){
      print_method    = 'Y';
      redirect_method = 'N';
  }
  saveSale();
});
$(".save_sale_only").click(function(){
    save_only = true;
    saveSale();
});
$("input[name='mobile_no']").focus(function(){
check_sms_service();
});

  $(".whole_discount").on('blur change keyup',function(){
      $(this).calculateColumnTotals();
  });
  $(".whole_discount").blur();

  $("input.trade_offer").on("keyup change",function(){
      $(this).calculateColumnTotals();
  });

  $(".discount_dom").on('blur change',function(){
      var discount_type = $("input.discount_type:checked").val();
      var dominate_discount = $(this).val();
      var amountTotal       = 0;

      if(dominate_discount == ''){
          return;
      }
      $("tr.transactions").each(function(){
        var cartons       = parseFloat($(this).find("td.cartons").text())||0;
        var rate_carton   = parseFloat($(this).find("td.rate_carton").text())||0;
        var quantity      = parseFloat($(this).find("td.quantity").text())||0;
        var unitPrice     = parseFloat($(this).find("td.unitPrice").text())||0;
        var amnt          = (quantity*unitPrice)+(cartons*rate_carton);
        amountTotal      += amnt;
      });
      var new_discount = dominate_discount;
      $("tr.transactions").each(function(){
          var taxType       = ($(".taxType").is(":checked"))?"I":"E";
          var taxRate       = parseFloat($(this).find("td.taxRate").text())||0;
          var cartons       = parseFloat($(this).find("td.cartons").text())||0;
          var rate_carton   = parseFloat($(this).find("td.rate_carton").text())||0;
          var quantity      = parseFloat($(this).find("td.quantity").text())||0;
          var unitPrice     = parseFloat($(this).find("td.unitPrice").text())||0;
          var amount        = Math.round(((quantity*unitPrice)+(cartons*rate_carton))*100)/100;

          if(discount_type == 'R'){
              new_discount = (amount/amountTotal)*dominate_discount;
          }
          new_discount = Math.round(new_discount*100)/100;
          $(this).find("td.discount").text(new_discount);

          var discountPerCentage = 0;
          if($("input.individual_discount").val()=='Y'){
              if(discount_type == 'R'){
                  discountPerCentage = new_discount;
              }else if(discount_type == 'P'){
                  new_discount = Math.round(new_discount*100)/100;
                  discountPerCentage = amount*(new_discount/100);
                  discountPerCentage = Math.round(discountPerCentage*100)/100;
              }
              discountPerCentage   = Math.round(discountPerCentage*100)/100;
              $(this).find("td.discount").attr('data-amount',discountPerCentage);
              amount -= discountPerCentage;
              amount = Math.round(amount*100)/100;
          }

          var taxAmount = 0;
          if(taxRate > 0){
              if(taxType == 'I'){
                  taxAmount = amount*(taxRate/100);
              }else if(taxType == 'E'){
                  taxAmount = amount*(taxRate/100);
              }
          }
          if(taxType == 'I'){
              amount -= taxAmount;
          }else{
              amount += taxAmount;
          }
          $(this).find("td.totalAmount").text(amount);
      });
      $(this).calculateColumnTotals();
  });

  $("div.itemSelector button").focus();
  $(window).keyup(function(e){
      if(e.keyCode == 113){
          window.location.href = 'dist-sale-details.php';
      }
  });
  $(window).keydown(function(e){
    //   console.log(e.keyCode);
      if(e.altKey == true&&e.keyCode == 73){
          e.preventDefault();
          $("#form").click();
          $("div.itemSelector button").click();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 65){
          e.preventDefault();
          $("#form").click();
          $("div.supplierSelector button").click();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 66){
          e.preventDefault();
          $("#form").click();
          $("input.barcode_input").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 67){
          e.preventDefault();
          $("#form").click();
          $("input.supplier_name").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 77){
          e.preventDefault();
          $("#form").click();
          $("input.customer_mobile").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 68){
          e.preventDefault();
          $("#form").click();
          $("input.whole_discount").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 79){
          e.preventDefault();
          $("#form").click();
          $("input.inv_charges").focus();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 78){
          e.preventDefault();
          $("#form").click();
          print_method = 'N';
          saveSale();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 83){
          e.preventDefault();
          $("#form").click();
          print_method = 'N';
          saveSale();
          return false;
      }
      if(e.altKey == true&&e.keyCode == 80){
          e.preventDefault();
          $("#form").click();
          print_method = 'Y';
          if($("input[name=radiog_dark]:checked").val() == 'C'){
              redirect_method = 'N';
          }
          saveSale();
          return false;
      }
      if(e.keyCode == 112){
          e.preventDefault();
          $("#form").click();
          print_method    = 'N';
          redirect_method = 'N';
          saveSale();
          return false;
      }
  });
<?php
  if(isset($_GET['print'])){
      $inv_idx = isset($_GET['method'])?(int)($_GET['method']):$inventory['ID'];
?>
  var id  = <?php echo $inv_idx; ?> ;
  var win = window.open("<?php echo $invoiceFile; ?>?id="+id,"_blank");
  if(win){
      win.focus();
  }
<?php
  }
?>
});
<?php
if(isset($_GET['saved'])){
?>
displayMessage('Bill Saved Successfully!');
<?php
}
?>
<?php
if(isset($_GET['updated'])){
?>
displayMessage('Bill Updated Successfully!');
<?php
}
?>

</script>
