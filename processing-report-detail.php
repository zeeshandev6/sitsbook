<?php
    ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/accounts.php');
    include ('common/classes/customers.php');
    include ('common/classes/suppliers.php');
    include ('common/classes/processing_report.php');
    include ('common/classes/processing_report_details.php');

    //Permission
    if(!in_array('processing-report-form',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource-sp/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource-sp/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objChartOfAccounts         = new ChartOfAccounts();
    $objCustomer                = new Customers();
    $objSupplier                = new suppliers();
    $objUserAccount             = new UserAccounts();
    $objProcessingReport        = new ProcessingReport();
    $objProcessingReportDetails = new ProcessingReportDetails();

    if(isset($_POST['RowId'])){
        $objProcessingReportDetails->delete($_POST['RowId']);
        exit();
    }
    $suppliers_list     = $objSupplier->getList();

    $pr_id = 0;
    if(isset($_POST['id'])){
        $pr_id = mysql_real_escape_string($_POST['id']);

        $objProcessingReport->user_id     = $_SESSION['classuseid'];
        $objProcessingReport->lot_no      = mysql_real_escape_string($_POST['lot_no']);
        $objProcessingReport->report_date = date("Y-m-d",strtotime($_POST['report_date']));
        $objProcessingReport->supplier_id = mysql_real_escape_string($_POST['supplier_id']);
        $objProcessingReport->comments    = mysql_real_escape_string($_POST['comments']);

        if($pr_id == 0){
          $objProcessingReport->save();
          $pr_id = mysql_insert_id();
          $action  = 'saved';
        }else{
          $objProcessingReport->update($pr_id);
          $action  = 'updated';
        }
        if($pr_id){
            $json_data   = $_POST['json_data'];
            $json_data   = json_decode($json_data,true);
            foreach ($json_data as $key => $value){
                $row_id                                 = $value['row_id'];
                $objProcessingReportDetails->pr_id      = $pr_id;

                $objProcessingReportDetails->quality    = $value['quality'];
                $objProcessingReportDetails->issue      = $value['issue'];
                $objProcessingReportDetails->fresh      = $value['fresh'];
                $objProcessingReportDetails->sample     = $value['sample'];
                $objProcessingReportDetails->b_grade    = $value['b_grade'];
                $objProcessingReportDetails->c_p        = $value['c_p'];
                $objProcessingReportDetails->received   = $value['received'];
                $objProcessingReportDetails->difference = $value['difference'];

                if($row_id == 0){
                    $objProcessingReportDetails->save();
                    $is   = 'saved';
                }else{
                    $objProcessingReportDetails->update($row_id);
                    $is   = 'updated';
                }
            }

        }
        if(isset($action)){
            header('location:processing-report-detail.php?id='.$pr_id."&action=".$action);
        }
        exit();
    }

    if(isset($_GET['id'])){
        $pr_id                  = mysql_real_escape_string($_GET['id']);
        $processingreport       = $objProcessingReport->getRecordDetailsById($pr_id);
        $processingreportdetail = $objProcessingReportDetails->getListByPrId($pr_id);
    }else{
        $processingreport = NULL;
    }

?>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style media="screen">
          table tr.record_row td,tfoot td{
            font-size: 12px !important;
            padding: 5px !important;
          }
        </style>

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/processing.report.config.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('input[name=report_date]').datepicker({
                    dateFormat:'dd-mm-yy',
                    showAnim: 'show',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '2000:+10'
                });
                $("select").selectpicker();
                $("select[name='contract']").change(function(){
                    $("input[name=supplier_id]").val($("select[name='contract'] option:selected").attr('data-supplier'));
                });
                $(".received,.difference").prop('readonly',true);
                $("input[name=supplier_id]").val($("select[name='contract'] option:selected").attr('data-supplier'));
                $("#pro_table").change(function(){
                    calculate_percentage();
                });
                $("#pro_table").change();
                $("input[readonly]").attr("tabindex","-1");
            });
            var calculate_percentage = function(){
                var issue      = 0;
                var fresh      = 0;
                var sample     = 0;
                var b_grade    = 0;
                var c_p        = 0;
                var received   = 0;
                var difference = 0;
                $(".record_row").each(function(){
                    issue      += parseFloat($(this).find("td.issue").text())||0;
                    fresh      += parseFloat($(this).find("td.fresh").text())||0;
                    sample     += parseFloat($(this).find("td.sample").text())||0;
                    b_grade    += parseFloat($(this).find("td.b_grade").text())||0;
                    c_p        += parseFloat($(this).find("td.c_p").text())||0;
                    received   += parseFloat($(this).find("td.received").text())||0;
                    difference += parseFloat($(this).find("td.difference").text())||0;
                });

                $("td#grand_issue").text(issue);
                $("td#grand_fresh").text(fresh);
                $("td#grand_sample").text(sample);
                $("td#grand_b_grade").text(b_grade);
                $("td#grand_c_p").text(c_p);
                $("td#grand_received").text(received);
                $("td#grand_difference").text(difference);

                var perc_fresh_sample = (parseFloat(((fresh+sample)/issue)*100)||0).toFixed(2);
                var perc_b_grade = (parseFloat((b_grade/issue)*100)||0).toFixed(2);
                var perc_c_p = (parseFloat((c_p/issue)*100)||0).toFixed(2);
                var perc_received = (parseFloat((received/issue)*100)||0).toFixed(2);
                var perc_difference = (parseFloat((difference/issue)*100)||0).toFixed(2);

                $("td#sign_fresh_sample").text(perc_fresh_sample);
                $("td#sign_b_grade").text(perc_b_grade);
                $("td#sign_c_p").text(perc_c_p);
                $("td#sign_received").text(perc_received);
                $("td#sign_difference").text(perc_difference);
            }
        </script>
        <style>
            .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
                width: 258px;
            }
            table td,table input{
                text-align: center;
            }
            table input[name=des]{
                text-align: center;
            }
            table td#des{
                text-align: left;
            }
        </style>

    </head>
    <body>
        <div id="sidebar"><?php include("common/left_menu.php") ?></div>
        <div id="bodyWrapper">
        	<div class="content-box-top">
            	<div class="summery_body">
               		<div class = "content-box-header">
						          <p style="font-size:15px;"><b>Processing Report Details</b></p>
                  		<span id="tabPanel">
                            <div class="tabPanel">
                                <a href="processing-report-list.php"><div class="tab">List</div></a>
                                <a href="processing-report-list.php"><div class="tab">Search</div></a>
                                <div class="tabSelected">Detail</div>
                            </div>
                        </span>
                        <div style = "clear:both;"></div>
               		</div><!-- End .content-box-header -->
                  <div style = "clear:both; height:20px"></div>
              		<div id = "bodyTab1">
                        <?php
                        //msgs
                        if(isset($_GET['action'])){
                            if($_GET['action']=='updated')  { successMessage("Processing Report Updated.");}
                            elseif($_GET['action']=='added'){ successMessage("New Processing Report Added.");}
                            elseif($_GET['action']=='error'){ errorMessage("Something went wrong.");}
                        }
                        ?>
                    	<form method = "post" action = "" name="add_folding_reports">
                        	<div id="form">
                                <div class="caption">Lot # :</div>
                                <div class="form-group field" style="margin-bottom:0px;width:350px;">
                                    <input type="text" class="form-control" name="lot_no" id="pon" value="<?php echo $processingreport['LOT_NO']; ?>" style="width: 350px;" >
                                    <input type="hidden" name="id"       value="<?php echo $pr_id; ?>" />
                                    <input type="hidden" name="json_data"       value="" />
                                    <input type="hidden" name="report"          value="processingreport" id="report"/>
                                </div>
                                <div class="clear"></div>

                                <div class="caption">Processing Unit :</div>
                                <div class="field" style="width:350px;">
                                    <select class="selectpicker show-tick form-control" name="supplier_id" data-live-search="true" >
                                        <option value=""></option>
                                        <?php
                            						if(mysql_num_rows($suppliers_list)){
                            							while($account = mysql_fetch_array($suppliers_list)){
                            								$selected = ($processingreport['SUPPLIER_ID']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
                            								?>
                            								<option <?php echo $selected; ?> data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
                            								<?php
                            							}
                            						}
                            						?>
                                    </select>
                                </div>

                                <div class="caption">Report Date :</div>
                                <div class="field" style=" width:145px">
                                    <input class="form-control datepicker" name="report_date" value="<?php echo ($processingreport == NULL)?date('d-m-Y'):$processingreport['REPORT_DATE']; ?>" />
                                </div>
                                <div class="clear"></div>

                                <div class="caption"> Comments :</div>
                                <div class="field" style="width:700px">
                                	<textarea name="comments"  style="height:100px;" class="form-control"><?php echo $processingreport['COMMENTS']; ?></textarea>
                                </div>
                                <div class="clear" style="height:20px"></div>

                                <div class="caption"></div>
                                <div class="field" style="width:900px;">
                                	<table width="100%" name="pro_table" id="pro_table">
                                        <thead>
                                            <tr>
                                               <th width="20%"  style="text-align:center">Quality</th>
                                               <th width="10%"  style="text-align:center">Issue</th>
                                               <th width="10%"  style="text-align:center">Fresh</th>
                                               <th width="10%"  style="text-align:center">Sample</th>
                                               <th width="10%"  style="text-align:center">B Grade</th>
                                               <th width="10%"  style="text-align:center">C.P</th>
                                               <th width="10%"  style="text-align:center">Received</th>
                                               <th width="10%"  style="text-align:center">Difference</th>
                                               <th width="10%" style="text-align:center">Action</th>
                                            </tr>
                                            <tr class="design">
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="quality form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="issue form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="fresh addup form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="sample addup form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="b_grade addup form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="c_p addup form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="received form-control" />
                                                </td>
                                                <td style="text-align:center; height:20px;">
                                                    <input type="text" class="difference form-control" />
                                                </td>
                                                <td><button type="button" class="button btn-block enter_new_row">Enter</button></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($processingreportdetail)){
                                                    if(mysql_num_rows($processingreportdetail)){
                                                        while($detailListArr = mysql_fetch_assoc($processingreportdetail)){
                                                            ?>
                                                            <tr class="record_row" data-id="<?php echo $detailListArr['ID']; ?>">
                                                                <td class="quality"><?php echo $detailListArr['QUALITY']; ?></td>
                                                                <td class="issue"><?php echo number_format($detailListArr['ISSUE'],0,'.',''); ?></td>
                                                                <td class="fresh"><?php echo number_format($detailListArr['FRESH'],0,'.',''); ?></td>
                                                                <td class="sample"><?php echo number_format($detailListArr['SAMPLE'],0,'.','');  ?></td>
                                                                <td class="b_grade"><?php echo number_format($detailListArr['B_GRADE'],0,'.','');  ?></td>
                                                                <td class="c_p"><?php echo number_format($detailListArr['C_P'],0,'.','');  ?></td>
                                                                <td class="received"><?php echo number_format($detailListArr['RECEIVED'],0,'.','');  ?></td>
                                                                <td class="difference"><?php echo number_format($detailListArr['DIFERENCE'],0,'.',''); ?></td>
                                                                <td>
                                                                    <a id="view_button" onclick="editMe(this);"><i class="fa fa-pencil"></i></a>
                                                                    <a class="pointer" onclick="deleteMe(this);"><i class="fa fa-times"></i></a>
                                                                </td>
                                                                <input type='hidden' id='products_val' value=''>
                                                                <input type='hidden' id='RecordId' value="" >
                                                            </tr>
                                                        <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td class="text-center"  >GRAND TOTAL</td>
                                                <td class="text-center" id="grand_issue" >0</td>
                                                <td class="text-center" id="grand_fresh" >0</td>
                                                <td class="text-center" id="grand_sample" >0</td>
                                                <td class="text-center" id="grand_b_grade" >0</td>
                                                <td class="text-center" id="grand_c_p" >0</td>
                                                <td class="text-center" id="grand_received" >0</td>
                                                <td class="text-center" id="grand_difference" >0</td>
                                                <td class="text-center"> - - - </td>
                                            </tr>
                                            <tr class="has_percents">
                                                <td class="text-center not-percent"  >PERCENTAGE</td>
                                                <td class="text-center mr-5"> 100</td>
                                                <td class="text-center" id="sign_fresh_sample" colspan="2" >0</td>
                                                <td class="text-center" id="sign_b_grade" >0</td>
                                                <td class="text-center" id="sign_c_p" >0</td>
                                                <td class="text-center" id="sign_received" >0</td>
                                                <td class="text-center" id="sign_difference" >0</td>
                                                <td class="text-center not-percent"> - - - </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"></div>
                                <div class="field" style="width:350px">
                                  <input type="button" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="button" />
                                  <?php if(isset($_GET['id'])){ ?>
                                  <a class="button" target="_blank" href="print-processing-report.php?id=<?php echo $_GET['id']; ?>">Print</a>
                                  <?php } ?>
                                  <input type="button" value="New Form" class="button" onclick="window.location.href='processing-report-detail.php'";  />
                                </div>
                                <div class="clear"></div>
                            </div><!--form-->
						</form>
                    </div><!--bodyTab1-->
				</div><!--summery_body-->
			</div><!--content-box-top-->



            <!-- Delete confirmation popup -->
            <div id="myConfirm" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- PLease fill product table information popup -->
            <div id="myFillAll" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please provide all details, in detail table.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <!-- PLease fill product table information popup -->
            <div id="selectcontract" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please Select Contract No. first.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Loading bar confirmation popup -->
            <div id="myLoading" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Processing...</h4>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span class="sr-only">100% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- PLease fill product table information popup -->
            <div id="enterPON" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please enter PON first.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- jquery post error of product confirmation popup -->
            <div id="myError" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #e4b9c0; color: white;">
                            <h4 class="modal-title">Alert!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-warning" style="font-weight: bold;">Your Product Detail in not submitted.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <!-- Edit popup -->
            <div id="myedit" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Edit Record</h4>
                        </div>
                        <div id="form" class="modal-body">
                            <form class="editmyform" id="pro_table">

                                <input type="hidden" class="form-control" name="rowid" value="" />

                                <div style="width:450px; margin: auto;">
                                    <div class="caption"> Quality :</div>
                                    <div class="field">
                                        <input class="form-control quality"  value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption"> Issue:</div>
                                    <div class="field">
                                        <input class="form-control issue" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Fresh :</div>
                                    <div class="field">
                                        <input class="form-control addup fresh" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption"> Sample :</div>
                                    <div class="field">
                                        <input class="form-control addup sample" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">B Grade :</div>
                                    <div class="field">
                                        <input class="form-control addup b_grade" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">C.P :</div>
                                    <div class="field">
                                        <input class="form-control addup c_p" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Received :</div>
                                    <div class="field">
                                        <input class="form-control received" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Difference :</div>
                                    <div class="field">
                                        <input class="form-control difference" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="clear" style="height:30px"></div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary btn-sm" id="addrow" data-dismiss="modal">Update</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div><!--bodyWrapper-->
    </body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
