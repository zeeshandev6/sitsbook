<?php
    

    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/messages.php');
    include ('common/classes/message-docs.php');
    include ('common/classes/customers.php');
    include('common/classes/company_details.php');

    $objMessages        = new Messages();
    $objCustomer        = new Customers();
    $objCompanyDetails  = new CompanyDetails();
    $objMessageDocs     = new MessageDocs();

    $mode = "";
	$loadPage = true;

	if(isset($_GET['id'])){
        $id = $_GET['id'];
        if($id!=''){
            $array = $objMessages->getListById($id);
            $date       = date('d-m-Y', strtotime($array['MSG_DATE']));
            $subject    = $array['SUBJECT'];
            $about      = $array['DESCRIPTIONS'];
            $from_user  = $array['FROM_USER_ID'];
			$to_user 	= $array['TO_USER_ID'];
			$sender_details   = $objAccounts->getDetails($from_user);
			$receiver_details = $objAccounts->getDetails($to_user);
            $session_id = $_SESSION['classuseid'];
			if($to_user == $session_id){
				$objMessages->updateMessageStatus($id, 'Y');
			}
            $documents = $objMessageDocs->getList($id);
        }
    }

    $companyLogo   = $objCompanyDetails->getLogo($session_branch_id);
    $company  = $objCompanyDetails->getRecordDetails($session_branch_id);
?>
    <!DOCTYPE html 
        >
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Message Details view/print</title>
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/hitex-print.css" type="text/css" media="screen" />
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
        		$(".launch").click(function(){
        			$(".print_div").printThis({
						  debug: false,
						  importCSS: false,
						  printContainer: true,
						  loadCSS: 'resource/css/hitex-print.css',
						  pageTitle: "",
						  removeInline: false,
						  printDelay: 500,
						  header: null
					});
        		});
                $("img[data-toggle=tooltip]").tooltip();
        	});
        </script>
    </head>

    <body>
        <div class="containter">
            <div class="col-md-12 col-xs-12">
            	<div class="panel panel-default" style="width: 800px;float: none;margin: 20px auto;">
            		<div class="panel-heading">
            			<span>Message Details</span>
            			<button class="button pull-right launch"><i class="fa fa-print"></i> Print </button>
            			<div class="clearfix"></div>
            		</div>
            		<div class="print_div">
                            <div class="hitex-header">
                                <img class="hitext-logo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
                                <h3 ><?php echo $company['NAME']; ?></h3>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
	            		<div class="panel-body">
	            			<div class="panel panel-default" style="padding: 1em;margin-bottom:5px;">
		            			<div class="row">
		            				<div class="col-xs-12"> <span class="capshan"> From :</span> <?php echo $sender_details['FIRST_NAME']." ".$sender_details['LAST_NAME']; ?></div>
		            				<div class="col-xs-12"> <span class="capshan"> To :</span> <?php echo $receiver_details['FIRST_NAME']." ".$receiver_details['LAST_NAME']; ?></div>
		            				<div class="col-xs-12"> <span class="capshan"> Dated :</span> <?php echo $date; ?></div>
		            				<div class="clearfix"></div>
		            				<div class="col-xs-12"> <span class="capshan">Subject :</span> <?php echo $subject; ?> </div>
		            			</div>
		            		</div>
	        				<div class="row"  style="padding: 1em;">
	            				<div class="clearfix"></div>
	            				<div class="panel panel-default" style="padding: 1em;text-align: justify;">
	            					<div class="col-xs-12"><?php echo $about; ?></div>
	            					<div class="clearfix"></div>
	            				</div>
	            				<div class="clearfix"></div>
	            			</div>
<?php
                                if(isset($documents) && mysql_num_rows($documents)){
?>
                            <div class="row noprint"  style="padding: 1em;">
                                <div class="clearfix"></div>
                                <div class="panel panel-default" style="padding: 1em;text-align: justify;">
<?php
                                    while($document = mysql_fetch_assoc($documents)){
                                        $icondir  = "resource/images/mime-types/";
                                        $iconfile = $icondir.strtolower(pathinfo($document['FILE_NAME'],PATHINFO_EXTENSION)).".png";
?>
                                    
                                    <div class="hover-stripes">
                                        <div class="col-xs-1"> <img src="<?php echo $iconfile; ?>" style="height:25px;" /> </div> <div class="col-xs-10"><?php echo $document['TITLE']; ?></div> <div class="col-xs-1"> <a href="<?php echo "uploads/docs/$date/".$document['FILE_NAME']; ?>" download="<?php echo $document['TITLE']; ?>" > <img src="resource/images/download-icon.png"  style="height:25px;" /> </a> </div> 
                                        <div class="clear"></div>
                                    </div>
<?php
                                    }
?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
<?php
                                }
?>
	            		</div>
	            		<div class="panel-footer">
	            			<?php echo $company['NAME']; ?>
	            		</div>
	            	</div>
            	</div>
            </div>
        </div>     <!-- End summer -->
    </body>
    </html>
<?php include('conn.close.php'); ?>
