<?php
	include 'common/connection.php';
	include 'common/config.php';
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');

	$objItems = new Items;
	$objItemsCategory = new itemCategory;
	$objDepartments   = new Departments;

	$today = date('d-m-Y');

	if(isset($_POST['search'])){
		$department_id = $_POST['department'];
		$category_id   = $_POST['item_cate'];
		if($department_id != 0){
			$searchData       = $objItems->searchParent($department_id,$category_id);
		}
	}
	$categoryList = $objItemsCategory->getList();

?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Inventory Stock</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		#recordPanel td{
			font-size: 16px !important;
		}
		.trHasHead th{
			font-size: 16px !important;
		}
	</style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Inventory Stock</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1" style="display:block;">
                	<div class="printPage">
                        <div class="heading" style="background-color:rgba(90,90,90,1.0);">Posted Entries Dated :  <?php echo date('d-m-Y'); ?></div>
                        <div style="height: 20px;"></div>
                        <table cellspacing="0" style="margin: 0 auto;width:95%;" >
                        <thead>
                            <tr class="trHasHead">
                               <th width="5%" style="text-align:center">Sr#</th>
                               <th width="15%" style="text-align:center">Item</th>
                               <th width="15%" style="text-align:center">Purchase Price</th>
                               <th width="15%" style="text-align:center">Sale Price</th>
                               <th width="10%" style="text-align:center">Stock In Hand</th>
                            </tr>
                        </thead>

                        <tbody>
<?php
							$categoryList = $objItemsCategory->getList();

							if(mysql_num_rows($categoryList)){
								while($category = mysql_fetch_array($categoryList)){
									$itemsList = $objItems->getListByCategory($category['ITEM_CATG_ID']);
									if(mysql_num_rows($itemsList)){
										$categoryName = $objItemsCategory->getDetails($category['ITEM_CATG_ID']);
?>
										<tr class="itemCategory" data-type-id="<?php echo $category['ITEM_CATG_ID']; ?>">
             				 	<th colspan="5" class="pl-10"><?php echo $categoryName['NAME']; ?></th>
	                	</tr>
<?php

?>
<?php
										$counter = 1;
										while($row = mysql_fetch_array($itemsList)){
										$active = ($row['ACTIVE']=='Y')?"checked=\"checked\"":"";
?>
                                            <tr id="recordPanel">
                                                <td style="text-align:center"><?php echo $counter; ?></td>
                                                <td style="text-align:left;padding-left: 5px;"><?php echo $row['NAME']; ?></td>
                                                <td style="text-align:center"><?php echo $row['PURCHASE_PRICE']; ?></td>
                                                <td style="text-align:center"><?php echo $row['SALE_PRICE']; ?></td>
                                                <td style="text-align:center"><?php echo $row['STOCK_QTY']; ?></td>
                                            </tr>
<?php
										$counter++;
										}
									}
								}
							}
?>
                        </tbody>
                    </table>
                	</div><!--class="printPage"-->
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>
                <div id="bodyTab2" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"block":"none"; ?>;" >
                    <div id="form">
                    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="title" style="font-size:20px; margin-bottom:20px">Search receipt inventory</div>

                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

                        <div class="caption">Supplier</div>
                        <div class="field" style="width:305px;">
                        	<select class="selectpicker form-control"
                            		name='supplierAccCode'
                                    data-style="btn-default"
                                    data-live-search="true" style="border:none">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                                $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Bill No</div>
                        <div class="field">
                        	<input type="text" value="" name="billNum" class="input_size" />
                        </div>
                        <div class="clear"></div>
                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="searchInventory" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(function(){
		$(window).scroll(function(){
			$('#sidebar').height($(document).height());
			var topOffsetPos = $(window).scrollTop();
			$(".toggleSideBar").css({'top':5+topOffsetPos});
		});
		$.fn.sideBarFunction = function(){
			if($('#sidebar').css('left')=='0px'){
				$(".toggleSideBar").css({'opacity':'0.6'});
				$('#sidebar').animate({'left':'-230px'},200);
				$(".content-box-top").animate({'left':'10px'},200);
				$(".content-box-body").animate({'left':'10px'},200);
				$(".toggleSideBar").children("i.fa-caret-left").removeClass('fa-caret-left').addClass('fa-caret-right');
			}else{
				$('#sidebar').animate({'left':'0'},200);
				$(".toggleSideBar").css({'opacity':'1.0'});
				$(".toggleSideBar").children("i.fa-caret-right").removeClass('fa-caret-right').addClass('fa-caret-left');
				$(".content-box-top").animate({'left':'240px'},200);
				$(".content-box-body").animate({'left':'240px'},200);
			}
		};
		//Call
		$(".toggleSideBar,.toggleSideBar i").on('click',function(){
			$(this).sideBarFunction();
		});
	});
	$(window).load(function(){
		$('#sidebar').height($(document).height());
	});
</script>
