<?php
	include 'common/connection.php';
	include 'common/config.php';
	include('common/classes/emb_sale.php');
	include('common/classes/emb_sale_details.php');
	include 'common/classes/customers.php';
	include 'common/classes/machines.php';
	include('common/classes/services.php');
	include 'common/classes/j-voucher.php';
	include('common/classes/company_details.php');

	$objCompanyDetails 	   			= new CompanyDetails();
	$objEmbroiderySale         	= new EmbroiderySale();
	$objEmbroiderySaleDetails  	= new EmbroiderySaleDetails();
	$objCustomers          			= new Customers();
	$objMachines  	            = new Machines();
	$objServices           			= new Services();
	$objJournalVoucher     			= new JournalVoucher();
	$objConfigs   	       			= new Configs();

	//INVOICE_FORMAT Config Size - Header - Duplicate
	$invoice_format        = $objConfigs->get_config('INVOICE_FORMAT');
	$show_header 					 = $objConfigs->get_config('SHOW_INVOICE_HEADER');
	$invoice_format        = explode('_', $invoice_format);
	$invoice_num 		   		 = $invoice_format[1];
	$invoiceStyleCss       = 'resource/css/invoiceStyle.css';

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;

	$cutomer_balance_array 						= array();
	$cutomer_balance_array['BALANCE'] = 0;
	$cutomer_balance_array['TYPE']    = '';

	if(isset($_GET['id'])){
		$sale_id 		       = mysql_real_escape_string($_GET['id']);
		$saleDetails 	     = $objEmbroiderySale->getDetail($sale_id);
		if($saleDetails['COMPANY_ID']>0){
			$companyLogo   = $objCompanyDetails->getLogo($saleDetails['COMPANY_ID']);
			$company  		 = $objCompanyDetails->getRecordDetails($saleDetails['COMPANY_ID']);
		}else{
			$companyLogo   = $objCompanyDetails->getLogo();
			$company  		 = $objCompanyDetails->getActiveProfile();
			$objEmbroiderySale->insertCompanyId($sale_id,$company['ID']);
		}
		$saleDetailList        = $objEmbroiderySaleDetails->getList($sale_id);

		if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010101'){
			$customer_balance      = $objJournalVoucher->getOpeningBalanceOfVoucher($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID'],$saleDetails['SALE_DATE']);
			$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
		}
	}
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>

	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "<?php echo $invoiceStyleCss; ?>",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		//$(".printThis").click();
		$(".ledger_view").click(function(){
			var gl_date = $(this).attr('data-date');
			var gl_code = $(this).attr('data-code');
			if(gl_code != ''){
				$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
					if(data == ''){
						var win = window.open('invoice-ledger-print.php','_blank');
						if(win){
							win.focus();
						}else{
							displayMessage('Please allow popups for This Site');
						}
					}
				});
			}
		});
		//$(".printThis").click();
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $saleDetails['CUSTOMER_NAME'];
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
?>
		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
					<button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print View </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="clear"></div>
			<div class="printThisDiv" style="position: relative;height:9.7in;">
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint" style="height:10.6in !important;">
						<div class="invoiceHead" style="width: 100%;margin: 0px auto;">
							<div class="<?php echo $show_header=='N'?"opacity_zero":""; ?>">
								<?php
								if($companyLogo != ''){
									?>
									<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
									<?php
								}
								?>
								<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
									<div style="font-size:26px;line-height: 40px;"> <?php echo $company['NAME']; ?> </div>
									<?php echo $company['ADDRESS']; ?>
									<br />
									Contact : <?php echo $company['CONTACT']; ?>
									<br />
									Email : <?php echo $company['EMAIL']; ?>
									<br />
								</div>
							</div>
							<div class="clear"></div>
							<div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
								<?php echo (substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101')?"CASH":""; ?> SALES INVOICE
							</div>
							<div class="clear" style="height: 5px;"></div>

							<?php
							if($invoice_num == '1'){

								$customer['CUST_ACC_TITLE'] = explode(',', $customer['CUST_ACC_TITLE']);
								$customer['CUST_ACC_TITLE'] = implode('<br />', $customer['CUST_ACC_TITLE']);
								?>
								<div class="infoPanel pull-left" style="height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">To : <?php echo $customer['CUST_ACC_TITLE']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['ADDRESS']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $customer['CITY']; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="infoPanel pull-right" style="width:200px;height:auto !important;">
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Inv No. <?php echo $saleDetails['BILL_NO']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Date. <?php echo $sale_date; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Lot No. <?php echo $saleDetails['LOT_NO']; ?></span>
									<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;">Gate Pass. <?php echo $saleDetails['GP_NO']; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="clear"></div>
								<?php
							}

							if($invoice_num == '2'){
								?>
								<div class="invoPanel">
									<div class="invoPanel">
										<div class="clear"></div>
										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Name: </span>
											<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">Address: </span>
											<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
										</span>
										<div class="clear"></div>

										<span class="pull-left" style="font-size:14px;">
											<span class="caption">City: </span>
											<span class="text_line"><?php echo $customer['CITY']; ?></span>
										</span>
										<div class="clear"></div>
									</div>
								</div>
								<div class="clear" style="height:10px;"></div>
								<?php
							}
							?>

							<?php if(isset($saleDetails['SUBJECT']) && $saleDetails['SUBJECT'] != ''){ ?>
								<div class="infoPanelWide pull-left" style="min-height:auto !important;">
									<span class="pull-left">
										<span class="pull-left" style="padding:0px 0px !important;font-size:14px;">Subject : </span>
										<span class="pull-left" style="padding:0px 0px;font-size:14px;border-bottom:1px solid #000;"><?php echo $saleDetails['SUBJECT']; ?></span>
									</span>
									<div class="clear" style="height:10px;"></div>
								</div>
								<div class="clear" style="height:10px;"></div>
								<?php } ?>
								<div class="clear"></div>
							</div><!--invoiceHead-->
							<div class="clear"></div>
							<div class="invoiceBody" style="width: 100%;margin: 0px auto;">
								<table>
									<thead>
										<tr>
											<th width="5%">Sr#</th>
											<th width="30%">Machine</th>
												<th width="10%">Design #</th>
												<th width="10%">Stitch Rate</th>
												<th width="10%">Stitches</th>
												<th width="10%">Amount</th>
												<th width="10%">Qty/Length</th>
												<th width="10%">Total</th>
													</tr>
												</thead>
												<tbody>
<?php
													$stitches      = 0;
													$totalAmount   = 0;
													$counter       = 1;
													if(mysql_num_rows($saleDetailList)){
														while($row = mysql_fetch_array($saleDetailList)){
															$machine_name = $objMachines->getName($row['MACHINE_ID']);
?>
																		<tr>
																			<td><?php echo $counter; ?></td>
																			<td style="font-size:12px;"><span style="float:left;margin-left:5px;"><?php echo $machine_name; ?></span></td>
																			<td class="text-center"><?php echo $row['DESIGN_NO']; ?></td>
																			<td class="text-center"><?php echo number_format($row['UNIT_PRICE'],2); ?></td>
																			<td class="text-center"><?php echo number_format($row['STITCHES'],0); ?></td>
																			<td class="text-center"><?php echo number_format($row['TOTAL_AMOUNT'],2); ?></td>
																			<td class="text-center"><?php echo number_format($row['QTY_LENGTH'],2); ?></td>
																			<td class="text-center"><?php echo number_format($row['FINAL_AMOUNT'],2); ?></td>
																		</tr>
																		<?php
																		$stitches    += $row['STITCHES'];
																		$totalAmount += $row['FINAL_AMOUNT'];
																		$counter++;
																	}
																}
																?>
															</tbody>
															<?php
															$colum_skipper = 0;
															$columns       = 4;
															$columns += $colum_skipper;
															?>
															<tfoot>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="4"><span style="float:right;margin-right:15px;">Total </span></td>
																	<td class="text-center"><?php echo number_format($stitches,0); ?></td>
																	<td class="text-center" colspan="2"></td>
																	<td class="text-center"><?php echo number_format($totalAmount,2); ?></td>
																</tr>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="7"><span style="float:right;margin-right:15px;">Opening Balance : </span></td>
																	<td class="text-center"><?php echo number_format($cutomer_balance_array['BALANCE'],2)."  ".$cutomer_balance_array['TYPE']; ?></td>
																</tr>
																<?php
																	if($cutomer_balance_array['TYPE']=='DR'||$cutomer_balance_array['TYPE']==''){
																		$closing = $cutomer_balance_array['BALANCE']+$totalAmount;
																	}else{
																		$closing = $cutomer_balance_array['BALANCE']-$totalAmount;
																	}
																?>
																<tr>
																	<td style="text-align:right;border:none !important;" colspan="7"><span style="float:right;margin-right:15px;">Closing Balance : </span></td>
																	<td class="text-center"><?php echo number_format($closing,2)."  ".$cutomer_balance_array['TYPE']; ?></td>
																</tr>
															</tfoot>
														</table>
														<div class="clear"></div>

														<?php if($saleDetails['NOTES'] != ''){ ?>
															<div class="infoPanel pull-left" style="position:relative;top:0px;">
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;font-size:14px;border-bottom:1px solid #000;">Notes &amp; Enclosures</span>
																</span>
																<div class="clear" style="height:10px;"></div>
																<span class="pull-left" style="padding-left:0px !important;">
																	<span class="pull-left" style="padding:0px 0px !important;;font-size:14px;text-align:justify !important;"><?php echo $saleDetails['NOTES']; ?></span>
																</span>
																<div class="clear"></div>
															</div>
															<?php } ?>
															<div class="clear"></div>

															<div class="invoice_footer <?php echo $show_header=='N'?"opacity_zero":""; ?>" style="position:absolute !important;bottom:5px !important;">
																<div class="auths pull-left">
																	Prepared By
																	<span class="authorized"></span>
																</div><!--partyTitle-->
																<div class="auths pull-right">
																	Authorized By
																</div><!--partyTitle-->
																<div class="clear" style="height: 10px;"></div>
																<div class="invoice-developer-info text-right"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
															</div>
														</div><!--invoiceBody-->
													</div><!--invoiceLeftPrint-->
													<div class="clear"></div>
												</div><!--invoiceContainer-->
											</div><!--printThisDiv-->
										</div><!--invoiceBody-->
										<?php
									}
									?>
								</body>
								</html>
								<?php include('conn.close.php'); ?>
