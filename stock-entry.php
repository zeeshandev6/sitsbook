<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');

	$objItems  		  = new Items;
	$objItemCategory  = new itemCategory;

	$stockList = $objItems->getActiveList();
	if(isset($_POST['update'])){
		$itemCode = $_POST['stockAccCode'];
		$current_stock = $objItems->getStock($itemCode);
		$stox = $_POST['units'];
		$price  = $_POST['unit_price'];
		$total_cost = $price*$stox;
		if($current_stock == 0){
			$updated = $objItems->stock_entry($itemCode,$stox,$total_cost,$price);
		}else{
			$updated = false;
		}
		if($updated){
			$message = "Stock Updated Successfully!";
		}
	}
	if(isset($_GET['item'])){
		$item_id = mysql_real_escape_string($_GET['item']);
	}else{
		$item_id = 0;
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Item Stocks</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link type="text/css"  href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css"  href="resource/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css"  href="resource/css/bootstrap-select.css" rel="stylesheet">

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Stock Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                    	<a class="tab" href="items-management.php?tab=list">List</a>
                        <div class="tabSelected" id="tab1">Stock </div>
                        <a class="tab" href="item-details.php">Add New</a>
                    </div><!--tabPanel-->
                </span><!--tabPanel-->
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">

                            <div class="caption">Item : </div>
                            <div class="field" style="width:302px;">
                                <select class="itemCode show-tick form-control" data-style="btn-default" data-live-search="true" name="stockAccCode"  style="border:none">
                                       <option selected value="0"></option>
<?php
                                if(mysql_num_rows($stockList)){
                                    while($account = mysql_fetch_array($stockList)){
										$cat = $objItemCategory->getTitle($account['ITEM_CATG_ID']);
										$item_selected = $item_id == $account['ID']?"selected='selected'":"";
										if($account['INV_TYPE'] == 'B'){
											continue;
										}
?>
                                       <option value="<?php echo $account['ID']; ?>" <?php echo $item_selected; ?> data-subtext="<?php echo $cat; ?>" ><?php echo $account['NAME']; ?></option>
<?php
                                    }
                                }
?>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Quantity in Hand:</div>
                            <div class="field" >
                                <input type="text" name="units" value="" class="input_size units" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Unit Price:</div>
                            <div class="field" >
                                <input type="text" name="unit_price" value="" class="input_size unit_price" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Total Cost:</div>
                            <div class="field" >
                                <input type="text" name="total_cost" value="" class="input_size total_cost" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
								<input type="submit" name="update" value="Update" class="button" />
                            </div>
                            <div class="clear"></div>
                        </form>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="popUpForm"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$.fn.multiplication = function($elm1,$elm2,$product){
			$($product).val($($elm1).val()*$($elm2).val());
		}
		$(".units").keyup(function(){
			$(this).multiplication(".units",".unit_price",".total_cost");
		});
		$(".unit_price").keyup(function(){
			$(this).multiplication(".units",".unit_price",".total_cost");
		});
		$(".itemCode").selectpicker();
		$(".itemCode").change(function(){
			var itemCode = $(this).find("option:selected").val();
			$.post('db/get-item-stock.php',{item_id:itemCode},function(data){
				data = $.parseJSON(data);
				$("input[name='units']").animate({'background-color':'rgba(0,255,0,0.5)'},200,function(){
					$("input[name='units']").animate({'background-color':'rgba(255,255,255,1.0)'},200).val(data['QTY'])
				});
				$("input[name=unit_price]").val(data['PRICE']);
				$("input[name=total_cost]").val(data['PRICE']*data['QTY']);
				if(data['QTY'] > 0){
					$("input:submit").prop('disabled',true);
				}else{
					$("input:submit").prop('disabled',false);
				}
			});
		});
    });
</script>
<script>
<?php
	if(isset($message)){
?>
		displayMessage('<?php echo $message; ?>');
<?php
	}
?>
</script>
