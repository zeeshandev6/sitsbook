<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');
	include('common/settings/captions.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes      = new ChartOfAccounts();
	$objSales 			  = new Sale();
	$objSaleDetails       = new SaleDetails();
	$objSaleReturnDetails = new SaleReturnDetails();
	$objItems             = new Items();
	$objServices          = new Services();
	$objItemCategory      = new itemCategory();
	$objTaxRates          = new TaxRates();
	$objDepartments       = new Departments();
	$objConfigs 		 		  = new Configs();

	$suppliersList   			= $objAccountCodes->getAccountByCatAccCode('010104');
	$cashAccounts  	 			= $objAccountCodes->getAccountByCatAccCode('010101');
	$taxRateList     			= $objTaxRates->getList();

	$brands_list     			= $objItems->getBrandNameList();


	$currency_type 			 	= $objConfigs->get_config('CURRENCY_TYPE');
	$use_cartons   			 	= $objConfigs->get_config('USE_CARTONS');

	if(isset($_GET['search'])){

		$objSales->fromDate 		= ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objSales->toDate   		= ($_GET['toDate']=='')?"":date('Y-m-d',strtotime($_GET['toDate']));
		if(isset($_GET['user_id'])){
			$objSales->user_id 				= ($_SESSION['classuseid'] == 1)?$_GET['user_id']:$_GET['user_id'];
		}else{
			$objSales->user_id 				= $_SESSION['classuseid'];
		}
		$objSales->item     		= mysql_real_escape_string($_GET['item']);
		$objSales->supplierAccCode  = mysql_real_escape_string($_GET['supplier']);
		$objSales->account_type     = mysql_real_escape_string($_GET['account_type']);
		$objSales->with_tax  		= isset($_GET['with_tax'])?mysql_real_escape_string($_GET['with_tax']):"";
		$objSales->cat              = (isset($_GET['item_category']))?mysql_real_escape_string($_GET['item_category']):"";
		$objSales->brand_name       = (isset($_GET['brand_name']))?mysql_real_escape_string($_GET['brand_name']):"";
		$objSales->billNum			= '';

		$supplierTypeReport   =  ($objSales->supplierAccCode == '')?false:true;
		$itemTypeReport       =  ($objSales->item == '')?false:true;

		//both selected
		if(($objSales->supplierAccCode != '') && ($objSales->item != '')){
			$supplierTypeReport = true;
		}

		$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objSales->supplierAccCode);
		$theItemTitle      = $objItems->getItemTitle($objSales->item);

		if($supplierTypeReport){
			$titleRepo = $supplierHeadTitle;
		}elseif($itemTypeReport){
			$titleRepo = $theItemTitle;
		}else{
			$titleRepo = '';
		}
		if($objSales->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objSales->fromDate = $startThisMonth;
		}
		$purchaseReport = $objSales->fabricSaleReport();
	}
?>
<!DOCTYPE html>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$(".printThis").click(function(){
			var MaxHeight = 450;
			var RunningHeight = 0;
			var PageNo = 1;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if (RunningHeight + $(this).height() > MaxHeight){
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate = $(".repoDate").text();
			//remove previous thead/tfoot
			$(".tHeader").remove();
			$(".tableFooter").remove();
			$(".repoDate").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();

			$("div.tablePage").each(function(i,e){
				$(this).prepend($(".pageHeader").first().clone());
			});
			$(".pageHeader").first().remove();
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports-horizontal.css',
				pageTitle: "Sit Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		var $field = $("td.tax_amnt_tf");
		var $fieldBeacon = $("th.tax_amnt_th");
		var tax_amnt_total = parseFloat($field.text())||0;
		if(tax_amnt_total == 0){
			var $fieldRow = $fieldBeacon.parent();
			var tax_amnt_col_pos = $fieldBeacon.index();
			var tax_amnt_col_pos2 = tax_amnt_col_pos - 1;
			var sub_amnt_col_pos = tax_amnt_col_pos2 - 1;

			$("thead tr").each(function(i,v){
				$(this).find('th').eq(tax_amnt_col_pos).remove();
				$(this).find('th').eq(tax_amnt_col_pos2).remove();
				$(this).find('th').eq(sub_amnt_col_pos).remove();
			});
			$("tbody tr").each(function(i,v){
				$(this).find('td').eq(tax_amnt_col_pos).remove();
				$(this).find('td').eq(tax_amnt_col_pos2).remove();
				$(this).find('td').eq(sub_amnt_col_pos).remove();
			});
			$field.prev().prev().remove();
			$field.prev().remove();
			$field.remove();
		}
		$('select').selectpicker();
		if($(".carton_th").length){
			var colspan = $(".carton_th").prevAll().length;
		}else{
			var colspan = $(".tHeader th").eq(4).prevAll().length;
		}
		$(".total_tf").attr('colspan',colspan);
	});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sales Reports</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="fabric-sale-report.php">
							<div class="caption">From Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>

							<div class="caption">To Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>
							<?php
							if($_SESSION['classuseid'] == 1){
								?>
								<div class="caption">Salesman</div>
								<div class="field" style="width:300px;position:relative;">
									<select name="user_id" class="form-control" data-style="btn btn-default">
										<option></option>
										<?php
										$userList = $objAccounts->getActiveList();
										if(mysql_num_rows($userList)){
											while($the_user = mysql_fetch_array($userList)){
												?>
												<option value="<?php echo $the_user['ID']; ?>"><?php echo $the_user['FIRST_NAME']." ".$the_user['LAST_NAME']; ?></option>
												<?php
											}
										}
										?>
									</select>
								</div>
								<div class="clear"></div>
								<?php
							}
							?>
							<div class="caption">Customer</div>
							<div class="field" style="width:300px;position:relative;">
								<select class="supplierSelector form-control "
								name="supplier"
								data-style="btn btn-default"
								data-live-search="true" style="border:none" >
								<option selected value=""></option>
								<?php
								if(mysql_num_rows($cashAccounts)){
									while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
										?>
										<option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
										<?php
									}
								}
								?>
								<?php
								if(mysql_num_rows($suppliersList)){
									while($account = mysql_fetch_array($suppliersList)){
										$selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
										?>
										<option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="clear"></div>

						<div class="caption">Category </div>
						<div class="field" style="width:300px;">
							<select class="itemSelector show-tick form-control"
							data-style="btn btn-default"
							data-live-search="true" style="border:none" name="item_category">
							<option selected value=""></option>
							<?php
							$itemsCategoryList  = $objItemCategory->getList();
							if(mysql_num_rows($itemsCategoryList)){
								while($ItemCat = mysql_fetch_array($itemsCategoryList)){
									?>
									<option value="<?php echo $ItemCat['ITEM_CATG_ID']; ?>" ><?php echo $ItemCat['NAME']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="clear"></div>

					<div class="caption">Brands</div>
					<div class="field" style="width:300px;">
						<select class="form-control" name="brand_name">
							<option value=""></option>
							<?php if(mysql_num_rows($brands_list)){ ?>
								<?php while($brand = mysql_fetch_assoc($brands_list)){ ?>
							<option value="<?php echo $brand['COMPANY']; ?>"><?php echo $brand['COMPANY']; ?></option>
							<?php 	} ?>
							<?php } ?>
						</select>
					</div>
					<div class="clear"></div>

					<div class="caption">Item </div>
					<div class="field" style="width:300px;">
						<select class="itemSelector show-tick form-control"
						data-style="btn btn-default"
						data-live-search="true" style="border:none" name="item">
						<option selected value=""></option>
						<?php
						$itemsCategoryList  = $objItemCategory->getList();
						if(mysql_num_rows($itemsCategoryList)){
							while($ItemCat = mysql_fetch_array($itemsCategoryList)){
								$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
								?>
								<optgroup label="<?php echo $ItemCat['NAME']; ?>">
									<?php
									if(mysql_num_rows($itemList)){
										while($theItem = mysql_fetch_array($itemList)){
											if($theItem['ACTIVE'] == 'N'){
												continue;
											}
											if($theItem['INV_TYPE'] == 'B'){
												continue;
											}
											?>
											<option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
											<?php
										}
									}
									?>
								</optgroup>
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="clear"></div>

				<div class="caption">Report Type</div>
				<div class="field" style="width:300px;">
					<select id="repoType" class="form-control" name="repoType">
						<option value="S" selected>Sales</option>
						<option value="R">Sales Return</option>
					</select>
				</div><!--field-->
				<div class="clear"></div>

				<div class="caption">Tax Invoice</div>
				<div class="field" style="width:300px;position:relative;">
					<input type="radio" name="with_tax"  value="" id="radio0x" class="css-checkbox" checked  />
					<label for="radio0x" class="css-label-radio radGroup2"> All </label>
					<input type="radio" name="with_tax"  value="Y" id="radio1x" class="css-checkbox" />
					<label for="radio1x" class="css-label-radio radGroup2"> YES </label>
					<input type="radio" name="with_tax"  value="N" id="radio3x" class="css-checkbox" />
					<label for="radio3x" class="css-label-radio radGroup2"> NO </label>
				</div>
				<div class="clear"></div>


				<div class="caption">Transaction Type</div>
				<div class="field" style="width:300px;position:relative;">
					<input type="radio" name="account_type"  value="" id="radio0" checked class="css-checkbox" />
					<label for="radio0" class="css-label-radio radGroup1">All </label>
					<input type="radio" name="account_type"  value="010101" id="radio1" class="css-checkbox" />
					<label for="radio1" class="css-label-radio radGroup1"> Cash </label>
					<input type="radio" name="account_type"  value="010104" id="radio2" class="css-checkbox" />
					<label for="radio2" class="css-label-radio radGroup1"> Account </label>
					<input type="radio" name="account_type"  value="010102" id="radio3" class="css-checkbox" />
					<label for="radio3" class="css-label-radio radGroup1"> Card </label>
				</div>
				<div class="clear"></div>

				<div class="caption"></div>
				<div class="field">
					<input type="submit" value="Search" name="search" class="button"/>
				</div>
				<div class="clear"></div>
			</form>
		</div><!--form-->

		<?php
		if(isset($purchaseReport)){
			?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							Fabric Sale  Report
							<?php
							if($objSales->user_id != ''){
								$ozer_name = $objAccounts->getFullName($objSales->user_id);
								?>
								- <?php echo $ozer_name['FIRST_NAME']." ".$ozer_name['LAST_NAME']; ?>
								<?php
							}else{
								?>
								<?php echo ($titleRepo == '')?"":" - ".$titleRepo; ?>
								<?php
							}
							?>
						</p>
						<p style="font-size:16px;text-align:left;padding: 0px;">
							<?php echo ($objSales->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objSales->fromDate)); ?>
							<?php echo ($objSales->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objSales->toDate)); ?>
						</p>
						<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
						<div class="clear"></div>
					</div>
					<?php
					$prevBillNo = '';
					if(mysql_num_rows($purchaseReport)){
						?>
						<table class="tableBreak">
							<thead class="tHeader">
								<tr style="background:#EEE;">
									<th width="5%" style="font-size:12px;text-align:center">Inv#</th>
									<th width="8%" style="font-size:12px;text-align:center">Date</th>
									<?php
									if(!$supplierTypeReport){
										?>
										<th width="25%" style="font-size:12px;text-align:center">Customer</th>
										<?php
									}
									if(!$itemTypeReport){
										?>
										<th width="20%" style="font-size:12px;text-align:center">Item</th>
										<?php
									}
									?>
									<?php if($use_cartons == 'Y'){ ?>
										<th width="7%"  class="carton_th" style="font-size:12px;text-align:center"><?=$captionText['saleform']['cartons'];?></th>
										<th width="7%"  style="font-size:12px;text-align:center"><?=$captionText['saleform']['per_carton'];?></th>
										<?php } ?>
										<th width="7%"  style="font-size:12px;text-align:center"><?=$captionText['saleform']['quantity'];?></th>
										<th width="7%" style="font-size:12px;text-align:center"><?=$captionText['saleform']['unit_price'];?></th>
										<th width="7%" style="font-size:12px;text-align:center">Disc /@</th>
										<th width="7%" style="font-size:12px;text-align:center">Amount</th>
										<th width="7%" style="font-size:12px;text-align:center">Tax%</th>
										<th width="7%" class="tax_amnt_th" style="font-size:12px;text-align:center">Tax Amt</th>
										<th width="8%" style="font-size:12px;text-align:center">Total Amt</th>
										<th width="8%" style="font-size:12px;text-align:center">Cost</th>
										<th width="8%" style="font-size:12px;text-align:center">Profit/Loss</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$total_crtn   = 0;
									$total_qty    = 0;
									$total_price  = 0;
									$total_tax    = 0;
									$total_amount = 0;
									$total_cost   = 0;
									$total_pnl    = 0;
									while($detailRow = mysql_fetch_array($purchaseReport)){
										$supplierTitle = $objAccountCodes->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);
										if($detailRow['SERVICE_ID'] > 0){
											$item_id  = $detailRow['SERVICE_ID'];
											$itemName = $objServices->getTitle($item_id);
											$item_type= 'S';
										}else{
											$item_id  = $detailRow['ITEM_ID'];
											$itemName = $objItems->getRecordDetails($detailRow['ITEM_ID']);
											$itemName = substr($itemName['NAME'],0,25)."(".$itemName['ITEM_BARCODE'].")";
											$item_type= 'I';
										}
										$user_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
										$user_full_name = $user_full_name['FIRST_NAME']." ".$user_full_name['LAST_NAME'];

										$qull_cost = $detailRow['COST_PRICE']*$detailRow['QUANTITY'];

										$discount_type = ($detailRow['DISCOUNT_TYPE'] == 'P')?"%":$currency_type;

										if($detailRow['BILL_NO'] !== $prevBillNo){
											$thisBillNo = $detailRow['BILL_NO'];
										}else{
											$thisBillNo = '';
										}

										$total_pnl += ($detailRow['SUB_AMOUNT']-$qull_cost);
										?>
										<tr id="recordPanel" class="alt-row">
											<td class="text-center"><?php echo $thisBillNo; ?></td>
											<td class="text-left"><?php echo date('d/m/Y',strtotime($detailRow['SALE_DATE'])); ?></td>
											<?php
											if(!$supplierTypeReport){
												?>
												<td class="text-left"><?php echo substr($supplierTitle, 0,16); ?></td>
												<?php
											}
											if(!$itemTypeReport){
												?>
												<td class="text-left"><?php echo $detailRow['CATEGORY_NAME']; ?></td>
												<?php
											}
											?>
											<?php if($use_cartons == 'Y'){ ?>
												<td class="text-center"><?php echo $detailRow['CARTONS']; ?></td>
												<td class="text-center"><?php echo $detailRow['PER_CARTON']; ?></td>
												<?php } ?>
												<td class="text-center"><?php echo $detailRow['QUANTITY']; ?></td>
												<td class="text-center"><?php echo number_format($detailRow['UNIT_PRICE'],2); ?></td>
												<td class="text-center">
													<?php echo number_format($detailRow['SALE_DISCOUNT'],2); ?>
													<?php echo ($detailRow['DISCOUNT_TYPE'] == 'P')?"%":""; ?>
												</td>
												<td class="text-right"><?php echo  number_format($detailRow['SUB_AMOUNT'],2); ?></td>
												<td class="text-center"><?php echo number_format($detailRow['TAX_RATE'],2); ?></td>
												<td class="text-center"><?php echo number_format($detailRow['TAX_AMOUNT'],2); ?></td>
												<td class="text-right"><?php echo  number_format($detailRow['TOTAL_AMOUNT'],2); ?></td>
												<td class="text-right"><?php echo number_format(($qull_cost),2); ?></td>
												<td class="text-right"><?php echo number_format(($detailRow['SUB_AMOUNT']-$qull_cost),2); ?></td>
											</tr>
											<?php
											$total_crtn  		+= $detailRow['CARTONS'];
											$total_qty   		+= $detailRow['QUANTITY'];
											$total_price 		+= $detailRow['SUB_AMOUNT'];
											$total_tax 			+= $detailRow['TAX_AMOUNT'];
											$total_amount   += $detailRow['TOTAL_AMOUNT'];
											$prevBillNo 		 = $detailRow['BILL_NO'];
											$total_cost 		+= ($qull_cost);
										}
										?>

									</tbody>
									<?php
								}//end if
								if(mysql_num_rows($purchaseReport)){
									$total_price  = number_format($total_price,2);
									$total_tax    = number_format($total_tax,2);
									$total_amount = number_format($total_amount,2);
									$total_cost   = number_format($total_cost,2);

									$columnSkip = 3;
									if($itemTypeReport || $supplierTypeReport){
										$columnSkip = 4;
									}
									?>
									<tfoot class="tableFooter">
										<tr>
											<td style="text-align:right;" colspan="" class="total_tf">Total:</td>
											<?php if($use_cartons == 'Y'){ ?>
												<td style="text-align:center;"> <?php echo (isset($total_crtn))?$total_crtn:"0"; ?> </td>
												<td style="text-align:center;"> - - - </td>
												<?php } ?>
												<td class="text-center"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> - - - </td>
												<td class="text-right sub_amnt_tf"> <?php echo (isset($total_price))?$total_price:"0"; ?> </td>
												<td class="text-center tax_rate_tf"> - - - </td>
												<td class="text-center tax_amnt_tf"> <?php echo (isset($total_tax))?$total_tax:"0"; ?> </td>
												<td class="text-right"> <?php echo (isset($total_amount))?$total_amount:"0"; ?> </td>
												<td class="text-right"> <?php echo (isset($total_cost))?$total_cost:"0"; ?> </td>
												<td class="text-right"> <?php echo (isset($total_pnl))?number_format($total_pnl,2):"0"; ?> </td>
											</tr>
										</tfoot>
										<?php
									}
									?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					} //end if is generic report
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
if(isset($reportType)&&$reportType=='generic'){
	?>
	tab('1', '1', '2')
	<?php
}
?>
<?php
if(isset($reportType)&&$reportType=='specific'){
	?>
	tab('2', '1', '2')
	<?php
}
?>
</script>
