/* global $ */
$(document).ready(function() {
    $(".h3_php_error").click(function(){
	$(".h3_php_error").fadeOut('slow');
	});
	$("input[type='submit']").click(function(){
		$("#xfade").fadeIn('fast');
	});
	$(".barcode_input").change(function(){
		var barcode = $(this).val();
		if(barcode == ''){
			return;
		}
		if($("select.itemSelector option[data-subtext='"+barcode+"']").length){
			$("select.itemSelector option[data-subtext='"+barcode+"']").prop("selected",true).parent().selectpicker('refresh');
			$(".barcode_input").val('');
			$(this).getItemReturnDetails();
			$("input.quantity").focus();
			$(".barcode_input").attr('data-focus','Y');
		}
	});
	$(window).keyup(function(e){
		if(e.keyCode == 27){
			$(this).clearPanelValues();
			$("div.itemSelector button").focus();
			if($(".updateMode").length){
				$(".updateMode").removeClass('updateMode');
			}
		}
	});
	$("#main-nav li ul").hide();
	$("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	var fileSelf = $(".this_file_name").val();
		$("#main-nav li a.nav-top-item.no-submenu[href='"+fileSelf+"']").addClass('current');
		$("#main-nav li a.nav-top-item").next("ul").find('li').find("a[href='"+fileSelf+"']").addClass("current").parent().parent().prev("a.nav-top-item").addClass("current");
	$("#main-nav li .nav-top-item").hover(
		function () {
			$(this).stop().animate({ paddingRight: "25px" }, 200);
		},
		function () {
			$(this).stop().animate({ paddingRight: "15px" });
		}
	);
	$(".content-box-header h3").css({ "cursor":"default" }); // Give the h3 in Content Box Header a different cursor
	$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
	$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"

	// Content box tabs:

	$.fn.numericOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
			||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190
			|| (e.keyCode == 65 && e.ctrlKey === true)
			|| (e.keyCode >= 35 && e.keyCode <= 39)){
				return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$.fn.numericFloatOnly = function(){
		$(this).keydown(function(e){
			if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9
				||  e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 190 || e.keyCode == 110
				|| (e.keyCode == 65 && e.ctrlKey === true)
				|| (e.keyCode >= 35 && e.keyCode <= 39)){
					return true;
			}else{
				if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
					e.preventDefault();
				}
			}
		});
	};
	$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
	$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
	$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

	$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
		function() {
			$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
			$(this).addClass('current'); // Add class "current" to clicked tab
			var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
			$(currentTab).siblings().hide(); // Hide all content divs
			$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
			return false;
		}
	);

	//Close button:

	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);

	// Alternating table rows:

	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

	// Check all checkboxes when the one in a table head is checked:

	$('.check-all').click(
		function(){
			$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
		}
	);
	$.fn.sumColumn = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseInt($(this).text());
        });
		$(insertToFeild).text(sumAll);
	};
	$.fn.sumColumnFloat = function(sumOfFeild,insertToFeild){
		var sumAll = 0;
		$(sumOfFeild).each(function(index, element) {
             sumAll += parseFloat($(this).text())||0;
        });
		sumAll = Math.round(sumAll*100)/100;
		$(insertToFeild).text(sumAll);
	};
	$.fn.multiplyTwoFeilds = function(multiplyToElmVal,writeProductToElm){
		$(writeProductToElm).val($(this).val()*$(multiplyToElmVal).val());
	};
	$.fn.multiplyTwoFloats = function(multiplyToElmVal,writeProductToElm){
		$(this).keyup(function(e){
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var productVal = Math.round((thisVal*thatVal)*100)/100;
			$(writeProductToElm).val(productVal);
		});
	};
	$.fn.deleteRowConfirmation = function(file){
		var idValue = $(this).attr("do");
		var currentRow = $(this).parent().parent();
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$.post(file, {id : idValue}, function(data){
			var supplierName = currentRow.children("td").first().next().next().text();
			if(data==1){
				$("#popUpDel .confirm").text(" "+supplierName+"Contains Information! Do You Really Want To Delete "+supplierName+"?.");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						if(data == 'L'){
							$("#popUpDel .confirm").text(" Stock is Issued To Sheds Cannot Delete This Record! ");
							$(".dodelete").hide();
							$(".nodelete").text('Close');
							$(".nodelete").click(function(){
								$("#popUpDel").slideUp();
								$("#xfade").fadeOut();
							});
						}else{
							currentRow.slideUp();
							$("#popUpDel").slideUp();
							$("#xfade").fadeOut();
						}
					});
				});
			}else{
				$("#popUpDel .confirm").text("Are Sure you Want To Delete "+supplierName+"?");
				$("#popUpDel").slideDown();
				$(".dodelete").click(function(){
					$.post(file, {cid : idValue}, function(data){
						currentRow.slideUp();
						$("#popUpDel").slideUp();
						$("#xfade").fadeOut();
					});
				});
			}
		});
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.deletePanel = function(file){
		var idValue = $(this).attr("do");
		$("#xfade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Confirm Delete?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#xfade").fadeIn();
		$("#popUpDel").centerThisDiv();
		$("#popUpDel .confirm").text("Are Sure you Want To Delete ?");
        $("#popUpDel").slideDown();
        $(".dodelete").click(function(){
            $.post(file, {cid : idValue}, function(data){
                data = $.parseJSON(data);
                if(data['OK'] == 'N'){

                }
                $("#panel-"+idValue).slideUp(function(){
                    $(this).remove();
                });
                $("#popUpDel").slideUp();
                $("#xfade").fadeOut();
            });
        });
		$(".nodelete").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut();
			});
		$(".close_popup").click(function(){
			$("#popUpDel").slideUp();
			$("#xfade").fadeOut('fast');
			});
	};
	$.fn.multiplyTwoFloatsCheckTax = function(multiplyToElmVal,writeProductToElm,writeProductToElmNoTax,taxAmountElm){
		$(this).keyup(function(e){
			var taxRate = parseFloat($("input.taxRate").val())||0;
			var taxType = ($(".taxType").is(":checked"))?"I":"E";
			var thisVal = parseFloat($(this).val())||0;
			var thatVal = parseFloat($(multiplyToElmVal).val())||0;
			var amount = Math.round((thisVal*thatVal)*100)/100;
			if(taxRate > 0){
				if(taxType == 'I'){
					taxAmount = amount*(taxRate/100);
					amount -= taxAmount;
				}else if(taxType == 'E'){
					taxAmount = amount*(taxRate/100);
				}

			}else{
				taxAmount = 0;
			}
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			var finalAmount = Math.round((amount+taxAmount)*100)/100;
			$(taxAmountElm).val(taxAmount);
			$(writeProductToElm).val(finalAmount);
			$(writeProductToElmNoTax).val(amount);
			$(this).calculateRowTotal();
		});
	};
	$.fn.centerThisDiv = function(){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$(this).height()/2;
		win_width = win_width-$(this).width()/2;
		$(this).css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
	};
	//inventory.php end
	$.fn.setFocusTo = function(Elm){
		$(this).keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}else{
					e.preventDefault();
					$(Elm).focus();
				}
			}
		});
	};
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		showAnim : 'show',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+10'
	});
	$.fn.getItemDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		if(item_id != ''){
			$.post('db/get-item-details.php',{p_item_id:item_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
				var itemPrice = data['P_PRICE'];
				$("input.unitPrice").val(itemPrice);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
			});
		}
	};
	$.fn.getItemReturnDetails = function(){
		var item_id = $(".itemSelector option:selected").val();
		var sale_id = parseInt($("input.sale_id").val())||0;
		if(item_id != ''&&sale_id>0){
			$.post('db/getDistReturnDetails.php',{p_item_id:item_id,sale_id:sale_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];
				var itemPrice = parseFloat($("td[data-saleprice-id="+item_id+"]").text())||0;
				$("input.unitPrice").val(itemPrice);
        $("input.rate_carton").val(data['RATE_CARTON']);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
				$(".qty_limit").val(data['LIMIT_QTY']);
        $(".carton_limit").val(data['LIMIT_CARTONS']);
        $("input.qty_carton").val(data['QTY_CARTON']);
				$(".qty_returned").val(data['RETURNED']);
			});
		}
	};
	$.fn.quickSave = function(){
		var item_id       = parseInt($(".itemSelector option:selected").val())||0;
		var item_name     = $(".itemSelector option:selected").text();
    var cartons       = parseFloat($("input.cartons").val())||0;
    var qty_carton    = parseFloat($("input.cartons").attr('data-qty'))||0;
    var rate_carton   = parseFloat($("input.rate_carton").val())||0;
		var quantity      = parseFloat($("input.quantity").val())||0;
		var unitPrice     = parseFloat($("input.unitPrice").val())||0;
		var discount      = parseFloat($("input.discount").val())||0;
		var subAmount     = parseFloat($("input.subAmount").val())||0;
		var taxRate       = parseFloat($("input.taxRate").val())||0;
		var taxAmount     = parseFloat($("input.taxAmount").val())||0;
		var totalAmount   = parseFloat($("input.totalAmount").val())||0;
		var updateMode    = $(".updateMode").length;
		var discount_type = $("input.discount_type").val();

		if(discount_type == 'P'){
			discountPerCentage = quantity*unitPrice*(discount/100);
			discountPerCentage = Math.round(discountPerCentage*100)/100;
		}else{
			discountPerCentage = discount;
		}

		$(this).blur();
		if((quantity+cartons) == 0){
			return false;
		}
		if(taxRate == '' || taxRate == 0){
			taxAmount = 0;
		}
		if ($("tbody.trans-rows td[data-item-id='"+item_id+"']").length&&updateMode==0){
			displayMessage(item_name+" Already Included In Bill!");
			return false;
		}
		if(item_id > 0 && unitPrice > 0 && subAmount > 0 && totalAmount > 0){
			if(updateMode == 0){
        var the_row = '<tr class="alt-row calculations transactions" data-row-id="0">';
            the_row+= '<td class="text-left itemName"  data-item-id="'+item_id+'">'+item_name+'</td>';
            the_row+= '<td class="text-center cartons" data-qty="'+qty_carton+'">'+cartons+'</td>';
            the_row+= '<td class="text-center rate_carton">'+rate_carton+'</td>';
            the_row+= '<td class="text-center quantity">'+quantity+'</td>';
            the_row+= '<td class="text-center unit_price">'+unitPrice+'</td>';
            the_row+= '<td class="text-center sale_discount" data-dsct="'+discountPerCentage+'">'+discount+'</td>';
            the_row+= '<td class="text-center sub_amount">'+subAmount+'</td>';
            the_row+= '<td class="text-center tax_rate">'+taxRate+'</td>';
            the_row+= '<td class="text-center tax_amount">'+taxAmount+'</td>';
            the_row+= '<td class="text-center total_amount">'+totalAmount+'</td>';
            the_row+= '<td class="text-center"> - - - </td>';
            the_row+= '<td class="text-center">';
            the_row+= '<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>';
            the_row+= '<a class="pointer" do="" title="Delete" onclick="$(this).parent().parent().remove();"><i class="fa fa-times"></i></a>';
            the_row+= '</td>';
            the_row+= '</tr>';
			$(the_row).insertAfter($(".calculations").last());
			}else if(updateMode == 1){
				$("tr.updateMode").find('td.itemName').text(item_name);
				$("tr.updateMode").find('td.itemName').attr('data-item-id',item_id);
        $("tr.updateMode").find('td.cartons').text(cartons);
        $("tr.updateMode").find('td.cartons').attr('data-qty',qty_carton);
        $("tr.updateMode").find('td.rate_carton').text(rate_carton);
				$("tr.updateMode").find('td.quantity').text(quantity);
				$("tr.updateMode").find('td.unit_price').text(unitPrice);
				$("tr.updateMode").find('td.sale_discount').text(discount);
				$("tr.updateMode").find('td.sub_amount').text(subAmount);
				$("tr.updateMode").find('td.tax_rate').text(taxRate);
				$("tr.updateMode").find('td.tax_amount').text(taxAmount);
				$("tr.updateMode").find('td.total_amount').text(totalAmount);
				$("tr.updateMode").removeClass('updateMode');
			}
			$(this).clearPanelValues();
			$(this).calculateColumnTotals();
			if($(".barcode_input").attr('data-focus') == 'Y'){
				$(".barcode_input").focus();
			}else{
				$("select.itemSelector").parent().find(".dropdown-toggle").focus();
			}
		}else{
			displayMessage('Values Missing!');
		}
		$(".over_discount").keyup();
	};

	$.fn.calculateColumnTotals = function(){
    $(this).sumColumnFloat("table.input_table td.cartons",    "table.input_table  td.cartnTotal");
		$(this).sumColumnFloat("table.input_table td.quantity",   "table.input_table  td.qtyTotal");
		$(this).sumColumnFloat("table.input_table td.sub_amount",  "table.input_table  td.amountSub");
		$(this).sumColumnFloat("table.input_table td.tax_amount",  "table.input_table  td.amountTax");
		$(this).sumColumnFloat("table.input_table td.total_amount","table.input_table  td.amountTotal");
	};
	$.fn.calculateRowTotal = function(){
		var discount_type = $("input.discount_type").val();
		var taxRate       = parseFloat($("input.taxRate").val())||0;
		var taxType       = ($(".taxType").is(":checked"))?"I":"E";
    var cartons       = parseFloat($("input.cartons").val())||0;
    var rate_carton   = parseFloat($("input.rate_carton").val())||0;
		var qty           = parseFloat($("input.quantity").val())||0;
		var unitPrice     = parseFloat($("input.unitPrice").val())||0;

		var amount        = ((qty*unitPrice)+(cartons*rate_carton)).toFixed(2);
		var discountAvail = parseFloat($("input.discount").val())||0;

		if(discount_type == 'P'){
			taxAmount = Math.round(taxAmount*100)/100;
			amount = Math.round(amount*100)/100;
			discountAvail = Math.round(discountAvail*100)/100;
			discountPerCentage = amount*(discountAvail/100);
			discountPerCentage = Math.round(discountPerCentage*100)/100;
		}else{
			discountPerCentage = discountAvail;
		}

		amount -= discountPerCentage;
		amount = Math.round(amount*100)/100;

		if(taxRate > 0){
			if(taxType == 'I'){
				taxAmount = amount*(taxRate/100);
			}else if(taxType == 'E'){
				taxAmount = amount*(taxRate/100);
			}
		}else{
			taxAmount = 0;
		}
		var finalAmount = Math.round((amount+taxAmount)*100)/100;
		finalAmount = Math.round(finalAmount*100)/100;
		if(taxType == 'I'){
			amount		-= taxAmount;
			finalAmount -= taxAmount;
		}
		$("input.subAmount").val(amount);
		$("input.taxAmount").val(taxAmount);
		$("input.totalAmount").val(finalAmount);
	};
	$.fn.calculateFinalAmount = function(){
		$(this).keyup(function(){
			var discount_type = $("input.discount_type").val();
			var amount        = parseFloat($(".totals .amountTotal").text())||0;
			var bill_discount = 0;

			$("td.discountAmount").each(function(){
				bill_discount += parseFloat($(this).attr('data-dsct'))||0;
			});

			var over_discount = parseFloat($("input.over_discount").val())||0;
			var the_amount    = amount;
			var disk  		  = 0;
			if(discount_type == 'P'){
				disk = (amount*over_discount)/100;
				the_amount = amount - disk;
			}else{
				the_amount = amount - over_discount;
			}
			$("input.bill_discount").val(bill_discount+disk);
			$("input.over_total").val(the_amount);
		});
	};
	$.fn.updateStockInHand = function(){
		var stockOfBill = 0;
		$("td.quantity").each(function(index, element) {
            stockOfBill += parseInt($(this).text())||0;
        });
		stockOfBill += parseInt($("input.quantity").val())||0;
	};
	$.fn.clearPanelValues = function(){
		$("select.itemSelector option").prop('selected',false);
		$("select.itemSelector").selectpicker('refresh');
    $("input.cartons").val('');
    $("input.rate_carton").val('');
		$("input.quantity").val('');
		$("input.unitPrice").val('');
		$("input.discount").val('');
		$("input.subAmount").val('');
		$("input.taxRate").val('');
		$("input.taxAmount").val('');
		$("input.totalAmount").val('');
		$("input.inStock").val('').attr('thestock','');
	};
});
		// PreLoad FUNCTION //
		//////////////////////
	var letMeGo = function(thisElm){
		$(thisElm).fadeOut(300,function(){
			$(this).html('');
		});
	};
	var editThisRow = function(thisElm){
		var thisRow 	= $(thisElm).parent('td').parent('tr');
		$(".updateMode").removeClass('updateMode');
		thisRow.addClass('updateMode');
		var item_name 	= thisRow.find('td').text();
		var item_id 	  = parseFloat(thisRow.find('td.itemName').attr('data-item-id'))||0;
		var cartons 	  = parseFloat(thisRow.find('td.cartons').text())||0;
    var rate_carton = parseFloat(thisRow.find('td.rate_carton').text())||0;
    var quantity 	  = parseFloat(thisRow.find('td.quantity').text())||0;
		var unitPrice 	= parseFloat(thisRow.find('td.unit_price').text())||0;
		var discount 	  = parseFloat(thisRow.find('td.discount').text())||0;
		var subAmount 	= parseFloat(thisRow.find('td.subAmount').text())||0;
		var taxRate 	  = parseFloat(thisRow.find('td.taxRate').text())||0;
		var taxAmount   = parseFloat(thisRow.find('td.taxAmount').text())||0;
		var totalAmount = parseFloat(thisRow.find('td.totalAmount').text())||0;
		if(item_id == 0){
			return false;
		}

		var sale_id = parseInt($("input.sale_id").val())||0;
		if(item_id != ''&&sale_id>0){
			$.post('db/getDistReturnDetails.php',{p_item_id:item_id,sale_id:sale_id},function(data){
				data = $.parseJSON(data);
				var itemStock = data['STOCK'];

        $("select.itemSelector option[value='"+item_id+"']").prop("selected",true);
        $("select.itemSelector").selectpicker('refresh');

				$("input.unitPrice").val(unitPrice);
        $("input.rate_carton").val(data['RATE_CARTON']);
				$("input.inStock").attr('thestock',itemStock).val(itemStock);
				$(".qty_limit").val(data['LIMIT_QTY']);
        $(".carton_limit").val(data['LIMIT_CARTONS']);
				$(".qty_returned").val(data['RETURNED']);

        $("input.cartons").val(cartons);
        $("input.rate_carton").val(rate_carton);
        $("input.qty_carton").val(data['QTY_CARTON']);
        $("input.quantity").val(quantity);

        $("input.unitPrice").val(unitPrice);
  			$("input.discount").val(discount);
  			$("input.subAmount").val(subAmount);
  			$("input.taxRate").val(taxRate);
  			$("input.taxAmount").val(taxAmount);
  			$("input.totalAmount").val(totalAmount);
  			stockOs();
  			$("div.itemSelector button").focus();

			});
		}
	};
	var stockOs = function(){
		return;
    var NewStock = 0;
    var cartons     = parseFloat($("input.cartons").val())||0;
    var qty_carton  = parseFloat($("input.qty_carton").val())||0;
    var crtns_qty   = cartons*qty_carton;
		var thisQty     = parseFloat($("input.quantity").val())||0;
		var inStock     = parseFloat($("input.inStock").attr('thestock'))||0;
		var limit_qty   = parseFloat($("input.qty_limit").val())||0;
        var limit_crtn  = parseFloat($("input.carton_limit").val())||0;
		var item_id     = parseFloat($("select.itemSelector option:selected").val());

		if((limit_qty+(limit_crtn*qty_carton)) >= (thisQty+crtns_qty)){
			var NewStock =  inStock - limit_qty;
			$("input.inStock").val(NewStock);
      		return;
		}else{
			if ($(".wel_made tr td[data-item-id='" + item_id + "']").length){
				$("input.quantity").val(limit_qty);
				$("input.cartons").val(limit_crtn);
				$("input.inStock").val(inStock);
				$(".wel_made tr td[data-item-id='" + item_id + "']").next("td").animate({ 'background-color': 'rgba(255,0,0,0.6)' }, 300, function () {
					$(".wel_made tr td[data-item-id='" + item_id + "']").next("td").animate({ 'background-color': 'rgba(255,255,255,1.0)' }, 300);
				});
			}
    }
		NewStock =  inStock + thisQty;
		if(NewStock < 0){
			$("input.quantity").val('');
		}else{
			$("input.inStock").val(NewStock);
		}
	};
	var saveSale = function(){
		var sale_id		   = $("input.sale_id").val();
		var sale_return_id = $("input.sale_return_id").val();
		var saleDate       = $("input.datepicker").val();
		var billNum        = $("input.bill_number").val();
		var po_number      = $("input.po_number").val();
		var supplierCode   = $("select.supplierSelector option:selected").val();
		var supplier_name  = $("input.supplier_name").val();
    	var order_taker_id = parseInt($("input[name='order_taker_id']").val())||0;
    	var salesman_id    = parseInt($("input[name='salesman_id']").val())||0;
		var over_discount  = $("input.over_discount").val();
		var discount_type  = $("input.discount_type").val();
		var bill_discount  = $("input.bill_discount").val();
		var over_total     = $("input.over_total").val();

		if($("tr.transactions").length == 0){
			displayMessage('No Transactions Made!');
			return false;
		}
		$(".savePurchase").hide();
		var jSonString = {};

		$("tr.transactions").each(function(index, element){
      jSonString[index] = {};
      jSonString[index].rowId       = parseInt($(this).attr('data-row-id'))||0;
      jSonString[index].item_id     = parseInt($(this).find('td.itemName').attr('data-item-id'))||0;
      jSonString[index].cartons     = parseFloat($(this).find('td.cartons').text())||0;
      jSonString[index].rate_carton = parseFloat($(this).find('td.rate_carton').text())||0;
      jSonString[index].quantity    = parseFloat($(this).find('td.quantity').text())||0;
      jSonString[index].unitPrice   = parseFloat($(this).find('td.unit_price').text())||0;
      jSonString[index].discount    = parseFloat($(this).find('td.sale_discount').text())||0;
      jSonString[index].subAmount   = parseFloat($(this).find('td.sub_amount').text())||0;
      jSonString[index].taxRate     = parseFloat($(this).find('td.tax_rate').text())||0;
      jSonString[index].taxAmount   = parseFloat($(this).find('td.tax_amount').text())||0;
      jSonString[index].totalAmount = parseFloat($(this).find('td.total_amount').text())||0;
    });
    jSonString = JSON.stringify(jSonString);
		$.post("db/saveDistSaleReturn.php",{sale_return_id:sale_return_id,
									  		sale_id:sale_id,
									  		saleDate:saleDate,
									  		billNum:billNum,
									  		po_number:po_number,
									  		customerCode:supplierCode,
									  		supplier_name:supplier_name,
									  		order_taker:order_taker_id,
									  		salesman:salesman_id,
									  		over_discount:over_discount,
									  		discount_type:discount_type,
									  		bill_discount:bill_discount,
									  		over_total:over_total,
									  		jSonString:jSonString},function(data){
				data = $.parseJSON(data);
				if(data['ID'] > 0){
					window.location.href = 'dist-sale-return-by-bill.php?sid='+sale_id+"&saved";
				}else{
					displayMessage(data['MSG']);
				}
		});
	};
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete btn btn-info'>Close</a></div>");
		$("#popUpDel").hide();
		$("#fade").fadeIn('slow');
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		win_hi = win_hi-$("#popUpDel").height()/2;
		win_width = win_width-$("#popUpDel").width()/2;
		$("#popUpDel").css({
			'position': 'fixed',
			'top': win_hi,
			'left': win_width
		});
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#popUpDel").slideDown(function(){
				$("#popUpDel").remove();
			});
			$("#fade").fadeOut('slow');
		});
	};

	var shorDeleteRowDilog = function(rowChildElement){
		var idValue = $(rowChildElement).attr("do");
		var clickedDel = $(rowChildElement);
		$("#fade").hide();
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete btn btn-danger'>Delete</a><a class='nodelete btn btn-info'>Cancel</a></div>");
		$("#popUpDel").hide();
		$("#popUpDel").centerThisDiv();
		$("#fade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".dodelete").click(function(){
			$("#popUpDel").children(".confirm").text('Row Deleted Successfully!');
			$("#popUpDel").children(".dodelete").hide();
			$("#popUpDel").children(".nodelete").text("Close");
			clickedDel.parent().parent().remove();
		});
		$(".nodelete").click(function(){
			$("#fade").fadeOut();
			$("#popUpDel").fadeOut();
			});
		$(".close_popup").click(function(){
		$("#popUpDel").slideUp();
		$("#fade").fadeOut('fast');
		});
	};
	var getBillsBySupplierCode = function(){
		var supplierCode = $("select.supplierSelector option:selected").val();
		if(supplierCode == ''){
			$("div.supplierSelector button").focus();
			return false;
		}
		$.post('db/getBillListBySupplerCode.php',{supplierCode:supplierCode},function(data){
			if(data != ''){
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").append(data);
				$("select.billSelector").selectpicker('refresh');
				$("div.billSelector button").focus();
			}else{
				$("select.billSelector").find("option").first().nextAll().remove();
				$("select.billSelector").selectpicker('refresh');
				$("div.supplierSelector button").focus();
			}
		});
	};
	var display_alert = function(message){
		$(".message_div").text(message);
		$(".message_div").fadeIn(1000).delay(1000).fadeOut(1000);
	};
	var getBillDetails = function(){
		var supplier_code  = $("select.supplierSelector option:selected").val();
		var bill_number	   = parseInt($("select.billSelector option:selected").val())||0;
		var sale_id    = $(".sale_id").val();
		if(bill_number == 0 && supplier_code == ''){
			return false;
		}
		$.post('db/getPurchaseBill.php',{purchase_id:purchase_id,supplier_code:supplier_code,bill_num:bill_number},function(data){
			$("tr.calculations").first().nextAll(".calculations").remove();
			$(data).insertAfter($("tr.calculations").first());
			$(this).calculateColumnTotals();
			if(purchase_id == 0){
				$("tr.isRedRow").find('td').animate({'background-color':'rgba(255,0,0,0.2)'},300);;
			}
		});
	};
	var get_recent_sale_returns = function($element){
		var sale_id = parseInt($(".sale_id").val())||0;
		$.get('db/get-recent-dist-sale-returns.php',{sale_id:sale_id},function(data){
			$($element).html(data);
		});
	};
