<?php
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/dist_sale.php');
    include ('common/classes/dist_sale_return.php');
    include ('common/classes/accounts.php');
    include ('common/classes/j-voucher.php');
    include ('common/classes/items.php');
    include ('common/classes/services.php');
    include ('common/classes/customers.php');

    //Permission
    if ((!in_array('sales', $permissionz)) && (!in_array('sales-panel', $permissionz)) && ($admin != true)) {
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objSale            	  = new DistributorSale();
    $objDistributorSaleReturn = new DistributorSaleReturn();
    $objJournalVoucher  = new JournalVoucher();
    $objAccountCodes    = new ChartOfAccounts();
    $objItems           = new Items();
    $objServices        = new Services();
    $objCustomers       = new Customers();
    $objConfigs         = new Configs();


    $totalRows          = $objSale->countRows();
    $salesPanelFile     = (in_array('sales-panel', $permissionz) && $salePanelModule == 'Y') ? "sale-panel.php" : "dist-sale-details.php";
    $invoice_format     = $objConfigs->get_config('INVOICE_FORMAT');
    $invoice_format     = explode('_', $invoice_format);
    $currency_type      = $objConfigs->get_config('CURRENCY_TYPE');
    $salesman_specific  = $objConfigs->get_config('SPECIFIC_SALESMAN');

    $total = $objConfigs->get_config('PER_PAGE');

    $invoiceSize = $invoice_format[0]; // S  L

    if ($invoiceSize == 'L') {
        $invoiceFile = 'dist-sales-invoice.php';
    } elseif ($invoiceSize == 'M') {
        $invoiceFile = 'dist-sales-invoice-duplicates.php';
    } elseif ($invoiceSize == 'S') {
        $invoiceFile = 'dist-sales-invoice-small.php';
    }
    $origin_invoice = $invoiceFile;
    $customersList  = $objCustomers->getList();
	$cashAccounts   = $objAccountCodes->getAccountByCatAccCode('010101');
	
	$objSale->fromDate = date("Y-m-01");
	$objSale->toDate   = date("Y-m-d");

    if (isset($_GET['search'])) {
        $objSale->fromDate          = ($_GET['fromDate'] == '') ? date("Y-m-01") : date('Y-m-d', strtotime($_GET['fromDate']));
        $objSale->toDate            = ($_GET['toDate'] == '') ? date("Y-m-d") : date('Y-m-d', strtotime($_GET['toDate']));
        $objSale->order_taker       = mysql_real_escape_string($_GET['order_taker']);
        $objSale->user_id           = mysql_real_escape_string($_GET['salesman']);
        $objSale->supplierAccCode   = mysql_real_escape_string($_GET['supplierAccCode']);
        $objSale->po_number         = mysql_real_escape_string($_GET['po_number']);
        $objSale->billNum           = (int)mysql_real_escape_string($_GET['billNum']);
        $objSale->with_tax          = isset($_GET['tax_invoice']) ? $_GET['tax_invoice'] : "";
    }
    if (!$admin) {
        $objSale->user_id = $user_id;
    }
    if (isset($_GET['page'])) {
        $this_page = $_GET['page'];
        if ($this_page >= 1) {
            $this_page--;
            $start = $this_page * $total;
        }
    } else {
        $start = 0;
        $this_page = 0;
    }
    $saleList = $objSale->search($start, $total);
    $found_records = $objSale->found_records;
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style type="text/css">
	table td{
		padding: 7px !important;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/dist.sale.config.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select").selectpicker();
			$("a.delete_record").click(function(){
				$(this).deleteMainRow("db/del-dist-sale.php");
			});
			$("td[data-items-list]").on("mouseover",function(){
				var data = $(this).attr("data-items-list");
				$(this).text(data);
			});
			$("td[data-items-list]").on("mouseout",function(){
				var data = $(this).attr("data-items-list");
				$(this).text(data.substr(0,40));
			});
            $("input.receipt_date").change(function(){
                $("input.receipt_date").val($(this).val());
			});
			$("#account-ledger-modal").on('show.bs.modal',function(){
				
			});
			$("a.show-ledger-popup").click(function(){
				$("#account-ledger-modal").modal();
				$("#account-ledger-modal .modal-body").html("Please Wait...");
				$.get($(this).attr("data-href"),function(html_data){
					$("#account-ledger-modal .modal-body").html($(html_data).find("#bodyTab").html());
					$("#account-ledger-modal .repoGen").remove();
				});
			});
            $("input.receipt_amount").keydown(function(e){
                if(e.which==13){
					var $this = $(this);
					var dist_sale_id   = (parseInt($(this).parent().parent().attr("data-row-id"))||0);
                    var account_code   = $(this).attr('data-account');
                    var receipt_amount = parseFloat($(this).val())||0;
					var receipt_date   = $(this).parent().prev("td.receipt_date").find("input.receipt_date").val();
					
					if(receipt_amount == 0){
						$(this).addClass('input-invalid');
						return;
					}
					$this.prop('readonly',true);
                    $.post('db/save-payment-voucher.php',{dist_sale_id:dist_sale_id,account_code:account_code,receipt_amount:receipt_amount,receipt_date:receipt_date},function(data){
                        $this.prop('readonly',false);
                        data = $.parseJSON(data);
						$this.val('');
						$this.parent().parent();
						displayMessage(data['MSG']);
						$("tr[data-row-id="+dist_sale_id+"] td.receipt_sum").text(data['RECEIPT_SUM']);
						$("tr[data-row-id="+dist_sale_id+"] td.receipt_sum").addClass('bg-info');
						sum_receipt_total();
                    });
                }
            }).on("keyup change",function(){
				if($(this).val()!=''){
					$(this).removeClass('input-invalid');
				}
			});
		});
		var sum_receipt_total = function(){
			var sum_amnt = 0;
			$("td.receipt_sum").each(function(){
				sum_amnt += (parseFloat($(this).text().replace(',','')) || 0);
			});
			$("th.recpt_table_total").text((sum_amnt).toFixed(2));;
		}
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include ("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Cash Mangement - Distributor Sales </p>
				<span id="tabPanel">
					<div class="tabPanel">
						<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
						<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
					</div>
				</span>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->
			<div class="content-box-content">
				<div id="bodyTab1" style="display:block;">
					<h4 class="pull-left m-20">Showing <?php echo $found_records; ?> Records from Period <span class="badge"><?=date("d-M-Y",strtotime($objSale->fromDate))?></span> To <span class="badge"><?=date("d-M-Y",strtotime($objSale->toDate))?></span> </h4>
					<a class="btn btn-default m-10 pull-right" target="_blank" href="posted-enteries.php?v_type=CR&fromJv=&toJv=&fromDate=<?=$objSale->fromDate;?>&toDate=<?=$objSale->toDate;?>&search=Search">Cash Receipts List</a>
					<div class="clear" style="height: 20px;"></div>
					<table width="100%" cellspacing="0" class="table-striped" >
						<thead>
							<tr>
								<th class="bold-text p-5" style="text-align:center;width:5%;">Sr.</th>
								<th class="bold-text p-5" style="text-align:center;width:5%;">Bill#</th>
								<th class="bold-text p-5" style="text-align:center;width:5%;">Order.Date</th>
								<th class="bold-text p-5" style="text-align:center;width:5%;">Exe.Date</th>
								<th class="bold-text p-5" style="text-align:center;width:15%;">Customer</th>
								<th class="bold-text p-5" style="text-align:center;width:15%;">Items</th>
								<th class="bold-text p-5" style="text-align:center;width:5%">Qty Sold</th>
								<th class="bold-text p-5" style="text-align:center;width:5%;">Amount</th>
                                <th class="bold-text p-5" style="text-align:center;width:5%;">Returns</th>
                                <th class="bold-text p-5" style="text-align:center;width:8%;">Net Amount</th>
								<th class="bold-text p-5" style="text-align:center;width:8%;">ReceiptDate</th>
                                <th class="bold-text p-5" style="text-align:center;width:8%;">ReceiptAmt</th>
								<th class="bold-text p-5" style="text-align:center;width:5%;">TotalRecpt</th>
                                <th class="bold-text p-5" style="text-align:center;width:5%;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$table_amount_recpt_total = 0;
								$table_quantity_total     = 0;
								$table_amount_total       = 0;
								$table_ramount_total      = 0;
								$table_total_amount_total = 0;
								$counter = $start+1;
if (mysql_num_rows($saleList)) {
    while ($saleRow = mysql_fetch_array($saleList)) {
        $billTotal = $objSale->getInventoryAmountSum($saleRow['ID']);
        $supplierTitle = $objAccountCodes->getAccountTitleByCode($saleRow['CUST_ACC_CODE']);
        $billTotalQuantity = $objSale->getQuantityPerBill($saleRow['ID']);
        $billTotalQuantity = array_sum($billTotalQuantity);
        $itemsArray = $objSale->getSaleDetailArrayItemOnly($saleRow['ID']);
		$servicesArray = $objSale->getSaleDetailArrayServiceOnly($saleRow['ID']);
        $itemsList = '';
        foreach ($servicesArray as $key => $service_id) {
            if ($key > 0) {
                $itemsList.= ', ';
            }
            $itemsList.= $objServices->getTitle($service_id);
        }
        if ($itemsList != '') {
            $itemsList.= ', ';
        }
        foreach ($itemsArray as $key => $item_id) {
            if ($key > 0) {
                $itemsList.= ', ';
            }
            $itemsList.= $objItems->getItemTitle($item_id);
        }
        if ($saleRow['DISCOUNT_TYPE'] == 'P') {
            $billTotal-= (($billTotal * $saleRow['DISCOUNT']) / 100);
        } else {
            $billTotal-= $saleRow['DISCOUNT'];
		}
		$billTotal -= $saleRow['TRADE_OFFER'];
        $billTotal += $saleRow['CHARGES'];
		$returnAmount = (float)$objDistributorSaleReturn->getInventoryAmountSumByBillNumber($saleRow['BILL_NO']);
        $netTotal     =  $billTotal - $returnAmount;

        if ($saleRow['WITH_TAX'] == "Y") {
            $invoiceFile = 'sales-tax-invoice.php';
        } else {
            $invoiceFile = $origin_invoice;
		}

		$rcpt_sum    = $objJournalVoucher->getTransactionSumByRef('0101010001','Dr',$saleRow['ID'],'DSV');

		$table_amount_recpt_total   += $rcpt_sum;
		$table_quantity_total 		+= $billTotalQuantity;
		$table_amount_total 		+= $billTotal;
		$table_ramount_total 		+= $returnAmount;
		$table_total_amount_total 	+= $netTotal;

?>
									<tr data-row-id="<?php echo $saleRow['ID']; ?>">
										<td class="text-center"><?=$counter;?></td>
										<td style="text-align:center"><?php echo $saleRow['BILL_NO']; ?></td>
										<td style="text-align:center"><?php echo date('d-m-y', strtotime($saleRow['ORDER_DATE'])); ?></td>
										<td style="text-align:center"><?php echo date('d-m-y', strtotime($saleRow['SALE_DATE'])); ?></td>
										<td style="text-align:left">
                                            <?php echo $supplierTitle; ?>
                                            <a  href="#!" class="badge pull-right show-ledger-popup"  data-href="ledger-details.php?accountCode=<?php echo $saleRow['CUST_ACC_CODE']; ?>&fromDate=<?php echo date('01-m-Y'); ?>&toDate=<?php echo date('d-m-Y'); ?>&generalReport=Generate" title="Ledger"><i class="fa fa-book"></i></a>
                                        </td>
										<td style="text-align:left;" class="pl-10" data-items-list="<?php echo $itemsList; ?>"><?php echo substr($itemsList, 0, 40); ?></td>
										<td style="text-align:center"><?php echo $billTotalQuantity; ?></td>
										<td style="text-align:center"><?php echo number_format($billTotal, 2); ?></td>
                                        <td style="text-align:center"><?php echo number_format($returnAmount, 2); ?></td>
                                        <td style="text-align:center"><?php echo number_format($netTotal, 2); ?></td>
										<td class="text-center receipt_date">
                                            <input type="text" class="form-control datepicker receipt_date" value="<?php echo date('d-m-Y');?>" />
                                        </td>
                                        <td class="text-center">
                                            <input type="text" class="form-control text-center receipt_amount" data-account="<?php echo $saleRow['CUST_ACC_CODE']; ?>" value=""   />
                                        </td>
										<td class="text-center receipt_sum"><?=number_format($rcpt_sum,2);?></td>
                                        <td class="text-center">
                                            <div class="dropdown">
												<button class="dropdown-toggle" id="view_button" type="button" data-toggle="dropdown"> <i class="fa fa-cog"></i>
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo $salesPanelFile; ?>?id=<?php echo $saleRow['ID']; ?>" target="_blank"> <i class="fa fa-pencil"></i> Edit </a></li>
                                                    <li><a href="<?php echo $salesPanelFile; ?>?c&id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-copy"></i> Copy</a></li>
                                                    <li><a target="_blank" href="<?php echo $invoiceFile ?>?id=<?php echo $saleRow['ID']; ?>"> <i class="fa fa-print"></i> Invoice</a></li>
                                                    <li><a target="_blank" href="voucher-print.php?id=<?php echo $saleRow['VOUCHER_ID']; ?>" title="Print"> <i class="fa fa-print"></i> Voucher</a></li>
                                                    <?php if (in_array('delete-sales', $permissionz) || $admin == true) { ?>
                                                    <li><a href="#" do="<?php echo $saleRow['ID']; ?>" class="delete_record" title="Delete"> <i class="fa fa-times"></i> Delete</a></li>
                                                    <?php } ?>
                                                    <?php if (in_array('sale-returns', $permissionz) || $admin == true) { ?>
                                                    <li><a href="dist-sale-return-by-bill.php?sid=<?php echo $saleRow['ID']; ?>" title="Return" > <i class="fa fa-reply"></i> Return</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </td>
									</tr>
												<?php
												$counter++;
    }
} else {
?>
											<tr>
												<th colspan="100" style="text-align:center;">
													<?php echo (isset($_POST['searchInventory'])) ? "0 Records Found!!" : "0 Records!!" ?>
												</th>

											</tr>
											<?php
}
?>
									</tbody>
									<tfoot>
										<tr>
											<th class="text-right  p-5 bold-text" colspan="6">Total</th>
											<th class="text-center p-5 bold-text"><?=number_format($table_quantity_total,2);?></th>
											<th class="text-center p-5 bold-text"><?=number_format($table_amount_total,2);?></th>
											<th class="text-center p-5 bold-text"><?=number_format($table_ramount_total,2);?></th>
											<th class="text-center p-5 bold-text"><?=number_format($table_total_amount_total,2);?></th>
											<th class="text-center p-5 bold-text" colspan="2">- - -</th>
											<th class="text-center p-5 bold-text recpt_table_total"><?=number_format($table_amount_recpt_total,2)?></th>
											<th class="text-center p-5 bold-text"></th>
										</tr>
									</tfoot>
								</table>
								<div class="col-xs-12 text-center">
									<?php
if ($found_records > $total) {
    $get_url = "";
    foreach ($_GET as $key => $value) {
        $get_url.= ($key == 'page') ? "" : "&" . $key . "=" . $value;
    }
?>
										<nav>
											<ul class="pagination">
												<?php
    $count = $found_records;
    $total_pages = ceil($count / $total);
    $i = 1;
    $thisFileName = basename($_SERVER['PHP_SELF']);
    if (isset($this_page) && $this_page > 0) {
?>
													<li>
														<?php echo "<a href=" . $thisFileName . "?" . $get_url . "&page=1>First</a>"; ?>
													</li>
													<?php
    }
    if (isset($this_page) && $this_page >= 1) {
        $prev = $this_page;
?>
													<li>
														<?php echo "<a href=" . $thisFileName . "?" . $get_url . "&page=" . $prev . ">Prev</a>"; ?>
													</li>
													<?php
    }
    $this_page_act = $this_page;
    $this_page_act++;
    while ($total_pages >= $i) {
        $left = $this_page_act - 5;
        $right = $this_page_act + 5;
        if ($left <= $i && $i <= $right) {
            $current_page = ($i == $this_page_act) ? "active" : "";
?>
														<li class="<?php echo $current_page; ?>">
															<?php echo "<a href=" . $thisFileName . "?" . $get_url . "&page=" . $i . ">" . $i . "</a>"; ?>
														</li>
														<?php
        }
        $i++;
    }
    $this_page++;
    if (isset($this_page) && $this_page < $total_pages) {
        $next = $this_page;
?>
													<li><?php echo "<a href=" . $thisFileName . "?" . $get_url . "&page=" . ++$next . ">Next</a>"; ?></li>
													<?php
    }
    if (isset($this_page) && $this_page < $total_pages) {
?>
													<li>
														<?php echo "<a href=" . $thisFileName . "?" . $get_url . "&page=" . $total_pages . ">Last</a>"; ?>
													</li>
												</ul>
											</nav>
											<?php
    }
}
?>
								</div>
							</div> <!--End bodyTab1-->
							<div style="height:0px;clear:both"></div>
							<div id="bodyTab2" style="display:<?php echo (isset($_GET['tab']) && $_GET['tab'] == 'search') ? "block" : "none"; ?>;" >
								<div id="form">
									<form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
										<div class="caption"></div>
										<div class="message red"><?php echo (isset($message)) ? $message : ""; ?></div>
										<div class="clear"></div>

										<div class="caption">From Date </div>
										<div class="field">
											<input type="text" class="form-control datepicker" name="fromDate" style="width:150px;" />
										</div>
										<div class="clear"></div>

										<div class="caption">To Date </div>
										<div class="field">
											<input type="text" class="form-control datepicker" name="toDate" style="width:150px;" value="<?php echo date('d-m-Y'); ?>" />
										</div>
										<div class="clear"></div>
<?php
if ($salesman_specific == 'Y') {
    $sales_man_arr = array();
    $users_list = $objAccounts->getActiveList();
    if (mysql_num_rows($users_list)) {
        while ($user = mysql_fetch_assoc($users_list)) {
            if ($user['DESIGNATION_TYPE'] == '') {
                //$user['DESIGNATION_TYPE'] = 'S';
                continue;
            }
            if (!isset($sales_man_arr[$user['DESIGNATION_TYPE']])) {
                $sales_man_arr[$user['DESIGNATION_TYPE']] = array();
            }
            $sales_man_arr[$user['DESIGNATION_TYPE']][] = $user;
        }
    }
?>
										<div class="caption">OrderTaker</div>
										<div class="field" style="width:270px;position:relative;">
											<select class="order_taker" name="order_taker">
												<option value=""></option>
												<?php
    if (isset($sales_man_arr['O'])) {
        foreach ($sales_man_arr['O'] as $key => $user) {
?>
															<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME'] . " " . $user['LAST_NAME'] . "(" . $user['USER_NAME'] . ")"; ?></option>
															<?php
        }
    }
?>
											</select>
										</div>
										<div class="clear"></div>


										<div class="caption">Salesman</div>
										<div class="field" style="width:270px;position:relative;">
											<select class="user_id" name="salesman">
												<option value=""></option>
												<?php
    if (isset($sales_man_arr['S'])) {
        foreach ($sales_man_arr['S'] as $key => $user) {
?>
															<option value="<?php echo $user['ID']; ?>"><?php echo $user['FIRST_NAME'] . " " . $user['LAST_NAME'] . "(" . $user['USER_NAME'] . ")"; ?></option>
															<?php
        }
    }
?>
											</select>
										</div>
										<div class="clear"></div>

										<div class="caption">Customer</div>
										<div class="field">
											<select class="selectpicker form-control"
											name='supplierAccCode'
											data-style="btn-default"
											data-live-search="true" style="border:none">
											<option selected value=""></option>
											<?php
    if (mysql_num_rows($cashAccounts)) {
        while ($cash_ina_hand = mysql_fetch_array($cashAccounts)) {
?>
													<option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
													<?php
        }
    }
?>
											<?php
    if (mysql_num_rows($customersList)) {
        while ($account = mysql_fetch_array($customersList)) {
?>
													<option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
													<?php
        }
    }
?>
										</select>
									</div>
									<div class="clear"></div>
<?php
}
?>

									<div class="caption">PO No</div>
									<div class="field">
										<input type="text" value="" name="po_number" class="form-control" />
									</div>
									<div class="clear"></div>

									<div class="caption">Bill No</div>
									<div class="field">
										<input type="text" value="" name="billNum" class="form-control" />
									</div>
									<div class="clear"></div>

									<div class="caption"></div>
									<div class="field">
										<input type="submit" value="Search" name="search" class="button"/>
									</div>
									<div class="clear"></div>
								</form>
							</div><!--form-->
						</div> <!-- End bodyTab2 -->
					</div> <!-- End .content-box-content -->
				</div> <!-- End .content-box -->
			</div><!--body-wrapper-->
			<div id="xfade"></div>
			<!-- Modal -->
			<div id="account-ledger-modal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Account Ledger</h4>
					</div>
					<div class="modal-body">
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					</div>
				</div>
			</div>
		</body>
		</html>
		<?php include ('conn.close.php'); ?>
		<script>
		$(document).ready(function() {
			$(".shownCode,.loader").hide();
			$("input.supplierTitle").keyup(function(){
				$(this).fetchSupplierCodeToSpan(".supplierAccCode",".shownCode",".loader");
			});
			$(window).keydown(function(e){
				if(e.keyCode==113){
					e.preventDefault();
					window.location.href = "<?php echo "inventory-details.php"; ?>";
				}
			});
			<?php if (isset($_GET['tab']) && $_GET['tab'] == 'search') { ?>
				$("#bodyTab2 .dropdown-toggle").focus();
				$("#bodyTab2 billNum").setFocusTo("#bodyTab2 input[type='submit']");
				$("#bodyTab2 select").change(function(){
					$("#bodyTab2 input[name='billNum']").focus();
				});
				<?php
} ?>
			});
			$(function(){
				$("#fromDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
				$("#toDatepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					showAnim : 'show',
					changeMonth: true,
					changeYear: true,
					yearRange: '2000:+10'
				});
			});
			<?php echo (isset($_GET['tab']) && $_GET['tab'] == 'search') ? "tab('2', '1', '2');" : ""; ?>

			</script>