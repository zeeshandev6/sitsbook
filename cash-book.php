<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/cash-book.php');

	$objCashBook 	 = new cashbook;
	$objAccountCodes = new ChartOfAccounts;

	//Permission
	if(!in_array('cash-book',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$book_dayz			 = $objCashBook->get_dayz_of_cash();
	if(isset($_POST['search'])){
		$fromDate = ($_POST['toDate']!=="")?date('Y-m-d',strtotime($_POST['toDate'])):"";
		$toDate = ($_POST['toDate']!=="")?date('Y-m-d',strtotime($_POST['toDate'])):"";
		if($fromDate==''){
			$fromDate = date('Y-m-01');
		}
		$cashPurchases       = $objCashBook->getCashPurchases($toDate);
		$cashSales       	   = $objCashBook->getCashSales($toDate);
		$cashReceipts        = $objCashBook->getCashReceipts($toDate);
		$cashPayments        = $objCashBook->getCashPayments($toDate);
		$cashBookAccCodes = array();
		$cashBookAccCodes[]= '010101';
		$openingBalance   = $objCashBook->getOpeningBalanceSum($toDate,$cashBookAccCodes);
		$openingBalance   = round($openingBalance,2);
		$openingBalanceType = ($openingBalance<0)?"Cr":"Dr";
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
      	<!-- jQuery -->
      	<script src="resource/scripts/jquery.1.11.min.js" type="text/javascript"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      	<script src="resource/scripts/tab.js" type="text/javascript"></script>
      	<script src="resource/scripts/configuration.js" type="text/javascript"></script>
        <script src="resource/scripts/sideBarFunctions.js" type="text/javascript"></script>
        <script src="resource/scripts/printThis.js" type="text/javascript"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
        		$("select").selectpicker();
        	});
        </script>
   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >Cash Book Management</p>
                            <span id = "tabPanel">
                            	<div class = "tabPanel">
                        			<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">Cash Book</div>
                        			<div class="tab" id="tab2" onClick="tab('2', '1', '2');">History</div>
                                </div>
                            </span>
                            <div class="clear"></div>
                  		</div><!-- End .content-box-header -->

                    	<div id="bodyTab1">
                            <div class="clear"></div>
                            <div id="form">
                                <form method="post" action="">
                                    <div class="caption">Report Date:</div>
                                    <div class="field" style="width:100px">
                                        <input type="text" name="toDate" value="<?php echo isset($toDate)?date('d-m-Y',strtotime($toDate)):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px" />
                                    </div>
                                    <div class="caption" style="width: 150px;"><input type="submit" value="Generate" name="search" class="enter_button"/></div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->
                		<div id="bodyTab2" style="display:none;">
                            <div class="clear"></div>
                            <div id="form">
                                <form method="post" action="">
                                    <div class="caption">Report Date:</div>
                                    <div class="field" style="width:145px">
                                    	<select name="toDate" class="form-control">
                                    		<option></option>
<?php
									foreach ($book_dayz as $key => $day){
?>
											<option value="<?php echo $day ?>"><?php echo $day ?></option>
<?php
									}
?>
                                    	</select>
                                    </div>
                                    <div class="caption" style="width: 105px;"><input type="submit" value="Generate" name="search" class="enter_button"/></div>
                                    <div class="clear"></div>
                                </form>
                            </div><!--form-->
                  	  		<div class="clear"></div>
                		</div> <!-- End bodyTab1 -->
            		</div> <!-- End .content-box-content -->
<?php
					if(isset($_POST['search'])){
?>
        			<div class="content-box-content	">
                    	<span style="float:right;"><button class="button printThis">Print</button></span>
                		<div id="bodyTab" class="printTable" style="width: 99%;margin: 0 auto;">
                            <div style="text-align:left;">

                                <p style="font-size:18px;"> <?php echo $companyTitle; ?></p>
                                <p>
                                    <span style="color:#036;font-weight: bold;" >
                                        Cash Book Summary  As On <?php echo date('d-m-Y',strtotime($toDate)); ?>
                                     </span>
                                     <span class="repoDate" style=" float:right;font-size:14px">
                                        Report generated on : <?php echo date('d-m-Y'); ?>
                                     </span>
                                 </p>
                            </div>
                            <div style="clear:both; height:1;"></div>
                            <table class="prom tableBreak" style="margin:0; width:100%;" cellspacing="0" align="center">
	                    	<thead class="tHeader">
	                    		<tr>
	                        		<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Cash Receipts</b></td>
	                        	</tr>
	                            <tr>
	                               <th width="10%" class="bg-color-x">Voucher#</th>
	                               <th width="10%" class="bg-color-x">A/c Code</th>
	                               <th width="10%" class="bg-color-x">A/c Title</th>
	                               <th width="40%" class="bg-color-x">Description</th>
	                               <th width="20%" class="bg-color-x" style="text-align:center">Amount</th>
	                            </tr>
	                        </thead>
	                        <tbody>
<?php
				$totalAmountReceived = 0;
				if(mysql_num_rows($cashSales)){
					while($row = mysql_fetch_array($cashSales)){
						$row['ACCOUNT_CODE']  = '0201010001';
						$row['ACCOUNT_TITLE'] = $objAccountCodes->getAccountTitleByCode($row['ACCOUNT_CODE']);

?>
	                            <tr>
	                               <td  style="padding-left:5px;">JV # <?php echo $row['VOUCHER_NO']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['ACCOUNT_CODE']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['ACCOUNT_TITLE']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['NARRATION']; ?></td>
	                               <td class=" digits" style="text-align:right;"><?php echo number_format($row['AMOUNT'],2); ?></td>
	                            </tr>
	<?php
								$totalAmountReceived += $row['AMOUNT'];
					}
				}
				if(mysql_num_rows($cashReceipts)){
					while($row = mysql_fetch_array($cashReceipts)){
?>
	                            <tr>
	                               <td  style="padding-left:5px;">JV # <?php echo $row['VOUCHER_NO']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['ACCOUNT_CODE']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['ACCOUNT_TITLE']; ?></td>
	                               <td  style="text-align:left"><?php echo $row['NARRATION']; ?></td>
	                               <td class=" digits" style="text-align:right;"><?php echo number_format($row['AMOUNT'],2); ?></td>
	                            </tr>
	<?php
								$totalAmountReceived += $row['AMOUNT'];
					}
				}// end of if cash receiptside num rows
	?>
								</tr>
	                                <td  colspan="4"><div style="float: right;">Total Receipts:</div></td>
	                                <td class=" digits" style="text-align:right; font-weight:bold;">
										<?php echo (isset($totalAmountReceived))?number_format($totalAmountReceived,2):'0'; ?> DR
	                                </td>
								</tr>
								<tr>
	                        		<td colspan="5" style="font-size:14px;text-align:left;border:none;"><b>Cash Payments</b></td>
	                        	</tr>
<?php
				$totalAmountPaid = 0;
				if(mysql_num_rows($cashPurchases)){
					while($voucher_detail = mysql_fetch_array($cashPurchases)){
						$voucher_detail['ACCOUNT_CODE']  = '0101060001';
						$voucher_detail['ACCOUNT_TITLE'] = $objAccountCodes->getAccountTitleByCode($voucher_detail['ACCOUNT_CODE']);
?>
	                            <tr class="lol">
	                               <td  style="padding-left:5px;">JV # <?php echo $voucher_detail['VOUCHER_NO']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_CODE']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_TITLE']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['NARRATION']; ?></td>
	                               <td class="digits" style="text-align:right;"><?php echo number_format($voucher_detail['AMOUNT'],2); ?></td>
	                            </tr>
<?php
										$totalAmountPaid += $voucher_detail['AMOUNT'];
					}
				}
				if(mysql_num_rows($cashPayments)){
					while($voucher_detail = mysql_fetch_array($cashPayments)){
?>
	                            <tr class="lol">
	                               <td  style="padding-left:5px;">JV # <?php echo $voucher_detail['VOUCHER_NO']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_CODE']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['ACCOUNT_TITLE']; ?></td>
	                               <td  style="text-align:left"><?php echo $voucher_detail['NARRATION']; ?></td>
	                               <td class="digits" style="text-align:right;"><?php echo number_format($voucher_detail['AMOUNT'],2); ?></td>
	                            </tr>
<?php
										$totalAmountPaid += $voucher_detail['AMOUNT'];
					}
				}
?>
								</tr>
	                                <td  colspan="4"><div style="float: right;">Total Payments:</div></td>
	                                <td class=" digits" style="text-align:right; font-weight:bold;">
										<?php echo (isset($totalAmountPaid))?number_format($totalAmountPaid,2):'0.00'; ?> CR
	                                </td>
								</tr>
	                            </tbody>
								<tfoot class="tableFooter">
	<?php
								$openingBalanceType = ($openingBalance<0)?"CR":"DR";
								$closingBalance     = ($openingBalance+$totalAmountReceived) - $totalAmountPaid;

								$closingBalanceType = ($closingBalance<0)?"CR":"DR";
								$closingBalance     = str_replace('-','',$closingBalance);
	?>
	                            </tr>
	                                <td  colspan="4"><div  class="cashBookTotals">Opening Balance in Rs. </div></td>
	                                <td class=" digits" style="text-align:right"><?php echo number_format(str_replace('-','',$openingBalance),2)." ".$openingBalanceType; ?></td>
								</tr>
	                            </tr>
	                                <td  colspan="4"><div  class="cashBookTotals">Total Cash Receipt in Rs. </div></td>
	                                <td class=" digits" style="text-align:right">
										<?php echo (isset($totalAmountReceived))?number_format($totalAmountReceived,2)." DR":"0.00"; ?>
	                                </td>
								</tr>
	                            </tr>
	                                <td  colspan="4"><div  class="cashBookTotals">Total Cash Payment in Rs. </div></td>
	                                <td class=" digits" style="text-align:right">
										<?php echo (isset($totalAmountPaid))?number_format($totalAmountPaid,2)." CR":"0.00"; ?>
	                                </td>
								</tr>
	                            </tr>
	                                <td  colspan="4"><div  class="cashBookTotals">Total Cash on Hand in Rs. </div></td>
	                                <td class=" digits" style="text-align:right"><b><?php echo (isset($closingBalance))?number_format($closingBalance,2)." ".$closingBalanceType:"0.00"; ?></b></td>
								</tr>
	                        </tfoot>

	                    </table>
                            <div style="clear:both; height:20px"></div>
                        </div><!--bodyTab2-->
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
<?php
					}   //if post search
?>
         	</div><!--bodyWrapper-->
		</div><!--body-wrapper-->
   </body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$(".printThis").click(function(){
			var pageHeaderHeight = $(".pageHeader").height();
			var MaxHeight = 800;
			var RunningHeight = 0;
			var PageNo = 1;
			var MaxHeight_after = 0;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if(PageNo == 1){
					MaxHeight = 750;
				}else{
					MaxHeight = 800;
				}
				if (RunningHeight + $(this).height() > MaxHeight){
					RunningHeight = 0;
					PageNo += 1;
				}
				if($(this).height() > 29){

				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate    = $(".repoDate").text();
			//remove previous thead/tfoot
			$(".tHeader").remove();
			$(".repoDate").remove();
			$(".tableFooter").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				if(i == 1){
					MaxHeight = 750;
				}else{
					MaxHeight = 800;
				}
				$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}

			$(".newTable").last().append(tableFooter);

			$('table.tableBreak').remove();
			$(".printTable").printThis({
			  debug: false,
			  importCSS: false,
			  printContainer: true,
			  loadCSS: 'resource/css/reports.css',
			  pageTitle: "DN Enterprise",
			  removeInline: false,
			  printDelay: 500,
			  header: null
		  });
		});


		$.fn.stDigits = function(){
			return this.each(function(){
				$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			})
		};
		$("tr").each(function(index, element) {
            $(this).find("td.digits").stDigits();
        });
		$('td.digits').css({'padding-right':'4px'});
    });
</script>
