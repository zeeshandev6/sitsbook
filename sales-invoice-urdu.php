<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/sale.php');
	include('common/classes/sale_details.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/services.php');
	include('common/classes/j-voucher.php');
	include('common/classes/company_details.php');

	$objChartOfAccounts    = new ChartOfAccounts();
	$objCompanyDetails 	   = new CompanyDetails();
	$objSales              = new Sale();
	$objSaleDetails        = new SaleDetails();
	$objSaleReturnDetails  = new SaleReturnDetails();
	$objCustomers          = new Customers();
	$objItems  	           = new Items();
	$objServices           = new Services();
	$objJournalVoucher     = new JournalVoucher();
	$objConfigs   	       = new Configs();

	$locale 		 			 		 = parse_ini_file('common/locale/urdu/invoice.ini');
	$urdu_month_names 		 = parse_ini_file('common/settings/months.ini');

	//INVOICE_FORMAT Config Size - Header - Duplicate
	$use_cartons           = $objConfigs->get_config('USE_CARTONS');
	$use_cartons           = ($use_cartons == 'Y')?true:false;
	$invoice_format        = $objConfigs->get_config('INVOICE_FORMAT');
	$show_header 		   		 = $objConfigs->get_config('SHOW_INVOICE_HEADER');
	$notes_enclosures 	   = $objConfigs->get_config('USE_NOTES_ENCLOSURES');

	$invoice_format        = explode('_', $invoice_format);
	$invoice_num 		   		 = $invoice_format[1];
	$invoiceStyleCss       = 'resource/css/invoiceStyleUrdu.css';

	$individual_discount = $objConfigs->get_config('SHOW_DISCOUNT');
	$individual_discount = ($individual_discount=='Y')?true:false;

	$use_taxes           = $objConfigs->get_config('SHOW_TAX');
	$use_taxes           = ($use_taxes=='Y')?true:false;

	$cutomer_balance_array 						= array();
	$cutomer_balance_array['BALANCE'] = 0;
	$cutomer_balance_array['TYPE']    = '';

	$merge_item_by_category 	= false;
	if($fabricProcessAddon=='Y'&&!isset($_GET['office'])){
		$merge_item_by_category = true;
	}

	if(isset($_GET['id'])){
		$sale_id 		     		= mysql_real_escape_string($_GET['id']);
		$saleDetails 	     	= mysql_fetch_array($objSales->getRecordDetails($sale_id));
		if($saleDetails['COMPANY_ID']>0){
			$companyLogo     	= $objCompanyDetails->getLogo($saleDetails['COMPANY_ID']);
			$company  		 		= $objCompanyDetails->getRecordDetails($saleDetails['COMPANY_ID']);
		}else{
			$companyLogo   	 	= $objCompanyDetails->getLogo();
			$company  		 		= $objCompanyDetails->getActiveProfile();
			$objSales->insertCompanyId($sale_id,$company['ID']);
		}
		if($merge_item_by_category){
			$saleDetailList        = $objSaleDetails->getListMergeByCategory($sale_id);
		}else{
			$saleDetailList        = $objSaleDetails->getList($sale_id);
		}

		if(substr($saleDetails['CUST_ACC_CODE'],0,6) != '010101'){
			$customer_balance      = $objJournalVoucher->getOpeningBalanceOfVoucher($saleDetails['CUST_ACC_CODE'],$saleDetails['VOUCHER_ID'],$saleDetails['SALE_DATE']);
			$cutomer_balance_array = $objJournalVoucher->getBalanceType($saleDetails['CUST_ACC_CODE'], $customer_balance);
		}

		$replaced_items = $objSaleReturnDetails->getReturnReplaceItems($sale_id);
	}
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".printThis").click(function(){
			$(".printThisDiv").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: "<?php echo $invoiceStyleCss; ?>",
				pageTitle: "Software Power By SIT Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$(".ledger_view").click(function(){
			var gl_date = $(this).attr('data-date');
			var gl_code = $(this).attr('data-code');
			if(gl_code != ''){
				$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
					if(data == ''){
						var win = window.open('invoice-ledger-print.php','_blank');
						if(win){
							win.focus();
						}else{
							displayMessage('Please allow popups for This Site');
						}
					}
				});
			}
		});
		// $(".printThis").click();
	});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		if(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $saleDetails['CUSTOMER_NAME'];
		}elseif(substr($saleDetails['CUST_ACC_CODE'], 0,6) == '010102'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}else{
			$customer 	   = $objCustomers->getCustomer($saleDetails['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($saleDetails['SALE_DATE']));
		$month 				 = $urdu_month_names[date('m',strtotime($sale_date))];
		$urdu_date 	   = date('Y-'.$month.'-d',strtotime($sale_date));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$bill_discount = $saleDetails['SALE_DISCOUNT'];
?>
	<div class="col-xs-12 mt-20" >
		<div class="invoiceBody invoiceReady">
			<div class="header">
				<div class="headerWrapper">
						<button class="btn btn-default btn-sm printThis pull-left" title="Print"> <i class="fa fa-print"></i> Print View </button>
				</div><!--headerWrapper-->
			</div><!--header-->
			<div class="clear"></div>
			<div class="col-xs-12 mt-20"></div>
			<div class="printThisDiv" style="position: relative;height:9.7in;">
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint" style="height:10.6in !important;">
						<div class="invoiceHead" style="width: 100%;margin: 0px auto;">
							<div class="<?php echo $show_header=='N'?"opacity_zero":""; ?>">
								<?php
								if($companyLogo != ''){
									?>
									<img class="invLogo pull-right" src="uploads/logo/<?php echo $companyLogo; ?>" />
									<?php
								}
								?>
								<div class="partyTitle pull-right mr-20 mt-20" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
									<div class="pull-right">  <span class="font-size-26 urdu-font"><?php echo $company['URDU_NAME']; ?></span> &nbsp;&nbsp;&nbsp; <span class="font-size-26"><?php echo $company['NAME']; ?></span> </div>
									<div class="clear"></div>
									<div class="pull-right">  <span class="font-size-16 urdu-font"><?php echo $company['URDU_ADDRESS']; ?></span> </div>
									<div class="clear"></div>
									<div class="pull-right">  <span class="font-size-16 urdu-font"> <?php echo $locale['PHONE_NUMBER']; ?> <?php echo $company['CONTACT']; ?></span> </div>
									<br />
								</div>
								<div class="invoice-title pull-left font-size-26 mt-30 urdu-font ">
									<?php echo $locale['SALE']." ".$locale['INVOICE']; ?>
								</div>
							</div>
							<div class="clear"></div>
							<hr />
							<div class="clear" style="height: 5px;"></div>

							<?php
							if($invoice_num == '1'){
								?>
								<div class="infoPanel pull-right" style="height:auto !important;">
									<span class="pull-right urdu-font font-size-16" style="padding:0px 0px;"> <?php echo $locale['TO_CUSTOMER'].":"; ?> <?php echo $customer['URDU_TITLE']; ?></span>
									<div class="clear"></div>
									<span class="pull-right" style="padding:0px 0px;font-size:14px;"><?php echo isset($customer['ADDRESS'])?$customer['ADDRESS']:""; ?></span>
									<div class="clear"></div>
									<span class="pull-right" style="padding:0px 0px;font-size:14px;"><?php echo isset($customer['CITY'])?$customer['CITY']:""; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="infoPanel pull-left" style="width:200px;height:auto !important;">
									<span class="pull-right urdu-font font-size-16"> <?php echo $locale['BILL_NO']." : "; ?> <?php echo $saleDetails['BILL_NO']; ?></span>
									<div class="clear"></div>
									<span class="pull-right urdu-font font-size-16"><?php echo $locale['DATE']." : "; ?> <?php echo $urdu_date; ?></span>
									<div class="clear"></div>
								</div><!--partyTitle-->
								<div class="clear"></div>
								<?php
							}
							?>
								<div class="clear"></div>
							</div><!--invoiceHead-->
							<div class="clear"></div>
							<div class="invoiceBody" style="width: 100%;margin: 0px auto;">
								<table>
									<thead>
										<tr>
											<th width="5%">#</th>
											<th width="20%" class="urdu-font text-center font-size-16"><?php echo $locale['DETAILS']; ?></th>
											<?php if($use_cartons){ ?>
												<th width="5%" class="urdu-font text-center font-size-16"><?php echo ($fabricProcessAddon=='Y')?$locale['THAAN']:$locale['CARTONS']; ?></th>
													<?php if($fabricProcessAddon!='Y'){ ?>
												<th width="5%" class="urdu-font text-center font-size-16"><?php echo $locale['PER_CARTON']; ?></th>
													<?php } ?>
												<?php } ?>
												<th width="10%" class="urdu-font text-center font-size-16"><?php echo ($fabricProcessAddon=='Y')?$locale['METERS']:$locale['QUANTITY']; ?></th>
												<th width="10%" class="urdu-font text-center font-size-16"><?php echo $locale['RATE']; ?></th>
												<?php if($individual_discount){ ?>
													<th width="10%" class="urdu-font text-center font-size-16"><?php echo $locale['DISCOUNT']; ?></th>
													<?php } ?>
													<?php if($use_taxes){ ?>
														<th width="10%" class="urdu-font text-center font-size-16"><?php echo $locale['TAX']." ".$locale['RATE']; ?></th>
														<?php } ?>
														<th width="15%" class="urdu-font text-center font-size-16"><?php echo $locale['AMOUNT']; ?></th>
													</tr>
												</thead>
												<tbody>
<?php
													$total_cartons = 0;
													$quantity      = 0;
													$subAmount     = 0;
													$taxAmount     = 0;
													$totalAmount   = 0;
													$counter       = 1;
													if(mysql_num_rows($saleDetailList)){
														while($row = mysql_fetch_array($saleDetailList)){
															if($merge_item_by_category){
																$itemName 						= $row['CATEGORY_NAME'];
																$row['QUANTITY'] 			= $row['QTY'];
																$row['TOTAL_AMOUNT'] 	= $row['AMNT'];
																$row['CARTONS'] 			= $row['CARTON'];
																$row['PER_CARTON']	 	= '- - -';
																$row['UNIT_PRICE']   	= $objSaleDetails->getAverageCostByItemCategory($row['ITEM_CATG_ID'],$row['SALE_ID']);
															}elseif($row['ITEM_ID'] != 0){
																$item_detail = $objItems->getRecordDetails($row['ITEM_ID']);
																$itemName    = $item_detail['URDU_NAME'];
															}else{
																$itemName = $objServices->getTitle($row['SERVICE_ID']);
															}
?>
															<tr>
																<td class="text-center font-size-16"><?php echo $counter; ?></td>
																<td class="urdu-font text-right font-size-16"><?php echo $itemName; ?></td>
																<?php if($use_cartons){ ?>
																	<td class=" text-center font-size-16"><?php echo $row['CARTONS']; ?></td>
																		<?php if($fabricProcessAddon!='Y'){ ?>
																	<td class=" text-right font-size-16"><?php echo $row['PER_CARTON']; ?></td>
																		<?php } ?>
																	<?php } ?>
																	<td class="text-center font-size-16"><?php echo ($row['SERVICE_ID'] > 0 && $row['QUANTITY'] == 0)?"1":$row['QUANTITY']; ?></td>
																	<td class="text-center font-size-16"><?php echo number_format($row['UNIT_PRICE'],2); ?></td>
																	<?php if($individual_discount){ ?>
																		<td class="text-center font-size-16"><?php echo $row['SALE_DISCOUNT']; ?> <?php echo ($saleDetails['DISCOUNT_TYPE'] == 'P')?"%":""; ?></td>
																		<?php } ?>
																		<?php if($use_taxes){ ?>
																			<td class="text-center font-size-16"><?php echo $row['TAX_RATE']; ?></td>
																			<?php } ?>
																			<td class="text-left font-size-16"><?php echo number_format($row['TOTAL_AMOUNT'],2); ?></td>
																		</tr>
																		<?php
																		$total_cartons += $row['CARTONS'];
																		$quantity    	 += $row['QUANTITY'];
																		$subAmount 	 	 += $row['QUANTITY']*$row['UNIT_PRICE'];
																		$taxAmount   	 += $row['TAX_AMOUNT'];
																		$totalAmount 	 += $row['TOTAL_AMOUNT'];
																		$counter++;
																	}
																}
																$return_total = 0;
																if(mysql_num_rows($replaced_items)){
																	while($returns = mysql_fetch_assoc($replaced_items)){
																		$item_detail = $objItems->getRecordDetails($returns['ITEM_ID']);
																		$item_name   = $item_detail['URDU_NAME'];
																?>
																<tr>
																	<td class="text-center bg-danger urdu-font font-size-16"><?php echo $locale['RETURN']; ?></td>
																	<td class="text-right bg-danger  urdu-font font-size-16"><?php echo $item_name; ?></td>
																<?php if($use_cartons){ ?>
																	<td class="text-center">- - -</td>
																	<?php if($fabricProcessAddon!='Y'){ ?>
																	<td class="text-center">- - -</td>
																	<?php } ?>
																<?php } ?>
																	<td class="text-center bg-danger font-size-16"><?php echo $returns['QUANTITY']; ?></td>
																	<td class="text-center bg-danger font-size-16"><?php echo $returns['UNIT_PRICE']; ?></td>
																	<?php if($individual_discount){ ?>
																	<td class="text-center bg-danger font-size-16"><?php echo $returns['SALE_DISCOUNT']; ?></td>
																	<?php } ?>
																	<?php if($use_taxes){ ?>
																	<td class="text-center bg-danger font-size-16"><?php echo $returns['TAX_RATE']; ?></td>
																	<?php } ?>
																	<td class="text-left bg-danger font-size-16"><?php echo $returns['TOTAL_AMOUNT']; ?></td>
																</tr>
																<?php
																	$return_total += $returns['TOTAL_AMOUNT'];
																	$quantity     -= $returns['QUANTITY'];
																	}
																}
																?>
															</tbody>
<?php
															$colum_skipper  = 0;
															$columns        = 4;
															$colum_skipper += ($use_cartons)?1:0;
															$colum_skipper += ($individual_discount)?1:0;
															$colum_skipper += ($use_taxes)?1:0;
															$colum_skipper += ($fabricProcessAddon=='Y')?0:1;
															$columns 	   += $colum_skipper;
															$columnx 	    = $columns;

															$columnx 	   -= 2;
															$columnx       -= ($use_cartons)?2:1;
															$columnx       -= ($fabricProcessAddon!='Y')?1:0;

															$columns       -= ($fabricProcessAddon!='Y'&&!$use_cartons)?1:0;
?>
															<tfoot>
																<tr>
																	<td class="text-center urdu-font font-size-16" colspan="2"><?php echo $locale['TOTAL']; ?></td>
																	<?php if($use_cartons){ ?>
																		<td class="text-center font-size-16"><?php echo $total_cartons; ?></td>
																		<?php if($fabricProcessAddon!='Y'){ ?>
																		<td class="text-center">- - -</td>
																		<?php } ?>
																	<?php } ?>
																	<td class="text-center font-size-16"><?php echo $quantity; ?></td>
																	<td class="text-center urdu-font font-size-16" colspan="<?php echo $columnx ; ?>"><?php echo $locale['GROSS_SALE']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($subAmount,2); ?></td>
																</tr>
																<?php if(mysql_num_rows($replaced_items)){ ?>
																<tr>
																	<td class="text-left urdu-font font-size-16 no-border" colspan="<?php echo $columns; ?>"><?php echo $locale['RETURN']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($return_total,2); ?></td>
																</tr>
																<?php } ?>
																<?php $totalAmount -= $saleDetails['DISCOUNT']; ?>
																<?php if($saleDetails['SALE_DISCOUNT'] > 0){ ?>
																<tr>
																	<td class="text-left urdu-font font-size-16 no-border" colspan="<?php echo $columns; ?>"><?php echo $locale['DISCOUNT']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($saleDetails['SALE_DISCOUNT'],2); ?></td>
																</tr>
																<?php } ?>
																<?php if($taxAmount > 0){ ?>
																<tr>
																	<td class="text-left urdu-font font-size-16 no-border" colspan="<?php echo $columns; ?>"><?php echo $locale['TAX']." ".$locale['AMOUNT']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($taxAmount,2); ?></td>
																</tr>
																<?php } ?>
																<?php $totalAmount += $saleDetails['CHARGES']; ?>
																<?php if($saleDetails['CHARGES'] > 0){ ?>
																<tr>
																	<td class="text-left urdu-font font-size-16 no-border" colspan="<?php echo $columns; ?>"><?php echo $locale['CHARGES']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($saleDetails['CHARGES'],2); ?></td>
																</tr>
																<?php } ?>
																<tr class="bold_table_tr">
																	<td colspan="<?php echo $columns; ?>" class="text-left urdu-font font-size-16 no-border"><?php echo $locale['INVOICE']." ".$locale['TOTAL']; ?></td>
																	<td class="text-left font-size-16">
																			<?php
																				$totalAmount = (float)(str_ireplace('-','',$totalAmount));
																				$totalAmount -= $return_total;
																				echo number_format(($totalAmount),2)
																			 ?>
																	</td>
																</tr>
																<?php if($saleDetails['RECEIVED_CASH'] > 0){ ?>
																<tr>
																	<td class="text-left urdu-font font-size-16 no-border" colspan="<?php echo $columns; ?>"><?php echo $locale['RECEIVED']; ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($saleDetails['RECEIVED_CASH'],2); ?></td>
																</tr>
																<?php } ?>
																<?php if($saleDetails['CHANGE_RETURNED']>0){ ?>
																<tr class="bold_table_tr">
																	<td colspan="<?php echo $columns; ?>" class="text-left urdu-font font-size-16 no-border"><?php echo $locale['CHANGE_GIVEN']; ?></td>
																	<td class="text-left font-size-16"><?php echo $saleDetails['CHANGE_RETURNED']; ?></td>
																</tr>
																<?php } ?>
																<?php if(substr($saleDetails['CUST_ACC_CODE'],0,6) == '010104'){ ?>
																<tr>
																	<td colspan="<?php echo $columns; ?>" class="text-left urdu-font font-size-16 no-border"><?php echo $locale['PREVIOUS']." ".$locale['BALANCE'] ?></td>
																	<td class="text-left font-size-16"><?php echo number_format(($cutomer_balance_array['BALANCE']),2); ?></td>
																</tr>
																<?php
																	if($saleDetails['RECOVERED_BALANCE']=='Y'){
																		$ending_balance = $cutomer_balance_array['BALANCE'];
																		$ending_balance -= $saleDetails['RECOVERY_AMOUNT'];
																	}else{
																		$ending_balance = ($cutomer_balance_array['BALANCE']+$totalAmount) - ($saleDetails['RECEIVED_CASH']-$saleDetails['CHANGE_RETURNED']);
																	}
																?>
																<tr class="bold_table_tr">
																	<td colspan="<?php echo $columns; ?>" class="text-left urdu-font font-size-16 no-border"><?php echo $locale['BALANCE'] ?></td>
																	<td class="text-left font-size-16"><?php echo number_format($ending_balance,2) ?></td>
																</tr>
																<?php }else{
																	$cutomer_balance_array['BALANCE'] = 0;
																} ?>
															</tfoot>
														</table>
														<div class="clear"></div>

															<div class="invoice_footer <?php echo $show_header=='N'?"opacity_zero":""; ?>" style="position:absolute !important;bottom:5px !important;">
																<div class="auths pull-right">
																	Prepared By
																	<span class="authorized"></span>
																</div><!--partyTitle-->
																<div class="auths pull-left">
																	Authorized By
																</div><!--partyTitle-->
																<div class="clear" style="height: 10px;"></div>
																<div class="invoice-developer-info text-right"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
															</div>
														</div><!--invoiceBody-->
													</div><!--invoiceLeftPrint-->
													<div class="clear"></div>
												</div><!--invoiceContainer-->
											</div><!--printThisDiv-->
										</div><!--invoiceBody-->
										<?php
									}
									?>
								</div>
								</body>
								</html>
								<?php include('conn.close.php'); ?>
