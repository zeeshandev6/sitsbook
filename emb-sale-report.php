<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/emb_sale.php');
	include('common/classes/emb_sale_details.php');
	include('common/classes/machines.php');
	include('common/classes/customers.php');
	include('common/settings/captions.php');

	//Permission
	if(!in_array('sales-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     = new ChartOfAccounts();
	$objSale         		 = new EmbroiderySale();
	$objSaleDetails  		 = new EmbroiderySaleDetails();
	$objMachines         = new Machines();
	$objCustomers    		 = new Customers();
	$objConfigs 		 		 = new Configs();

	$customersList       = $objCustomers->getList();

	if(isset($_GET['search'])){
		$objSale->from_date 		= ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objSale->to_date   		= ($_GET['toDate']=='')?"":date('Y-m-d',strtotime($_GET['toDate']));
		if(isset($_GET['user_id'])){
			$objSale->user_id 			= ($_SESSION['classuseid'] == 1)?$_GET['user_id']:$_GET['user_id'];
		}else{
			$objSale->user_id 			= $_SESSION['classuseid'];
		}

		$objSale->account_code  = mysql_real_escape_string($_GET['account_code']);
		$objSale->machine_id    = mysql_real_escape_string($_GET['machine_id']);
		$objSale->lot_no				= mysql_real_escape_string($_GET['lot_no']);
		$objSale->gp_no					= mysql_real_escape_string($_GET['gp_no']);
		$objSale->design_no			= mysql_real_escape_string($_GET['design_no']);
		$objSale->bill_no				= mysql_real_escape_string($_GET['bill_no']);

		$titleRepo = '';

		if($objSale->account_code != ''){
			$supplierHeadTitle = $objAccountCodes->getAccountTitleByCode($objSale->account_code);
			$titleRepo = $supplierHeadTitle;
		}

		if($objSale->machine_id != ''){
			$theItemTitle      = $objMachines->getName($objSale->machine_id);
			$titleRepo         = $theItemTitle;
		}

		if($objSale->from_date == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objSale->from_date = $startThisMonth;
		}

		$purchaseReport = $objSale->saleReport();
	}
?>
<!DOCTYPE html>



<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
	html{
	}
	.ui-tooltip{
		font-size: 12px;
		padding: 5px;
		box-shadow: none;
		border: 1px solid #999;
	}
	.input_sized{
		float:left;
		width: 152px;
		padding-left: 5px;
		border: 1px solid #CCC;
		height:30px;
		-webkit-box-shadow:#F4F4F4 0 0 0 2px;
		border:1px solid #DDDDDD;

		border-top-right-radius: 3px;
		border-top-left-radius: 3px;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;

		-moz-border-radius-topleft:3px;
		-moz-border-radius-topright:3px;
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:5px;

		-webkit-border-top-left-radius:3px;
		-webkit-border-top-right-radius:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;

		box-shadow: 0 0 2px #eee;
		transition: box-shadow 300ms;
		-webkit-transition: box-shadow 300ms;
	}
	.input_sized:hover{
		border-color: #9ecaed;
		box-shadow: 0 0 2px #9ecaed;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
	<script type="text/javascript">
	$(window).on('load',function(){
		$(".printThis").click(function(){
			var MaxHeight = 450;
			var RunningHeight = 0;
			var PageNo = 1;
			//Sum Table Rows (tr) height Count Number Of pages
			$('table.tableBreak>tbody>tr').each(function(){
				if (RunningHeight + $(this).height() > MaxHeight){
					RunningHeight = 0;
					PageNo += 1;
				}
				RunningHeight += $(this).height();
				//store page number in attribute of tr
				$(this).attr("data-page-no", PageNo);
			});
			//Store Table thead/tfoot html to a variable
			var tableHeader = $(".tHeader").html();
			var tableFooter = $(".tableFooter").html();
			var repoDate = $(".repoDate").text();
			//remove previous thead/tfoot
			$(".tHeader").remove();
			$(".tableFooter").remove();
			$(".repoDate").remove();
			//Append .tablePage Div containing Tables with data.
			for(i = 1; i <= PageNo; i++){
				$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
				//get trs by pagenumber stored in attribute
				var rows = $('table tr[data-page-no="' + i + '"]');
				$('#Table' + i).find("thead").append(tableHeader);
				$('#Table' + i).find("tbody").append(rows);
			}
			$(".newTable").last().append(tableFooter);
			$('table.tableBreak').remove();

			$("div.tablePage").each(function(i,e){
				$(this).prepend($(".pageHeader").first().clone());
			});
			$(".pageHeader").first().remove();
			$(".printTable").printThis({
				debug: false,
				importCSS: false,
				printContainer: true,
				loadCSS: 'resource/css/reports-horizontal.css',
				pageTitle: "Sit Solution",
				removeInline: false,
				printDelay: 500,
				header: null
			});
		});
		$('select').selectpicker();
		if($(".carton_th").length){
			var colspan = $(".carton_th").prevAll().length;
		}else{
			var colspan = $(".tHeader th").eq(7).prevAll().length;
		}
		$(".total_tf").attr('colspan',colspan);
	});
	</script>
</head>

<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div class="content-box-top">
			<div class="content-box-header">
				<p>Sales Reports</p>
				<div class="clear"></div>
			</div> <!-- End .content-box-header -->

			<div class="content-box-content">
				<div id="bodyTab1">
					<div id="form">
						<form method="get" action="emb-sale-report.php">
							<div class="caption">From Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>

							<div class="caption">To Date </div>
							<div class="field" style="width:300px;">
								<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
							</div><!--field-->
							<div class="clear"></div>

							<div class="caption">Customer</div>
							<div class="field">
								<select class="account_code form-control show-tick" name="account_code"
												data-style="btn-default"
												data-live-search="true" data-hide-disabled='true' style="border:none;" >
									 <option selected value=""></option>
<?php
						if(mysql_num_rows($customersList)){
								while($account = mysql_fetch_array($customersList)){
?>
									 <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
								}
						}
						if(mysql_num_rows($cash_in_hand_list)){
								while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
										$cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
?>
										<option  data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
<?php
								}
						}
?>
								</select>
						</div>
						<div class="clear"></div>

						<div class="caption">Machines</div>
						<div class="field">
							<select class="machine_id show-tick form-control" name="machine_id" data-style="btn-default" data-live-search="true" style="border:none">
								 <option selected value=""></option>
								 <?php
								 $machine_list = $objMachines->getList();
								 $machine_arry = array();
								 if(mysql_num_rows($machine_list)){
									 while($machine = mysql_fetch_assoc($machine_list)){
										 $machine_arry[$machine['ID']] = $machine;
										 ?>
										 <option value="<?php echo $machine['ID']; ?>" data-subtext="<?php echo $machine['RATE']; ?>"><?php echo $machine['NAME']; ?></option>
										 <?php
									 }
								 }
								 ?>
							</select>
						</div>
						<div class="clear"></div>

						<div class="caption">Lot #</div>
						<div class="field" style="width: 150px;">
							<input type="text" name="lot_no" value="" class="form-control" />
						</div>
						<div class="caption">Gate Pass</div>
						<div class="field" style="width: 150px;">
							<input type="text" name="gp_no" value="" class="form-control" />
						</div>
						<div class="clear"></div>

						<div class="caption">Design</div>
						<div class="field" style="width: 150px;">
							<input type="text" name="design_no" value="" class="form-control" />
						</div>

						<div class="caption">Bill #</div>
						<div class="field" style="width: 150px;">
							<input type="text" name="bill_no" value=""  class="form-control" />
						</div>
						<div class="clear"></div>

				<div class="caption"></div>
				<div class="field">
					<input type="submit" value="Search" name="search" class="button"/>
				</div>
				<div class="clear"></div>
			</form>
		</div><!--form-->

		<?php
		if(isset($purchaseReport)){
			?>
			<div id="form">
			<span style="float:right;"><button class="button printThis">Print</button></span>
			<div class="clear"></div>
				<div id="bodyTab" class="printTable" style="margin: 0 auto;">
					<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
						<p style="text-align: left;font-size:24px;margin: 0px;padding: 0px;">
							Embroidery Sale Report
							<?php echo ($titleRepo == '')?"":" - ".$titleRepo; ?>
						</p>
						<p style="font-size:16px;text-align:left;padding: 0px;">
							<?php echo ($objSale->from_date=="")?"":"From ".date('d-m-Y',strtotime($objSale->from_date)); ?>
							<?php echo ($objSale->to_date=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objSale->to_date)); ?>
						</p>
						<p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
						<div class="clear"></div>
					</div>
					<?php
					$prevBillNo = '';
					if(mysql_num_rows($purchaseReport)){
						?>
						<table class="tableBreak">
							<thead class="tHeader">
									<tr style="background:#EEE;">
										<th width="5%" style="font-size:12px;text-align:center">Bill#</th>
										<th width="5%" style="font-size:12px;text-align:center">Lot#</th>
										<th width="5%" style="font-size:12px;text-align:center">GatePass</th>
										<th width="8%" style="font-size:12px;text-align:center">SaleDate</th>
										<th width="15%" style="font-size:12px;text-align:center">Party</th>
										<th width="15%" style="font-size:12px;text-align:center">Machine</th>
										<th width="7%" style="font-size:12px;text-align:center">Design</th>
										<th width="7%" style="font-size:12px;text-align:center">MachineRate</th>
										<th width="7%" style="font-size:12px;text-align:center">Stitches</th>
										<th width="7%" style="font-size:12px;text-align:center">StitchRate</th>
										<th width="7%" style="font-size:12px;text-align:center">Amount</th>
										<th width="7%" style="font-size:12px;text-align:center">Qty/Len</th>
										<th width="7%" style="font-size:12px;text-align:center">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$total_crtn   = 0;
									$total_qty    = 0;
									$total_price  = 0;
									$total_tax    = 0;
									$total_amount = 0;
									$total_cost   = 0;
									$total_len    = 0;

									while($detailRow = mysql_fetch_array($purchaseReport)){
										$supplierTitle = $objAccountCodes->getAccountTitleByCode($detailRow['CUST_ACC_CODE']);

										$itemName = $objMachines->getName($detailRow['MACHINE_ID']);

										$user_full_name = $objAccounts->getFullName($detailRow['USER_ID']);
										$user_full_name = $user_full_name['FIRST_NAME']." ".$user_full_name['LAST_NAME'];

										if($detailRow['BILL_NO'] !== $prevBillNo){
											$thisBillNo = $detailRow['BILL_NO'];
										}else{
											$thisBillNo = '';
										}
										?>
										<tr id="recordPanel" class="alt-row">
											<td class="text-center"><?php echo $thisBillNo; ?></td>
											<td class="text-center"><?php echo $detailRow['LOT_NO']; ?></td>
											<td class="text-center"><?php echo $detailRow['GP_NO']; ?></td>
											<td class="text-left"><?php echo date('d/m/Y',strtotime($detailRow['SALE_DATE'])); ?></td>
											<td class="text-left"><?php echo substr($supplierTitle, 0,16); ?></td>
											<td class="text-left"><?php echo $itemName; ?></td>
											<td class="text-center"><?php echo $detailRow['DESIGN_NO']; ?></td>
											<td class="text-center"><?php echo $detailRow['MACHINE_RATE']; ?></td>
											<td class="text-center"><?php echo $detailRow['STITCHES']; ?></td>
											<td class="text-center"><?php echo number_format($detailRow['UNIT_PRICE'],2); ?></td>
												<td class="text-right"><?php echo  number_format($detailRow['TOTAL_AMOUNT'],2); ?></td>
												<td class="text-center"><?php echo  number_format($detailRow['QTY_LENGTH'],2); ?></td>
												<td class="text-right"><?php echo  number_format($detailRow['FINAL_AMOUNT'],2); ?></td>
											</tr>
											<?php
											$total_qty   	  += $detailRow['STITCHES'];
											$total_len   	  += $detailRow['QTY_LENGTH'];
											$total_amount   += $detailRow['FINAL_AMOUNT'];
											$prevBillNo 		 = $detailRow['BILL_NO'];
										}
										?>

									</tbody>
									<?php
								}//end if
								if(mysql_num_rows($purchaseReport)){
									$total_price  = number_format($total_price,2);
									$total_amount = number_format($total_amount,2);
									$total_len = number_format($total_len,2);
									$columnSkip = 3;
									?>
									<tfoot class="tableFooter">
											<tr>
												<td style="text-align:right;" colspan="" class="total_tf">Total:</td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> <?php echo (isset($total_qty))?$total_qty:"0"; ?> </td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> - - - </td>
												<td class="text-center"> <?php echo (isset($total_len))?$total_len:"0"; ?> </td>
												<td class="text-right"> <?php echo (isset($total_amount))?$total_amount:"0"; ?> </td>
											</tr>
										</tfoot>
										<?php
									}
									?>
								</table>
								<div class="clear"></div>
							</div> <!--End bodyTab-->
						</div> <!--End #form-->
						<?php
					} //end if is generic report
					?>
				</div> <!-- End bodyTab1 -->
			</div> <!-- End .content-box-content -->
		</div><!--content-box-->
	</div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
if(isset($reportType)&&$reportType=='generic'){
	?>
	tab('1', '1', '2')
	<?php
}
?>
<?php
if(isset($reportType)&&$reportType=='specific'){
	?>
	tab('2', '1', '2')
	<?php
}
?>
</script>
