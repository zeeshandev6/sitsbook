<?php
	include 'common/connection.php';
  include 'common/config.php';
	include 'common/classes/emb_outward.php';
	include 'common/classes/emb_inward.php';
	include 'common/classes/machines.php';
	include 'common/classes/measure.php';
	include 'common/classes/emb_lot_register.php';
	include 'common/classes/accounts.php';

	$objOutwards          = new outward();
	$objInward            = new EmbroideryInward();
	$objMachines          = new machines();
	$objMeasures          = new Measures();
	$objChartOfAccounts   = new ChartOfAccounts();
	$objLotRegisterDetils = new EmbLotRegister();

	$machineList   = $objMachines->getList();
	$customersList = $objOutwards->getCustomerslist();

	if(isset($_POST['search'])){
		$objOutwards->fromDate        = "";
		$objOutwards->toDate          = "";
		$objOutwards->customerAccCode = mysql_real_escape_string($_POST['custAccCode']);

		if($objOutwards->fromDate == ''){
			$thisYear = date('Y');
			$firstJanuary = '01-01';
			$firstJanuaryThisYear = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
			$objOutwards->fromDate = $firstJanuaryThisYear;
		}

		$objInward->from_date     = $objOutwards->fromDate;
		$objInward->to_date       = $objOutwards->toDate;
		$objInward->account_code = $objOutwards->customerAccCode;

		if($objOutwards->customerAccCode!==''){
			$InwardReport  = $objInward->getsSpecificInwardJoined();
			$OutwardReport = $objOutwards->getSpecificOutward();
		}else{
			$message = 'All Feilds Are Required!';
		}
	}
?>
<!DOCTYPE html>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
    <script type="text/javascript">
        $(window).on('load', function () {
            $('.selectpicker').selectpicker();
			$(".printThis").click(function(){
				var MaxHeight = 1200;
				var RunningHeight = 0;
				var PageNo = 1;
				var MaxHeight_after = 0;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if(PageNo == 1){
						MaxHeight_after = 1150;
					}else{
						MaxHeight_after = 1200;
					}
					if (RunningHeight + $(this).height() > MaxHeight_after) {
						RunningHeight = 0;
						PageNo += 1;
					}
					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate    = $(".repoDate").text();
				//remove previous thead/tfoot/ReportDate
				$(".tHeader").remove();
				$(".repoDate").remove();
				$(".tableFooter").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);

				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Emque Embroidery",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
        });
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Cloth Stock Reports</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="emb-outward-report.php"><div class="tab">Generic</div></a>
                        <a href="specific-outward-report.php"><div class="tab">Specific</div></a>
                        <a href="cloth-stock-report.php"><div class="tabSelected">Cloth Register</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
							<div class="col-xs-12">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="">
                        <div class="caption"></div>
                        <div class="redColor"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>
                        <div class="caption">Customer Title</div>
                        <div class="field" style="width:295px;">
                        	<select class="selectpicker show-tick form-control" name='custAccCode' data-live-search="true">
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
?>
                               <option value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Generate" name="search" class="button" id="hide"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
							</div>
            </div> <!-- End .content-box-content -->
				<?php
                    if(isset($_POST['search'])&&!isset($message)){
                ?>
        	<div class="content-box-content">
						<div class="col-xs-12">
            	<span style="float:right;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printTable" style="width:900px;margin: 0 auto;">
                	<div style="text-align:left;margin-bottom:10px;">
                    	<p style="margin: 0px;font-weight: bold;font-size:18px;">
                        	Cloth Stock Register <?php echo $objChartOfAccounts->getAccountTitleByCode($objInward->account_code); ?>
                        </p>
                        <p style="font-weight:bold;">From <?php echo ($objOutwards->fromDate=="")?"Beginging":date('d-m-Y',strtotime($objOutwards->fromDate)); ?> to <?php echo date('d-m-Y',strtotime($objOutwards->toDate)); ?></p>
                    </div>
                    <span style="float:left;font-size:14px" class="repoDate">Report generated on:<?php echo date('d-m-Y'); ?> </span>
                  <div class="clear"></div>
                    <div style="padding:5px">
                        <table style="margin:0; width:100%" class="tableBreak">
                            <thead class="tHeader">
                                 <tr style="background:#EEE;">
                                   <th width="10%" style="font-size:14px;text-align:center">Lot#</th>
                                   <th width="10%" style="font-size:14px;text-align:center">Cloth In</th>
																	 <th width="15%" style="font-size:14px;text-align:center">Process</th>
                                   <th width="15%" style="font-size:14px;text-align:center">Complete</th>
                                   <th width="10%" style="font-size:14px;text-align:center">Cloth Out</th>
                                   <th width="10%" style="font-size:14px;text-align:center">Closing Stock</th>
                                   <th width="10%" style="font-size:14px;text-align:center">Measure</th>
                                   <th width="10%" style="font-size:14px;text-align:center">Design#</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								if(mysql_num_rows($InwardReport)){
									$clothInTotal  		 	= 0;
									$clothProcessTotal  = 0;
									$clothCenterTotal  	= 0;
									$clothOutTotal 		 	= 0;
									$stockTotal    		 	= 0;
									while($inwardRow = mysql_fetch_array($InwardReport)){
										$clothIn 			= $objInward->getInwardClothIn($inwardRow['ACCOUNT_CODE'],$inwardRow['LOT_NO']);
										$clothProcess = $objLotRegisterDetils->getUnderProcessLotLength($inwardRow['ACCOUNT_CODE'],$inwardRow['LOT_NO']);
										$clothCenter 	= $objLotRegisterDetils->getCompletedLotLength($inwardRow['ACCOUNT_CODE'],$inwardRow['LOT_NO']);
										$clothOut 		= $objOutwards->getOutwardClothOut($inwardRow['ACCOUNT_CODE'],$inwardRow['LOT_NO']);
										$designCode 	= $objOutwards->getDesignCodes($inwardRow['ACCOUNT_CODE'],$inwardRow['LOT_NO']);
?>
                                <tr id="recordPanel" class="alt-row">
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $inwardRow['LOT_NO']; ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $clothIn; ?></td>
																		<td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $clothProcess; ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $clothCenter; ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $clothOut; ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo ($clothIn-$clothOut); ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $objMeasures->getName($inwardRow['MEASURE_ID']); ?></td>
                                    <td style="text-align:center;font-size:14px !important;padding:2px !important;"><?php echo $designCode; ?></td>
                                </tr>
<?php
										$clothInTotal 		 += $clothIn;
										$clothProcessTotal += $clothProcess;
										$clothCenterTotal  += $clothCenter;
										$clothOutTotal 		 += $clothOut;
										$stockTotal 			 += ($clothIn-$clothOut)-$clothCenter;
									}
								}
?>
                            </tbody>
                            <tfoot class="tableFooter">
                                <tr>
                                    <td style="text-align:center"><b>Total</b></td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo (isset($clothInTotal))?$clothInTotal:0; ?></td>
																		<td style="text-align:center;font-weight:bold;"><?php echo (isset($clothProcessTotal))?$clothProcessTotal:0; ?></td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo (isset($clothCenterTotal))?$clothCenterTotal:0; ?></td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo (isset($clothOutTotal))?$clothOutTotal:0; ?></td>
                                    <td style="text-align:center;font-weight:bold;"><?php echo (isset($stockTotal))?$stockTotal:0; ?></td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                    <td style="text-align:center;font-weight:bold;"> - - - </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="clear;" style="height:20px"></div>
                </div> <!--End bodyTab-->
							  </div>
            </div> <!-- End .content-box-content -->
<?php
					}
?>
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script type="text/javascript">
	$(window).load(function(){
		$(".dropdown-toggle").focus();
	});
</script>
