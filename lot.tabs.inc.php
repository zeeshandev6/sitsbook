<?php
  if(isset($lot_no)){
    $lot_dtl = $objConfigs->get_lot_details_by_lot_no($lot_no);
    $active_tabs = explode(',',$lot_dtl['STAGES']);
?>
  <div class = "content-box-header tab-configure" style="margin-top: 0px !important;">
    <span id="tabPanel" class="pull-left ml-20">
      <div class="tabPanel pull-left">
        <a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tab">Lot</div></a>
        <?php if(in_array('F',$active_tabs)){ ?>
        <a href="fabric-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'fabric-contract')!==FALSE?"tabSelected":"tab"; ?>">Grey Fabric</div></a>
        <?php } ?>
        <?php if(in_array('P',$active_tabs)){ ?>
        <a href="processing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'processing-contract')!==FALSE?"tabSelected":"tab"; ?>">Dyeing</div></a>
        <?php } ?>
        <a href="packing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'packing-contract')!==FALSE?"tabSelected":"tab"; ?>">Packing</div></a>
        <?php if(in_array('E',$active_tabs)){ ?>
        <a href="embroidery-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'embroidery-contract')!==FALSE?"tabSelected":"tab"; ?>">Embroidery</div></a>
        <?php } ?>
        <a href="lot-ledger.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'ledger')!==FALSE?"tabSelected":"tab"; ?>">Ledger</div></a>
        <a href="lot-costing.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo stripos(basename($_SERVER['PHP_SELF']),'costing')!==FALSE?"tabSelected":"tab"; ?>">Costing</div></a>
      </div>
    </span>
    <div class="clear"></div>
  </div><!-- End .content-box-header -->
<?php } ?>
