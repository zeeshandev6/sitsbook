<?php
	ob_start();
	include ('msgs.php');
	include ('common/connection.php');
	include ('common/config.php');
	include ('common/classes/rental_booking.php');
	include('common/classes/services.php');
	include ('common/classes/items.php');
	include ('common/classes/itemCategory.php');

	//Permission
	if(!in_array('rental-booking',$permissionz) && $admin != true){
			echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
			echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
			echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
			echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
			echo '</div>';
			exit();
	}
	//Permission ---END--

	$objRentalBooking = new RentalBooking();
	$objServices      = new Services();
	$objItems 				= new Items();
	$objItemCategory  = new itemCategory();

	$servicesList        = $objServices->getList();
	$itemsCategoryList   = $objItemCategory->getList();

	$rbid 							 = (isset($_GET['id']))?$_GET['id']:"0";

	if($rbid>0){
		$rentalDetail 		 = $objRentalBooking->getDetail($rbid);
	}
	$current_order_no = $objRentalBooking->getListBookingOrder();
	$current_order_no = $current_order_no+1;
?>
<!DOCTYPE html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Admin Panel</title>
  <link rel="stylesheet" href="resource/css/reset.css"                         type="text/css" />
  <link rel="stylesheet" href="resource/css/style.css"                         type="text/css" />
  <link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css" />
  <link rel="stylesheet" href="resource/css/form.css"                          type="text/css" />
  <link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css" />
  <link rel="stylesheet" href="resource/css/reports.css"                       type="text/css" />
  <link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css" />
  <link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css" />
  <link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.min.css"  type="text/css" />
  <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.css"      type="text/css" />

  <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
  <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  <script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('input.datepicker_style,input.delivereddate').datetimepicker({
				format: "dd-mm-yyyy",
				weekStart: 1,
				minView: 2,
        todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
        showMeridian: 1
			});
			$("input[name=total_price],input[name=advance]").on("change keyup",function(){
				var tp = $("input[name=total_price]").val();
				var a  = $("input[name=advance]").val();
				var r  = tp - a;
				$("input[name=remaining]").val((r).toFixed(2));
			});
			$("select").selectpicker();
			setTimeout(function(){
				$("div.item_id button").attr('tabindex',7);
			},300);
			$(".addDetailRow").click(function(){
				$(this).quickSave();
			});
			$(".save_rental").click(function(){
				saveRentalBooking();
			});
			$("div button").setFocusTo(".quantity");
			$("select.item_id").change(function(){
				$(this).getItemDetails();
				$(".quantity").focus();
			});
			$(".quantity").setFocusTo(".delivereddate");
			$(".delivereddate").setFocusTo(".returndate");
			$(".returndate").setFocusTo(".addDetailRow");
			$("input.order_status").change(function(){
				if($(this).is(":checked")){
					$(".delivereddate").val("");
				}
			});
		});
  </script>
	<style>
		tr.transactions{
			height: 40px !important;
		}
	</style>
</head>
<body>
  <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
        <div id="bodyWrapper">
          <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
              <div class = "content-box-header">
                <p style="font-size:15px;"><B>Rental Booking Details</B></p>
								<span id="tabPanel">
										<div class="tabPanel">
												<a href="rental-booking-management.php?tab=list"><div class="tab">List</div></a>
												<a href="rental-booking-management.php?tab=search"><div class="tab">Search</div></a>
												<a href="rental-booking-details.php"><div class="tabSelected"><?php echo ($rbid>0)?"Detail":"New"; ?></div></a>
										</div>
								</span>
								<div class="clear"></div>
              </div><!-- End .content-box-header -->
							<div class="clear"></div>

							<div class="content-box-header">
	                <span id="tabPanel" class="pull-left ml-10">
	                    <div class="tabPanel">
	                        <a href="rental-booking-details.php?id=<?php echo $rbid; ?>" class="tabSelected"><i class="fa fa-book"></i> Booking</a>
	                        <a href="<?php echo ($rbid>0)?"rental-booking-receipts.php?rbid=".$rbid:"#"; ?>" class="tab"><i class="fa fa-random"></i> Receipts</a>
													<a href="<?php echo ($rbid>0)?"rental-booking-ledger.php?rbid=".$rbid:"#"; ?>" class="tab"><i class="fa fa-file-o"></i> Ledger</a>
	                    </div>
	                </span>
	                <div class="clear"></div>
	            </div> <!-- End .content-box-header -->
	            <div class="clear"></div>

              <?php
	              //msgs
	              if(isset($_GET['action'])){
	                if($_GET['action']=='updated')  { successMessage("Booking Detail Update Successfully.");}
	                elseif($_GET['action']=='added'){ successMessage("Booking Detail Save Successfully.");}
	                elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
	              }
              ?>
              <div id = "bodyTab1">
                <div id="form">
                    	<div class="caption">Booking Order No:</div>
                    	<div class="form-group field" style="margin-bottom:0px;width:320px;">
                      	<input readonly="readonly" type="text" class="form-control" name="booking_order_no" id="pon" value="<?php echo (isset($rentalDetail))?$rentalDetail['BOOKING_ORDER_NO']:$current_order_no; ?>">
                      </div>

											<div class="caption" style="width:190px;"> Booking Date :</div>
                      <div class="field" style="width: 180px;">
                        <input type="text" class="form-control datepicker_style" tabindex="4" name="booking_date_time" value="<?php if(isset($rentalDetail) && $rentalDetail['BOOKING_DATE_TIME'] != "0000-00-00"){ echo date('d-m-Y',strtotime($rentalDetail['BOOKING_DATE_TIME'])); }else{ echo date('d-m-Y'); } ?>" />
                      </div>
											<div class="clear"></div>

											<div class="caption">Name :</div>
                      <div class="field" style="width:150px">
                        <input class="form-control" name="clinet_name" tabindex="1" value="<?php echo (isset($rentalDetail))?$rentalDetail['CLIENT_NAME']:""; ?>" />
                      </div>

                      <div class="field ml-10" style="width:160px">
                        <input class="form-control" placeholder="Mobile No" tabindex="2" name="clinet_mobile" value="<?php echo (isset($rentalDetail))?$rentalDetail['CLIENT_MOBILE']:""; ?>" maxlength="50" placeholder="Mobile Number" />
                      </div>

											<div class="caption" style="width:190px;"> Delivery Date :</div>
                      <div class="field" style="width: 180px;">
                        <input type="text" class="form-control datepicker_style" tabindex="5" name="deliver_date_time" value="<?php if(isset($rentalDetail) && $rentalDetail['DELIVERY_DATE_TIME'] != "0000-00-00"){ echo date('d-m-Y',strtotime($rentalDetail['DELIVERY_DATE_TIME'])); }else{ echo date('d-m-Y'); } ?>" />
                      </div>
                      <div class="clear"></div>

											<div class="caption">Address :</div>
                      <div class="field" style="width:320px">
                        <textarea class="form-control" style="height: 35px;" tabindex="3" name="client_address"><?php echo (isset($rentalDetail))?$rentalDetail['ADDRESS']:""; ?></textarea>
                      </div>
											<div class="caption" style="width:190px;"> Return Date :</div>
                      <div class="field" style="width: 180px;">
                        <input type="text" class="form-control datepicker_style" tabindex="6" name="return_date_time" value="<?php if(isset($rentalDetail) && $rentalDetail['RETURNING_DATE_TIME'] != "0000-00-00"){ echo date('d-m-Y',strtotime($rentalDetail['RETURNING_DATE_TIME'])); }else{ echo ""; } ?>" />
                      </div>
                      <div class="clear"></div>

											<div class="caption"> Items :</div>
                      <div class="field" style="width: 825px;">
												<table class="prom">
	                      <thead>
	                          <tr>
	                             <th width="20%"  style="font-size:12px;font-weight:normal;text-align:center">Description</th>
	                             <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Qty</th>
															 <th width="5%" style="font-size:12px;font-weight:normal;text-align:center">Order</th>
															 <th width="5%" style="font-size:12px;font-weight:normal;text-align:center">Rent</th>
															 <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
	                          </tr>
	                      </thead>
	                      <tbody id="transactions_list">
	                          <tr class="quickSubmit" style="background:none">
	                              <td>
																		<select class="item_id show-tick form-control"
	                                          data-style="btn-default"
	                                          data-live-search="true" style="border:none">
	                                     <option selected value=""></option>
	<?php
	                              if(mysql_num_rows($itemsCategoryList)){
	                                  while($ItemCat = mysql_fetch_array($itemsCategoryList)){
									$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
	?>
										<optgroup label="<?php echo $ItemCat['NAME']; ?>">
	<?php
									if(mysql_num_rows($itemList)){
										while($theItem = mysql_fetch_array($itemList)){
											if($theItem['ACTIVE'] == 'N'){
												continue;
											}
											if($theItem['INV_TYPE'] == 'B'){
												continue;
											}
	?>
	                                                 <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
	<?php
										}
									}
	?>
										</optgroup>
	<?php
	                                  }
	                              }
	?>
	<?php
	                              if(mysql_num_rows($servicesList)){
	                                  ?><optgroup label="Services"><?php
	                                  while($srv = mysql_fetch_assoc($servicesList)){
	?>
	                                      <option data-type="S" value="<?php echo $srv['ID']; ?>"><?php echo $srv['TITLE']; ?></option>
	<?php
	                                  }
	                                  ?></optgroup><?php
	                              }
	?>
	                                  </select>
	                              </td>
	                              <td>
	                                  <input type="text" tabindex="9" class="quantity form-control text-center"/>
	                              </td>
																<td>
																	<input id="cmn-toggle-order" tabindex="11" value="Y" class="css-checkbox order_status" type="checkbox" />
																	<label for="cmn-toggle-order" class="css-label"></label>
	                              </td>
																<td>
																	<input id="cmn-toggle-onrent" tabindex="12" value="Y" class="css-checkbox rent_status" type="checkbox" />
																	<label for="cmn-toggle-onrent" class="css-label"></label>
	                              </td>
	                              <td class="text-center">
																	<input type="button" tabindex="13" class="addDetailRow" value="Enter" id="clear_filter" />
																</td>
	                          </tr>

<?php
														$rental_detail_record = $objRentalBooking->getRentalDetailListByRentalId($rbid);
														if($rental_detail_record != NULL && mysql_num_rows($rental_detail_record)){
															while($rental_detail_records = mysql_fetch_array($rental_detail_record)){

																if($rental_detail_records['ITEM_ID'] > 0){
																	$item_name = $objItems->getItemTitle($rental_detail_records['ITEM_ID']);
																	$item_id   = $rental_detail_records['ITEM_ID'];
																	$item_type = 'I';
																}else{
																	$item_name = $objServices->getTitle($rental_detail_records['SERVICE_ID']);
																	$item_id   = $rental_detail_records['SERVICE_ID'];
																	$item_type = 'S';
																}
?>
																<tr class="alt-row calculations transactions dynamix" data-row-id="<?php echo $rental_detail_records['ID'] ?>">
																	<td class="item_id" data-item-id="<?php echo $item_id; ?>" data-type="<?php echo $item_type; ?>"><?php echo $item_name; ?></td>
																	<td style="text-align:center;" class="quantity"><?php echo $rental_detail_records['QUANTITY'] ?></td>
																		<td style="text-align:center;" class="order_status" data-state="<?php echo $rental_detail_records['ORDERING_STATUS']; ?>"><?php echo $rental_detail_records['ORDERING_STATUS']=='Y'?"Yes":"- - -"; ?></td>
																		<td style="text-align:center;" class="rent_status" data-state="<?php echo $rental_detail_records['RENT_STATUS']; ?>"><?php echo $rental_detail_records['RENT_STATUS']=='Y'?"Yes":"- - -"; ?></td>
																	<td style="text-align:center;">
																		<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
																		<a class="pointer" onclick="showDeleteRowDilog(this);" title="Delete"><i class="fa fa-times"></i></a>
																	</td>
																</tr>
<?php
															}
														}
?>

	                      </tbody>

	                  		</table>
                      </div>
                      <div class="clear"></div>

											<div class="caption">Total Price :</div>
                      <div class="field" style="width:120px">
                        <input class="form-control" name="total_price" value="<?php echo isset($rentalDetail)?$rentalDetail['TOTAL_PRICE']:""; ?>" />
                      </div>

											<div class="caption">Advance :</div>
                      <div class="field" style="width:120px">
                        <input class="form-control" name="advance" value="<?php echo isset($rentalDetail)?$rentalDetail['ADVANCE']:""; ?>" maxlength="11"  />
                      </div>

											<div class="caption">Remaining :</div>
                      <div class="field" style="width:120px">
                        <input class="form-control" name="remaining" readonly value="<?php echo isset($rentalDetail)?$rentalDetail['REMAINING']:""; ?>" maxlength="11"  />
                      </div>
                      <div class="clear"></div>

											<div class="caption"> Description :</div>
                      <div class="field" style="width: 350px;">
                        <textarea style="width:725px;height:100px;" class="form-control" name="description"><?php echo (isset($rentalDetail))?$rentalDetail['DESCRIPTION']:""; ?></textarea>
                      </div>
                      <div class="clear"></div>

                      <div class="caption"></div>
                      <div class="field" style="width:350px">
												<input type="hidden" name="rbid" value="<?php echo (isset($rentalDetail))?$rentalDetail['ID']:"0"; ?>">
                        <input type="button" name="save" value="<?php echo ($rbid>0)?"Update":"Save"; ?>" class="button save_rental" />
                        <a href="rental-booking-details.php"><input type="button" value="New Form" class="button" /></a>
												<?php if(isset($rentalDetail)){ ?>
													<a target="_blank" class="button printThis" href="rental-invoice.php?id=<?php echo $rentalDetail['ID'] ?>">Print</a>
												<?php } ?>
                      </div>
                      <div class="clear"></div>
                    <!--</form>-->
                  </div><!--form-->
                </div><!--bodyTab1-->
              </div><!--summery_body-->
            </div><!--content-box-top-->
            <div style = "clear:both; height:20px"></div>
          </div><!--bodyWrapper-->
				</div>
				<div id="fade"></div>
				<div id="xfade"></div>
  </body>
  </html>
  <?php ob_end_flush(); ?>
  <?php include("conn.close.php"); ?>
