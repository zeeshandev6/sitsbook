<?php
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/customers.php');
    include ('common/classes/items.php');
    include ('common/classes/itemCategory.php');
    include ('common/classes/departments.php');
    include ('common/classes/scheduling.php');

    $objScheduling   = new Scheduling();
    $objCustomers    = new Customers();
    $objItems        = new Items;
    $objItemCategory = new itemCategory;
    $objDepartments  = new Departments;

    $formDetails = NULL;
    if(isset($_GET['id'])){
        $id = mysql_real_escape_string($_GET['id']);
        $formDetails = $objScheduling->get_details($id);
    }
    $customer_list = $objCustomers->getList();
    $items_list = array();
    $item_categories = $objItems->getCategoryListFull();
    if(mysql_num_rows($item_categories)){
        while($category = mysql_fetch_array($item_categories)){
            $itemList = $objItems->getListByCategory($category['ITEM_CATG_ID']);
            $category_name = $objItemCategory->getTitle($category['ITEM_CATG_ID']);
            if(mysql_num_rows($itemList)){
                $items_list[$category_name] = array();
                while($theItem = mysql_fetch_array($itemList)){
                    if($theItem['ACTIVE'] == 'N'){
                        continue;
                    }
                    $items_list[$category_name][$theItem['ID']] = array();
                    $items_list[$category_name][$theItem['ID']]['NAME'] = $theItem['NAME'];
                    $items_list[$category_name][$theItem['ID']]['COMPANY'] = $theItem['COMPANY'];
                }
            }
        }
    }
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select").selectpicker();
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p>Scheduling</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="scheduling-list.php"><div class="tab">List</div></a>
                                <a href="scheduling-list.php?tab=search"><div class="tab">Search</div></a>
                                <a href="scheduling.php"><div class="tabSelected">New</div></a>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Item Category updated.");}
                    elseif($_GET['action']=='added'){ successMessage("Item Category added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
                }
                ?>
                <div id="bodyTab1">
                    <form method="post" action="">
                        <input type="hidden" name="id" value="<?php echo (int)$formDetails['ID']; ?>" />
                        <div id="form">
                            <div class="caption">Customer</div>
                            <div class="field">
                                <select class="form-control show-tick" name="customer">
                                    <option value=""></option>
                                <?php
                                    if(mysql_num_rows($customer_list)){
                                        while($customer = mysql_fetch_assoc($customer_list)){
                                            $cust_selected = ($formDetails['CUSTOMER_ID'] == $customer['CUST_ID'])?"selected":"";
                                            ?>
                                                <option value="<?php echo $customer['CUST_ID']; ?>" <?php echo $cust_selected; ?> ><?php echo $customer['CUST_ACC_TITLE']; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Product</div>
                            <div class="field">
                                <select class="itemSelect show-tick form-control"
                                    data-style="btn-default"
                                    data-live-search="true" data-show-subtext="true" style="border:none" >
                                   <option value=""></option>
<?php
                                    foreach ($items_list as $category_name => $items) {
?>
                                        <optgroup label="<?php echo $category_name; ?>">
                                            <?php
                                                foreach ($items as $item_id => $item_details){
                                                    $item_selected = ($formDetails['ITEM_ID'] == $item_id)?"selected":"";
                                            ?>
                                                <option value="<?php echo $item_id; ?>" data-subtext="<?php echo $item_details['COMPANY']; ?>" <?php echo $item_selected; ?> ><?php echo $item_details['NAME']; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </optgroup>
<?php
                                    }
?>
                                </select>
                            </div>
                            <div class="caption">Product Price</div>
                            <div class="field">
                                <input type="text" name="product_amount" class="form-control" value="<?php $formDetails['PRODUCT_AMOUNT']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Start Date</div>
                            <div class="field">
                                <input type="text" name="from_date" class="form-control datepicker" value="<?php echo  ($formDetails == NULL)?date("d-m-Y"):date('d-m-Y',strtotime($formDetails['FROM_DATE'])); ?>" />
                            </div>
                            <div class="caption">Rate %</div>
                            <div class="field">
                                <input type="text" name="rate" class="form-control" value="<?php $formDetails['RATE']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">End Date</div>
                            <div class="field">
                                <input type="text" name="to_date" class="form-control datepicker" value="<?php echo  ($formDetails == NULL)?date("d-m-Y"):date('d-m-Y',strtotime($formDetails['TO_DATE'])); ?>" />
                            </div>
                            <div class="caption">Total Amount</div>
                            <div class="field">
                                <input type="text" name="total_amount" class="form-control" value="<?php $formDetails['TOTAL_PRICE']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Tenure</div>
                            <div class="field">
                                <input type="text" name="tenure" class="form-control" value="<?php $formDetails['INSTALLMENT_TENURE']; ?>" />
                            </div>
                            <div class="caption">Monthly Installment</div>
                            <div class="field">
                                <input type="text" name="installment" class="form-control" value="<?php $formDetails['MONTHLY_INSTALLMENT']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Advance</div>
                            <div class="field">
                                <input type="text" name="advance" class="form-control" value="<?php $formDetails['ADVANCE']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Remaining Amount</div>
                            <div class="field">
                                <input type="text" name="remaining_amount" class="form-control" value="<?php $formDetails['REMAINING_AMOUNT']; ?>" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption"></div>
                            <div class="field">
                                <input type="submit" name="save" value="<?php echo ($formDetails == NULL)?"Save":"Update"; ?>" class="button" />
                                <input type="button" value="New Form" class="button" onclick="window.location.href='scheduling.php'";  />
                            </div>
                            <div class="clear"></div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>

        </div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
