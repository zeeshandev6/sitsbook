<?php
	$out_buffer = ob_start();
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/accounts.php';
	include 'common/classes/linked_accounts.php';

	//Permission
	if(!in_array('linked-coa',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts 	   = new ChartOfAccounts();
	$objLinkedAccounts = new LinkedAccounts();

	if(isset($_POST['delete_id'])){
		$row_id = mysql_real_escape_string($_POST['delete_id']);
		$objLinkedAccounts->delete($row_id);
		exit();
	}

	if(isset($_POST['account_one'])){
		$account_one = mysql_real_escape_string($_POST['account_one']);
		$account_two = mysql_real_escape_string($_POST['account_two']);
		$objLinkedAccounts->account_one = $account_one;
		$objLinkedAccounts->account_two = $account_two;
		$objLinkedAccounts->save();
		exit(header('location:linked-accounts.php'));
	}

	$customers_list = $objAccounts->getAccountByCatAccCode("010104");
	$suppliers_list = $objAccounts->getAccountByCatAccCode("040101");
?>
<!DOCTYPE html>

   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" type="text/css" href="resource/css/reset.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/style.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/invalid.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/form.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/tabs.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/reports.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/scrollbar.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/font-awesome.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/bootstrap.min.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/bootstrap-select.css"   />
        <link rel="stylesheet" type="text/css" href="resource/css/lightbox.css" />
        <link rel="stylesheet" type="text/css" href="resource/css/jquery-ui/jquery-ui.min.css" />

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
      	<script type="text/javascript" src="resource/scripts/light-box.js"></script>
        <script>
			$(document).ready(function(){
				$("select").selectpicker();
            });
            var unlink_accounts = function(id){
            	id = parseInt(id)||0;
            	if(id){
            		$.post('linked-accounts.php',{delete_id:id},function(data){
            			$("tr[data-id='"+id+"']").remove();
            		});
            	}
            };
		</script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
<body>
  	<div id="body-wrapper">
    	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
  	<div id="bodyWrapper">
     	<div class="content-box-top">
        	<div class="summery_body">
           		<div class="content-box-header">
              		<p>Link Accounts</p>
              		<div class="clear"></div>
           		</div><!--End .content-box-header-->
           		<div class="clear"></div>
           		<div id="bodyTab1">
	           		<div id="form">
	           			<form method="post" action="">
		           			<label class="caption">Debtors</label>
		           			<div class="field">
		           				<select class="form-control show-tick" name="account_one" data-live-search="true" required>
		           						<option value=""></option>
	<?php
								if(mysql_num_rows($customers_list)){
									while($customer = mysql_fetch_assoc($customers_list)){
	?>
		           						<option value="<?php echo $customer['ACC_CODE'] ?>"><?php echo $customer['ACC_TITLE'] ?></option>
	<?php
									}
								}
	?>
		           				</select>
		           			</div>
		           			<div class="clear"></div>

		           			<label class="caption">Creditors</label>
		           			<div class="field">
		           				<select class="form-control show-tick" name="account_two" data-live-search="true" required>
		           						<option value=""></option>
	<?php
								if(mysql_num_rows($suppliers_list)){
									while($supplier = mysql_fetch_assoc($suppliers_list)){
	?>
		           						<option value="<?php echo $supplier['ACC_CODE'] ?>"><?php echo $supplier['ACC_TITLE'] ?></option>
	<?php
									}
								}
	?>
		           				</select>
		           			</div>
		           			<div class="clear"></div>

		           			<label class="caption"></label>
		           			<div class="field">
		           				<input type="submit" class="button" name="report" />
		           			</div>
		           			<div class="clear"></div>
		           		</form>
		           		<hr />

<?php
						$linked_accounts_list1 = $objLinkedAccounts->getList('ACCOUNT_ONE','010104');
						$linked_accounts_list2 = $objLinkedAccounts->getList('ACCOUNT_TWO','040101');
?>
		           		<div class="col-xs-12">
		           			<div class="title">Linked Accounts</div>
		           			<div class="clear"></div>
		           			<table>
		           				<thead>
		           					<tr>
		           						<th class="text-center" >Sr.</th>
		           						<th class="text-center" >Account Code</th>
		           						<th class="text-center" >Account Title</th>
		           						<th class="text-center" ><b>Action</b></th>
		           						<th class="text-center" >Account Code</th>
		           						<th class="text-center" >Account Title</th>
		           					</tr>
		           				</thead>
		           				<tbody>
		           				<?php
		           					$counter = 1;
		           					if(mysql_num_rows($linked_accounts_list1)){
		           						while($row = mysql_fetch_assoc($linked_accounts_list1)){
		           							$account_title = $objAccounts->getAccountTitleByCode($row['ACCOUNT_ONE']);
		           							$account_title2 = $objAccounts->getAccountTitleByCode($row['ACCOUNT_TWO']);
		           				?>
		           					<tr data-id="<?php echo $row['ID']; ?>">
		           						<td class="text-center"><?php echo $counter; ?></td>
		           						<td class="text-center"><?php echo $row['ACCOUNT_ONE']; ?></td>
		           						<td class="text-center"><?php echo $account_title; ?></td>
		           						<td class="text-center"><button type="button" class="pointer" title="Delete Link" onclick="unlink_accounts(<?php echo $row['ID']; ?>);"><i class="fa fa-exchange"></i></button></td>
		           						<td class="text-center"><?php echo $row['ACCOUNT_TWO']; ?></td>
		           						<td class="text-center"><?php echo $account_title2; ?></td>
		           					</tr>
		           				<?php
		           							$counter++;
		           						}
		           					}
		           				?>
		           				</tbody>
		           			</table>
		           		</div>
		           		<div class="clear"></div>
	           		</div>
	           		<div class="clear"></div>
           		</div><!--End bodyTab1-->
        	</div><!-- End summer -->
     	</div><!-- End .content-box -->
  	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
