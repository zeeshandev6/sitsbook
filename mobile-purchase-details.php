<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/mobile-purchase.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/suppliers.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/sms_templates.php');
	include('common/classes/sitsbook_sms_api.php');

	//Permission
	if(!in_array('mobile-purchase',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts     = new ChartOfAccounts();
	$objSuppliers						= new suppliers();
	$objScanPurchase   	 		= new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objItems            		= new Items();
	$objItemCategory        = new itemCategory();
	$objDepartments         = new Departments();
	$objSmsTemplates     		= new SmsTemplates();
	$objSitsbookSmsApi   		= new SitsbookSmsApi();
	$objConfigs  						= new Configs();

	$sms_config    		 = $objConfigs->get_config('SMS');

	if(isset($_POST['send_sms_by_id'])){
		if($sms_config == 'N'){
			exit();
		}
		$purchase_id 			= (int)mysql_real_escape_string($_POST['send_sms_by_id']);
		$purchase_details = $objScanPurchase->getDetail($purchase_id);

		if(substr($purchase_details['SUPP_ACC_CODE'],0,6) == '040101'){
			$mobile_number = $objSuppliers->getMobileIfActive($purchase_details['SUPP_ACC_CODE']);
		}
		if($mobile_number==''){
			echo "Error! Mobile number not available.";
			exit();
		}

		$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
		$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');

		$purchase_date 		 = date("d-m-Y",strtotime($purchase_details['SCAN_DATE']));
		$account_code      = $purchase_details['SUPP_ACC_CODE'];
		$amount 					 = $objScanPurchase->getSupplierAmountFromVoucher($purchase_id);
		$account_title 		 = $objChartOfAccounts->getAccountTitleByCode($purchase_details['SUPP_ACC_CODE']);

		$sms_template = $objSmsTemplates->get_template(5);
		$sms_body = $sms_template['SMS_BODY'];

		$sms_body = str_replace("[DATE]",$purchase_date,$sms_body);
		$sms_body = str_replace("[AMOUNT]",$amount,$sms_body);
		$sms_body = str_replace("[ACCOUNT_TITLE]",$account_title,$sms_body);
		$sms_body = str_replace("[PURCHASE_TYPE]",'A/c',$sms_body);

		$no_of_sms   = 0;
		$acc_id  	   = 0;
		$sent_status = $objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
		if(stripos($sent_status,'Successfully')!==false){
			$objScanPurchase->setSmsStatus($purchase_id,'Y');
		}
		echo $sent_status;
		exit();
	}

	$show_tax 		   	 = $objConfigs->get_config('SHOW_TAX');
	$show_discount 	   = $objConfigs->get_config('SHOW_DISCOUNT');
	$currency_type     = $objConfigs->get_config('CURRENCY_TYPE');
	$scan_type         = $objConfigs->get_config('SCAN_PURCHASE_TYPE');

	$last_barcode      = $objScanPurchaseDetails->getLastBarcode();

	$scan_bill 				 = NULL;
	$scan_bill_details = NULL;

	if(isset($_GET['id'])){
		$id = mysql_real_escape_string($_GET['id']);
		$scan_bill = $objScanPurchase->getDetail($id);
		$scan_bill_details = $objScanPurchaseDetails->getList($id);
	}
	$suppliersList = $objSuppliers->getList();

	$items_list = array();
	$item_categories = $objItems->getCategoryListFull();

	if(mysql_num_rows($item_categories)){
	    while($category = mysql_fetch_array($item_categories)){
			$itemList = $objItems->getListByCategory($category['ITEM_CATG_ID']);
			$category_name = $objItemCategory->getTitle($category['ITEM_CATG_ID']);
			if(mysql_num_rows($itemList)){
				$items_list[$category_name] = array();
				while($theItem = mysql_fetch_array($itemList)){
					if($theItem['ACTIVE'] == 'N' || $theItem['INV_TYPE'] != 'B'){
						continue;
					}
					$items_list[$category_name][$theItem['ID']] 						= array();
					$items_list[$category_name][$theItem['ID']]['NAME'] 		= $theItem['NAME'];
					$items_list[$category_name][$theItem['ID']]['COMPANY'] 	= $theItem['COMPANY'];
				}
			}
		}
	}

?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Scan Purchase</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style>
		th,td{
			text-align: center;
			font-size:  14px !important;
		}
		th{
			padding: 8px 5px !important;
		}
		.barcode_me.recorrect{
			background-color: rgba(255, 112, 0, 0.5);
			color: #FFF;
		}
		.expire_all_th{
			cursor: pointer;
		}
		.existed{
			background-color: rgba(255,0,0,0.1);
			color: #000;
			font-weight: bold;
		}
		tfoot tr td{
			border: 1px solid #E5E5E5 !important;
		}
		.caption{
			padding: 5px;
		}
		.onfocus_wider{
			transition:all 300ms;
			-webkit-transition:all 300ms;
			-moz-transition:all 300ms;
		}
		.onfocus_wider:focus{
			width: 300px !important;
		}
	</style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/scan.purchase.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
		var show_discount,show_tax;
    	$(document).ready(function(){
			show_discount = '<?php echo $show_discount; ?>';
			show_tax 	  = '<?php echo $show_tax; ?>';
    		$(window).load(function(){
    			transcationCounter();
    		});
    		$("input[name='billNum']").numericOnly();
			$("#datepicker").setFocusTo("input[name='billNum']");
			$("input[name='billNum']").setFocusTo("div.supplierSelector button");
			$("input[name='gpNum']").focus();
			$('.supplierSelector').selectpicker();
    		$('.itemSelect').selectpicker();
			$(".barcodes_table *").on('change remove',function(){
				//$("#reset_bt").show();
			});
			$("#reset_bt").click(function(){
				$("#xfade").show();
			});
			$(".reload_item").click(function(){
	            $.get('mobile-purchase-details.php',{},function(data){
	            	$("select.item_source").html($(data).find("select.item_source").html());
	                $("select.itemSelect").each(function(){
	                	var this_selected = $(this).find("option:selected").val();
	                	$(this).html($(data).find("select.item_source").html());
	                	$(this).find("option[value="+this_selected+"]").prop("selected",true);
	                	$(this).selectpicker('refresh');
	                });
	            });
	        });
	        $("input[readonly]").prop("tabindex",'-1');
	        $(".barcode_me").last().find(".purchase_td input").blur();
	        makeItCash(".trasactionType");
	        $(window).keydown(function(e){
	            if(e.altKey == true&&e.keyCode == 73){
	                e.preventDefault();
	                $("#form").click();
	                $("div.itemSelector button").click();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 65){
	                e.preventDefault();
	                $("#form").click();
	                $("div.supplierSelector button").click();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 66){
	                e.preventDefault();
	                $("#form").click();
	                $("input.barcode_input").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 67){
	                e.preventDefault();
	                $("#form").click();
	                $("input.supplier_name").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 77){
	                e.preventDefault();
	                $("#form").click();
	                $("input.customer_mobile").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 78){
	                e.preventDefault();
	                $("#form").click();
	                $("textarea.inv_notes").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 68){
	                e.preventDefault();
	                $("#form").click();
	                $("input.whole_discount").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 79){
	                e.preventDefault();
	                $("#form").click();
	                $("input.inv_charges").focus();
	                return false;
	            }
	            if(e.altKey == true&&e.keyCode == 83){
	                e.preventDefault();
	                $("#form").click();
	                save_purchase_barcodes();
	                return false;
	            }
	        });
    	});
    </script>
</head>

<body>
    <div id="body-wrapper">
    	<input type="hidden" class="discount_type" value="<?php echo $discountType; ?>" />
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    	</div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Scanning</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="mobile-purchase.php?tab=list"><div class="tab">List</div></a>
                        <a href="mobile-purchase.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                	<div class="clear"></div>
					<input type="hidden" class="last_code" value="<?php echo (int)$last_barcode; ?>" />

					<div id="form" style="float:none;margin:10px 15px;">
						<div class="myCat"></div>
                        <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Purchase Date</div>
                        <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                            <input type="text" name="rDate" id="datepicker" value="<?php echo $scan_bill['SCAN_DATE']!=''?date("d-m-Y",strtotime($scan_bill['SCAN_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:150px" />
                        </div>

                        <div class="caption" style="width:80px;">Bill#</div>
                        <div class="field" style="width:100px">
                            <input type="text" name="billNum" value="<?php echo $scan_bill['BILL_NO']; ?>" class="form-control text-center" style="width:100px" />
                        </div>
                        <div class="clear"></div>

						<div class="caption" style="width:80px;">Account</div>
                        <div class="field" style="width:270px;">
                        	<span onclick="add_supplier();" class="btn btn-default pull-right" ><i class="fa fa-plus" style="color:#06C;margin:0px;padding:0px;" ></i></span>
                            <select class="supplierSelector form-control "
                                    data-style="btn-default" data-width="230"
                                    data-live-search="true" data-hide-disabled='true' style="border:none" >
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($suppliersList)){
                            while($account = mysql_fetch_array($suppliersList)){
                            	$disabled = (substr($scan_bill['SUPP_ACC_CODE'], 0,6)=='010101')?"disabled":"";
                            	$selected = ($scan_bill['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected":"";
?>
                               <option <?php echo $selected; ?> <?php echo $disabled; ?> data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>"><?php echo $account['SUPP_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
<?php
if(mysql_num_rows($cash_in_hand_list)){
	while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
		$cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
		if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
			continue;
		}
		if($scan_bill != NULL){
            $disabled = (substr($scan_bill['SUPP_ACC_CODE'], 0,6)=='040101')?"disabled":"";
        }else{
            $disabled = '';
        }
        $selected = ($scan_bill['SUPP_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected":"";
	?>
		<option <?php echo $selected; ?> <?php echo $disabled; ?> data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
	<?php
	}
}
?>
                            </select>
                        </div>
                        <input type="hidden" class="bills-cash-account" value="<?php echo $scan_bill['SUPP_ACC_CODE']; ?>" />
                        <div class="caption" style="width: 450px;">
                        	<div class="pull-left" style="padding-top:5px;">
                        		<?php
                        			if($scan_bill == NULL){
                        				$marka = 'checked="checked"';
                        			}else{
                        				$marka = (substr($scan_bill['SUPP_ACC_CODE'], 0,6)=='010101')?'checked="checked"':"";
                        			}
                        		?>
                        		<input type="checkbox" id="mor1" <?php echo $marka; ?> onchange="makeItCash(this);" class="trasactionType css-checkbox"/>
                            	<label id="mor1" for="mor1" class="css-label"><small>Cash Transaction</small></label>
                        	</div>
                            <div class="pull-left" style="margin-left:10px;">
                                    <input type="text" name="supplier_name" value="<?php echo $scan_bill['SUPPLIER_NAME']; ?>" class="form-control onfocus_wider" style="width:190px;display:<?php echo ($marka == '')?"none":""; ?>;" placeholder="Supplier Info" />
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div style="height: 10px;"></div>
                        <div class="panel panel-default" style="padding:0em;border:none;box-shadow:none;">
                                <div id="form" class="scanner_div" style="text-align: center;margin:0px;margin-top:10px;;">
									<?php if($scan_type == 'S'){  ?>
                                    <input type="text" class="barcode_input form-control text-center" value="" placeholder="Scan Barcode" style="height:50px;font-size:26px;">
									<?php } ?>
									<?php if($scan_type == 'M'){  ?>
									<table style="width: 100%;" class="gen_table">
										<thead>
											<tr>
												<th width="10%">StartCode</th>
												<th width="20%" style="vertical-align: middle;" class="text-center">
													Item
													<span class="pull-right mr-10">
														<a onclick="add_new_item();" class="btn btn-xs btn-default"><i class="fa fa-plus"></i></a>
														<a class="reload_item btn btn-xs btn-default"><i class="fa fa-refresh"></i></a>
													</span>
												</th>
												<th width="5%">Qty</th>
												<th width="5%">Stock</th>
												<th width="15%">Feature</th>
												<th width="10%">Purchase Price</th>
												<?php if($show_discount=='Y'){ ?>
												<th width="7%">Discount</th>
												<?php } ?>
												<?php if($show_tax=='Y'){ ?>
												<th width="7%">Tax</th>
												<?php } ?>
												<th width="10%">Price</th>
												<th width="16%">Sale Price</th>
												<th width="5%">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr class="barcode_gen">
												<td class="start_code"><input tabindex="-1" readonly="readonly" class="form-control text-center" value="<?php echo (int)$last_barcode; ?>"   /></td>
												<td class="item_td">
													<select class="itemSelect show-tick form-control"
														data-style="btn-default"
														data-live-search="true" data-show-subtext="true">
													<option selected value=""></option>
			<?php
														foreach ($items_list as $category_name => $items) {
			?>
															<optgroup label="<?php echo $category_name; ?>">
																<?php
																	foreach ($items as $item_id => $item_details){
																		$item_selected = "";
																?>
																	<option value="<?php echo $item_id; ?>" data-subtext="<?php echo $item_details['COMPANY']; ?>" <?php echo $item_selected; ?> ><?php echo $item_details['NAME']; ?></option>
																<?php
																	}
																?>
															</optgroup>
			<?php
														}
			?>
													</select>
												</td>
												<td class="qty_td"><input  type="text"  value="" class="form-control text-center" /></td>
												<td class="stock_td"><input  type="text"  value="" class="form-control text-center" readonly /></td>
												<td class="colour_td"><input  type="text"  value="" class="form-control text-center" /></td>
												<td class="purchase_td"><input  type="text"  value="" class="form-control text-center" /></td>
												<?php if($show_discount=='Y'){ ?>
												<td class="discount_td"><input  type="text"  value="" class="form-control text-center" /></td>
												<?php } ?>
												<?php if($show_tax=='Y'){ ?>
												<td class="tax_td"><input  type="text"  value="" class="form-control text-center"  /></td>
												<?php } ?>
												<td class="price_td"><input  type="text"  value="" class="form-control text-center" readonly /></td>
												<td class="sprice_td"><input type="text" value="" class="form-control text-center" /></td>
												<td>
													<button type="button" class="btn btn-primary btn-sm btn-sm generate_rows"> Generate </button>
												</td>
											</tr>
										</tbody>
									</table>
									<?php } ?>
                                </div> <!-- End form -->
                            <div class="clear"></div>
                        </div>
						<table style="width: 100%;" class="barcodes_table">
							<thead>
								<tr>
									<th width="5%">Sr.</th>
									<th width="10%">Barcode</th>
									<th width="20%">
										Item
									</th>
									<th width="5%">Stock</th>
									<th width="15%">Feature</th>
									<th width="10%">Purchase Price</th>
									<?php if($show_discount=='Y'){ ?>
									<th width="7%">Discount</th>
									<?php } ?>
									<?php if($show_tax=='Y'){ ?>
									<th width="7%">Tax</th>
									<?php } ?>
									<th width="10%">Price (<?php echo $currency_type; ?>)</th>
									<th width="16%">Sale Price</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
<?php
						if($scan_bill_details != NULL && mysql_num_rows($scan_bill_details)){
							while($row = mysql_fetch_array($scan_bill_details)){
								$status = '';
								$status = ($row['STOCK_STATUS'] == 'S')?"Sold":$status;
								$status = ($row['STOCK_STATUS'] == 'R')?"Returned":$status;
?>
								<tr class="barcode_me dynamic" row-id="<?php echo $row['ID']; ?>">
									<td class="text-center sr_td" ></td>
									<td class="barcode_td" data-code="<?php echo $row['BARCODE']; ?>"><input tabindex="-1" readonly="readonly" class="form-control text-center" value="<?php echo $row['BARCODE']; ?>"   /></td>
									<td class="item_td" data-item="<?php echo $row['ITEM_ID']; ?>">
										<select class="itemSelect show-tick form-control"
								            data-style="btn-default"
								            data-live-search="true" data-show-subtext="true" style="border:none" <?php echo ($row['STOCK_STATUS']!='A')?"disabled":""; ?> >
									       <option selected value=""></option>
<?php
											foreach ($items_list as $category_name => $items) {
?>
												<optgroup label="<?php echo $category_name; ?>">
													<?php
														foreach ($items as $item_id => $item_details){
															$item_selected = ($item_id==$row['ITEM_ID'])?"selected":"";
													?>
														<option value="<?php echo $item_id; ?>" data-subtext="<?php echo $item_details['COMPANY']; ?>" <?php echo $item_selected; ?> ><?php echo $item_details['NAME']; ?></option>
													<?php
														}
													?>
												</optgroup>
<?php
											}
?>
									    </select>
									</td>
									<td class="stock_td"><input  type="text"  value="" class="form-control text-center" readonly /></td>
									<td class="colour_td"><input  type="text"  value="<?php echo $row['COLOUR']; ?>" class="form-control text-center" <?php echo ($row['STOCK_STATUS']!='A')?"readonly":""; ?> /></td>
									<td class="purchase_td"><input  type="text"  value="<?php echo $row['PURCHASE_PRICE']; ?>" class="form-control text-center" <?php echo ($row['STOCK_STATUS']!='A')?"readonly":""; ?> /></td>
									<?php if($show_discount=='Y'){ ?>
									<td class="discount_td"><input  type="text"  value="<?php echo $row['DISCOUNT']; ?>" class="form-control text-center" <?php echo ($row['STOCK_STATUS']!='A')?"readonly":""; ?> /></td>
									<?php } ?>
									<?php if($show_tax=='Y'){ ?>
									<td class="tax_td"><input  type="text"  value="<?php echo $row['TAX']; ?>" class="form-control text-center" <?php echo ($row['STOCK_STATUS']!='A')?"readonly":""; ?> /></td>
									<?php } ?>
									<td class="price_td"><input  type="text"  value="<?php echo $row['PRICE']; ?>" class="form-control text-center" readonly /></td>
									<td class="sprice_td"><input type="text" value="<?php echo $row['SALE_PRICE']; ?>" class="form-control text-center" <?php echo ($row['STOCK_STATUS']!='A')?"":""; ?> /></td>
									<td>
										<?php if($row['STOCK_STATUS']=='A'){ ?>
											<a class="pointer"><i class="fa fa-times"></i></a>
										<?php }else{
											echo $status;
										} ?>
									</td>
								</tr>
<?php
							}
						}
?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5" class="text-right">Total : </td>
									<td><input tabindex="-1" class="form-control total_purchase_price text-center" readonly="readonly" /></td>
									<?php if($show_discount=='Y'){ ?>
									<td><input tabindex="-1" class="form-control total_discount text-center" readonly="readonly" /></td>
									<?php } ?>
									<?php if($show_tax=='Y'){ ?>
									<td><input tabindex="-1" class="form-control total_tax text-center" readonly="readonly" /></td>
									<?php } ?>
									<td><input tabindex="-1" class="form-control total_amount text-center" readonly="readonly" /></td>
									<td colspan="3"></td>
								</tr>
							</tfoot>
						</table>
						<div class="clear" style="height: 20px;"></div>
						<div class="panel panel-default pull-left" style="width:550px;">
                            <div class="panel-heading">Notes : </div>
                            <textarea class="form-control inv_notes" style="height:130px;border-radius:0px;" class="inv_notes"><?php echo $scan_bill['NOTES']; ?></textarea>
                        </div>
						<input type="hidden" class="scan_purchase_id" value="<?php echo ($scan_bill==NULL)?"0":$id; ?>" />
						<div style="margin:0px 5px;" class="button pull-right" onclick="window.location.href='mobile-purchase-details.php';">New Form</div>
						<?php if(($scan_bill != NULL && in_array('modify-mobile-purchase',$permissionz)) || $admin == true || $scan_bill == NULL){ ?>
						<button class="button pull-right save_scan"><?php echo ($scan_bill==NULL)?"Save":"Update"; ?></button>
						<?php } ?>
<?php
						if($scan_bill != NULL){
?>
							<a target="_blank" href="mobile-purchase-invoice.php?id=<?php echo $id; ?>"> <button class="button pull-right mr-5" ><i class="fa fa-print"></i> Print</button></a>
							<?php
								if($sms_config=='Y'){
								?>
									<button class="button pull-right mr-5" onclick="send_sms_form(this,<?php echo $id ?>);"> <i class="fa fa-envelope"></i> Send SMS</button>
							<?php
								}
								?>
							<a class="button pull-right mr-5" id="reset_bt" style="display: none;" href="mobile-purchase-details.php?id=<?php echo $id; ?>"><i class="fa fa-navicon"></i> Reset</a>
<?php
						}
?>
					</div>
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div style="display:none;" class="item_source_div">
    	<select class="item_source show-tick form-control"
            data-style="btn-default"
            data-live-search="true" data-show-subtext="true" style="border:none">
	       <option selected value=""></option>
<?php
			foreach ($items_list as $category_name => $items) {
?>
				<optgroup label="<?php echo $category_name; ?>">
					<?php
						foreach ($items as $item_id => $item_details){
					?>
						<option value="<?php echo $item_id; ?>" data-subtext="<?php echo $item_details['COMPANY']; ?>" ><?php echo $item_details['NAME']; ?></option>
					<?php
						}
					?>
				</optgroup>
<?php
			}
?>
	    </select>
    </div>
    <div id="fade"></div>
    <div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
