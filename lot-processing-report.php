<?php
    ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/accounts.php');
    include ('common/classes/customers.php');
    include ('common/classes/suppliers.php');
    include('common/classes/fabric_contracts.php');
  	include('common/classes/fabric_contract_details.php');
  	include('common/classes/processing_contracts.php');
  	include('common/classes/processing_contracts_details.php');
    include ('common/classes/processing_report.php');
    include ('common/classes/processing_report_details.php');

    //Permission
    if(!in_array('processing-report-form',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource-sp/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource-sp/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objChartOfAccounts            = new ChartOfAccounts();
    $objCustomer                   = new Customers();
    $objSupplier                   = new suppliers();
    $objUserAccount                = new UserAccounts();
    $objFabricContracts 					 = new FabricContracts();
  	$objFabricContractDetails 		 = new FabricContractDetails();
  	$objProcessingContracts    		 = new ProcessingContracts();
  	$objProcessingContractDetails  = new ProcessingContractDetails();
    $objProcessingReport           = new ProcessingReport();
    $objProcessingReportDetails    = new ProcessingReportDetails();

    if(isset($_GET['lot_no'])){
  		$lot_no 	 = (int)mysql_real_escape_string($_GET['lot_no']);
  		$typee 		 = strtoupper(mysql_real_escape_string($_GET['typee']));
  	}else{
  		exit();
  	}


    $objProcessingReport->lot_no = $lot_no;
    $record_list = $objProcessingContracts->lotReport();

  	$rl = (isset($_GET['rl']))?base64_decode($_GET['rl']):"";
  	$rf = (isset($_GET['rf']))?base64_decode($_GET['rf']):"";
?>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style media="screen">
          table tr.record_row td,tfoot td{
            font-size: 12px !important;
            padding: 5px !important;
          }
        </style>

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/processing.report.config.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('input[name=report_date]').datepicker({
                    dateFormat:'dd-mm-yy',
                    showAnim: 'show',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '2000:+10'
                });
                $("select").selectpicker();
                $("select[name='contract']").change(function(){
                    $("input[name=supplier_id]").val($("select[name='contract'] option:selected").attr('data-supplier'));
                });
                $(".received,.difference").prop('readonly',true);
                $("input[name=supplier_id]").val($("select[name='contract'] option:selected").attr('data-supplier'));

                $("#pro_table").change();
                $("input[readonly]").attr("tabindex","-1");

                var total_meters   = parseFloat($("b.total_meters").text())||0;
                var total_received = parseFloat($("th.total_received").text())||0;
                var per_dif = (((total_received/total_meters)*100)).toFixed(0) - 100;

                $("b.difference").text( per_dif ) ;
            });
        </script>
        <style>
            .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
                width: 258px;
            }
            table td,table input{
                text-align: center;
            }
            table input[name=des]{
                text-align: center;
            }
            table td#des{
                text-align: left;
            }
        </style>

    </head>
    <body>
        <div id="sidebar"><?php include("common/left_menu.php") ?></div>
        <div id="bodyWrapper">
        	<div class="content-box-top">
            	<div class="summery_body">
               		<div class="content-box-header">
          					<p>Lot Ledger Details</p>
          					<span id="tabPanel">
          						<div class="tabPanel">
          							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
          							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
          						</div>
          					</span>
          					<div class="clear"></div>
          				</div><!--End.content-box-header-->
          				<div class="clear"></div>

          				<div class = "content-box-header" style="margin-top: 0px !important;">
          			    <span id="tabPanel" class="pull-left ml-20">
          			      <div class="tabPanel pull-left back_links">
          			        <a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tab">Lot</div></a>
          			        <a href="fabric-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo $typee=='FC'?"tabSelected":"tab"; ?>">Grey Fabric</div></a>
          			        <a href="processing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo $typee=='PC'?"tabSelected":"tab"; ?>">Dyeing</div></a>
          			        <a href="packing-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo $typee=='PK'?"tabSelected":"tab"; ?>">Packing</div></a>
          			        <a href="embroidery-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="<?php echo $typee=='EC'?"tabSelected":"tab"; ?>">Embroidery</div></a>
          			        <a href="lot-ledger.php?lot_no=<?php echo $lot_no; ?>"><div class="tab">Ledger</div></a>
          			      </div>
          			    </span>
          			    <div class="clear"></div>
          			  </div><!-- End .content-box-header -->
          				<div class="clear"></div>

          				<div class = "content-box-header" style="margin-top: 0px !important;">
          					<span id="tabPanel" class="pull-left" style="margin-left: 70px;">
          						<div class="tabPanel">
          							<a href="<?php echo $rl; ?>?lot_no=<?php echo $lot_no; ?>"><div class="tab">List</div></a>
          							<a href="<?php echo $rf; ?>?lot_no=<?php echo $lot_no; ?>"><div class="tab">Form</div></a>
          							<a href="lot-stock-history.php?rl=<?php echo base64_encode($rl); ?>&rf=<?php echo base64_encode($rf); ?>&io_status=i&typee=<?php echo $typee; ?>&lot_no=<?php echo $lot_no; ?>"><div class="tab">Inward</div></a>
          							<a href="lot-stock-history.php?rl=<?php echo base64_encode($rl); ?>&rf=<?php echo base64_encode($rf); ?>&io_status=o&typee=<?php echo $typee; ?>&lot_no=<?php echo $lot_no; ?>"><div class="tab">Outward</div></a>
                        <a href="lot-processing-report.php?lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Report</div></a>
          						</div>
          					</span>
          					<div class="clear"></div>
          				</div><!-- End .content-box-header -->
              		<div id="bodyTab1">

                    <?php
                      $lot_list = array();
                      if(mysql_num_rows($record_list)){
                        while($row = mysql_fetch_assoc($record_list)){
                          if(!isset($lot_list[$row['LOT_NO']])){
                            $lot_list[$row['LOT_NO']] = array();
                          }
                          if(!isset($lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']])){
                            $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']] = array();
                          }
                          if(!isset($lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']])){
                            $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']] = array();
                          }
                          if(!isset($lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['FRESH'])){
                            $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['FRESH'] = 0;
                          }
                          if(!isset($lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['C_P'])){
                            $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['C_P'] = 0;
                          }
                          if(!isset($lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['B_GRADE'])){
                            $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['B_GRADE'] = 0;
                          }

                          $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['FRESH']   += ($row['FABRIC_TYPE']=='F')?$row['QUANTITY']:0;
                          $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['B_GRADE'] += ($row['FABRIC_TYPE']=='B')?$row['QUANTITY']:0;
                          $lot_list[$row['LOT_NO']][$row['DYEING_UNIT_LOT_NO']][$row['CONTRACT_DATE']]['C_P']     += ($row['FABRIC_TYPE']=='C')?$row['QUANTITY']:0;
                        }
                      }
                      foreach($lot_list as $lot_no => $dyeing_lot_numbers){
                        foreach($dyeing_lot_numbers as $dyeing_lot_no => $date_records){

                          $total_meters = $objFabricContractDetails->getTotalMetersIssued($lot_no,$dyeing_lot_no);

                    ?>
                    <div class="clear" style="height: 10px;"></div>

                    <div class="caption pull-left ml-20">
                      Fabric Lot # : <b><?php echo $lot_no; ?></b>
                    </div>
                    <div class="caption pull-left ml-20">
                      Dyeing Unit Lot # : <b><?php echo $dyeing_lot_no; ?></b>
                    </div>
                    <div class="clear" style="height: 5px;"></div>

                    <div class="caption pull-left ml-20" >
                      Total Meters : <b class="total_meters"><?php echo $total_meters; ?></b>
                    </div>
                    <div class="caption pull-left ml-20" >
                      Difference : <b class="difference"></b>
                    </div>
                    <div class="clear" style="height: 10px;"></div>

                    <table cellspacing="0" width="100%" >
          						<thead>
          							<tr>
          								<th width="5%" style="text-align:center">Sr.</th>
          								<th width="10%" style="text-align:center">RecordDate</th>
          								<th width="10%" style="text-align:center">Fresh</th>
          								<th width="10%" style="text-align:center">B.Grade</th>
                          <th width="5%" style="text-align:center">C.P</th>
          								<th width="10%" style="text-align:center">Received</th>
          							</tr>
          						</thead>
          						<tbody>
          							<?php
          								$counter       = 1;
                          $total_fresh   = 0;
                          $total_b_grade = 0;
                          $total_c_p     = 0;

                          foreach($date_records as $record_date => $records){
          										?>
          										<tr id="recordPanel">
          											<td style="text-align:center"><?php echo $counter; ?></td>
          											<td style="text-align:center"><?php echo date("d-m-Y",strtotime($record_date)) ?></td>
          											<td style="text-align:center"><?php echo $records['FRESH']; ?></td>
                                <td style="text-align:center"><?php echo $records['B_GRADE']; ?></td>
                                <td style="text-align:center"><?php echo $records['C_P']; ?></td>
                                <td style="text-align:center"><?php echo ($records['FRESH']+$records['B_GRADE']+$records['C_P']) ?></td>
          										</tr>
          										<?php
          										$counter++;

                              $total_fresh   += $records['FRESH'];
                              $total_b_grade += $records['B_GRADE'];
                              $total_c_p     += $records['C_P'];
                          }
          							?>
          							</tbody>
          							<tfoot>
          								<tr>
          									<th class="text-right" colspan="2"> <b>Total :</b> </th>
          									<th class="text-center"><b><?php echo $total_fresh; ?></b></th>
          									<th class="text-center"><b><?php echo $total_b_grade; ?></b></th>
          									<th class="text-center"><b><?php echo $total_c_p; ?></b></th>
                            <th class="text-center total_received"><b><?php echo ($total_fresh+$total_b_grade+$total_c_p); ?></b></th>
          								</tr>
          							</tfoot>
          						</table>
                      <?php
                        }
                      }
                      ?>
                  </div><!--bodyTab1-->
				</div><!--summery_body-->
			</div><!--content-box-top-->



            <!-- Delete confirmation popup -->
            <div id="myConfirm" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- PLease fill product table information popup -->
            <div id="myFillAll" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please provide all details, in detail table.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <!-- PLease fill product table information popup -->
            <div id="selectcontract" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please Select Contract No. first.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Loading bar confirmation popup -->
            <div id="myLoading" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Processing...</h4>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span class="sr-only">100% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- PLease fill product table information popup -->
            <div id="enterPON" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Attention!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger" style="font-weight: bold;">Please enter PON first.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='closee'  name="close">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



            <!-- jquery post error of product confirmation popup -->
            <div id="myError" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #e4b9c0; color: white;">
                            <h4 class="modal-title">Alert!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-warning" style="font-weight: bold;">Your Product Detail in not submitted.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <!-- Edit popup -->
            <div id="myedit" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #72a9d8; color: white;">
                            <h4 class="modal-title">Edit Record</h4>
                        </div>
                        <div id="form" class="modal-body">
                            <form class="editmyform" id="pro_table">

                                <input type="hidden" class="form-control" name="rowid" value="" />

                                <div style="width:450px; margin: auto;">
                                    <div class="caption"> Quality :</div>
                                    <div class="field">
                                        <input class="form-control quality"  value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption"> Issue:</div>
                                    <div class="field">
                                        <input class="form-control issue" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Fresh :</div>
                                    <div class="field">
                                        <input class="form-control addup fresh" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption"> Sample :</div>
                                    <div class="field">
                                        <input class="form-control addup sample" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">B Grade :</div>
                                    <div class="field">
                                        <input class="form-control addup b_grade" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">C.P :</div>
                                    <div class="field">
                                        <input class="form-control addup c_p" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Received :</div>
                                    <div class="field">
                                        <input class="form-control received" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="caption">Difference :</div>
                                    <div class="field">
                                        <input class="form-control difference" value="" />
                                    </div>
                                    <div class="clear"></div>

                                    <div class="clear" style="height:30px"></div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary btn-sm" id="addrow" data-dismiss="modal">Update</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div><!--bodyWrapper-->
    </body>
</html>
<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
