<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sale.php');
	include('../common/classes/sale_details.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/settings.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/customer_contacts.php');

	$objAccounts         	= new ChartOfAccounts();
	$objSale        	 	= new Sale();
	$objSaleDetails 	 	= new SaleDetails();
	$objCustomer 		 	= new Customers();
	$objItems            	= new Items();
	$objItemCategory     	= new itemCategory();
	$objJournalVoucher   	= new JournalVoucher();
	$objConfigs          	= new Configs();
	$objUserAccounts   	 	= new UserAccounts();
	$objCustomerContacts 	= new CustomerContacts();

	if(!(isset($_SESSION['pos']) && $_SESSION['pos'] == 'Y')){
		mysql_close($con);
		exit();
	}


	$user_details        = $objUserAccounts->getDetails($_SESSION['classuseid']);
	$disc_type 			 = $objConfigs->get_config('SHOW_DISCOUNT');
	$send_sms_cond 		 = $objConfigs->get_config('SMS');
	$admin_sms			 = $objConfigs->get_config('ADMIN_SMS');
	$admin_message 		 = $objConfigs->get_config('ADMIN_MSG');
	$invoice_msg    	 = $objConfigs->get_config('INVOICE_MSG');

	//JV Account Codes Used.
	$user_cashinhand_code = ($user_details['CASH_IN_HAND'] == 'Y')?$user_details['CASH_ACC_CODE']:"0101010001";
	$user_cashinhand_title= $objAccounts->getAccountTitleByCode($user_cashinhand_code);

	$taxAccCode  = '0401050001';
	$taxAccTitle  = $objAccounts->getAccountTitleByCode($taxAccCode);

	$costAccCode  = '0301040001';
	$costAccTitle  = $objAccounts->getAccountTitleByCode($costAccCode);

	$inventoryAccCode = '0101060001';
	$inventoryAccTitle = $objAccounts->getAccountTitleByCode($inventoryAccCode);

	$salesAccCode = '0201010001';
	$salesAccTitle = $objAccounts->getAccountTitleByCode($salesAccCode);

	$discountAccCode = '0301010001';
	$discountAccTitle = $objAccounts->getAccountTitleByCode($discountAccCode);

	$returnData = array('MSG'=>'Nothing Happened','ID'=>0,'TOM'=>'');
	$costaItem = 0;
	$costaItemPrev = 0;
	if(isset($_POST['jSonString'])){
		$sale_id   = mysql_real_escape_string($_POST['sale_id']);
		$objSale->saleDate    	  	= date('Y-m-d',strtotime($_POST['saleDate']));
		$objSale->billNum         	= $objSale->genBillNumber();
		$objSale->supplierAccCode 	= mysql_real_escape_string($_POST['supplierCode']);
		$objSale->discount        	= mysql_real_escape_string($_POST['discount']);
		$objSale->discount_type   	= mysql_real_escape_string($_POST['discount_type']);
		$objSale->bill_amount   	= mysql_real_escape_string($_POST['bill_amount']);
		$objSale->sale_discount   	= mysql_real_escape_string($_POST['total_discount']);
		$objSale->mobile_no       	= mysql_real_escape_string($_POST['customer_mobile']);
		$objSale->customer_name   	= mysql_real_escape_string($_POST['customer_name']);
		$objSale->charges   	  	= mysql_real_escape_string($_POST['inv_charges']);
		$objSale->notes   		  	= mysql_real_escape_string($_POST['inv_notes']);
		$objSale->received_cash   	= mysql_real_escape_string($_POST['received_cash']);
		$objSale->recovered_balance = mysql_real_escape_string($_POST['balance_recovered']);
		$objSale->recovery_amount   = mysql_real_escape_string($_POST['recovery_amount']);
		$objSale->change_returned   = mysql_real_escape_string($_POST['change_return']);
		$objSale->remaining_amount  = mysql_real_escape_string($_POST['remaining_amount']);

		if($objSale->mobile_no != ''){
			$objCustomerContacts->customer_mobile 	= $objSale->mobile_no;
			$objCustomerContacts->customer_name 	= mysql_real_escape_string($_POST['customer_name']);
			$objCustomerContacts->customer_address 	= mysql_real_escape_string($_POST['customer_address']);

			$contact_exists = $objCustomerContacts->getDetails($objCustomerContacts->customer_mobile);
			if($contact_exists == NULL){
				$objCustomerContacts->save();
			}else{
				$objCustomerContacts->update($objCustomerContacts->customer_mobile);
			}
		}

		if($sale_id == 0){
			$success = array();
			$sale_id  = $objSale->save();
			if(!$sale_id){
				$returnData['MSG'] = "--ERROR-- Bill Could Not Be Saved.";
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}
			$objSaleDetails->sale_id = $sale_id;
			$jSonData = json_decode($_POST['jSonString']);
			$transactions = get_object_vars($jSonData);
			//get Each Transaction Detail and Save.
			$transac_cost = 0;
			foreach($transactions as $t=>$tansaction){

				$objSaleDetails->item_id 	 		  = $tansaction->item_id;
				$objSaleDetails->qtyReceipt 		  = $tansaction->quantity;
				$objSaleDetails->unitPrice 	  	  	  = $tansaction->unitPrice;
				$objSaleDetails->saleDiscount    	  = $tansaction->discount;
				$objSaleDetails->subTotal			  = $tansaction->subAmount;
				$objSaleDetails->taxRate			  = $tansaction->taxRate;
				$objSaleDetails->taxAmount			  = $tansaction->taxAmount;
				$objSaleDetails->totalAmount		  = $tansaction->totalAmount;
				$average_price 						  = $objItems->getPurchasePrice($objSaleDetails->item_id);
				$objSaleDetails->costPrice 			  = $average_price;

				$sale_detail_id = $objSaleDetails->save();
				//Update Correspoding item Stock.
				if($sale_detail_id){
					if($average_price <= 0){
						//$success[] = 'N';
						$returnData['MSG'] = "ERROR! PURCHASE PRICE FOUND EQUALS TO ZERO.";
					}
					$costaItem    = round($average_price*$objSaleDetails->qtyReceipt,2);
					$transac_cost += $costaItem;
					//$objItems->removeStockValue($objSaleDetails->item_id,$objSaleDetails->qtyReceipt,$costaItem);
				}
				if($sale_detail_id){
					$success[] = "Y";
				}else{
					$success[] = "N";
					$returnData['MSG'] = "ERROR! RECORD CANNOT BE SAVED.";
				}
			}
			//delete the false bill if there is an error!
			if(in_array('N',$success)){
				$saleDetailsArray = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
				$thisVoucherId = $objSale->getVoucherId($objSaleDetails->sale_id);
				foreach($saleDetailsArray as $p => $sale_detail_id){
					$saleDetails = $objSaleDetails->getDetails($sale_detail_id);
					$avg_price   = $objItems->getPurchasePrice($saleDetails['ITEM_ID']);
					$avg_price   = $saleDetails['QUANTITY']*$avg_price;
					//$objItems->addStockValue($saleDetails['ITEM_ID'],$saleDetails['QUANTITY'],$avg_price);
					$objSaleDetails->delete($sale_detail_id);
				}
				$objSale->delete($objSaleDetails->sale_id);
				if($thisVoucherId > 0){
					$objJournalVoucher->reverseVoucherDetails($thisVoucherId);
					$objJournalVoucher->deleteJv($thisVoucherId);
				}
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
exit();
			}elseif(!in_array('N',$success)){
				$taxAmount  = $objSale->getInventoryTaxAmountSum($objSaleDetails->sale_id);
				$billAmount = $objSale->getInventoryAmountSum($objSaleDetails->sale_id);
				if($objSale->discount_type == 'R'){
					$billAmount-= $objSale->discount;
				}elseif($objSale->discount_type == 'P'){
					$billAmount-= ($billAmount*$objSale->discount)/100;
				}
				//GetCostaSales
				$details_id_array = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
				//$costaSales = $transac_cost;
				$supplierAccCode = $objSale->supplierAccCode;
				$supplierAccTitle= $objAccounts->getAccountTitleByCode($objSale->supplierAccCode);
				$sale_discount   = $objSale->sale_discount;

				$returnData['MSG'] = "Bill#".$objSale->billNum." Saved Successfully.";
				$returnData['ID'] = $objSaleDetails->sale_id;
			}
		}elseif($sale_id > 0){
			$objSale->billNum = $_POST['billNum'];
			$objSale->update($sale_id);

			$success = array();
			$objSaleDetails->sale_id = $sale_id;
			$jSonData = json_decode($_POST['jSonString']);
			$transactions = get_object_vars($jSonData);
			$newTransactions = array();

			//get Previous Transactions
			$prevTransactions = $objSale->getSaleDetailArrayIdOnly($sale_id);
			//get New Transactions
			foreach($transactions as $t=>$tansaction){
				$newTransactions[] = $tansaction;
			}
			//New Transaction Id Array
			foreach($transactions as $t=>$tansaction){
				$newTransactionIds[] = $tansaction->row_id;
			}
			$transac_cost = 0;
			//update previous transactions
			foreach($newTransactions as $new=>$tansaction){
				if(in_array($tansaction->row_id,$prevTransactions)){
					$prevDetail = $objSaleDetails->getDetails($tansaction->row_id);

					$objSaleDetails->item_id 	 		  = $tansaction->item_id;
					$objSaleDetails->qtyReceipt 		  = $tansaction->quantity;
					$objSaleDetails->unitPrice 	  	  	  = $tansaction->unitPrice;
					$objSaleDetails->saleDiscount    	  = $tansaction->discount;
					$objSaleDetails->subTotal			  = $tansaction->subAmount;
					$objSaleDetails->taxRate			  = $tansaction->taxRate;
					$objSaleDetails->taxAmount			  = $tansaction->taxAmount;
					$objSaleDetails->totalAmount		  = $tansaction->totalAmount;
					$average_price 						  = $objItems->getPurchasePrice($objSaleDetails->item_id);
					$objSaleDetails->costPrice 			  = $average_price;

					$sale_detail_id = $objSaleDetails->update($prevDetail['ID']);
					if($sale_detail_id){
						///return to inventory
						$costaItemPrev     = $prevDetail['COST_PRICE']*$prevDetail['QUANTITY'];
						//$objItems->addStockValue($prevDetail['ITEM_ID'],$prevDetail['QUANTITY'],$costaItemPrev);
						///return to inventory
						///----------------///
						///fetch  from inventory
						//$average_price     = $objItems->getPurchasePrice($objSaleDetails->item_id);
						$costaItem         = $prevDetail['COST_PRICE']*$objSaleDetails->qtyReceipt;
						$transac_cost     += $costaItem;
						//$objItems->removeStockValue($objSaleDetails->item_id,$objSaleDetails->qtyReceipt,$costaItem);
						///fetch  from inventory
					}
				}
			}

			//delete transactions
			foreach($prevTransactions as $prev=>$prev_id){
				if(!in_array($prev_id,$newTransactionIds) && $prev_id != 0){
					$prevDetail = $objSaleDetails->getDetails($prev_id);
					$costaItemPrev = $prevDetail['COST_PRICE']*$prevDetail['QUANTITY'];
					//$objItems->addStockValue($prevDetail['ITEM_ID'],$prevDetail['QUANTITY'],$costaItemPrev);
					$objSaleDetails->delete($prev_id);
				}
			}
			//get Each Transaction Detail and Save.
			foreach($newTransactions as $t=>$tansaction){
				if($tansaction->row_id == 0){
					$objSaleDetails->item_id 	 		  = $tansaction->item_id;
					$objSaleDetails->qtyReceipt 		  = $tansaction->quantity;
					$objSaleDetails->unitPrice 	  	  	  = $tansaction->unitPrice;
					$objSaleDetails->saleDiscount    	  = $tansaction->discount;
					$objSaleDetails->subTotal			  = $tansaction->subAmount;
					$objSaleDetails->taxRate			  = $tansaction->taxRate;
					$objSaleDetails->taxAmount			  = $tansaction->taxAmount;
					$objSaleDetails->totalAmount		  = $tansaction->totalAmount;

					$average_price 						  = $objItems->getPurchasePrice($objSaleDetails->item_id);
					$objSaleDetails->costPrice 			  = $average_price;

					$sale_detail_id = $objSaleDetails->save();
					//Update Correspoding item Stock.
					if($sale_detail_id){
						$average_price = $objItems->getPurchasePrice($objSaleDetails->item_id);
						$costaItem     = $average_price*$objSaleDetails->qtyReceipt;
						$transac_cost += $costaItem;
						//$objItems->removeStockValue($objSaleDetails->item_id,$objSaleDetails->qtyReceipt,$costaItem);
					}
				}
			}

			$voucher_id = $objSale->getVoucherId($sale_id);

			$objJournalVoucher->reverseVoucherDetails($voucher_id);

			$taxAmount  = $objSale->getInventoryTaxAmountSum($objSaleDetails->sale_id);
			$billAmount = $objSale->getInventoryAmountSum($objSaleDetails->sale_id);
			if($objSale->discount_type == 'R'){
				$billAmount-= $objSale->discount;
			}elseif($objSale->discount_type == 'P'){
				$billAmount-= ($billAmount*$objSale->discount)/100;
			}
			//GetCostaSales
			$details_id_array = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
			//$costaSales = $transac_cost;
			/*
			foreach($details_id_array as $key => $row_id){
				$rowDetail = $objSaleDetails->getDetails($row_id);
				$average_price = $objItems->getPurchasePrice($rowDetail['ITEM_ID']);
				$costaSales += round($average_price*$rowDetail['QUANTITY'],2);
			}
			 * */
			$supplierAccCode = $objSale->supplierAccCode;
			$supplierAccTitle = $objAccounts->getAccountTitleByCode($objSale->supplierAccCode);

			if($voucher_id == 0){
				$returnData['MSG'] = "Cant Find Sale Voucher!";
				$returnData['ID'] = $objSaleDetails->sale_id;
				echo json_encode($returnData);
				mysql_close($con);
exit();
			}
			$sale_discount   = $objSale->sale_discount;
			$returnData['MSG'] = "Bill#".$objSale->billNum." Updated Successfully.";
			$returnData['ID'] = $objSaleDetails->sale_id;
		}

		//voucher entry
		if($objSaleDetails->sale_id){
			$voucher_id = $objSale->getVoucherId($objSaleDetails->sale_id);

			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($objSale->saleDate));
			$objJournalVoucher->voucherType    = 'SV';
			$objJournalVoucher->reference      = 'Sales Voucher';
			$objJournalVoucher->reference_date = date('Y-m-d',strtotime($objSale->saleDate));
			$objJournalVoucher->status	  = 'P';

			if($voucher_id == 0){
				$voucher_id = $objJournalVoucher->saveVoucher();

				$objSale->insertVoucherId($objSaleDetails->sale_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}
			if($voucher_id){

				$billNumber						 = $objSale->billNum;
				$amount 						 = $billAmount;
				$charges						 = $objSale->charges;


				$cash_amount = $objSale->received_cash;
				$cust_amount = ($amount + $charges) + $objSale->recovery_amount;

				$objJournalVoucher->voucherId    = $voucher_id;

				if(substr($supplierAccCode,0,6) == '010104'){
					//on account

					if($objSale->remaining_amount == 0){
						$cash_amount -= $objSale->change_returned;
						//full payment received against bill amount == cleared

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $cust_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $salesAccCode;
						$objJournalVoucher->accountTitle = $salesAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $amount + $sale_discount - $taxAmount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $discountAccCode;
						$objJournalVoucher->accountTitle = $discountAccTitle;
						$objJournalVoucher->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $sale_discount;
						if($sale_discount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}

						//Sale Tax Credit
						$objJournalVoucher->accountCode  = $taxAccCode;
						$objJournalVoucher->accountTitle = $taxAccTitle;
						$objJournalVoucher->narration    = "Sales Tax Payable against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $taxAmount;

						if($taxAmount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}

						//Charges Credit
						$objJournalVoucher->accountCode     = '0201020001';
						$objJournalVoucher->accountTitle    = 'Income A/c Other Revenue';
						$objJournalVoucher->narration       = "Charged to customer against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount          = $charges;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  	= $user_cashinhand_code;
						$objJournalVoucher->accountTitle 	= $user_cashinhand_title;
						$objJournalVoucher->narration    	= "Cash received against billa # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $cust_amount;

						$objJournalVoucher->saveVoucherDetail();


						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = "Amount paid in cash for invoice no ".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						//If Previous Balance Recovered
						if(($objSale->recovered_balance == 'Y') && ($objSale->recovery_amount > 0)){
							$objJournalVoucher->amount       = $cust_amount + $objSale->recovery_amount;
						}else{
							$objJournalVoucher->amount       = $cust_amount;
						}

						$objJournalVoucher->saveVoucherDetail();
					}

					if($objSale->remaining_amount != 0){
						//full payment received against bill amount == cleared

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $cust_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $salesAccCode;
						$objJournalVoucher->accountTitle = $salesAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $amount + $sale_discount - $taxAmount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountTitle = $discountAccTitle;
						$objJournalVoucher->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $sale_discount;
						if($sale_discount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}
						//Sale Tax Credit

						$objJournalVoucher->accountCode  = $taxAccCode;
						$objJournalVoucher->accountTitle = $taxAccTitle;
						$objJournalVoucher->narration    = "Sales Tax Payable against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $taxAmount;

						if($taxAmount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}

						//Charges Credit
						$objJournalVoucher->accountCode     = '0201020001';
						$objJournalVoucher->accountTitle    = 'Income A/c Other Revenue';
						$objJournalVoucher->narration       = "Charged to customer against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount          = $charges;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  	= $user_cashinhand_code;
						$objJournalVoucher->accountTitle 	= $user_cashinhand_title;
						$objJournalVoucher->narration    	= "Cash received against billb # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       	= $cash_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = "Amount paid in cash for invoice no ".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						if(($objSale->recovered_balance == 'Y') && ($objSale->recovery_amount > 0)){
							$objJournalVoucher->amount       = $cust_amount + $objSale->recovery_amount;
						}else{
							$objJournalVoucher->amount       = $cust_amount - $objSale->remaining_amount;
						}

						$objJournalVoucher->saveVoucherDetail();
					}
				}

				if(substr($supplierAccCode,0,6) != '010104'){
					//on account

					if($objSale->remaining_amount == 0){
						$cash_amount -= $objSale->change_returned;
						//full payment received against bill amount == cleared

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $cust_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $salesAccCode;
						$objJournalVoucher->accountTitle = $salesAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $amount + $sale_discount - $taxAmount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountTitle = $discountAccTitle;
						$objJournalVoucher->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $sale_discount;
						if($sale_discount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}
						//Sale Tax Credit

						$objJournalVoucher->accountCode  = $taxAccCode;
						$objJournalVoucher->accountTitle = $taxAccTitle;
						$objJournalVoucher->narration    = "Sales Tax Payable against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $taxAmount;

						if($taxAmount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}

						//Charges Credit
						$objJournalVoucher->accountCode     = '0201020001';
						$objJournalVoucher->accountTitle    = 'Income A/c Other Revenue';
						$objJournalVoucher->narration       = "Charged to customer against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount          = $charges;

						$objJournalVoucher->saveVoucherDetail();

					}

					if($objSale->remaining_amount != 0){
						//full payment received against bill amount == cleared

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $cust_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $salesAccCode;
						$objJournalVoucher->accountTitle = $salesAccTitle;
						$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $amount + $sale_discount - $taxAmount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountTitle = $discountAccTitle;
						$objJournalVoucher->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       = $sale_discount;
						if($sale_discount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}
						//Sale Tax Credit

						$objJournalVoucher->accountCode  = $taxAccCode;
						$objJournalVoucher->accountTitle = $taxAccTitle;
						$objJournalVoucher->narration    = "Sales Tax Payable against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $taxAmount;

						if($taxAmount > 0){
							$objJournalVoucher->saveVoucherDetail();
						}

						//Charges Credit
						$objJournalVoucher->accountCode     = '0201020001';
						$objJournalVoucher->accountTitle    = 'Income A/c Other Revenue';
						$objJournalVoucher->narration       = "Charged to customer against bill#".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount          = $charges;

						$objJournalVoucher->saveVoucherDetail();
					}
				}
				//END If Previous Balance Recovered

				//CostaSales
				$objJournalVoucher->accountCode  = $costAccCode;
				$objJournalVoucher->accountTitle = $costAccTitle;
				$objJournalVoucher->narration    = "Cost of Inventory Sold expensed against bill # ".$billNumber;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       = 0;
				$objJournalVoucher->saveVoucherDetail();

				//CostaInventory
				$objJournalVoucher->accountCode  = $inventoryAccCode;
				$objJournalVoucher->accountTitle = $inventoryAccTitle;
				$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cost of Goods Sold vide invoice no. ".$billNumber:"Cost of Goods Sold to ".$supplierAccTitle." Against Bill# ".$billNumber;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       = 0;
				$objJournalVoucher->saveVoucherDetail();
			}

		}

		echo json_encode($returnData);
		mysql_close($con);
exit();

		if($objSale->mobile_no != '' && $send_sms_cond == 'Y'){
			$dataSMS = array();
			$dataSMS['id'] = $objConfigs->get_config('USER_NAME');

			$dataSMS['admin_sms']   = $admin_sms;
			$dataSMS['admin_messsage'] = $admin_message;

			$dataSMS['destination'] = $objSale->mobile_no.",";
			$dataSMS['message']  = $invoice_msg;
			$sms_search_vars = array();
			$sms_search_vars[] = '[INVOICE]';
			$sms_search_vars[] = '[DATE]';
			$sms_search_vars[] = '[AMOUNT]';

			$sms_replace_vars = array();
			$sms_replace_vars[] = $objSale->billNum;
			$sms_replace_vars[] = date('d-m-Y',strtotime($objSale->saleDate));
			$sms_replace_vars[] = $billAmount - $sale_discount;

			$dataSMS['message'] = str_replace($sms_search_vars, $sms_replace_vars, $dataSMS['message']);
			$dataSMS['admin_messsage'] = str_replace($sms_search_vars, $sms_replace_vars, $dataSMS['admin_messsage']);
			//Soap Client

		}
	}

/*
//sms service
if($objSale->mobile_no != '' && $send_sms_cond == 'Y'){
	$dataSMS = array();
	$dataSMS['id'] = $objConfigs->get_config('USER_NAME');
	$dataSMS['destination'] = $objSale->mobile_no;
	$dataSMS['message'] = 'Dear Customer! Your invoice # '.$objSale->billNum.' at date '.date('d-m-Y',strtotime($objSale->saleDate))." Amount ".$billAmount." has been generated.";

	$options = array('location' => 'http://ring-away.com/smsserver/sms-link/soap-server.php',
                  'uri' => 'http://ring-away.com/smsserver/sms-link/');
	$api = new SoapClient(NULL, $options);
	$api->process($dataSMS['id'],$dataSMS['destination'],$dataSMS['message']);
}
//sms service END

 * TRASH CAN *
foreach($details_id_array as $key => $row_id){
	$rowDetail = $objSaleDetails->getDetails($row_id);
	$item_id = $rowDetail['ITEM_ID'];
	$quantity = $rowDetail['QUANTITY'];
	$purchasePrice = $objItems->getPurchasePrice($item_id);
	$cost = $purchasePrice*$quantity;
	$cost = round($cost,2);
	$costaSales += $cost;
}
*/
mysql_close($con);
exit();
?>
