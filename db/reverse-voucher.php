<?php
	include('../common/db.connection.php');
	include '../common/classes/j-voucher.php';

	$objJournalVoucher = new JournalVoucher;

	$message = array();
	$message['DONE']   =  'N';
	if(isset($_POST['id'])){
		$id = mysql_real_escape_string($_POST['id']);

		$voucher_dat = $objJournalVoucher->getVoucherDate($id);

		if($voucher_dat == '00-00-0000'){
			exit();
		}

		$objJournalVoucher->voucherType = 'RV';
		$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
		$objJournalVoucher->type_num 	= $objJournalVoucher->genTypeNumber('RV');
		$objJournalVoucher->voucherDate = $voucher_dat;
		$objJournalVoucher->reference   = 'Reversal Entry';

		$reversal_id = $objJournalVoucher->saveVoucher();
		$objJournalVoucher->insertReversalId($id,$reversal_id);
		$objJournalVoucher->voucherId = $reversal_id;

		$source_voucher = $objJournalVoucher->getVoucherDetailList($id);
		$source_voucher_details = $objJournalVoucher->getRecordDetailsVoucher($id);
			//reversals
		if($reversal_id){
			if(mysql_num_rows($source_voucher)){
				while($source_row = mysql_fetch_array($source_voucher)){
					$source_row['TRANSACTION_TYPE']    = ($source_row['TRANSACTION_TYPE'] == 'Dr')?"Cr":"Dr";

					$objJournalVoucher->accountCode     = mysql_real_escape_string($source_row['ACCOUNT_CODE']);
					$objJournalVoucher->accountTitle    = mysql_real_escape_string($source_row['ACCOUNT_TITLE']);
					$objJournalVoucher->narration       = mysql_real_escape_string('REVERSAL OF VOUCHER # '.$source_voucher_details['VOUCHER_NO']." DATED : ".date('d-m-Y',strtotime($source_voucher_details['VOUCHER_DATE'])));
					$objJournalVoucher->transactionType = $source_row['TRANSACTION_TYPE'];
					$objJournalVoucher->amount          = (float)$source_row['AMOUNT'];

					$objJournalVoucher->saveVoucherDetail();
				}
			}
		}
		if($reversal_id){
			$message['DONE']   =  'Y';
		}
		echo json_encode($message);
	}
	mysql_close($con);
exit();
?>
