<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/emb_inward.php');
	include('../common/classes/emb_products.php');
	include('../common/classes/measure.php');
	include('../common/classes/customers.php');

	$objAccounts    = new ChartOfAccounts();
	$objInwards 	= new EmbroideryInward();
	$objMeasure 	= new Measures();
	$objProducts 	= new EmbProducts();
	$objCustomers 	= new Customers();


	$inward_id = 0;

	if(isset($_POST['json_data'])){
		$inward_id                    			= (int)mysql_real_escape_string($_POST['inward_id']);

		$objInwards->inward_date 				= date('Y-m-d',strtotime($_POST['inward_date']));
		$objInwards->lot_num 					= mysql_real_escape_string($_POST['lot_num']);
		$objInwards->gate_pass_no				= mysql_real_escape_string($_POST['gate_pass_no']);
		$objInwards->account_code				= mysql_real_escape_string($_POST['account_code']);

		if($inward_id==0){
			$inward_id = $objInwards->saveInward();
		}else{
			$objInwards->updateInward($inward_id);
		}

		$json_data 							 	= json_decode($_POST['json_data']);
		$transactions 					 		= get_object_vars($json_data);

		$deleted_rows 							= json_decode($_POST['deleted_rows']);
		$deleted_rows 					 		= get_object_vars($deleted_rows);

		foreach($deleted_rows as $key => $inward_detail_id){
			$objInwards->deleteDetails($inward_detail_id);
		}

		if($inward_id>0){
			$objInwards->inward_id 			= $inward_id;
			foreach($transactions as $key => $transaction){

				$objInwards->prod_id 		= $transaction->product_id;
				$objInwards->quality 		= $transaction->quality;
				$objInwards->color 			= $transaction->colour;
				$objInwards->than 			= $transaction->thaan;
				$objInwards->measure_id 	= $transaction->measure_id;
				$objInwards->mlength 		= $transaction->len;
				$objInwards->total_length   = $transaction->total_length;

				if($transaction->row_id == 0){
					$transaction->row_id = $objInwards->addInwardDetail();
				}else{
					$objInwards->updateInwardDetail($transaction->row_id);
				}
			}
		}
	}
	echo $inward_id;
	mysql_close($con);
	exit();
?>
