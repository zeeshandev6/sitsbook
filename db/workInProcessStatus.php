<?php
	include('../common/db.connection.php');
	include('../common/classes/wip.php');
	include('../common/classes/wip_issue_details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');

	$objWorkInProcess 	 		 		 = new WorkInProgress();
	$objWorkInProcessConsumeDetails 	 = new WorkInProcessConsumeDetails();
	$objItems            		 		 = new Items();
	$objItemCategory     		 		 = new itemCategory();
	$objJournalVoucher   		 		 = new JournalVoucher();
	$objAccounts         		 		 = new ChartOfAccounts();

	$inv_acc_code = '0101060001';
	$wip_acc_code = '0101120001';
	$foh_acc_code = '0401040002';

	$inv_acc_title = $objAccounts->getAccountTitleByCode($inv_acc_code);
	$wip_acc_title = $objAccounts->getAccountTitleByCode($wip_acc_code);
	$foh_acc_title = $objAccounts->getAccountTitleByCode($foh_acc_code);

	if(isset($_POST['wip_id'])){
		$wip_id 		= mysql_real_escape_string($_POST['wip_id']);
		$status 		= mysql_real_escape_string($_POST['status']);
		$completionDate = date('Y-m-d');
		$statusUpdated  = $objWorkInProcess->updateStatus($wip_id,$status,$completionDate);
		if($statusUpdated){
			$done = 'Y';
		}else{
			$done = 'N';
		}
		$returnData = array('OK'=>$done);
		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
