<?php
	include("../common/db.connection.php");
	include("../common/classes/mobile-purchase.php");
	include("../common/classes/mobile-purchase-details.php");
	include("../common/classes/mobile-sale-details.php");
	include("../common/classes/items.php");

	$objScanPurchase 			= new ScanPurchase();
	$objScanPurchaseDetails 	= new ScanPurchaseDetails();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objItems 					= new Items();

	if(isset($_GET['barcode'])){
		$barcode = mysql_real_escape_string($_GET['barcode']);
		$purchase_details = $objScanPurchaseDetails->get_details_by_barcode($barcode, 'A');
		if($purchase_details != NULL){
			 $item_details = $objItems->getRecordDetails($purchase_details['ITEM_ID']);
			 $purchase_details['STOCK'] = $objScanPurchaseDetails->get_item_stock($purchase_details['ITEM_ID']);
			 $purchase_details['COMPANY'] = $item_details['COMPANY'];
			 $purchase_details['ITEM_NAME'] = $item_details['NAME'];
			 $purchase_details['EXPIRY_DATE']=date('d-m-Y',strtotime($purchase_details['EXPIRY_DATE']));
			 echo json_encode($purchase_details);
		}
	}

	if(isset($_GET['msrcode'])){
		$barcode = mysql_real_escape_string($_GET['msrcode']);
		$purchase_details = $objScanPurchaseDetails->get_details_by_barcode($barcode, '');
		if($purchase_details != NULL){
			$scan_sale_detail = $objScanSaleDetails->getLastSale($purchase_details['BARCODE']);
			$item_details = $objItems->getRecordDetails($purchase_details['ITEM_ID']);
			$purchase_details['SS_ID']       = $objScanSaleDetails->getSSIDByBarcode($purchase_details['BARCODE']);
			$purchase_details['STOCK']       = $objScanPurchaseDetails->get_item_stock($purchase_details['ITEM_ID']);
			$purchase_details['SALE_INFO']   = $scan_sale_detail;
			$purchase_details['SALE_INFO']['SALE_DATE'] = "";
			if($scan_sale_detail != NULL){
				$purchase_details['SALE_INFO']['SALE_DATE'] = date("d-m-Y",strtotime($scan_sale_detail['SALE_DATE']));
			}
			$purchase_details['COMPANY']     = $item_details['COMPANY'];
			$purchase_details['ITEM_NAME']   = $item_details['NAME'];
			$purchase_details['EXPIRY_DATE'] = date('d-m-Y',strtotime($purchase_details['EXPIRY_DATE']));
			echo json_encode($purchase_details);
		}
	}
	mysql_close($con);
exit();
?>
