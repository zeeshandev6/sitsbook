<?php
	include('../common/db.connection.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/items.php');
	include('../common/classes/measure.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/settings.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');

	$objUserAccounts 					= new UserAccounts();
	$objItems 		     	  		= new Items();
	$objMeasures         	  	= new Measures();
	$objItemsCategory    	  	= new itemCategory();
	$objMobilePurchaseDetails = new ScanPurchaseDetails();
	$objBranches  	          = new Branches();
	$objBranchStock           = new BranchStock();
  $objStockTransfer         = new StockTransfer();
  $objStockTransferDetail   = new StockTransferDetail();
	$objConfigs       		 		= new Configs();

	$permissionz 				= $objUserAccounts->getPermissions($_SESSION['classuseid']);
	$permissionz 				= explode('|',$permissionz);

	$barcode_print_flag = $objConfigs->get_config('BARCODE_PRINT');
	$barcode_print_flag = ($barcode_print_flag == 'Y')?true:false;

	$counter = 1;
	if(isset($_POST['cat_id'])){
		$sale_scanner = $objConfigs->get_config('SALE_SCANNER');
		$category_id = mysql_real_escape_string($_POST['cat_id']);
		$itemsList = $objItems->getListByCategory($category_id);
		$category  = $objItemsCategory->getDetails($category_id);
		if(mysql_num_rows($itemsList)){
			while($row = mysql_fetch_array($itemsList)){
				if(isset($items_exclude) && !in_array($row['ID'],$items_exclude)){
					continue;
				}
				$active = ($row['ACTIVE']=='Y')?"checked":"";
				$measureName  = $objMeasures->getName($row['MEASURE']);

				if($row['INV_TYPE']=='B'){
					$row['STOCK_QTY'] = $objMobilePurchaseDetails->get_item_stock($row['ID']);
				}
				$godowns_stock = $objBranchStock->getItemStockGodowns($row['ID']);
				$braches_stock = $objBranchStock->getItemStockBranches($row['ID']);

				$item_total_cost = $row['PURCHASE_PRICE']*$row['STOCK_QTY'];
				$item_total_cost+= $row['PURCHASE_PRICE']*$braches_stock;
				$item_total_cost+= $row['PURCHASE_PRICE']*$godowns_stock;


				$row['ITEM_BARCODE'] = ($row['ITEM_BARCODE']=='')?"":"(".$row['ITEM_BARCODE'].")";
?>
                <tr id="recordPanel" data-cat-id="<?php echo $category['ITEM_CATG_ID']; ?>">
                    <td style="text-align:center"><?php echo $counter; ?></td>
                    <td style="text-align:left"><?php echo $row['COMPANY']; ?></td>
                    <td style="text-align:left !important;padding-left:5px;"><?php echo $row['NAME'].$row['ITEM_BARCODE']; ?></td>
                    <td style="text-align:center"><?php echo $row['STOCK_QTY']; ?></td>
                    <td style="text-align:center"><?php echo $godowns_stock; ?>&nbsp; </td>
                    <td style="text-align:center"><?php echo $braches_stock; ?>&nbsp; </td>
                    <td style="text-align:center" data-min-qty="<?php echo $row['MIN_QTY']; ?>" data-item-id="<?php echo $row['ID']; ?>"><?php echo $row['MIN_QTY']; ?></td>
                    <td style="text-align:center;background-color:<?php echo ($row['INV_TYPE']=='B')?"#F5F5F5":""; ?>;"><?php echo ($row['INV_TYPE']=='B')?"":$row['PURCHASE_PRICE']; ?></td>
                    <td style="text-align:center"><?php echo $row['ITEM_DISCOUNT']; ?></td>
                    <td style="text-align:center" <?php echo ($row['INV_TYPE'] != 'B')?'data-price-type="s"':"data-grey='true'"; ?> data-item-id="<?php echo $row['ID']; ?>"><?php echo ($row['SALE_PRICE'] == 0)?"":$row['SALE_PRICE']; ?></td>
                    <td style="text-align:center"><?php echo $item_total_cost; ?></td>
                    <td style="text-align:center">
                        <span class="switch">
                            <input id="cmn-toggle-<?php echo $row['ID']; ?>" class="cmn-toggle cmn-toggle-round checkActive" do="<?php echo $row['ID']; ?>" type="checkbox" <?php echo $active; ?> >
                            <label for="cmn-toggle-<?php echo $row['ID']; ?>"></label>
                        </span>
                    </td>
                    <td style="text-align:center"><?php echo ($row['INV_TYPE'] == 'B')?"<img src='resource/images/validtext.png' />":''; ?></td>
                    <td style="text-align:center">
                        <a target="_blank" id="view_button" onclick="$(this).parent().addClass('bg-info');" href="item-details.php?id=<?php echo $row['ID']; ?>"><i class="fa fa-pencil"></i></a>
                        <?php if($barcode_print_flag){ ?>
                        <a id="view_button" target="_blank" href="item-barcodes-print.php?id=<?php echo $row['ID']; ?>"><i class="fa fa-barcode"></i></a>
                        <?php } ?>
												<?php if(in_array('items-managment-modify',$permissionz) || $admin){ ?>
                        <a do="<?php echo $row['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
												<?php } ?>
                    </td>
                </tr>
<?php
			$counter++;
			}
		}
	}
	mysql_close($con);
exit();
?>
