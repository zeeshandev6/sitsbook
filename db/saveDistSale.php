<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/dist_sale.php');
	include('../common/classes/dist_sale_details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/settings.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts         	   = new ChartOfAccounts();
	$objSale        	 	   = new DistributorSale();
	$objSaleDetails 	 	   = new DistributorSaleDetails();
	$objBranches  	           = new Branches();
	$objBranchStock            = new BranchStock();
	$objStockTransfer          = new StockTransfer();
	$objStockTransferDetail    = new StockTransferDetail();
	$objCustomer 		       = new Customers();
	$objItems                  = new Items();
	$objItemCategory           = new itemCategory();
	$objJournalVoucher         = new JournalVoucher();
	$objConfigs                = new Configs();
	$objUserAccounts   	       = new UserAccounts();

	$user_details        = $objUserAccounts->getDetails($_SESSION['classuseid']);
	//$branch_id 		 = $user_details['BRANCH_ID'];
	$branch_id 			 = 0;
	$disc_type 			 = $objConfigs->get_config('SHOW_DISCOUNT');
	$send_sms_cond 		 = $objConfigs->get_config('SMS');
	$admin_sms			 = $objConfigs->get_config('ADMIN_SMS');
	$admin_message 		 = $objConfigs->get_config('ADMIN_MSG');
	$invoice_msg    	 = $objConfigs->get_config('INVOICE_MSG');
	$stock_check    	 = $objConfigs->get_config('STOCK_CHECK');
	$sale_subject    	 = $objConfigs->get_config('SALE_SUBJECT');
	$use_cartons    	 = $objConfigs->get_config('USE_CARTONS');
	$stock_check         = ($stock_check == 'Y')?true:false;

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems            	   = new Items();
      $objBranchStock          = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->addStockValue($item_id,$quantity,$total_cost);
      }
  }
  function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems            	   = new Items();
      $objBranchStock          = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->removeStockValue($item_id,$quantity,$total_cost);
      }
  }

	//JV Account Codes Used.
	$user_cashinhand_code = ($user_details['CASH_IN_HAND'] == 'Y')?$user_details['CASH_ACC_CODE']:"0101010001";
	$user_cashinhand_title= $objAccounts->getAccountTitleByCode($user_cashinhand_code);

  $other_income_acc      = '0201020001';
  $other_income_title    = $objAccounts->getAccountTitleByCode($other_income_acc);;

  $services_income_acc   = '0202010001';
  $services_income_title = $objAccounts->getAccountTitleByCode($services_income_acc);;

	$taxAccCode            = '0401050001';
	$taxAccTitle           = $objAccounts->getAccountTitleByCode($taxAccCode);

	$costAccCode           = '0301040001';
	$costAccTitle          = $objAccounts->getAccountTitleByCode($costAccCode);

	$inventoryAccCode      = '0101060001';
	$inventoryAccTitle     = $objAccounts->getAccountTitleByCode($inventoryAccCode);

	$salesAccCode          = '0201010001';
	$salesAccTitle         = $objAccounts->getAccountTitleByCode($salesAccCode);

	$discountAccCode       = '0301010001';
	$discountAccTitle      = $objAccounts->getAccountTitleByCode($discountAccCode);

	$tradeOfferAccCode       = '0301010003';
	$tradeOfferAccTitle      = $objAccounts->getAccountTitleByCode($tradeOfferAccCode);

	$returnData            = array('MSG'=>'Nothing Happened','ID'=>0,'TOM'=>'');
	$costaItem             = 0;

	$costaItemPrev         = 0;

  $tax_service_charges   = 0;
  $total_service_charges = 0;

	$hit_stock 			= 'Y';
	$prev_hit_stock = 'Y';

	if(isset($_POST['trans_rows'])){
		$sale_id                     = mysql_real_escape_string($_POST['sale_id']);
		$objSale->user_id 			 = (int)mysql_real_escape_string($_POST['user_id']);
		$objSale->order_taker 		 = (int)mysql_real_escape_string($_POST['order_taker']);
		$objSale->saleDate    	  	 = date('Y-m-d',strtotime($_POST['saleDate']));
		$objSale->order_date    	 = date('Y-m-d',strtotime($_POST['order_date']));
		$objSale->record_time 		 = date('Y-m-d h:i:s');
		$objSale->billNum         	 = (int)mysql_real_escape_string($_POST['billNum']);
		$objSale->supplierAccCode 	 = mysql_real_escape_string($_POST['supplierCode']);
		$objSale->po_number 	 	 = mysql_real_escape_string($_POST['po_number']);
		$objSale->trade_offer        = (float)mysql_real_escape_string($_POST['trade_offer']);
		$objSale->discount        	 = mysql_real_escape_string($_POST['discount']);
		$objSale->discount_type   	 = mysql_real_escape_string($_POST['discount_type']);
		$objSale->bill_amount   	 = mysql_real_escape_string($_POST['bill_amount']);
		$objSale->sale_discount   	 = mysql_real_escape_string($_POST['total_discount']);
		$objSale->mobile_no       	 = mysql_real_escape_string($_POST['customer_mobile']);
		$objSale->customer_name   	 = mysql_real_escape_string($_POST['customer_name']);
		$objSale->charges   	  	 = mysql_real_escape_string($_POST['inv_charges']);
		$objSale->subject   		 = mysql_real_escape_string(trim($_POST['subject']));
		$objSale->notes   		  	 = mysql_real_escape_string($_POST['inv_notes']);
		$objSale->received_cash   	 = mysql_real_escape_string($_POST['received_cash']);
		$objSale->recovered_balance  = mysql_real_escape_string($_POST['balance_recovered']);
		$objSale->recovery_amount    = mysql_real_escape_string($_POST['recovery_amount']);
		$objSale->change_returned    = mysql_real_escape_string($_POST['change_return']);
		$objSale->remaining_amount   = mysql_real_escape_string($_POST['remaining_amount']);
		$objSale->with_tax  		 = mysql_real_escape_string($_POST['tax_invoice']);
		$objSale->registered_tax  	 = mysql_real_escape_string($_POST['registered_tax']);
		$objSale->tax_bill_num  	 = mysql_real_escape_string($_POST['tax_bill_num']);

		$objSale->user_id            = $objSale->user_id==0?1:$objSale->user_id;

		$_SESSION['ORDER_TAKER_ID']	= (int)$objSale->order_taker;
		$_SESSION['SALESMAN_ID']	  = (int)$objSale->user_id;
		$_SESSION['SALE_DATE']      = $objSale->saleDate;
		$_SESSION['OSALE_DATE']     = $objSale->order_date;

		if($objSale->with_tax == 'Y' && $objSale->registered_tax == 'N'){
			$hit_stock = 'N';
		}

		if($sale_id == 0){
			$success = array();
			$sale_id  = $objSale->save();
			if(!$sale_id){
				$returnData['MSG'] = "--ERROR-- Bill Could Not Be Saved.";
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}
			$objSaleDetails->sale_id = $sale_id;
			$jSonData 		= json_decode($_POST['trans_rows']);
			$transactions 	= get_object_vars($jSonData);

			//get Each Transaction Detail and Save.
			$transac_cost = 0;
			foreach($transactions as $t=>$tansaction){
				if($tansaction->item_type == 'S'){
					$objSaleDetails->service_id 	  = $tansaction->item_id;
					$objSaleDetails->item_id        = 0;
				}else{
					$objSaleDetails->service_id     = 0;
					$objSaleDetails->item_id 	      = $tansaction->item_id;
				}
				$objSaleDetails->cartons 		  = $tansaction->cartons;
				$objSaleDetails->rate_carton 	  = $tansaction->rate_carton;
				$objSaleDetails->dozens 		  = $tansaction->dozen;
				$objSaleDetails->dozen_price 	  = $tansaction->dozen_price;
				$objSaleDetails->qtyReceipt 	  = $tansaction->quantity;
				$objSaleDetails->unitPrice 	  	  = $tansaction->unitPrice;
				$objSaleDetails->saleDiscount     = $tansaction->discount;
				$objSaleDetails->subTotal		  = $tansaction->subAmount;
				$objSaleDetails->taxRate		  = $tansaction->taxRate;
				$objSaleDetails->taxAmount		  = $tansaction->taxAmount;
				$objSaleDetails->totalAmount	  = $tansaction->totalAmount;

				if($tansaction->item_type == 'S'){
					$tax_service_charges   += $tansaction->taxAmount;
					$total_service_charges += $tansaction->totalAmount;
				}
				if($tansaction->item_type != 'S'){
					$average_price 				= $objItems->getPurchasePrice($objSaleDetails->item_id);
					$objSaleDetails->costPrice 	= $average_price;
				}
				$sale_detail_id 				= $objSaleDetails->save();

				//Update Correspoding item Stock.
				if($tansaction->item_type != 'S'){
					if($sale_detail_id){
						if($average_price <= 0){
								$success[] = 'N';
								$returnData['MSG'] = "ERROR! PURCHASE PRICE FOUND EQUALS TO ZERO.";
						}
						$objSaleDetails->qtyReceipt   += $tansaction->qty_carton*$tansaction->cartons;
						$objSaleDetails->qtyReceipt   += $objSaleDetails->dozens*12;
						$costaItem     				   = $average_price*$objSaleDetails->qtyReceipt;
						$transac_cost 				  += $costaItem;
						if($hit_stock == 'Y'){
							remove_branch_stock($objSaleDetails->item_id,$branch_id,$objSaleDetails->qtyReceipt,$costaItem);
						}
					}
				}
				if($sale_detail_id){
					$success[] = "Y";
				}else{
					$success[] = "N";
					$returnData['MSG'] = "ERROR! RECORD CANNOT BE SAVED.[1]";
				}
			}
			//delete the false bill if there is an error!
			if(in_array('N',$success)){
				$saleDetailsArray = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
				$thisVoucherId = $objSale->getVoucherId($objSaleDetails->sale_id);
				foreach($saleDetailsArray as $p => $sale_detail_id){
					$saleDetails = $objSaleDetails->getDetails($sale_detail_id);
					$qty_per_carton = $objItems->getItemQtyPerCarton($saleDetails['ITEM_ID']);
					$avg_price   = $objItems->getPurchasePrice($saleDetails['ITEM_ID']);
					$saleDetails['QUANTITY'] += $qty_per_carton*$saleDetails['CARTONS'];
					$avg_price   = $saleDetails['QUANTITY']*$avg_price;
					if($hit_stock == 'Y'){
						add_branch_stock($saleDetails['ITEM_ID'],$branch_id,$saleDetails['QUANTITY'],$avg_price);
					}
					$objSaleDetails->delete($sale_detail_id);
				}
				$objSale->delete($objSaleDetails->sale_id);
				if($thisVoucherId > 0){
					$objJournalVoucher->reverseVoucherDetails($thisVoucherId);
					$objJournalVoucher->deleteJv($thisVoucherId);
					$objSale->insertVoucherId($objSaleDetails->sale_id,0);
				}
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}elseif(!in_array('N',$success)){
				$taxAmount  = $objSale->getInventoryTaxAmountSum($objSaleDetails->sale_id);
				$billAmount = $objSale->getInventoryAmountSum($objSaleDetails->sale_id);
				if($objSale->discount_type == 'R'){
					$billAmount -= $objSale->discount;
				}elseif($objSale->discount_type == 'P'){
					$billAmount -= ($billAmount*$objSale->discount)/100;
				}
				$billAmount -= $objSale->trade_offer;
				//GetCostaSales
				$details_id_array = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
				$costaSales 			= $transac_cost;
				$supplierAccCode 	= $objSale->supplierAccCode;
				$supplierAccTitle = $objAccounts->getAccountTitleByCode($objSale->supplierAccCode);
				$sale_discount   	= $objSale->sale_discount;

				$returnData['MSG'] = "Bill#".$objSale->billNum." Saved Successfully.";
				$returnData['ID']  = $objSaleDetails->sale_id;
			}
		}elseif($sale_id > 0){
			$objSale->billNum 		 = $_POST['billNum'];
			$prev_sale  					 = $objSale->getDetail($sale_id);

			//$user_details        = $objUserAccounts->getDetails($prev_sale['USER_ID']);
			//$branch_id 		       = (int)$user_details['BRANCH_ID'];

			if($prev_sale['USER_ID'] != $_SESSION['classuseid']){
				//$returnData['MSG'] = "Error! cannot update record.( please change user )";
				//echo json_encode($returnData);
				//mysql_close($con);
				//exit();
			}

			$objSale->update($sale_id);

			if($prev_sale['WITH_TAX'] == 'Y' && $prev_sale['REGISTERED_TAX'] == 'N'){
				$prev_hit_stock = 'N';
			}

			$success 				 				 = array();
			$objSaleDetails->sale_id = $sale_id;
			$jSonData 				 			 = json_decode($_POST['trans_rows']);
			$transactions 			 		 = get_object_vars($jSonData);

			$newTransactions 		 	= array();

			//get Previous Transactions
			$prevTransactions 		 = $objSale->getSaleDetailArrayIdOnly($sale_id);
			//get New Transactions
			foreach($transactions as $t=>$tansaction){
				$newTransactions[] = $tansaction;
			}
			//New Transaction Id Array
			foreach($transactions as $t=>$tansaction){
				$newTransactionIds[] = $tansaction->row_id;
			}
			$transac_cost = 0;
			//update previous transactions
			foreach($newTransactions as $new=>$tansaction){
				if(in_array($tansaction->row_id,$prevTransactions)){
					$prevDetail = $objSaleDetails->getDetails($tansaction->row_id);

	          if($tansaction->item_type == 'S'){
	              $objSaleDetails->service_id 	  = $tansaction->item_id;
	              $objSaleDetails->item_id        = 0;
	          }else{
	              $objSaleDetails->service_id     = 0;
	              $objSaleDetails->item_id 	      = $tansaction->item_id;
	          }
	          	$objSaleDetails->cartons 		      = $tansaction->cartons;
			  	$objSaleDetails->rate_carton 		  = $tansaction->rate_carton;
			  	$objSaleDetails->dozens 		      = $tansaction->dozen;
			  	$objSaleDetails->dozen_price 		  = $tansaction->dozen_price;
				$objSaleDetails->qtyReceipt 		  = $tansaction->quantity;
				$objSaleDetails->unitPrice 	  	      = $tansaction->unitPrice;
				$objSaleDetails->saleDiscount    	  = $tansaction->discount;
				$objSaleDetails->subTotal			  = $tansaction->subAmount;
				$objSaleDetails->taxRate			  = $tansaction->taxRate;
				$objSaleDetails->taxAmount			  = $tansaction->taxAmount;
				$objSaleDetails->totalAmount		  = $tansaction->totalAmount;
	          if($tansaction->item_type == 'S'){
	            $tax_service_charges   += $tansaction->taxAmount;
	            $total_service_charges += $tansaction->totalAmount;
	          }
	          if($tansaction->item_type != 'S'){
					   $average_price 						  = $objItems->getPurchasePrice($objSaleDetails->item_id);
					   $objSaleDetails->costPrice 	= $average_price;
					 }
					 $sale_detail_id = $objSaleDetails->update($prevDetail['ID']);
					 if($sale_detail_id){
						  if($prev_hit_stock == 'Y'){
								///return to inventory
								$qty_per_carton 					 = $objItems->getItemQtyPerCarton($prevDetail['ITEM_ID']);
								$prevDetail['QUANTITY']   += $qty_per_carton*$prevDetail['CARTONS'];
		 						$costaItemPrev     				 = $prevDetail['COST_PRICE']*$prevDetail['QUANTITY'];
								add_branch_stock($prevDetail['ITEM_ID'],$branch_id,$prevDetail['QUANTITY'],$costaItemPrev);
								///return to inventory
							}
							///----------------///
							if($tansaction->item_type != 'S'){
								if($hit_stock == 'Y'){
									///fetch  from inventory
									$average_price = $prevDetail['COST_PRICE'];
									if($prevDetail['ITEM_ID']!=$objSaleDetails->item_id){
										$average_price     = $objItems->getPurchasePrice($objSaleDetails->item_id);
									}
									$objSaleDetails->qtyReceipt   += $tansaction->qty_carton*$tansaction->cartons;
									$objSaleDetails->qtyReceipt   += $tansaction->dozen*12;
									$costaItem         = $prevDetail['COST_PRICE']*$objSaleDetails->qtyReceipt;
									$transac_cost     += $costaItem;
									remove_branch_stock($objSaleDetails->item_id,$branch_id,$objSaleDetails->qtyReceipt,$costaItem);
									///fetch  from inventory
								}
							}
					}
				}
			}

			//delete transactions
			foreach($prevTransactions as $prev=>$prev_id){
				if(!in_array($prev_id,$newTransactionIds) && $prev_id != 0){
					if($prev_hit_stock == 'Y'){
						$prevDetail = $objSaleDetails->getDetails($prev_id);
						$qty_per_carton 					 = $objItems->getItemQtyPerCarton($prevDetail['ITEM_ID']);
						$prevDetail['QUANTITY']   += $qty_per_carton*$prevDetail['CARTONS'];
						$costaItemPrev = $prevDetail['COST_PRICE']*$prevDetail['QUANTITY'];
						add_branch_stock($prevDetail['ITEM_ID'],$branch_id,$prevDetail['QUANTITY'],$costaItemPrev);
					}
					$objSaleDetails->delete($prev_id);
				}
			}

			//get Each Transaction Detail and Save.
			foreach($newTransactions as $t=>$tansaction){
				if($tansaction->row_id == 0){
          if($tansaction->item_type == 'S'){
              $objSaleDetails->item_id          = 0;
              $objSaleDetails->service_id 	  	= $tansaction->item_id;
          }else{
              $objSaleDetails->service_id       = 0;
              $objSaleDetails->item_id 	      	= $tansaction->item_id;
          }
          $objSaleDetails->cartons 		      = $tansaction->cartons;
		  $objSaleDetails->rate_carton 		  = $tansaction->rate_carton;
		  $objSaleDetails->dozens 		      = $tansaction->dozen;
		  $objSaleDetails->dozen_price 		  = $tansaction->dozen_price;
		  $objSaleDetails->qtyReceipt 		  = $tansaction->quantity;
		  $objSaleDetails->unitPrice 	  	  = $tansaction->unitPrice;
		  $objSaleDetails->saleDiscount    	  = $tansaction->discount;
		  $objSaleDetails->subTotal			  = $tansaction->subAmount;
		  $objSaleDetails->taxRate			  = $tansaction->taxRate;
		  $objSaleDetails->taxAmount		  = $tansaction->taxAmount;
		  $objSaleDetails->totalAmount		  = $tansaction->totalAmount;

          if($tansaction->item_type == 'S'){
              $tax_service_charges   += $tansaction->taxAmount;
              $total_service_charges += $tansaction->totalAmount;
          }

          if($tansaction->item_type != 'S'){
						$average_price 						  = $objItems->getPurchasePrice($objSaleDetails->item_id);
						$objSaleDetails->costPrice 			  = $average_price;
					}
					$sale_detail_id = $objSaleDetails->save();

					//Update Correspoding item Stock.
					if($tansaction->item_type != 'S'){
							if($sale_detail_id){
									$average_price = $objItems->getPurchasePrice($objSaleDetails->item_id);
									$objSaleDetails->qtyReceipt   += $tansaction->qty_carton*$tansaction->cartons;
									$objSaleDetails->qtyReceipt   += $tansaction->dozen*12;
									$costaItem     = $average_price*$objSaleDetails->qtyReceipt;
									$transac_cost += $costaItem;
									if($hit_stock == 'Y'){
										remove_branch_stock($objSaleDetails->item_id,$branch_id,$objSaleDetails->qtyReceipt,$costaItem);
									}
							}
					}
				}
			}

			$billAmount = $objSale->getInventoryAmountSum($objSaleDetails->sale_id);

			if($objSale->discount_type == 'R'){
				$billAmount-= $objSale->discount;
			}elseif($objSale->discount_type == 'P'){
				$billAmount-= ($billAmount*$objSale->discount)/100;
			}

			//GetCostaSales
			$details_id_array = $objSale->getSaleDetailArrayIdOnly($objSaleDetails->sale_id);
			$costaSales       = $transac_cost;

			$supplierAccCode = $objSale->supplierAccCode;
			$supplierAccTitle = $objAccounts->getAccountTitleByCode($objSale->supplierAccCode);

			$sale_discount   = $objSale->sale_discount;
			$returnData['MSG'] = "Bill#".$objSale->billNum." Updated Successfully.";
			$returnData['ID'] = $objSaleDetails->sale_id;
		}


		$taxAmount  = $objSale->getInventoryTaxAmountSum($objSaleDetails->sale_id);
		//voucher entry
		if($objSaleDetails->sale_id){
			$voucher_id = $objSale->getVoucherId($objSaleDetails->sale_id);

			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($objSale->saleDate));
			$objJournalVoucher->voucherType    = 'SV';
			$objJournalVoucher->reference      = 'Sales Voucher';
			$objJournalVoucher->reference_date = date('Y-m-d',strtotime($objSale->saleDate));
			$objJournalVoucher->status	       = 'P';

			if($voucher_id == 0){
				$voucher_id = $objJournalVoucher->saveVoucher();
				$objSale->insertVoucherId($objSaleDetails->sale_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id && $hit_stock == 'Y'){
				$billNumber						= $objSale->billNum;
				$amount 						= $billAmount;
				$charges						= $objSale->charges;
				$trade_offer_amount  			= $objSale->trade_offer;
				$cash_amount = $objSale->received_cash;
				$cust_amount = ($amount + $charges) - $trade_offer_amount;
				$objJournalVoucher->voucherId    = $voucher_id;

					if(substr($supplierAccCode,0,6) == '010104'){
						//on account
						if($objSale->remaining_amount == 0){
							$cash_amount -= $objSale->change_returned;
							//full payment received against bill amount == cleared
							$objJournalVoucher->accountCode  = $supplierAccCode;
							$objJournalVoucher->accountTitle = $supplierAccTitle;
							if($sale_subject == 'Y' && $objSale->subject != ''){
								$objJournalVoucher->narration    = $objSale->subject;
							}else{
								$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Invoice # ".$billNumber:"Amount Recievable Vide Invoice # ".$billNumber;
							}
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       = $cust_amount;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $tradeOfferAccCode;
							$objJournalVoucher->accountTitle 		= $tradeOfferAccTitle;
							$objJournalVoucher->narration    		= "Trade Offer On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType 	= 'Dr';
							$objJournalVoucher->amount       		= (float)$trade_offer_amount;
							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  = $salesAccCode;
							$objJournalVoucher->accountTitle = $salesAccTitle;
							$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       = ($amount + $sale_discount) - ($total_service_charges+$taxAmount);

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  = $discountAccCode;
							$objJournalVoucher->accountTitle = $discountAccTitle;
							$objJournalVoucher->narration    = "Discount Allowed On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       = $sale_discount;
							if($sale_discount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}

							//Sale Tax Credit
							$objJournalVoucher->accountCode  = $taxAccCode;
							$objJournalVoucher->accountTitle = $taxAccTitle;
							$objJournalVoucher->narration    = "Sales Tax Payable against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       = $taxAmount;

							if($taxAmount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}

							//Charges Credit
							$objJournalVoucher->accountCode     = $other_income_acc;
							$objJournalVoucher->accountTitle    = $other_income_title;
							$objJournalVoucher->narration       = "Charged to customer against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $charges;

							$objJournalVoucher->saveVoucherDetail();

		          //Service Charges Credit
		          $objJournalVoucher->accountCode     = $services_income_acc;
							$objJournalVoucher->accountTitle    = $services_income_title;
							$objJournalVoucher->narration       = "Services Income against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $total_service_charges;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  	= $user_cashinhand_code;
							$objJournalVoucher->accountTitle 	= $user_cashinhand_title;
							$objJournalVoucher->narration    	= "Cash received against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount          = $objSale->received_cash - $objSale->change_returned ;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  = $supplierAccCode;
							$objJournalVoucher->accountTitle = $supplierAccTitle;
							$objJournalVoucher->narration    = "Amount paid in cash for invoice no ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';

							//If Previous Balance Recovered
							if(($objSale->recovered_balance == 'Y') && ($objSale->recovery_amount > 0)){
								$objJournalVoucher->amount       = $cust_amount + $objSale->recovery_amount;
							}

							$objJournalVoucher->saveVoucherDetail();
						}

						if($objSale->remaining_amount != 0){
							//full payment received against bill amount == cleared

							$objJournalVoucher->accountCode  = $supplierAccCode;
							$objJournalVoucher->accountTitle = $supplierAccTitle;
							if($sale_subject == 'Y' && $objSale->subject != ''){
								$objJournalVoucher->narration    = $objSale->subject;
							}else{
								$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Invoice # ".$billNumber:"Amount Recievable Vide Invoice # ".$billNumber;
							}
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       = $cust_amount;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $tradeOfferAccCode;
							$objJournalVoucher->accountTitle 		= $tradeOfferAccTitle;
							$objJournalVoucher->narration    		= "Trade Offer On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType 	= 'Dr';
							$objJournalVoucher->amount       		= (float)$trade_offer_amount;
							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  = $salesAccCode;
							$objJournalVoucher->accountTitle = $salesAccTitle;
							$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide Invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       = ($amount + $sale_discount) - ($total_service_charges+$taxAmount);

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountTitle = $discountAccTitle;
							$objJournalVoucher->narration    = "Discount Allowed On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       = $sale_discount;
							if($sale_discount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}
							//Sale Tax Credit

							$objJournalVoucher->accountCode  = $taxAccCode;
							$objJournalVoucher->accountTitle = $taxAccTitle;
							$objJournalVoucher->narration    = "Sales Tax Payable against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       = $taxAmount;

							if($taxAmount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}

							//Charges Credit
							$objJournalVoucher->accountCode     = $other_income_acc;
							$objJournalVoucher->accountTitle    = $other_income_title;
							$objJournalVoucher->narration       = "Charged to customer against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $charges;

							$objJournalVoucher->saveVoucherDetail();

	            //Service Charges Credit
	            $objJournalVoucher->accountCode     = $services_income_acc;
							$objJournalVoucher->accountTitle    = $services_income_title;
							$objJournalVoucher->narration       = "Services Income against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $total_service_charges;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $user_cashinhand_code;
							$objJournalVoucher->accountTitle 		= $user_cashinhand_title;
							$objJournalVoucher->narration    		= "Cash received against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       		= $cash_amount;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $supplierAccCode;
							$objJournalVoucher->accountTitle 		= $supplierAccTitle;
							$objJournalVoucher->narration    		= "Amount paid in cash for Invoice no ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							if(($objSale->recovered_balance == 'Y') && ($objSale->recovery_amount > 0)){
								$objJournalVoucher->amount        = $cust_amount + $objSale->recovery_amount;
							}

							$objJournalVoucher->saveVoucherDetail();
						}
					}

					if(substr($supplierAccCode,0,6) != '010104'){
						//on account

						if($objSale->remaining_amount == 0){
							$cash_amount -= $objSale->change_returned;
							//full payment received against bill amount == cleared

							$objJournalVoucher->accountCode  		= $supplierAccCode;
							$objJournalVoucher->accountTitle 		= $supplierAccTitle;
							$objJournalVoucher->narration    		= (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Invoice # ".$billNumber:"Amount Recievable Vide Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       		= $cust_amount;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $tradeOfferAccCode;
							$objJournalVoucher->accountTitle 		= $tradeOfferAccTitle;
							$objJournalVoucher->narration    		= "Trade Offer On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType 	= 'Dr';
							$objJournalVoucher->amount       		= (float)$trade_offer_amount;
							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $salesAccCode;
							$objJournalVoucher->accountTitle 		= $salesAccTitle;
							$objJournalVoucher->narration    		= (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide Invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       		= ($amount + $sale_discount) - ($total_service_charges+$taxAmount);

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountTitle 		= $discountAccTitle;
							$objJournalVoucher->narration    		= "Discount Allowed On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       		= $sale_discount;
							if($sale_discount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}
							//Sale Tax Credit

							$objJournalVoucher->accountCode 	 	= $taxAccCode;
							$objJournalVoucher->accountTitle	 	= $taxAccTitle;
							$objJournalVoucher->narration    		= "Sales Tax Payable against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       		= $taxAmount;

							if($taxAmount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}

							//Charges Credit
							$objJournalVoucher->accountCode     = $other_income_acc;
							$objJournalVoucher->accountTitle    = $other_income_title;
							$objJournalVoucher->narration       = "Charged to customer against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $charges;

							$objJournalVoucher->saveVoucherDetail();

	            //Service Charges Credit
							$objJournalVoucher->accountCode     = $services_income_acc;
							$objJournalVoucher->accountTitle    = $services_income_title;
							$objJournalVoucher->narration       = "Services Income against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $total_service_charges;

							$objJournalVoucher->saveVoucherDetail();
						}
						if($objSale->remaining_amount != 0){
							//full payment received against bill amount == cleared
							$objJournalVoucher->accountCode  		= $supplierAccCode;
							$objJournalVoucher->accountTitle 		= $supplierAccTitle;
							$objJournalVoucher->narration    		= (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Invoice # ".$billNumber:"Amount Recievable Vide Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       		= $cust_amount;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountCode  		= $salesAccCode;
							$objJournalVoucher->accountTitle 		= $salesAccTitle;
							$objJournalVoucher->narration    		= (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       		= ($amount + $sale_discount - $taxAmount) - $total_service_charges;

							$objJournalVoucher->saveVoucherDetail();

							$objJournalVoucher->accountTitle 		= $discountAccTitle;
							$objJournalVoucher->narration    		= "Discount Allowed On Sales Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Dr';
							$objJournalVoucher->amount       		= $sale_discount;
							if($sale_discount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}
							//Sale Tax Credit

							$objJournalVoucher->accountCode  		= $taxAccCode;
							$objJournalVoucher->accountTitle		= $taxAccTitle;
							$objJournalVoucher->narration    		= "Sales Tax Payable against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount       		= $taxAmount;

							if($taxAmount > 0){
								$objJournalVoucher->saveVoucherDetail();
							}

							//Charges Credit
							$objJournalVoucher->accountCode     = $other_income_acc;
							$objJournalVoucher->accountTitle    = $other_income_title;
							$objJournalVoucher->narration       = "Charged to customer against Invoice #".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $charges;

							$objJournalVoucher->saveVoucherDetail();

	            //Service Charges Credit
							$objJournalVoucher->accountCode     = $services_income_acc;
							$objJournalVoucher->accountTitle    = $services_income_title;
							$objJournalVoucher->narration       = "Services Income against Invoice # ".$billNumber;
							$objJournalVoucher->transactionType = 'Cr';
							$objJournalVoucher->amount          = $total_service_charges;

							$objJournalVoucher->saveVoucherDetail();
						}
					}
					//END If Previous Balance Recovered

					//CostaSales
					$objJournalVoucher->accountCode  		= $costAccCode;
					$objJournalVoucher->accountTitle 		= $costAccTitle;
					$objJournalVoucher->narration    		= "Cost of Inventory Sold expensed against Invoice # ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       		= $costaSales;

					$objJournalVoucher->saveVoucherDetail();

					//CostaInventory
					$objJournalVoucher->accountCode 		= $inventoryAccCode;
					$objJournalVoucher->accountTitle 		= $inventoryAccTitle;
					$objJournalVoucher->narration    	  = (substr($supplierAccCode,0,6) == '010101')?"Cost of Goods Sold vide Invoice no. ".$billNumber:"Cost of Goods Sold to ".$supplierAccTitle." Against Invoice# ".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       		= $costaSales;

					$objJournalVoucher->saveVoucherDetail();
			}

			if($voucher_id && $hit_stock == 'N'){

				$objJournalVoucher->voucherId       = $voucher_id;
				//CostaSales
				$objJournalVoucher->accountCode  		= $salesAccCode;
				$objJournalVoucher->accountTitle 		= $salesAccTitle;
				$objJournalVoucher->narration    		= "Invoice # ".$objSale->billNum;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       		= $taxAmount;

				$objJournalVoucher->saveVoucherDetail();

				//CostaInventory
				$objJournalVoucher->accountCode 		= $taxAccCode;
				$objJournalVoucher->accountTitle 		= $taxAccTitle;
				$objJournalVoucher->narration       = "Sales Tax Payable against Invoice #".$objSale->billNum;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       		= $taxAmount;

				$objJournalVoucher->saveVoucherDetail();
			}
		}
		echo json_encode($returnData);
		mysql_close($con);
		exit();
		if($objSale->mobile_no != '' && $send_sms_cond == 'Y'){
			$dataSMS = array();
			$dataSMS['id'] = $objConfigs->get_config('USER_NAME');

			$dataSMS['admin_sms']   = $admin_sms;
			$dataSMS['admin_messsage'] = $admin_message;

			$dataSMS['destination'] = $objSale->mobile_no.",";
			$dataSMS['message']  = $invoice_msg;
			$sms_search_vars = array();
			$sms_search_vars[] = '[INVOICE]';
			$sms_search_vars[] = '[DATE]';
			$sms_search_vars[] = '[AMOUNT]';

			$sms_replace_vars = array();
			$sms_replace_vars[] = $objSale->billNum;
			$sms_replace_vars[] = date('d-m-Y',strtotime($objSale->saleDate));
			$sms_replace_vars[] = $billAmount - $sale_discount;

			$dataSMS['message']        = str_replace($sms_search_vars, $sms_replace_vars, $dataSMS['message']);
			$dataSMS['admin_messsage'] = str_replace($sms_search_vars, $sms_replace_vars, $dataSMS['admin_messsage']);
			//Soap Client
		}
	}
	mysql_close($con);
	exit();
?>
