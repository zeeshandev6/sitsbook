<?php
	include('../common/db.connection.php');
	include('../common/classes/dist_sale_return.php');
	include('../common/classes/dist_sale_return_details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/settings.php');

	$objSale 		   		   = new DistributorSaleReturn();
	$objSaledetails    		   = new DistributorSaleReturnDetails();
	$objBranches  	           = new Branches();
	$objBranchStock            = new BranchStock();
	$objStockTransfer          = new StockTransfer();
	$objStockTransferDetail    = new StockTransferDetail();
	$objJournalVoucher 	       = new JournalVoucher();
	$objItems  		   	       = new Items();
	$objConfigs 			   = new Configs();

	$stock_check = $objConfigs->get_config('STOCK_CHECK');

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
		$objItems            	     = new Items();
		$objBranchStock            = new BranchStock();
		if($branch_id > 0){
			$objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
		}else{
			$objItems->addStockValue($item_id,$quantity,$total_cost);
		}
	}
	function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
		$objItems            	     = new Items();
		$objBranchStock            = new BranchStock();
		if($branch_id > 0){
			$objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
		}else{
			$objItems->removeStockValue($item_id,$quantity,$total_cost);
		}
	}
	function get_branch_stock($brnch_id,$item_id){
		$objItems            	     = new Items();
		$objBranchStock            = new BranchStock();
		if($brnch_id > 0){
			return $objBranchStock->getItemStock($brnch_id,$item_id);
		}else{
			return $objItems->getStock($item_id);
		}
	}

	$return_data = array();

	if(isset($_POST['cid'])){
		$sale_id 		= (int)$_POST['cid'];
		$saleDetailList = $objSaledetails->getList($sale_id);
		//$branch_id 		= $objSale->getBranchId($sale_id);
		$branch_id 			= 0;
		$voucher_id 		= $objSale->getVoucherId($sale_id);
		$success 			= array();
		$saleDetailList = $objSaledetails->getList($sale_id);
		$items_array = array();
		if(mysql_num_rows($saleDetailList)){
			while($row = mysql_fetch_array($saleDetailList)){
				$item_id   = $row['ITEM_ID'];
				$qty_carton= $objItems->getItemQtyPerCarton($item_id);
				$quantity  = (float)$row['QUANTITY'];
				$quantity += $qty_carton*$row['CARTONS'];
				$quantity += ($row['DOZENS']*12);

				$items_array[$item_id] = $quantity;

				$itemStock = (float)(get_branch_stock($branch_id,$item_id));
				if($itemStock >= $quantity){
					$success[] = 'Y';
				}else{
					if($stock_check == 'Y'){
						$success[] = 'N';
					}
				}
			}
		}
		if(!in_array('N',$success)){
			foreach($items_array as $item_id => $qty){
				$avg_price = $objItems->getPurchasePrice($item_id);
				$avg_price = $avg_price*$qty;
				remove_branch_stock($item_id,$branch_id,$qty,$avg_price);
			}
			$objSale->delete($sale_id);
			$objSaledetails->deleteCompleteBill($sale_id);

			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			$return_data['MSG'] = 'Record Deleted Successfully!';
			$return_data['OK']  = 'Y';
			echo json_encode($return_data);
			exit();
		}else{
			$return_data['MSG'] = 'Not Enough Stock!';
			$return_data['OK']  = 'N';
			echo json_encode($return_data);
			exit();
		}
	}
	mysql_close($con);
?>
