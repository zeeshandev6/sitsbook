<?php
	include('../common/db.connection.php');
	include '../common/classes/accounts.php';
	include '../common/classes/emb_outward.php';
	include '../common/classes/measure.php';
	include '../common/classes/machines.php';
	include '../common/classes/j-voucher.php';
	include '../common/classes/emb_lot_register.php';

	$objAccoutns   	   			= new ChartOfAccounts();
	$objOutwards       			= new outward();
	$objJournalVoucher 			= new JournalVoucher();
	$objMeasure        			= new Measures();
	$objMachines       			= new machines();
	$objLotRegisterDetails  = new EmbLotRegister();

	$done = 0;
	$message = 'Success! Outward gate pass created!';

	if(isset($_POST['createVoucher'])){
		$salesAccCode  = '0201010001';
		$salesAccTitle = $objOutwards->getAccountTitle($salesAccCode);

		$mainId 			 = $_POST['mainId'];

		$objOutwards->outwd_date       = date('Y-m-d',strtotime($_POST['outward_date']));
		$objOutwards->billNum          = mysql_real_escape_string($_POST['billNumber']);
		$objOutwards->cloth          	 = mysql_real_escape_string($_POST['cloth']);
		$objOutwards->yarn          	 = mysql_real_escape_string($_POST['yarn']);
		$objOutwards->transport        = mysql_real_escape_string($_POST['transport']);
		$objOutwards->customerAccCode  = mysql_real_escape_string($_POST['customerAccCode']);
		$objOutwards->customerAccTitle = mysql_real_escape_string($_POST['customerAccTitle']);
		$objOutwards->third_party_code = mysql_real_escape_string($_POST['third_party_code']);

		$lot_register_rows  					 = get_object_vars(json_decode($_POST['lot_register_rows']));
		$bill_rows  					 				 = get_object_vars(json_decode($_POST['bill_rows']));
		$deleted_rows  					 			 = get_object_vars(json_decode($_POST['deleted_rows']));

		if($mainId == 0){
			$billExists = $objOutwards->checkBillDuplication($objOutwards->billNum,$mainId);
			 if(!$billExists){
				if($objOutwards->save()){
					$mainId = mysql_insert_id();
				}else{
					$mainId = 0;
				}
			 }
		}else{
			$message = 'Success! Outward gate pass updated!';
		}
		if($mainId){
			$objOutwards->update($mainId);
			$machine_sale = array();
			$objOutwards->outwd_id 				= $mainId;
			foreach($lot_register_rows as $key => $lotRow){
					$objOutwards->lot_register_id = mysql_real_escape_string($lotRow->row_id);
					$objOutwards->product_id 			= mysql_real_escape_string($lotRow->product_id);
					$objOutwards->lotNum 					= mysql_real_escape_string($lotRow->lot_no);
					$objOutwards->quality 				= mysql_real_escape_string($lotRow->quality);
					$objOutwards->measure 				= mysql_real_escape_string($lotRow->measure_id);
					$objOutwards->billing_type 		= mysql_real_escape_string($lotRow->billing_type);
					$objOutwards->stitches 				= mysql_real_escape_string($lotRow->stitches);
					$objOutwards->total_laces 		= mysql_real_escape_string($lotRow->total_laces);
					$objOutwards->length 					= mysql_real_escape_string($lotRow->measure_length);
					$objOutwards->embRate 				= mysql_real_escape_string($lotRow->emb_rate);
					$objOutwards->embAmount 			= mysql_real_escape_string($lotRow->emb_amount);
					$objOutwards->stitchRate 			= mysql_real_escape_string($lotRow->stitch_rate);
					$objOutwards->stitchAmount 		= mysql_real_escape_string($lotRow->stitch_amount);
					$objOutwards->stitchAccount 	= mysql_real_escape_string($lotRow->stitch_acc);
					$objOutwards->designNum 			= mysql_real_escape_string($lotRow->design_code);
					$objOutwards->machineNum 			= mysql_real_escape_string($lotRow->machine_id);

					$objOutwards->remarks 				= (isset($lotRow->remarks))?mysql_real_escape_string($lotRow->remarks):"";

					$outward_detail_id 						= $objOutwards->saveOutwardDetails();

					if($lotRow->row_id > 0){
						$objLotRegisterDetails->insert_outaward_detail_id($lotRow->row_id,$outward_detail_id);
						$objLotRegisterDetails->status($lotRow->row_id,'O');
						$lot_length = $objLotRegisterDetails->getLength($lotRow->row_id);
						if($lot_length > $lotRow->measure_length){
							$objLotRegisterDetails->setPartialStatus($lotRow->row_id,'Y');
						}
						if($lot_length == $lotRow->measure_length){
							$objLotRegisterDetails->setPartialStatus($lotRow->row_id,'N');
						}
					}
					if(isset($machine_sale[$lotRow->machine_id])){
						$machine_sale[$lotRow->machine_id] += $lotRow->emb_amount+$lotRow->stitch_amount;
					}else{
						$machine_sale[$lotRow->machine_id] = $lotRow->emb_amount+$lotRow->stitch_amount;
					}
			}

			foreach($bill_rows as $key => $bill_row){

					$objOutwards->product_id 			= mysql_real_escape_string($bill_row->product_id);
					$objOutwards->lotNum 					= mysql_real_escape_string($bill_row->lot_no);
					$objOutwards->quality 				= mysql_real_escape_string($bill_row->quality);
					$objOutwards->measure 				= mysql_real_escape_string($bill_row->measure_id);
					$objOutwards->billing_type 		= mysql_real_escape_string($bill_row->billing_type);
					$objOutwards->stitches 				= mysql_real_escape_string($bill_row->stitches);
					$objOutwards->total_laces 		= mysql_real_escape_string($bill_row->total_laces);
					$objOutwards->length 					= mysql_real_escape_string($bill_row->measure_length);
					$objOutwards->embRate 				= mysql_real_escape_string($bill_row->emb_rate);
					$objOutwards->embAmount 			= mysql_real_escape_string($bill_row->emb_amount);
					$objOutwards->stitchRate 			= mysql_real_escape_string($bill_row->stitch_rate);
					$objOutwards->stitchAmount 		= mysql_real_escape_string($bill_row->stitch_amount);
					$objOutwards->stitchAccount 	= mysql_real_escape_string($bill_row->stitch_acc);
					$objOutwards->designNum 			= mysql_real_escape_string($bill_row->design_code);
					$objOutwards->machineNum 			= mysql_real_escape_string($bill_row->machine_id);

					$objOutwards->remarks 				= (isset($bill_row->remarks))?mysql_real_escape_string($bill_row->remarks):"";

					$objOutwards->updateOutwardDetail($bill_row->row_id);

					if(isset($machine_sale[$bill_row->machine_id])){
						$machine_sale[$bill_row->machine_id] += $bill_row->emb_amount+$bill_row->stitch_amount;
					}else{
						$machine_sale[$bill_row->machine_id] = $bill_row->emb_amount+$bill_row->stitch_amount;
					}
			}
			foreach($deleted_rows as $key => $outwd_detl_id){
				$objLotRegisterDetails->statusByOutwardDetailId($outwd_detl_id,'C');
				$objOutwards->deleteDetails($outwd_detl_id);
			}
			if($mainId){
				$done = 1;
			}
			//get outward details - required
			$thisDate 				= $objOutwards->getOutwardDate($mainId);
			$outwardBillNum 	= $objOutwards->getBillNumber($mainId);
			$outwardDetail 		= $objOutwards->getDetailsById($mainId);
			$amountTotal  		= $objOutwards->getAmountSum($mainId);

			$customerAccCode 			= $objOutwards->getCutomerOfOutward($mainId);
			$customerAccTitle 		= $objOutwards->getAccountTitle($customerAccCode);
			$machineListIdAmount 	= $objOutwards->getMachineIdAmountList($mainId);

			//calculate Shed Sale
			$shedSale = array();
			foreach($machine_sale as $machine_id => $amount){
				 $shedAccCode 	= $objMachines->getShedAccCode($machine_id);
				 settype($shedAccCode,'string');
				 if(isset($shedSale[$shedAccCode])){
					 $shedSale[$shedAccCode] += $amount;
				 }else{
					 $shedSale[$shedAccCode] = $amount;
				 }
			}

			/*

			if($outward_voucher_id == 0){
				//create voucher if not exits
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->accountCode = $customerAccCode;
				$objJournalVoucher->voucherDate = $thisDate;
				$objJournalVoucher->status 			= 'P';
				//create voucher
				$voucherCreated = $objJournalVoucher->saveVoucher();
				if($voucherCreated){
					$voucherId 		= $voucherCreated;
					$objOutwards->insertVoucherId($mainId,$voucherId);
					//make Transaction
					$objJournalVoucher->voucherId 			= $voucherId;

					$objJournalVoucher->accountCode 		= $customerAccCode;
					$objJournalVoucher->accountTitle 		= $customerAccTitle;
					$objJournalVoucher->narration 			= "BILL No.".$outwardBillNum;
					$objJournalVoucher->transactionType = "Dr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $salesAccCode;
					$objJournalVoucher->accountTitle    = $salesAccTitle;
					$objJournalVoucher->narration 	    = "Sale Amount Recoverable From ".$customerAccTitle."-Bill#".$objOutwards->billNum;
					$objJournalVoucher->transactionType = "Cr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();
					$done = 1;
				}else{
					$done = 0;
				}
			}else{
				$voucherStatus = $objJournalVoucher->getVoucherStatus($outward_voucher_id);
				$objJournalVoucher->reverseVoucherDetails($outward_voucher_id);

				$objJournalVoucher->voucherDate    = $thisDate;
				$objJournalVoucher->updateVoucherDate($outward_voucher_id);
				if($outward_voucher_id){
					$voucherId = $outward_voucher_id;
					//make Transaction
					$objJournalVoucher->voucherId 			= $voucherId;
					$objJournalVoucher->accountCode 		= $customerAccCode;
					$objJournalVoucher->accountTitle 		= $customerAccTitle;
					$objJournalVoucher->narration 			= "BILL No.".$outwardBillNum;
					$objJournalVoucher->transactionType = "Dr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();

					foreach($shedSale as $shedAccCode => $shedAmount){
						$objJournalVoucher->accountCode 		= $shedAccCode;
						$objJournalVoucher->accountTitle 		= $objAccoutns->getAccountTitleByCode($shedAccCode);
						$objJournalVoucher->narration 			= "Sale Amount Recoverable From ".$customerAccTitle."-Bill#".$objOutwards->billNum;
						$objJournalVoucher->transactionType = "Cr";
						$objJournalVoucher->amount 					= $shedAmount;

						$objJournalVoucher->saveVoucherDetail();
					}
					$done = 1;
				}else{
					$done = 0;
				}
			}*/
		}
		if($done == 1){
			echo $message;
		}else{
			echo "Error Occured while completing process!";
		}
	}
?>
