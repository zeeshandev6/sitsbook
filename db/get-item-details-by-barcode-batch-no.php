<?php
	include('../common/db.connection.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/items.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/branch_stock.php');

	$objItems 		 							= new Items();
	$objItemBatchStock     			= new ItemBatchStock();
	$objBranchStock            	= new BranchStock();

	if(isset($_GET['barcode'])){
		$item_barcode = mysql_real_escape_string($_GET['barcode']);
		$item_details = $objItems->getDetailByBarcode($item_barcode);
		if($item_details!=NULL){
			$active_batch_details = $batch_details  = $objItemBatchStock->getActiveBatchNumber($item_details['ID'],date('Y-m-d'));
			$item_details['STOCK_QTY']		= $active_batch_details['STOCK'];
			$item_details['BATCH_NO']  		= $active_batch_details['BATCH_NO'];
			$item_details['EXPIRY_DATE']  = $active_batch_details['EXPIRY_DATE'];
			echo json_encode($item_details);
		}
		exit();
	}
?>
