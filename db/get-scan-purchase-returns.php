<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/mobile-purchase-return.php');
	include('../common/classes/mobile-purchase-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts              = new ChartOfAccounts();
	$objScanPurchase          = new ScanPurchase();
	$objScanPurchaseDetails   = new ScanPurchaseDetails();
	$objScanPurchaseReturn    = new ScanPurchaseReturn();
	$objScanPurchaseReturnDetails = new ScanPurchaseReturnDetails();
	$objItems            	  = new Items();
	$objJournalVoucher        = new JournalVoucher();

	$purchase_id = mysql_real_escape_string($_GET['purchase_id']);
		$purchase_returns = $objScanPurchaseReturn->getReturns($purchase_id);
		if(mysql_num_rows($purchase_returns)){
			while($purchase_return_main = mysql_fetch_array($purchase_returns)){
				$purchase_return_details = $objScanPurchaseReturnDetails->getList($purchase_return_main['ID']);
				if(mysql_num_rows($purchase_return_details)){
?>
					<div id="panel-<?php echo $purchase_return_main['ID']; ?>">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            	<div class="caption_simple">
                            		Return Date : <?php echo date('d-m-Y',strtotime($purchase_return_main['RETURN_DATE'])) ?>
                            	</div>
                            	<div class="pull-right">
                            		<button class="btn btn-default btn-sm delete_purchase_return" do="<?php echo $purchase_return_main['ID']; ?>"><span class="fa fa-times"></span></button>
                            	</div>
                            </div>
                            <table class="wel_made">
                            <thead>
                                <tr>
                                   <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">Barcode</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Item Name</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Colour</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Price</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
							$price = 0;
							while($invRow = mysql_fetch_array($purchase_return_details)){
								$sp_details = $objScanPurchaseDetails->getDetails($invRow['MPDID']);
								$itemName = $objItems->getItemTitle($sp_details['ITEM_ID']);
?>
                                <tr class="alt-row" data-row-id='<?php echo $invRow['ID']; ?>'>
                                	<td style="text-align:center;"><?php echo $sp_details['BARCODE'] ?></td>
                                    <td style="text-align:left;" class="itemName" data-item='<?php echo $sp_details['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
                                    <td style="text-align:center;"><?php echo $sp_details['COLOUR'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['PRICE'] ?></td>
                                </tr>
<?php
								$price += $invRow['PRICE'];
							}
?>
                                <tr class="totals">
                                    	<td style="text-align:center;background-color:#EEEEEE;" colspan="3">Total</td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo number_format($price,2,'.',''); ?></td>
                                    </tr>
                            </tbody>
                        </table>
                        </div>
                        <hr />
                        </div>
<?php
						}
					}
				}else{
?>
						<div class="block text-center">No Recent Returns!</div>
<?php
				}
				mysql_close($con);
exit();
?>
