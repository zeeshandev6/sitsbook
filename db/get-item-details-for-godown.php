<?php
	include('../common/db.connection.php');
	include('../common/classes/items.php');
	include('../common/classes/godown.php');
	include('../common/classes/godown-details.php');

	$objItmes  		   = new Items;
	$objGodown 		   = new Godown;
	$objGodownDetails  = new GodownDetails;

	$return = array();

	if(isset($_GET['item_id'])){
		$item_id 	 = mysql_real_escape_string($_GET['item_id']);
		$from_godown = mysql_real_escape_string($_GET['from_godown']);
		$to_godown   = mysql_real_escape_string($_GET['to_godown']);

		if($from_godown != '0'){
			$from_godown_stock = $objGodownDetails->getItemStockInGodown($from_godown, $item_id);
		}else{
			$from_godown_stock = $objItmes->getStock($item_id);
		}
		if($to_godown != '0'){
			$to_godown_stock = $objGodownDetails->getItemStockInGodown($to_godown, $item_id);
		}else{
			$to_godown_stock = $objItmes->getStock($item_id);
		}

		$return['ITEM_ID'] 	   = $item_id;
		$return['FROM_GODOWN'] = $from_godown_stock;
		$return['TO_GODOWN']   = $to_godown_stock;

		echo json_encode($return);
	}
	mysql_close($con);
exit();
?>
