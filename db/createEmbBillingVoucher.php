<?php
	include('../common/db.connection.php');
	include '../common/classes/accounts.php';
	include '../common/classes/emb_outward.php';
	include '../common/classes/emb_billing.php';
	include '../common/classes/emb_products.php';
	include '../common/classes/measure.php';
	include '../common/classes/machines.php';
	include '../common/classes/j-voucher.php';
	include '../common/classes/emb_lot_register.php';

	$objChartOfAccounts   	= new ChartOfAccounts();
	$objEmbBilling 					= new EmbBilling();
	$objEmbProducts 				= new EmbProducts();
	$objOutwards       			= new outward();
	$objJournalVoucher 			= new JournalVoucher();
	$objMeasure        			= new Measures();
	$objMachines       			= new machines();
	$objLotRegisterDetails  = new EmbLotRegister();

	$done 						= 0;
	$message 					= 'Success! Embroidery Bill created!';
	$bill_amount 			= 0;
	$narration_emb		= '';
	$production_len 	= array();
	$production_amt 	= array();

	if(isset($_POST['createVoucher'])){
		$salesAccCode  = '0201010001';

		$mainId 			 = (int)mysql_real_escape_string($_POST['mainId']);

		$objEmbBilling->outwd_date       = date('Y-m-d',strtotime($_POST['outward_date']));
		$objEmbBilling->billNum          = mysql_real_escape_string($_POST['billNumber']);
		$objEmbBilling->gate_pass_number = mysql_real_escape_string($_POST['gp_no']);
		$objEmbBilling->cloth          	 = mysql_real_escape_string($_POST['cloth']);
		$objEmbBilling->yarn          	 = mysql_real_escape_string($_POST['yarn']);
		$objEmbBilling->transport        = mysql_real_escape_string($_POST['transport']);
		$objEmbBilling->customerAccCode  = mysql_real_escape_string($_POST['customerAccCode']);
		$objEmbBilling->customerAccTitle = mysql_real_escape_string($_POST['customerAccTitle']);
		$objEmbBilling->third_party_code = mysql_real_escape_string($_POST['third_party_code']);

		$narration_emb .= "Bill#".$objEmbBilling->billNum." OGP#".$objEmbBilling->gate_pass_number;
		$narration_emb .= " ".$objEmbBilling->cloth." ".$objEmbBilling->yarn." ";

		if($objEmbBilling->third_party_code != ''){
			$salesAccCode = $objEmbBilling->third_party_code;
		}

		$lot_register_rows  					 = get_object_vars(json_decode($_POST['lot_register_rows']));
		$bill_rows  					 				 = get_object_vars(json_decode($_POST['bill_rows']));
		$deleted_rows									 = get_object_vars(json_decode($_POST['deleted_rows']));

		if($mainId == 0){
			$billExists = $objEmbBilling->checkBillDuplication($objEmbBilling->billNum,$mainId);
			 if(!$billExists){
				if($objEmbBilling->save()){
					$mainId = mysql_insert_id();
				}else{
					$mainId = 0;
				}
			 }
		}else{
			$message = 'Success! Embroidery Bill updated!';
		}
		if($mainId){
			$objEmbBilling->update($mainId);
			$objEmbBilling->outwd_id 				= $mainId;
			foreach($lot_register_rows as $key => $lotRow){
					//$lotRow->row_id    == emb_outward_details PRIMARY_INDEX OUTWD_DETL_ID
					$objEmbBilling->outward_lot_id  = mysql_real_escape_string($lotRow->row_id);
					$objEmbBilling->product_id 			= mysql_real_escape_string($lotRow->product_id);
					$objEmbBilling->lotNum 					= mysql_real_escape_string($lotRow->lot_no);
					$objEmbBilling->quality 				= mysql_real_escape_string($lotRow->quality);
					$objEmbBilling->measure 				= mysql_real_escape_string($lotRow->measure_id);
					$objEmbBilling->billing_type 		= mysql_real_escape_string($lotRow->billing_type);
					$objEmbBilling->stitches 				= mysql_real_escape_string($lotRow->stitches);
					$objEmbBilling->total_laces 		= mysql_real_escape_string($lotRow->total_laces);
					$objEmbBilling->length 					= mysql_real_escape_string($lotRow->measure_length);
					$objEmbBilling->embRate 				= mysql_real_escape_string($lotRow->emb_rate);
					$objEmbBilling->embAmount 			= mysql_real_escape_string($lotRow->emb_amount);
					$objEmbBilling->stitchRate 			= mysql_real_escape_string($lotRow->stitch_rate);
					$objEmbBilling->stitchAmount 		= mysql_real_escape_string($lotRow->stitch_amount);
					$objEmbBilling->stitchAccount 	= mysql_real_escape_string($lotRow->stitch_acc);
					$objEmbBilling->designNum 			= mysql_real_escape_string($lotRow->design_code);
					$objEmbBilling->machineNum 			= mysql_real_escape_string($lotRow->machine_id);
					$objEmbBilling->gatePassNumber 	= mysql_real_escape_string($lotRow->gp_no);
					$objEmbBilling->remarks 				= (isset($lotRow->remarks))?mysql_real_escape_string($lotRow->remarks):"";

					$outward_detail_id 						= $objEmbBilling->saveOutwardDetails();

					if($outward_detail_id){
						if(!isset($production_len[$objEmbBilling->product_id])){
							$production_len[$objEmbBilling->product_id] = $objEmbBilling->total_laces;
						}else{
							$production_len[$objEmbBilling->product_id] += $objEmbBilling->total_laces;
						}
					}
			}

			foreach($bill_rows as $key => $bill_row){
					//$bill_row->row_id    == emb_billing_details PRIMARY_INDEX ID
					$objEmbBilling->product_id 			= mysql_real_escape_string($bill_row->product_id);
					$objEmbBilling->lotNum 					= mysql_real_escape_string($bill_row->lot_no);
					$objEmbBilling->quality 				= mysql_real_escape_string($bill_row->quality);
					$objEmbBilling->measure 				= mysql_real_escape_string($bill_row->measure_id);
					$objEmbBilling->billing_type 		= mysql_real_escape_string($bill_row->billing_type);
					$objEmbBilling->stitches 				= mysql_real_escape_string($bill_row->stitches);
					$objEmbBilling->total_laces 		= mysql_real_escape_string($bill_row->total_laces);
					$objEmbBilling->length 					= mysql_real_escape_string($bill_row->measure_length);
					$objEmbBilling->embRate 				= mysql_real_escape_string($bill_row->emb_rate);
					$objEmbBilling->embAmount 			= mysql_real_escape_string($bill_row->emb_amount);
					$objEmbBilling->stitchRate 			= mysql_real_escape_string($bill_row->stitch_rate);
					$objEmbBilling->stitchAmount 		= mysql_real_escape_string($bill_row->stitch_amount);
					$objEmbBilling->stitchAccount 	= mysql_real_escape_string($bill_row->stitch_acc);
					$objEmbBilling->designNum 			= mysql_real_escape_string($bill_row->design_code);
					$objEmbBilling->machineNum 			= mysql_real_escape_string($bill_row->machine_id);
					$objEmbBilling->gatePassNumber 	= mysql_real_escape_string($bill_row->gp_no);
					$objEmbBilling->remarks 				= (isset($bill_row->remarks))?mysql_real_escape_string($bill_row->remarks):"";

					$updated_rec = $objEmbBilling->updateOutwardDetail($bill_row->row_id);

					if($updated_rec){
						if(!isset($production_len[$objEmbBilling->product_id])){
							$production_len[$objEmbBilling->product_id] = $objEmbBilling->total_laces;
						}else{
							$production_len[$objEmbBilling->product_id] += $objEmbBilling->total_laces;
						}
					}
			}
			foreach($deleted_rows as $key => $emb_billing_details_id){
				$objEmbBilling->deleteDetails($emb_billing_details_id);
			}
			if($mainId){
				$done = 1;
			}
			//get outward details - required
			$thisDate 				= $objEmbBilling->getOutwardDate($mainId);
			$outwardBillNum 	= $objEmbBilling->getBillNumber($mainId);
			$outwardDetail 		= $objEmbBilling->getDetailsById($mainId);
			$amountTotal  		= $objEmbBilling->getAmountSum($mainId);

			$customerAccCode 			= $objEmbBilling->getCutomerOfOutward($mainId);
			$customerAccTitle 		= $objEmbBilling->getAccountTitle($customerAccCode);
			$machineListIdAmount 	= $objEmbBilling->getMachineIdAmountList($mainId);

			$outward_voucher_id = $objEmbBilling->getVoucherId($mainId);

			foreach ($production_len as $product_id => $production) {
				$narration_emb .= $objEmbProducts->getTitle($product_id)."(".$production.") ";
			}

			if($outward_voucher_id == 0){
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->accountCode = $customerAccCode;
				$objJournalVoucher->voucherDate = $thisDate;
				$objJournalVoucher->voucherType = 'SV';
				$objJournalVoucher->status 			= 'P';
				$voucherCreated 								= $objJournalVoucher->saveVoucher();

				if($voucherCreated){
					$voucherId 		= $voucherCreated;
					$objEmbBilling->insertVoucherId($mainId,$voucherId);
					$objJournalVoucher->voucherId 			= $voucherId;

					$objJournalVoucher->accountCode 		= $customerAccCode;
					$objJournalVoucher->accountTitle 		= $customerAccTitle;
					$objJournalVoucher->narration 			= $narration_emb;
					$objJournalVoucher->transactionType = "Dr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $salesAccCode;
					$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($salesAccCode);
					$objJournalVoucher->narration 	    = $narration_emb;
					$objJournalVoucher->transactionType = "Cr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();
					$done = 1;
				}else{
					$done = 0;
				}
			}else{
				$voucherStatus = $objJournalVoucher->getVoucherStatus($outward_voucher_id);
				$objJournalVoucher->reverseVoucherDetails($outward_voucher_id);

				$objJournalVoucher->voucherDate    		= $thisDate;
				$objJournalVoucher->updateVoucherDate($outward_voucher_id);
				if($outward_voucher_id){
					$voucherId = $outward_voucher_id;
					//make Transaction
					$objJournalVoucher->voucherId 			= $voucherId;

					$objJournalVoucher->accountCode 		= $customerAccCode;
					$objJournalVoucher->accountTitle 		= $customerAccTitle;
					$objJournalVoucher->narration 			= $narration_emb;
					$objJournalVoucher->transactionType = "Dr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $salesAccCode;
					$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($salesAccCode);
					$objJournalVoucher->narration 	    = $narration_emb;
					$objJournalVoucher->transactionType = "Cr";
					$objJournalVoucher->amount 					= $amountTotal;
					$objJournalVoucher->saveVoucherDetail();

					$done = 1;
				}else{
					$done = 0;
				}
			}
		}
		if($done == 1){
			echo $message;
		}else{
			echo "Error Occured while completing process!";
		}
	}
?>
