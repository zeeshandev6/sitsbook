<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-sale.php');
	include('../common/classes/mobile-sale-details.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/items.php');
	include('../common/classes/settings.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts        		= new ChartOfAccounts();
	$objScanSale   	 				= new ScanSale();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objJournalVoucher  		= new JournalVoucher();
	$objItems 							= new Items();
	$objConfigs         		= new Configs();
	$objUserAccounts   	    = new UserAccounts();

	$send_sms_cond 			= $objConfigs->get_config('SMS');
	$admin_sms			 		= $objConfigs->get_config('ADMIN_SMS');
	$admin_message 		 	= $objConfigs->get_config('ADMIN_MSG');
	$invoice_msg    	 	= $objConfigs->get_config('INVOICE_MSG');

	$objScanSale->user_id 	= $_SESSION['classuseid'];

	$user_details        		= $objUserAccounts->getDetails($objScanSale->user_id);
	//JV Account Codes Used.
	$user_cashinhand_code   = ($user_details['CASH_IN_HAND'] == 'Y')?$user_details['CASH_ACC_CODE']:"0101010001";
	$user_cashinhand_title  = $objAccounts->getAccountTitleByCode($user_cashinhand_code);

	$inventoryAccCode  			= '0101060001';
	$inventoryAccTitle 			= $objAccounts->getAccountTitleByCode($inventoryAccCode);

	$other_income_acc      	= '0401020001';
	$other_income_title    	= $objAccounts->getAccountTitleByCode($other_income_acc);;

	$salesAccCode 	 	 			= '0201010001';
	$salesAccTitle   	 			= $objAccounts->getAccountTitleByCode($salesAccCode);

	$taxAccCode    		 			= "0401050001";
	$taxAccTitle   		 			= $objAccounts->getAccountTitleByCode($taxAccCode);

	$discountAccCode    		= "0301010001";
	$discountAccTitle   		= $objAccounts->getAccountTitleByCode($discountAccCode);

	$costAccCode      			= '0301040001';
	$costAccTitle     			= $objAccounts->getAccountTitleByCode($costAccCode);

	$returnData = array('MSG'=>'Nothing Happened');
	$success    = array();

	if(isset($_POST['jSonString'])){
		$objScanSaleDetails->ss_id 	    = (int)(mysql_real_escape_string($_POST['scan_sale_id']));
		$objScanSale->sale_date 	  		= date('Y-m-d',strtotime($_POST['sale_date']));
		$objScanSale->record_time 			= date('Y-m-d h:i:s');
		$objScanSale->bill_no		  			= ($objScanSaleDetails->ss_id > 0)?mysql_real_escape_string($_POST['bill_no']):$objScanSale->genBillNumber();
		$objScanSale->cust_acc_code	  	= mysql_real_escape_string($_POST['customer_acc_code']);
		$objScanSale->customer_name	  	= mysql_real_escape_string($_POST['customer_name']);
		$objScanSale->customer_mobile 	= mysql_real_escape_string($_POST['customer_mobile']);
		$objScanSale->sale_discount   	= (isset($_POST['sale_discount']))?mysql_real_escape_string($_POST['sale_discount']):0;
		$objScanSale->total_amount   		= mysql_real_escape_string($_POST['total_amount']);
		$objScanSale->received_cash   	= mysql_real_escape_string($_POST['received_cash']);
		$objScanSale->recovered_balance = mysql_real_escape_string($_POST['recovered_balance']);
		$objScanSale->recovery_amount   = (float)mysql_real_escape_string($_POST['recovery_amount']);
		$objScanSale->change_returned   = mysql_real_escape_string($_POST['change_returned']);
		$objScanSale->remaining_amount  = (float)mysql_real_escape_string($_POST['remaining_amount']);
		$objScanSale->delivery_charges  = (float)mysql_real_escape_string($_POST['delivery_charges']);
		$objScanSale->card_charges  		= (float)mysql_real_escape_string($_POST['card_charges']);

		$sale_order_no   	= (int)mysql_real_escape_string($_POST['sale_order_no']);
		$objScanSaleDetails->sale_order_no = $sale_order_no;

		//($objScanSale->total_amount * $objScanSale->card_charges) / 100;

		$bill_discount 				  				= (isset($_POST['sale_discount']))?mysql_real_escape_string($_POST['sale_discount']):0;
		$bill_tax 				      				= (isset($_POST['bill_tax']))?mysql_real_escape_string($_POST['bill_tax']):0;

		$objScanSale->sale_discount     = $bill_discount;
		$objScanSale->sale_tax          = $bill_tax;

		//New Sale Voucher Variables
		$supplierAccCode  = $objScanSale->cust_acc_code;
		$supplierAccTitle = $objAccounts->getAccountTitleByCode($supplierAccCode);

		$jSonData = json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);
		$validated = true;
		$barcodes_array = array();
		$scanPurchaseDetailsArray = array();
		$costaSales    = 0;
		$sale_price    = 0;
		$customer_bill = 0;

		foreach($transactions as $t=>$tansaction){
			$sp_details = $objScanPurchaseDetails->getDetails($tansaction->sp_detail_id);
			$scanPurchaseDetailsArray[$tansaction->sp_detail_id] = $sp_details;
			$costaSales += $sp_details['SUB_TOTAL'];
			$sale_price += $tansaction->sale;
		}
		if($validated){
			if($objScanSaleDetails->ss_id==0){
				$objScanSaleDetails->ss_id = $objScanSale->save();

				if($objScanSaleDetails->ss_id){
					foreach($transactions as $t=>$tansaction){
						$objScanSaleDetails->sp_id			= $tansaction->sp_detail_id;
						$objScanSaleDetails->orig_price	= $tansaction->sale;
						$objScanSaleDetails->discount		= isset($tansaction->discount)?$tansaction->discount:0;
						$objScanSaleDetails->tax				= isset($tansaction->tax)?$tansaction->tax:0;
						$objScanSaleDetails->price			= $tansaction->price;
						$barcode_id = $objScanSaleDetails->save();
						if($barcode_id > 0){
							$objScanPurchaseDetails->set_status_by_spdid($tansaction->sp_detail_id, 'S');
							$sp_itemid = $scanPurchaseDetailsArray[$tansaction->sp_detail_id]['ITEM_ID'];
							$sp_itempr = $scanPurchaseDetailsArray[$tansaction->sp_detail_id]['PRICE'];
							$objItems->removeScanCost($sp_itemid,$sp_itempr);
						}
						$success[] = ($barcode_id)?true:false;
					}
				}
			}else{
				if($objScanSaleDetails->ss_id){
					$objScanSale->update($objScanSaleDetails->ss_id);
					$objScanSaleDetails->ss_id = $objScanSaleDetails->ss_id;
					$details_id_array = $objScanSaleDetails->getListIDArray($objScanSaleDetails->ss_id);
					$new_details_id_array = array();
					//get row ids to array
					foreach($transactions as $t=>$tansaction){
						$new_details_id_array[] = $tansaction->row_id;
					}
					//delete from db as well as not found in returning rows
					foreach($details_id_array as $t=>$id){
						if(!in_array($id, $new_details_id_array)){
							$ss_details = $objScanSaleDetails->getDetails($id);
							$sp_details = $objScanPurchaseDetails->getDetails($ss_details['SP_DETAIL_ID']);
							if($sp_details['STOCK_STATUS'] == 'S'){
								$delted = $objScanSaleDetails->delete($id);
								if($delted){
									$objScanPurchaseDetails->set_status_by_spdid($sp_details['ID'], 'A');
									$objItems->addScanCost($sp_details['ITEM_ID'],$sp_details['PRICE']);
								}
							}
						}
					}
					foreach($transactions as $t=>$tansaction){
						$row_id 												= $tansaction->row_id;
						$objScanSaleDetails->sp_id	    = $tansaction->sp_detail_id;
						$objScanSaleDetails->orig_price	= $tansaction->sale;
						$objScanSaleDetails->discount		= isset($tansaction->discount)?$tansaction->discount:0;
						$objScanSaleDetails->tax				= isset($tansaction->tax)?$tansaction->tax:0;
						$objScanSaleDetails->price	    = $tansaction->price;
						if($row_id == 0){
							$barcode_id = $objScanSaleDetails->save();
							if($barcode_id > 0){
								$objScanPurchaseDetails->set_status_by_spdid($tansaction->sp_detail_id, 'S');
								$sp_itemid = $scanPurchaseDetailsArray[$tansaction->sp_detail_id]['ITEM_ID'];
								$sp_itempr = $scanPurchaseDetailsArray[$tansaction->sp_detail_id]['PRICE'];
								$objItems->removeScanCost($sp_itemid,$sp_itempr);
							}
						}elseif($row_id > 0){
							$sp_details = $objScanPurchaseDetails->getDetails($row_id);
							$objScanSaleDetails->update($row_id);
							$barcode_id = $row_id;
						}
					}
				}
			}

			if(!in_array(false, $success)){
				if($objScanSaleDetails->ss_id){
					$objJournalVoucher->voucherId = (int)($objScanSale->getVoucherId($objScanSaleDetails->ss_id));

					$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
					$objJournalVoucher->voucherDate = $objScanSale->sale_date;
					$objJournalVoucher->voucherType = 'SV';
					if($objJournalVoucher->voucherId == 0){
						$objJournalVoucher->voucherId   = $objJournalVoucher->saveVoucher();
						$objScanSale->insertVoucherId($objScanSaleDetails->ss_id,$objJournalVoucher->voucherId);
					}else{
						if($objJournalVoucher->voucherId > 0){
							$objJournalVoucher->reverseVoucherDetails($objJournalVoucher->voucherId);
							$objJournalVoucher->updateVoucherDate($objJournalVoucher->voucherId);
						}
					}
					$voucher_id = $objJournalVoucher->voucherId;

					$billNumber						 = $objScanSale->bill_no;
					$sale_discount  			 = $objScanSale->sale_discount;
					$taxAmount  					 = $bill_tax;
					$charges						 	 = $objScanSale->delivery_charges;

					$cust_amount 					 = ($objScanSale->total_amount + $charges);

					$objScanSale->received_cash -= $objScanSale->change_returned;

					$objJournalVoucher->accountCode  		= $supplierAccCode;
					$objJournalVoucher->accountTitle 		= $supplierAccTitle;
					$objJournalVoucher->narration    		= (substr($supplierAccCode,0,6) == '010101')?"Cash Received Vide Bill# ".$billNumber:"Amount Recievable Vide Bill# ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       		= $objScanSale->total_amount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode  = $salesAccCode;
					$objJournalVoucher->accountTitle = $salesAccTitle;
					$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Sold Goods On Cash vide invoice no. ".$billNumber:"Sold Goods to ".$supplierAccTitle." Against Bill#".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       = ($objScanSale->total_amount - $charges) + $sale_discount - $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode  = $discountAccCode;
					$objJournalVoucher->accountTitle = $discountAccTitle;
					$objJournalVoucher->narration    = "Discount Allowed On Sales Bill # ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       = $sale_discount;
					if($sale_discount > 0){
						$objJournalVoucher->saveVoucherDetail();
					}

					//Sale Tax Credit
					$objJournalVoucher->accountCode  = $taxAccCode;
					$objJournalVoucher->accountTitle = $taxAccTitle;
					$objJournalVoucher->narration    = "Sales Tax Payable against bill #".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       = $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					//Charges Credit
					$objJournalVoucher->accountCode     = $other_income_acc;
					$objJournalVoucher->accountTitle    = $other_income_title;
					$objJournalVoucher->narration       = "Charged to customer against bill#".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = $charges;

					$objJournalVoucher->saveVoucherDetail();

					//If Previous Balance Recovered
					//if(($objScanSale->recovered_balance == 'Y') && ($objScanSale->received_cash > 0)){
					if($objScanSale->received_cash > 0 && substr($supplierAccCode,0,6) == '010104'){
						$objJournalVoucher->accountCode  	= $user_cashinhand_code;
						$objJournalVoucher->accountTitle 	= $user_cashinhand_title;
						$objJournalVoucher->narration    	= "Cash received against bill # ".$billNumber;
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       	= $objScanSale->received_cash;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->narration    = "Amount paid in cash for invoice no ".$billNumber;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $objScanSale->received_cash;

						$objJournalVoucher->saveVoucherDetail();
					}

					/*
					if(($objScanSale->recovered_balance == 'Y') && ($objScanSale->recovery_amount > 0)){
						$objJournalVoucher->accountCode  	= $user_cashinhand_code;
						$objJournalVoucher->accountTitle 	= $user_cashinhand_title;
						$objJournalVoucher->narration    	= "Balance Recovered";
						$objJournalVoucher->transactionType = 'Dr';
						$objJournalVoucher->amount       	= $objScanSale->recovery_amount;

						$objJournalVoucher->saveVoucherDetail();

						$objJournalVoucher->accountCode  = $supplierAccCode;
						$objJournalVoucher->accountTitle = $supplierAccTitle;
						$objJournalVoucher->transactionType = 'Cr';
						$objJournalVoucher->amount       = $objScanSale->recovery_amount;

						$objJournalVoucher->saveVoucherDetail();
					}
					*/

					//CostaSales
					$objJournalVoucher->accountCode  = $costAccCode;
					$objJournalVoucher->accountTitle = $costAccTitle;
					$objJournalVoucher->narration    = "Cost of Inventory Sold expensed against bill # ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       = $costaSales;

					if($costaSales > 0){
						$objJournalVoucher->saveVoucherDetail();
					}

					//CostaInventory
					$objJournalVoucher->accountCode  = $inventoryAccCode;
					$objJournalVoucher->accountTitle = $inventoryAccTitle;
					$objJournalVoucher->narration    = (substr($supplierAccCode,0,6) == '010101')?"Cost of Goods Sold vide invoice no. ".$billNumber:"Cost of Goods Sold to ".$supplierAccTitle." Against Bill# ".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       = $costaSales;

					$objJournalVoucher->saveVoucherDetail();

					$returnData['ID']  =  $objScanSaleDetails->ss_id;
					$returnData['OK']  = 'Y';
					$returnData['MSG'] = 'Bill # '.$objScanSale->bill_no.' Saved Successfully!';

					//New Sale Voucher __END__
				}
			}else{
				file_get_contents('deleteScanSale.php?id='.$objScanSaleDetails->ss_id);
				$returnData['OK'] = 'N';
				$returnData['MSG'] = 'Error! Cannot Save Data.';
			}
		}else{
			$returnData['OK'] = 'N';
		}
		echo json_encode($returnData);
	}
	mysql_close($con);
exit();
?>
