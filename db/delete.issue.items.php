<?php
	include('../common/db.connection.php');
	include('../common/classes/issue_items.php');
	include('../common/classes/issue_items_details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objIssueItems        		= new IssueItems();
	$objIssueItemDetails  		= new IssueItemDetails();
	$objJournalVoucher 	  		= new JournalVoucher();
	$objItems  		      		= new items();

	//delete sale
	if(isset($_POST['cid'])){
		$issue_id = mysql_real_escape_string($_POST['cid']);
		$issue_items_list = $objIssueItemDetails->getList($issue_id);
		$voucher_id       = $objIssueItems->getVoucherId($issue_id);
		$item_rows_array  = array();
		$ok = 'Y';
		if($ok = 'Y'){
			if(mysql_num_rows($issue_items_list)){
				while($item_row = mysql_fetch_assoc($issue_items_list)){
					$objItems->addStockValue($item_row['ITEM_ID'],$item_row['QUANTITY'],$item_row['TOTAL_COST']);
				}
			}
			$objIssueItems->delete($issue_id);
			$objIssueItemDetails->deleteCompleteByIssueId($issue_id);
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			echo json_encode(array('OK'=>'Y','MSG'=>'Record has been Deleted Successfully!'));
		}else{
			echo json_encode(array('OK'=>'N','MSG'=>'Error! Not enough stock.'));
		}
		exit();
	}
?>