<?php
	include('../common/db.connection.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sms_templates.php');

	$objVoucher 		 = new JournalVoucher();
	$objAccounts 		 = new ChartOfAccounts();
	$objSmsTemplates = new SmsTemplates();

	$message = array('ID'=>'0','MSG'=>'Error! ');

	if(isset($_POST['newJv'])){
		$success = array();
		$objVoucher->voucherId			= (int)mysql_real_escape_string($_POST['jv_id']);
		$objVoucher->voucherDate 		= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->voucherType 		= 'JV';
		$objVoucher->type_num    		= 0;
		$objVoucher->jVoucherNum 		= $objVoucher->genJvNumber();
		$objVoucher->reference 			= $_POST['jvRef'];
		$objVoucher->reference_date = date('Y-m-d',strtotime($_POST['refDate']));
		$objVoucher->order_taker_id = (int)mysql_real_escape_string($_POST['order_taker_id']);
		$objVoucher->salesman_id    = (int)mysql_real_escape_string($_POST['salesman_id']);

		$transactions = $_POST['transactions'];
		$transactionz = json_decode($transactions);
		$transactionz = get_object_vars($transactionz);

		$deleted_transactions = $_POST['deleted_transactions'];
		$deleted_transactions = json_decode($deleted_transactions);
		$deleted_transactions = get_object_vars($deleted_transactions);

		$prev_transaction_id_arr = array();
		if($transactionz == NULL){
			$message['MSG'] = 'No Transactions Found';
			exit();
		}

		if($objVoucher->voucherId == 0){

			$objVoucher->voucherId = $objVoucher->saveVoucher();
			$message['MSG'] = 'Voucher Saved Successfully!';
		}elseif($objVoucher->voucherId > 0){

			$objVoucher->reverseVoucherDetails($objVoucher->voucherId);
			$objVoucher->updateVoucherDate($objVoucher->voucherId);
			$objVoucher->updateVoucherOrderTakerAndSalesman($objVoucher->voucherId);
			$message['MSG'] = 'Voucher  Updated Successfully!';
		}else{

		}
		if($objVoucher->voucherId > 0){
			if($deleted_transactions){
				foreach ($deleted_transactions as $key => $voucher_detail_id) {
					$objVoucher->deleteTransaction($voucher_detail_id);
				}
			}
			foreach($transactionz as $key => $transaction){
				$objVoucher->accountCode  = $transaction->account_code;
				$objVoucher->accountTitle = mysql_real_escape_string($transaction->account_title);
				$transaction->narration   = mysql_real_escape_string(base64_decode($transaction->narration));
				$objVoucher->narration 	  = mysql_real_escape_string($transaction->narration);
				$objVoucher->instrumentNo = 0;
				$transaction->debit       = (float)str_ireplace(',','',$transaction->debit);
				$transaction->credit      = (float)str_ireplace(',','',$transaction->credit);

				if($transaction->debit > 0 && $transaction->credit == ''){
					$objVoucher->transactionType = 'Dr';
					$objVoucher->amount = $transaction->debit;
				}elseif($transaction->credit > 0 && $transaction->debit == ''){
					$objVoucher->transactionType = 'Cr';
					$objVoucher->amount = $transaction->credit;
				}
				$success[] = $objVoucher->saveVoucherDetail();
			}
		}else{
			$success[] = 0;
		}
		if(in_array(0, $success)){
			$message['MSG'] = 'Error! Voucher Could Not Be Saved!';
			echo json_encode($message);
			exit();
		}else{
			$message['ID']  = $objVoucher->voucherId;
			echo json_encode($message);
			exit();
		}
	}
	mysql_close($con);
	exit();
?>
