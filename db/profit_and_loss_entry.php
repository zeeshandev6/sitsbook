<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/ledger_report.php');
	include('../common/classes/settings.php');

	$objAccounts 	   = new ChartOfAccounts();
	$objJournalVoucher = new JournalVoucher();
	$objGeneralLedger  = new ledgerReport();
	$objConfigs        = new Configs();

	$returnArray = array('OK'=>'N','MSG'=>'');

	$pnl_account_code = '0501020001';
	$pnl_account_title= $objAccounts->getAccountTitleByCode($pnl_account_code);

	if(isset($_POST['entryDate'])){
		$entryDate = mysql_real_escape_string($_POST['entryDate']);
		$entryDate = date('Y-m-d',strtotime($entryDate));

		$exits = $objJournalVoucher->checkProfitAndLossEntry($entryDate,$pnl_account_code);

		if($exits){
			$returnArray['MSG'] = 'Warning! Profit &amp; Loss Entry Already Posted!';
		}

		$income = $objAccounts->getLevelThreeListByGeneral('02');
		$expense= $objAccounts->getLevelThreeListByGeneral('03');

		$incomeAmount = array();
		$expenseAmount = array();

		if(mysql_num_rows($income)){
			while($incomeMainRow = mysql_fetch_array($income)){
				$mains = $objAccounts->getAccountByCatAccCode($incomeMainRow['ACC_CODE']);
				if(mysql_num_rows($mains)){
					while($mainRow = mysql_fetch_array($mains)){
						if(!isset($incomeAmount[$mainRow['ACC_CODE']])){
							$incomeAmount[$mainRow['ACC_CODE']] = 0;
						}
						$ledgerBalance = $objGeneralLedger->getLastBalance($entryDate, $mainRow['ACC_CODE']);
						if($ledgerBalance == 0){
							continue;
						}
						$incomeAmount[$mainRow['ACC_CODE']] += $ledgerBalance;
					}
				}
			}
		}

		if(mysql_num_rows($expense)){
			while($expenseMainRow = mysql_fetch_array($expense)){
				$mains = $objAccounts->getAccountByCatAccCode($expenseMainRow['ACC_CODE']);
				if(mysql_num_rows($mains)){
					while($mainRow = mysql_fetch_array($mains)){
						if(!isset($expenseAmount[$mainRow['ACC_CODE']])){
							$expenseAmount[$mainRow['ACC_CODE']] = 0;
						}
						$ledgerBalance = $objGeneralLedger->getLastBalance($entryDate, $mainRow['ACC_CODE']);
						if($ledgerBalance == 0){
							continue;
						}
						$expenseAmount[$mainRow['ACC_CODE']] += $ledgerBalance;
					}
				}
			}
		}

		//create voucher entry
		$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
		$objJournalVoucher->voucherDate = $entryDate;
		$objJournalVoucher->reference   = 'PnL Entry';
		$objJournalVoucher->voucherType = 'PL';

		$objJournalVoucher->voucherId = $objJournalVoucher->saveVoucher();

		if(!$objJournalVoucher->voucherId){ //voucher id is zero
			$returnArray['MSG'] = 'Error! Cannot Create Profit &amp; Loss Entry!';
		}

		$totalExpense = 0;
		$totalRevenue = 0;

		if($objJournalVoucher->voucherId){ //voucher id is not zero
			if($expenseAmount != NULl){
				foreach ($expenseAmount as $account_code => $balance){
					if($balance == 0){
						continue;
					}

					if($balance < 0){
						$expense_transaction_type = 'Dr';
					}else{
						$expense_transaction_type = 'Cr';
					}

					//expenses
					//voucher id is set --- OK
					//voucher date is set --- OK
					$objJournalVoucher->accountCode  = $account_code;
					$objJournalVoucher->accountTitle = $objAccounts->getAccountTitleByCode($account_code);
					$objJournalVoucher->narration    = 'Closed To Profit And Loss A/c';
					$objJournalVoucher->transactionType= $expense_transaction_type;
					$objJournalVoucher->amount       = str_ireplace('-','', $balance);

					$objJournalVoucher->saveVoucherDetail();
					$totalExpense += $balance;
				}
			}

			if($incomeAmount != NULl){
				foreach ($incomeAmount as $account_code => $balance){
					if($balance == 0){
						continue;
					}

					if($balance < 0){
						$income_transaction_type = 'Cr';
					}else{
						$income_transaction_type = 'Dr';
					}

					//revenue
					//voucher id is set --- OK
					//voucher date is set --- OK
					$objJournalVoucher->accountCode  = $account_code;
					$objJournalVoucher->accountTitle = $objAccounts->getAccountTitleByCode($account_code);
					$objJournalVoucher->narration    = 'Closed To Profit And Loss A/c';
					$objJournalVoucher->transactionType= $income_transaction_type;
					$objJournalVoucher->amount       = str_ireplace('-','', $balance);

					$objJournalVoucher->saveVoucherDetail();
					$totalRevenue += $balance;
				}
			}

			if($totalRevenue > $totalExpense){
				$pnl_transaction_type = 'Cr';
				$pnl_amount = $totalRevenue - $totalExpense;
				$pnl_narration = 'Profit Income From Operations';
			}elseif($totalRevenue < $totalExpense){
				$pnl_transaction_type = 'Dr';
				$pnl_amount = $totalExpense - $totalRevenue;
				$pnl_narration = 'Loss Occured From Operations';
			}else{
				$pnl_transaction_type = 'Cr';
				$pnl_amount = 0;
				$pnl_narration = 'No Profit No Loss';
			}

			$objJournalVoucher->accountCode     = $pnl_account_code;
			$objJournalVoucher->accountTitle    = $pnl_account_title;
			$objJournalVoucher->narration       = $pnl_narration;
			$objJournalVoucher->transactionType = $pnl_transaction_type;
			$objJournalVoucher->amount          = $pnl_amount;

			$objJournalVoucher->saveVoucherDetail();
			$returnArray['OK'] = 'Y';
			$returnArray['MSG'] = 'Profit And Loss Entry Posted Successfully!';
		}
		$objConfigs->set_config($entryDate, 'PNL_DATE');
		echo json_encode($returnArray);
		mysql_close($con);
		exit();
	}
?>
