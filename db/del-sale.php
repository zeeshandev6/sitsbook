<?php
	include('../common/db.connection.php');
	include('../common/classes/sale.php');
	include('../common/classes/sale_details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/item_batch_stock.php');

	$objSale 		   		   				= new Sale();
	$objSaledetails    		   		= new SaleDetails();
	$objBranches  	            = new Branches();
	$objBranchStock             = new BranchStock();
	$objStockTransfer           = new StockTransfer();
	$objStockTransferDetail     = new StockTransferDetail();
	$objItemBatchStock 					= new ItemBatchStock();
	$objJournalVoucher 		   		= new JournalVoucher();
	$objItmes  		   		   			= new Items();

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock          = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->addStockValue($item_id,$quantity,$total_cost);
        }
    }
    function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock          = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->removeStockValue($item_id,$quantity,$total_cost);
        }
    }

	$prev_hit_stock = 'Y';
	//delete sale
	if(isset($_POST['cid'])){
		$sale_id = $_POST['cid'];

		$found_in_returns = $objSale->check_in_sale_returns($sale_id);
		if($found_in_returns){
			echo json_encode(array('OK'=>'N','MSG'=>'Error! Sale Return Exists Against This Invoice!'));
			mysql_close($con);
			exit();
		}
		$saleDetailList = $objSaledetails->getList($sale_id);
		$branch_id      = $objSale->getBranchId($sale_id);
		$voucher_id 	= $objSale->getVoucherId($sale_id);
		$prev_sale  	= $objSale->getDetail($sale_id);
		$success 		= array();
		$saleDetailList = $objSaledetails->getList($sale_id);



		if($prev_sale['WITH_TAX'] == 'Y' && $prev_sale['REGISTERED_TAX'] == 'N'){
			$prev_hit_stock = 'N';
		}

		if($prev_hit_stock=='Y'&&mysql_num_rows($saleDetailList)){
			while($row = mysql_fetch_array($saleDetailList)){
				$item_id  = $row['ITEM_ID'];
				$quantity = $row['QUANTITY'];
				$avg_price= $objItmes->getPurchasePrice($item_id);
				$total_avg_price = $avg_price*$quantity;
				add_branch_stock($item_id,$branch_id,$quantity,$total_avg_price);
				if($row['BATCH_NO']!=''){
					$objItemBatchStock->addStock($row['ITEM_ID'],$row['BATCH_NO'],$row['EXPIRY_DATE'],$row['QUANTITY']);
				}
			}
		}

		$objSale->delete($sale_id);
		$objSaledetails->deleteCompleteBill($sale_id);

		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		echo json_encode(array('OK'=>'Y','MSG'=>'Bill Deleted Successfully!'));
		mysql_close($con);
		exit();
	}
?>
