<?php
	include('../common/db.connection.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');


	$objItems            	   = new Items();
	$objItemCategory     	   = new itemCategory();
	$objBranches  	           = new Branches();
	$objBranchStock            = new BranchStock();
  $objStockTransfer          = new StockTransfer();
  $objStockTransferDetail    = new StockTransferDetail();

    function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock            = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->addStockValue($item_id,$quantity,$total_cost);
        }
    }
    function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock            = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->removeStockValue($item_id,$quantity,$total_cost);
        }
    }
	if(isset($_POST['cid'])){
		$transfer_id = (int)(mysql_real_escape_string($_POST['cid']));
        if($transfer_id > 0){
            $prev_id_array    = $objStockTransferDetail->getRowIdArray($transfer_id);
            foreach($prev_id_array as $key => $prev_id){
                $prev_detail = $objStockTransferDetail->getDetail($prev_id);
                add_branch_stock($prev_detail['ITEM_ID'],$prev_detail['FROM_BRANCH'],$prev_detail['QUANTITY'],$prev_detail['TOTAL_COST']);
                remove_branch_stock($prev_detail['ITEM_ID'],$prev_detail['TO_BRANCH'],$prev_detail['QUANTITY'],$prev_detail['TOTAL_COST']);
                $objStockTransferDetail->delete($prev_id);
            }
            $deleted = $objStockTransfer->delete($transfer_id);
            if($deleted){
                $returnData['OK'] = 'Y';
                $returnData['MSG'] = 'Success! Record deleted.';
            }else{
                $returnData['OK'] = 'N';
                $returnData['MSG'] = 'Error! Cannot delete record.';
            }
            $returnData['ID'] = $transfer_id;
            echo json_encode($returnData);
            mysql_close($con);
exit();
        }
	}
?>
