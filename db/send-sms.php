<?php
	include('../common/db.connection.php');
	include('../common/classes/sms_log.php');
	include('../common/classes/sms_templates.php');
	include('../common/classes/sitsbook_sms_api.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/customers.php');
	include('../common/classes/suppliers.php');
	include('../common/classes/settings.php');

	$objSmsLog 				 = new SmsLog();
	$objSmsTemplates 	 = new SmsTemplates();
	$objSitsbookSmsApi = new SitsbookSmsApi();
	$objJournalVoucher = new JournalVoucher();
	$objCustomers 		 = new Customers();
	$objSuppliers 		 = new suppliers();
	$objConfigs 			 = new Configs();

	$sms_user_name 		 = $objConfigs->get_config('USER_NAME');
	$admin_sms     		 = $objConfigs->get_config('ADMIN_SMS');

	if(isset($_POST['send_sms'])){
		$mobile_number = mysql_real_escape_string($_POST['mobile_number']);
		$sms_body 		 = mysql_real_escape_string($_POST['sms_body']);
		$no_of_sms 		 = mysql_real_escape_string($_POST['no_of_sms']);
		$acc_id 		 	 = mysql_real_escape_string($_POST['acc_id']);
		$objSitsbookSmsApi->postSoapRequest($sms_user_name,$admin_sms,$mobile_number,$sms_body,$no_of_sms,$acc_id);
	}
	mysql_close($con);
?>
