<?php
	include('../common/db.connection.php');
	include( '../common/classes/accounts.php' );
	include( '../common/classes/customers.php' );
	include( '../common/classes/suppliers.php' );
	include( '../common/classes/banks.php' );
	include( '../common/classes/payrolls.php' );



	$objAccounts = new ChartOfAccounts;
	$objCustomers= new Customers;
	$objSuppliers= new suppliers;
	$objPayrolls = new Payrolls;
	$objBanks    = new Banks();


	if(isset($_POST['acc_id'])){
		$acc_id = mysql_real_escape_string($_POST['acc_id']);
		$acc_title = mysql_real_escape_string($_POST['new_title']);
		$acc_code  = mysql_real_escape_string($_POST['acc_code']);
		$updated = $objAccounts->updateTitle($acc_id, $acc_title);
		if($updated){
			if(strlen($acc_code) > 6){
				if(substr($acc_code, 0,6) == '010104'){
					$objCustomers->setTitle($acc_code,$acc_title);
				}
				if(substr($acc_code, 0,6) == '040101'){
					$objSuppliers->setTitle($acc_code,$acc_title);
				}
				if(substr($acc_code, 0,6) == '030105'){
					$objPayrolls->setTitle($acc_code,$acc_title);
				}
				if(substr($acc_code, 0,6) == '010102'){
					$objBanks->setTitle($acc_code,$acc_title);
				}
			}

			echo json_encode(array('OK'=>'Y'));
		}else{
			echo json_encode(array('OK'=>'N'));
		}
	}
	mysql_close($con);
exit();
?>
