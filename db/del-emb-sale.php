<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_sale.php');
	include('../common/classes/emb_sale_details.php');
	include('../common/classes/j-voucher.php');

	$objEmbroiderySale     		= new EmbroiderySale();
	$objEmbroiderySaleDetails 	= new EmbroiderySaleDetails();
	$objJournalVoucher 			= new JournalVoucher();

	if(isset($_POST['cid'])){
		$sale_id 				= (int)mysql_real_escape_string($_POST['cid']);
		$voucher_id 			= $objEmbroiderySale->getVoucherId($sale_id);
		$objEmbroiderySale->delete($sale_id);
		$objEmbroiderySaleDetails->deleteCompleteBill($sale_id);
		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		echo json_encode(array('OK'=>'Y','MSG'=>'Bill Deleted Successfully!'));
		mysql_close($con);
		exit();
	}
?>
