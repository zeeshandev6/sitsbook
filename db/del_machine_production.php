<?php
	include('../common/db.connection.php');
	include('../common/classes/machine_production.php');
	include('../common/classes/machine_production_details.php');

	$objMachineProduction 			 = new MachineProduction();
	$objMachineProductionDetails = new MachineProductionDetails();

	if(isset($_POST['id'])){
		$id 					= (int)mysql_real_escape_string($_POST['id']);
		$objMachineProduction->delete($id);
		$objMachineProductionDetails->delete_report($id);
		echo json_encode(array('OKAY'=>'Y'));
		exit();
	}
?>
