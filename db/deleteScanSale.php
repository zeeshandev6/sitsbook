<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-sale.php');
	include('../common/classes/mobile-sale-details.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         		= new ChartOfAccounts();
	$objScanSale   	 				= new ScanSale();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objJournalVoucher   		= new JournalVoucher();
	$objItems 							= new Items();

	$ret = array();
	$ret['OKAY'] = 'N';

	if(isset($_POST['id'])){
		$id = mysql_real_escape_string($_POST['id']);
		$sp_detail_list = $objScanSaleDetails->getList($id);
		$stock_used = 0;
		$sp_id_array = array();
		if(mysql_num_rows($sp_detail_list)){
			while($row = mysql_fetch_array($sp_detail_list)){
				$returned = $objScanSaleDetails->checkForReturnedAgainstCurrentBill($row['ID']);
				$stock_used += ($returned)?1:0;
				$sp_id_array[] = $row['SP_DETAIL_ID'];
			}
		}
		if($stock_used == 0){
			foreach ($sp_id_array as $key => $sp_detail_id) {
				$objScanPurchaseDetails->set_status_by_spdid($sp_detail_id, 'A');
				$rowz = $objScanPurchaseDetails->getDetails($sp_detail_id);
				$objItems->addScanCost($rowz['ITEM_ID'],$rowz['PRICE']);
			}
			$voucher_id = $objScanSale->getVoucherId($id);
			$removed 		= $objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			if($removed){
				$objScanSale->delete($id);
				$objScanSaleDetails->deleteCompleteBill($id);
				$ret['MSG'] = 'Bill Deleted Successfully!';
				$ret['OKAY'] = 'Y';
			}else{
				$ret['MSG'] = 'Error! Bill Cant be deleted.';
				$ret['OKAY'] = 'N';
			}
		}else{
			$ret['MSG'] = 'Items Returned Against This Bill!';
			$ret['OKAY'] = 'N';
		}
		echo json_encode($ret);
	}
	mysql_close($con);
exit();
?>
