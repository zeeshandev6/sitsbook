<?php
	include('../common/db.connection.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/items.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/mobile-purchase-return.php');
	include('../common/classes/mobile-purchase-return-details.php');
	include('../common/classes/mobile-sale-return.php');
	include('../common/classes/mobile-sale-return-details.php');

	$objScanPurchase          	  = new ScanPurchase();
	$objScanPurchaseDetails       = new ScanPurchaseDetails();
	$objScanPurchaseReturn        = new ScanPurchaseReturn();
	$objScanPurchaseReturnDetails = new ScanPurchaseReturnDetails();
	$objScanSaleReturn        	  = new ScanSaleReturn();
	$objScanSaleReturnDetails 	  = new ScanSaleReturnDetails();
	$objJournalVoucher 			  		= new JournalVoucher();
	$objItems 					  				= new Items();

	if(isset($_POST['prid'])){
		$mr_id = mysql_real_escape_string($_POST['prid']);
		$mrd_list = $objScanPurchaseReturnDetails->getList($mr_id);
		$voucher_id = $objScanPurchaseReturn->getVoucherId($mr_id);

		if(mysql_num_rows($mrd_list)){
			while($row = mysql_fetch_array($mrd_list)){
				$sp_d  = $objScanPurchaseDetails->getDetails($row['MPDID']);
				$objItems->addScanCost($sp_d['ITEM_ID'],$sp_d['PRICE']);
				$objScanPurchaseDetails->set_status_by_spdid($row['MPDID'], 'A');
			}
		}

		$objScanPurchaseReturnDetails->deleteBill($mr_id);
		echo $objScanPurchaseReturn->delete($mr_id);

		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
	}
	if(isset($_POST['msid'])){
		$mr_id = mysql_real_escape_string($_POST['msid']);
		$mrd_list = $objScanSaleReturnDetails->getList($mr_id);
		$voucher_id = $objScanSaleReturn->getVoucherId($mr_id);
		if(mysql_num_rows($mrd_list)){
			while($row = mysql_fetch_array($mrd_list)){
				$sp_d  = $objScanPurchaseDetails->getDetails($row['MSDID']);
				$objItems->removeScanCost($sp_d['ITEM_ID'],$sp_d['PRICE']);
				$objScanPurchaseDetails->set_status($row['MSDID'], 'S');
			}
		}
		$objScanSaleReturnDetails->deleteBill($mr_id);
		echo $objScanSaleReturn->delete($mr_id);
		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
	}
	mysql_close($con);
exit();
?>
