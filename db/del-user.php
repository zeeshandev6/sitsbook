<?php
	include('../common/db.connection.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/accounts.php');

	$objUserAccounts = new UserAccounts;
	$objAccountCodes = new ChartOfAccounts;

	if(isset($_POST['id'])){
		$user_id = mysql_real_escape_string($_POST['id']);
		if($user_id == 1){
			echo json_encode(array('OKAY'=>'N','MSG'=>'Admin Account Cant Be Deleted'));
		}elseif($user_id > 1){
			$user_details = $objUserAccounts->getDetails($user_id);
			$num_rows     = $objAccountCodes->accountCheckInBooks($user_details['CASH_ACC_CODE']);
			if($num_rows == 0){
				if($objUserAccounts->delete($user_id)){
					$objAccountCodes->deleteAccCode($user_details['CASH_ACC_CODE']);
					echo json_encode(array('OKAY'=>'Y','MSG'=>'User Successfully Deleted!'));
				}else{
					echo json_encode(array('OKAY'=>'N','MSG'=>'Error Deleting User'));
				}
			}else{
				echo json_encode(array('OKAY'=>'N','MSG'=>'Error, Transaction History Exists against '.$user_details['FIRST_NAME']." ".$user_details['LAST_NAME']));
			}
		}
	}
	mysql_close($con);
exit();
?>
