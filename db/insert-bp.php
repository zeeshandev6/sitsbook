<?php
	include('../common/db.connection.php');
	include( '../common/classes/j-voucher.php' );
	include( '../common/classes/accounts.php' );
	include( '../common/classes/userAccounts.php' );

	$objVoucher = new JournalVoucher;
	$objAccounts = new ChartOfAccounts;
	$objUsers	= new UserAccounts;

	$returnData = array('OK'=>'N','MSG'=>'Nothing','ID'=>'0');
	$user_details = $objUsers->getDetails($user_id);
	if(isset($_POST['newJv'])){
		$success 											= array();
		$voucher_id										= mysql_real_escape_string($_POST['jv_id']);

		$objVoucher->voucherDate 			= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->type_num					= $objVoucher->genTypeNumber('BP');
		$objVoucher->voucherType    	= 'BP';
		$objVoucher->jVoucherNum 			= $objVoucher->genJvNumber();
		$objVoucher->reference 				= $_POST['jvRef'];
		$objVoucher->reference_date 	= date('Y-m-d',strtotime($_POST['refDate']));
		$bankAccCode 									= $_POST['bank_acc_code'];
		$bankAccTitle 							  = mysql_real_escape_string($objAccounts->getAccountTitleByCode($bankAccCode));

		if(!$objVoucher->checkVoucherByType($objVoucher->type_num,$objVoucher->voucherType) && $voucher_id == 0){
			$returnData = array('OK'=>'N','MSG'=>'Cash Payment Number Already Exists!','ID'=>'0');
			echo json_encode($returnData);
			mysql_close($con);
			exit();
		}
		if($voucher_id > 0){
			$objVoucher->reverseVoucherDetails($voucher_id);
			$objVoucher->updateVoucher($voucher_id);
			$objVoucher->updateVoucherDate($voucher_id);
			$objVoucher->voucherId = $voucher_id;
		}else{
			$objVoucher->voucherId = $objVoucher->saveVoucher();
		}
		$bank_amount = 0;
		$re_nar = '';

		$transactions_array 								= get_object_vars(json_decode($_POST['transactions']));

		if($objVoucher->voucherId){
			foreach($transactions_array as $key => $transaction){
				$objVoucher->accountCode 	 		= mysql_real_escape_string(trim($transaction->accCode));
				$objVoucher->accountTitle	 		= mysql_real_escape_string($objAccounts->getAccountTitleByCode($objVoucher->accountCode));
				$objVoucher->narration 	 	 		= mysql_real_escape_string(trim($transaction->narration));
				$objVoucher->amount 		   		= (float)(trim($transaction->credit));
				$objVoucher->transactionType 	= 'Dr';
				$success[] 										= $objVoucher->saveVoucherDetail();

				if($key > 0){
					$re_nar .= ', ';
				}
				$bank_amount       		  	+= $objVoucher->amount;
				$re_nar .= $objVoucher->accountTitle;
			}

			$objVoucher->accountCode 	   = $bankAccCode;
			$objVoucher->accountTitle 	 = mysql_real_escape_string($bankAccTitle);
			$objVoucher->narration 	 	 	 = mysql_real_escape_string("Payment to ".$re_nar);
			$objVoucher->transactionType = 'Cr';
			$objVoucher->amount   	  	 = $bank_amount;
			$success[]	 								 = $objVoucher->saveVoucherDetail();

			$voucher_id = $objVoucher->voucherId;
		}else{
			$returnData = array('OK'=>'N','MSG'=>'Error! cannot post Bank Payment Voucher.','ID'=>'0');
			$success[] = 0;
		}
		if(!in_array('0',$success)){
			$returnData = array('OK'=>'Y','MSG'=>'Bank Payment Saved Successfully!','ID'=>$voucher_id);
			echo json_encode($returnData);
		}else{
			$returnData = array('OK'=>'N','MSG'=>'Error! cannot post Bank Payment Voucher.','ID'=>'0');
			echo json_encode($returnData);
		}
	}
	mysql_close($con);
?>
