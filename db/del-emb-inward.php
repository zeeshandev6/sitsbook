<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_inward.php');

	$objInwards = new EmbroideryInward();

	if(isset($_POST['wid'])){
		$wid = mysql_real_escape_string($_POST['wid']);
		if($objInwards->checkInherited($wid)){
			echo true;
		}else{
			echo false;
		}
	}
	if(isset($_POST['cid'])){
		$cid 	 = (int)mysql_real_escape_string($_POST['cid']);
		$existed = $objInwards->existsInLotRegister($cid);
		if(!$existed){
			$result = $objInwards->delete($cid);
			if($result>0){
				echo 'Y';
			}else{
				echo 'N';
			}
		}else{
			echo 'N';
		}
	}
	if(isset($_POST['did'])){
		$did = mysql_real_escape_string($_POST['did']);
		$existed = $objInwards->DetailRowexistsInLotRegister($did);
		if(!$existed){
			echo $objInwards->deleteDetails($did);
		}else{
			echo "X";
		}
	}
?>
