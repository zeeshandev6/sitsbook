<?php
    include ('../common/db.connection.php');
    include ('../common/classes/accounts.php');
    include ('../common/classes/j-voucher.php');

    $objJournalVoucher  = new JournalVoucher();
    $objAccountCodes    = new ChartOfAccounts();


    if(isset($_POST['batch_cash_receipt'])){
        $data = json_decode($_POST['data']);
        print_r($data);
        foreach ($data as $key => $row) {
            $ref_type    = 'DSV';
            $ref_id      = (int)mysql_real_escape_string($row->dist_sale_id);
            $accountCode = mysql_real_escape_string($row->account_code);
            $amount      = (float)mysql_real_escape_string($row->receipt_amount);
            $date        = date('Y-m-d',strtotime($row->receipt_date));

            $objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
            $objJournalVoucher->voucherDate = $date;
            $objJournalVoucher->reference   = 'Cash Receipt against Invoice';
            $objJournalVoucher->voucherType = 'CR';
            $objJournalVoucher->ref_type    = $ref_type;
            $objJournalVoucher->ref_id      = $ref_id;
            
            $voucher_id = $objJournalVoucher->saveVoucher();

            $objJournalVoucher->voucherId           = $voucher_id;
            $objJournalVoucher->accountCode         = '0101010001';
            $objJournalVoucher->accountTitle        = mysql_real_escape_string($objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode));
            $objJournalVoucher->narration           = mysql_real_escape_string('Cash Received From ' . $objAccountCodes->getAccountTitleByCode($accountCode));
            $objJournalVoucher->transactionType     = "Dr";
            $objJournalVoucher->amount              = $amount;
            $objJournalVoucher->saveVoucherDetail();

            $objJournalVoucher->accountCode     = $accountCode;
            $objJournalVoucher->accountTitle    = mysql_real_escape_string($objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode));
            $objJournalVoucher->narration       = mysql_real_escape_string('Amount received in cash');
            $objJournalVoucher->transactionType = "Cr";
            $objJournalVoucher->amount          = $amount;
            $objJournalVoucher->saveVoucherDetail();
        }
        exit();
    }

    // get post parameters
    $ref_type    = 'DSV';
    $ref_id      = (int)mysql_real_escape_string($_POST['dist_sale_id']);
    $accountCode = mysql_real_escape_string($_POST['account_code']);
    $amount      = mysql_real_escape_string($_POST['receipt_amount']);
    $date        = date('Y-m-d',strtotime($_POST['receipt_date']));

    $objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
    $objJournalVoucher->voucherDate = $date;
    $objJournalVoucher->reference   = 'Cash Receipt against Invoice';
    $objJournalVoucher->voucherType = 'CR';
    $objJournalVoucher->ref_type    = $ref_type;
    $objJournalVoucher->ref_id      = $ref_id;
    
    $voucher_id = $objJournalVoucher->saveVoucher();

    $objJournalVoucher->voucherId           = $voucher_id;
    $objJournalVoucher->accountCode         = '0101010001';
    $objJournalVoucher->accountTitle        = mysql_real_escape_string($objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode));
    $objJournalVoucher->narration           = mysql_real_escape_string('Cash Received From ' . $objAccountCodes->getAccountTitleByCode($accountCode));
    $objJournalVoucher->transactionType     = "Dr";
    $objJournalVoucher->amount              = $amount;
    $objJournalVoucher->saveVoucherDetail();

    $objJournalVoucher->accountCode     = $accountCode;
    $objJournalVoucher->accountTitle    = mysql_real_escape_string($objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode));
    $objJournalVoucher->narration       = mysql_real_escape_string('Amount received in cash');
    $objJournalVoucher->transactionType = "Cr";
    $objJournalVoucher->amount          = $amount;
    $objJournalVoucher->saveVoucherDetail();

    if($voucher_id){
        $receipt_sum_amount = $objJournalVoucher->getTransactionSumByRef('0101010001','Dr',$ref_id,$ref_type);
        echo json_encode(array('MSG'=>'Cash Receipt Voucher saved Successfully.','RECEIPT_SUM'=>$receipt_sum_amount));
    }
?>