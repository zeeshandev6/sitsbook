<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/mobile-purchase-return.php');
	include('../common/classes/mobile-purchase-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	      = new ChartOfAccounts();
	$objScanPurchase          	  = new ScanPurchase();
	$objScanPurchaseDetails       = new ScanPurchaseDetails();
	$objScanPurchaseReturn   	  = new ScanPurchaseReturn();
	$objScanPurchaseReturnDetails = new ScanPurchaseReturnDetails();
	$objJournalVoucher   		  = new JournalVoucher();
	$objItems 					  = new Items();

	$inventoryReturnAccCode  = '0101060002';
	$inventoryReturnAccTitle = $objAccounts->getAccountTitleByCode($inventoryReturnAccCode);

	$salesTaxPayableAccCode  = '0401050001';
	$salesTaxPayableAccTitle = $objAccounts->getAccountTitleByCode($salesTaxPayableAccCode);

	$returnData = array('MSG'=>'Nothing Happened');
	$success = array();
	if(isset($_POST['jSonString'])){
		$objScanPurchaseReturn->mp_id 		= mysql_real_escape_string($_POST['purchase_id']);
		$objScanPurchaseReturn->return_date = date('Y-m-d',strtotime($_POST['return_date']));

		$purchase_bill_details = $objScanPurchase->getDetail($objScanPurchaseReturn->mp_id);

		$bill_number        = mysql_real_escape_string($_POST['bill_number']);
		$supplier_acc_code  = mysql_real_escape_string($_POST['supplier']);
		$supplier_acc_title = $objAccounts->getAccountTitleByCode($supplier_acc_code);
		$supplier_name      = $purchase_bill_details['SUPPLIER_NAME'];

		$jSonData = json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);

		$validated = true;
		///Validation
		foreach ($transactions as $key => $obj) {
			if($obj->pdid==0||$obj->price==0){
				$validated = false;
			}
		}

		if(!$validated){
			$returnData['MSG'] = 'There was an error in your form Please Recheck!';
			mysql_close($con);
exit();
		}

		$mr_id = $objScanPurchaseReturn->save();
		$bill_amount = 0;
		$tax_amount  = 0;
		if($mr_id){
			foreach ($transactions as $key => $obj) {
				$objScanPurchaseReturnDetails->mr_id  = $mr_id;
				$objScanPurchaseReturnDetails->mpd_id = $obj->pdid;
				$objScanPurchaseReturnDetails->price  = $obj->price;
				$mrd_id = $objScanPurchaseReturnDetails->save();
				if($mrd_id){
					$sp_dl  = $objScanPurchaseDetails->getDetails($obj->pdid);
					$tm = ($sp_dl['SUB_TOTAL']*$sp_dl['TAX']/100);
					$tax_amount  += $tm;
					$bill_amount += $obj->price;
					$objScanPurchaseDetails->set_status_by_spdid($obj->pdid, 'R');
					$objItems->removeScanCost($sp_dl['ITEM_ID'],$sp_dl['PRICE']);
				}
			}
		}

		if($mr_id){
			$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate = $objScanPurchaseReturn->return_date;
			//voucher number Saved and voucher id is set for voucher details
			$objJournalVoucher->voucherId   = $objJournalVoucher->saveVoucher();
			if($objJournalVoucher->voucherId){
				//supplier Debit Entry
				$objJournalVoucher->accountCode     = $supplier_acc_code;
				$objJournalVoucher->accountTitle    = $supplier_acc_title;
				$objJournalVoucher->narration       = 'Cash received on a/c of Goods Returned Against Bill # '.$bill_number;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount    	  	= $bill_amount;

				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();


				//Sales Tax Cr Entry
				$objJournalVoucher->accountCode     = $salesTaxPayableAccCode;
				$objJournalVoucher->accountTitle    = $salesTaxPayableAccTitle;
				$objJournalVoucher->narration       = 'Sales Tax Refunded Against Bill # '.$bill_number;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount    	  	= $tax_amount;

				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				$returned_to_person_name = (substr($supplier_acc_code, 0,6) == '040101')?$supplier_acc_title:$supplier_name;

				//Purchase Return Cr Entry
				$objJournalVoucher->accountCode     = $inventoryReturnAccCode;
				$objJournalVoucher->accountTitle    = $inventoryReturnAccTitle;
				$objJournalVoucher->narration       = 'Goods Returned to '.$returned_to_person_name.' Against Bill # '.$bill_number;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount    	  	= $bill_amount - $tax_amount;

				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				$objScanPurchaseReturn->insertVoucherId($mr_id, $objJournalVoucher->voucherId);
			}
		}
		$returnData['ID'] = $mr_id;
		if($mr_id){
			$returnData['MSG'] = 'Scan Returns Saved Successfully!';
		}else{
			$returnData['MSG'] = 'Error! Cannot Save Record!';
		}
		echo json_encode($returnData);
		mysql_close($con);
exit();
	}
?>
