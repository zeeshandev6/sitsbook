<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sale_return.php');
	include('../common/classes/sale_return_details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         		= new ChartOfAccounts;
	$objInventoryReturns        = new SaleReturn;
	$objInventoryReturnsDetails = new SaleReturnDetails;
	$objItems            		= new Items;
	$objItemCategory     		= new itemCategory;
	$objJournalVoucher   		= new JournalVoucher;

	if(isset($_POST['bill_num'])){
		$bill_num 	   = mysql_real_escape_string($_POST['bill_num']);
		$customer_code = mysql_real_escape_string($_POST['supplier_code']);
		$sale_id   = mysql_real_escape_string($_POST['sale_id']);
		if($customer_code == '' || $bill_num == 0 || $bill_num == ''){
			if($sale_id == 0){
				$sale_id   = $objInventoryReturns->getSaleId($customer_code,$bill_num);
				if($sale_id == 0){
					$inventoryDetails = $objInventoryReturnsDetails->getList($sale_id);
					$updation = true;
				}
			}else{
				$inventoryDetails = $objInventoryReturnsDetails->getList($sale_id);
				$updation = false;
			}
		}
	}
?>
<?php
	if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
		$counter = 1;
		while($invRow = mysql_fetch_array($inventoryDetails)){
			$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
			$itemStockOs = $objItems->getStock($invRow['ITEM_ID']);

			$transactionColored = ($invRow['QUANTITY'] > $itemStockOs)?"":""; // sale return
?>
                      <tr class="alt-row calculations transactions <?php echo $transactionColored; ?>" data-row-id='0'>
                          <td style="text-align:left;"
                          	class="itemName"
                              data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
					<?php echo $itemName; ?>
                          </td>
                          <td style="text-align:center;" class="quantity" data-limit="<?php echo $invRow['QUANTITY']; ?>" ><?php echo $invRow['QUANTITY']; ?></td>
                          <td style="text-align:center;" class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                          <td style="text-align:center;" class="discount"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                          <td style="text-align:right;" class="subAmount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                          <td style="text-align:center;" class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                          <td style="text-align:right;" class="taxAmount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                          <td style="text-align:right;" class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                          <td style="text-align:center;"> <?php echo $itemStockOs; ?> </td>
                          <td style="text-align:center;">
<?php
				if($updation){
?>
                          	<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
                              <input type="checkbox" id="check<?php echo $counter; ?>" class="rowCheckBox css-checkbox" value="" />
                              <label class="css-label" id="check<?php echo $counter; ?>" for="check<?php echo $counter; ?>" ></label>
<?php
				}
?>
                          </td>
                      </tr>
<?php
			$counter++;
		}
	}
	mysql_close($con);
exit();
?>
