<?php
	include('../common/db.connection.php');
	include('../common/classes/inventory.php');
	include('../common/classes/inventoryDetails.php');
	include('../common/classes/items.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/j-voucher.php');

	$objInventory 			= new inventory;
	$objInventorydetails 	= new inventory_details;
	$objJournalVoucher 		= new JournalVoucher;
	$objItmes  		   		= new Items;
	$objItemBatchStock 		= new ItemBatchStock();

	$return_data = array();

	//delete inventory
	if(isset($_POST['cid'])){
		$purchase_id = $_POST['cid'];

		$found_in_return = $objInventory->check_in_purchase_returns($purchase_id);
		if($found_in_return){
			$return_data['MSG'] = 'Error! Purchase returns exists against this bill.';
			$return_data['OK']  = 'N';
			echo json_encode($return_data);
			exit();
		}

		$inventoryDetailList = $objInventorydetails->getList($purchase_id);
		$voucher_id = $objInventory->getVoucherId($purchase_id);
		$success = array();
		if(mysql_num_rows($inventoryDetailList)){
			while($row = mysql_fetch_array($inventoryDetailList)){
				$item_id = $row['ITEM_ID'];
				$quantity = $row['STOCK_QTY'];
				$itemStock = $objItmes->getStock($item_id);
				if($itemStock >= $quantity){
					$success[] = 'Y';
				}else{
					$success[] = 'N';
				}
				if($row['BATCH_NO']!=''){
					$batch_stock = $objItemBatchStock->getItemBatchStock($row['ITEM_ID'],$row['BATCH_NO'],$row['EXPIRY_DATE']);
					if($batch_stock >= $row['STOCK_QTY']){
						$success[] = 'Y';
					}else{
						$success[] = 'N';
					}
				}
			}
		}

		if(!in_array('N',$success)){
			$inventoryDetailList = $objInventorydetails->getList($purchase_id);
			if(mysql_num_rows($inventoryDetailList)){
				while($row = mysql_fetch_array($inventoryDetailList)){
					$item_id    = $row['ITEM_ID'];
					$quantity   = $row['STOCK_QTY'];
					$cost_price = $row['SUB_AMOUNT'];
					$objItmes->removeStockValue($item_id,$quantity,$cost_price);
					if($row['BATCH_NO']!=''){
						$objItemBatchStock->removeStock($item_id,$row['BATCH_NO'],$row['EXPIRY_DATE'],$row['STOCK_QTY']);
					}
				}
			}
			$objInventory->delete($purchase_id);
			$objInventorydetails->deleteCompleteBill($purchase_id);

			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			$return_data['MSG'] = 'Record Deleted Successfully!';
			$return_data['OK']  = 'Y';
		}else{
			$return_data['MSG'] = 'Not Enough Stock!';
			$return_data['OK']  = 'N';
		}
		echo json_encode($return_data);
		exit();
	}
	mysql_close($con);
?>
