<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/emb_lot_register.php');

	$objJournalVoucher 			= new JournalVoucher();
	$objOutward 				= new outward();
	$objLotRegisterDetail 		= new EmbLotRegister();

	if(isset($_GET['wid'])){
		$wid = $_GET['wid'];
		if($objOutward->checkInherited($wid)){
			echo true;
		}else{
			echo false;
		}
	}
	if(isset($_GET['cid'])){
		$cid = $_GET['cid'];
		$voucher_id = $objOutward->getVoucherId($cid);
		if($voucher_id !== 0){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
		}
		$outward_detail_list = $objOutward->get_outward_detail_id_list($cid);
		if(mysql_num_rows($outward_detail_list)){
			while($detail_row = mysql_fetch_array($outward_detail_list)){
				$objLotRegisterDetail->statusByOutwardDetailId($detail_row['OUTWD_DETL_ID'],'C');
			}
		}
		echo $objOutward->delete($cid);
	}
	if(isset($_GET['did'])){
		$did = $_GET['did'];
		echo $objOutward->deleteDetails($did);
	}
?>
