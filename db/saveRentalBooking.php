<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/j-voucher.php');
	include ('../common/classes/rental_booking.php');
	include('../common/classes/items.php');

	$objAccounts       = new ChartOfAccounts();
	$objJournalVoucher = new JournalVoucher();
	$objRentalBooking  = new RentalBooking();
	$objItems 				 = new Items();

	$rent_dues_acc     		 = '010111002';
	$rent_dues_title   		 = $objAccounts->getAccountTitleByCode($rent_dues_acc);

	$cash_acc     				 = '0101010001';
	$cash_title   				 = $objAccounts->getAccountTitleByCode($cash_acc);

	$service_income_acc    = '0202010001';
	$service_income_title  = $objAccounts->getAccountTitleByCode($service_income_acc);

	$inventory_rent_acc    = '0101060003';
	$inventory_rent_title  = $objAccounts->getAccountTitleByCode($inventory_rent_acc);

	$inventory_acc    		 = '0101060001';
	$inventory_title    	 = $objAccounts->getAccountTitleByCode($inventory_acc);

	$total_cost = 0;

	if(isset($_POST['jSonString'])){
		$rental_id = mysql_real_escape_string($_POST['rental_id']);
		$objRentalBooking->user_id = $user_id;
		$objRentalBooking->title   = '';
		$objRentalBooking->description = mysql_real_escape_string($_POST['description']);
		if($_POST['booking_date_time'] == ""){
			$objRentalBooking->booking_time = date('Y-m-d H:i:s');
		}else{
			$objRentalBooking->booking_time = date("Y-m-d H:i:s",strtotime($_POST['booking_date_time']));
		}

		$objRentalBooking->booking_time   	= ($_POST['booking_date_time'] == "")?"0000-00-00":date("Y-m-d",strtotime($_POST['booking_date_time']));
		$objRentalBooking->delivery_time  	= ($_POST['delivery_time'] 		 == "")?"0000-00-00":date("Y-m-d",strtotime($_POST['delivery_time']));
		$objRentalBooking->returning_time 	= ($_POST['returning_time'] 	 == "")?"0000-00-00":date("Y-m-d",strtotime($_POST['returning_time']));

		$objRentalBooking->booking_status 	= 'N';
		$objRentalBooking->client_name 			= mysql_real_escape_string($_POST['clinet_name']);
		$objRentalBooking->client_mobile 		= mysql_real_escape_string($_POST['clinet_mobile']);
		$objRentalBooking->clinet_phone 		= '';
		$objRentalBooking->address 					= mysql_real_escape_string($_POST['client_address']);
		$objRentalBooking->total_price 			= mysql_real_escape_string($_POST['total_price']);
		$objRentalBooking->advance 					= mysql_real_escape_string($_POST['advance']);
		$objRentalBooking->remaining 				= mysql_real_escape_string($_POST['remaining']);
		$objRentalBooking->booking_order_no = mysql_real_escape_string($_POST['booking_order_no']);

		if($rental_id == 0){
			$rental_id  = $objRentalBooking->save();
			$update_status = true;
		}else{
			$objRentalBooking->updateMain($rental_id);
		}

		$jSonData 		= json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);

		$deleted_rows 		= json_decode($_POST['deleted_rows']);
		$deleted_rows     = get_object_vars($deleted_rows);

		if(isset($update_status)){
			$avail_states = array();
			foreach($transactions as $t=>$tansaction){
				$avail_states[] = isset($tansaction->order_status)?$tansaction->order_status:"Y";
			}
			if(in_array('Y',$avail_states)&&in_array('N',$avail_states)){
				$objRentalBooking->booking_status = 'P';
			}elseif(in_array('Y',$avail_states)&&!in_array('N',$avail_states)){
				$objRentalBooking->booking_status = 'O';
			}elseif(!in_array('Y',$avail_states)&&in_array('N',$avail_states)){
				$objRentalBooking->booking_status = 'D';
			}else{
				$objRentalBooking->booking_status = 'N';
			}
			$objRentalBooking->updateStatus($rental_id);
		}

		//get Each Transaction Detail and Save.
		foreach($transactions as $t=>$tansaction){
			$objRentalBooking->rental_booking_id = $rental_id;
			if($tansaction->item_type == 'I'){
				$objRentalBooking->item_id 					 = $tansaction->item_id;
				$objRentalBooking->service_id 			 = 0;
			}else{
				$objRentalBooking->item_id 					 = 0;
				$objRentalBooking->service_id 			 = $tansaction->item_id;
			}
			$objRentalBooking->quantity				   = $tansaction->quantity;
			$objRentalBooking->avail_status 		 = isset($tansaction->order_status)?$tansaction->order_status:"Y";
			$objRentalBooking->rent_status 		   = isset($tansaction->rent_status)?$tansaction->rent_status:"";

			$objRentalBooking->return_date   	 = "0000-00-00 00:00:00";
			$objRentalBooking->delivereddate   = "0000-00-00 00:00:00";

			if($tansaction->row_id==0){
				$tansaction->row_id  								   = $objRentalBooking->saveDetail();
			}else{
				$row = $objRentalBooking->getDetailDetail($tansaction->row_id);
				if($row['ORDERING_STATUS']=='N'&&$row['ITEM_ID']>0){
					$cost  = $objItems->getPurchasePrice($row['ITEM_ID']);
					$tcost = $row['QUANTITY']*$cost;
					$objItems->addStockValue($row['ITEM_ID'],$row['QUANTITY'],$tcost);
				}
				$objRentalBooking->update($tansaction->row_id);
			}

			if($tansaction->row_id>0&&$objRentalBooking->avail_status=='N'){
				if($tansaction->item_type == 'I'){
					$cost_price = $objItems->getPurchasePrice($objRentalBooking->item_id);
					$tcost 			= $cost_price*$objRentalBooking->quantity;
					$objItems->removeStockValue($objRentalBooking->item_id,$objRentalBooking->quantity,$tcost);
					$total_cost += $tcost;
				}
			}
		}
		foreach ($deleted_rows as $key => $row_id){
			$row = $objRentalBooking->getDetailDetail($row_id);
			$deletd = $objRentalBooking->deleteDetail($row_id);
			if($deletd&&$row['ORDERING_STATUS']=='N'){
				$cost  = $objItems->getPurchasePrice($row['ITEM_ID']);
				$tcost = $row['QUANTITY']*$cost;
				$objItems->addStockValue($row['ITEM_ID'],$row['QUANTITY'],$tcost);
			}
		}

		if($rental_id>0){
			$voucher_id 											   = $objRentalBooking->getVoucherId($rental_id);

			$objJournalVoucher->jVoucherNum      = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate      = date('Y-m-d',strtotime($objRentalBooking->booking_time));
			$objJournalVoucher->voucherType      = 'SV';
			$objJournalVoucher->reference        = 'Retnal Booking';
			$objJournalVoucher->reference_date   = date('Y-m-d',strtotime($objRentalBooking->booking_time));
			$objJournalVoucher->status	  	     = 'P';

			if($voucher_id==0){
				$voucher_id = $objJournalVoucher->saveVoucher();
				$objRentalBooking->insertVoucherId($rental_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id>0){
				$objJournalVoucher->voucherId       = $voucher_id;

				$objJournalVoucher->accountCode     = $rent_dues_acc;
				$objJournalVoucher->accountTitle    = $rent_dues_title;
				$objJournalVoucher->narration       = " Amount recoverable from ".$objRentalBooking->client_name." against booking order # ".$objRentalBooking->booking_order_no;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount          = $objRentalBooking->remaining;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $cash_acc;
				$objJournalVoucher->accountTitle    = $cash_title;
				$objJournalVoucher->narration       = " Amount received from ".$objRentalBooking->client_name." against booking order # ".$objRentalBooking->booking_order_no;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount          = $objRentalBooking->advance;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $service_income_acc;
				$objJournalVoucher->accountTitle    = $service_income_title;
				$objJournalVoucher->narration       = " Service against booking order # ".$objRentalBooking->booking_order_no." to ".$objRentalBooking->client_name;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount          = $objRentalBooking->total_price;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $inventory_rent_acc;
				$objJournalVoucher->accountTitle    = $inventory_rent_title;
				$objJournalVoucher->narration       = " Cost of inventory parked against booking order # ".$objRentalBooking->booking_order_no;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount          = $total_cost;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $inventory_acc;
				$objJournalVoucher->accountTitle    = $inventory_title;
				$objJournalVoucher->narration       = " Cost parked to ".$inventory_rent_title."against  booking order # ".$objRentalBooking->booking_order_no;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount          = $total_cost;

				$objJournalVoucher->saveVoucherDetail();

			}
		}
		echo $rental_id;
	}
	mysql_close($con);
	exit();
?>
