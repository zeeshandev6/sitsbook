<?php
	include('../common/db.connection.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/mobile-sale.php');
	include('../common/classes/mobile-sale-details.php');
	include('../common/classes/items.php');
	include('../common/classes/accounts.php');

	$objScanPurchase        = new ScanPurchase;
	$objScanPurchaseDetails = new ScanPurchaseDetails;
	$objScanSale			= new ScanSale;
	$objScanSaleDetails 	= new ScanSaleDetails;
	$objAccountCodes		= new ChartOfAccounts;
	$objItems       		= new Items;
	if(isset($_GET['b_code'])){
		$barcode = mysql_real_escape_string($_GET['b_code']);
		$purchase_details = $objScanPurchaseDetails->get_details_by_barcode($barcode, '');
		if($purchase_details != NULL){
			$item_detail  = $objItems->getRecordDetails($purchase_details['ITEM_ID']);
			$scanPurchase = $objScanPurchase->getDetail($purchase_details['SP_ID']);
			if(substr($scanPurchase['SUPP_ACC_CODE'], 0,6) == '010101'){
				$supplier_name = $scanPurchase['SUPPLIER_NAME'];
			}else{
				$supplier_name = $objAccountCodes->getAccountTitleByCode($scanPurchase['SUPP_ACC_CODE']);
			}
?>
				<div class="col-xs-10" style="margin: 0px auto;float: none;">
					<h4 class="text-left"><?php echo $item_detail['COMPANY']." - ".$item_detail['NAME']; ?></h4>
					<div class="panel panel-info">
						<div class="panel-heading">Purchase Details</div>
							<div class="col-xs-6 the_row">Supplier Title</div>
							<div class="col-xs-6 the_row"><?php echo $supplier_name; ?></div>
							<div class="clearfix"></div>

							<div class="col-xs-6 the_row">Purchase Date</div>
							<div class="col-xs-6 the_row"><?php echo date('d-M-Y',strtotime($scanPurchase['SCAN_DATE'])); ?></div>
							<div class="clearfix"></div>

							<div class="col-xs-6 the_row"><a href="mobile-purchase-details.php?id=<?php echo $purchase_details['SP_ID']; ?>">Invoice #</a></div>
							<div class="col-xs-6 the_row"><?php echo $scanPurchase['BILL_NO']; ?></div>
							<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-xs-10" style="margin: 0px auto;float: none;">
					<div class="panel panel-info">
						<div class="panel-heading">Sale Details</div>
<?php
					if($purchase_details['STOCK_STATUS'] == 'S'){
						$sale_details = $objScanSaleDetails->getDetailsBySpDetailID($purchase_details['ID']);
						$sale      	  = $objScanSale->getDetail($sale_details['SS_ID']);
						if(substr($sale['CUST_ACC_CODE'], 0,6) == '010101'){
							$customer_name = $sale['CUSTOMER_NAME'];
						}else{
							$customer_name = $objAccountCodes->getAccountTitleByCode($sale['CUST_ACC_CODE']);
						}
?>
						<div class="col-xs-6 the_row">Customer Title</div>
						<div class="col-xs-6 the_row"><?php echo $customer_name; ?></div>
						<div class="clearfix"></div>

						<div class="col-xs-6 the_row">Sale Date</div>
						<div class="col-xs-6 the_row"><?php echo date('d-M-Y',strtotime($sale['SALE_DATE'])); ?></div>
						<div class="clearfix"></div>

						<div class="col-xs-6 the_row"><a href="mobile-sale-details.php?id=<?php echo $sale['ID']; ?>">Invoice #</a></div>
						<div class="col-xs-6 the_row"><?php echo $sale['BILL_NO']; ?></div>
						<div class="clearfix"></div>
<?php
					}else{
?>
						<div class="col-xs-6 the_row">Item <?php echo ($purchase_details['STOCK_STATUS']=='R')?"was RETURNED TO SUPPLIER":"Is Available For Sale."; ?></div>
						<div class="clearfix"></div>
<?php
					}
?>
					</div>
					<div class="clearfix"></div>
				</div>
<?php
		}else{
?>
				<div class="col-xs-10" style="margin: 0px auto;float: none;">
					<div class="panel panel-info text-center">
						<div class="panel-heading">Purchase Details</div>
						<div class="col-xs-12 the_row">Item Not Found!</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
<?php
		}
	}
	mysql_close($con);
exit();
?>
