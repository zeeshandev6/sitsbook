<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/rental_booking.php');
	include('../common/classes/items.php');

	$objAccounts       = new ChartOfAccounts();
	$objJournalVoucher = new JournalVoucher();
	$objRentalBooking  = new RentalBooking();
	$objItems 				 = new Items();

	$inventory_rent_acc    = '0101060003';
	$inventory_rent_title  = $objAccounts->getAccountTitleByCode($inventory_rent_acc);

	$inventory_acc    		 = '0101060001';
	$inventory_title    	 = $objAccounts->getAccountTitleByCode($inventory_acc);

	if(isset($_POST['booking_status'])){
		$objRentalBooking->booking_status = mysql_real_escape_string($_POST['booking_status']);
		$rental_booking_id 								= (int)mysql_real_escape_string($_POST['row_id']);
		$rental_booking_details 					= $objRentalBooking->getDetail($rental_booking_id);
		$voucher_id     									= $rental_booking_details['RETURN_VOUCHER_ID'];

		$status_updated = $objRentalBooking->updateStatus($rental_booking_id);

		if($rental_booking_details['BOOKING_STATUS'] == 'R'&&$objRentalBooking->booking_status != 'R'){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			$rental_booking_item_list 				= $objRentalBooking->getRentalDetailListByRentalId($rental_booking_id);
			if(mysql_num_rows($rental_booking_item_list)){
				while($row = mysql_fetch_assoc($rental_booking_item_list)){
					$cost_price = $objItems->getPurchasePrice($row['ITEM_ID']);
					$item_cost  = $cost_price*$row['QUANTITY'];
					$objItems->removeStockValue($row['ITEM_ID'],$row['QUANTITY'],$item_cost);
				}
			}
		}

		if($status_updated&&$objRentalBooking->booking_status == 'R'){
			$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate = date("Y-m-d");
			$objJournalVoucher->reference   = '';
			$objJournalVoucher->voucherType = 'SV';
			if($voucher_id==0){
				$voucher_id = $objJournalVoucher->saveVoucher();
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id>0){
				$inventory_cost = $objRentalBooking->getBookingTotalCost($rental_booking_id);

				$objJournalVoucher->voucherId = $voucher_id;

				$objJournalVoucher->accountCode     = $inventory_acc;
				$objJournalVoucher->accountTitle    = mysql_real_escape_string($inventory_title);
				$objJournalVoucher->narration       = "Inventory Returned from rent";
				$objJournalVoucher->transactionType = "Dr";
				$objJournalVoucher->amount          = $inventory_cost;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $inventory_rent_acc;
				$objJournalVoucher->accountTitle    = mysql_real_escape_string($inventory_rent_title);
				$objJournalVoucher->transactionType = "Cr";
				$objJournalVoucher->amount          = $inventory_cost;

				$objJournalVoucher->saveVoucherDetail();

				$objRentalBooking->insertReturnVoucherId($rental_booking_id,$voucher_id);
			}
			$rental_booking_item_list 				= $objRentalBooking->getRentalDetailListByRentalId($rental_booking_id);
			if(mysql_num_rows($rental_booking_item_list)){
				while($row = mysql_fetch_assoc($rental_booking_item_list)){
					$cost_price = $objItems->getPurchasePrice($row['ITEM_ID']);
					$item_cost  = $cost_price*$row['QUANTITY'];
					$objItems->addStockValue($row['ITEM_ID'],$row['QUANTITY'],$item_cost);
				}
			}
		}
	}
	mysql_close($con);
	exit();
?>
