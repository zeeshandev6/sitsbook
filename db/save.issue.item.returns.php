<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/issue_items.php');
	include('../common/classes/issue_items_details.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/settings.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts         = new ChartOfAccounts();
	$objIssueItems       = new IssueItems();
	$objIssueItemDetails = new IssueItemDetails();
	$objCustomer 		 		 = new customers();
	$objItems            = new items();
	$objItemCategory     = new ItemCategory();
	$objJournalVoucher   = new JournalVoucher();
	$objConfigs          = new Configs();
	$objUserAccounts   	 = new UserAccounts();

	$user_details      = $objUserAccounts->getDetails($_SESSION['classuseid']);
	$disc_type 				 = $objConfigs->get_config('SHOW_DISCOUNT');
	$send_sms_cond 		 = $objConfigs->get_config('SMS');
	$admin_sms			 	 = $objConfigs->get_config('ADMIN_SMS');
	$admin_message 		 = $objConfigs->get_config('ADMIN_MSG');
	$invoice_msg    	 = $objConfigs->get_config('INVOICE_MSG');

	//JV Account Codes Used.
	$inventoryAccCode   = '0101060001';
	$inventoryAccTitle  = $objAccounts->getAccountTitleByCode($inventoryAccCode);
	$costOfSalesAccCode = '0301040001';

	$returnData 		= array('MSG'=>'Nothing Happened','ID'=>0,'TOM'=>'');
	$costaItem  		= 0;
	$costaItemPrev 	= 0;
	$totalCost      = 0;
	if(isset($_POST['jSonString'])){
		$issue_id   					  					= mysql_real_escape_string($_POST['issue_id']);
		$objIssueItems->issue_date    	  = date('Y-m-d',strtotime($_POST['issue_date']));
		$objIssueItems->charged_to   	  	= mysql_real_escape_string($_POST['expensed_to']);
		$objIssueItems->notes   		  		= mysql_real_escape_string($_POST['inv_notes']);

		$costOfSalesAccCode   			  = $objIssueItems->charged_to;

		if($issue_id == 0){
			$success = array();
			$issue_id  = (int)$objIssueItems->save();
			if($issue_id==0){
				$returnData['MSG'] = "--ERROR-- Record Could Not Be Saved.";
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				exit();
			}
			$objIssueItemDetails->issue_id = $issue_id;
			$jSonData = json_decode($_POST['jSonString']);
			$transactions = get_object_vars($jSonData);
			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$tansaction){

				$objIssueItemDetails->item_id 	 		  = $tansaction->item_id;
				$objIssueItemDetails->machine_id 	 	  = $tansaction->machine_id;
				$objIssueItemDetails->quantity 		    = $tansaction->quantity;
				$objIssueItemDetails->cost_price 	  	= $tansaction->unitPrice;
				$objIssueItemDetails->total_cost		  = $tansaction->totalAmount;

				$sale_detail_id = $objIssueItemDetails->save();

				if($sale_detail_id){
					$totalCost += $tansaction->totalAmount;
					$objItems->addStockValue($tansaction->item_id,$tansaction->quantity,$tansaction->totalAmount);
					$success[] = "Y";
				}else{
					$success[] = "N";
					$returnData['MSG'] = "ERROR! RECORD CANNOT BE SAVED.";
				}
			}
			//GetCostaSales

			$returnData['MSG'] = "Record Saved Successfully.";
			$returnData['ID'] = $objIssueItemDetails->issue_id;
		}elseif($issue_id > 0){
			$objIssueItems->update($issue_id);

			$success 					   = array();
			$objIssueItemDetails->issue_id = $issue_id;
			$jSonData 					   = json_decode($_POST['jSonString']);
			$transactions 				   = get_object_vars($jSonData);
			$newTransactions 			   = array();

			//get Previous Transactions
			$prevTransactions = $objIssueItemDetails->getListArrayIds($issue_id);
			//get New Transactions
			foreach($transactions as $t=>$tansaction){
				$newTransactions[] = $tansaction;
			}
			//New Transaction Id Array
			foreach($transactions as $t=>$tansaction){
				$newTransactionIds[] = $tansaction->row_id;
			}
			//delete transactions
			foreach($prevTransactions as $prev=>$prev_id){
				if(!in_array($prev_id,$newTransactionIds)){
					$prevDetail    = $objIssueItemDetails->getDetail($prev_id);
					$deleted = $objIssueItemDetails->delete($prev_id);
					if($deleted){
						$objItems->removeStockValue($prevDetail['ITEM_ID'],$prevDetail['QUANTITY'],$prevDetail['TOTAL_COST']);
					}
				}
			}
			//update previous transactions
			foreach($newTransactions as $new=>$tansaction){
				if(in_array($tansaction->row_id,$prevTransactions)){
					$prevDetail = $objIssueItemDetails->getDetail($tansaction->row_id);

					$objIssueItemDetails->item_id 	 		  = $tansaction->item_id;
					$objIssueItemDetails->machine_id 	 	  = $tansaction->machine_id;
					$objIssueItemDetails->quantity 		    = $tansaction->quantity;
					$objIssueItemDetails->cost_price 	  	= $tansaction->unitPrice;
					$objIssueItemDetails->total_cost		  = $tansaction->totalAmount;

					$sale_detail_id = $objIssueItemDetails->update($prevDetail['ID']);

					if($sale_detail_id){
						$objItems->removeStockValue($prevDetail['ITEM_ID'],$prevDetail['QUANTITY'],$prevDetail['TOTAL_COST']);
						$objItems->addStockValue($tansaction->item_id,$tansaction->quantity,$tansaction->totalAmount);
						$totalCost += $tansaction->totalAmount;
					}
				}
			}
			//get Each Transaction Detail and Save.
			foreach($newTransactions as $t=>$tansaction){
				if($tansaction->row_id == 0){
					$objIssueItemDetails->item_id 	 		  = $tansaction->item_id;
					$objIssueItemDetails->machine_id 	 	  = $tansaction->machine_id;
					$objIssueItemDetails->quantity 		    = $tansaction->quantity;
					$objIssueItemDetails->cost_price 	  	= $tansaction->unitPrice;
					$objIssueItemDetails->total_cost		  = $tansaction->totalAmount;

					$sale_detail_id = $objIssueItemDetails->save();

					if($sale_detail_id){
						$objItems->addStockValue($tansaction->item_id,$tansaction->quantity,$tansaction->totalAmount);
						$totalCost += $tansaction->totalAmount;
						$success[] = "Y";
					}else{
						$success[] = "N";
						$returnData['MSG'] = "ERROR! RECORD CANNOT BE SAVED.";
					}
				}
			}

			$returnData['MSG'] = "Record Updated Successfully.";
			$returnData['ID']  = $objIssueItemDetails->issue_id;
		}

		if($returnData['ID']){
			$voucher_id = (int)($objIssueItems->getVoucherId($returnData['ID']));

			$objJournalVoucher->po_number 	= $objIssueItems->po_number;
			$objJournalVoucher->voucherDate = $objIssueItems->issue_date;

			if($voucher_id == 0){
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherType = 'SV';
				$voucher_id  				    				= $objJournalVoucher->saveVoucher();
				$objIssueItems->insertVoucherId($objIssueItemDetails->issue_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
				$objJournalVoucher->updateVoucherPoNumber($voucher_id);
			}

			if($voucher_id){
				$objJournalVoucher->voucherId = $voucher_id;

				$objJournalVoucher->accountCode     = $inventoryAccCode;
				$objJournalVoucher->accountTitle    = $inventoryAccTitle;
				$objJournalVoucher->narration       = "Cost of material received back in ".$objJournalVoucher->accountTitle.".";
				$objJournalVoucher->transactionType = "Dr";
				$objJournalVoucher->amount 					= $totalCost;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $costOfSalesAccCode;
				$objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($costOfSalesAccCode);
				$objJournalVoucher->narration       = "Cost of material returned to store.";
				$objJournalVoucher->transactionType = "Cr";
				$objJournalVoucher->amount 					= $totalCost;

				$objJournalVoucher->saveVoucherDetail();
			}
		}

		echo json_encode($returnData);
		exit();
	}
?>
