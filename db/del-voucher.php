<?php
	include('../common/db.connection.php');
	include '../common/classes/j-voucher.php';

	$objVouchers = new JournalVoucher();

	if(isset($_POST['v_id'])){
		$voucher_id = (int)$_POST['v_id'];
		$objVouchers->reverseVoucherDetails($voucher_id);
		$objVouchers->deleteJv($voucher_id);
	}
	$message = array('MSG'=>'Entries Could Not Be Deleted!');
	$message['DONE']   =  'N';
	if(isset($_POST['v'])){
		$vouchers = explode(',',$_POST['v']);
		foreach($vouchers as $key=>$voucher_id){
			if($voucher_id!==''){
				$objVouchers->reverseVoucherDetails($voucher_id);
				$objVouchers->deleteJv($voucher_id);
			}
		}
		$message['MSG'] = '<img src="resource/images/success.png" style="width:25px;" /> Vouchers Deleted Successfully!';
		$message['DONE']   =  'Y';
		echo json_encode($message);
	}
	mysql_close($con);
exit();
?>
