<?php
	include('../common/db.connection.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/wip.php');
	include('../common/classes/wip_issue_details.php');
	include('../common/classes/wip_produce_details.php');
	include('../common/classes/items.php');
  	include('../common/classes/branch_stock.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objUserAccounts                   = new UserAccounts();
	$objWorkInProcess 	 		 	   = new WorkInProgress();
	$objWorkInProcessConsumeDetails    = new WorkInProcessConsumeDetails();
	$objWorkInProcessProduceDetails    = new WorkInProcessProduceDetails();
	$objItems            		 	   = new Items();
	$objItemCategory     		 	   = new itemCategory();
	$objJournalVoucher   		 	   = new JournalVoucher();

	$user_details        = $objUserAccounts->getDetails($_SESSION['classuseid']);
	$branch_id 		     	 = $user_details['BRANCH_ID'];

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->addStockValue($item_id,$quantity,$total_cost);
      }
  }
  function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->removeStockValue($item_id,$quantity,$total_cost);
      }
  }
	if(isset($_POST['id'])){
		$wip_id = mysql_real_escape_string($_POST['id']);
		$voucher_id = $objWorkInProcess->get_voucher_id($wip_id);
		$wip_details= $objWorkInProcess->getRecordDetails($wip_id);
		if($wip_details['STATUS'] == 'P'){
			$thisWipDetailList = $objWorkInProcessConsumeDetails->getList($wip_id);
			$objWorkInProcess->delete($wip_id);
			if(mysql_num_rows($thisWipDetailList)){
				while($wip_row = mysql_fetch_array($thisWipDetailList)){
					$item_id 						= $wip_row['ITEM_ID'];
					$item_quantity 			= $wip_row['QUANTITY'];
					$wip_detail_id_this = $wip_row['ID'];

					add_branch_stock($item_id,$branch_id,$item_quantity,$wip_row['GROSS_COST']);
					$objWorkInProcessConsumeDetails->delete($wip_detail_id_this);
				}
			}
			$wip_produce_list = $objWorkInProcessProduceDetails->getList($wip_id);
			$objWorkInProcess->delete($wip_id);
			if(mysql_num_rows($wip_produce_list)){
				while($wip_row = mysql_fetch_array($wip_produce_list)){
					$item_id 					  = $wip_row['ITEM_ID'];
					$item_quantity 			= $wip_row['QUANTITY'];
					$wip_detail_id_this = $wip_row['ID'];

					remove_branch_stock($item_id,$branch_id,$item_quantity,$wip_row['GROSS_COST']);
					$objWorkInProcessProduceDetails->delete($wip_detail_id_this);
				}
			}
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			$returnData = array('MSG'=>'Process Removed Successfully!','OKAY'=>'Y');
		}else{
			$returnData = array('MSG'=>'Error! Process Cannot be Removed.','OKAY'=>'N');
		}
		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
