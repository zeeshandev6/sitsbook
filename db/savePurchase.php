<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/inventory.php');
	include('../common/classes/inventoryDetails.php');
	include('../common/classes/items.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts         	= new ChartOfAccounts();
	$objInventory        	= new inventory();
	$objInventoryDetails 	= new inventory_details();
	$objItems            	= new Items();
	$objItemBatchStock 		= new ItemBatchStock();
	$objItemCategory     	= new itemCategory();
	$objJournalVoucher   	= new JournalVoucher();
	$objUserAccounts   	 	= new UserAccounts();

	$user_details        	= $objUserAccounts->getDetails($_SESSION['classuseid']);
	$branch_id 		     	 	= $user_details['BRANCH_ID'];

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->addStockValue($item_id,$quantity,$total_cost);
      }
  }
  function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->removeStockValue($item_id,$quantity,$total_cost);
      }
  }
		//JV Account Codes Used.
		$taxAccCode             = '0101080001';
		$taxAccTitle            = $objAccounts->getAccountTitleByCode($taxAccCode);
		$inventoryAccCode       = '0101060001';
		$inventoryAccTitle      = $objAccounts->getAccountTitleByCode($inventoryAccCode);
		$purcahseDscountCode    = '0201010002';
		$purcahseDscountTitle   = $objAccounts->getAccountTitleByCode($purcahseDscountCode);
		$income_tax_acc 				= '0101080002';
		$income_tax_title 			= $objAccounts->getAccountTitleByCode($income_tax_acc);

		$returnData = array('MSG'=>'Nothing Happened','ID'=>0);

		if(isset($_POST['jSonString'])){
			$purchase_id   					 				 = (isset($_POST['purchase_id']))?mysql_real_escape_string($_POST['purchase_id']):"";
			$objInventory->purchaseDate      = date('Y-m-d',strtotime($_POST['purchaseDate']));
			$objInventory->billNum           = (isset($_POST['billNum']))?mysql_real_escape_string($_POST['billNum']):"";
			$objInventory->billNum           = ((int)$objInventory->billNum == 0)?$objInventory->genBillNumber():mysql_real_escape_string($objInventory->billNum);
			$objInventory->vendorBillNum     = (isset($_POST['vendorBillNum']))?mysql_real_escape_string($_POST['vendorBillNum']):"";
			$objInventory->supplierAccCode   = (isset($_POST['supplierCode']))?mysql_real_escape_string($_POST['supplierCode']):"";
			$objInventory->supplier_name     = (isset($_POST['supplier_name']))?mysql_real_escape_string($_POST['supplier_name']):"";
			$objInventory->po_number		 		 = (isset($_POST['po_number']))?mysql_real_escape_string($_POST['po_number']):"";
			$objInventory->gpNum		   	 		 = (isset($_POST['gp_num']))?mysql_real_escape_string($_POST['gp_num']):"";
			$objInventory->batch_no		   	 	 = (isset($_POST['batch_no']))?mysql_real_escape_string($_POST['batch_no']):"";
			$objInventory->charges   	   	 	 = (isset($_POST['charges']))?mysql_real_escape_string($_POST['charges']):"";
			$objInventory->discount   	     = (isset($_POST['discount']))?mysql_real_escape_string($_POST['discount']):"";
			$objInventory->total_discount    = (isset($_POST['total_discount']))?mysql_real_escape_string($_POST['total_discount']):"";
			$objInventory->discount_type     = (isset($_POST['disc_type']))?mysql_real_escape_string($_POST['disc_type']):"";
			$objInventory->inv_notes	   	 	 = (isset($_POST['inv_notes']))?mysql_real_escape_string($_POST['inv_notes']):"";
			$objInventory->income_tax        = (isset($_POST['income_tax']))?mysql_real_escape_string($_POST['income_tax']):"";
			$objInventory->unregistered_tax  = (isset($_POST['unregistered_tax']))?mysql_real_escape_string($_POST['unregistered_tax']):"";
			$objInventory->subject 			 		 = (isset($_POST['subject']))?mysql_real_escape_string($_POST['subject']):"";

			if(isset($_POST['deleted_rows'])){
				$deleted_rows = json_decode($_POST['deleted_rows']);
				$deleted_rows = get_object_vars($deleted_rows);
				if(count($deleted_rows)){
					foreach($deleted_rows as $key => $prev_id){
						$prevDetail = $objInventoryDetails->getDetails($prev_id);
						$objItems->removeStockValue($prevDetail['ITEM_ID'],$prevDetail['STOCK_QTY'],$prevDetail['SUB_AMOUNT']);
						if($prevDetail['BATCH_NO']!=''){
							$objItemBatchStock->removeStock($prevDetail['ITEM_ID'],$prevDetail['BATCH_NO'],$prevDetail['EXPIRY_DATE'],$prevDetail['STOCK_QTY']);
						}
						$objInventoryDetails->delete($prev_id);
					}
				}
			}
			if($purchase_id == 0){
				$success                          = array();
				$purchase_id                      = $objInventory->save();
				$objInventoryDetails->purchase_id = $purchase_id;

				$jSonData                         = json_decode($_POST['jSonString']);
				$transactions                     = get_object_vars($jSonData);

				//get Each Transaction Detail and Save.
				foreach($transactions as $t=>$tansaction){
					$objInventoryDetails->item_id 	 		   = $tansaction->item_id;
					$objInventoryDetails->cartons 		     = $tansaction->cartons;
					$objInventoryDetails->per_carton 		   = $tansaction->per_carton;
					$objInventoryDetails->qtyReceipt 		   = $tansaction->quantity;
					$objInventoryDetails->unitPrice 	  	 = $tansaction->unitPrice;
					$objInventoryDetails->purchaseDiscount = $tansaction->discount;
					$objInventoryDetails->subTotal			   = $tansaction->subAmount;
					$objInventoryDetails->taxRate			  	 = $tansaction->taxRate;
					$objInventoryDetails->taxAmount			   = $tansaction->taxAmount;
					$objInventoryDetails->totalAmount		   = $tansaction->totalAmount;
					if(isset($tansaction->batch_no)){
						$objInventoryDetails->batch_no		   = $tansaction->batch_no;
						$objInventoryDetails->expiry_date		 = date('Y-m-d',strtotime($tansaction->expiry_date));
					}
					$purchase_detail_id = $objInventoryDetails->save();
					//Update Correspoding item Stock.
					if($purchase_detail_id){
						$objItems->addStockValue($objInventoryDetails->item_id,$objInventoryDetails->qtyReceipt,$objInventoryDetails->subTotal);
						if(isset($tansaction->batch_no)){
							$objItemBatchStock->addStock($objInventoryDetails->item_id,$objInventoryDetails->batch_no,$objInventoryDetails->expiry_date,$objInventoryDetails->qtyReceipt);
						}
					}
					$success[] = ($purchase_detail_id)?"Y":"N";
				}
				//delete the false bill if there is an error!
				if(in_array('N',$success)){
					$purchaseDetailsArray = $objInventory->getPurchaseDetailArrayIdOnly($objInventoryDetails->purchase_id);
					foreach($purchaseDetailsArray as $p => $purchase_detail_id){
						$purchaseDetails = $objInventoryDetails->getDetails($purchase_detail_id);
						$objItems->removeStockValue($purchaseDetails['ITEM_ID'],$purchaseDetails['STOCK_QTY'],$purchaseDetails['SUB_AMOUNT']);
						$objInventoryDetails->delete($purchase_detail_id);
					}

					$voucher_id = $objInventory->getVoucherId($purchase_id);
					$objJournalVoucher->reverseVoucherDetails($voucher_id);

					$returnData['MSG'] = "Bill Could Not Be Saved!";
					$returnData['ID']  = 0;
					echo json_encode($returnData);
					mysql_close($con);
					exit();
				}elseif(!in_array('N',$success)){
					$returnData['MSG']  = "Bill#".$objInventory->billNum." Saved Successfully.";
					$returnData['ID']   = $objInventoryDetails->purchase_id;
				}
			}elseif($purchase_id > 0){
				$objInventory->update($purchase_id);

				$success                          = array();
				$objInventoryDetails->purchase_id = $purchase_id;
				$jSonData                         = json_decode($_POST['jSonString']);
				$transactions                     = get_object_vars($jSonData);
				$newTransactions                  = array();
				$newTransactionIds                = array();

				//get Previous Transactions
				$prevTransactions = $objInventory->getPurchaseDetailArrayIdOnly($purchase_id);
				//get New Transactions
				foreach($transactions as $t=>$tansaction){
					$newTransactions[] = $tansaction;
				}
				//New Transaction Id Array
				foreach($transactions as $t=>$tansaction){
					$newTransactionIds[] = $tansaction->row_id;
				}
				//update previous transactions
				foreach($newTransactions as $new=>$tansaction){
					if(in_array($tansaction->row_id,$prevTransactions)){
						$prevDetail = $objInventoryDetails->getDetails($tansaction->row_id);

						$objInventoryDetails->item_id 	 		  	= $tansaction->item_id;
						$objInventoryDetails->cartons 		    	= $tansaction->cartons;
						$objInventoryDetails->per_carton 		  	= $tansaction->per_carton;
						$objInventoryDetails->qtyReceipt 		  	= $tansaction->quantity;
						$objInventoryDetails->unitPrice 	  		= $tansaction->unitPrice;
						$objInventoryDetails->purchaseDiscount	= $tansaction->discount;
						$objInventoryDetails->subTotal			  	= $tansaction->subAmount;
						$objInventoryDetails->taxRate			  		= $tansaction->taxRate;
						$objInventoryDetails->taxAmount			  	= $tansaction->taxAmount;
						$objInventoryDetails->totalAmount		  	= $tansaction->totalAmount;
						if(isset($tansaction->batch_no)){
							$objInventoryDetails->batch_no		   	= $tansaction->batch_no;
							$objInventoryDetails->expiry_date		  = date('Y-m-d',strtotime($tansaction->expiry_date));
						}
						$purchase_detail_id = $objInventoryDetails->update($prevDetail['ID']);

						//Update Correspoding item Stock.
						if($purchase_detail_id){
							$objItems->removeStockValue($prevDetail['ITEM_ID'],$prevDetail['STOCK_QTY'],$prevDetail['SUB_AMOUNT']);
							if(isset($prevDetail['BATCH_NO'])){
								$objItemBatchStock->removeStock($prevDetail['ITEM_ID'],$prevDetail['BATCH_NO'],$prevDetail['EXPIRY_DATE'],$prevDetail['STOCK_QTY']);
							}
							$objItems->addStockValue($objInventoryDetails->item_id,$objInventoryDetails->qtyReceipt,$objInventoryDetails->subTotal);
							if(isset($tansaction->batch_no)){
								$objItemBatchStock->addStock($objInventoryDetails->item_id,$objInventoryDetails->batch_no,$objInventoryDetails->expiry_date,$objInventoryDetails->qtyReceipt);
							}
						}
					}
				}

				//get Each Transaction Detail and Save.
				foreach($newTransactions as $t=>$tansaction){
					if($tansaction->row_id == 0){
						$objInventoryDetails->item_id 	 		  		= $tansaction->item_id;
						$objInventoryDetails->cartons 		      	= $tansaction->cartons;
						$objInventoryDetails->per_carton 		  		= $tansaction->per_carton;
						$objInventoryDetails->qtyReceipt 		  		= $tansaction->quantity;
						$objInventoryDetails->unitPrice 	  	  	= $tansaction->unitPrice;
						$objInventoryDetails->purchaseDiscount    = $tansaction->discount;
						$objInventoryDetails->subTotal			  		= $tansaction->subAmount;
						$objInventoryDetails->taxRate			  			= $tansaction->taxRate;
						$objInventoryDetails->taxAmount			  		= $tansaction->taxAmount;
						$objInventoryDetails->totalAmount		  		= $tansaction->totalAmount;
						if(isset($tansaction->batch_no)){
							$objInventoryDetails->batch_no		   	  = $tansaction->batch_no;
							$objInventoryDetails->expiry_date		 		= date('Y-m-d',strtotime($tansaction->expiry_date));
						}
						$purchase_detail_id = $objInventoryDetails->save();
						//Update Correspoding item Stock.
						if($purchase_detail_id){
							$objItems->addStockValue($objInventoryDetails->item_id,$objInventoryDetails->qtyReceipt,$objInventoryDetails->totalAmount);
							if(isset($tansaction->batch_no)){
								$objItemBatchStock->addStock($objInventoryDetails->item_id,$objInventoryDetails->batch_no,$objInventoryDetails->expiry_date,$objInventoryDetails->qtyReceipt);
							}
						}
					}
				}
				$returnData['MSG']  = "Bill#".$objInventory->billNum." Updated Successfully.";
				$returnData['ID']   = $objInventoryDetails->purchase_id;
			}
			if($purchase_id>0){
				$voucher_id 			  = $objInventory->getVoucherId($purchase_id);
				$taxAmount          = $objInventory->getInventoryTaxAmountSum($objInventoryDetails->purchase_id);
				$billAmount         = $objInventory->getInventoryAmountSum($objInventoryDetails->purchase_id);
				$supplierAccTitle   = $objAccounts->getAccountTitleByCode($objInventory->supplierAccCode);

				$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($objInventory->purchaseDate));
				$objJournalVoucher->voucherType    = 'PV';
				$objJournalVoucher->reference      = 'Inventory Purchase';
				$objJournalVoucher->reference_date = date('Y-m-d',strtotime($objInventory->purchaseDate));
				$objJournalVoucher->status	  	   = 'P';

				$pdisc_amount = (float)$objInventory->discount;

				$income_tax   = (float)$objInventory->income_tax;

				$income_tax_amount = round((($billAmount - $pdisc_amount)*$income_tax)/100);

				if($voucher_id==0){
					$voucher_id = $objJournalVoucher->saveVoucher();
				}else{
					$objJournalVoucher->reverseVoucherDetails($voucher_id);
					$objJournalVoucher->updateVoucherDate($voucher_id);
				}

				if($voucher_id>0){
					$objJournalVoucher->voucherId       = $voucher_id;

					$objJournalVoucher->accountCode     = $inventoryAccCode;
					$objJournalVoucher->accountTitle    = $inventoryAccTitle;
					$objJournalVoucher->narration       = (substr($objInventory->supplierAccCode,0,6) == '010101')?"Goods Purchased On Cash Vide Bill# ".$objInventory->billNum:"Goods Purchased Vide Bill# ".$objInventory->billNum." From ".$supplierAccTitle;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = $billAmount;
					$objJournalVoucher->amount         -= $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $taxAccCode;
					$objJournalVoucher->accountTitle    = $taxAccTitle;
					$objJournalVoucher->narration       = "GST for purchase of inventory vide bill#".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $income_tax_acc;
					$objJournalVoucher->accountTitle    = $income_tax_title;
					$objJournalVoucher->narration       = "Income Tax for purchase of inventory vide bill#".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = (float)$income_tax_amount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $objInventory->supplierAccCode;
					$objJournalVoucher->accountTitle    = $supplierAccTitle;
					$objJournalVoucher->narration       = (substr($objInventory->supplierAccCode,0,6) == '010101')?"Cash Paid to ".$objInventory->supplier_name." against bill# ".$objInventory->billNum:"Amount payable against bill#".$objInventory->billNum;
					if($objInventory->subject!=''){
						$objJournalVoucher->narration     = $objInventory->subject;
					}
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = $billAmount - $pdisc_amount + $income_tax_amount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $purcahseDscountCode;
					$objJournalVoucher->accountTitle    = $purcahseDscountTitle;
					$objJournalVoucher->narration       = "Discount Earned on Purchases against bill # ".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = $pdisc_amount;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = '0301040002';
					$objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
					$objJournalVoucher->narration       = (substr($objInventory->supplierAccCode,0,6) == '010101')?"Delivery charges paid against bill # ".$objInventory->billNum:"Delivery charges payable to ".$supplierAccTitle."  against bill # ".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = $objInventory->charges;

					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $objInventory->supplierAccCode;
					$objJournalVoucher->accountTitle    = $supplierAccTitle;
					$objJournalVoucher->narration       = "Delivery charges against bill # ".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = $objInventory->charges;

					$objJournalVoucher->saveVoucherDetail();
				}
				$objInventory->insertVoucherId($objInventoryDetails->purchase_id,$voucher_id);
			}
			echo json_encode($returnData);
			mysql_close($con);
			exit();
		}
?>
