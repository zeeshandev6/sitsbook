<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/inventory-return.php');
	include('../common/classes/inventory-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         				= new ChartOfAccounts();
	$objInventoryReturns        = new InventoryReturn();
	$objInventoryReturnsDetails = new InventoryReturnDetails();
	$objItems           				= new Items();
	$objItemCategory     				= new itemCategory();
	$objJournalVoucher   				= new JournalVoucher();

	if(isset($_POST['bill_num'])){
		$bill_num 	   = mysql_real_escape_string($_POST['bill_num']);
		$supplier_code = mysql_real_escape_string($_POST['supplier_code']);
		$purchase_id   = mysql_real_escape_string($_POST['purchase_id']);
		if($supplier_code == '' || $bill_num == 0 || $bill_num == ''){
			if($purchase_id == 0){
				$purchase_id   = $objInventoryReturns->getPurchaseId($supplier_code,$bill_num);
				if($purchase_id == 0){
					$inventoryDetails = $objInventoryReturnsDetails->getList($purchase_id);
					$updation = true;
				}
			}else{
				$inventoryDetails = $objInventoryReturnsDetails->getList($purchase_id);
				$updation = false;
			}
		}
	}
?>
<?php
	if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
		$counter = 1;
		while($invRow = mysql_fetch_array($inventoryDetails)){
			$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
			$itemStockOs = $objItems->getStock($invRow['ITEM_ID']);
			$transactionColored = ($itemStockOs == 0)?"isRedRow":"";
			if($transactionColored != ''){
				$updation = false;
			}else{
				$updation = true;
			}
?>
                      <tr class="alt-row calculations transactions <?php echo $transactionColored; ?>" data-row-id='0'>
                          <td style="text-align:left;"
                          	class="itemName"
                              data-item-id='<?php echo $invRow['ITEM_ID']; ?>'>
					<?php echo $itemName; ?>
                          </td>
                          <td style="text-align:center;" class="quantity"><?php echo $invRow['STOCK_QTY']; ?></td>
                          <td style="text-align:center;" class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                          <td style="text-align:center;" class="discount"><?php echo $invRow['PURCHASE_DISCOUNT'] ?></td>
                          <td style="text-align:right;" class="subAmount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                          <td style="text-align:center;" class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                          <td style="text-align:right;" class="taxAmount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                          <td style="text-align:right;" class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                          <td style="text-align:center;"> <?php echo $itemStockOs; ?> </td>
                          <td style="text-align:center;">
<?php
				if($updation){
?>
                          	<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
                              <input type="checkbox" id="check<?php echo $counter; ?>" class="rowCheckBox css-checkbox" value="" />
                              <label class="css-label" id="check<?php echo $counter; ?>" for="check<?php echo $counter; ?>" ></label>
<?php
				}
?>
                          </td>
                      </tr>
<?php
			$counter++;
		}
	}
	mysql_close($con);
exit();
?>
