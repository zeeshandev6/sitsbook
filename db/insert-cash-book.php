<?php
	include('../common/db.connection.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');
	include('../common/classes/userAccounts.php');

	$objVoucher 				= new JournalVoucher();
	$objUserAccounts    = new UserAccounts();
	$objChartOfAccounts = new ChartOfAccounts();

	$message = array('ID'=>'0','MSG'=>'Error!');

	$sessionUserDetails = $objUserAccounts->getDetails($_SESSION['classuseid']);

	if($sessionUserDetails['CASH_IN_HAND'] == 'Y'){
		$cash_in_hand_acc_code  = $sessionUserDetails['CASH_ACC_CODE'];
	}else{
		$cash_in_hand_acc_code  = '0101010001';
	}

	$cash_in_hand_code  = $cash_in_hand_acc_code;
	$cash_in_hand_title = $objChartOfAccounts->getAccountTitleByCode($cash_in_hand_code);

	if(isset($_POST['newJv'])){
		$success = array();
		$objVoucher->voucherId		= (int)mysql_real_escape_string($_POST['jv_id']);
		$objVoucher->voucherDate 	= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->voucherType 	= 'CB';
		$objVoucher->type_num    	= 0;
		$objVoucher->jVoucherNum 	= $objVoucher->genJvNumber();
		$objVoucher->reference 		= $_POST['jvRef'];
		$objVoucher->reference_date = date('Y-m-d',strtotime($_POST['refDate']));

		$transactions = $_POST['transactions'];
		$transactionz = json_decode($transactions);
		$transactionz = get_object_vars($transactionz);

		$deleted_transactions = $_POST['deleted_transactions'];
		$deleted_transactions = json_decode($deleted_transactions);
		$deleted_transactions = get_object_vars($deleted_transactions);

		$prev_transaction_id_arr = array();
		if($transactionz == NULL){
			$message['MSG'] = 'No Transactions Found';
			exit();
		}

		if($objVoucher->voucherId == 0){
			//New Save
			$objVoucher->voucherId = $objVoucher->saveVoucher();
			$message['MSG'] = 'Voucher Saved Successfully!';
		}elseif($objVoucher->voucherId > 0){
			//Update


			$objVoucher->reverseVoucherDetails($objVoucher->voucherId);
			$objVoucher->updateVoucherDate($objVoucher->voucherId);
			$message['MSG'] = 'Voucher  Updated Successfully!';
		}else{
			//$success[] = 0;
		}
		if($objVoucher->voucherId > 0){
			if($deleted_transactions){
				foreach ($deleted_transactions as $key => $voucher_detail_id) {
					$objVoucher->deleteTransaction($voucher_detail_id);
				}
			}
			$cash_transaction_type = 'DR';
			foreach($transactionz as $key => $transaction){

				$transaction->narration   = mysql_real_escape_string(base64_decode($transaction->narration));
				$objVoucher->narration 	  = mysql_real_escape_string($transaction->narration);
				$transaction->debit       = (float)str_ireplace(',','',$transaction->debit);
				$transaction->credit      = (float)str_ireplace(',','',$transaction->credit);

				/*------------CASH DEBIT ENTRY--------------*/
				if($transaction->credit > 0 && $transaction->debit == ''){
					$objVoucher->accountCode  	 = $cash_in_hand_code;
					$objVoucher->accountTitle 	 = mysql_real_escape_string($cash_in_hand_title);
					$objVoucher->transactionType = 'Dr';
					$objVoucher->amount     		 = $transaction->credit;
					$objVoucher->cash_book_flag  = 'Y';
					$success[] 									 = $objVoucher->saveVoucherDetail();
				}
				/*------------CASH DEBIT ENTRY-----END---------*/

				/*------------ACCOUNT ENTRY--------------*/
				$objVoucher->accountCode  = $transaction->account_code;
				$objVoucher->accountTitle = mysql_real_escape_string($transaction->account_title);
				$objVoucher->instrumentNo = 0;
				if($transaction->debit > 0 && $transaction->credit == ''){
					$objVoucher->transactionType = 'Dr';
					$objVoucher->amount = $transaction->debit;
				}elseif($transaction->credit > 0 && $transaction->debit == ''){
					$objVoucher->transactionType = 'Cr';
					$objVoucher->amount = $transaction->credit;
				}
				$objVoucher->cash_book_flag  = '';
				$success[] = $objVoucher->saveVoucherDetail();
				/*------------ACCOUNT ENTRY---END-----------*/

				/*------------CASH CREDIT ENTRY--------------*/
				if($transaction->debit > 0 && $transaction->credit == ''){
					$objVoucher->accountCode  	 = $cash_in_hand_code;
					$objVoucher->accountTitle 	 = mysql_real_escape_string($cash_in_hand_title);
					$objVoucher->transactionType = 'Cr';
					$objVoucher->amount     		 = $transaction->debit;
					$objVoucher->cash_book_flag  = 'Y';
					$success[] = $objVoucher->saveVoucherDetail();
				}
				/*------------CASH CREDIT ENTRY---END-----------*/
			}
		}else{
			$success[] = 0;
		}
		if(in_array(0, $success)){
			$message['MSG'] = 'Error! Voucher Could Not Be Saved!';
			echo json_encode($message);
			exit();
		}else{
			$message['ID']  = $objVoucher->voucherId;
			echo json_encode($message);
			exit();
		}
	}
	mysql_close($con);
	exit();
?>
