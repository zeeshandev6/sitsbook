<?php
	include('../common/db.connection.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');

	$objVoucher  = new JournalVoucher();
	$objAccounts = new ChartOfAccounts();

	$message = array('ID'=>'0','MSG'=>'Error!');

	if(isset($_POST['newJv'])){
		$success = array();
		$objVoucher->voucherId		   = mysql_real_escape_string($_POST['jv_id']);
		$objVoucher->voucherDate 	   = date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->voucherType 	   = mysql_real_escape_string($_POST['jvType']);
		$objVoucher->type_num    	   = '0';
		$objVoucher->jVoucherNum 	   = $objVoucher->genJvNumber();
		$objVoucher->reference 		   = mysql_real_escape_string($_POST['jvRef']);
		$objVoucher->reference_date  = date('Y-m-d',strtotime($_POST['refDate']));
		$objVoucher->po_number 		   = mysql_real_escape_string($_POST['pon']);

		$transactions = $_POST['transactions'];
		$transactionz = json_decode($transactions);
		$transactionz = get_object_vars($transactionz);

		if($transactionz == NULL){
			$message['MSG'] = 'No Transactions Found';
			exit();
		}

		if($objVoucher->voucherId == 0){
			//New Save
			$objVoucher->voucherId = $objVoucher->saveVoucher();
			$message['MSG'] = 'Voucher Saved Successfully!';
		}elseif($objVoucher->voucherId > 0){
			//Update
			$objVoucher->reverseVoucherDetails($objVoucher->voucherId);
			$objVoucher->updateVoucherDate($objVoucher->voucherId);
			$objVoucher->updateVoucherPoNumber($objVoucher->voucherId);
			$message['MSG'] = 'Voucher  Updated Successfully!';
		}else{
			$success[] = 0;
		}

		if($objVoucher->voucherId > 0){
			foreach($transactionz as $key => $transaction){

				$objVoucher->accountCode  = $transaction->account_code;
				$objVoucher->accountTitle = mysql_real_escape_string($transaction->account_title);
				$objVoucher->narration 	  = mysql_real_escape_string($transaction->narration);

				if($transaction->debit > 0 && $transaction->credit == ''){
					$objVoucher->transactionType = 'Dr';
					$objVoucher->amount = $transaction->debit;
				}elseif($transaction->credit > 0 && $transaction->debit == ''){
					$objVoucher->transactionType = 'Cr';
					$objVoucher->amount = $transaction->credit;
				}
				$success[] = $objVoucher->saveVoucherDetail();
			}
		}else{
			$success[] = 0;
		}
		if(in_array(0, $success)){
			$objVoucher->reverseVoucherDetails($objVoucher->voucherId);
			$objVoucher->deleteJv($objVoucher->voucherId);
			$message['MSG'] = 'Error! Voucher Could Not Be Saved!';
			echo json_encode($message);
			exit();
		}else{
			$message['ID']  = $objVoucher->voucherId;
			echo json_encode($message);
			exit();
		}
	}
?>
