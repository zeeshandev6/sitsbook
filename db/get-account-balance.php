<?php
	include('../common/db.connection.php');
	include '../common/classes/ledger_report.php';

	$objLedgerReport = new ledgerReport;

	$today = date('Y-m-d');

	if(isset($_POST['supplierAccCode'])){
		$postedAccCode = mysql_real_escape_string($_POST['supplierAccCode']);

		if($postedAccCode == '' || $postedAccCode == 0){
			$json_array['BALANCE'] = '';
			echo json_encode($json_array);
			exit();
		}
		$ledgerOpeningBalance = $objLedgerReport->getLastBalance($today,$postedAccCode);
		$account_type = substr($postedAccCode,0,2);

		if($ledgerOpeningBalance >= 0 && ($account_type == '01' || $account_type == '03')){
			$ledgerOpeningBalanceType = 'Dr';
		}elseif($ledgerOpeningBalance < 0 && ($account_type == '01' || $account_type == '03')){
			$ledgerOpeningBalanceType = 'Cr';
		}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance >= 0){
			$ledgerOpeningBalanceType = 'Cr';
		}elseif(($account_type == '02' || $account_type == '04' || $account_type == '05') && $ledgerOpeningBalance < 0){
			$ledgerOpeningBalanceType = 'Dr';
		}
		$ledgerOpeningBalanceBefore = $ledgerOpeningBalance;

		$ledgerOpeningBalance = str_replace('-','',$ledgerOpeningBalance);

		$json_array = array();
		$json_array['BALANCE'] = $ledgerOpeningBalance." ".strtoupper($ledgerOpeningBalanceType);
		$json_array['AMOUNT']  = $ledgerOpeningBalanceBefore;
		echo json_encode($json_array);
	}
	mysql_close($con);
exit();
?>
