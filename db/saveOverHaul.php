<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/overhaul.php');
	include('../common/classes/overhaul_details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         		 = new ChartOfAccounts();
	$objOverHaul 	 		 = new OverHaul();
	$objOverHaulDetails 	 = new OverHaulDetails();
	$objItems            		 = new Items();
	$objItemCategory     		 = new itemCategory();
	$objJournalVoucher   		 = new JournalVoucher();

	$inv_acc_code = '0101060001';
	$wip_acc_code = '010112001';
	$foh_acc_code = '0401040002';

	$inv_acc_title = $objAccounts->getAccountTitleByCode($inv_acc_code);
	$wip_acc_title = $objAccounts->getAccountTitleByCode($wip_acc_code);
	$foh_acc_title = $objAccounts->getAccountTitleByCode($foh_acc_code);

	$returnData = array('MSG'=>'Nothing Happened','ID'=>0);

	if(isset($_POST['jSonString'])){
		$oh_id   = mysql_real_escape_string($_POST['oh_id']);
		$objOverHaul->started_on          = date('Y-m-d',strtotime($_POST['started_on_date']));
        $objOverHaul->labour_account      = mysql_real_escape_string($_POST['labour_account']);
		$objOverHaul->labour         	  = (float)mysql_real_escape_string($_POST['labour']);
        $objOverHaul->overhead_account    = mysql_real_escape_string($_POST['overhead_account']);
		$objOverHaul->overhead            = (float)mysql_real_escape_string($_POST['overhead']);
		$objOverHaul->result_item         = mysql_real_escape_string($_POST['resulting_item_id']);
		$objOverHaul->result_quantity     = mysql_real_escape_string($_POST['resulting_item_qty']);
		$objOverHaul->status   		   = 'P';

		$success = array();
		$total_item_cost= 0;

		if($objOverHaul->result_item == ''){
			$returnData = array('MSG'=>'Resulting Product Is Not Selected!','ID'=>0);
			echo json_encode($returnData);
			exit();
		}
		if($objOverHaul->result_quantity == '' || $objOverHaul->result_quantity == 0){
			$returnData = array('MSG'=>'Resulting Product Quantity Not Defined!','ID'=>0);
			echo json_encode($returnData);
			exit();
		}
		if($oh_id == 0){
			$succes_message = 'Record Saved Successfully!';
			$oh_id  = $objOverHaul->save();
			if($oh_id){
				$objOverHaulDetails->oh_id = $oh_id;
			}else{
				$succes_message = 'Error! Saving Record!';
			}
		}elseif($oh_id > 0){
			$succes_message = 'Record Updated Successfully!';
			$updated = $objOverHaul->update($oh_id);
		}else{
			$returnData = array('MSG'=>'Error Saving Record!','ID'=>0);
			echo json_encode($returnData);
			exit();
		}
		if($oh_id > 0){
			//reverse Issued Items
 			$prevWipDetailList = $objOverHaulDetails->getList($oh_id);
			if(mysql_num_rows($prevWipDetailList)){
				while($wip_row = mysql_fetch_array($prevWipDetailList)){
					$item_id 			= $wip_row['ITEM_ID'];
					$item_quantity 		= $wip_row['QUANTITY'];
					$wip_detail_id_that = $wip_row['ID'];
					$gross_cost    		= $wip_row['GROSS_COST'];

					$objItems->addStockValue($item_id,$item_quantity,$gross_cost);
					$objOverHaulDetails->delete($wip_detail_id_that);
				}
			}
			$objOverHaulDetails->oh_id = $oh_id;
		}
		$jSonData 	  = json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);

		if($oh_id){
			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$tansaction){
				$objOverHaulDetails->item_id 	 		  = $tansaction->item_id;
				$objOverHaulDetails->quantity 		  = $tansaction->quantity;
				$ip_price 									  = $tansaction->cost_price;
				$objOverHaulDetails->gross_cost         = $objOverHaulDetails->quantity*$ip_price;
				$wip_detail_id = $objOverHaulDetails->save();

				$total_item_cost += $objOverHaulDetails->gross_cost;

				//Update Correspoding item Stock.
				if($wip_detail_id){
					$objItems->removeStockValue($objOverHaulDetails->item_id,$objOverHaulDetails->quantity,$objOverHaulDetails->gross_cost);
				}
				$success[] = ($wip_detail_id)?"Y":"N";
			}
			if(in_array('N',$success)){
				$thisWipDetailList = $objOverHaulDetails->getList($oh_id);
				$objOverHaul->delete($oh_id);
				if(mysql_num_rows($thisWipDetailList)){
					while($wip_row = mysql_fetch_array($thisWipDetailList)){
						$item_id 			= $wip_row['ITEM_ID'];
						$item_quantity 		= $wip_row['QUANTITY'];
						$wip_detail_id_this = $wip_row['ID'];
						$gross_cost  		= $wip_row['GROSS_COST'];

						$objItems->addStockValue($item_id,$item_quantity,$gross_cost);
						$objOverHaulDetails->delete($wip_detail_id_this);
					}
				}
				$returnData = array('MSG'=>'Error Occured While Saving!','ID'=>0);
				echo json_encode($returnData);
				exit();
			}else{
				$voucher_id = $objOverHaul->get_voucher_id($oh_id);
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate = $objOverHaul->started_on;
				$objJournalVoucher->voucherType = 'PV';

				if($voucher_id > 0){
					$objJournalVoucher->reverseVoucherDetails($voucher_id);
					$objJournalVoucher->updateVoucherDate($voucher_id);
				}else{
					$voucher_id = $objJournalVoucher->saveVoucher();
				}
				$objJournalVoucher->voucherId = $voucher_id;
				if($objJournalVoucher->voucherId){

					$objJournalVoucher->accountCode  = $wip_acc_code;
					$objJournalVoucher->accountTitle = $wip_acc_title;
					$objJournalVoucher->narration    = "Costing for job # ".$oh_id;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       = $total_item_cost + $objOverHaul->labour + $objOverHaul->overhead;

					if($objJournalVoucher->amount > 0){
						$objJournalVoucher->saveVoucherDetail();
					}

					$objJournalVoucher->accountCode  = $inv_acc_code;
					$objJournalVoucher->accountTitle = $inv_acc_title;
					$objJournalVoucher->narration    = "Inventory cost charged to wip job # ".$oh_id;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       = $total_item_cost;

					if($objJournalVoucher->amount > 0){
						$objJournalVoucher->saveVoucherDetail();
					}

                    if($objOverHaul->labour_account != ''){
                        $objJournalVoucher->accountCode     = $objOverHaul->labour_account;
                        $objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
                        $objJournalVoucher->narration       = "External Expenses charged to wip job # ".$oh_id;
                        $objJournalVoucher->transactionType = 'Cr';
                        $objJournalVoucher->amount          = $objOverHaul->labour;
                        $objJournalVoucher->saveVoucherDetail();
                    }

                    if($objOverHaul->overhead_account != ''){
                        $objJournalVoucher->accountCode     = $objOverHaul->overhead_account;
                        $objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
                        $objJournalVoucher->narration       = "Internal Expenses charged to wip job # ".$oh_id;
                        $objJournalVoucher->transactionType = 'Cr';
                        $objJournalVoucher->amount          = $objOverHaul->overhead;
                        $objJournalVoucher->saveVoucherDetail();
                    }

					$objOverHaul->set_voucher_id($oh_id,$objJournalVoucher->voucherId);
				}
				$returnData = array('MSG'=>$succes_message,'ID'=>$oh_id);
				echo json_encode($returnData);
			}
		}
	}
	mysql_close($con);
exit();
?>
