<?php
	include('../common/db.connection.php');
	include('../common/classes/inventory-return.php');
	include('../common/classes/inventory-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/j-voucher.php');

	$objInventory 			 = new InventoryReturn();
	$objInventorydetails = new InventoryReturnDetails();
	$objJournalVoucher 	 = new JournalVoucher();
	$objItmes  		   		 = new Items();
	$objItemBatchStock   = new ItemBatchStock();

	//delete inventory Return
	if(isset($_POST['cid'])){
		$purchase_id = $_POST['cid'];
		$inventoryDetailList = $objInventorydetails->getList($purchase_id);
		$voucher_id = $objInventory->getVoucherId($purchase_id);
		$inventoryDetailList = $objInventorydetails->getList($purchase_id);
		if(mysql_num_rows($inventoryDetailList)){
			while($row = mysql_fetch_array($inventoryDetailList)){
				$item_id    = $row['ITEM_ID'];
				$quantity   = $row['STOCK_QTY'];
				$cost_price = $row['UNIT_PRICE'];
				$total_cost = $cost_price*$quantity;
				$objItmes->addStockValue($item_id,$quantity,$total_cost);
				if($row['BATCH_NO']!=''){
					$objItemBatchStock->addStock($row['ITEM_ID'],$row['BATCH_NO'],$row['EXPIRY_DATE'],$row['STOCK_QTY']);
				}
			}
		}
		$deleted = $objInventory->delete($purchase_id);
		$objInventorydetails->deleteCompleteBill($purchase_id);

		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		if($deleted){
			$return_data['MSG'] = 'Record Deleted Successfully!';
			$return_data['OK']  = 'Y';
		}else{
			$return_data['MSG'] = 'Error! Cannot Delete.';
			$return_data['OK']  = 'N';
		}
		echo json_encode($return_data);
		exit();
	}
	mysql_close($con);
?>
