<?php
	include('../common/db.connection.php');
	include( '../common/classes/j-voucher.php' );
	include( '../common/classes/accounts.php' );
	include( '../common/classes/userAccounts.php' );

	$objVoucher 		= new JournalVoucher();
	$objAccounts 		= new ChartOfAccounts();
	$objUsers				= new UserAccounts();

	$user_details = $objUsers->getDetails($user_id);

	if($user_id > 1 && $user_details['CASH_IN_HAND'] == 'Y'){
		$cash_in_hand_acc_code = $user_details['CASH_ACC_CODE'];
	}else{
		$cash_in_hand_acc_code = '0101010001';
	}

	$returnData = array('OK'=>'N','MSG'=>'Nothing','ID'=>'0');

	if(isset($_POST['newJv'])){
		$success = array();

		$voucher_id								  = mysql_real_escape_string($_POST['jv_id']);

		$cash_in_hand_acc_code 			= mysql_real_escape_string($_POST['cash_in_hand']);
		$objVoucher->voucherDate 		= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->type_num				= $objVoucher->genTypeNumber('CP');
		$objVoucher->voucherType    = 'CP';
		$objVoucher->jVoucherNum 		= $objVoucher->genJvNumber();
		$objVoucher->reference 			= $_POST['jvRef'];
		$objVoucher->reference_date = date('Y-m-d',strtotime($_POST['refDate']));
		$objVoucher->status					= 'F';

		$transactions_array = get_object_vars(json_decode($_POST['transactions']));

		if(!$objVoucher->checkVoucherByType($objVoucher->type_num,$objVoucher->voucherType) && $voucher_id == 0){
			$returnData = array('OK'=>'N','MSG'=>'Cash Payment Number Already Exists!','ID'=>'0');
			echo json_encode($returnData);
			exit();
		}
		if($voucher_id > 0){
			$objVoucher->reverseVoucherDetails($voucher_id);
			$objVoucher->updateVoucher($voucher_id);
			$objVoucher->updateVoucherDate($voucher_id);
			$objVoucher->voucherId = $voucher_id;
		}else{
			$objVoucher->voucherId = $objVoucher->saveVoucher();
		}
		if($objVoucher->voucherId){
			$cash_amount = 0;
			$re_nar = '';
			foreach($transactions_array as $key => $transaction){
				$cash_amount += (float)(trim($transaction->credit));
				if($key > 0){
					$re_nar .= ', ';
				}
				$re_nar .= trim($transaction->accTitle);
			}
			foreach($transactions_array as $key => $transaction){
				$objVoucher->voucherDate   	 = $objVoucher->getVoucherDate($objVoucher->voucherId);
				$objVoucher->accountCode 	 	 = trim($transaction->accCode);
				$objVoucher->accountTitle	 	 = mysql_real_escape_string($objAccounts->getAccountTitleByCode($transaction->accCode));
				$objVoucher->narration 	 	 	 = mysql_real_escape_string(trim($transaction->narration));
				$objVoucher->amount 				 = trim($transaction->credit);
				$objVoucher->transactionType = 'Dr';
				$success[] 									 = $objVoucher->saveVoucherDetail();
			}
			$objVoucher->accountCode 			= $cash_in_hand_acc_code;
			$objVoucher->accountTitle 		= mysql_real_escape_string($objAccounts->getAccountTitleByCode($objVoucher->accountCode));
			$objVoucher->narration 	 			= "Cash Paid To ".mysql_real_escape_string($re_nar);
			$objVoucher->transactionType 	= 'Cr';
			$objVoucher->amount 					= $cash_amount;
			$success[] 										= $objVoucher->saveVoucherDetail();

			$voucher_id 									= $objVoucher->voucherId;
		}else{
			$returnData = array('OK'=>'N','MSG'=>'Voucher Error!','ID'=>'0');
			$success[] = 0;
		}
		if(!in_array('0',$success)){
			$returnData = array('OK'=>'Y','MSG'=>'Cash Payment Saved Successfully!','ID'=>$voucher_id);
			echo json_encode($returnData);
		}else{
			$returnData = array('OK'=>'N','MSG'=>'Error! cannot post Cash Payment Voucher.','ID'=>'0');
			echo json_encode($returnData);
		}
	}
	mysql_close($con);
exit();
?>
