<?php
	include("../common/db.connection.php");
	include("../common/classes/machine_production.php");
	include("../common/classes/machine_production_details.php");

	$objMachineProduction				= new MachineProduction();
	$objMachineProductionDetail = new MachineProductionDetails();

	if(isset($_POST['add_production'])){
		$production_id 																= (int)mysql_real_escape_string($_POST['production_id']);
		$objMachineProduction->report_date 						= date('Y-m-d',strtotime($_POST['report_date']));
		$objMachineProduction->machine_id  						= mysql_real_escape_string($_POST['machine_id']);
		$objMachineProduction->machine_rpm						= mysql_real_escape_string($_POST['machine_rpm']);
		$objMachineProduction->expense 								= mysql_real_escape_string($_POST['expense']);
		$objMachineProduction->comment 								= mysql_real_escape_string($_POST['comment']);
		$objMachineProduction->stitch_shift_a 				= mysql_real_escape_string($_POST['stitch_shift_a']);
		$objMachineProduction->commander_shift_a			= mysql_real_escape_string($_POST['commander_shift_a']);
		$objMachineProduction->stitch_shift_b 				= mysql_real_escape_string($_POST['stitch_shift_b']);
		$objMachineProduction->commander_shift_b			= mysql_real_escape_string($_POST['commander_shift_b']);
		$objMachineProduction->niddle_setting 				= mysql_real_escape_string($_POST['niddle_setting']);
		$objMachineProduction->sale_rate 							= mysql_real_escape_string($_POST['saleRate']);
		if($production_id){
			$objMachineProduction->update($production_id);
		}else{
			$production_id = $objMachineProduction->save();
		}
		$json_data 		= get_object_vars(json_decode($_POST['jsonString']));
		$deleted_rows = get_object_vars(json_decode($_POST['deleted_rows']));
		foreach($deleted_rows as $key => $deleted_id){
			$objMachineProductionDetail->delete($deleted_id);
		}
		foreach($json_data as $record){
			$objMachineProductionDetail->mp_id  					= $production_id;
			$objMachineProductionDetail->party_acc_code 	= $record->party_acc_code;
			$objMachineProductionDetail->fabric_type 			= $record->farbric_type;
			$objMachineProductionDetail->thread_type			= $record->thread_type;
			$objMachineProductionDetail->design_no				= $record->design_no;
			$objMachineProductionDetail->item							= $record->item;
			$objMachineProductionDetail->shift_a					= $record->prod_shift_a;
			$objMachineProductionDetail->shift_b					= $record->prod_shift_b;
			$objMachineProductionDetail->tp								= $record->total_pord;
			$objMachineProductionDetail->yard							= $record->yard;
			$objMachineProductionDetail->thread_weight		= $record->thread_weight;
			$objMachineProductionDetail->at_rate					= $record->rate;
			$objMachineProductionDetail->thread_price			= $record->thread_price;
			$objMachineProductionDetail->total_price			= $record->total_price;
			$objMachineProductionDetail->stitch						= $record->stitch;
			$objMachineProductionDetail->stitch_a					= $record->stitch_a;
			$objMachineProductionDetail->stitch_b					= $record->stitch_b;
			$objMachineProductionDetail->rate							= $record->sale_rate;
			$objMachineProductionDetail->sale							= $record->sale;

			$record->row_id 															= (int)mysql_real_escape_string($record->row_id);

			if($record->row_id == 0){
				$objMachineProductionDetail->save();
			}else{
				$objMachineProductionDetail->update($record->row_id);
			}
		}
		echo $production_id;
	}
?>
