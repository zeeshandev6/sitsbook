<?php
	include('../common/db.connection.php');
	include('../common/classes/settings.php');
	include('../common/classes/accounts.php');
	include('../common/classes/inventory.php');
	include('../common/classes/inventory-return.php');
	include('../common/classes/inventory-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	= new ChartOfAccounts();
	$objPurchase 		 			= new inventory();
	$objInventory        	= new InventoryReturn();
	$objInventoryDetails 	= new InventoryReturnDetails();
	$objItems            	= new Items();
	$objItemCategory     	= new itemCategory();
	$objItemBatchStock 		= new ItemBatchStock();
	$objJournalVoucher   	= new JournalVoucher();
	$objConfigs		     		= new Configs();

	//JV Account Codes Used.
	$taxAccCode  				= '0101080001';
	$taxAccTitle  			= $objAccounts->getAccountTitleByCode($taxAccCode);
	$inventoryAccCode 	= '0101060002';
	$inventoryAccTitle  = $objAccounts->getAccountTitleByCode($inventoryAccCode);
	$income_tax_acc 		= '0101080002';
	$income_tax_title 	= $objAccounts->getAccountTitleByCode($income_tax_acc);

	$returnData = array('MSG'=>'Memo Could Not Be Saved!','ID'=>0);

	$items_narration = "";

	$discount_type  = $objConfigs->get_config('DISCOUNT');
	$stock_check    = $objConfigs->get_config('STOCK_CHECK');

	$dicount_amount = 0;
	if(isset($_POST['jSonString'])){
		$purchase_return_id   		       	 = mysql_real_escape_string($_POST['purchase_return_id']);
		$objInventory->purchase_id         = mysql_real_escape_string($_POST['purchase_id']);
		$objInventory->po_number           = isset($_POST['po_number'])?mysql_real_escape_string($_POST['po_number']):"";
		$objInventory->purchaseDate        = date('Y-m-d',strtotime($_POST['purchaseDate']));
		if($objInventory->purchase_id > 0){
			$purchase_details 			   		 = $objPurchase->getDetail($objInventory->purchase_id);
			$objInventory->billNum         = $purchase_details['BILL_NO'];
			$objInventory->supplierAccCode = $purchase_details['SUPP_ACC_CODE'];
		}else{
			$objInventory->billNum         = mysql_real_escape_string($_POST['billNum']);
			$objInventory->supplierAccCode = mysql_real_escape_string($_POST['supplierCode']);
			$objInventory->supplier_name   = mysql_real_escape_string($_POST['supplier_name']);
		}
		$objInventory->income_tax 		   = isset($_POST['income_tax'])?(float)(mysql_real_escape_string($_POST['income_tax'])):0;
		$objInventory->unregistered_tax  = isset($_POST['unregistered_tax'])?mysql_real_escape_string($_POST['unregistered_tax']):"";

		if($purchase_return_id == 0){
			$success 									  				= array();
			$purchase_return_id  								= $objInventory->save();
			$jSonData 									  			= json_decode($_POST['jSonString']);
			$transactions 											= get_object_vars($jSonData);

			$objInventoryDetails->purchase_id 	= $purchase_return_id;

			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$tansaction){
				$objInventoryDetails->item_id 	 		  	= $tansaction->item_id;
				if($tansaction->batch_no!=''){
					$objInventoryDetails->batch_no 	 		  	= $tansaction->batch_no;
					$objInventoryDetails->expiry_date 	 		= date('Y-m-d',strtotime($tansaction->expiry_date));
				}
				$objInventoryDetails->qtyReceipt 		  	= $tansaction->quantity;
				$objInventoryDetails->unitPrice 	  	  = $tansaction->unitPrice;
				$objInventoryDetails->purchaseDiscount  = $tansaction->discount;
				$objInventoryDetails->subTotal			  	= $tansaction->subAmount;
				$objInventoryDetails->taxRate			  		= $tansaction->taxRate;
				$objInventoryDetails->taxAmount			  	= $tansaction->taxAmount;
				$objInventoryDetails->totalAmount		  	= $tansaction->totalAmount;

				$purchase_detail_id = $objInventoryDetails->save();

				$itemStockInHand 		= $objItems->getStock($objInventoryDetails->item_id);
				if($discount_type == 'P'){
					$discount_amount = (($objInventoryDetails->qtyReceipt*$objInventoryDetails->unitPrice)*$objInventoryDetails->purchaseDiscount/100);
				}else{
					$discount_amount = $objInventoryDetails->purchaseDiscount;
				}
				if($itemStockInHand < $objInventoryDetails->qtyReceipt && $stock_check == 'Y'){
					$success[] = 'S';
					$itemWithLessStock = $objItems->getItemTitle($objInventoryDetails->item_id);
				}elseif($purchase_detail_id){
					$costaItem     		 = $objInventoryDetails->unitPrice*$objInventoryDetails->qtyReceipt;
					$costaItem     		-= $discount_amount;
					$objItems->removeStockValue($objInventoryDetails->item_id,$objInventoryDetails->qtyReceipt,$costaItem);
					if($tansaction->batch_no!=''){
						$objItemBatchStock->removeStock($objInventoryDetails->item_id,$objInventoryDetails->batch_no,$objInventoryDetails->expiry_date,$objInventoryDetails->qtyReceipt);
					}
				}
				if($objInventory->purchase_id > 0){
					$qty_limit = $objPurchase->get_item_qty_by_bill($objInventory->purchase_id, $objInventoryDetails->item_id);
					if($qty_limit < $objInventoryDetails->qtyReceipt){
						$itemWithLessStock = $objItems->getItemTitle($objInventoryDetails->item_id);
						$success[] = 'L';
					}
				}
				$item_name = $objItems->getItemTitle($objInventoryDetails->item_id);
				$txt       = "(".$item_name."-".$objInventoryDetails->qtyReceipt."@".$objInventoryDetails->unitPrice.")";
				$items_narration .= $txt;
				$success[] = ($purchase_detail_id)?"Y":"N";
			}
			//delete the false bill if there is an error!
			if(in_array('N',$success)||in_array('S',$success)||in_array('L',$success)){
				$purchaseDetailsArray = $objInventory->getPurchaseDetailArrayIdOnly($objInventoryDetails->purchase_id);
				foreach($purchaseDetailsArray as $p => $purchase_detail_id){
					$purchaseDetails = $objInventoryDetails->getDetails($purchase_detail_id);
					$objItems->addStockValue($purchaseDetails['ITEM_ID'],$purchaseDetails['STOCK_QTY'],$purchaseDetails['TOTAL_AMOUNT']);
					if($purchaseDetails['BATCH_NO']!=''){
						$objItemBatchStock->addStock($purchaseDetails['ITEM_ID'],$purchaseDetails['BATCH_NO'],$purchaseDetails['EXPIRY_DATE'],$purchaseDetails['STOCK_QTY']);
					}
					$objInventoryDetails->delete($purchase_detail_id);
				}
				$objInventory->delete($objInventoryDetails->purchase_id);
				if(in_array('S',$success)){
					$returnData['MSG'] = "Not Enough Stock - "+$itemWithLessStock;
				}elseif(in_array('L',$success)){
					$returnData['MSG'] = "Already Returned All "+$itemWithLessStock;
				}else{
					$returnData['MSG'] = "Memo Could Not Be Saved!!";
				}
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}elseif(!in_array('N',$success)){
				$taxAmount 					= $objInventory->getInventoryTaxAmountSum($objInventoryDetails->purchase_id);
				$billAmount 				= $objInventory->getInventoryAmountSum($objInventoryDetails->purchase_id);
				$supplierAccCode 		= $objInventory->supplierAccCode;
				$supplierAccTitle 	= $objAccounts->getAccountTitleByCode($objInventory->supplierAccCode);

				//Save Purchase Return Voucher

				$objJournalVoucher->jVoucherNum         = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate         = date('Y-m-d',strtotime($objInventory->purchaseDate));
				$objJournalVoucher->voucherType         = 'PR';
				$objJournalVoucher->reference           = 'Purchase Return';
				$objJournalVoucher->status	  		    	= 'P';

				$voucher_id 			   = $objJournalVoucher->saveVoucher();

				$income_tax   			= (float)$objInventory->income_tax;
				$income_tax_amount 	= round(($billAmount*$income_tax)/100);

				if($voucher_id){
					$objJournalVoucher->voucherId       = $voucher_id;

					$objJournalVoucher->accountCode     = $supplierAccCode;
					$objJournalVoucher->accountTitle    = $supplierAccTitle;
					$objJournalVoucher->narration       = $items_narration." Returned Against Bill#".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount          = $billAmount + $income_tax_amount;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode      = $inventoryAccCode;
					$objJournalVoucher->accountTitle     = $inventoryAccTitle;
					$objJournalVoucher->transactionType  = 'Cr';
					$objJournalVoucher->amount       	 	 = $billAmount - $taxAmount;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode      = $taxAccCode;
					$objJournalVoucher->accountTitle     = $taxAccTitle;
					$objJournalVoucher->narration        = "GST Against Return of inventory vide bill#".$objInventory->billNum;
					$objJournalVoucher->transactionType  = 'Cr';
					$objJournalVoucher->amount       	 	 = $taxAmount;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->accountCode     = $income_tax_acc;
					$objJournalVoucher->accountTitle    = $income_tax_title;
					$objJournalVoucher->narration       = "Income Tax return of purchases against bill#".$objInventory->billNum;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount          = (float)$income_tax_amount;

					$objJournalVoucher->saveVoucherDetail();
				}

				$objInventory->insertVoucherId($objInventoryDetails->purchase_id,$voucher_id);
				$returnData['MSG'] 	= "Memo # ".$objInventory->billNum." Return Entry Saved Successfully.";
				$returnData['ID'] 	= $objInventoryDetails->purchase_id;
			}
		}
		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
