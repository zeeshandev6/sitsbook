<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/emb_billing.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/emb_lot_register.php');

	$objJournalVoucher 			= new JournalVoucher();
	$objOutward 						= new outward();
	$objEmbBilling 					= new EmbBilling();

	if(isset($_GET['wid'])){
		$wid = $_GET['wid'];
		if($objEmbBilling->checkInherited($wid)){
			echo true;
		}else{
			echo false;
		}
	}
	if(isset($_GET['cid'])){
		$cid = $_GET['cid'];
		$voucher_id = $objEmbBilling->getVoucherId($cid);
		if($voucher_id !== 0){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
		}
		echo $objEmbBilling->delete($cid);
	}
	if(isset($_GET['did'])){
		$did = $_GET['did'];
		echo $objEmbBilling->deleteDetails($did);
	}
?>
