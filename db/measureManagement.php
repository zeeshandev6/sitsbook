<?php
	include('../common/db.connection.php');
	include('../common/classes/measure.php');

	$objMeasures = new Measures;

	if(isset($_POST['del'])){

		$measure_id = mysql_real_escape_string($_POST['del']);

		$deleted =  $objMeasures->delete($measure_id);
		if($deleted){
			echo json_encode(array('OK'=>'Y','MSG'=>'Measure Deleted Successfully!'));
		}else{
			echo json_encode(array('OK'=>'N','MSG'=>'Request Denied!'));
		}
	}

	if(isset($_POST['save'])){
		$measure_name = mysql_real_escape_string($_POST['save']);
		$objMeasures->name = $measure_name;
		$saved        = $objMeasures->save();
		if($saved > 0){
			echo json_encode(array('OK'=>'Y','ID'=>$saved));
		}else{
			echo json_encode(array('OK'=>'N','ID'=>'0'));
		}
	}
	mysql_close($con);
exit();
?>
