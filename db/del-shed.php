<?php
	
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sheds.php');
	include('../common/classes/machines.php');

	$objChartOfAccounts 	 = new ChartOfAccounts();
	$objSheds 	 			 = new Sheds();
	$objMachines 			 = new Machines();

	$shed_id 	 = (int)mysql_real_escape_string($_POST['id']);
	$shed_code   = $objSheds->getAccCode($shed_id);

	$mech_count   = $objMachines->countMachineByShed($shed_id);
	$account_used = $objChartOfAccounts->accountCheckInBooks($shed_code);

	$return = array();

	$return['OKAY'] = 'N';
	$return['MSG'] = 'Shed Deleted Successfully!';

	if(($mech_count+$account_used) == 0){
		$objSheds->delete($shed_id);
		$objChartOfAccounts->deleteAccCode($shed_code);
		$return['OKAY'] = 'Y';
	}else{
		$return['MSG'] = 'Error! Shed contains machines.';
	}
	echo json_encode($return);
?>
	