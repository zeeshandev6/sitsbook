<?php
	include('../common/db.connection.php');
	include('../common/classes/items.php');
	include('../common/classes/branch_stock.php');

	$objItmes 			= new Items();
	$objBranchStock = new BranchStock();

	if(isset($_POST['item_id'])){
		$item_id   = mysql_real_escape_string($_POST['item_id']);
		$godown_id = isset($_POST['godown_id'])?(int)mysql_real_escape_string($_POST['godown_id']):0;

		$itemPurchasePrice = $objItmes->getPurchasePrice($item_id);
		if($godown_id==0){
			$itemStock = $objItmes->getStock($item_id);
		}else{
			$itemStock = $objItmes->getItemStock($godown_id,$item_id);
		}
		echo json_encode(array('QTY'=>$itemStock,'PRICE'=>$itemPurchasePrice));
	}
	mysql_close($con);
	exit();
?>
