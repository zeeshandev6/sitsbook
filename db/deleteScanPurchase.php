<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	= new ChartOfAccounts();
	$objScanPurchase   	 	= new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objJournalVoucher   	= new JournalVoucher();
	$objItems 				= new Items();
	$ret = array();
	$ret['OKAY'] = 'N';
	if(isset($_POST['id'])){
		$id = mysql_real_escape_string($_POST['id']);
		$stock_used = $objScanPurchaseDetails->check_stock_status($id,'A');
		if($stock_used == 0){
			$voucher_id = $objScanPurchase->getVoucherId($id);
			$removed = $objJournalVoucher->reverseVoucherDetails($voucher_id);
			if($removed){
				$sp_list = $objScanPurchaseDetails->getList($id);
				if(mysql_num_rows($sp_list)){
					while($row = mysql_fetch_assoc($sp_list)){
						$objItems->removeScanCost($row['ITEM_ID'],$row['PRICE']);
					}
				}
				$objScanPurchase->delete($id);
				$objScanPurchaseDetails->deleteCompleteBill($id);
				$ret['MSG'] = 'Bill Deleted Successfully!';
				$ret['OKAY'] = 'Y';
			}else{
				$ret['MSG'] = 'Error! Bill Cant be deleted.';
				$ret['OKAY'] = 'N';
			}
		}else{
			$ret['MSG'] = 'Items Sold Against This Bill!';
			$ret['OKAY'] = 'N';
		}
		echo json_encode($ret);
	}
	mysql_close($con);
exit();
?>
