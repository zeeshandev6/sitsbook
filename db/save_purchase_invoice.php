<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/purchase_invoice.php');
	include('../common/classes/purchase_invoice_details.php');

	$objAccounts         			   = new ChartOfAccounts();
	$objCustomer 		 			   		 = new Customers();
	$objItems            			   = new Items();
	$objItemCategory     			   = new ItemCategory();
	$objJournalVoucher   			   = new JournalVoucher();
	$objInvoice                  = new PurchaseInvoice();
	$objInvoiceDetails           = new PurchaseInvoiceDetails();

	$salesAccCode = '0201010001';
	$salesAccTitle = $objAccounts->getAccountTitleByCode($salesAccCode);

	$returnData    = array('MSG'=>'Nothing Happened','ID'=>0,'TOM'=>'');
	$total_amount  = 0;
	$success = array();

	if(isset($_POST['invoice_id'])){
		$invoice_id 								= (int)($_POST['invoice_id']);
		$objInvoice->invoice_no   	= mysql_real_escape_string($_POST['invoice_no']);
		$objInvoice->invoice_date   = date("Y-m-d",strtotime($_POST['invoice_date']));
		$objInvoice->customer   		= mysql_real_escape_string($_POST['customer']);
		$objInvoice->expense_account = mysql_real_escape_string($_POST['expense_account']);
		$objInvoice->subject   			= mysql_real_escape_string($_POST['subject']);
		$objInvoice->notes   				= mysql_real_escape_string($_POST['comments']);
		$objInvoice->total_amount   = mysql_real_escape_string($_POST['total_amount']);

		if($invoice_id == 0){
			$invoice_id = $objInvoice->save();
			echo mysql_error();
			$returnData['MSG'] = "SUCCESS! Record saved successfully.";
		}else{
			$objInvoice->update($invoice_id);
			$objInvoiceDetails->deleteByMain($invoice_id);
			$returnData['MSG'] = "SUCCESS! Record updated successfully.";
		}
		$invoice_id = (int)$invoice_id;
		if($invoice_id > 0){
			$objInvoiceDetails->invoice_id 		  = $invoice_id;

			if(isset($_POST['custom_rows'])){
				$jSonData   	 = json_decode($_POST['custom_rows']);
				$custom_rows   = get_object_vars($jSonData);
				foreach($custom_rows as $t=>$tansaction){
					$objInvoiceDetails->description   = $tansaction->desc;
					$objInvoiceDetails->amount 		  	= (float)$tansaction->amount;
					$service_detail_id 				  			= $objInvoiceDetails->save();
					if($service_detail_id){
						$total_amount += $objInvoiceDetails->amount;
					}
				}
			}
		}

		if($invoice_id && $total_amount > 0){

			$customer_title = $objAccounts->getAccountTitleByCode($objInvoice->customer);

			$voucher_id = $objInvoice->getVoucherId($invoice_id);

			$objJournalVoucher->voucherType = 'SV';
			$objJournalVoucher->voucherDate = date("Y-m-d",strtotime($objInvoice->invoice_date));

			if($voucher_id == 0){
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$voucher_id = $objJournalVoucher->saveVoucher();
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}
			if($voucher_id){
				$objInvoice->setVoucherId($invoice_id,$voucher_id);
				$objJournalVoucher->voucherId = $voucher_id;

				$objJournalVoucher->accountCode  		= $objInvoice->expense_account;
				$objJournalVoucher->accountTitle 		= $objAccounts->getAccountTitleByCode($objInvoice->expense_account);
				$objJournalVoucher->narration    		= $objInvoice->subject." vide Bill # ".$objInvoice->invoice_no." From ".$customer_title;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       		= $total_amount;
				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode  		= $objInvoice->customer;
				$objJournalVoucher->accountTitle 		= $customer_title;
				$objJournalVoucher->narration    		= "Payable vide Bill # ".$objInvoice->invoice_no." against services acquired ";
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       		= $total_amount;
				$objJournalVoucher->saveVoucherDetail();
			}
		}

		$returnData['ID']  		= (int)$invoice_id;
		if(!$invoice_id){
			$returnData['MSG']  = "ERROR! RECORD CANNOT BE SAVED.";
		}
		echo json_encode($returnData);
		exit();
	}
?>
