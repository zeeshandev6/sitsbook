<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_lot_register.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');
	include('../common/classes/machines.php');

	$objLotRegisterDetails = new EmbLotRegister();
	$objChartOfAccounts    = new ChartOfAccounts();
	$objJournalVoucher     = new JournalVoucher();
	$objMachines           = new Machines();

	$voucher_id = 0;
	if(isset($_POST['cust_acc_code'])){
		$lot_details_id 													= isset($_POST['lot_details_id'])?(int)($_POST['lot_details_id']):0;
		$objLotRegisterDetails->lot_date 					= date('Y-m-d',strtotime($_POST['lot_date']));
		$objLotRegisterDetails->customerAccCode 	= mysql_real_escape_string($_POST['cust_acc_code']);
		$objLotRegisterDetails->product_id 				= mysql_real_escape_string($_POST['product_id']);
		$objLotRegisterDetails->lotNum 						= mysql_real_escape_string($_POST['lotNum']);
		$objLotRegisterDetails->quality 					= mysql_real_escape_string($_POST['pQuality']);
		$objLotRegisterDetails->measure 					= mysql_real_escape_string($_POST['pMeasure']);
		$objLotRegisterDetails->billing_type      = mysql_real_escape_string($_POST['billing_type']);
		$objLotRegisterDetails->stitches         	= mysql_real_escape_string($_POST['stitches']);
		$objLotRegisterDetails->total_laces       = mysql_real_escape_string($_POST['total_laces']);
		$objLotRegisterDetails->length 						= mysql_real_escape_string($_POST['pLength']);
		$objLotRegisterDetails->embRate 					= mysql_real_escape_string($_POST['embRate']);
		$objLotRegisterDetails->embAmount 				= mysql_real_escape_string($_POST['embAmount']);
		$objLotRegisterDetails->stitchRate 				= mysql_real_escape_string($_POST['stitchRate']);
		$objLotRegisterDetails->stitchAmount 			= mysql_real_escape_string($_POST['stitchAmount']);
		$objLotRegisterDetails->stitchAccount 		= mysql_real_escape_string($_POST['stitchAccount']);
		$objLotRegisterDetails->designNum 				= mysql_real_escape_string($_POST['designNum']);
		$objLotRegisterDetails->machineNum 				= mysql_real_escape_string($_POST['machineNum']);
		$objLotRegisterDetails->gatePassNumber 		= mysql_real_escape_string($_POST['gp_Number']);

		if($_POST['lot_status'] == 'P'){
			$objLotRegisterDetails->lotStatus      = 'P';
		}elseif($_POST['lot_status'] == 'R'){
			$objLotRegisterDetails->lotStatus      = 'R';
		}
		if($lot_details_id==0){
			$lot_details_id = $objLotRegisterDetails->save();
		}else{
			$lot_details  = $objLotRegisterDetails->getDetail($lot_details_id);
			$objLotRegisterDetails->updateLotDetail($lot_details_id);
			$voucher_id = (int)$lot_details['VOUCHER_ID'];
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
		}

		if($objLotRegisterDetails->lotStatus == 'R'){
			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = $objLotRegisterDetails->lot_date;
			$objJournalVoucher->reference      = "Lot Claim Entry";
			$objJournalVoucher->reference_date = $objLotRegisterDetails->lot_date;
			$objJournalVoucher->status 	 	 		 = 'F';
			$machine_id 											 = $objLotRegisterDetails->machineNum;
			$shedAccCode 											 = $objMachines->getShedAccCode($machine_id);

			if($voucher_id == 0){
				$voucher_id 											 = $objJournalVoucher->saveVoucher();
			}else{
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id){
				$objJournalVoucher->voucherId       = $voucher_id;
				$objJournalVoucher->accountCode     = $shedAccCode;
				$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($shedAccCode);
				$objJournalVoucher->amount		    	= $objLotRegisterDetails->embAmount;
				$objJournalVoucher->narration				= "Claim lodge and refunded to ".$objChartOfAccounts->getAccountTitleByCode($objLotRegisterDetails->customerAccCode);;
				$objJournalVoucher->amount          = str_replace('-','',$objJournalVoucher->amount);
				$objJournalVoucher->transactionType = 'Dr';

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = $objLotRegisterDetails->customerAccCode;
				$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
				$objJournalVoucher->transactionType = 'Cr';

				$objJournalVoucher->saveVoucherDetail();

				$objLotRegisterDetails->insertVoucherId($lot_details_id,$voucher_id);
			}else{
				$objLotRegisterDetails->delete($lot_details_id);
				$lot_details_id = '';
			}
		}
		echo  $lot_details_id;
	}
?>
