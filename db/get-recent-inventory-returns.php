<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/inventory.php');
	include('../common/classes/inventory-return.php');
	include('../common/classes/inventory-return-details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         				= new ChartOfAccounts();
	$objPurchase 		 						= new inventory();
	$objInventoryReturn  				= new InventoryReturn();
	$objInventoryReturnDetails 	= new InventoryReturnDetails();
	$objItems            				= new Items();
	$objItemCategory     				= new itemCategory();
	$objJournalVoucher   				= new JournalVoucher();

		$purchase_id   = mysql_real_escape_string($_GET['purchase_id']);
		$show_batch_no = isset($_GET['show_batch_no'])?$_GET['show_batch_no']:'N';
		$purchase_returns = $objInventoryReturn->getListByPurchaseId($purchase_id);
		if(mysql_num_rows($purchase_returns)&&$purchase_id > 0){
			while($purchase_return_main = mysql_fetch_array($purchase_returns)){
				$purchase_return_details = $objInventoryReturnDetails->getList($purchase_return_main['ID']);
					$qty = 0;
					$stotal = 0;
					$ttotal = 0;
					$atotal = 0;
				if(mysql_num_rows($purchase_return_details)){
?>
					<div id="panel-<?php echo $purchase_return_main['ID']; ?>">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            	<div class="caption_simple">
                            		Return Date : <?php echo date('d-m-Y',strtotime($purchase_return_main['PURCHASE_DATE'])) ?>
                            	</div>
                            	<div class="pull-right">
                            		<button class="btn btn-default btn-sm delete_purchase_return" do="<?php echo $purchase_return_main['ID']; ?>"><span class="fa fa-times"></span></button>
                            	</div>
                            </div>
                            <table class="wel_made">
                            <thead>
                                <tr>
                                   <th width="10%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
																	 <?php if($show_batch_no=='Y'){ ?>
																	 <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Batch#</th>
																	 <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">ExpiryDate</th>
																	 <?php } ?>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">Discount %</th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="8%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
							while($invRow = mysql_fetch_array($purchase_return_details)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
?>
                                <tr class="alt-row" data-row-id='<?php 		echo $invRow['ID']; ?>'>
                                    <td class="text-left itemName" data-item='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
																		<?php if($show_batch_no=='Y'){ ?>
	 																	<td style="text-align:center;"><?php 	echo $invRow['BATCH_NO'] ?></td>
	 																	<td style="text-align:center;"><?php 	echo date('d-m-Y',strtotime($invRow['EXPIRY_DATE'])) ?></td>
	 																	<?php } ?>
                                    <td style="text-align:center;"><?php 	echo $invRow['STOCK_QTY'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['UNIT_PRICE'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['PURCHASE_DISCOUNT'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['TAX_RATE'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td style="text-align:center;"><?php 	echo $invRow['TOTAL_AMOUNT'] ?></td>
                                </tr>
<?php
								$qty += $invRow['STOCK_QTY'];
								$stotal += $invRow['SUB_AMOUNT'];
								$ttotal += $invRow['TAX_AMOUNT'];
								$atotal += $invRow['TOTAL_AMOUNT'];
							}
?>
                                <tr class="totals">
                                    	<td style="text-align:center;background-color:#EEEEEE;" colspan="3">Total</td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo $qty; ?></td>
																				<?php if($show_batch_no=='Y'){ ?>
                                        <td style="text-align:center;background-color:#EEE;"> - - - </td>
                                        <td style="text-align:center;background-color:#f5f5f5;"> - - - </td>
																				<?php } ?>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo number_format($stotal,2); ?></td>
                                        <td style="text-align:center;background-color:#EEE;"> - - - </td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo number_format($ttotal,2); ?></td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo number_format($atotal,2); ?></td>
                                    </tr>
                            </tbody>
                        </table>
                        </div>
                        <hr />
                        </div>
<?php
						}
					}
				}else{
?>
						<div class="block text-center">No Recent Returns!</div>
<?php
				}
				mysql_close($con);
exit();
?>
