<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-purchase.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	= new ChartOfAccounts();
	$objScanPurchase   	 	= new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objJournalVoucher   	= new JournalVoucher();
	$objItems 				= new Items();

	$inv_acc_code  = '0101060001';
	$inv_acc_title = $objAccounts->getAccountTitleByCode($inv_acc_code);

	$taxAccCode  = '0401050001';
	$taxAccTitle  = $objAccounts->getAccountTitleByCode($taxAccCode);

	$discountAccCode  = '0201010002'; //purchase Discount
	$discountAccTitle  = $objAccounts->getAccountTitleByCode($discountAccCode);

	$objScanPurchase->user_id = $_SESSION['classuseid'];

	$returnData = array('MSG'=>'Nothing Happened');
	$success = array();
	if(isset($_POST['jSonString'])){
		$scan_purchase_id       		= mysql_real_escape_string($_POST['sp_id']);
		$objScanPurchase->scan_date 	= date('Y-m-d',strtotime($_POST['purchase_date']));
		$objScanPurchase->bill_no		= mysql_real_escape_string($_POST['bill_no']);
		$objScanPurchase->supp_acc_code	= mysql_real_escape_string($_POST['supplier_acc_code']);
		$objScanPurchase->supplier_name	= mysql_real_escape_string($_POST['supplier_name']);
		$objScanPurchase->inv_notes	    = mysql_real_escape_string($_POST['inv_notes']);

		$jSonData     = json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);

		$bill_discount = mysql_real_escape_string($_POST['bill_discount']);
		$bill_tax      = mysql_real_escape_string($_POST['bill_tax']);
		$billTotal     = mysql_real_escape_string($_POST['total_bill']);
		$total_sub     = mysql_real_escape_string($_POST['total_sub']);

		$validated = true;
		foreach($transactions as $t=>$tansaction){
			if($tansaction->item_id < 1){
				$validated = false;
			}
			if($tansaction->barcode == ''){
				$validated = false;
				$returnData['MSG'] = 'Error! Please Fill The Form Correctly.';
			}
			$exists = $objScanPurchaseDetails->check_barcodeExclude_id($tansaction->barcode,$tansaction->row_id);
			if($exists){
				$validated = false;
				$returnData['MSG'] = 'Error! '.$tansaction->barcode.' Already In Stock.';
			}
		}
		if($validated){
			if($scan_purchase_id == 0){
				$objScanPurchaseDetails->sp_id = $objScanPurchase->save();
				foreach($transactions as $t=>$tansaction){
					$objScanPurchaseDetails->item_id 	 	= $tansaction->item_id;
					$objScanPurchaseDetails->barcode		= $tansaction->barcode;
					$objScanPurchaseDetails->colour 		= $tansaction->colour;
					$objScanPurchaseDetails->purchase_price = $tansaction->purchase;
					$objScanPurchaseDetails->discount  		= $tansaction->discount;
					$objScanPurchaseDetails->sub_total  	= $tansaction->sub_total;
					$objScanPurchaseDetails->tax  			= $tansaction->tax;
					$objScanPurchaseDetails->price  		= $tansaction->price;
					$objScanPurchaseDetails->sale_price     = $tansaction->sale_price;
					$objScanPurchaseDetails->stock_status  	= 'A';

					$barcode_id = $objScanPurchaseDetails->save();
					$objItems->addScanCost($tansaction->item_id,$tansaction->price);
					$success[] = $barcode_id;
				}
			}elseif($scan_purchase_id > 0){
				$updated = $objScanPurchase->update($scan_purchase_id);
				$details_id_array = $objScanPurchaseDetails->getListIDArray($scan_purchase_id);
				$new_details_id_array = array();
				if($updated){
					$objScanPurchaseDetails->sp_id = $scan_purchase_id;
					foreach($transactions as $t=>$tansaction){
						$new_details_id_array[] = $tansaction->row_id;
					}
					foreach($details_id_array as $key=>$id){
						if(!in_array($id, $new_details_id_array)){
							$objScanPurchaseDetails->deleteInStock($id);
							$spd = $objScanPurchaseDetails->getDetails($id);
							$objItems->removeScanCost($spd['ITEM_ID'],$spd['PRICE']);
						}
					}
					foreach($transactions as $t=>$tansaction){
						$objScanPurchaseDetails->item_id 	 	= $tansaction->item_id;
						$objScanPurchaseDetails->colour 		= $tansaction->colour;
						$objScanPurchaseDetails->barcode		= $tansaction->barcode;
						$objScanPurchaseDetails->purchase_price = $tansaction->purchase;
						$objScanPurchaseDetails->discount  		= $tansaction->discount;
						$objScanPurchaseDetails->sub_total  	= $tansaction->sub_total;
						$objScanPurchaseDetails->tax  			= $tansaction->tax;
						$objScanPurchaseDetails->price  		= $tansaction->price;
						$objScanPurchaseDetails->sale_price     = $tansaction->sale_price;
						$objScanPurchaseDetails->stock_status  	= 'A';

						if($tansaction->row_id == 0){
							$barcode_id = $objScanPurchaseDetails->save();
							$objItems->addScanCost($tansaction->item_id,$tansaction->price);
						}elseif($tansaction->row_id > 0){
							$sp_details = $objScanPurchaseDetails->getDetails($tansaction->row_id);
							if($sp_details['STOCK_STATUS'] == 'A'){
								if(in_array($tansaction->row_id, $details_id_array)){
									$objItems->removeScanCost($sp_details['ITEM_ID'],$sp_details['PRICE']);
									$objItems->addScanCost($tansaction->item_id,$tansaction->price);
									$update_detail = $objScanPurchaseDetails->update($tansaction->row_id);
									if($update_detail){
										$barcode_id = $tansaction->row_id;
									}else{
										$barcode_id = 0;
									}
									$success[] = $barcode_id;
								}
							}
						}
					}
				}
			}else{
				$success[] = false;
			}
			if(!in_array(false, $success)){
				$objJournalVoucher->voucherId = $objScanPurchase->getVoucherId($scan_purchase_id);
				$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate = $objScanPurchase->scan_date;
				$objJournalVoucher->voucherType = 'PV';
				if($objJournalVoucher->voucherId == 0){
					$objJournalVoucher->voucherId   = $objJournalVoucher->saveVoucher();
					$objScanPurchase->insertVoucherId($objScanPurchaseDetails->sp_id,$objJournalVoucher->voucherId);
				}else{
					if($objJournalVoucher->voucherId > 0){
						$objJournalVoucher->updateVoucherType($objJournalVoucher->voucherId);
						$objJournalVoucher->updateVoucherDate($objJournalVoucher->voucherId);
						$objJournalVoucher->reverseVoucherDetails($objJournalVoucher->voucherId);
					}else{
						$success[] = false;
					}
				}
				if($objJournalVoucher->voucherId > 0){
					$supplier_amount   = $billTotal;
					$Inventory_amount  = ($billTotal - $bill_tax);

					$supplier_acc_title = $objAccounts->getAccountTitleByCode($objScanPurchase->supp_acc_code);

					$objJournalVoucher->amount       = $Inventory_amount;
					$objJournalVoucher->accountCode  = $inv_acc_code;
					$objJournalVoucher->accountTitle = $inv_acc_title;
					$objJournalVoucher->transactionType = 'Dr';

					if(substr($objScanPurchase->supp_acc_code,0,6)=='010101'){
						$objJournalVoucher->narration = 'Purchased Inventory on cash from '.$objScanPurchase->supplier_name.'  Against Bill # '.$objScanPurchase->bill_no;
					}else{
						$objJournalVoucher->narration = 'Purchased Inventory Against Bill # '.$objScanPurchase->bill_no." From ".$supplier_acc_title;
					}
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->amount          = $bill_tax;
					$objJournalVoucher->accountCode     = $taxAccCode;
					$objJournalVoucher->accountTitle    = $taxAccTitle;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->narration = 'Sales Tax Against Bill # '.$objScanPurchase->bill_no;
					$objJournalVoucher->saveVoucherDetail();

					$objJournalVoucher->amount 		 = $supplier_amount;
					$objJournalVoucher->accountCode  = $objScanPurchase->supp_acc_code;
					$objJournalVoucher->accountTitle = $supplier_acc_title;
					$objJournalVoucher->transactionType = 'Cr';
					if(substr($objScanPurchase->supp_acc_code,0,6)=='010101'){
						$objJournalVoucher->narration = 'Cash Paid Against Bill # '.$objScanPurchase->bill_no;
					}else{
						$objJournalVoucher->narration = 'Amount Payable Against Bill # '.$objScanPurchase->bill_no;
					}
					$objJournalVoucher->saveVoucherDetail();
					$returnData['ID'] =  $objScanPurchaseDetails->sp_id;
					$returnData['OK'] = 'Y';
					$returnData['MSG'] = 'Bill # '.$objScanPurchase->bill_no.' Saved Successfully!';
				}
			}
			if(in_array(false, $success)){
				$objScanPurchase->delete($objScanPurchaseDetails->sp_id);
				$objScanPurchaseDetails->deleteCompleteBill($objScanPurchaseDetails->sp_id);
				$returnData['OK'] = 'N';
				$returnData['MSG'] = 'Error! Cannot Save Data.';
			}
		}else{
			$returnData['OK'] = 'N';
		}
		echo json_encode($returnData);
		mysql_close($con);
exit();
	}
?>
