<?php
	include('../common/db.connection.php');
	include( '../common/classes/accounts.php' );
	include( '../common/classes/customers.php' );
	include( '../common/classes/suppliers.php' );
	include( '../common/classes/userAccounts.php' );
	include( '../common/classes/payrolls.php' );
	include( '../common/classes/banks.php' );

	$objAccounts = new ChartOfAccounts();
	$objCustomers= new Customers();
	$objSuppliers= new suppliers();
	$objPayrolls = new Payrolls();
	$objUsers    = new UserAccounts();
	$objBanks    = new Banks();

	$ret = array('OK'=>'N','MSG'=>'Nothing Happened!');
	if(isset($_GET['id'])){
		$term = $_GET['id'];
		$accountDetailArray = mysql_fetch_array($objAccounts->getAccountCodeById($term));
		$query = "DELETE FROM `account_code` WHERE ACC_CODE_ID = '$term' LIMIT 1";
		$excecuted = false;
		if($accountDetailArray['ACC_CODE'] == ''){
			exit();
		}
		$num_rows = $objAccounts->accountCheckInBooks($accountDetailArray['ACC_CODE']);

		if(strpos($term,'040107') !== false ){
			$num_rows = 1;
		}
		if($num_rows == 0){
			$objCustomers->delete_code($accountDetailArray['ACC_CODE']);
			$objSuppliers->delete_code($accountDetailArray['ACC_CODE']);
			$objUsers->delete_by_acc_code($accountDetailArray['ACC_CODE']);
			$excecuted = mysql_query($query);
			if($excecuted){
				if($excecuted){
					$ret['OK'] = 'Y';
					$ret['MSG'] = 'Record Deleted Successfully!';
				}else{
					$ret['MSG'] = 'Error, Deleting Record!';
				}
			}
		}else{
			$ret['MSG'] = 'Account Code Can Not Be Deleted!';
		}
		echo json_encode($ret);
		exit();
	}

	if(isset($_GET['sid'])){
		$term = $_GET['sid'];
		$supplier_details = mysql_fetch_array($objSuppliers->getRecordDetails($term));
		$acc_code = $supplier_details['SUPP_ACC_CODE'];
		$query = "DELETE FROM `account_code` WHERE ACC_CODE = '".$acc_code."' LIMIT 1";
		$excecuted = false;
		if($acc_code == ''){
			exit();
		}
		$num_rows = $objAccounts->accountCheckInBooks($acc_code);
		if($num_rows == 0){
			$objSuppliers->delete($term);
			$excecuted = mysql_query($query);
			if($excecuted){
				$ret['OK'] = 'Y';
				$ret['MSG'] = 'Record Deleted Successfully!';
			}else{
				$ret['MSG'] = 'Error, Deleting Record!';
			}
		}else{
			$ret['MSG'] = 'Account Code Can Not Be Deleted!';
		}
		echo json_encode($ret);
		exit();
	}
	if(isset($_GET['cid'])){
		$term = $_GET['cid'];
		$customer_details = mysql_fetch_array($objCustomers->getRecordDetails($term));
		$acc_code = $customer_details['SUPP_ACC_CODE'];
		$query = "DELETE FROM `account_code` WHERE ACC_CODE = '".$acc_code."' LIMIT 1";
		$excecuted = false;
		if($acc_code == ''){
			exit();
		}
		$num_rows = $objAccounts->accountCheckInBooks($acc_code);
		if($num_rows == 0){
			$objCustomers->delete($term);
			$excecuted = mysql_query($query);
			if($excecuted){
				if($excecuted){
					$ret['OK'] = 'Y';
					$ret['MSG'] = 'Record Deleted Successfully!';
				}else{
					$ret['MSG'] = 'Error, Deleting Record!';
				}
			}
		}else{
			$ret['MSG'] = 'Account Code Can Not Be Deleted!';
		}
		echo json_encode($ret);
		exit();
	}
	if(isset($_GET['acc_code'])){
		$account_code = mysql_real_escape_string($_GET['acc_code']);
		if($account_code == ''){
			exit();
		}
		$num_rows = $objAccounts->accountCheckInBooks($account_code);
		if(substr($account_code,0,6) == '040107' ){
			$ret['MSG'] = 'Notice : Please delete from shed management!';
			echo json_encode($ret);
			exit();
		}
		if($num_rows == 0){
			$objCustomers->delete_code($account_code);
			$objSuppliers->delete_code($account_code);
			$objUsers->delete_by_acc_code($account_code);
			$objPayrolls->delete_code($account_code);
			$objBanks->delete_code($account_code);
			$deleted = $objAccounts->deleteAccCode($account_code);
			if($deleted){
				$ret['OK'] = 'Y';
				$ret['MSG'] = 'Record Deleted Successfully!';
			}else{
				$ret['MSG'] = 'Error, Deleting Record!';
			}
		}else{
			$ret['MSG'] = 'Error! Transactions existing against this account.';
		}
		echo json_encode($ret);
		exit();
	}
	mysql_close($con);
?>
