<?php
	include('../common/db.connection.php');
	include('../common/classes/items.php');
	include('../common/classes/dist_sale.php');
	include('../common/classes/dist_sale_return.php');
	include('../common/classes/dist_sale_return_details.php');

	$objItems                  = new Items();
	$objSale	                 = new DistributorSale();
	$objSaleReturn             = new DistributorSaleReturn();
	$objSaleReturnDetails      = new DistributorSaleReturnDetails();

	if(isset($_POST['p_item_id'])&&isset($_POST['purchase_id'])){
		$purchase_id = mysql_real_escape_string($_POST['purchase_id']);
		$item_id  	 = mysql_real_escape_string($_POST['p_item_id']);

		$items_returned = $objInventoryReturn->get_item_qty_by_bill($purchase_id, $item_id);
		$item_limit     = $objInventory->get_item_qty_by_bill($purchase_id, $item_id);
		$itemStock 		= $objItems->getStock($item_id);
		$itemPrice 		= $objItems->getPurchasePrice($item_id);

		$item_limit -= $items_returned;

		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$itemPrice,'RETURNED'=>$items_returned,'LIMIT'=>$item_limit));
		mysql_close($con);
		exit();
	}
	if(isset($_POST['p_item_id'])&&isset($_POST['sale_id'])){
		$sale_id = mysql_real_escape_string($_POST['sale_id']);
		$item_id = mysql_real_escape_string($_POST['p_item_id']);

		$items_returned = $objSaleReturn->get_item_qty_by_bill($sale_id, $item_id);
		$item_limit     = $objSale->get_item_qty_by_bill($sale_id, $item_id);

		$item_details   = $objItems->getRecordDetails($item_id);

		$itemStock 			= $item_details['STOCK_QTY'];
		$itemPrice 			= $item_details['SALE_PRICE'];
		$rate_carton        = $item_details['RATE_CARTON'];
		$qty_carton         = $item_details['QTY_PER_CARTON'];

		$item_limit['QTY'] -= $items_returned;

		echo json_encode(array('STOCK'=>$itemStock,'S_PRICE'=>$itemPrice,'RETURNED'=>$items_returned,'LIMIT_QTY'=>$item_limit['QTY'],'LIMIT_CARTONS'=>$item_limit['CARTNS'],'LIMIT_DOZENS'=>$item_limit['DZNS'],'QTY_CARTON'=>$qty_carton,'RATE_CARTON'=>$rate_carton,'DOZEN_RATE'=>$item_details['DOZEN_RATE']));
		mysql_close($con);
		exit();
	}
	if(isset($_POST['p_item_id'])){
		$item_id = mysql_real_escape_string($_POST['p_item_id']);
		$itemStock = $objItems->getStock($item_id);
		$itemPrice = $objItems->getPurchasePrice($item_id);
		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$itemPrice));
		mysql_close($con);
exit();
	}
	if(isset($_POST['s_item_id'])){
		$item_id = mysql_real_escape_string($_POST['s_item_id']);
		$itemStock = $objItems->getStock($item_id);
		$itemPrice = $objItems->getSalePrice($item_id);
		echo json_encode(array('STOCK'=>$itemStock,'S_PRICE'=>$itemPrice));
		mysql_close($con);
exit();
	}
?>
