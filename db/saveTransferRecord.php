<?php
	include('../common/db.connection.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');

	$objItems            	   = new Items();
	$objItemCategory     	   = new itemCategory();
	$objBranches  	         = new Branches();
	$objBranchStock          = new BranchStock();
  $objStockTransfer        = new StockTransfer();
  $objStockTransferDetail  = new StockTransferDetail();

  function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems            	   = new Items();
      $objBranchStock          = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->addStockValue($item_id,$quantity,$total_cost);
      }
  }
  function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems            	   = new Items();
      $objBranchStock          = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->removeStockValue($item_id,$quantity,$total_cost);
      }
  }

	$returnData = array('MSG'=>'Error! nothing affected.','ID'=>0);

	if(isset($_POST['jSonString'])){
		$transfer_id   									= mysql_real_escape_string($_POST['transfer_id']);
		$objStockTransfer->entry_date   = date('Y-m-d',strtotime($_POST['transfer_date']));

		if($transfer_id == 0){
			$transfer_id = $objStockTransfer->save();
			$returnData['MSG'] = "Success! Record saved successfully.";
		}elseif($transfer_id > 0){
			$objStockTransfer->update($transfer_id);
			$returnData['MSG'] = "Success! Record updated successfully.";
		}

		if($transfer_id > 0){
			$prev_id_array     = $objStockTransferDetail->getRowIdArray($transfer_id);
			$jSonData          = json_decode($_POST['jSonString']);
			$transactions      = get_object_vars($jSonData);

			$new_record_id_arr = array();

			$objStockTransferDetail->transfer_id = $transfer_id;
			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$tansaction){
        $tansaction->row_id = (int)$tansaction->row_id;
        if($tansaction->row_id >0){
            $new_record_id_arr[]  = $tansaction->row_id;
        }
				$objStockTransferDetail->item_id 	 		  = $tansaction->item_id;
        $objStockTransferDetail->quantity       = $tansaction->quantity;
        $objStockTransferDetail->from_branch    = $tansaction->from_branch;
        $objStockTransferDetail->to_branch      = $tansaction->to_branch;
        $objStockTransferDetail->unit_cost      = $tansaction->unit_cost;
        $objStockTransferDetail->total_cost     = $tansaction->total_cost;

				if($tansaction->row_id == 0){
					$tansaction->row_id = $objStockTransferDetail->save();
					if($tansaction->row_id>0){
							remove_branch_stock($tansaction->item_id,$tansaction->from_branch,$tansaction->quantity,$tansaction->total_cost);
							add_branch_stock($tansaction->item_id,$tansaction->to_branch,$tansaction->quantity,$tansaction->total_cost);
					}
				}

        if($tansaction->row_id > 0){
            $objStockTransferDetail->update($tansaction->row_id);
            if(mysql_affected_rows()>0){
                $prev_detail = $objStockTransferDetail->getDetail($tansaction->row_id);
                add_branch_stock($prev_detail['ITEM_ID'],$prev_detail['FROM_BRANCH'],$prev_detail['STOCK_QTY'],$prev_detail['TOTAL_COST']);
                remove_branch_stock($prev_detail['ITEM_ID'],$prev_detail['TO_BRANCH'],$prev_detail['STOCK_QTY'],$prev_detail['TOTAL_COST']);

                remove_branch_stock($tansaction->item_id,$tansaction->from_branch,$tansaction->quantity,$tansaction->total_cost);
                add_branch_stock($tansaction->item_id,$tansaction->to_branch,$tansaction->quantity,$tansaction->total_cost);
            }
        }
			}

	    //delete transaction
	    foreach($prev_id_array as $key => $prev_id){
	        if(!in_array($prev_id,$new_record_id_arr)){
	            $prev_detail = $objStockTransferDetail->getDetail($prev_id);
	            add_branch_stock($prev_detail['ITEM_ID'],$prev_detail['FROM_BRANCH'],$prev_detail['QUANTITY'],$prev_detail['TOTAL_COST']);
	            remove_branch_stock($prev_detail['ITEM_ID'],$prev_detail['TO_BRANCH'],$prev_detail['QUANTITY'],$prev_detail['TOTAL_COST']);
	            $objStockTransferDetail->delete($prev_id);
	        }
	    }
		}
		$returnData['ID'] = $transfer_id;
		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
