<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/tax-rates.php');

	$objAccounts = new ChartOfAccounts;
	$objTaxRates = new TaxRates;

	$ret = array();

	if(isset($_POST['id'])){
		$id = mysql_real_escape_string($_POST['id']);
		$taxDetail = $objTaxRates->getDetail($id);
		$taxAccCode= $taxDetail['TAX_ACC_CODE'];
		$record_in_books = $objAccounts->accountCheckInBooks($taxAccCode);
		if($record_in_books==0){
			$objAccounts->deleteAccCode($taxAccCode);
			$deleted = $objTaxRates->delete($id);
			if($deleted){
				$ret['OK'] = 'Y';
				$ret['MSG']= 'Deleted Successfully!';
			}else{
				$ret['OK'] = 'N';
				$ret['MSG']= 'Error! Unable To Delete Record.';
			}
			echo json_encode($ret);
		}
	}
	mysql_close($con);
exit();
?>
