<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/dist_sale.php');
	include('../common/classes/dist_sale_return.php');
	include('../common/classes/dist_sale_return_details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts         	 = new ChartOfAccounts();
	$objSale			 	 = new DistributorSale();
	$objSaleReturn       	 = new DistributorSaleReturn();
	$objSaleReturnDetails	 = new DistributorSaleReturnDetails();
	$objBranches  	         = new Branches();
	$objBranchStock          = new BranchStock();
  	$objStockTransfer        = new StockTransfer();
  	$objStockTransferDetail  = new StockTransferDetail();
	$objCustomer 		     = new Customers();
	$objItems                = new Items();
	$objItemCategory         = new itemCategory();
	$objJournalVoucher       = new JournalVoucher();
	$objUserAccounts   	     = new UserAccounts();

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock          = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->addStockValue($item_id,$quantity,$total_cost);
        }
    }
    function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new Items();
        $objBranchStock          = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->removeStockValue($item_id,$quantity,$total_cost);
        }
    }

    $user_details     = $objUserAccounts->getDetails($_SESSION['classuseid']);
    $branch_id 		    = $user_details['BRANCH_ID'];

	//JV Account Codes Used.
	$taxAccCode  		= '0401050001';
	$taxAccTitle  		= $objAccounts->getAccountTitleByCode($taxAccCode);
	$costAccCode  		= '0301040001';
	$costAccTitle  		= $objAccounts->getAccountTitleByCode($costAccCode);
	$inventoryAccCode   = '0101060001'; //sale Returns a/c
	$inventoryAccTitle  = $objAccounts->getAccountTitleByCode($inventoryAccCode);
	$salesAccCode 		= '0201010003'; //inventory account
	$salesAccTitle 	    = $objAccounts->getAccountTitleByCode($salesAccCode);

	$returnData = array('MSG'=>'Memo Could Not Be Saved!','ID'=>0);

	$items_narration = "";

	if(isset($_POST['trans_rows'])){
		$sale_id   						= mysql_real_escape_string($_POST['sale_id']);
		$objSaleReturn->sale_id	 		= $sale_id;
		$sale_return_id   				= (isset($_POST['sale_return_id']))?mysql_real_escape_string($_POST['sale_return_id']):0;
		$objSaleReturn->order_taker 	= mysql_real_escape_string($_POST['order_taker']);
		$objSaleReturn->user_id 		= mysql_real_escape_string($_POST['salesman']);
		$objSaleReturn->po_number 		= (isset($_POST['po_number']))?mysql_real_escape_string($_POST['po_number']):"";
		$objSaleReturn->saleDate    	= date('Y-m-d',strtotime($_POST['saleDate']));
		$objSaleReturn->discount_type 	= mysql_real_escape_string($_POST['discount_type']);
		$objSaleReturn->bill_discount 	= mysql_real_escape_string($_POST['bill_discount']);
		$objSaleReturn->over_discount 	= mysql_real_escape_string($_POST['over_discount']);
		$over_total 				  	= mysql_real_escape_string($_POST['over_total']);

		if($sale_id > 0){
			$sale_details  			        = $objSale->getDetail($sale_id);
			$objSaleReturn->billNum         = $sale_details['BILL_NO'];
			$objSaleReturn->customerAccCode = $sale_details['CUST_ACC_CODE'];
		}else{
			$objSaleReturn->billNum         = mysql_real_escape_string($_POST['billNum']);
			$objSaleReturn->customerAccCode = mysql_real_escape_string($_POST['customerCode']);
			$objSaleReturn->customer_name   = mysql_real_escape_string($_POST['customer_name']);
		}

		$cost_sales = 0;
		if($sale_return_id == 0){
			$success = array();
			$sale_return_id  = $objSaleReturn->save();
			if($sale_return_id == 0){
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}
			$objSaleReturnDetails->sale_id = $sale_return_id;
			$jSonData 		= json_decode($_POST['trans_rows']);
			$transactions = get_object_vars($jSonData);
			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$transaction){
				$objSaleReturnDetails->item_id 	 	  = $transaction->item_id;
				$objSaleReturnDetails->cartons 		  = $transaction->cartons;
				$objSaleReturnDetails->rate_carton 	  = $transaction->rate_carton;
				$objSaleReturnDetails->dozens 		  = $transaction->dozen;
				$objSaleReturnDetails->dozen_price 	  = $transaction->dozen_price;
				$objSaleReturnDetails->qtyReceipt 	  = $transaction->quantity;
				$objSaleReturnDetails->unitPrice 	  = $transaction->unitPrice;
				$objSaleReturnDetails->saleDiscount   = $transaction->discount;
				$objSaleReturnDetails->subTotal		  = $transaction->subAmount;
				$objSaleReturnDetails->taxRate		  = $transaction->taxRate;
				$objSaleReturnDetails->taxAmount	  = $transaction->taxAmount;
				$objSaleReturnDetails->totalAmount	  = $transaction->totalAmount;

				$sale_detail_id = $objSaleReturnDetails->save();
				//Update Correspoding item Stock.
				if($sale_detail_id){

					$qty_carton= $objItems->getItemQtyPerCarton($objSaleReturnDetails->item_id);
					$trans_qty = $objSaleReturnDetails->cartons*$qty_carton;
					$trans_qty+= ($objSaleReturnDetails->dozens*12);
					$trans_qty+= $objSaleReturnDetails->qtyReceipt;

					$item_costa  = $objSaleReturnDetails->getHistoryCostPrice($objSaleReturn->sale_id,$objSaleReturnDetails->item_id);
					$costa_item  = $item_costa*$trans_qty;
					$cost_sales += round($costa_item,2);
					add_branch_stock($objSaleReturnDetails->item_id,$branch_id,$trans_qty,$costa_item);

					$item_name = $objItems->getItemTitle($objSaleReturnDetails->item_id);
					$txt       = "(".$item_name."-".$trans_qty."@".$objSaleReturnDetails->unitPrice.")";
					$items_narration .= $txt;
				}
				$success[] = ($sale_detail_id)?"Y":"N";
			}
			//delete the false bill if there is an error!
			if(in_array('N',$success)){
				$saleDetailsArray = $objSaleReturn->getSaleDetailArrayIdOnly($objSaleReturnDetails->sale_id);
				$thisVoucherId = $objSaleReturn->getVoucherId($objSaleReturnDetails->sale_id);
				foreach($saleDetailsArray as $p => $sale_detail_id){
					$saleDetails = $objSaleReturnDetails->getDetails($sale_detail_id);

					$qty_carton= $objItems->getItemQtyPerCarton($saleDetails['ITEM_ID']);
					$trans_qty = $saleDetails['CARTONS']*$qty_carton;
					$trans_qty+= $saleDetails['QUANTITY'];

					$avg_price   = $objItems->getPurchasePrice($saleDetails['ITEM_ID']);
					$avg_price   = $trans_qty*$avg_price;
					remove_branch_stock($saleDetails['ITEM_ID'],$branch_id,$trans_qty,$avg_price);
					$objSaleReturnDetails->delete($sale_detail_id);
				}
				$objSaleReturn->delete($objSaleReturnDetails->sale_id);
				if($thisVoucherId > 0){
					$objJournalVoucher->reverseVoucherDetails($thisVoucherId);
					$objJournalVoucher->deleteJv($thisVoucherId);
				}
				$returnData['MSG'] = "--ERROR-- Memo Could Not Be Saved.";
				$returnData['ID']  = 0;
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}elseif(!in_array('N',$success)){
				$taxAmount = $objSaleReturn->getInventoryTaxAmountSum($objSaleReturnDetails->sale_id);
				//$billAmount = $objSaleReturn->getInventoryAmountSum($objSaleReturnDetails->sale_id);
				$billAmount   = $over_total;
				//GetCostaSales
				$details_id_array = $objSaleReturn->getSaleDetailArrayIdOnly($objSaleReturnDetails->sale_id);
				$costaSales = $cost_sales;
				$customerAccCode = $objSaleReturn->customerAccCode;
				$customerAccTitle = $objAccounts->getAccountTitleByCode($objSaleReturn->customerAccCode);

				$billNumber = $objSaleReturn->billNum;
				$entryDate  = $objSaleReturn->saleDate;

				//Sale Return Voucher
				$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
				$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($entryDate));
				$objJournalVoucher->voucherType    = 'SR';
				$objJournalVoucher->reference      = 'Sales Returns Voucher';
				$objJournalVoucher->reference_date = date('Y-m-d',strtotime($entryDate));
				$objJournalVoucher->status	  	   = 'P';

				$voucher_id = $objJournalVoucher->saveVoucher();

				if($voucher_id){
					$objJournalVoucher->voucherId    	= $voucher_id;

					//Sale Return
					$objJournalVoucher->accountCode     = $salesAccCode;
					$objJournalVoucher->accountTitle    = $salesAccTitle;
					$objJournalVoucher->narration       = (substr($customerAccCode,0,6) == '010101')?"Value of Goods Returned Against Bill No.".$billNumber:"Value of Goods Returned By ".$customerAccTitle." Against Bill# ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       	= $billAmount - $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					//Sale Tax Credit
					$objJournalVoucher->accountCode     = $taxAccCode;
					$objJournalVoucher->accountTitle    = $taxAccTitle;
					$objJournalVoucher->narration       = "Sales Tax Reversed Against Bill# ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       	= $taxAmount;

					$objJournalVoucher->saveVoucherDetail();

					//!Customer Debit
					$objJournalVoucher->accountCode     = $customerAccCode;
					$objJournalVoucher->accountTitle    = $customerAccTitle;
					$objJournalVoucher->narration       = $items_narration." Value of Goods Returned by ".$customerAccTitle." Against Bill # ".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       	= $billAmount;

					$objJournalVoucher->saveVoucherDetail();

					//CostaInventory
					$objJournalVoucher->accountCode     = $inventoryAccCode;
					$objJournalVoucher->accountTitle    = $inventoryAccTitle;
					$objJournalVoucher->narration       = $items_narration." Returned Against Bill # ".$billNumber;
					$objJournalVoucher->transactionType = 'Dr';
					$objJournalVoucher->amount       	= $costaSales;

					$objJournalVoucher->saveVoucherDetail();

					//CostaSales
					$objJournalVoucher->accountCode     = $costAccCode;
					$objJournalVoucher->accountTitle    = $costAccTitle;
					$objJournalVoucher->narration       = "Cost of sale Returned To Inventory against bill# ".$billNumber;
					$objJournalVoucher->transactionType = 'Cr';
					$objJournalVoucher->amount       	  = $costaSales;

					$objJournalVoucher->saveVoucherDetail();
				}

				$objSaleReturn->insertVoucherId($objSaleReturnDetails->sale_id,$voucher_id);
				$returnData['MSG'] = "Credit Memo # ".$objSaleReturn->billNum." Saved Successfully.";
				$returnData['ID'] = $objSaleReturnDetails->sale_id;
			}
		}
		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
