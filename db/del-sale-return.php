<?php
include('../common/db.connection.php');
include('../common/classes/sale_return.php');
include('../common/classes/sale_return_details.php');
include('../common/classes/branches.php');
include('../common/classes/branch_stock.php');
include('../common/classes/stock_transfer.php');
include('../common/classes/stock_transfer_detail.php');
include('../common/classes/items.php');
include('../common/classes/item_batch_stock.php');
include('../common/classes/j-voucher.php');

$objSale 		   		   			 = new SaleReturn();
$objSaledetails    		   	 = new SaleReturnDetails();
$objBranches  	           = new Branches();
$objBranchStock            = new BranchStock();
$objStockTransfer          = new StockTransfer();
$objStockTransferDetail    = new StockTransferDetail();
$objJournalVoucher 	       = new JournalVoucher();
$objItmes  		   	         = new Items();
$objItemBatchStock 				 = new ItemBatchStock();

function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
	$objItems            	   = new Items();
	$objBranchStock            = new BranchStock();
	if($branch_id > 0){
		$objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
	}else{
		$objItems->addStockValue($item_id,$quantity,$total_cost);
	}
}
function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
	$objItems            	   = new Items();
	$objBranchStock            = new BranchStock();
	if($branch_id > 0){
		$objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
	}else{
		$objItems->removeStockValue($item_id,$quantity,$total_cost);
	}
}
function get_branch_stock($brnch_id,$item_id){
	$objItems            	   = new Items();
	$objBranchStock            = new BranchStock();
	if($brnch_id > 0){
		return $objBranchStock->getItemStock($brnch_id,$item_id);
	}else{
		return $objItems->getStock($item_id);
	}
}

$return_data = array();

if(isset($_POST['cid'])){
	$sale_id = $_POST['cid'];
	$saleDetailList = $objSaledetails->getList($sale_id);
	$branch_id 			=  $objSale->getBranchId($sale_id);
	$voucher_id 		= $objSale->getVoucherId($sale_id);
	$success 				= array();
	$saleDetailList = $objSaledetails->getList($sale_id);
	if(mysql_num_rows($saleDetailList)){
		while($row = mysql_fetch_array($saleDetailList)){
			$item_id   = $row['ITEM_ID'];
			$quantity  = (float)$row['QUANTITY'];
			$itemStock = (float)(get_branch_stock($branch_id,$item_id));
			if($itemStock >= $quantity){
				$success[] = 'Y';
			}else{
				$success[] = 'N';
			}
		}
	}
	if(!in_array('N',$success)){
		$saleDetailList = $objSaledetails->getList($sale_id);
		if(mysql_num_rows($saleDetailList)){
			while($row = mysql_fetch_array($saleDetailList)){
				$avg_price = ($objItmes->getPurchasePrice($row['ITEM_ID'])) * $row['QUANTITY'];
				remove_branch_stock($item_id,$branch_id,$row['QUANTITY'],$avg_price);
				if($row['BATCH_NO']!=''){
					$objItemBatchStock->removeStock($row['ITEM_ID'],$row['BATCH_NO'],$row['EXPIRY_DATE'],$row['QUANTITY']);
				}
			}
		}
		$objSale->delete($sale_id);
		$objSaledetails->deleteCompleteBill($sale_id);

		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		$return_data['MSG'] = 'Record Deleted Successfully!';
		$return_data['OK']  = 'Y';
		echo json_encode($return_data);
		exit();
	}else{
		$return_data['MSG'] = 'Not Enough Stock!';
		$return_data['OK']  = 'N';
		echo json_encode($return_data);
		exit();
	}
}
mysql_close($con);
?>
