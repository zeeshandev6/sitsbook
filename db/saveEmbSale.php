<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/emb_sale.php');
	include('../common/classes/emb_sale_details.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	     = new ChartOfAccounts();
	$objEmbroiderySale         = new EmbroiderySale();
	$objEmbroiderySaleDetails  = new EmbroiderySaleDetails();
	$objJournalVoucher         = new JournalVoucher();

	$salesAccCode          = '0201010001';
	$salesAccTitle         = $objAccounts->getAccountTitleByCode($salesAccCode);

	$returnData            = array('MSG'=>'Nothing Happened','ID'=>0,'TOM'=>'');
	$success 							 = array();

	if(isset($_POST['jSonString'])){
		$sale_id                    					= (int)mysql_real_escape_string($_POST['sale_id']);
		$objEmbroiderySale->user_id 				  = (int)$_SESSION['classuseid'];
		$objEmbroiderySale->record_time 			= date('Y-m-d h:i:s');
		$objEmbroiderySale->sale_date					= date('Y-m-d',strtotime($_POST['sale_date']));
		$objEmbroiderySale->bill_no						= (int)mysql_real_escape_string($_POST['bill_no']);
		$objEmbroiderySale->lot_no						= mysql_real_escape_string($_POST['lot_no']);
		$objEmbroiderySale->gp_no							= mysql_real_escape_string($_POST['gp_no']);
		$objEmbroiderySale->subject						= mysql_real_escape_string(trim($_POST['subject']));
		$objEmbroiderySale->account_code			= mysql_real_escape_string($_POST['account_code']);
		$objEmbroiderySale->notes							= mysql_real_escape_string($_POST['inv_notes']);
		$jSonData 							 						  = json_decode($_POST['jSonString']);
		$transactions 					 						  = get_object_vars($jSonData);
		$deleted_rows 					 						  = NULL;
		if(isset($_POST['deleted_rows'])){
			$deleted_rows 					 = json_decode($_POST['deleted_rows']);
			$deleted_rows 					 = get_object_vars($deleted_rows);
		}
		if($sale_id == 0){
			$sale_id  = $objEmbroiderySale->save();
		}elseif($sale_id > 0){
			$objEmbroiderySale->update($sale_id);
		}
		if(count($deleted_rows)){
			foreach ($deleted_rows as $key => $detail_row_id) {
				$objEmbroiderySaleDetails->delete($detail_row_id);
			}
		}
		$total_amount = 0;
		if($sale_id>0 && isset($transactions)){
			$objEmbroiderySaleDetails->sale_id = $sale_id;
			foreach($transactions as $t=>$transaction_row){
				$sale_detail_id 												= $transaction_row->row_id;
				$objEmbroiderySaleDetails->machine_id 	= $transaction_row->machine_id;
				$objEmbroiderySaleDetails->machine_rate = $transaction_row->machine_rate;
				$objEmbroiderySaleDetails->design_no 		= $transaction_row->design_no;
				$objEmbroiderySaleDetails->stitches 		= $transaction_row->stitches;
				$objEmbroiderySaleDetails->unit_price 	= $transaction_row->rate;
				$objEmbroiderySaleDetails->total_amount = $transaction_row->amount;
				$objEmbroiderySaleDetails->qty_length   = $transaction_row->qty_length;
				$objEmbroiderySaleDetails->final_amount = $transaction_row->final_amount;
				$total_amount 							 					 += $objEmbroiderySaleDetails->final_amount;
				if($sale_detail_id==0){
					$objEmbroiderySaleDetails->save();
				}else{
					$objEmbroiderySaleDetails->update($sale_detail_id);
				}
			}
		}
		//voucher entry
		$sale_id = (int)$sale_id;
		if($sale_id){
			$voucher_id = $objEmbroiderySale->getVoucherId($sale_id);

			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($objEmbroiderySale->sale_date));
			$objJournalVoucher->voucherType    = 'SV';
			$objJournalVoucher->reference      = 'Sales Voucher';
			$objJournalVoucher->reference_date = date('Y-m-d',strtotime($objEmbroiderySale->sale_date));
			$objJournalVoucher->status	       = 'P';

			if($voucher_id == 0){
				$voucher_id = $objJournalVoucher->saveVoucher();
				$objEmbroiderySale->insertVoucherId($objEmbroiderySaleDetails->sale_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id>0){
				$objEmbroiderySale->insertVoucherId($sale_id,$voucher_id);

				$objJournalVoucher->voucherId 			= $voucher_id;

				$objJournalVoucher->accountCode  		= $objEmbroiderySale->account_code;
				$objJournalVoucher->accountTitle 		= $objAccounts->getAccountTitleByCode($objEmbroiderySale->account_code);
				if($objEmbroiderySale->subject!=''){
					$objJournalVoucher->narration  			= $objEmbroiderySale->subject;
				}else{
					$objJournalVoucher->narration    		= " Embroidery Service Invoice # ".$objEmbroiderySale->bill_no;
				}
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       		= $total_amount;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode  		= $salesAccCode;
				$objJournalVoucher->accountTitle 		= $salesAccTitle;
				$objJournalVoucher->narration    		= " Embroidery Service Invoice # ".$objEmbroiderySale->bill_no;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       		= $total_amount;

				$objJournalVoucher->saveVoucherDetail();
			}
			$returnData['MSG'] = "Success! Record saved Successfully.";
			$returnData['ID']  = $sale_id;
		}

		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
