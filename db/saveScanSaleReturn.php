<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/mobile-sale.php');
	include('../common/classes/mobile-sale-details.php');
	include('../common/classes/mobile-sale-return.php');
	include('../common/classes/mobile-sale-return-details.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/items.php');
	include('../common/classes/j-voucher.php');

	$objAccounts         	  = new ChartOfAccounts();
	$objScanSale          	  = new ScanSale();
	$objScanSaleDetails       = new ScanSaleDetails();
	$objScanSaleReturn   	  = new ScanSaleReturn();
	$objScanSaleReturnDetails = new ScanSaleReturnDetails();
	$objScanPurchaseDetails   = new ScanPurchaseDetails();
	$objJournalVoucher   	  = new JournalVoucher();
	$objItems 				  = new Items();

	$saleReturnAccCode  = '0201010003';  //Dr.
	$saleReturnAccTitle = $objAccounts->getAccountTitleByCode($saleReturnAccCode);

	$inv_acc_code  = '0101060001';   //Dr.
	$inv_acc_title = $objAccounts->getAccountTitleByCode($inv_acc_code);

	$costa_acc_code= '0301040001';   //Cr.
	$costa_acc_title = $objAccounts->getAccountTitleByCode($costa_acc_code);

	$salesTaxPayableAccCode  = '0401050001';
	$salesTaxPayableAccTitle = $objAccounts->getAccountTitleByCode($salesTaxPayableAccCode);

	$returnData = array('MSG'=>'Nothing Happened');
	$success = array();
	if(isset($_POST['jSonString'])){
		$objScanSaleReturn->ms_id 		= (isset($_POST['purchase_id']))?mysql_real_escape_string($_POST['purchase_id']):0;
		$objScanSaleReturn->return_date = date('Y-m-d',strtotime($_POST['return_date']));
		$objScanSaleReturn->sale_invoice_link = isset($_POST['sale_invoice_link'])?mysql_real_escape_string($_POST['sale_invoice_link']):"";
		$scanSaleDetailArray = $objScanSale->getDetail($objScanSaleReturn->ms_id);

		$bill_number        = mysql_real_escape_string($scanSaleDetailArray['BILL_NO']);
		$customer_acc_code  = mysql_real_escape_string($scanSaleDetailArray['CUST_ACC_CODE']);
		$customer_acc_title = $objAccounts->getAccountTitleByCode($customer_acc_code);
		if($scanSaleDetailArray == NULL){
			$returnData['MSG'] = 'Sales Invoice Not Found!';
			mysql_close($con);
exit();
		}
		$jSonData = json_decode($_POST['jSonString']);
		$transactions = get_object_vars($jSonData);
		$validated = true;
		// Validation
		foreach ($transactions as $key => $obj) {
			if($obj->msdid==0||$obj->price==0){
				$validated = false;
			}
		}
		if(!$validated){
			$returnData['MSG'] = 'There was an error in your form Please Recheck!';
			mysql_close($con);
exit();
		}
		$mr_id = $objScanSaleReturn->save();
		$bill_amount = 0;
		$cost_amount = 0;
		$tax_amount  = 0;
		if($mr_id){
			foreach ($transactions as $key => $obj) {
				$objScanSaleReturnDetails->ms_id  = $mr_id;
				$objScanSaleReturnDetails->msd_id = $obj->msdid;
				$objScanSaleReturnDetails->price  = $obj->price;
				$mrd_id = $objScanSaleReturnDetails->save();
				if($mrd_id){
					$ms_detail_row = $objScanSaleDetails->getDetails($obj->msdid);
					$disc_amnt     = ($ms_detail_row['PRICE']*$ms_detail_row['DISCOUNT'])/100;
					$sub_amnt      = $ms_detail_row['PRICE'] - $disc_amnt;
					$tax_amount   += (($sub_amnt*$ms_detail_row['TAX'])/100);

					$sp_d = $objScanPurchaseDetails->getDetailsByScanSaleDetailId($objScanSaleReturnDetails->msd_id);

					$bill_amount  += $obj->price;
					$cost_amount  += $objScanPurchaseDetails->getCostPriceExcludeTax($sp_d['ID']);
					$objScanPurchaseDetails->set_status($objScanSaleReturnDetails->msd_id, 'A');
					$objItems->addScanCost($sp_d['ITEM_ID'],$sp_d['PRICE']);
				}
			}
		}
		if($mr_id){
			$objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate = $objScanSaleReturn->return_date;
			//voucher number Saved and voucher id is set for voucher details
			$objJournalVoucher->voucherId   = $objJournalVoucher->saveVoucher();
			if($objJournalVoucher->voucherId){

				//supplier Debit Entry
				$objJournalVoucher->accountCode     = $saleReturnAccCode;
				$objJournalVoucher->accountTitle    = $saleReturnAccTitle;
				$objJournalVoucher->narration       = 'Goods Returned By '.$customer_acc_title.' Against Invoice # '.$bill_number;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount    	  	= $bill_amount - $tax_amount;
				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				//tax Debit Entry
				$objJournalVoucher->accountCode     = $salesTaxPayableAccCode;
				$objJournalVoucher->accountTitle    = $salesTaxPayableAccTitle;
				$objJournalVoucher->narration       = 'Sales Tax Refunded to customer Against Invoice # '.$bill_number;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount    	  	= $tax_amount;
				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				//supplier Debit Entry
				$objJournalVoucher->accountCode     = $customer_acc_code;
				$objJournalVoucher->accountTitle    = $customer_acc_title;
				$objJournalVoucher->narration       = 'Goods Returned By '.$customer_acc_title.' Against Invoice # '.$bill_number;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount    	  	= $bill_amount;
				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				//Inventory Debit Entry
				$objJournalVoucher->accountCode     = $inv_acc_code;
				$objJournalVoucher->accountTitle    = $inv_acc_title;
				$objJournalVoucher->narration       = 'Goods Returned By '.$customer_acc_title.' Against Invoice # '.$bill_number;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount    	  	= $cost_amount;
				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				//Costa Credit Entry
				$objJournalVoucher->accountCode     = $costa_acc_code;
				$objJournalVoucher->accountTitle    = $costa_acc_title;
				$objJournalVoucher->narration       = 'Goods Returned By '.$customer_acc_title.' Against Invoice # '.$bill_number;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount    	  	= $cost_amount;
				$voucher_detail_id = $objJournalVoucher->saveVoucherDetail();

				$objScanSaleReturn->insertVoucherId($mr_id, $objJournalVoucher->voucherId);
			}
		}
		$returnData['ID'] = $mr_id;
		if($mr_id){
			$returnData['MSG'] = 'Scan Returns Saved Successfully!';
		}else{
			$returnData['MSG'] = 'Error! Cannot Save Record!';
		}
		echo json_encode($returnData);
		mysql_close($con);
exit();
	}
?>
