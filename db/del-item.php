<?php
	include('../common/db.connection.php');
	include('../common/classes/items.php');

	$objItmes  		   = new Items;

	if(isset($_POST['id'])){
		$item_id = mysql_real_escape_string($_POST['id']);
		$beingUsed = $objItmes->beingUsed($item_id);
		if($beingUsed){
			echo json_encode(array('MSG'=>'Item is used in other records, It can not be deleted at this time.','OK'=>'N'));
		}else{
			$itemHasBeenDeleted = $objItmes->delete($item_id);
			if($itemHasBeenDeleted){
				$msg = 'Item Deleted Successfully!';
			}else{
				$msg = 'Error!, Item Could Not Be Deleted!';
			}
			echo json_encode(array('MSG'=>$msg,'OK'=>'Y'));
		}
	}
	mysql_close($con);
?>
