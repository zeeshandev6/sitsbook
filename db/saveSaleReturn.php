<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sale.php');
	include('../common/classes/sale_return.php');
	include('../common/classes/sale_return_details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');
	include('../common/classes/customers.php');
	include('../common/classes/items.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/userAccounts.php');

	$objAccounts         	   		= new ChartOfAccounts();
	$objSale			 	   					= new Sale();
	$objSaleReturn       	   		= new SaleReturn();
	$objSaleReturnDetails	   		= new SaleReturnDetails();
	$objBranches  	           	= new Branches();
	$objBranchStock            	= new BranchStock();
  $objStockTransfer          	= new StockTransfer();
  $objStockTransferDetail    	= new StockTransferDetail();
	$objCustomer 		       			= new Customers();
	$objItems                  	= new items();
	$objItemBatchStock 					= new ItemBatchStock();
	$objItemCategory           	= new itemCategory();
	$objJournalVoucher         	= new JournalVoucher();
	$objUserAccounts   	       	= new UserAccounts();

	function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new items();
        $objBranchStock          = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->addStockValue($item_id,$quantity,$total_cost);
        }
    }
    function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
        $objItems            	   = new items();
        $objBranchStock            = new BranchStock();
        if($branch_id > 0){
            $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
        }else{
            $objItems->removeStockValue($item_id,$quantity,$total_cost);
        }
    }

    $user_details  = $objUserAccounts->getDetails($_SESSION['classuseid']);
		$branch_id 	   = $user_details['BRANCH_ID'];

		//JV Account Codes Used.
		$taxAccCode  				= '0401050001';
		$taxAccTitle  			= $objAccounts->getAccountTitleByCode($taxAccCode);
		$costAccCode  			= '0301040001';
		$costAccTitle  			= $objAccounts->getAccountTitleByCode($costAccCode);
		$inventoryAccCode   = '0101060001'; //sale Returns a/c
		$inventoryAccTitle  = $objAccounts->getAccountTitleByCode($inventoryAccCode);
		$salesAccCode 			= '0201010003'; //inventory account
		$salesAccTitle 	    = $objAccounts->getAccountTitleByCode($salesAccCode);

		$returnData = array('MSG'=>'Memo Could Not Be Saved!','ID'=>0);

		$items_narration = "";

	if(isset($_POST['jSonString'])){
		$sale_id   					  						= (isset($_POST['sale_id']))?mysql_real_escape_string($_POST['sale_id']):"";
		$objSaleReturn->sale_id	 	  			= $sale_id;
		$objSaleReturn->sale_invoice_link = isset($_POST['sale_invoice_link'])?mysql_real_escape_string($_POST['sale_invoice_link']):"";
		$sale_return_id   			  				= (isset($_POST['sale_return_id']))?mysql_real_escape_string($_POST['sale_return_id']):0;
		$objSaleReturn->saleDate      		= date('Y-m-d',strtotime($_POST['saleDate']));
		$objSaleReturn->discount_type 		= mysql_real_escape_string($_POST['discount_type']);
		$objSaleReturn->bill_discount 		= mysql_real_escape_string($_POST['bill_discount']);
		$objSaleReturn->over_discount 		= mysql_real_escape_string($_POST['over_discount']);
		$objSaleReturn->registered_tax		= (isset($_POST['registered_tax']))?mysql_real_escape_string($_POST['registered_tax']):"";

		$over_total 				  						= mysql_real_escape_string($_POST['over_total']);

		if($sale_id > 0){
			$sale_details  			        		= $objSale->getDetail($sale_id);
			$objSaleReturn->billNum         = mysql_real_escape_string($sale_details['BILL_NO']);
			$objSaleReturn->customerAccCode = mysql_real_escape_string($sale_details['CUST_ACC_CODE']);
		}else{
			$objSaleReturn->billNum         = mysql_real_escape_string($_POST['billNum']);
			$objSaleReturn->customerAccCode = mysql_real_escape_string($_POST['customerCode']);
			$objSaleReturn->customer_name   = mysql_real_escape_string($_POST['customer_name']);
		}

		$cost_sales = 0;
		if($sale_return_id == 0){
			$success 				 = array();
			$sale_return_id  = $objSaleReturn->save();
			if($sale_return_id == 0){
				echo json_encode($returnData);
				mysql_close($con);
				exit();
			}
			$objSaleReturnDetails->sale_id = $sale_return_id;

			if(isset($_POST['deleted_rows'])){
				$deleted_rows = json_decode($_POST['deleted_rows']);
				$deleted_rows = get_object_vars($deleted_rows);

				foreach($deleted_rows as $key => $sale_return_detail_id){
					$return_detail_row = $objSaleReturnDetails->getDetails($sale_return_detail_id);
					$delted 					 = $objSaleReturnDetails->delete($sale_return_detail_id);
					if($delted){
						$icost  		 = $objSaleReturnDetails->getHistoryCostPrice($objSaleReturn->sale_id,$return_detail_row['ITEM_ID']);
						$icosta 		 = $return_detail_row['QUANTITY']*$icost;
						$cost_sales += round($costa_item,2);
						remove_branch_stock($return_detail_row['ITEM_ID'],$branch_id,$return_detail_row['QUANTITY'],$icosta);
					}
				}
			}

			$jSonData 		= json_decode($_POST['jSonString']);
			$transactions = get_object_vars($jSonData);

			//get Each Transaction Detail and Save.
			foreach($transactions as $t=>$tansaction){

				$sale_detail_id 										  = (int)$tansaction->row_id;

				$objSaleReturnDetails->item_id 	 		  = $tansaction->item_id;
				if(isset($tansaction->batch_no) && $tansaction->batch_no!=''){
					$objSaleReturnDetails->batch_no 		= $tansaction->batch_no;
					$objSaleReturnDetails->expiry_date 	= $tansaction->expiry_date;
				}
				$objSaleReturnDetails->qtyReceipt 		= $tansaction->quantity;
				$objSaleReturnDetails->unitPrice 	  	= $tansaction->unitPrice;
				$objSaleReturnDetails->saleDiscount   = $tansaction->discount;
				$objSaleReturnDetails->subTotal			  = $tansaction->subAmount;
				$objSaleReturnDetails->taxRate			  = $tansaction->taxRate;
				$objSaleReturnDetails->taxAmount		  = $tansaction->taxAmount;
				$objSaleReturnDetails->totalAmount		= $tansaction->totalAmount;

				if($sale_detail_id){
					$return_detail_row = $objSaleReturnDetails->getDetails($sale_detail_id);
					$updated 					 = $objSaleReturnDetails->update($sale_detail_id);
					if($updated){
						$icost  = $objSaleReturnDetails->getHistoryCostPrice($objSaleReturn->sale_id,$return_detail_row['ITEM_ID']);
						$icosta = $return_detail_row['QUANTITY']*$icost;
						$cost_sales += round($costa_item,2);
						remove_branch_stock($return_detail_row['ITEM_ID'],$branch_id,$return_detail_row['QUANTITY'],$icosta);
						if(isset($objSaleReturnDetails->batch_no)&&$objSaleReturnDetails->batch_no!=''){
							$objItemBatchStock->removeStock($return_detail_row['ITEM_ID'],$return_detail_row['BATCH_NO'],$return_detail_row['EXPIRY_DATE'],$return_detail_row['QUANTITY']);
						}
					}
				}else{
					$sale_detail_id = $objSaleReturnDetails->save();
				}

				//Update Correspoding item Stock.
				if($sale_detail_id){
					$item_costa  = $objSaleReturnDetails->getHistoryCostPrice($objSaleReturn->sale_id,$objSaleReturnDetails->item_id);
					$costa_item  = $item_costa*$objSaleReturnDetails->qtyReceipt;
					$cost_sales += round($costa_item,2);
					add_branch_stock($objSaleReturnDetails->item_id,$branch_id,$objSaleReturnDetails->qtyReceipt,$costa_item);
					if(isset($objSaleReturnDetails->batch_no) && $objSaleReturnDetails->batch_no!=''){
						$objItemBatchStock->addStock($objSaleReturnDetails->item_id,$objSaleReturnDetails->batch_no,$objSaleReturnDetails->expiry_date,$objSaleReturnDetails->qtyReceipt);
					}
					$item_name = $objItems->getItemTitle($objSaleReturnDetails->item_id);
					$txt       = "(".$item_name."-".$objSaleReturnDetails->qtyReceipt."@".$objSaleReturnDetails->unitPrice.")";
					$items_narration .= $txt;
				}
				$success[] = ($sale_detail_id)?"Y":"N";
			}
		}

		if($sale_return_id>0){
			$taxAmount 		= $objSaleReturn->getInventoryTaxAmountSum($objSaleReturnDetails->sale_id);
			$billAmount   = $over_total;

			$customerAccCode 	= $objSaleReturn->customerAccCode;
			$customerAccTitle = $objAccounts->getAccountTitleByCode($objSaleReturn->customerAccCode);
			$customerAccTitle = mysql_real_escape_string($customerAccTitle);

			$billNumber = $objSaleReturn->billNum;
			$entryDate  = $objSaleReturn->saleDate;

			//Sale Return Voucher
			$objJournalVoucher->jVoucherNum    = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    = date('Y-m-d',strtotime($entryDate));
			$objJournalVoucher->voucherType    = 'SR';
			$objJournalVoucher->reference   	 = 'Sales Returns Voucher';
			$objJournalVoucher->reference_date = date('Y-m-d',strtotime($entryDate));
			$objJournalVoucher->status	  		 = 'P';

			$voucher_id 												= $objJournalVoucher->saveVoucher();

			if($voucher_id){
				$objJournalVoucher->voucherId    	= $voucher_id;

				//Sale Return
				$objJournalVoucher->accountCode     = $salesAccCode;
				$objJournalVoucher->accountTitle    = $salesAccTitle;
				$objJournalVoucher->narration       = (substr($customerAccCode,0,6) == '010101')?"Value of Goods Returned Against Bill No.".$billNumber:"Value of Goods Returned By ".$customerAccTitle." Against Bill# ".$billNumber;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       		= $billAmount - $taxAmount;

				$objJournalVoucher->saveVoucherDetail();

				//Sale Tax Credit
				$objJournalVoucher->accountCode     = $taxAccCode;
				$objJournalVoucher->accountTitle    = $taxAccTitle;
				$objJournalVoucher->narration       = "Sales Tax Reversed Against Bill# ".$billNumber;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       	  = $taxAmount;

				$objJournalVoucher->saveVoucherDetail();

				//!Customer Debit
				$objJournalVoucher->accountCode     = $customerAccCode;
				$objJournalVoucher->accountTitle    = $customerAccTitle;
				$objJournalVoucher->narration       = $items_narration." Returned Against Bill # ".$billNumber;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       		= $billAmount;

				$objJournalVoucher->saveVoucherDetail();

				//CostaInventory
				$objJournalVoucher->accountCode     = $inventoryAccCode;
				$objJournalVoucher->accountTitle    = $inventoryAccTitle;
				$objJournalVoucher->narration       = $items_narration." Returned Against Bill # ".$billNumber;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount       		= $cost_sales;

				$objJournalVoucher->saveVoucherDetail();

				//CostaSales
				$objJournalVoucher->accountCode     = $costAccCode;
				$objJournalVoucher->accountTitle    = $costAccTitle;
				$objJournalVoucher->narration       = "Cost of sale Returned To Inventory against bill# ".$billNumber;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount       		= $cost_sales;

				$objJournalVoucher->saveVoucherDetail();
			}

			$objSaleReturn->insertVoucherId($objSaleReturnDetails->sale_id,$voucher_id);
			$returnData['MSG'] = "Credit Memo # ".$objSaleReturn->billNum." Saved Successfully.";
			$returnData['ID'] = $objSaleReturnDetails->sale_id;
		}

		echo json_encode($returnData);
		mysql_close($con);
		exit();
	}
?>
