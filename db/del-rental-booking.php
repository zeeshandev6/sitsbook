<?php
	include('../common/db.connection.php');
	include('../common/classes/rental_booking.php');
	include('../common/classes/ordering.php');
	include('../common/classes/rental_booking_receipts.php');
	include('../common/classes/j-voucher.php');

	$objRentalBooking = new RentalBooking();
	$objRentalBookingReceipts         = new RentalBookingReceipts();
	$objOrdering			= new Ordering();
	$objJournalVoucher= new JournalVoucher();

	$return_data = array();
	//delete inventory
	if(isset($_POST['cid'])){
		$rental_id = $_POST['cid'];

		$in_use = mysql_num_rows($objOrdering->getListByRental($rental_id));
		$in_use2= mysql_num_rows($objRentalBookingReceipts->getListByRbId($rental_id));

		if($in_use||$in_use2){
			if($in_use){
				$return_data['MSG'] = 'Error! Order has been placed against This Booking.';
				$return_data['OK']  = 'N';
			}
			if($in_use2){
				$return_data['MSG'] = 'Error! Voucher exists against This Booking.';
				$return_data['OK']  = 'N';
			}
		}else{
			$voucher_id  = $objRentalBooking->getVoucherId($rental_id);
			$voucher_id2 = $objRentalBooking->getReturnVoucherId($rental_id);

			$objRentalBooking->delete($rental_id);
			$objRentalBooking->delateRentalDetailListByRentalId($rental_id);

			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);

			if($voucher_id2){
				$objJournalVoucher->reverseVoucherDetails($voucher_id2);
				$objJournalVoucher->deleteJv($voucher_id2);
			}


			$return_data['MSG'] = 'Record Deleted Successfully!';
			$return_data['OK']  = 'Y';
		}

		echo json_encode($return_data);
		exit();
	}
	mysql_close($con);
?>
