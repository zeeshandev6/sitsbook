<?php
	include('../common/db.connection.php');
	include '../common/classes/cash_management.php';
	$objCashManagement = new CashManagement;
	if(isset($_POST['cash_id'])){
		$cash_id = $_POST['cash_id'];
		$messages = array();
		if($objCashManagement->delete($cash_id)){
			$messages['message'] = "1 Record Deleted Successfully!";
			$messages['confirmation'] = 1;
		}else{
			$messages['message'] = "Unable to delete Record!";
			$messages['confirmation'] = 0;
		}
		echo json_encode($messages);
	}
	mysql_close($con);
exit();
?>
