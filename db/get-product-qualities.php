<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/emb_inward.php');
	include('../common/classes/emb_lot_register.php');

	$objOutward 					 = new outward();
	$objInward 						 = new EmbroideryInward();
	$objLotRegisterDetails = new EmbLotRegister();

	if(isset($_POST['getDetail'])){
		$lotNum 										= mysql_real_escape_string($_POST['lotNum']);
		//$product_id 							= mysql_real_escape_string($_POST['product_id']);
		$customerCode 							= (isset($_POST['customerCode']))?mysql_real_escape_string($_POST['customerCode']):"";
		$outward_date 							= date('Y-m-d',strtotime($_POST['outward_date']));
		$inwdId 										= $objOutward->getInwardId($customerCode,$lotNum);
		$totalLotsIssued  					= $objLotRegisterDetails->getLotQuantityIssued($customerCode,$lotNum);
		$plainThansOutward 					= $objOutward->getPlainThansOutward($customerCode,$lotNum);
		//$inwardProductTotallength = $objInward->getInwardLotStockInHandWhloleLotByDate($customerCode,$lotNum,$outward_date);
		$inwardProductTotallength 	= $objInward->getInwardLotStockInHandWhloleLot($customerCode,$lotNum);

		$new_stock 								= $inwardProductTotallength - ($totalLotsIssued + $plainThansOutward) ;
		$getProductDetails 				= $objInward->getProductQualityQueryWholeLot($customerCode,$lotNum);
		$detail 									= array('ERROR'=>0);
		$detail['T_TOTAL'] 				= $new_stock;

		$detail['QUALITY_LIST']   = array();
		$detail['MEASURE_LIST']   = array();
		if(mysql_num_rows($getProductDetails)){
			while($row = mysql_fetch_assoc($getProductDetails)){
				$detail['QUALITY_LIST'][] = $row['QUALITY'];
				$detail['MEASURE_LIST'][] = $row['MEASURE_ID'];
			}
		}else{
			$detail['ERROR'] = 1;
		}
		$detail['QUALITY_LIST'] = array_unique($detail['QUALITY_LIST']);
		$detail['MEASURE_LIST'] = array_unique($detail['MEASURE_LIST']);
		echo json_encode($detail);
		exit();
	}

	if(isset($_POST['getDetailsForUpdate'])){
		$lotNum 									= mysql_real_escape_string($_POST['lotNum']);
		$prodCode 								= mysql_real_escape_string($_POST['prodCode']);
		$customerCode 						= mysql_real_escape_string($_POST['customerCode']);
		$outward_date 						= date('Y-m-d',strtotime($_POST['outward_date']));
		$inwdId 									= $objOutward->getInwardId($customerCode,$lotNum);
		$totalLotsIssued 					= $issuedLength = $objLotRegisterDetails->getLotQuantityIssued($customerCode,$lotNum);

		//$inwardProductTotallength = $objInward->getInwardLotStockInHandWhloleLotByDate($customerCode,$lotNum,$outward_date);

		$inwardProductTotallength = $objInward->getInwardLotStockInHandWhloleLot($customerCode,$lotNum);
		$plainThansOutward 					= $objOutward->getPlainThansOutward($customerCode,$lotNum);

		$newStock 				 				= $inwardProductTotallength - ($totalLotsIssued+$plainThansOutward);
		$getProductDetails 				= $objInward->getProductQualityQueryWholeLot($customerCode,$lotNum,$prodCode);
		$detail = array();
		if(mysql_num_rows($getProductDetails)){
			while($row = mysql_fetch_array($getProductDetails)){
				$detail[] = array('SUCCESS'=>'Y','QUALITY'=>$row['QUALITY'],'MEASURE_ID'=>$row['MEASURE_ID'],'T_TOTAL'=>$newStock);
			}
		}else{
			$detail[] = array('SUCCESS'=>'N');
		}
		echo json_encode($detail);
		exit();
	}
?>
