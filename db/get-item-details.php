<?php
	include('../common/db.connection.php');
	include('../common/classes/userAccounts.php');
	include('../common/classes/items.php');
	include('../common/classes/sale_details.php');
	include('../common/classes/inventoryDetails.php');
	include('../common/classes/mobile-purchase-details.php');
	include('../common/classes/branches.php');
	include('../common/classes/branch_stock.php');
	include('../common/classes/stock_transfer.php');
	include('../common/classes/stock_transfer_detail.php');

	$objuserAccounts 			= new userAccounts();
	$objItems 		 			= new Items();
	$objSaleDetails  			= new SaleDetails();
	$objScanPurchaseDetails 	= new ScanPurchaseDetails();
	$objInventoryDetails    	= new inventory_details();
	$objBranches  	           	= new Branches();
	$objBranchStock            	= new BranchStock();
	$objStockTransfer          	= new StockTransfer();
	$objStockTransferDetail    	= new StockTransferDetail();

	function get_branch_stock($brnch_id,$item_id){
		$objItems            	   = new Items();
		$objBranchStock          = new BranchStock();
		if($brnch_id > 0){
			return $objBranchStock->getItemStock($brnch_id,$item_id);
		}else{
			return $objItems->getStock($item_id);
		}
		mysql_close($con);
		exit();
	}


	if(isset($_POST['get_ps_price'])){
		$item_id 									= mysql_real_escape_string($_POST['get_ps_price']);
		$array   									= array();
		$details        					= $objScanPurchaseDetails->getPurchaseSalePrice($item_id);
		$array['STOCK'] 					= $objScanPurchaseDetails->get_item_stock($item_id);
		$array['PURCHASE_PRICE'] 	= $details['PURCHASE_PRICE'];
		$array['SALE_PRICE'] 	 		= $details['SALE_PRICE'];
		echo json_encode($array);
		mysql_close($con);
		exit();
	}

	if(isset($_POST['mobile_item_id'])){
		$item_id = mysql_real_escape_string($_POST['mobile_item_id']);
		$array = array();
		$array['STOCK'] = $objScanPurchaseDetails->get_item_stock($item_id);
		echo json_encode($array);
		mysql_close($con);
		exit();
	}

	if(isset($_POST['issue_item_id'])){
		$item_id   = mysql_real_escape_string($_POST['issue_item_id']);
		$itemStock = $objItems->getStock($item_id);
		$itemPrice = $objItems->getPurchasePrice($item_id);
		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$itemPrice));
		mysql_close($con);
		exit();
	}

	if(isset($_POST['item_id'])){
		$item_id    = mysql_real_escape_string($_POST['item_id']);
		$itemDetail = $objItems->getRecordDetails($item_id);
		echo json_encode(array('STOCK'=>$itemDetail['STOCK_QTY'],'P_PRICE'=>$itemDetail['PURCHASE_PRICE'],'PER_CARTON'=>$itemDetail['QTY_PER_CARTON']));
		mysql_close($con);
		exit();
	}

	if(isset($_POST['p_item_id'])){
		$item_id   = mysql_real_escape_string($_POST['p_item_id']);
		$itemStock = $objItems->getStock($item_id);
		if(isset($_POST['get_item_cost'])){
			$p_price   = $objItems->getPurchasePrice($item_id);
		}else{
			$p_price   = $objInventoryDetails->getLastPurchasedItemRecord($item_id);
		}
		$item_details  = $objItems->getRecordDetails($item_id);

		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$p_price,'QTY_CARTON'=>$item_details['QTY_PER_CARTON'],'DOZEN_RATE'=>$item_details['DOZEN_RATE']));
		mysql_close($con);
		exit();
	}

	if(isset($_POST['s_item_id'])){
		$show_costa =  isset($_SESSION['TOOLTIP_COST_PRICE'])?$_SESSION['TOOLTIP_COST_PRICE']:"";
		$branch_id  =  $objuserAccounts->getBranchId($user_id);
		$item_id 		= mysql_real_escape_string($_POST['s_item_id']);
		$stock_modifier = 0;
		if(isset($_POST['row_id'])){
			$row_id  = mysql_real_escape_string($_POST['row_id']);
			if($row_id > 0){
				$stock_modifier = $objSaleDetails->getQty($row_id);
			}
		}
		$the_item     		= $objItems->getRecordDetails($item_id);
		$itemStock    		= get_branch_stock($branch_id,$item_id);
		$qty_carton   		= $the_item['QTY_PER_CARTON'];
		$rate_carton  		= $the_item['RATE_CARTON'];
		$carton_dozen  		= $the_item['CARTON_DOZEN'];
		$dozen_rate  		= $the_item['DOZEN_RATE'];
		$itemPrice    		= $the_item['SALE_PRICE'];
		$itemDiscount 		= $the_item['ITEM_DISCOUNT'];
		if($show_costa=='Y'){
			$itemCost    = $the_item['PURCHASE_PRICE'];
		}else{
			$itemCost    		= 0;
		}
		$itemStock   += $stock_modifier;
		$data   = array('STOCK'=>$itemStock,'S_PRICE'=>$itemPrice,'DISCOUNT'=>$itemDiscount,'QTY_CARTON'=>$qty_carton,'RATE_CARTON'=>$rate_carton);
		$data['CARTON_DOZEN'] = $carton_dozen;
		$data['DOZEN_RATE'] = $dozen_rate;
		if($show_costa=='Y'){
			$data['AVG_COSTA'] = $itemCost;
		}
		echo json_encode($data);
		mysql_close($con);
		exit();
	}

	if(isset($_GET['barcode'])){
		$barcode = mysql_real_escape_string($_GET['barcode']);
		$array = $objItems->getDetailByBarcode($barcode);
		if($array!=NULL){
			echo json_encode($array);
		}
		mysql_close($con);
		exit();
	}
?>
