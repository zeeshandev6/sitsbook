<?php
	include('../common/db.connection.php');
	include('../common/classes/items.php');
	include('../common/classes/item_batch_stock.php');
	include('../common/classes/inventory.php');
	include('../common/classes/inventoryDetails.php');
	include('../common/classes/inventory-return.php');
	include('../common/classes/inventory-return-details.php');
	include('../common/classes/sale.php');
	include('../common/classes/sale_return.php');
	include('../common/classes/sale_return_details.php');

	$objItems                  = new Items();
	$objItemBatchStock 				 = new ItemBatchStock();
	$objInventory              = new inventory();
	$objInventoryDetails       = new inventory_details();
	$objInventoryReturn        = new InventoryReturn();
	$objInventoryReturnDetails = new InventoryReturnDetails();
	$objSale	                 = new Sale();
	$objSaleReturn             = new SaleReturn();
	$objSaleReturnDetails      = new SaleReturnDetails();

	if(isset($_POST['p_item_id'])&&isset($_POST['purchase_id'])){
		$purchase_id 	= mysql_real_escape_string($_POST['purchase_id']);
		$item_id 			= mysql_real_escape_string($_POST['p_item_id']);
		$batch_no 		= isset($_POST['batch_no'])?mysql_real_escape_string($_POST['batch_no']):"";
		$expiry_date	= isset($_POST['expiry_date'])?date('Y-m-d',strtotime($_POST['expiry_date'])):"";

		$items_returned = $objInventoryReturn->get_item_qty_by_bill($purchase_id, $item_id);
		$item_limit     = $objInventory->get_item_qty_by_bill($purchase_id, $item_id);
		if($batch_no!=''){
			$itemStock 			= $objItemBatchStock->getItemBatchCurrentStock($item_id,$batch_no,$expiry_date);
		}else{
			$itemStock 			= $objItems->getStock($item_id);
		}
		$itemPrice 				= $objItems->getPurchasePrice($item_id);
		$item_limit 		 -= $items_returned;
		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$itemPrice,'RETURNED'=>$items_returned,'LIMIT'=>$item_limit));
		mysql_close($con);
		exit();
	}
	if(isset($_POST['p_item_id'])&&isset($_POST['sale_id'])){
		$sale_id 			= mysql_real_escape_string($_POST['sale_id']);
		$item_id 			= mysql_real_escape_string($_POST['p_item_id']);
		$batch_no 		= isset($_POST['batch_no'])?mysql_real_escape_string($_POST['batch_no']):"";
		$expiry_date	= isset($_POST['expiry_date'])?date('Y-m-d',strtotime($_POST['expiry_date'])):"";

		$items_returned = $objSaleReturn->get_item_qty_by_bill($sale_id, $item_id);
		$item_limit     = $objSale->get_item_qty_by_bill($sale_id, $item_id);
		if($batch_no==''){
			$itemStock 			= $objItems->getStock($item_id);
		}else{
			$itemStock 			= $objItemBatchStock->getItemBatchCurrentStock($item_id,$batch_no,$expiry_date);
		}
		$itemPrice 			= $objItems->getSalePrice($item_id);
		$qty_carton     = $objItems->getItemQtyPerCarton($item_id);

		$item_limit -= $items_returned;

		echo json_encode(array('STOCK'=>$itemStock,'S_PRICE'=>$itemPrice,'RETURNED'=>$items_returned,'LIMIT'=>$item_limit,'QTY_CARTON'=>$qty_carton));
		mysql_close($con);
		exit();
	}
	if(isset($_POST['p_item_id'])){
		$item_id = mysql_real_escape_string($_POST['p_item_id']);
		$itemStock = $objItems->getStock($item_id);
		$itemPrice = $objItems->getPurchasePrice($item_id);
		echo json_encode(array('STOCK'=>$itemStock,'P_PRICE'=>$itemPrice));
		mysql_close($con);
		exit();
	}
	if(isset($_POST['s_item_id'])){
		$item_id = mysql_real_escape_string($_POST['s_item_id']);
		$itemStock = $objItems->getStock($item_id);
		$itemPrice = $objItems->getSalePrice($item_id);
		echo json_encode(array('STOCK'=>$itemStock,'S_PRICE'=>$itemPrice));
		mysql_close($con);
exit();
	}
?>
