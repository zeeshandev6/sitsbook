<?php
	include('../common/db.connection.php');
	include("../common/classes/daily-expenditure-management.php");
	include('../common/classes/j-voucher.php');


	$objExpoCashReciepts = new DailyExpenditure;
	$objJournalVoucher = new JournalVoucher;

	$returnMessage = array('OK'=>'N','MSG'=>'Error Deleting Record!');

	if(isset($_POST['id'])){
		$expoReceipt_id = $_POST['id'];

		$voucher_id = $objExpoCashReciepts->getVoucherId($expoReceipt_id);

		$deleted = $objExpoCashReciepts->delete($expoReceipt_id);
		if($deleted){
			$objJournalVoucher->reverseVoucherDetails($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
			$returnMessage = array('OK'=>'Y','MSG'=>'Record Deleted Successfully');
			echo json_encode($returnMessage);

		}else{
			echo json_encode($returnMessage);
		}
	}
	mysql_close($con);
?>
