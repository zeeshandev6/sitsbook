<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/payrolls.php');
	include('../common/classes/measure.php');
	include('../common/classes/machines.php');
	include('../common/classes/emb_products.php');
	include('../common/classes/emb_lot_register.php');
	require('../common/classes/userAccounts.php');

	$objoutward 			= new outward();
	$objMeasures 			= new Measures();
	$objMachines 			= new Machines();
	$objEmbProducts 	= new EmbProducts();
	$objPayrolls 		 	= new Payrolls();
	$objLotRegister 	= new EmbLotRegister();
	$objAccounts    	= new UserAccounts();

	$permissionz = $objAccounts->getPermissions($user_id);
	$permissionz = explode('|',$permissionz);

	$billing_types 			= array();
	$billing_types['S'] = 'Stitches';
	$billing_types['Y'] = 'Yards';
	$billing_types['U'] = 'Suites';
	$billing_types['L'] = 'Laces';

	if(isset($_POST['customerCode'])){
		$customerCode  = mysql_real_escape_string($_POST['customerCode']);
		$completedLots = $objLotRegister->getListByCustomerIncParials($customerCode);
		$lengthTotal   = 0;
		$embRateTotal  = 0;
		$stitchCount   = 0;
		$stitchTotal   = 0;
		$lacesTotal    = 0;
?>
		<tbody>
<?php
		if(mysql_num_rows($completedLots)){
?>
<?php
			while($lotRow = mysql_fetch_array($completedLots)){
	      $measurement 		= $objMeasures->getName($lotRow['MEASURE_ID']);
	      $stitchMan 			= $objPayrolls->getTitle($lotRow['STITCH_ACC']);
				$product_name 	= $objEmbProducts->getTitle($lotRow['PRODUCT_ID']);

				$partial_row = $objoutward->getLengthIfPartial($lotRow['ID']);
				if($partial_row['PARTIAL_LENGTH'] != 0){
					if($partial_row['PARTIAL_LENGTH'] == $lotRow['MEASURE_LENGTH']){
						continue;
					}
					//$lotRow['STITCHES'] 			-= $partial_row['TOTAL_STITCHES'];
					$lotRow['MEASURE_LENGTH'] -= $partial_row['PARTIAL_LENGTH'];
					$lotRow['TOTAL_LACES']    -= $partial_row['TOTAL_PRODUCTION'];

					if($lotRow['BILLING_TYPE'] == 'S' || $lotRow['BILLING_TYPE'] == 'L'){
						$lotRow['STITCH_AMOUNT']   = ( ($lotRow['STITCHES']*15)/1000 ) * $lotRow['STITCH_RATE'] * $lotRow['MEASURE_LENGTH'];
					}
					if($lotRow['BILLING_TYPE'] == 'L'){
						$lotRow['EMB_AMOUNT']   = $lotRow['EMB_RATE'] * $lotRow['TOTAL_LACES'];
					}
					if($lotRow['BILLING_TYPE'] == 'Y'){
						$lotRow['EMB_AMOUNT']   = $lotRow['EMB_RATE'] * $lotRow['TOTAL_LACES'] * 15;
					}
					if($lotRow['BILLING_TYPE'] == 'U'){
						$lotRow['EMB_AMOUNT']   = $lotRow['EMB_RATE'] * $lotRow['TOTAL_LACES'];
					}
				}
?>
        <tr class="alt-row calculations lot-register-rows" data-lot-id="<?php echo $lotRow['ID']; ?>">
            <td class="text-center product_id" data-product-id="<?php echo $lotRow['PRODUCT_ID']; ?>"><?php echo $product_name; ?></td>
            <td class="text-center lot_no"><?php echo $lotRow['LOT_NO']; ?></td>
            <td class="text-center quality"><?php echo $lotRow['QUALITY']; ?></td>
            <td class="text-center measure_id" data-measure-id="<?php echo $lotRow['MEASURE_ID']; ?>"><?php echo $measurement; ?></td>
						<td class="text-center billing_type" data-billing-type="<?php echo $lotRow['BILLING_TYPE']; ?>"><?php echo $billing_types[$lotRow['BILLING_TYPE']]; ?></td>
						<td class="text-center stitches"><?php echo $lotRow['STITCHES']; ?></td>
						<td class="text-center total_laces"><?php echo $lotRow['TOTAL_LACES']; ?></td>
            <td class="text-center measure_length lengthColumn"><?php echo $lotRow['MEASURE_LENGTH']; ?></td>
            <td class="text-center emb_rate"><?php echo $lotRow['EMB_RATE']; ?></td>
            <td class="text-center emb_amount embAmountColumn"><?php echo $lotRow['EMB_AMOUNT']; ?></td>
            <td class="text-center stitch_rate"><?php echo $lotRow['STITCH_RATE']; ?></td>
            <td class="text-center stitch_amount stichAmountColumn"><?php echo $lotRow['STITCH_AMOUNT']; ?></td>
            <td class="text-center stitch_acc" data-stitch-id="<?php echo $lotRow['STITCH_ACC']; ?>"><?php echo $stitchMan; ?></td>
            <td class="text-center design_code"><?php echo $lotRow['DESIGN_CODE']; ?></td>
            <td class="text-center machine_id"><?php echo $lotRow['MACHINE_ID']; ?></td>
						<td>
							<a class="quick_edit" id='view_button' do="<?php echo $lotRow['ID']; ?>" title="Modify"><i class="fa fa-pencil"></i></a>
						</td>
            <td style="text-align:center">
								<input type="checkbox" class="lotCheckBox css-checkbox" id="lot-checkv-<?php echo $lotRow['ID']; ?>" value="<?php echo $lotRow['ID']; ?>" />
								<label for="lot-checkv-<?php echo $lotRow['ID']; ?>" class="css-label"></label>
            </td>
        </tr>
<?php
				$lengthTotal 	+= $lotRow['MEASURE_LENGTH'];
				$embRateTotal += $lotRow['EMB_AMOUNT'];
				$stitchTotal 	+= $lotRow['STITCH_AMOUNT'];
				$stitchCount  += $lotRow['STITCHES'];
				$lacesTotal   += $lotRow['TOTAL_LACES'];
			}
		}
?>
									<tr class="totals">
                    <td class="text-center" colspan="5" >Total</td>
										<td class="text-center sumStitchTotal"><?php echo $stitchCount; ?></td>
										<td class="text-center sumLacesTotal"><?php echo $lacesTotal; ?></td>
                    <td class="text-center sumLenTotal"><?php echo $lengthTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center embAmountTotal"><?php echo $embRateTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center stichAmountTotal"><?php echo $stitchTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
									</tr>
	</tbody>
<?php
	}
	if(isset($_POST['custCode'])){
		$outward_id = $_POST['outward_id'];
		$customerCode = mysql_real_escape_string($_POST['custCode']);

		$objLotRegister->lotStatus = 'C';
		$objLotRegister->customerAccCode = $_POST['custCode'];

		$processedLots = $objLotRegister->search();

		$selectedLots = $objoutward->getDetailsById($outward_id);
		$outward = mysql_fetch_array($objoutward->getSpecificCustomerOutward($outward_id));
?>

<?php
			$lengthTotal 	 = 0;
			$embRateTotal  = 0;
			$stitchTotal 	 = 0;
			$stitchCount   = 0;
			$lacesTotal    = 0;
      if(mysql_num_rows($processedLots)){
?>
				<tbody class='removee'>
<?php
                while($row = mysql_fetch_array($processedLots)){
                    $measurement 	= $objMeasures->getName($row['MEASURE_ID']);
                    $stitchMan 		= $objPayrolls->getTitle($row['STITCH_ACC']);
										$product_name = $objEmbProducts->getTitle($lotRow['PRODUCT_ID']);
?>
                        <tr class="alt-row calculations">
                            <td class="text-center"><?php echo $product_name; ?></td>
                            <td class="text-center"><?php echo $row['LOT_NO']; ?></td>
                            <td class="text-center"><?php echo $row['QUALITY']; ?></td>
                            <td class="text-center"><?php echo $measurement; ?></td>
														<td class="text-center"><?php echo $billing_types[$row['BILLING_TYPE']]; ?></td>
														<td class="text-center"><?php echo $lotRow['STITCHES']; ?></td>
														<td class="text-center"><?php echo $lotRow['TOTAL_LACES']; ?></td>
                            <td class="text-center lengthColumn"><?php echo $row['MEASURE_LENGTH']; ?></td>
                            <td class="text-center"><?php echo $row['EMB_RATE']; ?></td>
                            <td class="text-center embAmountColumn"><?php echo $row['EMB_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $row['STITCH_RATE']; ?></td>
                            <td class="text-center stichAmountColumn"><?php echo $row['STITCH_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $stitchMan; ?></td>
                            <td class="text-center"><?php echo $row['DESIGN_CODE']; ?></td>
                            <td class="text-center"><?php echo $row['MACHINE_ID']; ?></td>
                            <td class="text-center"><?php echo $row['GP_NO']; ?></td>
                            <td>
<?php
				if(isset($permissionz)&&in_array('emb-outward-modify',$permissionz) || $admin == true){
					if($outward['VOUCHER_ID'] == 0){
?>

<?php
					}
				}
?>
                            </td>
                            <td style="text-align:center">
                            	<input type="checkbox" class="lotCheckBox" id="lot-chex-<?php echo $row['ID']; ?>"  <?php echo ($outward['VOUCHER_ID'] == 0)?"":"disabled"; ?>  value="<?php echo $row['ID']; ?>" />
															<label for="lot-chex-<?php echo $row['ID']; ?>" class="css-label"></label>
                            </td>
                        </tr>
<?php
						$lengthTotal  += $row['MEASURE_LENGTH'];
						$embRateTotal += $row['EMB_AMOUNT'];
						$stitchTotal  += $row['STITCH_AMOUNT'];
						$stitchCount  += $row['STITCHES'];
						$lacesTotal   += $lotRow['TOTAL_LACES'];
                }
?>

						<tr class="totals">
              <td class="text-center" colspan="5">Total</td>
							<td class="text-center sumStitchTotal"><?php echo $stitchCount; ?></td>
              <td class="text-center sumLenTotal"><?php echo $lengthTotal; ?></td>
              <td class="text-center">- - -</td>
              <td class="text-center embAmountTotal"><?php echo $embRateTotal; ?></td>
              <td class="text-center">- - -</td>
              <td class="text-center stichAmountTotal"><?php echo $stitchTotal; ?></td>
              <td class="text-center">- - -</td>
              <td class="text-center">- - -</td>
              <td class="text-center">- - -</td>
              <td class="text-center">- - -</td>
              <td class="text-center">- - -</td>
              <td class="text-center">- - -</td>
	          </tr>
<?php
			}
?>
<?php
			unset($processedLots);
            if(isset($processedLots)){
								$lengthTotal  = 0;
								$embRateTotal = 0;
								$stitchTotal  = 0;
								$stitchCount  = 0;
                while($row = mysql_fetch_array($processedLots)){
                    $measurement = $objMeasures->getName($row['MEASURE_ID']);
                    $stitchMan = $objPayrolls->getTitle($row['STITCH_ACC']);
					if($row['BILL_STATUS'] !== 'Y'){
?>
                        <tr class="alt-row calculations">
                            <td class="text-center"><?php echo $row['PRODUCT_ID']; ?></td>
                            <td class="text-center"><?php echo $row['LOT_NO']; ?></td>
                            <td class="text-center"><?php echo $row['QUALITY']; ?></td>
                            <td class="text-center"><?php echo $measurement; ?></td>
														<td class="text-center"><?php echo $billing_types[$row['BILLING_TYPE']]; ?></td>
														<td class="text-center"><?php echo $lotRow['STITCHES']; ?></td>
														<td class="text-center"><?php echo $lotRow['TOTAL_LACES']; ?></td>
                            <td class="text-center lengthColumn"><?php echo $row['MEASURE_LENGTH']; ?></td>
                            <td class="text-center"><?php echo $row['EMB_RATE']; ?></td>
                            <td class="text-center embAmountColumn"><?php echo $row['EMB_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $row['STITCH_RATE']; ?></td>
                            <td class="text-center stichAmountColumn"><?php echo $row['STITCH_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $stitchMan; ?></td>
                            <td class="text-center"><?php echo $row['DESIGN_CODE']; ?></td>
                            <td class="text-center"><?php echo $row['MACHINE_ID']; ?></td>
                            <td class="text-center"><?php echo $row['GP_NO']; ?></td>
                            <td class="text-center">
<?php
				if(isset($permissionz)&&in_array('emb-lot-register-modify',$permissionz) || $admin == true){
					if($outward['VOUCHER_ID'] == 0){
?>
                            	<a class="editDetailsFrom" id='view_button' do="<?php echo $row['ID']; ?>" title="Modify"><i class="fa fa-pencil"></i></a>
<?php
					}
				}
?>
                            </td>
                            <td class="text-center">
                            	<input type="checkbox" class="lotCheckBox" value="<?php echo $row['ID']; ?>" />
                            </td>
                        </tr>
<?php
						$lengthTotal  += $row['MEASURE_LENGTH'];
						$embRateTotal += $row['EMB_AMOUNT'];
						$stitchTotal  += $row['STITCH_AMOUNT'];
						$stitchCount  += $row['STITCHES'];
					}
                }
?>
                    </tbody>

<?php
			}else{
?>
				<tbody class='removee'><tr class="calculations" style="display:none;"></tr></tbody>
<?php
			}
	}elseif(isset($_POST['getLotList'])){
		$objLotRegister->customerAccCode = $_POST['customer_code'];
		if($_POST['lot_date'] != 'no_date'){
			$objLotRegister->lot_date = date('Y-m-d',strtotime($_POST['lot_date']));
		}
		if(isset($_POST['no_checks'])&&$_POST['no_checks'] == true){
			$Show_checks = false;
		}else{
			$Show_checks = true;
		}
		$underProcessLots = $objLotRegister->getBothLotsByCust($objLotRegister->customerAccCode);
?>
<?php
			$stitchesTotal 	= 0;
			$lengthTotal 		= 0;
			$embRateTotal	 	= 0;
			$stitchTotal 		= 0;
			$stitchCount    = 0;
			$lacesTotal     = 0;
            if(mysql_num_rows($underProcessLots)){
?>
				<tbody class='removee'>
<?php
                while($row = mysql_fetch_array($underProcessLots)){
										if(!$objLotRegister->checkIfClaimIsTransferred($row['ID']) || ($Show_checks && $row['LOT_STATUS'] == 'R')){
											continue;
										}
                    $measurement 	 = $objMeasures->getName($row['MEASURE_ID']);
                    $stitchMan 	 	 = $objPayrolls->getTitle($row['STITCH_ACC']);
										$product_title = $objEmbProducts->getTitle($row['PRODUCT_ID']);
?>
                        <tr class="alt-row calculations row-id-<?php echo $row['ID']; ?> <?php echo ($row['LOT_STATUS'] == 'R')?"innerTdRed":""; ?>">
                            <td class="text-left" data-id="<?php echo $row['PRODUCT_ID']; ?>"><?php echo $product_title; ?></td>
                            <td class="text-center"><?php echo $row['LOT_NO']; ?></td>
                            <td class="text-center"><?php echo $row['QUALITY']; ?></td>
                            <td class="text-center"><?php echo $measurement; ?></td>
														<td class="text-center"><?php echo $billing_types[$row['BILLING_TYPE']]; ?></td>
														<td class="text-center"><?php echo $row['STITCHES']; ?></td>
														<td class="text-center"><?php echo $row['TOTAL_LACES']; ?></td>
                            <td class="text-center lengthColumn"><?php echo $row['MEASURE_LENGTH']; ?></td>
                            <td class="text-center"><?php echo $row['EMB_RATE']; ?></td>
                            <td class="text-center embAmountColumn" ><?php echo $row['EMB_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $row['STITCH_RATE']; ?></td>
                            <td class="text-center stichAmountColumn" ><?php echo $row['STITCH_AMOUNT']; ?></td>
                            <td class="text-center"><?php echo $stitchMan; ?></td>
                            <td class="text-center"><?php echo $row['DESIGN_CODE']; ?></td>
                            <td class="text-center"><?php echo $objMachines->getName($row['MACHINE_ID']); ?></td>
                            <td class="text-center"><?php echo $row['GP_NO']; ?></td>
														<td class="text-center">- - -</td>
                            <td>
<?php
														if(isset($permissionz)&&in_array('emb-lot-register-modify',$permissionz) || $admin == true){
?>
															<a class="editDetailsFrom" id='view_button' do="<?php echo $row['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
<?php
														}
					if(!$Show_checks){
						if(isset($permissionz)&&in_array('emb-lot-register-delete',$permissionz) || $admin == true){
?>
                            	<a class="pointer" do='<?php echo $row['ID']; ?>' title='Delete' ><i class="fa fa-times"></i></a>
<?php
						}
					}
?>
                            </td>

<?php
						if($Show_checks){
?>
														<td class="text-center">
                            	<input type="checkbox" class="lotCheckBox" value="<?php echo $row['ID']; ?>" />
                            </td>
<?php
						}
?>
                        </tr>
<?php
														$stitchesTotal += $row['STITCHES'];
														$lengthTotal 	 += $row['MEASURE_LENGTH'];
														$embRateTotal  += $row['EMB_AMOUNT'];
														$stitchTotal   += $row['STITCH_AMOUNT'];
														$stitchCount   += $row['STITCHES'];
														$lacesTotal    += $row['TOTAL_LACES'];
				}
?>
                        <tr class="totals">
	                        <td class="text-right" colspan="5">Total</td>
													<td class="text-center sumStitchesTotal"><?php echo $stitchCount; ?></td>
													<td class="text-center sumLacesTotal"><?php echo $lacesTotal; ?></td>
	                        <td class="text-center sumLenTotal"><?php echo $lengthTotal; ?></td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center embAmountTotal"><?php echo $embRateTotal; ?></td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center stichAmountTotal"><?php echo $stitchTotal; ?></td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center">- - -</td>
	                        <td class="text-center">- - -</td>
                        </tr>
                    </tbody>
<?php
			}else{
?>
					<tbody class='removee'><tr class="calculations" style="display:none;"></tr></tbody>
<?php
			}
	}
?>
