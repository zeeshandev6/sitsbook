<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_lot_register.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/emb_outward.php');
	
	$objLotRegisterDetails = new EmbLotRegister();
	$objJournalVoucher     = new JournalVoucher();
	$objOutward  	 	   = new outward();
	
	if(isset($_POST['lid'])){
		$lid = $_POST['lid'];
		$details = $objLotRegisterDetails->getDetail($lid);
		$in_outward = $objLotRegisterDetails->checkIfClaimIsTransferred($lid);
		if($details['LOT_STATUS'] == 'R' && !$in_outward){
			echo 0;
			exit();
		}
		$voucher_id = $objLotRegisterDetails->getVoucherId($lid);
		$deleted = $objLotRegisterDetails->delete($lid);
		if($deleted && $voucher_id > 0){
			$objJournalVoucher->deleteVoucher($voucher_id);
			$objJournalVoucher->deleteJv($voucher_id);
		}
		echo $deleted;
	}
?>