<?php
	include('../common/db.connection.php');
	include('../common/classes/accounts.php');
	include('../common/classes/sale.php');
	include('../common/classes/sale_return.php');
	include('../common/classes/sale_return_details.php');
	include('../common/classes/items.php');
	include('../common/classes/itemCategory.php');
	include('../common/classes/j-voucher.php');

	$objAccounts = new ChartOfAccounts;
	$objSale = new Sale;
	$objSaleReturn = new SaleReturn;
	$objSaleReturnDetails = new SaleReturnDetails;
	$objItems = new Items;
	$objItemCategory = new itemCategory;

	$sale_id = mysql_real_escape_string($_GET['sale_id']);
		$sale_returns = $objSaleReturn->getListBySaleId($sale_id);
		if(mysql_num_rows($sale_returns)&&$sale_id > 0){
			while($sale_return_main = mysql_fetch_array($sale_returns)){
				$sale_return_details = $objSaleReturnDetails->getList($sale_return_main['ID']);
					$qty = 0;
					$stotal = 0;
					$ttotal = 0;
					$atotal = 0;
				if(mysql_num_rows($sale_return_details)){
?>
					<div id="panel-<?php echo $sale_return_main['ID']; ?>">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            	<div class="caption_simple">
                            		Return Date : <?php echo date('d-m-Y',strtotime($sale_return_main['SALE_DATE'])) ?>
                            	</div>
                            	<div class="pull-right">
                            		<button class="btn btn-default btn-sm delete_sale_return" do="<?php echo $sale_return_main['ID']; ?>"><span class="fa fa-times"></span></button>
                            	</div>
                            </div>
                            <table class="wel_made">
                            <thead>
                                <tr>
                                   <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Discount %</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="12%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
							while($invRow = mysql_fetch_array($sale_return_details)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
?>
                                <tr class="alt-row" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td style="text-align:left;"
                                    	class="itemName"
                                        data-item='<?php echo $invRow['ITEM_ID']; ?>'>
										<?php echo $itemName; ?>
                                    </td>
                                    <td style="text-align:center;"><?php echo $invRow['QUANTITY'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                    <td style="text-align:right;"><?php  echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td style="text-align:center;"><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td style="text-align:right;"><?php  echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td style="text-align:right;"><?php  echo $invRow['TOTAL_AMOUNT'] ?></td>
                                </tr>
<?php
								$qty += $invRow['QUANTITY'];
								$stotal += $invRow['SUB_AMOUNT'];
								$ttotal += $invRow['TAX_AMOUNT'];
								$atotal += $invRow['TOTAL_AMOUNT'];
							}
?>
                                <tr class="totals">
                                    	<td style="text-align:center;background-color:#EEEEEE;">Total</td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo $qty; ?></td>
                                        <td style="text-align:center;background-color:#EEE;"> - - - </td>
                                        <td style="text-align:right;background-color:#f5f5f5;"> - - - </td>
                                        <td style="text-align:center;background-color:#f5f5f5;"><?php echo number_format($stotal,2); ?></td>
                                        <td style="text-align:center;background-color:#EEE;"> - - - </td>
                                        <td style="text-align:right;background-color:#f5f5f5;"><?php echo number_format($ttotal,2); ?></td>
                                        <td style="text-align:right;background-color:#f5f5f5;"><?php echo number_format($atotal,2); ?></td>
                                    </tr>
                            </tbody>
                        </table>
                        </div>

                        <div class="underTheTable">

                        <?php $bill_total = $atotal - $sale_return_main['DISCOUNT']; ?>

                            <div class="pull-right col-xs-4">
                                <div class="col-xs-6 text-right hide" style="line-height:30px;">Discount</div>
                                <div class="pull-right col-xs-6 hide">
                                    <input type="text" class="form-control text-right" readonly value="<?php echo $sale_return_main['DISCOUNT'] ?>" />
                                </div>
                                <div class="clear" style="height:10px;"></div>
                                <div class="col-xs-6 text-right" style="line-height:30px;">Total Discount</div>
                                <div class="pull-right col-xs-6">
                                    <input type="text" class="form-control text-right" readonly value="<?php echo $sale_return_main['BILL_DISCOUNT'] ?>" />
                                </div>
                                <div class="clear" style="height:10px;"></div>
                                <div class="col-xs-6 text-right" style="line-height:30px;">Total</div>
                                <div class="pull-right col-xs-6">
                                    <input type="text" class="form-control text-right over_total" readonly value="<?php echo number_format($bill_total,2,'.',''); ?>" />
                                </div>
                                <div class="clear" style="height:10px;"></div>
                            </div>
                            <div class="clear"></div>
                        </div><!--underTheTable-->
                        <hr />
                        </div>
                        <div class="clear"></div>
<?php
						}
					}
				}else{
?>
						<div class="block text-center">No Recent Returns!</div>
<?php
				}
				mysql_close($con);
exit();
?>
