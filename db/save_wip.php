<?php
  include('../common/db.connection.php');
  include('../common/classes/accounts.php');
  include('../common/classes/wip.php');
  include('../common/classes/wip_issue_details.php');
  include('../common/classes/wip_produce_details.php');
  include('../common/classes/items.php');
  include('../common/classes/branch_stock.php');
  include('../common/classes/itemCategory.php');
  include('../common/classes/j-voucher.php');
  include('../common/classes/userAccounts.php');

  $objUserAccounts                  = new UserAccounts();
  $objAccounts         		 	        = new ChartOfAccounts();
  $objWorkInProcess 	 		 	        = new WorkInProgress();
  $objWorkInProcessConsumeDetails 	= new WorkInProcessConsumeDetails();
  $objWorkInProcessProduceDetails   = new WorkInProcessProduceDetails();
  $objItems            		 		      = new Items();
  $objItemCategory     		 		      = new itemCategory();
  $objJournalVoucher   		 		      = new JournalVoucher();

  $inv_acc_code = '0101060001';
  $wip_acc_code = '010112001';
  $foh_acc_code = '0401040002';

  $inv_acc_title = $objAccounts->getAccountTitleByCode($inv_acc_code);
  $wip_acc_title = $objAccounts->getAccountTitleByCode($wip_acc_code);
  $foh_acc_title = $objAccounts->getAccountTitleByCode($foh_acc_code);

  $returnData = array('MSG'=>'Error! Nothing Happened. ','ID'=>0);

  $user_details        = $objUserAccounts->getDetails($_SESSION['classuseid']);
  $branch_id 		   = $user_details['BRANCH_ID'];

  function add_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->addStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->addStockValue($item_id,$quantity,$total_cost);
      }
  }
  function remove_branch_stock($item_id,$branch_id,$quantity,$total_cost){
      $objItems       = new Items();
      $objBranchStock = new BranchStock();
      if($branch_id > 0){
          $objBranchStock->removeStock($item_id,$branch_id,$quantity,$total_cost);
      }else{
          $objItems->removeStockValue($item_id,$quantity,$total_cost);
      }
  }
  if(isset($_POST['wip_id'])){
      $wip_id                                = mysql_real_escape_string($_POST['wip_id']);
      $objWorkInProcess->started_on          = date('Y-m-d',strtotime($_POST['started_on_date']));
      $objWorkInProcess->labour_account      = mysql_real_escape_string($_POST['labour_account']);
      $objWorkInProcess->labour         	   = (float)mysql_real_escape_string($_POST['labour']);
      $objWorkInProcess->overhead_account    = mysql_real_escape_string($_POST['overhead_account']);
      $objWorkInProcess->overhead            = (float)mysql_real_escape_string($_POST['overhead']);
      $objWorkInProcess->status   		       = 'P';

      $success              = array();
      $total_issue_cost     = 0;
      $total_produce_cost   = 0;

      $issue_rows   = get_object_vars(json_decode($_POST['issue_rows']));
      $produce_rows = get_object_vars(json_decode($_POST['produce_rows']));

      if(count($issue_rows)==0 || count($produce_rows)==0){
        $returnData = array('MSG'=>'Error Saving Record!','ID'=>0);
        echo json_encode($returnData);
        mysql_close($con);
        exit();
      }

      $deleted_issue_rows   = get_object_vars(json_decode($_POST['deleted_issue_rows']));
      $deleted_produce_rows = get_object_vars(json_decode($_POST['deleted_produce_rows']));

      foreach($deleted_issue_rows as $key => $id){
        $issu_row_del = $objWorkInProcessConsumeDetails->getRecordDetails($id);
        add_branch_stock($issu_row_del['ITEM_ID'],$branch_id,$issu_row_del['QUANTITY'],$issu_row_del['GROSS_COST']);
        $objWorkInProcessConsumeDetails->delete($id);
      }

      foreach($deleted_produce_rows as $key => $id){
        $prod_row_del = $objWorkInProcessProduceDetails->getRecordDetails($id);
        remove_branch_stock($prod_row_del['ITEM_ID'],$branch_id,$prod_row_del['QUANTITY'],$prod_row_del['GROSS_COST']);
        $objWorkInProcessProduceDetails->delete($id);
      }

      if($wip_id == 0){
          $succes_message = 'Record Saved Successfully!';
          $wip_id  = $objWorkInProcess->save();
          if($wip_id==0){
              $succes_message = 'Error! Saving Record!';
          }
      }elseif($wip_id > 0){
          $succes_message = 'Record Updated Successfully!';
          $updated = $objWorkInProcess->update($wip_id);
      }else{
          $returnData = array('MSG'=>'Error Saving Record!','ID'=>0);
          echo json_encode($returnData);
          mysql_close($con);
          exit();
      }

      if($wip_id > 0){
        $objWorkInProcessConsumeDetails->wip_id = $wip_id;
        $objWorkInProcessProduceDetails->wip_id = $wip_id;

        foreach($issue_rows as $key => $issue_row){
          $issue_row->row_id                            = (int)$issue_row->row_id;
          $objWorkInProcessConsumeDetails->item_id      = (int)$issue_row->item_id;
          $objWorkInProcessConsumeDetails->quantity     = (float)$issue_row->quantity;
          $objWorkInProcessConsumeDetails->unit_cost    = (float)$issue_row->unit_cost;
          $objWorkInProcessConsumeDetails->gross_cost   = (float)$issue_row->gross_cost;

          $total_issue_cost                            += $objWorkInProcessConsumeDetails->gross_cost;

          if($issue_row->row_id > 0){
            $record_details = $objWorkInProcessConsumeDetails->getRecordDetails($issue_row->row_id);
            $updated        = $objWorkInProcessConsumeDetails->update($issue_row->row_id);
            if($updated){
              add_branch_stock($record_details['ITEM_ID'],$branch_id,$record_details['QUANTITY'],$record_details['GROSS_COST']);
              remove_branch_stock($issue_row->item_id,$branch_id,$issue_row->quantity,$issue_row->gross_cost);
            }
          }
          if($issue_row->row_id == 0){
            $new_row_id = $objWorkInProcessConsumeDetails->save();

            if($new_row_id){
              remove_branch_stock($issue_row->item_id,$branch_id,$issue_row->quantity,$issue_row->gross_cost);
            }
          }
        }

        foreach($produce_rows as $key => $produce_row){
          $produce_row->row_id                          = (int)$produce_row->row_id;
          $objWorkInProcessProduceDetails->item_id      = $produce_row->item_id;
          $objWorkInProcessProduceDetails->quantity     = $produce_row->quantity;
          $objWorkInProcessProduceDetails->unit_cost    = $produce_row->unit_cost;
          $objWorkInProcessProduceDetails->gross_cost   = $produce_row->gross_cost;

          $total_produce_cost += $objWorkInProcessProduceDetails->gross_cost;

          if($produce_row->row_id > 0){
            $record_details = $objWorkInProcessProduceDetails->getRecordDetails($produce_row->row_id);
            $updated        = $objWorkInProcessProduceDetails->update($produce_row->row_id);
            if($updated){
              remove_branch_stock($record_details['ITEM_ID'],$branch_id,$record_details['QUANTITY'],$record_details['GROSS_COST']);
              add_branch_stock($produce_row->item_id,$branch_id,$produce_row->quantity,$produce_row->gross_cost);
            }
          }
          if($produce_row->row_id == 0){
            $new_row_id = $objWorkInProcessProduceDetails->save($produce_row->row_id);
            if($new_row_id){
              add_branch_stock($produce_row->item_id,$branch_id,$produce_row->quantity,$produce_row->gross_cost);
            }
          }
        }
      }

      if($wip_id > 0){
          $voucher_id                     = $objWorkInProcess->get_voucher_id($wip_id);
          $objJournalVoucher->jVoucherNum = $objJournalVoucher->genJvNumber();
          $objJournalVoucher->voucherDate = $objWorkInProcess->started_on;
          $objJournalVoucher->voucherType = 'PV';

          if($voucher_id > 0){
              $objJournalVoucher->reverseVoucherDetails($voucher_id);
              $objJournalVoucher->updateVoucherDate($voucher_id);
          }else{
              $voucher_id = $objJournalVoucher->saveVoucher();
          }
          $objJournalVoucher->voucherId = $voucher_id;
          if($objJournalVoucher->voucherId){

              $objJournalVoucher->accountCode     = $wip_acc_code;
              $objJournalVoucher->accountTitle    = $wip_acc_title;
              $objJournalVoucher->narration       = "Costing for job # ".$wip_id;
              $objJournalVoucher->transactionType = 'Dr';
              $objJournalVoucher->amount          = $total_issue_cost + $objWorkInProcess->labour + $objWorkInProcess->overhead;
              if($objJournalVoucher->amount > 0){
                  $objJournalVoucher->saveVoucherDetail();
              }
              $objJournalVoucher->accountCode     = $inv_acc_code;
              $objJournalVoucher->accountTitle    = $inv_acc_title;
              $objJournalVoucher->narration       = "Inventory cost charged to wip job # ".$wip_id;
              $objJournalVoucher->transactionType = 'Cr';
              $objJournalVoucher->amount          = $total_issue_cost;
              if($objJournalVoucher->amount > 0){
                  $objJournalVoucher->saveVoucherDetail();
              }
              if($objWorkInProcess->labour_account != ''){
                  $objJournalVoucher->accountCode     = $objWorkInProcess->labour_account;
                  $objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
                  $objJournalVoucher->narration       = "External Expenses charged to wip job # ".$wip_id;
                  $objJournalVoucher->transactionType = 'Cr';
                  $objJournalVoucher->amount          = $objWorkInProcess->labour;

                  $objJournalVoucher->saveVoucherDetail();
              }
              if($objWorkInProcess->overhead_account != ''){
                  $objJournalVoucher->accountCode     = $objWorkInProcess->overhead_account;
                  $objJournalVoucher->accountTitle    = $objAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
                  $objJournalVoucher->narration       = "Internal Expenses charged to wip job # ".$wip_id;
                  $objJournalVoucher->transactionType = 'Cr';
                  $objJournalVoucher->amount          = $objWorkInProcess->overhead;

                  $objJournalVoucher->saveVoucherDetail();
              }

              $objJournalVoucher->accountCode     = $inv_acc_code;
              $objJournalVoucher->accountTitle    = $inv_acc_title;
              $objJournalVoucher->narration       = "Value added to Inventory against wip job # ".$wip_id;
              $objJournalVoucher->transactionType = 'Dr';
              $objJournalVoucher->amount          = $total_issue_cost + $objWorkInProcess->labour + $objWorkInProcess->overhead;
              if($objJournalVoucher->amount > 0){
                  $objJournalVoucher->saveVoucherDetail();
              }

              $objJournalVoucher->accountCode     = $wip_acc_code;
              $objJournalVoucher->accountTitle    = $wip_acc_title;
              $objJournalVoucher->narration       = "Value added to Inventory against wip job # ".$wip_id;
              $objJournalVoucher->transactionType = 'Cr';
              //$objJournalVoucher->amount          = $total_issue_cost;
              if($objJournalVoucher->amount > 0){
                  $objJournalVoucher->saveVoucherDetail();
              }

              $objWorkInProcess->set_voucher_id($wip_id,$objJournalVoucher->voucherId);
          }
          $returnData = array('MSG'=>$succes_message,'ID'=>$wip_id);
          echo json_encode($returnData);
          mysql_close($con);
          exit();
      }
      mysql_close($con);
      exit();
  }
?>
