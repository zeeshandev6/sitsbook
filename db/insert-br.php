<?php
	include('../common/db.connection.php');
	include( '../common/classes/j-voucher.php' );
	include( '../common/classes/accounts.php' );
	include( '../common/classes/userAccounts.php' );

	$objVoucher 	= new JournalVoucher();
	$objAccounts 	= new ChartOfAccounts();
	$objUsers			= new UserAccounts();

	$returnData 	= array('OK'=>'N','MSG'=>'Error! cannot post Bank Receipt Voucher.','ID'=>'0');
	$user_details = $objUsers->getDetails($user_id);
	if(isset($_POST['newJv'])){
		$success = array();

		$voucher_id					= mysql_real_escape_string($_POST['jv_id']);

		$objVoucher->voucherDate 		= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->type_num				= $objVoucher->genTypeNumber('BR');
		$objVoucher->voucherType    = 'BR';
		$objVoucher->jVoucherNum 		= $objVoucher->genJvNumber();
		$objVoucher->reference 			= $_POST['jvRef'];
		$objVoucher->reference_date = date('Y-m-d',strtotime($_POST['refDate']));
		$objVoucher->status					= 'F';

		$bankAccCode 					= mysql_real_escape_string($_POST['bank_acc_code']);
		$bankAccTitle 				= $objAccounts->getAccountTitleByCode($bankAccCode);

		$transactions_array   = get_object_vars(json_decode($_POST['transactions']));

		if(!$objVoucher->checkVoucherByType($objVoucher->type_num,$objVoucher->voucherType) && $voucher_id == 0){
			$returnData = array('OK'=>'N','MSG'=>'Cash Receipt Number Already Exists!','ID'=>'0');
			echo json_encode($returnData);
			exit();
		}
		if($voucher_id > 0){
			$objVoucher->reverseVoucherDetails($voucher_id);
			$objVoucher->updateVoucher($voucher_id);
			$objVoucher->updateVoucherDate($voucher_id);
			$objVoucher->voucherId = $voucher_id;
		}else{
			$objVoucher->voucherId = $objVoucher->saveVoucher();
		}
		$bank_amount = 0;
		if($objVoucher->voucherId){

			$re_nar = '';

			foreach($transactions_array as $key => $transaction){
				$bank_amount += trim($transaction->credit);

				if($key > 0){
					$re_nar .= ', ';
				}
				$re_nar .= trim($transaction->accTitle);
			}

			$objVoucher->accountCode 	   = $bankAccCode;
			$objVoucher->accountTitle	   = mysql_real_escape_string($bankAccTitle);
			$objVoucher->narration 	 	   = "Receipt From ".$re_nar;
			$objVoucher->amount 	 	 		 = $bank_amount;
			$objVoucher->transactionType = 'Dr';
			$success[] 					 				 = $objVoucher->saveVoucherDetail();

			foreach($transactions_array as $key => $transaction){
				$objVoucher->accountCode 	 		= trim($transaction->accCode);
				$objVoucher->accountTitle	 		= mysql_real_escape_string($transaction->accTitle);
				$objVoucher->narration 	 	 		= mysql_real_escape_string(trim($transaction->narration));
				$objVoucher->transactionType 	= 'Cr';
				$objVoucher->amount 					= trim($transaction->credit);
				$bank_amount += $objVoucher->amount;
				$success[] = $objVoucher->saveVoucherDetail();
			}
			$voucher_id = $objVoucher->voucherId;
		}else{
			$success[] = 0;
		}
		if(!in_array('0',$success)){
			$returnData = array('OK'=>'Y','MSG'=>'Bank Receipt Saved Successfully!','ID'=>$voucher_id);
			echo json_encode($returnData);
		}else{
			$returnData = array('OK'=>'N','MSG'=>'Error! cannot post Bank Receipt Voucher.','ID'=>'0');
			echo json_encode($returnData);
		}
	}
	mysql_close($con);
	exit();
?>
