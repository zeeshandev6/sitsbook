<?php
	include('../common/db.connection.php');
	include('../common/classes/settings.php');
	include('../common/classes/j-voucher.php');
	include('../common/classes/accounts.php');
	include('../common/classes/userAccounts.php');

	$objVoucher 				= new JournalVoucher();
	$objAccounts				= new ChartOfAccounts();
	$objUsers						= new UserAccounts();

	$user_details = $objUsers->getDetails($user_id);

	if($user_id > 1 && $user_details['CASH_IN_HAND'] == 'Y'){
		$cash_in_hand_acc_code = $user_details['CASH_ACC_CODE'];
	}else{
		$cash_in_hand_acc_code = '0101010001';
	}

	$returnData = array('OK'=>'N','MSG'=>'Error! cannot post Cash Receipt Voucher','ID'=>'0');
	if(isset($_POST['newJv'])){
		$success = array();
		$voucher_id									= mysql_real_escape_string($_POST['jv_id']);

		$cash_in_hand_acc_code 			= mysql_real_escape_string($_POST['cash_in_hand']);
		$objVoucher->voucherDate 		= date('Y-m-d',strtotime($_POST['jvDate']));
		$objVoucher->type_num				= $objVoucher->genTypeNumber('CR');
		$objVoucher->voucherType    = 'CR';
		$objVoucher->jVoucherNum 		= $objVoucher->genJvNumber();

		$objVoucher->order_taker_id = (int)mysql_real_escape_string($_POST['order_taker_id']);
		$objVoucher->salesman_id  	= (int)mysql_real_escape_string($_POST['salesman_id']);
		$objVoucher->reference 			= mysql_real_escape_string($_POST['jvRef']);
		$objVoucher->reference_date = date('Y-m-d',strtotime($_POST['refDate']));

		$transactions_array = get_object_vars(json_decode($_POST['transactions']));
		if(!$objVoucher->checkVoucherByType($objVoucher->type_num,$objVoucher->voucherType) && $voucher_id == 0){
			$returnData = array('OK'=>'N','MSG'=>'Cash Receipt Number Already Exists!','ID'=>'0');
			echo json_encode($returnData);
			exit();
		}
		if($voucher_id > 0){
			$objVoucher->reverseVoucherDetails($voucher_id);
			$objVoucher->updateVoucher($voucher_id);
			$objVoucher->updateVoucherDate($voucher_id);
			$objVoucher->voucherId = $voucher_id;
		}else{
			$objVoucher->voucherId = $objVoucher->saveVoucher();
		}
		$cash_amount = 0;
		if($objVoucher->voucherId){
			$re_nar = '';
			foreach($transactions_array as $key => $transaction){
				$cash_amount += trim($transaction->credit);
				if($key > 0){
					$re_nar .= ', ';
				}
				$re_nar .= $transaction->accTitle;
			}
			$objVoucher->accountCode 		 = $cash_in_hand_acc_code;
			$objVoucher->accountTitle		 = mysql_real_escape_string($objAccounts->getAccountTitleByCode($objVoucher->accountCode));
			$objVoucher->narration 	 		 = " Cash Received From ".mysql_real_escape_string($re_nar);
			$objVoucher->amount 				 = $cash_amount;
			$objVoucher->transactionType = 'Dr';
			$success[] 									 = $objVoucher->saveVoucherDetail();
			foreach($transactions_array as $key => $transaction){
				$objVoucher->accountCode 	 		= trim($transaction->accCode);
				$objVoucher->accountTitle	 		= mysql_real_escape_string($transaction->accTitle);
				$objVoucher->narration 	 	 		= mysql_real_escape_string(trim($transaction->narration));
				$objVoucher->transactionType 	= 'Cr';
				$objVoucher->amount 					= (float)(trim($transaction->credit));
				$success[] 										= $objVoucher->saveVoucherDetail();
			}
			$voucher_id = $objVoucher->voucherId;
		}else{
			$success[] = 0;
		}
		if(!in_array('0',$success)){
			$returnData = array('OK'=>'Y','MSG'=>'Cash Receipt Saved Successfully!','ID'=>$voucher_id);
			echo json_encode($returnData);
		}else{
			echo json_encode($returnData);
		}
	}
	mysql_close($con);
	exit();
?>
