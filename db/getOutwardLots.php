<?php
	include('../common/db.connection.php');
	include('../common/classes/emb_outward.php');
	include('../common/classes/payrolls.php');
	include('../common/classes/measure.php');
	include('../common/classes/machines.php');
	include('../common/classes/emb_products.php');
	include('../common/classes/emb_lot_register.php');
	require('../common/classes/userAccounts.php');

	$objOutward 			= new outward();
	$objMeasures 			= new Measures();
	$objMachines 			= new Machines();
	$objEmbProducts 	= new EmbProducts();
	$objPayrolls 		 	= new Payrolls();
	$objLotRegister 	= new EmbLotRegister();
	$objAccounts    	= new UserAccounts();

	$permissionz = $objAccounts->getPermissions($user_id);
	$permissionz = explode('|',$permissionz);

	$billing_types 			= array();
	$billing_types['S'] = 'Stitches';
	$billing_types['Y'] = 'Yards';
	$billing_types['U'] = 'Suites';
	$billing_types['L'] = 'Laces';

	$outward_row = NULL;
	if(isset($_POST['gp_no'])){
		$bill_no 		 = mysql_real_escape_string($_POST['gp_no']);
		$outward_row = $objOutward->getRecordByBillNumber($bill_no);
	}
	if(isset($_POST['customer_code'])){
?>
<tbody>
<?php
		if($_POST['customer_code'] == ''){
			$customer_code  = $outward_row['CUST_ACC_CODE'];
			$completedLots = $objOutward->getOutwardLotsForBilling($customer_code,$bill_no);
			?>
			<input type="hidden" name="hidden_cust_code" value="<?php echo $outward_row['CUST_ACC_CODE']; ?>" />
			<input type="hidden" name="hidden_cloth" 		 value="<?php echo $outward_row['CLOTH']; ?>" />
			<input type="hidden" name="hidden_yarn"	 		 value="<?php echo $outward_row['YARN']; ?>" />
			<input type="hidden" name="hidden_transport" value="<?php echo $outward_row['TRANSPORT']; ?>" />
			<?php
		}else{
			$customer_code  = mysql_real_escape_string($_POST['customer_code']);
			$completedLots = $objOutward->getOutwardLotsForBilling($customer_code,'');
		}
		$lengthTotal   = 0;
		$embRateTotal  = 0;
		$stitchCount   = 0;
		$stitchTotal   = 0;
		$lacesTotal    = 0;
?>
<?php
		if(mysql_num_rows($completedLots)){
			while($lotRow = mysql_fetch_array($completedLots)){
	      $measurement 		= $objMeasures->getName($lotRow['MEASURE_ID']);
	      $stitchMan 			= $objPayrolls->getTitle($lotRow['STITCH_ACC']);
				$product_name 	= $objEmbProducts->getTitle($lotRow['PRODUCT_ID']);

				$partial_row = $objOutward->getLengthIfPartial($lotRow['LOT_REGISTER_ID']);
				if($partial_row['PARTIAL_LENGTH'] != 0){
					if($partial_row['PARTIAL_LENGTH'] == $lotRow['MEASURE_LENGTH']){
						continue;
					}
					$lotRow['MEASURE_LENGTH'] -= $partial_row['PARTIAL_LENGTH'];
					$lotRow['TOTAL_LACES']    -= $partial_row['TOTAL_PRODUCTION'];
				}
?>
        <tr class="alt-row calculations lot-register-rows" data-lot-id="<?php echo $lotRow['OUTWD_DETL_ID']; ?>">
            <td class="text-center product_id" data-product-id="<?php echo $lotRow['PRODUCT_ID']; ?>"><?php echo $product_name; ?></td>
            <td class="text-center lot_no"><?php echo $lotRow['LOT_NO']; ?></td>
            <td class="text-center quality"><?php echo $lotRow['QUALITY']; ?></td>
            <td class="text-center measure_id" data-measure-id="<?php echo $lotRow['MEASURE_ID']; ?>"><?php echo $measurement; ?></td>
						<td class="text-center billing_type" data-billing-type="<?php echo $lotRow['BILLING_TYPE']; ?>"><?php echo $billing_types[$lotRow['BILLING_TYPE']]; ?></td>
						<td class="text-center stitches"><?php echo $lotRow['STITCHES']; ?></td>
						<td class="text-center total_laces"><?php echo $lotRow['TOTAL_LACES']; ?></td>
            <td class="text-center measure_length lengthColumn"><?php echo $lotRow['MEASURE_LENGTH']; ?></td>
            <td class="text-center emb_rate"><?php echo $lotRow['EMB_RATE']; ?></td>
            <td class="text-center emb_amount embAmountColumn"><?php echo $lotRow['EMB_AMOUNT']; ?></td>
            <td class="text-center stitch_rate"><?php echo $lotRow['STITCH_RATE']; ?></td>
            <td class="text-center stitch_amount stichAmountColumn"><?php echo $lotRow['STITCH_AMOUNT']; ?></td>
            <td class="text-center stitch_acc" data-stitch-id="<?php echo $lotRow['STITCH_ACC']; ?>"><?php echo $stitchMan; ?></td>
            <td class="text-center design_code"><?php echo $lotRow['DESIGN_CODE']; ?></td>
            <td class="text-center machine_id"><?php echo $lotRow['MACHINE_ID']; ?></td>
						<td>
							<a class="quick_edit" id='view_button' do="<?php echo $lotRow['OUTWD_DETL_ID']; ?>" title="Modify"><i class="fa fa-pencil"></i></a>
						</td>
            <td style="text-align:center">
								<input type="checkbox" class="lotCheckBox css-checkbox" id="lot-checkv-<?php echo $lotRow['OUTWD_DETL_ID']; ?>" value="<?php echo $lotRow['OUTWD_DETL_ID']; ?>" />
								<label for="lot-checkv-<?php echo $lotRow['OUTWD_DETL_ID']; ?>" class="css-label"></label>
            </td>
        </tr>
<?php
				$lengthTotal 	+= $lotRow['MEASURE_LENGTH'];
				$embRateTotal += $lotRow['EMB_AMOUNT'];
				$stitchTotal 	+= $lotRow['STITCH_AMOUNT'];
				$stitchCount  += $lotRow['STITCHES'];
				$lacesTotal   += $lotRow['TOTAL_LACES'];
			}
		}
?>
									<tr class="totals">
                    <td class="text-center" colspan="5" >Total</td>
										<td class="text-center sumStitchTotal"><?php echo $stitchCount; ?></td>
										<td class="text-center sumLacesTotal"><?php echo $lacesTotal; ?></td>
                    <td class="text-center sumLenTotal"><?php echo $lengthTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center embAmountTotal"><?php echo $embRateTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center stichAmountTotal"><?php echo $stitchTotal; ?></td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
                    <td class="text-center">- - -</td>
										<td class="text-center">- - -</td>
									</tr>
	</tbody>
<?php
	}
?>
