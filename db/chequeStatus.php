<?php
	include('../common/db.connection.php');
	include '../common/classes/cash_management.php';

	$objCashManagement = new CashManagement;

	if(isset($_POST['id'])){
		$cheque_id = $_POST['id'];
		$status    = $_POST['status'];
		$today     = date('Y-m-d');

		if($objCashManagement->changeChequeStatus($cheque_id,$status,$today)){
			echo date('d-m-Y');
		}
	}

	if(isset($_POST['post_id'])){
		$post_id = $_POST['post_id'];
		$details = $objCashManagement->getDetails($post_id);
		echo $details['POST'];
	}
	mysql_close($con);
?>
