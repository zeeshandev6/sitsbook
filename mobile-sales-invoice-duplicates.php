<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/customers.php');
	include('common/classes/items.php');
	include('common/classes/j-voucher.php');
	include('common/classes/company_details.php');

	$objCompanyDetails  		= new CompanyDetails();
	$objScanSale   					= new ScanSale();
	$objScanSaleDetails 		= new ScanSaleDetails();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objCustomers   		  	= new Customers();
	$objItems  	    		  	= new Items();
	$objJournalVoucher   	 	= new JournalVoucher();
	$objConfigs   					= new Configs();

	$print_meth     	 = $objConfigs->get_config('DIRECT_PRINT');

	$individual_tax 	= $objConfigs->get_config('SHOW_TAX');
  $individual_tax 	= ($individual_tax=='Y')?true:false;

	$individual_discount 	= $objConfigs->get_config('SHOW_DISCOUNT');
  $individual_discount  = ($individual_discount=='Y')?true:false;
  $currency_type 		  	= $objConfigs->get_config('CURRENCY_TYPE');
  $use_taxes            = $objConfigs->get_config('SHOW_TAX');
  $use_taxes            = ($use_taxes=='Y')?true:false;
	$invoice_format 	 		= $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_format 	 		= explode('_', $invoice_format);

	if(isset($_GET['id'])){
		$sale_id = mysql_real_escape_string($_GET['id']);
		$scan_bill = $objScanSale->getDetail($sale_id);
		$scan_bill_details = $objScanSaleDetails->getList($sale_id);
		$supplier_balance = $objJournalVoucher->getInvoiceBalance($scan_bill['CUST_ACC_CODE'],$scan_bill['VOUCHER_ID']);
		$supplier_balance_array = $objJournalVoucher->getBalanceType($scan_bill['CUST_ACC_CODE'], $supplier_balance);
	}
	///   INVOICE_FORMAT Config Size - Header - Duplicate
	$invoice_num 	 = $invoice_format[1];
	$invoiceStyleCss = 'resource/css/invoiceStyleLanscape.css';
	$duplicates 	 = 'Y';
	$max_counter 	 = 29;
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>

    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $invoiceStyleCss; ?>" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			<?php
				if($duplicates=='Y'){
				?>
				var copy = $(".invoiceLeftPrint").html();
				$("<div class='invoiceLeftPrint last'>"+copy+"</div>").insertAfter($(".invoiceLeftPrint"));

			<?php
				}
				?>
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "<?php echo $invoiceStyleCss; ?>",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			$(".printThis").click();
			$(window).keyup(function(e){
				if(e.ctrlKey && e.keyCode == 17){
					$(".printThis").click();
					e.preventDefault();
					return false;
				}
			});
			$(".ledger_view").click(function(){
				var gl_date = $(this).attr('data-date');
				var gl_code = $(this).attr('data-code');
				if(gl_code != ''){
					$.get('db/ledger-vars.php',{gl_date:gl_date,gl_code:gl_code},function(data){
						if(data == ''){
							var win = window.open('invoice-ledger-print.php','_blank');
							if(win){
							    win.focus();
							}else{
							    displayMessage('Please allow popups for This Site');
							}
						}
					});
				}
			});
			$(window).keyup(function(e){
				if(e.keyCode == 27){
					$(".cancel-button").click();
				}
			});
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($sale_id)){
		if(substr($scan_bill['CUST_ACC_CODE'], 0,6) == '010101'){
			$customer = array();
			$customer['ADDRESS'] = '';
			$customer['CITY'] = '';
			$customer['CUST_ACC_TITLE'] = $scan_bill['CUSTOMER_NAME'];
		}else{
			$customer  = $objCustomers->getCustomer($scan_bill['CUST_ACC_CODE']);
		}
		$sale_date     = date('d-M-Y',strtotime($scan_bill['SALE_DATE']));
		$ledgerDate    = date('Y-m-d',strtotime($sale_date));
		$company  		 = $objCompanyDetails->getActiveProfile();
?>

<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="button printThis pull-left" title="Print"> <i class="fa fa-print"></i> Print </button>
            <button class="btn btn-default pull-right cancel-button" onclick="window.close();" title="Close Window"> <span class="glyphicon glyphicon-remove"></span></button>
            <div style="height:20px;clear:both;"></div>
        </div><!--headerWrapper-->
    </div><!--header-->
    <div class="clear"></div>
    <div class="printThisDiv" style="position: relative;">
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
            <div class="invoiceHead" style="width: 100%;margin: 0px auto;">
            	<div class="partyTitle pull-left" style="text-align:<?php echo $company['LOGO'] == ''?'center':'left'; ?>;padding-left:10px;font-size: 10px;width: 95%;line-height:15px;">
            		<div class="company-title"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']."<br>"; ?>
										<?php echo ($company['CONTACT']=='')?"":"Contact:".$company['CONTACT']; ?>
                    <?php echo ($company['EMAIL']=='')?"":"Email:".$company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="clear" style="height: 5px;"></div>
<?php
				if($invoice_num == '1'){
?>
                <div class="infoPanel pull-left">
                	<div class="infoHead">Customer Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:12px;"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right">
                	<div class="infoHead">Invoice Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:12px;">Inv # <?php echo $scan_bill['BILL_NO']; ?> </span>
                	<span class="pull-right" style="padding:0px 2px;font-size:12px;"><?php echo $sale_date; ?> <?php echo date('h:i A',strtotime($scan_bill['RECORD_TIME'])); ?> </span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
<?php
				}

				if($invoice_num == '2'){
?>
				<div class="invoPanel">
					<div class="invoPanel">
						<div class="clear"></div>
	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Name: </span>
	                		<span class="text_line"><?php echo $customer['CUST_ACC_TITLE']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">Address: </span>
	                		<span class="text_line"><?php echo $customer['ADDRESS']; ?></span>
	                	</span>
	                	<div class="clear"></div>

	                	<span class="pull-left" style="font-size:14px;">
	                		<span class="caption">City: </span>
	                		<span class="text_line"><?php echo $customer['CITY']; ?></span>
	                	</span>
	                	<div class="clear"></div>
	                </div>
				</div>
				<div class="clear" style="height:10px;"></div>
<?php
				}
?>
				<div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 100%;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="5%">Sr</th>
                            <th width="45%">Description</th>
                            <th width="10%">Rate</th>
                            <?php if($individual_discount){ ?>
                            <th width="10%">Disc</th>
							<?php } ?>
							<?php if($individual_tax){ ?>
							<th width="10%">Tax</th>
							<?php } ?>

                            <th width="15%">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border-bottom:1px solid #222 !important;">
                    	<tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <?php if($individual_discount){ ?>
                            <td>&nbsp;</td>
                            <?php } ?>
                            <?php if($individual_tax){ ?>
                            <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
<?php
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($scan_bill_details)){
							while($row = mysql_fetch_array($scan_bill_details)){
								$pd_row   = $objScanPurchaseDetails->getDetails($row['SP_DETAIL_ID']);
								$itemName = $objItems->getItemTitle($pd_row['ITEM_ID']);
								$color    = '';
								$color    = ($pd_row['COLOUR'] == '')?"":" - ".$pd_row['COLOUR'];
?>
								<tr>
                                	<td><?php echo $counter; ?></td>
									<td><span style="float:left;margin-left:5px;"><?php echo $itemName; ?><?php echo $color; ?></span></td>
									<td><?php echo $row['PRICE']; ?></td>
									<?php if($individual_discount){ ?>
									<td><?php echo $row['DISCOUNT']; ?></td>
									<?php } ?>
									<?php if($individual_tax){ ?>
                                    <td><?php echo $row['TAX']; ?></td>
                                    <?php } ?>
                                    <td><span style="float:right;margin-right:5px;"> <?php echo number_format($row['SALE_PRICE'],2); ?></span></td>
								</tr>
<?php
									$sub_total    = $row['PRICE'];
									$taxAmount   += $row['TAX'];
									$totalAmount += $row['SALE_PRICE'];
									$counter++;
							}
						}
?>
<?php
						$class = '';
						$other_counter = $max_counter - $counter;
						for ($i=0; $i <= $other_counter; $i++) {
							if($other_counter == $i){
								$class = '';
							}
?>
							<tr>
	                            <td class="<?php echo $class; ?>" ><?php //echo $i+$counter; ?></td>
	                            <td>&nbsp;</td>
	                            <?php if($individual_discount){ ?>
	                            <td>&nbsp;</td>
	                            <?php } ?>
	                            <?php if($individual_tax){ ?>
	                            <td>&nbsp;</td>
	                            <?php } ?>
	                            <td>&nbsp;</td>
	                            <td>&nbsp;</td>
	                        </tr>
<?php
						}
?>
							<tr>
	                            <td class="bottom-border">&nbsp;</td>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php if($individual_discount){ ?>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php } ?>
	                            <?php if($individual_tax){ ?>
	                            <td class="bottom-border">&nbsp;</td>
	                            <?php } ?>
	                            <td class="bottom-border">&nbsp;</td>
	                            <td class="bottom-border">&nbsp;</td>
	                        </tr>
                    </tbody>
                    <?php
                    ?>
                    <?php
                    	$finalAmount = $totalAmount;

                    	$colz = 2;
                    	$colz -= ($individual_discount)?0:1;
                    	$colz -= ($individual_tax)?0:1;
                    ?>
                    <tfoot>

                    	<?php if($individual_discount){ ?>
                    	<tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Total Discount</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"><?php echo number_format($scan_bill['SALE_DISCOUNT'],2); ?></span></td>
                        </tr>
                        <?php } ?>

                        <?php if($individual_tax){ ?>
                        <tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Tax Amount</span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"><?php echo number_format($scan_bill['SALE_TAX'],2); ?></span></td>
                        </tr>
                        <?php } ?>
                        <tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Bill Total <?php echo $currency_type; ?> </span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"> <?php echo number_format($finalAmount,2); ?>  </span></td>
                        </tr>

                        <tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Cash Payment </span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"> <?php echo number_format($scan_bill['RECEIVED_CASH'],2); ?>  </span></td>
                        </tr>

                        <?php
                        	if($scan_bill['CHANGE_RETURNED'] > 0){
                        ?>
                        <tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Change Return </span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"><?php echo number_format($scan_bill['CHANGE_RETURNED'],2); ?></span></td>
                        </tr>
                        <?php
                        	}elseif(substr($scan_bill['CUST_ACC_CODE'], 0,6) != '010101'){
                        ?>
                        <tr>
                        	<?php if($colz > 0){ ?>
                        	<td style="text-align:right;padding:5px !important;" colspan="<?php echo $colz; ?>"></td>
                        	<?php } ?>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;" colspan="3"><span style="float:right;margin-right:15px;">Balance <?php echo $supplier_balance_array['TYPE']=='DR'?"O/s":"Payable"; ?> </span></td>
                            <td class="bottom-border" style="text-align:right;padding:5px !important;"><span style="float:right;margin-right:5px;"><?php echo number_format($supplier_balance_array['BALANCE'],2); ?></span></td>
                        </tr>
                        <?php
                        	}
                        ?>

                    </tfoot>
                </table>
                <div class="clear"></div>
                <div class="invoice_footer">
	                <div class="auths pull-left">
	                	<?php echo $usersFullName; ?>
	                </div><!--partyTitle-->
	                <div class="auths pull-left">
	                	Checked By
	                </div><!--partyTitle-->
	                <div class="clear" style="height: 10px;"></div>
	                <div class="invoice-developer-info"> Designed &amp; Developed By <b>SIT SOLUTIONS</b> , 041-8722200, www.sitsol.net </div>
	        	</div>
            </div><!--invoiceBody-->
        </div><!--invoiceLeftPrint-->
        <div class="clear"></div>
    </div><!--invoiceContainer-->
    </div><!--printThisDiv-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
