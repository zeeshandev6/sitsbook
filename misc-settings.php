<?php
	$out_buffer = ob_start();
	include ('common/connection.php');
	include ('common/config.php');
	include ('common/classes/currency_types.php');
	include ('common/classes/j-voucher.php');

	//Permission
	if(!in_array('misc-setting-managment',$permissionz) && !$admin){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objPagination 		= new Pagination();
	$objTaxConfig  		= new TaxConfig();
	$objJournalVoucher= new JournalVoucher();
	$objCurrecyTypes  = new CurrecyTypes();
	$objConfigs    		= new Configs;


	$notes_dir   = 'uploads/';

	$currency_list = $objCurrecyTypes->getList();
	//ADMIN_MSG
	if(isset($_GET['invoice_type'])){
		$invoice_type = mysql_real_escape_string($_GET['invoice_type']);
		$objConfigs->set_config($invoice_type,'INVOICE_FORMAT');
		$objConfigs->set_config($invoice_type,'SCAN_INVOICE_FORMAT');
		exit();
	}
	if(isset($_POST['invoice_notetxt'])){
		$invoice_notetxt = mysql_real_escape_string($_POST['invoice_notetxt']);
		$objConfigs->set_config($invoice_notetxt,'INVOICE_NOTE_TXT');
		header('location:misc-settings.php');
	    exit();
	}

	if(isset($_POST['clear_blank_vouchers'])){
		$objJournalVoucher->deleteUnRelatedVoucherEntries();
		exit();
	}

	if(isset($_GET['invoice_note_img_delete'])){
		$file_name =  $objConfigs->get_config('INVOICE_NOTE_IMG');
		if(is_file($notes_dir.$file_name)){
            echo unlink($notes_dir.$file_name);
        }
        header('location:misc-settings.php');
	    exit();
	}

	if(isset($_FILES['invoice_note_img'])){
		if($_FILES['invoice_note_img']['error'] == 0){

			$file_name =  $objConfigs->get_config('INVOICE_NOTE_IMG');
			if(is_file($notes_dir.$file_name)){
                unlink($notes_dir.$file_name);
            }

			$temp = explode(".", $_FILES["invoice_note_img"]["name"]);
	        $extension = end($temp);

	        $file_name =  "invoice.".$extension;

	        if(!is_dir($notes_dir)){
	            mkdir($notes_dir, 0777);
	        }

	        move_uploaded_file($_FILES["invoice_note_img"]["tmp_name"],$notes_dir.$file_name);
	        if(is_file($notes_dir.$file_name)){
	        	$objConfigs->set_config($file_name,"INVOICE_NOTE_IMG");
	        }
	    }
	    header('location:misc-settings.php');
	    exit();
	}

	if(isset($_GET['sms_enable'])){
		$sms_val = mysql_real_escape_string($_GET['sms_enable']);
		if($sms_val == 'Y' || $sms_val == 'N'){
			$sms_setting_updated = $objConfigs->set_config($sms_val,'SMS');
			if(!$sms_setting_updated){
				$message = 'Cannot Save SMS Config!';
			}else{
				echo '<script type="text/javascript">window.location.href = "'.$_SERVER['PHP_SELF'].'";</script>';
				exit();
			}
		}else{
			$message = 'Invalid Sms Setting Value';
		}
	}
	if(isset($_GET['scanner_enable'])){
		$sms_val = mysql_real_escape_string($_GET['scanner_enable']);
		if($sms_val == 'Y' || $sms_val == 'N'){
			$sms_setting_updated = $objConfigs->set_config($sms_val,'SALE_SCANNER');
			if(!$sms_setting_updated){
				$message = 'Cannot Save SMS Config!';
			}else{
				echo '<script type="text/javascript">window.location.href = "'.$_SERVER['PHP_SELF'].'";</script>';
				exit();
			}
		}else{
			$message = 'Invalid Sms Setting Value';
		}
	}
	if(isset($_GET['admin_sms'])){
		$sms_val = mysql_real_escape_string($_GET['admin_sms']);
		if($sms_val == 'Y' || $sms_val == 'N'){
			$sms_setting_updated = $objConfigs->set_config($sms_val,'ADMIN_SMS');
			if(!$sms_setting_updated){
				$message = 'Cannot Save SMS Config!';
			}else{
				echo '<script type="text/javascript">window.location.href = "'.$_SERVER['PHP_SELF'].'";</script>';
				exit();
			}
		}else{
			$message = 'Invalid Sms Setting Value';
		}
	}
	if(isset($_GET['tax_enable'])){
		$sms_val = mysql_real_escape_string($_GET['tax_enable']);
		if($sms_val == 'Y' || $sms_val == 'N'){
			$sms_setting_updated = $objTaxConfig->set_tax_conf($sms_val);
			if(!$sms_setting_updated){
				$message = 'Cannot Save SMS Config!';
			}else{
				echo '<script type="text/javascript">window.location.href = "'.$_SERVER['PHP_SELF'].'";</script>';
				exit();
			}
		}else{
			$message = 'Invalid Sms Setting Value';
		}
	}

	if(isset($_POST['update'])){
		if($objPagination->update($_POST['perPage'])){
			$message = "Records Per page : ".$_POST['perPage'];
		}
	}
	if(isset($_POST['currency'])){
		if($objConfigs->set_config($_POST['currency'],'CURRENCY_TYPE')){
			$message = "Default Currency Set Successfully !";
		}
	}
	if(isset($_POST['sms_user'])){
		if($objConfigs->set_config($_POST['smsusername'],'USER_NAME')){
			$message = "USER Name Value Successfully Saved!";
		}
	}
	if(isset($_POST['admin_msg'])){
		if($objConfigs->set_config($_POST['adminmsg'],'ADMIN_MSG')){
			$message = "Admin Message Successfully Saved!";
		}
	}
	if(isset($_POST['invoice_msg'])){
        echo $_POST['invoicemsg'];
        exit();
		if($objConfigs->set_config($_POST['invoicemsg'],'INVOICE_MSG')){
			$message = "Invoice Message Successfully Saved!";
		}
	}
	$perPage = $objPagination->getPerPage();
	$sms_config = $objConfigs->get_config('SMS');
	$tax_config = $objTaxConfig->get_tax_conf();
	$admin_sms_config = $objConfigs->get_config('ADMIN_SMS');
	$scanner_config = $objConfigs->get_config('SALE_SCANNER');
	$admin_message  = $objConfigs->get_config('ADMIN_MSG');
	$invoice_message= $objConfigs->get_config('INVOICE_MSG');
	$currenty       = $objConfigs->get_config('CURRENCY_TYPE');
	$invoice_type   = $objConfigs->get_config('INVOICE_FORMAT');
	$invoice_noteimg= $objConfigs->get_config('INVOICE_NOTE_IMG');
	$invoice_notetxt= $objConfigs->get_config('INVOICE_NOTE_TXT');
	$sms_check_enabled = '';

	if($sms_config == 'Y'){
		$sms_check_enabled = "checked='checked'";
	}

	$tax_check_enabled = '';
	if($tax_config == 'Y'){
		$tax_check_enabled = "checked='checked'";
	}
	$admin_sms_enabled = '';
	if($admin_sms_config == 'Y'){
		$admin_sms_enabled = "checked='checked'";
	}
	$scanner_enabled = '';
	if($scanner_config == 'Y'){
		$scanner_enabled = "checked='checked'";
	}

?>
<!DOCTYPE html 
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
			a.poplight{
				color:#666;
			}
		</style>
      	<script type="text/javascript" src = "resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
      	<script type="text/javascript" src = "resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript">
					$(document).ready(function(){
						$("select").selectpicker();
						$("input[name='perPage']").numericInputOnly();
						$("input[name='sms_enable']").change(function(){
							if($(this).is(":checked")){
								window.location.href = 'misc-settings.php?sms_enable=Y';
							}else{
								window.location.href = 'misc-settings.php?sms_enable=N';
							}
						});
						$("input[name='tax_enable']").change(function(){
							if($(this).is(":checked")){
								window.location.href = 'misc-settings.php?tax_enable=Y';
							}else{
								window.location.href = 'misc-settings.php?tax_enable=N';
							}
						});
						$("input[name='admin_sms']").change(function(){
							if($(this).is(":checked")){
								window.location.href = 'misc-settings.php?admin_sms=Y';
							}else{
								window.location.href = 'misc-settings.php?admin_sms=N';
							}
						});

						$("input[name='scanner_enable']").change(function(){
							if($(this).is(":checked")){
								window.location.href = 'misc-settings.php?scanner_enable=Y';
							}else{
								window.location.href = 'misc-settings.php?scanner_enable=N';
							}
						});
						$("select[name=invoice_type]").change(function(){
							var invoice_val = $("select[name=invoice_type] option:selected").val();
							$.get('misc-settings.php',{invoice_type:invoice_val},function(){
								displayMessage("Invoice Size Configured Successfully!");
							});
						});
						$("input[name=invoice_note_img]").change(function(){
							$("form[name=invoice_note]").submit();
						});
						$(".clear_blank_vouchers").click(function(){
							$(".clear_blank_vouchers").prop("disabled",true);
							$(".clear_blank_vouchers").html('<i class="fa fa-recycle"></i>');
							$(".clear_blank_vouchers i").addClass('fa-spin');
							$.post("?",{clear_blank_vouchers:true},function(data){
								setTimeout(function(){
									$(".clear_blank_vouchers").prop("disabled",false);
									$(".clear_blank_vouchers").html('<i class="fa fa-recycle"></i> Delete Blank Vouchers');
									$(".clear_blank_vouchers i").removeClass('fa-spin');
								},3000);
							});
						});
          });
				</script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php"); ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class = "content-box-top" style="padding-bottom: 30px;">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p>Pagination Settings</p>
                  		</div><!-- End .content-box-header -->
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">Rows Per Page</div>
    	                        <div class="field">
                            		<input type="text" class="form-control pull-left text-center" value="<?php echo $perPage; ?>" style="width:150px;" name="perPage" />
																<input type="submit" name="update" value="Update" class="btn  btn-primary" style="margin-left:20px;" />
        	                    </div>
                              <div class="clear"></div>
                            </form>
                        </div><!--form-->
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">Currency Type : </div>
    	                        <div class="field">
                                    <select name="currency" class="form-control" data-width="150">
                                    	<option value=""></option>
<?php
								if(mysql_num_rows($currency_list)){
									while($current = mysql_fetch_assoc($currency_list)){
										$current_selected = ($currenty == $current['SYMBOL'])?"selected":"";
?>
										<option value="<?php echo $current['SYMBOL'] ?>" <?php echo $current_selected; ?> ><?php echo $current['TITLE'] ?></option>
<?php
									}
								}
?>
                                    </select>
                                    <input type="submit" name="currenty" value="Update" class="btn btn-primary" style="margin-left:20px;" />
        	                    </div>
                                <div class="clear"></div>
                            </form>
                        </div><!--form-->
                        <?php /*
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">User Name</div>
    	                        <div class="field">
                            		<input type="text" class="input_size" value="" style="width:150px;" name="smsusername" />

                                    <input type="submit" name="sms_user" value="Update" class="button-green" style="height:30px;margin-left:20px;" />
        	                    </div>
                                <div class="clear"></div>
                            </form>
                        </div><!--form-->
                        */ ?>
                        <div id="form" style="">
                        	<div class="caption">Sales Scanner</div>
	                        <div class="field">
                        		<input name="scanner_enable" <?php echo $scanner_enabled; ?> type="checkbox" id="scanner_enable" class="css-checkbox" value='1' />
                        		<label class="css-label" for="scanner_enable"></label>
    	                    </div>
                            <div class="clear"></div>
                        </div><!--form-->
                        <div id="form" style="">
                        	<div class="caption">Invoice Size :</div>
	                        <div class="field">
                        		<select name="invoice_type" class="form-control">
                        			<option value="S_1" <?php echo ($invoice_type == 'S_1')?'selected':''; ?> >Small </option>
                        			<option value="M_1" <?php echo ($invoice_type == 'M_1')?'selected':''; ?> >Medium - Duplicates</option>
                        			<option value="L_1" <?php echo ($invoice_type == 'L_1')?'selected':''; ?>>Full  </option>
                        		</select>
    	                    </div>
                            <div class="clear"></div>
                        </div><!--form-->

                        <div id="form" style="">
                        	<form method="post" action="" enctype="multipart/form-data" name="invoice_note">
	                        	<div class="caption">Invoice Note :</div>
		                        <div class="field btn btn-primary btn-file">
	                        		<input type="file" name="invoice_note_img" accept="image/*"  />
	                        		Upload Image
	    	                    </div>
	                            <div class="clear"></div>
	                            <?php
	                            	if(is_file($notes_dir.$invoice_noteimg)){
	                            ?>
	                            	<div class="caption"></div>
	                            	<img src="<?php echo $notes_dir.$invoice_noteimg; ?>"  style="width:250px;border:1px solid #CCC;margin-top:20px;border-radius:3px;box-shadow:0px 0px 10px 0px #ddd;float:left;" />
	                            	<div class="clear"></div>
	                            	<div class="caption"></div>
	                            	<a href="misc-settings.php?invoice_note_img_delete" class="btn btn-danger btn-block pull-left" style="width:250px;margin-top:10px;"> <i class="fa fa-times"></i></a>
	                            <?php
	                            	}
	                            ?>
	                        </form>
                        </div><!--form-->
                        <div class="clear"></div>
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">Invoice Note(text) : </div>
    	                        <div class="field" style="width:340px;">
                                    <textarea style="min-height:100px;" class="form-control" name="invoice_notetxt"><?php echo $invoice_notetxt; ?></textarea>
        	                    </div>
        	                    <div class="clear"></div>

        	                    <div class="caption" style="width:150px;"></div>
        	                    <div class="field">
        	                    	<input type="submit" name="update" value="Update" class="btn btn-primary" style="margin-left:0px;" />
        	                    </div>
                                <div class="clear"></div>
                            </form>
                        </div><!--form-->

						<?php
							if(strpos($_SERVER['HTTP_USER_AGENT'],'Linux') == false){
						?>

						<div id="form">
							<div class="caption" style="width:150px;"></div>
							<div class="field">
								<a href="clean-temp.php" > <img src="resource/images/MedIcons/cleanup.png" alt="" /> </a>
							</div>
							<div class="clear"></div>
            </div><!--form-->

						<?php
							}
						?>

						<?php
							if($admin){
								?>
								<div id="form">
									<div class="caption" style="width:150px;"></div>
									<div class="field">
										<button class="clear_blank_vouchers btn btn-default" > <i class="fa fa-recycle"></i> Delete Blank Vouchers </button>
									</div>
									<div class="clear"></div>
		            </div><!--form-->
								<?php
							}
						?>

                        <?php /*

                        <div id="form" style="">
                        	<div class="caption">Tax Reporting</div>
	                        <div class="field">
                        		<input name="tax_enable" <?php echo $tax_check_enabled; ?> type="checkbox" id="tax-config" class="css-checkbox" value='1' />
                        		<label class="css-label" for="tax-config"></label>
    	                    </div>
                            <div class="clear"></div>
                        </div><!--form-->
                        <div id="form" style="">
                        	<div class="caption">Admin SMS</div>
	                        <div class="field">
                        		<input name="admin_sms" <?php echo $admin_sms_enabled; ?> type="checkbox" id="admin-sms-config" class="css-checkbox" value='1' />
                        		<label class="css-label" for="admin-sms-config"></label>
    	                    </div>
                            <div class="clear"></div>
                        </div><!--form-->
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">Admin Message</div>
    	                        <div class="field">
                            		<textarea class="input_size" style="width:300px;height: 100px;" name="adminmsg"><?php echo $admin_message; ?></textarea>
                            		<span class="text-muted pull-left">e.g [INVOICE] [DATE] [AMOUNT]</span>
        	                    </div>
        	                    <div class="clear"></div>
        	                    <div class="caption" style="width:150px;"></div>
        	                    <div class="field">
        	                    	<input type="submit" name="admin_msg" value="Update" class="button-green" style="height:30px;margin-left:20px;" />
        	                    </div>
                                <div class="clear"></div>
                            </form>
                        </div><!--form-->
                        <div id="form">
                        	<form action="" method="post">
	                        	<div class="caption" style="width:150px;">Invoice Message</div>
    	                        <div class="field">
                            		<textarea class="input_size" style="width:300px;height: 100px;" name="invoicemsg"><?php echo $invoice_message; ?></textarea>
                            		<span class="text-muted pull-left">e.g [INVOICE] [DATE] [AMOUNT]</span>
        	                    </div>
        	                    <div class="clear"></div>
        	                    <div class="caption" style="width:150px;"></div>
        	                    <div class="field">
        	                    	<input type="submit" name="invoice_msg" value="Update" class="button-green" style="height:30px;margin-left:20px;" />
        	                    </div>
                                <div class="clear"></div>
                            </form>
                        </div><!--form-->
                        */ ?>
               			<div style=" clear:both;"></div>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
        <div id="xfade"></div>
	</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
		if(isset($message)){
?>
			displayMessage("<?php echo $message; ?>");
<?php
		}
	?>
</script>
