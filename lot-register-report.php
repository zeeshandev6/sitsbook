<?php
	include('common/connection.php');
  include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/machines.php');
	include('common/classes/measure.php');
	include('common/classes/customers.php');
	include('common/classes/emb_stitch_account.php');
	include('common/classes/machine_sales.php');
	include('common/classes/company_details.php');

	$objChartOfAccounts = new ChartOfAccounts();
	$objEmbLotRegister  = new EmbLotRegister();
	$objMachine         = new machines();
	$objMeasure         = new Measures();
	$objStitch          = new EmbStitchAccount();
	$objMachineSales    = new machineSales();
	$objCustomers       = new customers();
	$objCompanyDetails 	 = new CompanyDetails();

	$customersList = $objCustomers->getList();

	if(isset($_POST['search'])){
		$objEmbLotRegister->fromDate 				= ($_POST['from_date']=="")?"":date('Y-m-d',strtotime($_POST['from_date']));
		$objEmbLotRegister->toDate 					= ($_POST['to_date']=="")?"":date('Y-m-d',strtotime($_POST['to_date']));
		$objEmbLotRegister->designNum 			= mysql_real_escape_string($_POST['designNum']);
		$objEmbLotRegister->customerAccCode = mysql_real_escape_string($_POST['customerCode']);
		$objEmbLotRegister->machineNum 			= mysql_real_escape_string(trim($_POST['machineNum']));
		$objEmbLotRegister->lotStatus 			= mysql_real_escape_string($_POST['lot_status']);

		if($objEmbLotRegister->fromDate==''){
			$objEmbLotRegister->fromDate = date('01-m-Y');
		}
		$report = $objEmbLotRegister->report();
		$company_details        = $objCompanyDetails->getActiveProfile();
	}
	$machineList = $objMachine->getList();

	$billing_types 			= array();
	$billing_types['S'] = 'Stitches';
	$billing_types['Y'] = 'Yards';
	$billing_types['U'] = 'Suites';
	$billing_types['L'] = 'Laces';
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     		 />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       		 />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     	 />
    <link rel="stylesheet" href="resource/css/reports-horizontal.css" type="text/css"  		     		 />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     			 />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  	 />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  				 />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  			 />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  				 />
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/emb.inward.config.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.tablesorter.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$("select").selectpicker();
			});
		</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <P> Lot Register Report</P>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form" style="width: 900px; margin: 0 auto;">
<?php
					if(isset($message)){
?>
						<div class="caption"></div><div class="redColor"><?php echo $message; ?></div>
<?php
					}
?>
                    <form method="post" action="">

                        <div class="caption">Start Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($objEmbLotRegister->fromDate))?date('d-m-Y',strtotime($objEmbLotRegister->fromDate)):""; ?>" name="from_date" class="form-control datepicker"  style="width:145px;"/>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">End Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($objEmbLotRegister->toDate))?date('d-m-Y',strtotime($objEmbLotRegister->toDate)):date('d-m-Y'); ?>" name="to_date" class="form-control datepicker" style="width:145px;" />
                        </div>
                        <div class="clear"></div>
                        <div class="caption">Machine</div>
                        <div class="field">
                            <select name="machineNum" class="form-control">
                            <option value=""></option>
<?php
						if(mysql_num_rows($machineList)){
							while($machine = mysql_fetch_array($machineList)){
?>
								<option value="<?php echo $machine['ID']; ?>"><?php echo $machine['MACHINE_NO']; ?> - <?php echo $machine['NAME']; ?></option>
<?php
							}
						}
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Customer</div>
                        <div class="field" style="width:250px;">
                        	<select class="custCodeSelector show-tick form-control" name="customerCode" data-live-search="true" >
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
                                $selected = (isset($outward)&&$outward['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                               <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Design Number</div>
                        <div class="field">
                            <input type="text" value="" name="designNum" class="form-control" style="width:145px;" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Lot Status:</div>
                        <div class="field">
                            <input type="radio" id="lot-state-1" class="css-checkbox" name="lot_status" value="" checked="checked" />
                            <label for="lot-state-1" class="css-label-radio">All</label>
														<div class="clear"></div>

                            <input type="radio" id="lot-state-2v" class="css-checkbox" name="lot_status" value="O" />
														<label for="lot-state-2v" class="css-label-radio">Delivered</label>
														<div class="clear"></div>

                            <input type="radio" id="lot-state-2" class="css-checkbox" name="lot_status" value="C" />
                            <label for="lot-state-2" class="css-label-radio">Completed</label>
														<div class="clear"></div>

                            <input type="radio" id="lot-state-3" class="css-checkbox" name="lot_status" value="P" />
                            <label for="lot-state-3" class="css-label-radio">Processing</label>
														<div class="clear"></div>

                            <input type="radio" id="lot-state-4" class="css-checkbox" name="lot_status" value="R" />
                            <label for="lot-state-4" class="css-label-radio">Claimed</label>
														<div class="clear"></div>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="btn btn-primary" />
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
				<?php
                    if(isset($report)){
                ?>
        	<div class="content-box-content">
	            <span style="float:right;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printTable">
									<div class="pageHeader">

										<div class="logo_aside">
											<h1 class="h1"><?php echo $company_details['NAME']; ?></h1>
											<p class="slogan">
												Cloth Inward Report
											</p>
											<p class="underSlogan">
												<?php echo ($objEmbLotRegister->fromDate=="")?"From Begining ":"From Period ".date('d-m-Y',strtotime($objEmbLotRegister->fromDate)); ?>  <?php echo ($objEmbLotRegister->toDate=="")?" To Period ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objEmbLotRegister->toDate)); ?>
											</p>
										</div>
										<div class="clear"></div>
										<p class="repoDate" style="text-align:left !important;">Report Generated On : <?php echo date('d-m-Y'); ?></p>
									</div>
									<div class="clear"></div>
                    <table class="tableBreak">
                        <thead class="tHeader">
                            <tr style="background:#EEE;">
                               <th width="5%"  class="text-center">Lot Status</th>
                               <th width="7%"  class="text-center">IssueDate</th>
															 <th width="10%" class="text-center">CustomerName</th>
                               <th width="4%"  class="text-center">Lot #</th>
                               <th width="7%"  class="text-center">Quality</th>
															 <th width="5%"  class="text-center">Measure</th>
															 <th width="5%"  class="text-center">Billing Type</th>
															 <th width="5%"  class="text-center">Stitches</th>
															 <th width="5%"  class="text-center">TP</th>
                               <th width="5%"  class="text-center">Thaan Issued</th>
                               <th width="5%"  class="text-center">Emb@</th>
															 <th width="5%"  class="text-center">Emb Amount</th>
                               <th width="4%"  class="text-center">Stitch@</th>
															 <th width="5%"  class="text-center">Stich Amount</th>
                               <th width="7%"  class="text-center">Stitch.A/c</th>
															 <th width="5%"  class="text-center">Design</th>
															 <th width="4%"  class="text-center">Mach.Num</th>
															 <th width="3%"  class="text-center">Waste</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
					if(mysql_num_rows($report)){
						$lastCustCode = "";
						$newMLength = 0;
						$embAmountTotal = 0;
						$stitchAmountTotal = 0;
							while($detailRow = mysql_fetch_array($report)){

								$measureMent   = $objMeasure->getName($detailRow['MEASURE_ID']);
								$stitchAccount = $objChartOfAccounts->getAccountTitleByCode($detailRow['STITCH_ACC']);
								$lotStatusType = ($detailRow['LOT_STATUS']=='P')?"PROCESSING":"";
								$lotStatusType = ($detailRow['LOT_STATUS']=='C')?"COMPLETED":$lotStatusType;
								$lotStatusType = ($detailRow['LOT_STATUS']=='O')?"DELIVERED":$lotStatusType;
								$lotStatusType = ($detailRow['LOT_STATUS']=='R')?"CLAIMED":$lotStatusType;

?>
								<tr id="recordPanel" class="alt-row <?php echo ($lotStatusType == 'CLAIMED')?"hasTdReds":""; ?>">

									<td style="text-align:center;font-size:12px !important;"><?php echo $lotStatusType; ?></td>
<?php
?>
									<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($detailRow['LOT_DATE'])); ?></td>
									<td style="text-align:left;font-size:11px !important"><?php echo $objCustomers->getTitle($detailRow['CUST_ACC_CODE']); ?></td>
									<td style="text-align:center"><?php echo $detailRow['LOT_NO']; ?></td>
									<td style="text-align:left; font-size:10px !important"><?php echo $detailRow['QUALITY']; ?></td>
									<td style="text-align:center"><?php  echo $measureMent; ?></td>
									<td style="text-align:center"><?php echo $billing_types[$detailRow['BILLING_TYPE']]; ?></td>
									<td style="text-align:center"><?php echo $detailRow['STITCHES']; ?></td>
									<td style="text-align:center"><?php echo $detailRow['TOTAL_LACES']; ?></td>
									<td style="text-align:center;"><?php echo $detailRow['MEASURE_LENGTH']; ?></td>
									<td style="text-align:center;"><?php echo $detailRow['EMB_RATE']; ?></td>
									<td style="text-align:center;"><?php echo number_format($detailRow['EMB_AMOUNT'],2); ?></td>
									<td style="text-align:center;"><?php echo $detailRow['STITCH_RATE']; ?></td>
									<td style="text-align:center;"><?php echo number_format($detailRow['STITCH_AMOUNT'],2); ?></td>
									<td style="text-align:left;font-size:12px !important;"><?php echo $stitchAccount; ?></td>
									<td style="text-align:center"><?php echo $detailRow['DESIGN_CODE']; ?></td>
									<td style="text-align:center"><?php echo $objMachine->getRecordDetailsValue($detailRow['MACHINE_ID'],'MACHINE_NO')."-".$objMachine->getName($detailRow['MACHINE_ID']); ?></td>
									<td style="text-align:center"><?php echo $detailRow['GP_NO']; ?></td>
								</tr>
<?php
										$lastCustCode       = $detailRow['CUST_ACC_CODE'];
										$newMLength        += ($detailRow['LOT_STATUS'] == 'R')?$detailRow['MEASURE_LENGTH']*-1:$detailRow['MEASURE_LENGTH'];
										$embAmountTotal    += ($detailRow['LOT_STATUS'] == 'R')?$detailRow['EMB_AMOUNT']*-1:$detailRow['EMB_AMOUNT'];
										$stitchAmountTotal += ($detailRow['LOT_STATUS'] == 'R')?$detailRow['STITCH_AMOUNT']*-1:$detailRow['STITCH_AMOUNT'];
							}
					}
?>
                        </tbody>
<?php
							if(!isset($newMLength)&&!isset($embAmountTotal)){
								$newMLength = 0;
								$embAmountTotal = 0;
							}
?>
                        <tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right; font-weight:bold" colspan="8"> Total:</td>
                                <td style="text-align:center;" ><?php echo (isset($newMLength))?$newMLength:0; ?></td>
                                <td style="text-align:center;"> - - - </td>
                                <td style="text-align:center;"> - - - </td>
																<td style="text-align:center;" ><?php echo (isset($embAmountTotal))?$embAmountTotal:0; ?></td>
																<td style="text-align:center;"> - - - </td>
																<td style="text-align:center;" ><?php echo (isset($stitchAmountTotal))?$stitchAmountTotal:0; ?></td>
                                <td style="text-align:center;" colspan="4"> - - - </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="clear"></div>
                </div> <!--End bodyTab-->
                <div style="height:0px;clear:both"></div>
                <?php
					}
				?>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
</body>
</html>
<script>
	$(document).ready(function(e){
			$("tr.hasTdReds").find('td').css({'color':'#F00 !important'});
			$(".printThis").click(function(){
				if($("div.tablePage").length==0){
					var MaxHeight = 580;
					var RunningHeight = 0;
					var PageNo = 1;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if (RunningHeight + $(this).height() > MaxHeight) {
							RunningHeight = 0;
							PageNo += 1;
						}
						if(PageNo > 1){
							MaxHeight = 640;
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var tableFooter = $(".tableFooter").html();
					var repoDate    = $(".repoDate").text();
					//remove previous thead/tfoot/ReportDate
					$(".tHeader").remove();
					$(".repoDate").remove();
					$(".tableFooter").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						$('table.tableBreak').parent().append("<div class='clear'></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						//$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					$(".newTable").last().append(tableFooter);
					$("div.tablePage").each(function(){
						//$("div.pageHeader").first().clone().insertBefore($(this).find("table.newTable"));
						$(this).find("table.newTable thead").html(tableHeader);
					});
					//$("div.pageHeader").first().remove();
					$('table.tableBreak').remove();
				}
				$(".printTable").printThis({
						debug: false,
						importCSS: false,
						printContainer: true,
						 loadCSS: 'resource/css/reports-horizontal.css',
						pageTitle: "Emque Embroidery",
						removeInline: false,
						printDelay: 500,
						header: null
					});
			});
	});
</script>
