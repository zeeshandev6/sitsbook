<?php
include("common/connection.php");
include('common/config.php');

$objUsers      = new UserAccounts();
$objConfigs    = new Configs();

$userList      = $objUsers->getFullList();

$use_branches  = $objConfigs->get_config('USE_BRANCHES');

if(!$admin){
	exit();
}
if(isset($_GET['usr'])){
	$user_id = mysql_real_escape_string($_GET['usr']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>User Permissions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style>
	h4{
		font-size: 16px;
		font-weight: normal;
		padding: 5px;
	}
	.panel{
		padding: 0px;
	}
	.panel-heading{
		padding: 0px;
	}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='user_acc_id']").selectpicker();
			$(".col-xs-12.col-md-8.col-lg-8").each(function(){
				$("<div class='clear'></div>").insertAfter($(this));
			});
			$(".glyphicon").css({'color':'#333'});
			$("*[name='save']").click(function(){
				var $user_id  	 = $("select[name='user_acc_id'] option:selected").val();
				var $user_name   = $("select[name='user_acc_id'] option:selected").text();
				var $permissions = '';
				var financial_flag = false;
				$("input.checkActive:checked").each(function(index, element) {
					if($(this).parent().parent().parent().hasClass("financials-tab")){
						financial_flag = true;
					}
					if(index > 0){
						$permissions += '|';
					}
					$permissions += $(this).val();
				});
				if(financial_flag){
					$permissions += '|';
					$permissions += 'financials-tab';
				}
				$.post('db/save-permission.php',{user_id:$user_id,permissions:$permissions},function(data){
					if(data != ''){
						data = $.parseJSON(data);
						if(data['OK'] == 'Y'){
							displayMessage('Permissions Saved Successfully For '+$user_name);
						}else if(data['OK'] == 'N'){
							displayMessage('Error! Permissions Could Not Be Saved.');
						}
					}
				});
			});
			$("input.checkActive").change(function(){
				var $this = $(this);
				var checked = $this.parent().parent().parent().find("input.checkActive:checked").length;
				var notChecked = $this.parent().parent().parent().find("input.checkActive").length - checked;
				if(checked > notChecked){
					$this.parent().parent().parent().find("input.toggle-x").prop('checked',true);
				}else if(checked < notChecked){
					$this.parent().parent().parent().find("input.toggle-x").prop('checked',false);
				}
			});
		});
	</script>
</head>
<body>
	<div id="body-wrapper">
		<div id="sidebar">
			<?php include("common/left_menu.php") ?>
		</div> <!-- End #sidebar -->
		<div id = "bodyWrapper">
			<div class = "content-box-top">
				<div class = "summery_body">
					<div class = "content-box-header">
						<p>User Permissions Management</p>
						<span id="tabPanel">
							<div class="tabPanel">
								<div class="tabSelected" id="tab1">Details</div>
								<a class="tab" href="users-management.php?tab=list">List</a>
							</div>
						</span>
						<div style = "clear:both;"></div>
					</div><!-- End .content-box-header -->
					<div style = "height:30px"></div>
					<div id = "bodyTab1" style="display:block">
						<form method="post" action="" class="form-horizontal">
							<div id = "form">
								<div class="col-xs-12" style="padding: 0em;">
									<?php
									if($admin){
										?>
										<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
											<div class="form-group">
										    <label for="inputEmail3" class="col-xs-2 control-label">User:</label>
										    <div class="col-xs-8">
													<select name="user_acc_id" class="show-tick form-control">
														<option value="0"></option>
														<?php
														if(mysql_num_rows($userList)){
															while($userRow = mysql_fetch_array($userList)){
																$userSelected = (isset($user_id)&&$user_id==$userRow['ID'])?"selected":"";
																if($userRow['ID'] == 1){
																	continue;
																}
																?>
																<option value="<?php echo $userRow['ID']; ?>" <?php echo $userSelected; ?> ><?php echo $userRow['USER_NAME']; ?></option>
																<?php
															}
														}
														?>
													</select>
										    </div>
										  </div>
										</div>
										<div class="clear"></div>
										<hr>
										<div class="clear" style="height:30px"></div>
										<?php
									}
									?>
									<div class="clear"></div>
									<div class="col-xs-12 panel panel-default">
										<div class="panel-heading">
											<div class="col-xs-4"><h4><b>Dashboard</b></h4></div>
											<div class="col-xs-8" style="padding: 1em;">
												<input id="cmn-toggle-6" value="dashboard" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-6" class="css-label" class="css-label" class="css-label"></label>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="col-xs-12 panel panel-default">
										<div class="panel-heading">
											<div class="col-xs-4"><h4><b>General journal</b></h4></div>
											<div class="col-xs-8" style="padding: 1em;">
												<input id="cmn-toggle-pov" class="css-checkbox toggle-x" type="checkbox" >
												<label for="cmn-toggle-pov" class="css-label" class="css-label"></label>
											</div>
											<div class="clear"></div>
										</div>
										<?php
											if($al_vouchers == 'Y' || $po_vouchers == 'Y'){
										?>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Journal Voucher</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-1" value="journal-voucher" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-1" class="css-label" class="css-label"></label>
											</div>
										</div>
										<?php
											}
										?>
										<?php
											if($cb_vouchers == 'Y'){
										?>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Cash Book</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-1cashb" value="cash-book-voucher" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-1cashb" class="css-label" class="css-label"></label>
											</div>
										</div>
										<?php
											}
											if($al_vouchers == 'Y'){
										?>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Cash Receipt</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-2" value="cash-receipt" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-2" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Cash Payment</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-3" value="cash-payment" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-3" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Bank Receipt</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-4" value="bank-receipt" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-4" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Bank Payment</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-5" value="bank-payment" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-5" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Search Vouchers</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-vxn" value="search-vouchers" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-vxn" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Reversals</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-vxvn" value="search-vouchers-reversal" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-vxvn" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Modification</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-vxvnx" value="search-vouchers-modify" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-vxvnx" class="css-label"></label>
											</div>
										</div>
										<?php
											}
										 ?>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Cash Visibility</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-2acash" value="cash-visible" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-2acash" class="css-label"></label>
											</div>
										</div>
									</div>
									<div style="height:15px;"></div>

									<div class="col-xs-12 panel panel-default">
										<div class="panel-heading">
											<div class="col-xs-4"><h4><b>Purchase Ordering</b></h4></div>
											<div class="col-xs-8" style="padding: 1em;">
												<input id="cmn-toggle-17pordrng" value="mobile-purchase-returns" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-17pordrng" class="css-label"></label>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div style="height:20px;"></div>
									<?php
									if($rentalBookingAddon == 'Y'){
									?>
									<div class="col-xs-12 panel panel-default">
										<div class="panel-heading">
											<div class="col-xs-4"><h4><b>Rental/Order Management</b></h4></div>
											<div class="col-xs-8" style="padding: 1em;">
												<input id="cmn-toggle-pov-rom" class="css-checkbox toggle-x" type="checkbox" >
												<label for="cmn-toggle-pov-rom" class="css-label"></label>
											</div>
											<div class="clear"></div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Rental Management</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-1-rom" value="rental-booking" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-1-rom" class="css-label"></label>
											</div>
										</div>
										<div class="col-xs-12" style="padding: 1em;">
											<div class="col-xs-4 text-right">Order Management</div>
											<div class="col-xs-3">
												<input id="cmn-toggle-2-rom" value="order-detail" class="css-checkbox checkActive" type="checkbox" >
												<label for="cmn-toggle-2-rom" class="css-label"></label>
											</div>
										</div>
									</div>
									<?php
									}
									?>
									<?php if($salePurchaseModule == 'Y'||$distSalesAddon == 'Y'){ ?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Purchase Management</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-pov2" class="css-checkbox toggle-x" type="checkbox" >
													<label for="cmn-toggle-pov2" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
											<div class="col-xs-12" style="padding: 1em;">
												<div class="col-xs-4 text-right">Purchases</div>
												<div class="col-xs-3">
													<input id="cmn-toggle-6f" value="purchase" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-6f" class="css-label"></label>
												</div>
												<div class="clear" style="height:20px;"></div>
												<div class="col-xs-9">
													<div class="col-xs-6 text-right">Modify</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-9a" value="modify-purchase" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9a" class="css-label"></label>
													</div>
													<div class="clear"></div>
													<div class="col-xs-6 text-right">Delete</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-9b" value="delete-purchase" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9b" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
											</div>
											<div class="col-xs-12" style="padding: 1em;">
												<div class="col-xs-4 text-right">Purchase Returns</div>
												<div class="col-xs-3">
													<input id="cmn-toggle-7" value="purchase-return" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-7" class="css-label"></label>
												</div>
											</div>
										</div>
										<?php } ?>
										<?php
										if($mobileAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="col-xs-4"><h4><b>Mobile Purchases</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-17" value="mobile-purchase" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-17" class="css-label"></label>
												</div>
												<div class="clear"></div>
												<div class="clear" style="height:10px;"></div>
												<div class="col-xs-9">
													<div class="col-xs-6 text-right">Modify</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-17a" value="modify-mobile-purchase" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-17a" class="css-label"></label>
													</div>
													<div class="clear"></div>
													<div class="col-xs-6 text-right">Delete</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-17b" value="delete-mobile-purchase" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-17b" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
											</div>
											<div class="col-xs-12 panel panel-default">
												<div class="col-xs-4"><h4><b>Mobile Purchase Returns</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-17mpr" value="mobile-purchase-returns" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-17mpr" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
											<?php
										}
										?>
										<?php
										if($fabricProcessAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Contracts Management</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov2contract" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov2contract" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Grey Fabric</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6ffabcont" value="fabric-contract" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6ffabcont" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-4 text-right">Dyeing</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6fprocont" value="processing-contract" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6fprocont" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-4 text-right">Packing</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6fpackcont" value="packing-contract" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6fpackcont" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-4 text-right">Embroidery</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6fembcont" value="embroidery-contract" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6fembcont" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>
												</div>
											</div>
											<?php
										}
										?>
										<?php
										if($inwardOutwardAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Inward/Outward Management</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov2inout" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov2inout" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Inward</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6finward" value="fabric-inward" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6finward" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>
													<div class="col-xs-4 text-right">Outward</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-6foutward" value="fabric-outward" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-6foutward" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>
												</div>
											</div>
											<?php
										}
										if($messagingAddon == 'Y'){
										?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Messages</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-8" value="messages" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-8" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
										</div>
<?php
									}
 									if($taskingAddon == 'Y'){
?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Tasking</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-8txt" value="tasking" class="css-checkbox checkActive" type="checkbox" />
													<label for="cmn-toggle-8txt" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
										</div>
										<?php
									}
										if($workInProcessAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Work In Process Management</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov3x" value="" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov3x" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Work In Process</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-8xa" value="work-in-process" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-8xa" class="css-label"></label>
													</div>

												</div>
											</div>
											<?php
										}
										?>
										<?php
										if($salePurchaseModule == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Sale Management</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov4" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov4" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Sale Orders</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-9so" value="sale-orders" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9so" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>
													<div class="col-xs-4 text-right">Sales</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-9" value="sales" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-4 text-right">Sales Cashier</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-9sconfirmation" value="sales-confirmation" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9sconfirmation" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-4 text-right">Sales Panel</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-9sp" value="sales-panel" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9sp" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-9y" value="modify-sales" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-9y" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-9x" value="delete-sales" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-9x" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
												<hr />
												<div class="clear"></div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Sale Returns</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-10" value="sale-returns" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-10" class="css-label"></label>
													</div>
												</div>
											</div>
											<?php
										}
										?>
										<?php
										if($embSalesAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Embroidery Sales</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov4es" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov4es" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Sales</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-9es" value="sales" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-9es" class="css-label"></label>
													</div>
													<div class="clear" style="height:20px;"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-9yes" value="modify-sales" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-9yes" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-9xes" value="delete-sales" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-9xes" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
												<hr />
												<div class="clear"></div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Sale Returns</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-10" value="sale-returns" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-10" class="css-label"></label>
													</div>

												</div>
											</div>
<?php
										}
										if($embProdAddon == 'Y'){
											?>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Embroidery Production</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-pov4ep" class="css-checkbox toggle-x" type="checkbox" >
														<label for="cmn-toggle-pov4ep" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Inward</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-inward" value="emb-inward" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-inward" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-inward-modify" value="emb-inward-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-inward-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-inward-delete" value="emb-inward-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-inward-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Lot Register</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-lot" value="emb-lot-register" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-lot" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-lot-modify" value="emb-lot-register-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-lot-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>

														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-lot-delete" value="emb-lot-register-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-lot-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Outward</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-outward" value="emb-outward" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-outward" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-outward-modify" value="emb-outward-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-outward-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-outward-delete" value="emb-outward-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-outward-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Print</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-outward-print" value="emb-outward-print" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-outward-print" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Billing</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-billing" value="emb-billing" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-billing" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-billing-modify" value="emb-billing-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-billing-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-billing-delete" value="emb-billing-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-billing-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Print</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-billing-print" value="emb-billing-print" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-billing-print" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear"></div>
												</div>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Inventory Issue Items</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-issue-items" value="emb-issue-items" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-issue-items" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-issue-items-modify" value="emb-issue-items-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-issue-items-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-issue-items-delete" value="emb-issue-items-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-issue-items-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear" style="height:20px;"></div>
												</div>
												<div class="clear"></div>

												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Inventory Issue Item Returns</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-emb-issue-item-returns" value="emb-issue-item-returns" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-emb-issue-item-returns" class="css-label"></label>
													</div>
													<div class="clear mt-20"></div>

													<div class="col-xs-9">
														<div class="col-xs-6 text-right">Modify</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-issue-item-returns-modify" value="emb-issue-item-returns-modify" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-issue-item-returns-modify" class="css-label"></label>
														</div>
														<div class="clear"></div>
														<div class="col-xs-6 text-right">Delete</div>
														<div class="col-xs-6">
															<input id="cmn-toggle-emb-issue-item-returns-delete" value="emb-issue-item-returns-delete" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-emb-issue-item-returns-delete" class="css-label"></label>
														</div>
														<div class="clear"></div>
													</div>
													<div class="clear" style="height:20px;"></div>
												</div>
												<div class="clear"></div>
											</div>
<?php
										}
										if($invoicePurchasesAddon == 'Y'){
?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Invoice - Purchases</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-pinvoice" value="invoice-purchases" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-pinvoice" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
										</div>
<?php
										}
										if($invoiceSalesAddon == 'Y'){
?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Invoice - Sales</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-invoice-sales" value="invoice-sales" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-invoice-sales" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
										</div>
<?php
										}
										if($gatePassAddon == 'Y'){
?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Gate Pass</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-gp" value="gate-pass" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-gp" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
										</div>
<?php
										}
										if($mobileAddon == 'Y'){
?>
											<div class="col-xs-12 panel panel-default">
												<div class="col-xs-4"><h4><b>Mobile Sales</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-27" value="mobile-sales" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-27" class="css-label"></label>
												</div>
												<div class="clear"></div>
												<div class="clear" style="height:10px;"></div>
												<div class="col-xs-9">
													<div class="col-xs-6 text-right">Modify</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-27a" value="modify-mobile-sales" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-27a" class="css-label"></label>
													</div>
													<div class="clear"></div>
													<div class="col-xs-6 text-right">Delete</div>
													<div class="col-xs-6">
														<input id="cmn-toggle-27b" value="delete-mobile-sales" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-27b" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
											</div>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Mobile Sale Returns</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-27msr" value="mobile-sale-return" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-27msr" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
											</div>
											<div class="col-xs-12 panel panel-default">
												<div class="panel-heading">
													<div class="col-xs-4"><h4><b>Open Mobile Sale Returns</b></h4></div>
													<div class="col-xs-8" style="padding: 1em;">
														<input id="cmn-toggle-27omsr" value="open-mobile-sale-return" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-27omsr" class="css-label"></label>
													</div>
													<div class="clear"></div>
												</div>
											</div>
											<?php
										}
										if($chequesAddon == 'Y'){
										?>
										<div class="col-xs-12 panel panel-default">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Cash Management</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-pov5" class="css-checkbox toggle-x" type="checkbox" >
													<label for="cmn-toggle-pov5" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>

											<div class="col-xs-12" style="padding: 1em;">
												<div class="col-xs-4 text-right">Receipts</div>
												<div class="col-xs-3">
													<input id="cmn-toggle-11" value="cash-receipts" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-11" class="css-label"></label>
												</div>
											</div>

											<div class="col-xs-12" style="padding: 1em;">
												<div class="col-xs-4 text-right">Payments</div>
												<div class="col-xs-3">
													<input id="cmn-toggle-12" value="cash-payments" class="css-checkbox checkActive" type="checkbox" >
													<label for="cmn-toggle-12" class="css-label"></label>
												</div>
											</div>
										</div>
										<?php
										}
										?>
										<div class="col-xs-12 panel panel-default financials-tab">
											<div class="panel-heading">
												<div class="col-xs-4"><h4><b>Financial Reports</b></h4></div>
												<div class="col-xs-8" style="padding: 1em;">
													<input id="cmn-toggle-pov6" value="" class="css-checkbox toggle-x" type="checkbox" >
													<label for="cmn-toggle-pov6" class="css-label"></label>
												</div>
												<div class="clear"></div>
											</div>
											<?php if($salePurchaseModule == 'Y' || $mobileAddon == 'Y'){ ?>
												<div class="col-xs-12" style="padding: 1em;">
													<div class="col-xs-4 text-right">Daily Transaction Report</div>
													<div class="col-xs-3">
														<input id="cmn-toggle-33" value="daily-report" class="css-checkbox checkActive" type="checkbox" >
														<label for="cmn-toggle-33" class="css-label"></label>
													</div>

												</div>
												<?php } ?>
												<?php
												if($mobileAddon == 'Y'){
													?>
													<div class="col-xs-12" style="padding: 1em;">
														<div class="col-xs-4 text-right">Scan Purchase Report</div>
														<div class="col-xs-3">
															<input id="cmn-toggle-30" value="mobile-purchase-report" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-30" class="css-label"></label>
														</div>
													</div>
													<div class="col-xs-12" style="padding: 1em;">
														<div class="col-xs-4 text-right">Scan Sale Report</div>
														<div class="col-xs-3">
															<input id="cmn-toggle-31" value="mobile-sale-report" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-31" class="css-label"></label>
														</div>
													</div>
													<?php
												}
												?>
												<?php if($salePurchaseModule == 'Y'){ ?>
													<div class="col-xs-12" style="padding: 1em;">
														<div class="col-xs-4 text-right">Purchase Report</div>
														<div class="col-xs-3">
															<input id="cmn-toggle-13" value="purchase-report" class="css-checkbox checkActive" type="checkbox" >
															<label for="cmn-toggle-13" class="css-label"></label>
														</div>
													</div>
													<?php } ?>
													<?php
													if($workInProcessAddon == 'Y'){
														?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Work In Process Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14" value="wip-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14" class="css-label"></label>
															</div>
														</div>
														<?php
													}
													?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Party Receivables Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14partyrr" value="party-receivables-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14partyrr" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Party Payables Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14partypr" value="party-payables-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14partypr" class="css-label"></label>
															</div>
														</div>
													<?php
													if($chequesAddon == 'Y'){
														?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Cheque Receipts Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14chqrr" value="cheques-receipts-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14chqrr" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Cheque Payments Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14chqpr" value="cheques-payments-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14chqpr" class="css-label"></label>
															</div>
														</div>
														<?php
													}
													?>
													<?php
													if($embProdAddon == 'Y'){
														?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Inward Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14eir" value="emb-inward-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14eir" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Outward Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14eor" value="emb-outward-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14eor" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Lot Register Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14elrr" value="emb-lot-register-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14elrr" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Machine Production Report [ Print Only ]</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14emprpo" value="machine-production-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14emprpo" class="css-label"></label>
															</div>
															<div class="clear"></div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Machine Production Report [ Full Access ]</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14emprfa" value="machine-production-report-full" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14emprfa" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"> <b class="text-dark pull-left">Embroidery</b> Items Issue Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-14eiir" value="emb-stocks-issue-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-14eiir" class="css-label"></label>
															</div>
														</div>
														<div class="clear"></div>
														<hr />
														<?php
													}
													?>
													<?php if($salePurchaseModule == 'Y'){ ?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Sales Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-15" value="sales-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-15" class="css-label"></label>
															</div>

														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Sales Tax Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-staxr" value="sales-tax-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-staxr" class="css-label"></label>
															</div>
														</div>
														<?php } ?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Aging Report</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-16" value="aging-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-16" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Check Barcode</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-25" value="barcode-check" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-25" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">General Invoice</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-25gi" value="new-invoice-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-25gi" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Cash Book</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-17" value="cash-book" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-17" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">General Ledger</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-18" value="general-ledger" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-18" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Trial Balance</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-19" value="trial-balance" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-19" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Profit &amp; Loss</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-20" value="profitAndLoss" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-20" class="css-label"></label>
															</div>
														</div>

														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Balance Sheet</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-21" value="balanceSheet" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-21" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Available Stock Report </div>
															<div class="col-xs-3">
																<input id="cmn-toggle-22" value="available-stock-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-22" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Demand Report </div>
															<div class="col-xs-3">
																<input id="cmn-toggle-2demand" value="demand-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-2demand" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Stock Report </div>
															<div class="col-xs-3">
																<input id="cmn-toggle-2brnchstock" value="branch-stock-report" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-2brnchstock" class="css-label"></label>
															</div>
														</div>
													</div>

													<div class="col-xs-12 panel panel-default">
														<div class="panel-heading">
															<div class="col-xs-4"><h4><b>General Settings</b></h4></div>
															<div class="col-xs-8" style="padding: 1em;">
																<input id="cmn-toggle-gsmain" value="" class="css-checkbox toggle-x" type="checkbox" >
																<label for="cmn-toggle-gsmain" class="css-label"></label>
															</div>
															<div class="clear"></div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Company Profile</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-cp" value="company-profile" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-cp" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Banks Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-bm" value="banks-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-bm" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Suppliers Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-supplier" value="suppliers-management" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-supplier" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Customers Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-customers" value="customer-management" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-customers" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Brokers Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-brokers" value="brokers-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-brokers" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Employee Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-employees" value="employees-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-employees" class="css-label"></label>
															</div>
														</div>
														<?php if($use_branches == 'Y'){ ?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Branch Stock Transfer</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-stock-trans" value="stock-transfer" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-stock-trans" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Branch Management</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-branchs" value="branch-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-branchs" class="css-label"></label>
															</div>
														</div>
														<?php } ?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Sheds Management</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-shed" value="shed-management" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-shed" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Category Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-category" value="category-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-category" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Services</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-servix" value="service-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-servix" class="css-label"></label>
															</div>
														</div>

														<?php if($embProdAddon=='Y'){ ?>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Product Management</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-epm" value="emb-product-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-epm" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Design Management</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-edm" value="emb-design-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-edm" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Machine Management</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-emachinem" value="machines-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-emachinem" class="css-label"></label>
															</div>
														</div>
														<?php } ?>
														<div class="col-xs-12" style="padding: 1em;padding-top: 2em !important;">
															<div class="col-xs-4 text-right">Items Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-items" value="items-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-items" class="css-label"></label>
																<div class="clear"></div>
															</div>
															<div class="clear"></div>
															<div class="col-xs-6 col-xs-offset-3">
																<div class="panel panel-primary">
																	<div class="panel-body">
																		<div class="col-xs-4 text-right">Modify/Delete</div>
																		<div class="col-xs-3">
																			<input id="cmn-toggle-items-modify" value="items-managment-modify" class="css-checkbox checkActive" type="radio" >
																			<label for="cmn-toggle-items-modify" class="css-label-radio"></label>
																			<div class="clear"></div>
																		</div>
																		<div class="clear"></div>

																		<div class="col-xs-4 text-right">Full Details</div>
																		<div class="col-xs-3">
																			<input id="cmn-toggle-items-full" value="stock-full" name="stock-limit" class="css-checkbox checkActive" type="radio" >
																			<label for="cmn-toggle-items-full" class="css-label-radio"></label>
																			<div class="clear"></div>
																		</div>
																		<div class="clear"></div>

																		<div class="col-xs-4 text-right">Stock Only</div>
																		<div class="col-xs-3">
																			<input id="cmn-toggle-items-limited" value="stock-limited" name="stock-limit" class="css-checkbox checkActive" type="radio" >
																			<label for="cmn-toggle-items-limited" class="css-label-radio"></label>
																			<div class="clear"></div>
																		</div>
																	</div>
																</div>
																<div class="clear"></div>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Measure Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-measure" value="measure-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-measure" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Currency Managment</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-currency" value="currency-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-currency" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Barcodes By Range</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-brage" value="brage-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-brage" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Barcodes By Name</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-bname" value="bname" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-bname" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Chart of Accounts</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-coa" value="coa-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-coa" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Linked Accounts</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-lcoa" value="linked-coa" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-lcoa" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Expense Accounts</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-ea" value="ea-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-ea" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Misc Settings</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-misc-setting" value="misc-setting-managment" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-misc-setting" class="css-label"></label>
															</div>
														</div>
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right">Sync Online</div>
															<div class="col-xs-3">
																<input id="cmn-toggle-sync" value="sync" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-sync" class="css-label"></label>
															</div>
														</div>
														<hr />
														<div class="col-xs-12" style="padding: 1em;">
															<div class="col-xs-4 text-right"><b>Data Back Up</b></div>
															<div class="col-xs-3">
																<input id="cmn-toggle-backup" value="backup" class="css-checkbox checkActive" type="checkbox" >
																<label for="cmn-toggle-backup" class="css-label"></label>
															</div>
														</div>
													</div>
				<div class="col-xs-2"></div>
				<div class="col-xs-3" style="padding: 1em;">

				</div><!--field-->
				<div class="col-xs-7"></div>
				<div class="clear"></div>
			</div><!---col-xs-12-->
		</div><!--form-->
		<div style = "clear:both;bodyWrapper"></div>
	</form>
</div><!--bodyTab1-->
</div><!--summery_body-->
</div><!--End.content-box-top-->
</div><!--bodyWrapper-->
</div>  <!--body-wrapper-->
<button type="submit" name="save" class="btn btn-primary btn-lg btn-floating-bottom-right"> <i class="fa fa-save"></i> Save </button>
<div id="xfade"></div>
</body>
<script type="text/javascript">
$(document).ready(function(){
	$("input.toggle-x+label").css({'margin-left':'5px'});
	$("input.toggle-x").change(function(){
		var $this = $(this);
		$(this).parent().parent().parent().find('.checkActive').prop('checked',$this.is(":checked"));
	});
	$("select[name='user_acc_id']").change(function(){
		var user_id = $(this).find('option:selected').val();
		getUserPermissionDetails(user_id);
	});
	$("select[name='user_acc_id']").change();
	<?php
	if(isset($message)){
		?>
		displayMessage('<?php echo $message; ?>');
		<?php
	}
	?>
});
var getUserPermissionDetails = function(user_id){
	if(user_id == 0 || user_id == ''){
		$("input[type='checkbox']").prop('checked',false);
		return false;
	}
	$.get('db/get-permission.php',{user_id:user_id},function(data){
		if(data != ''){
			$("input[type='checkbox']").prop('checked',false);
			data = $.parseJSON(data);
			$.each(data,function(index,value){
				if(value != ''){
					$("input.checkActive[value='"+value+"']").prop('checked',true);
				}
			});
		}
	});
}
</script>
</html>
<?php include('conn.close.php'); ?>
