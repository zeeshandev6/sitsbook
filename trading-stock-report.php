<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/mobile-purchase.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/items.php');
	include('common/classes/suppliers.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('supplier-stock-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes        = new ChartOfAccounts();
	$objScanPurchase        = new ScanPurchase();
	$objScanPurchaseDetails = new ScanPurchaseDetails();
	$objItems               = new Items();
	$objItemCategory        = new itemCategory();
	$objSuppliers		    		= new suppliers();
	$objConfigs 		    		= new Configs();

	$suppliersList   = $objSuppliers->getList();
	$item_categories = $objItemCategory->getList();
	$cashAccounts    = $objAccountCodes->getAccountByCatAccCode('010101');

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	$titleRepo = '';

	if(isset($_POST['search'])){
		$objScanPurchase->item_id     	= $_POST['item'];
		$objScanPurchase->supp_acc_code = $_POST['supplier'];
		$objScanPurchase->state 		= 'A';

		$current_user  = $user_id;

		if($current_user==1){
			$objScanPurchase->user_id = '';
		}else{
			$objScanPurchase->user_id = $current_user;
		}
		$purchaseReport = $objScanPurchase->supplierStockReport();
	}

	$less = false;

	$users_list = $objAccounts->getActiveList();
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/lightbox.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 884;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){

					if(PageNo == 1){
						MaxHeight = 750;
					}

					if(PageNo != 1){
						MaxHeight = 820;
					}

					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){

					if(i == 1){
						MaxHeight = 750;
					}

					if(i != 1){
						MaxHeight = 820;
					}

					$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Sitsbook.com",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('.itemSelector').selectpicker();
			$('.supplierSelector').selectpicker();
			$("select[name=state]").selectpicker();
			$('#repoType').selectpicker();
			$("select.user_id").selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Supplier Stock Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="">

                        <div class="caption">Account</div>
                        <div class="field" style="width:300px;position:relative;">
                                <select class="supplierSelector form-control "
                                		name="supplier"
                                        data-style="btn-default"
                                        data-live-search="true" style="border:none" >
                                        <option selected value=""></option>
<?php
                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Item </div>
                        <div class="field" style="width:300px;">
                        <select class="itemSelector show-tick form-control"
                        	name="item"
                            data-style="btn-default"
                            data-live-search="true" style="border:none">
        	               <option selected value=""></option>
<?php
					if(mysql_num_rows($item_categories)){
					    while($category = mysql_fetch_array($item_categories)){
							$itemList = $objItems->getActiveListCatagorically($category['ITEM_CATG_ID']);
							$category_name = $objItemCategory->getTitle($category['ITEM_CATG_ID']);
?>
							<optgroup label="<?php echo $category_name; ?>">
<?php
						if(mysql_num_rows($itemList)){
							while($theItem = mysql_fetch_array($itemList)){
								if($theItem['ACTIVE'] == 'N'){
									continue;
								}
?>
	       						<option value="<?php echo $theItem['ID']; ?>" data-subtext="<?php echo $theItem['COMPANY']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
							}
						}
?>
							</optgroup>
<?php
    				}
}
?>
                    		</select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />

<?php
					if(isset($purchaseReport)){
?>
                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Supplier Stock Report</p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						$prevBillNo = '';
						if(mysql_num_rows($purchaseReport)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Account</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Item</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Stock</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Cost Price</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Amount(<?php echo $currency_type; ?>)</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_amount = 0;
								$prev_bill_no = 0;
								while($row = mysql_fetch_array($purchaseReport)){
									$supplierTitle = $objAccountCodes->getAccountTitleByCode($row['SUPP_ACC_CODE']);
									$item = $objItems->getRecordDetails($row['ITEM_ID']);
									$u_full_name = $objAccounts->getFullName($row['USER_ID']);
?>
	                                <tr id="recordPanel" class="alt-row">
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo substr($supplierTitle, 0,12); ?></td>
	                                    <td style="text-align:left;font-size:10px !important;">
											<?php if(is_file("uploads/products/".$item['IMAGE'])){ ?>
											<a href="uploads/products/<?php echo $item['IMAGE']; ?>" data-lightbox="roadtrip">
												<?php echo $item['NAME']; ?><?php echo ($row['COLOUR']=='')?"":"-".$row['COLOUR']; ?>
											</a>
											<?php }else{ ?>
												<?php echo $item['NAME']; ?><?php echo ($row['COLOUR']=='')?"":"-".$row['COLOUR']; ?>
											<?php } ?>
										</td>
										<td style="text-align:center;font-size:10px !important;"><?php echo $row['ITEM_STOCK']; ?></td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php  echo number_format($row['PURCHASE_PRICE'],2); ?></td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php  echo number_format($row['PRICE']*$row['ITEM_STOCK'],2); ?></td>
	                                </tr>
<?php
									$total_amount += $row['PRICE'];
									$prev_bill_no = $row['BILL_NO'];
								}
?>
                            </tbody>
<?php
						}//end if
						if(mysql_num_rows($purchaseReport)){
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;" colspan="4">Total:</td>
                                <td style="text-align:center;"> <?php echo number_format($total_amount,2); ?> </td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
