<?php
	include("common/connection.php");
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/branches.php');

	$objUsers    = new UserAccounts();
	$objAccounts = new ChartOfAccounts();
	$objBranches = new Branches();
	$objConfigs  = new Configs();

	$branch_users = $objConfigs->get_config('BRANCH_USERS');

	if(isset($_POST['save'])){
		$user_id_form 		 = mysql_real_escape_string($_POST['user_id']);
		$objUsers->userName  = mysql_real_escape_string($_POST['userName']);

		if($user_id_form == 0){
			$password			 = $_POST['password'];
			$rePassword			 = $_POST['rePassword'];
		}else{
			$password			 = '';
			$rePassword			 = '';
		}

		$objUsers->firstName 		 = mysql_real_escape_string($_POST['firstName']);
		$objUsers->lastName  		 = mysql_real_escape_string($_POST['lastName']);
		$objUsers->designation  	 = isset($_POST['designation'])?mysql_real_escape_string($_POST['designation']):"";
		$objUsers->designation_type  = isset($_POST['designation_type'])?mysql_real_escape_string($_POST['designation_type']):"";
		$objUsers->contactNo 		 = mysql_real_escape_string($_POST['contact']);
		$objUsers->address   		 = mysql_real_escape_string($_POST['address']);

		$objUsers->branch_id   		 = isset($_POST['branch_id'])?(int)$_POST['branch_id']:"";

		$objUsers->cashinhand 			= ($user_id_form == 1)?"Y":mysql_real_escape_string($_POST['cash_status']);
		$objUsers->allow_other_cash 	= ($user_id_form == 1)?"Y":mysql_real_escape_string($_POST['allow_other_cash']);

		if(strlen($password) >= 5 || $user_id_form > 0 ){
			if(( $password == $rePassword && $password != '') || $user_id_form > 0 ){
				if(!$objUsers->userNameExists($objUsers->userName) || $user_id_form > 0 ){
					if($user_id_form == 0){
						$objUsers->salt     = $objUsers->buySalt();
						$objUsers->password = hash('sha256',$password.$objUsers->salt);
					}
					$objUsers->active   = 'Y';
					$objUsers->joinDate = date('Y-m-d');
					if($user_id_form == 0){
						$user_acc_title 		 = mysql_real_escape_string("Cash In Hand A/c ".$objUsers->firstName." ".$objUsers->lastName);
						$account_code_id 		 = $objAccounts->addSubMainChild($user_acc_title,'010101');
						$cash_in_hand    		 = mysql_fetch_array($objAccounts->getAccountCodeById($account_code_id));
						$objUsers->cash_acc_code = $cash_in_hand['ACC_CODE'];
						$user_id_form 			 = $objUsers->save();
					}elseif($user_id_form > 0){
						if(!$objUsers->userNameExistsUpdate($objUsers->userName,$user_id_form)){
							$objUsers->update($user_id_form);
						}else{
							$user_id_form = 0;
						}
					}
					if($user_id_form > 0){
						echo "<script>window.location.href = 'users-management.php?id=".$user_id_form."';</script>";
						exit();
					}else{
						$message = "Error!";
					}
				}else{
					$message = "User Name Already In Exists!";
				}
			}else{
				$message = "Giver Passwords Do Not Match!";
			}
		}else{
			$message = "Password Must Contain At Least 5 Characters!";
		}
	}
	$userDetails = NULL;
	if(isset($_GET['id'])){
		$user_id = mysql_real_escape_string($_GET['id']);
		$userDetails = $objUsers->getDetails($user_id);
	}
	$userList   = $objUsers->getFullList();
	$branchList = $objBranches->getList();
?>
<!DOCTYPE html>
<html>
   <head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>User Management</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<style>
			 table th, table td{
			 	font-size: 14px;
			 	text-align:center;
			 }
			 input.error{
			 	border-color: red !important;
			 }
		</style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	    <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
        <script type="text/javascript">
			$(document).ready(function(){
				<?php
					if(isset($_GET['tab'])&&$_GET['tab'] == 'list'){
				?>
					$("#tab2").click();
				<?php
					}
				?>
				$("td.desination_type_td").each(function(){
					var text_des = $("select[name='designation_type'] option[value='"+$(this).text()+"']").text();
					$(this).text(text_des);
				});
				$("#tab2").click(function(){
					$(".the-form input[type=text]").val('');
					$(".the-form textarea").val('');
				});
                $("select").selectpicker();
                $(".field").css({"width":"450px"});
                $("input[name='contact']").numericOnly();
                $(".the-form").validate({
					rules:{
						userName: "required",
						password: { required:true,
									minlength:5},
						rePassword: { required:true,
									  equalTo:"input[name='password']",
									  minlength:5},
						firstName: "required",
						lastName: "required",
						email: "email"
				    },messages:{
				    	userName: "Please Enter a User Name!",
				    	password: {minlength:"At Least Five Charactors!",required:"Required!"},
				    	rePassword: {minlength:"At Least Five Charactors!",
				    				 required:"Required!",
				    				 equalTo:"Passwords do not Match!"},
				    	firstName: "First Name Is Required!",
						lastName: "Last Name Is Required!",
						email: {email:"The Email is Not Valid!"}
				    }
				});
            });
        </script>
   </head>

   	<body>
   		<div id = "body-wrapper">
      		<div id="sidebar">
				<?php include("common/left_menu.php") ?>
            </div> <!-- End #sidebar -->
      		<div id = "bodyWrapper">
        		<div class = "content-box-top">
            		<div class = "summery_body">
               			<div class = "content-box-header">
                  			<p>Users Management</p>
                            <span id="tabPanel">
                                <div class="tabPanel">
                                    <div class="tab" id="tab2" onclick="tab('2','1','2');">List</div>
                                    <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">Details</div>
                                </div>
                            </span>
                        	<div style = "clear:both;"></div>
               			</div><!-- End .content-box-header -->

                        <div id = "bodyTab1" style="display:block">
                        <form method="post" action="" class="the-form form-horizontal">
                            <div id = "form">
															<div class="form-group">
														    <label class="control-label col-sm-2">User Name :</label>
														    <div class="col-sm-10">
																	<input type="text" name="userName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['USER_NAME']:""; ?>" />
														    </div>
														  </div>
<?php
								if(!isset($userDetails)){
?>
															<div class="form-group">
																<label class="control-label col-sm-2">Password :</label>
																<div class="col-sm-10">
																	<input type="password" name="password" class="form-control" value=""  />
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Confirm Password :</label>
																<div class="col-sm-10">
																	<input type="password" name="rePassword" class="form-control" value="" />
																</div>
															</div>
<?php
								}
?>
															<hr />

															<div class="form-group">
																<label class="control-label col-sm-2">First Name :</label>
																<div class="col-sm-10">
																	<input type="text" name="firstName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['FIRST_NAME']:""; ?>" />
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Last Name :</label>
																<div class="col-sm-10">
																	<input type="text" name="lastName" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['LAST_NAME']:""; ?>" />
																</div>
															</div>
                                <?php
									if($branch_users == 'Y'){
								?>
															<div class="form-group">
																<label class="control-label col-sm-2">Branch :</label>
																<div class="col-sm-10">
																	<select name="branch_id" class="form-control show-tick">
																			<option value=""></option>
																			<?php
																					if(mysql_num_rows($branchList)){
																							while($brnch = mysql_fetch_assoc($branchList)){
																									if($brnch['BRANCH_TYPE'] == 'G'){
																											continue;
																									}
																									$selected = ((isset($userDetails))&&$userDetails['BRANCH_ID']==$brnch['ID'])?"selected":"";
																									?><option value="<?php echo $brnch['ID'] ?>" <?php echo $selected; ?> ><?php echo $brnch['TITLE'] ?></option><?php
																							}
																					}
																			?>
																	</select>
																</div>
															</div>
								<?php } ?>

							<?php if($foodPointMgmtAddon == 'Y'){ ?>
															<div class="form-group">
																<label class="control-label col-sm-2">Designation :</label>
																<div class="col-sm-10">
																	<select class="form-control show-tick" name="designation_type">
																		<option value="" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='')?"selected":""; ?> ></option>
																		<option value="M" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='M')?"selected":""; ?> >Order Manager</option>
																		<option value="O" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='O')?"selected":""; ?> >Order Taker (Waiter)</option>
																		<option value="K" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='K')?"selected":""; ?> >Kitchen</option>
																	</select>
																</div>
															</div>
							<?php }elseif($distSalesAddon == 'Y'){ ?>
															<div class="form-group">
																<label class="control-label col-sm-2">Designation :</label>
																<div class="col-sm-10">
																	<select class="form-control show-tick" name="designation_type">
																		<option value="" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='')?"selected":""; ?> ></option>
																		<option value="S" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='S')?"selected":""; ?> >Salesman</option>
																		<option value="O" <?php echo (isset($userDetails)&&$userDetails['DESIGNATION_TYPE']=='O')?"selected":""; ?> >OrderTaker</option>
																	</select>
																</div>
															</div>
								<?php }else{ ?>
															<div class="form-group">
																<label class="control-label col-sm-2">Designation :</label>
																<div class="col-sm-10">
																	<input type="text" name="designation" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['DESIGNATION']:""; ?>" />
																</div>
															</div>
								<?php } ?>
															<div class="form-group">
																<label class="control-label col-sm-2">Contact # :</label>
																<div class="col-sm-10">
																	<input type="text" name="contact" class="form-control" value="<?php echo (isset($userDetails))?$userDetails['CONTACT_NO']:""; ?>" />
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Address # :</label>
																<div class="col-sm-10">
																	<textarea name="address" style="height: 100px;" class="form-control"><?php echo (isset($userDetails))?$userDetails['ADDRESS']:""; ?></textarea>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Use Cash In Hand :</label>
																<div class="col-sm-10">
																	<input type="radio" id="css-label-radio-1" class="css-checkbox" name="cash_status" value="Y" <?php echo $userDetails['CASH_IN_HAND']=='Y'||$userDetails['CASH_IN_HAND']!='N'?"checked":""; ?> />
																	<label for="css-label-radio-1" class="css-label-radio">Yes</label>
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                	<input type="radio" id="css-label-radio-2" class="css-checkbox" name="cash_status" value="N" <?php echo $userDetails['CASH_IN_HAND']=='N'?"checked":""; ?>  />
																	<label for="css-label-radio-2" class="css-label-radio">No</label>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2">Allow Other Cash A/c :</label>
																<div class="col-sm-10">
																	<input type="radio" id="css-label-radio-2e" class="css-checkbox" name="allow_other_cash" value="Y" <?php echo ($userDetails['ALLOW_OTHER_CASH']=='Y'||$userDetails['ALLOW_OTHER_CASH']!='N')?"checked":""; ?> />
																	<label for="css-label-radio-2e" class="css-label-radio">Yes</label>
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                	<input type="radio" id="css-label-radio-2x" class="css-checkbox" name="allow_other_cash" value="N" <?php echo ($userDetails['ALLOW_OTHER_CASH']=='N')?"checked":""; ?> />
																	<label for="css-label-radio-2x" class="css-label-radio">No</label>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-sm-2"></label>
																<div class="col-sm-10">
																	<input type="hidden" name="user_id" value="<?php echo (isset($userDetails))?$userDetails['ID']:0; ?>" />
                                  <input type="submit" class="btn btn-primary pull-left" name="save" value="<?php echo (isset($userDetails))?"Update":"Save"; ?>" />
                                  <input type="button" class="btn btn-default pull-right" onclick="window.location.href = 'users-management.php';" value="New Form" />
																</div>
															</div>
                            </div><!--form-->
                            <div style = "clear:both;bodyWrapper"></div>
                        </form>
                        </div><!--bodyTab1-->

                        <div id="bodyTab2" style="display:none">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                    	<th width="5%">Sr.</th>
                                        <th width="20%">Full Name</th>
                                        <th width="20%">User Name</th>
																				<?php
																					if($distSalesAddon == 'Y'){
																				?>
																				<th width="10%">Designation</th>
																				<?php
																					}
																				?>
                                        <th width="10%">Cash In Hand</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
	$counts = 1;
	if ($userList != NULL && mysql_num_rows($userList)){
		while($theUser = mysql_fetch_array($userList)){
			if($theUser['ID'] == 1){
				$department = 'Administator';
			}else{
				$department = $theUser['USER_TYPE'];
			}
?>

                                    <tr>
                                    	<td><?php echo $counts; ?></td>
                                    	<td><?php echo $theUser['FIRST_NAME']." ".$theUser['LAST_NAME']; ?></td>
                                        <td><?php echo $theUser['USER_NAME']; ?></td>
																				<?php if($distSalesAddon == 'Y'){ ?>
																				<td class="desination_type_td"><?php echo $theUser['DESIGNATION_TYPE']; ?></td>
																				<?php } ?>
                                        <td><?php echo ($theUser['CASH_IN_HAND'] == 'Y')?"Yes":"No"; ?></td>
                                        <td>
                                        	<a id="view_button" onclick="window.location.href = 'users-management.php?id=<?php echo $theUser['ID']; ?>';"><i class="fa fa-pencil"></i></a>
                                            <a id="view_button" onclick="window.location.href = 'change-password.php?id=<?php echo $theUser['ID']; ?>';"><i class="fa fa-key"></i></a>
                                            <?php if($theUser['ID'] > 1){ ?>
                                            	<a id="view_button" onclick="window.location.href = 'permissions.php?usr=<?php echo $theUser['ID']; ?>';"><i class="fa fa-check-square-o"></i></a>
                                            <?php } ?>
                                            <a do="<?php echo $theUser['ID']; ?>" class="pointer delete_user"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
<?php
			$counts++;
		}
	}
?>
                                </tbody>
                            </table>
                            <div style = "clear:both;"></div>
                        </div><!--bodyTab2-->
            		</div><!--summery_body-->
	         	</div><!--End.content-box-top-->
      		</div><!--bodyWrapper-->
      	</div>  <!--body-wrapper-->
      	<div id="fade"></div>
      	<div id="xfade"></div>
   </body>
</html>
<?php include('conn.close.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".delete_user").click(function(){
			$(this).deleteRowDefault('db/del-user.php');
		});
<?php
	if(isset($message)){
?>
		displayMessage('<?php echo $message; ?>');
<?php
	}
?>
    });
	var centerThisDiv = function(elementSelector){
		var win_hi = $(window).height()/2;
		var win_width = $(window).width()/2;
		var elemHeight = $(elementSelector).height()/2;
		var elemWidth = $(elementSelector).width()/2;
		var posTop = win_hi-elemHeight;
		var posLeft = win_width-elemWidth;
		$(elementSelector).css({
			'position': 'fixed',
			'top': posTop,
			'left': posLeft,
			'margin': '0px'
		});
	}
	var displayMessage= function(message){
		$("#popUpDel").remove();
		$("body").append("<div id='popUpDel'><p class='confirm'>"+message+"</p><a class='nodelete button'>Close</a></div>");
		centerThisDiv("#popUpDel");
		$("#popUpDel").hide();
		$("#xfade").fadeIn('slow');
		$("#popUpDel").fadeIn();
		$(".nodelete").click(function(){
			$("#xfade").fadeOut();
			$("#popUpDel").fadeOut(function(){
				$(this).remove();
			});
		});
	}

</script>
