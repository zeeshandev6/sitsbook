<?php

		include ('common/connection.php');
		include ('common/config.php');
    include ('common/classes/accounts.php');
		include ('common/classes/banks.php');

    $objAccountCodes  = new ChartOfAccounts();
    $objBanks         = new Banks();

    if(isset($_POST['delete_id'])){
        $delete_id =  mysql_real_escape_string($_POST['delete_id']);
        $detail 	 = $objBanks->getDetail($delete_id);

        $in_use 	 = $objAccountCodes->accountCheckInBooks($detail['ACCOUNT_CODE']);
        $deleted 	 = false;

        if($in_use){
            echo "error! Cant Delete.";
            exit();
        }else{
            $deleted = $objBanks->delete($delete_id);
            $objAccountCodes->deleteAccCode($detail['ACCOUNT_CODE']);
        }
        echo $deleted;
        exit();
    }
    $banksList = $objBanks->getList();
?>
    <!DOCTYPE html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
				<style media="screen">
					.list_tab_rows td,.list_tab_rows th{
						font-size: 16px !important;
						padding: 10px !important;
					}
				</style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
				<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
              $("a.pointer").click(function(){
                  var this_row = $(this).parent().parent();
                  $("#confirmation").modal('show');
                  var delete_id = $(this).attr('value');
                  $("#confirmation #delete").click(function(){
                      $.post('banks-management.php',{delete_id:delete_id},function(data){
                          if(data == 1){
                              $(this_row).remove();
                          }
                      });
                  });
              });
              $('input[name=fromdate], input[name=todate]').datepicker({
                  dateFormat:'dd-mm-yy',
                  showAnim: 'show',
                  changeMonth: true,
                  changeYear: true,
                  yearRange: '2000:+10'
              });
            });
        </script>
    </head>
    <body>
	    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	    <div id="bodyWrapper">
	        <div class = "content-box-top" style="overflow:visible;">
	            <div class = "summery_body">
	                <div class = "content-box-header">
	                    <p ><B>Banks Management</B> </p>
	                        <span id="tabPanel">
	                            <div class="tabPanel">
	                                <div class="tabSelected" id="tab1">List</div>
	                                <a href="add-bank-detail.php"><div class="tab">New</div></a>
	                            </div>
	                        </span>
	                    <div style = "clear:both;"></div>
	                </div><!-- End .content-box-header -->
	                <div style = "clear:both; height:20px"></div>
	                <div id = "bodyTab1">
	                    <table style="width:100%" class="table-hover"  >
	                        <thead>
	                        <tr>
	                            <th width="5%" style="text-align:center">Sr No.</th>
	                            <th width="10%" style="text-align:center"> Account Code </th>
	                            <th width="25%" style="text-align:center"> Bank Title </th>
	                            <th width="10%" style="text-align:center"> Account Type </th>
	                            <th width="25%" style="text-align:center">Description</th>
	                            <th width="10%" style="text-align:center">Action</th>
	                        </tr>
	                        </thead>
	                        <tbody>
	                        <?php
	                        if(mysql_num_rows($banksList)){
	                            $counter = 1;
	                            while($bankDetails = mysql_fetch_assoc($banksList)){
	                                ?>
	                                <tr class="list_tab_rows">
	                                    <td class="text-center"><?php echo $counter; ?></td>
	                                    <td class="department-list text-center"><?php echo $bankDetails['ACCOUNT_CODE']; ?></td>
	                                    <td class="department-list text-left"><?php echo $bankDetails['ACCOUNT_TITLE']; ?></td>
	                                    <td class="department-list text-center"><?php echo ($bankDetails['BANK_TYPE'] == 'M')?"Merchant":"Non Merchant"; ?></td>
	                                    <td class="department-list text-left"><?php echo $bankDetails['DESCRIPTION']; ?></td>
	                                    <td style="text-align:center">
	                                        <a id="view_button" href="add-bank-detail.php?id=<?php echo $bankDetails['ID']; ?>">View</a>
	                                        <a class="pointer"  onclick="delete(this)" value="<?php echo $bankDetails['ID']; ?>"><i class="fa fa-times"></i></a>
	                                    </td>
	                                </tr>
	                            <?php
	                                $counter++;
	                            }
	                        }
	                        else{
	                            ?>
	                            <tr class="list_tab_rows">
	                                <td style="text-align:center" colspan="6"> No Record Found!</td>
	                            </tr>
	                        <?php
	                        }
	                        ?>
	                        </tbody>
	                    </table>
	                </div> <!--bodyTab1-->
	                <div class="clear" style="height:30px"></div>
	                <div id="confirmation" class="modal fade">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Confirmation</h4>
												</div>
												<div class="modal-body">
													<p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
													<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
												</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div><!-- /.modal -->
	            </div><!-- End summer -->
	        </div><!-- End .content-box-top-->
	    </div><!--bodyWrapper-->
    </body>
</html>
<?php include('conn.close.php'); ?>
