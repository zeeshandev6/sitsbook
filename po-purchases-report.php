<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/inventory.php');
	include('common/classes/inventoryDetails.php');
	include('common/classes/inventory-return-details.php');
    include('common/classes/demand_list.php');
	include('common/classes/demand_list_detail.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('purchase-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes     	   	 = new ChartOfAccounts();
	$objInventory        	   	 = new inventory();
	$objInventoryDetails 	   	 = new inventory_details();
	$objInventoryReturnDetails = new InventoryReturnDetails();
  $objDemandList         	   = new DemandList();
  $objDemandListDetail   	   = new DemandListDetail();
	$objItems              	   = new Items();
	$objItemCategory       	   = new itemCategory();
	$objTaxRates           	   = new TaxRates();
	$objDepartments        	   = new Departments();
	$objConfigs 		   	   		 = new Configs();

	$suppliersList   = $objInventory->getSuppliersList();

	$titleRepo = '';

	if(isset($_GET['search'])){
		$objDemandList->from_date 		    = ($_GET['fromDate']=='')?"":date('Y-m-d',strtotime($_GET['fromDate']));
		$objDemandList->to_date   		    = ($_GET['toDate']=='')?"":date('Y-m-d',strtotime($_GET['toDate']));

    $objDemandList->po_number           = mysql_real_escape_string($_GET['po_number']);
    $objDemandList->supplier_acc_code   = mysql_real_escape_string($_GET['supplier_code']);
    $objDemandList->item_id             = mysql_real_escape_string($_GET['item']);

		$demand_report                      = $objDemandList->demand_report();
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				if($("div.tablePage").length==0){
					var MaxHeight = 480;
					var RunningHeight = 0;
					var PageNo = 1;
					//Sum Table Rows (tr) height Count Number Of pages
					$('table.tableBreak>tbody>tr').each(function(){
						if (RunningHeight + $(this).height() > MaxHeight){
							RunningHeight = 0;
							PageNo += 1;
						}
						RunningHeight += $(this).height();
						//store page number in attribute of tr
						$(this).attr("data-page-no", PageNo);
					});
					//Store Table thead/tfoot html to a variable
					var tableHeader = $(".tHeader").html();
					var tableFooter = $(".tableFooter").html();
					var repoDate = $(".repoDate").text();
					//remove previous thead/tfoot
					$(".tHeader").remove();
					$(".tableFooter").remove();
					$(".repoDate").remove();
					//Append .tablePage Div containing Tables with data.
					for(i = 1; i <= PageNo; i++){
						$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
						//get trs by pagenumber stored in attribute
						var rows = $('table tr[data-page-no="' + i + '"]');
						$('#Table' + i).find("thead").append(tableHeader);
						$('#Table' + i).find("tbody").append(rows);
					}
					$(".newTable").last().append(tableFooter);
					$('table.tableBreak').remove();

          $("div.tablePage").each(function(i,e){
              $(this).prepend($(".pageHeader").first().clone());
          });
          $(".pageHeader").first().remove();
				}
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports-horizontal.css',
				  pageTitle: "Sit Solution",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			if($(".carton_th").length){
				var colspan = $(".tHeader .carton_th").prevAll().length;
			}else{
				var colspan = $(".tHeader th.qty_th").prevAll().length;
			}
			$(".total_tf").attr('colspan',colspan);
			$('select').selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Purchase Ordering Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="get" action="">

                        <div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">PO #</div>
                        <div class="field">
                            <input type="text" class="form-control po_number" name="po_number" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Supplier</div>
                        <div class="field">
                                <select class="supplierSelector form-control "
                                		name="supplier_code"
                                        data-style="btn btn-default"
                                        data-live-search="true" style="border:none" >
                                        <option selected value=""></option>
<?php
                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    $selected = (isset($inventory)&&$inventory['SUPP_ACC_CODE']==$account['SUPP_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['SUPP_ACC_CODE']; ?>" value="<?php echo $account['SUPP_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['SUPP_ACC_TITLE']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Item </div>
                        <div class="field">
                        <select class="itemSelector show-tick form-control"
                                data-style="btn btn-default"
                                data-live-search="true" style="border:none" name="item">
                           <option selected value=""></option>
<?php
                    $itemsCategoryList  = $objItemCategory->getList();
                    if(mysql_num_rows($itemsCategoryList)){
                        while($ItemCat = mysql_fetch_array($itemsCategoryList)){
							$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
								<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
							if(mysql_num_rows($itemList)){
								while($theItem = mysql_fetch_array($itemList)){
									if($theItem['ACTIVE'] == 'N'){
										continue;
									}
									if($theItem['INV_TYPE'] == 'B'){
										continue;
									}
?>
                           			<option value="<?php echo $theItem['ID']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
								}
							}
?>
								</optgroup>
<?php
                        }
                    }
?>
                        </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="btn btn-info"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />

<?php
					if(isset($demand_report)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Purchase Ordering Report </p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            	<?php echo ($objDemandList->from_date=="")?"":"From ".date('d-m-Y',strtotime($objDemandList->from_date)); ?>
                                <?php echo ($objDemandList->to_date=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objDemandList->to_date)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						$prevBillNo = '';
						if(mysql_num_rows($demand_report)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                   <th width="5%"  class="text-center" >PO.#</th>
                                   <th width="7%"  class="text-center" >P.O date</th>
                                   <th width="10%" class="text-center" >Supplier</th>
                                   <th width="10%" class="text-center" >Items</th>
                                   <th width="7%"  class="text-center" >Qty.Ordered</th>
                                   <th width="7%"  class="text-center" >Qty.Receipt</th>
								   <th width="7%"  class="text-center" >Qty.Returns</th>
                                   <th width="7%"  class="text-center" >Qty.Pending</th>
                                   <th width="7%"  class="text-center" >Rate</th>
                                   <th width="7%"  class="text-center" >Amount</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_orders     = 0;
                                $total_receipt    = 0;
								$total_return     = 0;
                                $total_pending    = 0;
                                $total_amount     = 0;
								while($demand_row = mysql_fetch_array($demand_report)){
                                    $qty_receipt  = $objInventoryDetails->getQtyAgainstPoItem($demand_row['PO_NUMBER'],$demand_row['ITEM_ID']);
									$qty_returnd  = $objInventoryReturnDetails->getQtyAgainstPoItem($demand_row['PO_NUMBER'],$demand_row['ITEM_ID']);
                                    $pending      = $demand_row['QUANTITY']   - $qty_receipt + $qty_returnd;
                                    $amount       = ($demand_row['UNIT_RATE'] * ($qty_receipt-$qty_returnd) );
?>
                                <tr>
                                    <td class="text-center"><?php echo $demand_row['PO_NUMBER']; ?></td>
                                    <td class="text-center"><?php echo date("d-m-Y",strtotime($demand_row['ENTRY_DATE'])); ?></td>
                                    <td class="text-left"><?php   echo $demand_row['SUPPLIER_ACC_TITLE'] ?></td>
                                    <td class="text-left"><?php   echo $demand_row['ITEM_TITLE'] ?></td>
                                    <td class="text-center"><?php echo $demand_row['QUANTITY'] ?></td>
                                    <td class="text-center"><?php echo $qty_receipt; ?></td>
									<td class="text-center"><?php echo $qty_returnd; ?></td>
                                    <td class="text-center"><?php echo $pending; ?></td>
                                    <td class="text-center"><?php echo $demand_row['UNIT_RATE']; ?></td>
                                    <td class="text-center"><?php echo $amount; ?></td>
                                </tr>
<?php
                                    $total_orders   += $demand_row['QUANTITY'];
                                    $total_receipt  += $qty_receipt;
									$total_return   += $qty_returnd;
                                    $total_pending  += $pending;
                                    $total_amount   += $amount;
								}
?>

                            </tbody>
<?php
						}//end if
						if(mysql_num_rows($demand_report)){
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td class="text-center" colspan="4">Total:</td>
                                <td class="text-center"> <?php echo (isset($total_orders))?$total_orders:"0"; ?> </td>
                                <td class="text-center"> <?php echo (isset($total_receipt))?$total_receipt:"0"; ?> </td>
								<td class="text-center"> <?php echo (isset($total_return))?$total_return:"0"; ?> </td>
                                <td class="text-center"> <?php echo (isset($total_pending))?$total_pending:"0"; ?> </td>
                                <td class="text-center"> - - - </td>
                                <td class="text-center"> <?php echo (isset($total_amount))?$total_amount:"0"; ?> </td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
