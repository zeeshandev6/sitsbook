<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/measure.php');
	include('common/classes/emb_products.php');
	include('common/classes/machines.php');
	include('common/classes/payrolls.php');
	include('common/classes/customers.php');
	include('common/classes/emb_inward.php');
	include('common/classes/accounts.php');
	include('common/classes/j-voucher.php');
	include('common/classes/sheds.php');

	//Permission
	if(!in_array('emb-lot-register',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objLotRegister 		= new EmbLotRegister();
	$objMeasures  			= new Measures();
	$objMachines 				= new Machines();
	$objPayrolls   			= new Payrolls();
	$objEmbProducts 		= new EmbProducts();
	$objCustomers  			= new customers();
	$objInward    			= new EmbroideryInward();
	$objJournalVoucher 	= new JournalVoucher();
	$objChartOfAccounts = new ChartOfAccounts();
	$objSheds    				= new Sheds();

	$customersList 			= $objCustomers->getList();
	$productList   			= $objEmbProducts->getList();
	$measureList   			= $objMeasures->getList();
	$machineList   			= $objMachines->getList();
	$shed_list     			= $objSheds->getList();

	if(isset($_GET['customer_code'])){
		$fetch_customer_lots = mysql_real_escape_string($_GET['customer_code']);
	}
	if(isset($_GET['lid'])){
		$lid 				 = (int)mysql_real_escape_string($_GET['lid']);
		$lot_details = $objLotRegister->getDetail($lid);
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<style media="screen">
		table.input_table{
			width: 1700px;
		}
		table.input_table th,table.input_table td{
			padding: 10px !important;
		}
		tr.quickSubmit td{
			padding: 1px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
  <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/emb.lot.register.config.js"></script>
  <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
	<script type="text/javascript">
		$(function(){
			$('select').selectpicker();
		});
		var open_window = function(url){
			var win = window.open(url, '_blank');
			if (win) {
				win.focus();
			} else {
				alert('Please allow popups for this website');
			}
		};
	</script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
		<?php include("common/left_menu.php") ?>
    	</div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Lot Register</p>
                <span id="tabPanel">
                    <div class="tabPanel">
	                    <a href="lot-register.php?tab=list"><div class="tab">List</div></a>
                        <a href="lot-register.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">
                            <div class="caption">Lot Issue Date</div>
                            <div class="field" style="width:150px">
                                <input type="text" name="lot_date"  class="form-control lot_date datepicker" style="width:145px" value="<?php echo (isset($lot_details))?$lot_details['LOT_DATE']:date('d-m-Y'); ?>" autofocus />
                            </div>
                            <div class="caption" style="width:140px">Customer Title</div>
                            <div class="field" style="width:300px;position:relative;">
                                <select class="custCodeSelector show-tick form-control" data-live-search="true" >
                                   <option selected value=""></option>
<?php
                            if(mysql_num_rows($customersList)){
                                while($account = mysql_fetch_array($customersList)){
									$selected = '';
									if(isset($lot_details)){
										$selected = ($lot_details['CUST_ACC_CODE'] == $account['CUST_ACC_CODE'])?"selected='selected'":"";
									}elseif(isset($fetch_customer_lots)){
										$selected = ($fetch_customer_lots == $account['CUST_ACC_CODE'])?"selected='selected'":"";
									}

?>
                                   <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
                            </div>
														<div class="caption" style="width:200px;">
															<div class="switchBox">
																<div class="switch">
																	<input id="cmn-toggle-1" class="css-checkbox claimCheck" type="checkbox">
																	<label for="cmn-toggle-1" class="css-label">Claim</label>
																</div>
															</div><!--switchBox-->
														</div>
														<div class="clear"></div>
            					</form>
											<div class="clear" style="height: 30px;"></div>

											<div class="col-xs-12" style="overflow-x: auto;min-height: 320px !important;height: auto !important;">
											<table class="input_table">
													<thead>
															<tr>
																 <th width="7%" style="font-size:11px; text-align:center">Product</th>
																 <th width="7%"  style="font-size:11px; text-align:center">Lot#</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Quality</th>
																 <th width="5%"  style="font-size:11px;text-align:center;">Measure</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Billing Type</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Stitches</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Production</th>
																 <th width="3%"  style="font-size:11px; text-align:center">Thaan Issued</th>
																 <th width="5%"  style="font-size:11px; text-align:center">@Emb</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Rate/1000 Stitches</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
																 <th width="8%" class="text-center middle_align">
																 	<span class="pull-left">Commander</span>
																 	<button type="button" class="btn btn-default btn-xs pull-right" onclick="open_window('payroll-details.php');"><i class="fa fa-plus"></i></button>
																 	<button type="button" class="btn btn-default btn-xs pull-right" onclick="reloadDropdown('select.stitchAccount');"><i class="fa fa-refresh"></i></button>
																 </th>
																 <th width="5%"  style="font-size:11px; text-align:center">Design#</th>
																 <th width="8%" class="text-center middle_align">
																 	<span class="pull-left">Machine</span>
																 	<button type="button" class="btn btn-default btn-xs pull-right" onclick="open_window('machine-details.php');"><i class="fa fa-plus"></i></button>
																 	<button type="button" class="btn btn-default btn-xs pull-right" onclick="reloadDropdown('select.machine_id');"><i class="fa fa-refresh"></i></button>
																 </th>
																 <th width="5%"  style="font-size:11px; text-align:center">Waste</th>
																 <th width="5%"  style="font-size:11px; text-align:center">Stock</th>
																 <th width="10%"  style="font-size:11px; text-align:center">Action</th>
															</tr>
													</thead>
													<tbody>
															<tr class="quickSubmit">
																	<td>
																			<input type="hidden"  value="" class="insert_prod_title" style="width:95%;min-height:20px;text-align:center;" />
																			<select class="product_id show-tick form-control" data-live-search="true" style="border:none">
																				 <option value="0"></option>
<?php
																	if(mysql_num_rows($productList)){
																			while($account = mysql_fetch_array($productList)){
?>
																				 <option value="<?php echo $account['ID']; ?>" data-subtext="<?php echo $account['PROD_CODE']; ?>" ><?php echo $account['TITLE']; ?></option>
<?php
																			}
																	}
?>
																			</select>
																	</td>
																 <td>
																		<input type="text" value="" class="lotNum form-control" />
																	</td>
																	<td>
																		<select class="qualityDropDown form-control show-tick">
																					<option value="0">select</option>
																			</select>
																	</td>
																	<td>
																		<select name="measure" class="measure form-control show-tick" >
<?php
							if(mysql_num_rows($measureList)){
								while($row = mysql_fetch_array($measureList)){
?>
									<option value="<?php  echo $row['ID']; ?>"><?php  echo $row['NAME']; ?></option>
<?php
								}
							}
?>
								</select>
																	</td>
																	<td>
																		<select name="billing_type" class="billing_type form-control show-tick" >
																			<option value="S">Stitches</option>
																			<option value="Y">Yards</option>
																			<option value="U">Suits</option>
																			<option value="L">Laces</option>
																		</select>
																	</td>
																	<td>
																				<input type="text"  value="" class="stitches form-control" />
																	</td>
																	<td>
																				<input type="text"  value="" class="total_laces form-control" />
																	</td>
																	<td>
																				<input type="text"  value="" class="length form-control" />
																	</td>
																	<td>
																		<input type="text" class="embRate form-control" />
																	</td>
																	<td>
																		<input type="text"  value="" class="form-control embAmount" readonly="readonly" />
																	</td>
																	<td>
																		<input type="text" class="stitchRate form-control	" />
																	</td>
																	<td>
																		<input type="text"  value="" class="form-control stitchAmount" readonly="readonly" />
																	</td>
																	<td>
																		<select name="stitchAccount" class="form-control show-tick stitchAccount">
																				<option value="0"></option>
<?php
							$stitchList = $objPayrolls->getList();
							if(mysql_num_rows($stitchList)){
								while($stitch = mysql_fetch_array($stitchList)){
?>
									<option value="<?php echo $stitch['CUST_ACC_CODE']; ?>"><?php echo $stitch['CUST_ACC_TITLE']; ?></option>
<?php
								}
							}
?>
																			</select>
																	</td>
																	<td>
																		<input type="text" class="designNum form-control" />
																	</td>
																	<td>
																			<select class="machine_id form-control" >
<?php
							if(mysql_num_rows($machineList)){
								while($machine_row = mysql_fetch_array($machineList)){
?>
																				<option value="<?php echo $machine_row['ID']; ?>"><?php echo $machine_row['MACHINE_NO']."-".$machine_row['NAME']; ?></option>
<?php
								}
							}
?>
								</select>
							</td>
																	<td>
																		<input type="text" class="gp_no form-control" />
																	</td>
																	<td>
																		<input type="text"  value=""  class="form-control thaanLengthOS" inwardOS="0" readonly="readonly" />
																	</td>
																	<td>
																		<input type="button" class="quick_insert btn btn-default btn-block" value="Enter" />
																	</td>
															</tr>
													</tbody>
													<tbody class="removee">
														<tr class="calculations" style="display:none;"></tr>
													</tbody>
											</table>
											<div class="clear mt-20"></div>
										</div>

											<div class="underTheTable">
												<a href="emb-lot-register.php" class="button pull-right">New Form</a>
											</div>
											<div class="clear"></div>
                    </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box-top-->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm3" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
            	<p>New Machine:</p>
                <p class="textBoxGo" style="border:none;">
                	<select class="shed_id_pop form-control">
<?php
				if($shed_list){
					while($shedRow = mysql_fetch_array($shed_list)){
?>
                    	<option value="<?php echo $shedRow['SHED_ID'] ?>" ><?php echo $shedRow['SHED_TITLE'] ?></option>
<?php
					}
				}
?>
                    </select>
                </p>
                <p class="textBoxGo">
                	<input type="text" value="" name="newMachineTitle" class="input_size newMachineTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm2" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
            	<p>New Stitch Account:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="newStitchTitle" class="input_size newStitchTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#datepicker").keyup(function(e){
			if(e.keyCode ==13){
				$("div.custCodeSelector button").focus();
			}
		});
		$(".lotNum").keydown(function(e){
			if(e.keyCode==13){
				$(this).getProductQualities();
			}
		});
		$("div.measure button").keydown(function(e){
			if(e.keyCode==13){
				$(".length").focus();
			}
		});
		$(".length").keyup(function(e){
			if(e.keyCode==13&&$(this).val()!==""){
				$(".embRate").focus();
			}
			var claimOrIssue = 'P';
			if($(".claimCheck").is(":checked")){
				claimOrIssue = 'R';
			}
			if(claimOrIssue == 'P' || (claimOrIssue == 'R' && $(".length").val() < 0)){
				$(".thaanLengthOS").val($(".thaanLengthOS").attr("inwardOS")-$(".length").val());
			}
			if($(".thaanLengthOS").val()<0){
				$(".length").blur();
				$(".length").val('');
				$(".thaanLengthOS").val($(".thaanLengthOS").attr("inwardOS")-$(".length").val());
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'> You Can Not Issue Quantity exceeding Total Quantity!</p><a class='nodelete btn btn-info'>Ok</a></div>");
				$("#popUpDel").hide();
				$("#xfade").fadeIn();
				$("#popUpDel").fadeIn();
				$("#popUpDel").centerThisDiv();
				$(".nodelete").click(function(){
					$("#popUpDel").slideUp(function(){
						$("#popUpDel").remove();
					});
					$("#xfade").fadeOut();
					$(".getItemTitle").focus();
				});
			}
		});
		$(".embRate").keydown(function(e){
			if(e.keyCode==13){
				$(".stitchRate").focus();
			}
		});
		$(".stitchRate").keydown(function(e){
			if(e.keyCode==13){
				$("div.stitchAccount button").focus();
			}
		});
		$("div.stitchAccount button").keydown(function(e){
			if(e.keyCode==13){
				$("input.designNum").focus();
			}
		});
		$("input.designNum").keydown(function(e){
			if(e.keyCode==13){
				$("div.machine_id button").focus();
			}
		});
		$("div.machine_id button").keydown(function(e){
			if(e.keyCode==13){
				$(".gp_no").focus();
			}
		});
		$(".gp_no").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				$(".quick_insert").focus();
			}
		});
		$("a.pointer").click(function(){
			$(this).deleteRow();
		});
		$(".editDetailsFrom").click(function(){
			$(this).editLotDetails();
		});
		$(".quick_insert").click(function(){
			$(this).quickSubmit();
		});
		$(".quick_insert").keydown(function(e){
			if(e.keyCode==13){
				$(this).quickSubmit();
			}
		});
		$("div.qualityDropDown button").keydown(function(e){
			if(e.keyCode == 13){
				$("div.measure button").focus();
			}
		});
		$("div.measure button").keydown(function(e){
			if(e.keyCode == 13){
				$("div.billing_type button").focus();
			}
		});
		$("div.billing_type button").keydown(function(e){
			if(e.keyCode == 13){
				$("input.stitches").focus();
			}
		});
		$("input.stitches").keydown(function(e){
			if(e.keyCode == 13){
				$("input.total_laces").focus();
			}
		});
		$("input.total_laces").keydown(function(e){
			if(e.keyCode == 13){
				$("input.length").focus();
			}
		});
		$("input.length").keydown(function(e){
			if(e.keyCode == 13){
				$("input.embRate").focus();
			}
		});
		$("select.billing_type,input.length,input.stitchRate,input.stitches,input.embRate").change(function(){
			embroideryFormulas();
		});
		$("#datepicker").setFocusTo(".getCustomerTitle");
		$(".getCustomerTitle").keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()==""){
					e.preventDefault();
				}
			}
		});
		$("div.custCodeSelector button").keydown(function(e){
			if(e.keyCode == 13){
				$("div.product_id button").focus();
			}
		});
		$("div.product_id button").keydown(function(e){
			if(e.keyCode == 13){
				$(".lotNum").focus();
			}
		});
		$("select.custCodeSelector").change(function(){
			getProcessingLotsByCustomer();
		});
<?php
		if(isset($fetch_customer_lots)){
?>
			getProcessingLotsByCustomer();
			$(".insertCustomerCode").val('<?php echo $fetch_customer_lots; ?>');
<?php
		}
?>
		$(".lengthColumn").sumColumn(".sumLenTotal");
		$(".embAmountColumn").sumColumn(".embAmountTotal");
		$(".stichAmountColumn").sumColumn(".stichAmountTotal");
		$("div.journalizeThis").click(function(){
			$(this).journalizeThis("input[name='outwardDate']","input[name='custAccCode']","input[name='outwd_id']");
		});
		$(".reverseThisVoucher").click(function(){
			$(this).reverseVoucher("input[name='outwd_id']","input[name='custAccCode']","input[name='outwardDate']");
		});
    });
</script>
