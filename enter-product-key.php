<?php
	$out_buffer = ob_start();
	include 'common/connection.php';
	include 'common/classes/settings.php';

	$objConfigs = new Configs();

	$get = $objConfigs->get_config('SITS');
	if((strpos($_SERVER['SERVER_SOFTWARE'],'Linux') !== false) || strpos($_SERVER['SERVER_SOFTWARE'],'Unix') !== false){
		$command = 'cat /sys/class/dmi/id/product_uuid';
		$system_uuid = shell_exec($command);
		if($system_uuid == ''){
			linkBootstrap();
			errorMessage('Error! Cannot access product profile (cat).');
			exit();
		}
		$system_uuid = preg_replace('/\s+/', "", $system_uuid);
		$system_uuid = 'six'.$system_uuid.'sol';
		$system_uuid = hash('sha256',$system_uuid);
	}
	if((strpos($_SERVER['SERVER_SOFTWARE'],'Win32') !== false) || strpos($_SERVER['SERVER_SOFTWARE'],'Win64') !== false){
		$uuid = shell_exec("wmic csproduct get UUID");
		$system_uuid = str_replace("UUID", "", $uuid);
		$system_uuid = preg_replace('/\s+/', "", $system_uuid);
		$system_uuid = 'six'.$system_uuid.'sol';
		$system_uuid = hash('sha256',$system_uuid);
	}

	if($get==$system_uuid){
		exit();
	}


  if(isset($_POST['pkey'])){
      $key = mysql_real_escape_string($_POST['pkey']);
      $set = $objConfigs->set_config($key,'SITS');
      if($set){
          header('location:login.php');
          exit();
      }
  }
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Product Key</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />

    <script src="resource/scripts/jquery.1.11.min.js" type="text/javascript"></script>
    <script src="resource/scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    	$(window).load(function(){
    		$("#hasbg").fadeIn();
    	});
    </script>
</head>

	<body id="hasbg" style="display: none;">
		<div id="login-wrapper" class="png_bg">
			<div id="login-content" class="content-wide">
            	<img id="logo" src="resource/images/medical.png" alt="The Traders Logo" />
                <div class="clear" style="height: 5px;"></div>
				<form action="" method="post">
					<div class="alert alert-info">
						<p> <i class="fa fa-exclamation-circle text-primary"></i> &nbsp; Your Product Key has expired</p>
					</div>
					<div class="caption" style="padding:5px;">Product Key :</div>
					<div class="field">
						<input class="form-control pull-left" value="" name="pkey" style="width:80%;border-top-right-radius:0px;border-bottom-right-radius:0px;" />
						<input class="btn btn-primary  pull-left" style="width:18%;border-top-left-radius:0px;border-bottom-left-radius:0px;" type="submit" name="LogIn" value="Enter" />
					</div>
					<div class="clear"></div>
					<div class="clear"></div>
				</form>
				<div class="messageSlide">
                	<img src="resource/images/error.png" />
					<span><?php echo $error; ?></span>
                    <div class="clear"></div>
                </div>
			</div> <!-- End #login-content -->
		</div> <!-- End #login-wrapper -->
		<div class="ft-info">
        	<span class="ft-ex">Designed &amp; Developed By <a href="http://www.sitsol.net" target="_blank" class="sits-link-a" style="font-weight:bold; text-decoration: none;color: inherit;">SIT Solutions</a> </span>
        	<i class="fa fa-mobile-phone"></i> <span class="ft-ex"> </span>
        	<i class="fa fa-phone"></i> <span class="ft-ex"> 041-8722200 </span>
        	<span class="ft-ex">[ <small>Version : SIT-SP 2.4</small> ]</span>
        </div>
  </body>
</html>
<?php include('conn.close.php'); ?>
