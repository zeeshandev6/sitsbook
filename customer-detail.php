<?php
	include( 'common/connection.php' );
	include( 'common/config.php' );
	include( 'common/classes/accounts.php' );
	include( 'common/classes/customers.php' );

	$objAccountCodes = new ChartOfAccounts();
	$objCustomers 	 = new Customers();

	if(isset($_POST['supp_name'])){
		$objCustomers->account_title = $_POST['supp_name'];
		if($objCustomers->account_title == ''){
			$account_code_id = 0;
		}else{
			$account_code_id = $objAccountCodes->addSubMainChild($objCustomers->account_title,'010104');
		}
		if($account_code_id){
			$account_code_details = $objAccountCodes->getDetail($account_code_id);
			$objCustomers->account_code = $account_code_details['ACC_CODE'];
			$inserted = $objCustomers->addCustomer();
			if($inserted){
				echo json_encode(array('OK'=>'Y','ACC_CODE'=>$objCustomers->account_code,'ACC_TITLE'=>$objCustomers->account_title));
			}else{
				echo json_encode(array('OK'=>'N'));
			}
		}
		exit();
	}

	//Permission
	if(!in_array('customer-management',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	if(isset($_POST['addCustomer'])){
		$objCustomers->account_title = mysql_real_escape_string(trim($_POST['title']));
		if($objCustomers->account_title == ''){
			$account_code_id = 0;
		}else{
			$account_code_id = $objAccountCodes->addSubMainChild($objCustomers->account_title,'010104');
		}
		if($account_code_id){
			$account_code_details 			= $objAccountCodes->getDetail($account_code_id);
			$objCustomers->account_code 	= $account_code_details['ACC_CODE'];
			$objCustomers->urdu_title  		= isset($_POST['urdu_title'])?mysql_real_escape_string($_POST['urdu_title']):"";
			$objCustomers->contact_person  	= isset($_POST['contact_person'])?mysql_real_escape_string($_POST['contact_person']):"";
			$objCustomers->email 			= isset($_POST['email'])?mysql_real_escape_string($_POST['email']):"";
			$objCustomers->mobile 			= isset($_POST['mobile'])?mysql_real_escape_string($_POST['mobile']):"";
			$objCustomers->channel 			= isset($_POST['channel'])?mysql_real_escape_string($_POST['channel']):"";
			$objCustomers->facebook_link 	= isset($_POST['facebook_link'])?mysql_real_escape_string($_POST['facebook_link']):"";
			$objCustomers->whatsapp_mobile 	= isset($_POST['whatsapp_mobile'])?"Y":"N";
			$objCustomers->send_sms 		= isset($_POST['send_sms'])?"Y":"N";
			$objCustomers->phones 			= (isset($_POST['phones']))?mysql_real_escape_string($_POST['phones']):"";
			$objCustomers->city 			= (isset($_POST['city']))?mysql_real_escape_string($_POST['city']):"";
			$objCustomers->address 			= (isset($_POST['address']))?mysql_real_escape_string($_POST['address']):"";
			$objCustomers->area 			= (isset($_POST['area']))?mysql_real_escape_string(trim($_POST['area'])):"";
			$objCustomers->sector 			= (isset($_POST['sector']))?mysql_real_escape_string(trim($_POST['sector'])):"";
			$objCustomers->tax_reg_no 		= (isset($_POST['tax_reg_no']))?mysql_real_escape_string($_POST['tax_reg_no']):"";
			$objCustomers->ntn_reg_no 		= (isset($_POST['ntn_reg_no']))?mysql_real_escape_string($_POST['ntn_reg_no']):"";
			$objCustomers->cnic_no 		 	= (isset($_POST['cnic_no']))?mysql_real_escape_string($_POST['cnic_no']):"";

			$inserted = $objCustomers->addCustomer();
		}else{
			$inserted = false;
		}

		if(isset($_POST['return_json'])){
			$done = $inserted?"Y":"N";
			echo json_encode(array("DONE"=>$done));
			exit();
		}

		if($inserted){
			$newId = mysql_insert_id();
			echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$newId."&added');</script>";
			exit();
		}else{
			$message = 'Error! Customer Detail Could Not Be Saved.';
		}
	}
	if(isset($_POST['update'])){
		$customerId = $_POST['customer_id'];
		$objCustomers->account_title = $_POST['title'];
		if($objCustomers->account_title == ''){
			$customerId = 0;
		}
		$objCustomers->account_code 		= mysql_real_escape_string($_POST['code']);
		$objCustomers->urdu_title 			= isset($_POST['urdu_title'])?mysql_real_escape_string($_POST['urdu_title']):"";
		$objCustomers->contact_person 	= mysql_real_escape_string($_POST['contact_person']);
		$objCustomers->email 						= mysql_real_escape_string($_POST['email']);
		$objCustomers->mobile 					= mysql_real_escape_string($_POST['mobile']);
		$objCustomers->channel 					= mysql_real_escape_string($_POST['channel']);
		$objCustomers->facebook_link 		= mysql_real_escape_string($_POST['facebook_link']);
		$objCustomers->whatsapp_mobile 	= isset($_POST['whatsapp_mobile'])?"Y":"N";
		$objCustomers->send_sms 				= isset($_POST['send_sms'])?"Y":"N";
		$objCustomers->phones 					= mysql_real_escape_string($_POST['phones']);
		$objCustomers->city 						= mysql_real_escape_string($_POST['city']);
		$objCustomers->address 					= mysql_real_escape_string($_POST['address']);
		$objCustomers->area 						= mysql_real_escape_string($_POST['area']);
		$objCustomers->sector 					= mysql_real_escape_string($_POST['sector']);
		$objCustomers->tax_reg_no 			= mysql_real_escape_string($_POST['tax_reg_no']);
		$objCustomers->ntn_reg_no 			= mysql_real_escape_string($_POST['ntn_reg_no']);
		$objCustomers->cnic_no 		 			= mysql_real_escape_string($_POST['cnic_no']);

		if($customerId && $objCustomers->account_title != ''){
			$customer_details = mysql_fetch_array($objCustomers->getRecordDetails($customerId));
			$objAccountCodes->updateTitleByCode($customer_details['CUST_ACC_CODE'],$objCustomers->account_title);
			$updated = $objCustomers->update($customerId);
			if($updated){
				echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$customerId."&updated');</script>";
				exit();
			}else{
				$message = 'Error! Customer Detail Could Not Be Saved.';
			}
		}else{
			$message = 'Error! Customer Detail Could Not Be Saved.';
		}
	}
	if(isset($_GET['cid'])){
		$did = $_GET['cid'];
		$customerDetail = $objCustomers->getRecordDetails($did);
		$customerRow = mysql_fetch_array($customerDetail);
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>SIT Solutions</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	input.error{
		border-color: red !important;
	}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='mobile']").numericOnly();
		$("input[name='phones']").numericOnly();
		$(".the-form").validate({
			rules:{
				title: "required",
				email: "email"
			},messages:{
				title: "Account Title Is required!",
				email: {email:"The Email is Not Valid!"}
			}
		});
		$(".insert_title").focus();
		<?php
		if(isset($message)){
			?>
			displayMessage('<?php echo $message; ?>');
			<?php
		}
		?>
	});
	</script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>

<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div id = "bodyWrapper">
		<div class = "content-box-top">
			<div class = "summery_body">
				<div class = "content-box-header">
					<p>Customers Management</p>
					<span id = "tabPanel">
						<div class = "tabPanel">
							<a href="customer-management.php?tab=list" ><div class="tab">List</div></a>
							<a href="customer-management.php?tab=search" ><div class="tab">Search</div></a>
							<div class = "tabSelected">Details</div>
						</div>
					</span>
					<div class="clear"></div>
				</div><!-- End .content-box-header -->
				<div class="clear"></div>
				<div id = "bodyTab1">
					<div class="col-xs-12 col-md-10 col-lg-10 mt-20">
						<form method="post" action="" class="the-form form-horizontal" >
<?php
							if(isset($customerRow)){
?>
								<div class="form-group">
									<label class="control-label col-sm-2"> Account Code :</label>
									<div class="col-sm-10">
										<input name="code" value="<?php echo (isset($customerRow))?$customerRow['CUST_ACC_CODE']:""; ?>" type="text" readonly="readonly" class="form-control" />
									</div>
								</div>
<?php
							}
?>
							<div class="form-group">
								<label class="control-label col-sm-2"> Account Title :</label>
								<div class="col-sm-10">
									<input name="title" value="<?php echo (isset($customerRow))?$customerRow['CUST_ACC_TITLE']:""; ?>" type="text" class="form-control" autofocus />
								</div>
							</div>
							<?php if($urdu_account_names == 'Y'){ ?>
							<div class="form-group">
								<label class="control-label col-sm-2"> Title in Urdu :</label>
								<div class="col-sm-10">
									<input name="urdu_title" dir="rtl" value="<?php echo (isset($customerRow))?$customerRow['URDU_TITLE']:""; ?>" type="text" class="form-control text-right" />
								</div>
							</div>
							<?php } ?>
							<hr />

							<div class="form-group">
								<label class="control-label col-sm-2"> Tax Reg. # </label>
								<div class="col-sm-10">
									<input name="tax_reg_no" value="<?php echo (isset($customerRow))?$customerRow['TAX_REG_NO']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"> CNIC # </label>
								<div class="col-sm-10">
									<input name="cnic_no" value="<?php echo (isset($customerRow))?$customerRow['CNIC_NO']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"> NTN # </label>
								<div class="col-sm-10">
									<input name="ntn_reg_no" value="<?php echo (isset($customerRow))?$customerRow['NTN_REG_NO']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<hr />

							<div class="form-group">
								<label class="control-label col-sm-2"> Contact Person</label>
								<div class="col-sm-10">
									<input name="contact_person" value="<?php echo (isset($customerRow))?$customerRow['CONT_PERSON']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"> Contact Channel</label>
								<div class="col-sm-10">
									<input name="channel" value="<?php echo (isset($customerRow))?$customerRow['CHANNEL']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Email</label>
								<div class="col-sm-10">
									<input name="email"  value="<?php echo (isset($customerRow))?$customerRow['EMAIL']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Facebook Link</label>
								<div class="col-sm-8">
									<textarea class="form-control" name="facebook_link" style="min-height:100px;"><?php echo (isset($customerRow))?$customerRow['FACEBOOK_LINK']:""; ?></textarea>
								</div>
								<div class="col-sm-2">
									<?php if(isset($customerRow)&&$customerRow['FACEBOOK_LINK']!=''){ ?>
										<a href="<?php echo $customerRow['FACEBOOK_LINK']; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i></a>
									<?php } ?>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Mobile</label>
								<div class="col-sm-10">
									<input name="mobile" value="<?php echo (isset($customerRow))?$customerRow['MOBILE']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"></label>
								<div class="col-sm-10">
									<input id="cmn-toggle-recovered" value="" class="css-checkbox"  type="checkbox" <?php echo (isset($customerRow)&&$customerRow['WHATSAPP_MOBILE'] == 'Y')?"checked":""; ?> name="whatsapp_mobile" />
									<label for="cmn-toggle-recovered" class="css-label">Whatsapp</label>

									<input id="cmn-toggle-sms" value="" class="css-checkbox"  type="checkbox" <?php echo (isset($customerRow)&&$customerRow['SEND_SMS'] == 'Y')?"checked":""; ?> name="send_sms" />
									<label for="cmn-toggle-sms" class="css-label">SMS</label>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Telephones</label>
								<div class="col-sm-10">
									<input name="phones" value="<?php echo (isset($customerRow))?$customerRow['PHONES']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Sector</label>
								<div class="col-sm-10">
									<input name="sector" value="<?php echo (isset($customerRow))?$customerRow['SECTOR']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Area</label>
								<div class="col-sm-10">
									<input name="area" value="<?php echo (isset($customerRow))?$customerRow['AREA']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Address</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="address" style="min-height:100px;"><?php echo (isset($customerRow))?$customerRow['ADDRESS']:""; ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">City/Country</label>
								<div class="col-sm-10">
									<input name="city" value="<?php echo (isset($customerRow))?$customerRow['CITY']:""; ?>" type="text" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"></label>
								<div class="col-sm-10">
									<?php
									if(isset($customerRow)){
										?>
										<input type="hidden" name="customer_id" value="<?php echo $customerRow['CUST_ID']; ?>"  />
										<input name="update" type="submit" value="Update" class="btn btn-primary" />
										<?php
									}else{
										?>
										<input name="addCustomer" type="submit" value="Save" class="btn btn-primary" />
										<input type="reset" value="Reset" class="btn btn-default" />
										<?php
									}
									?>
									<input type="button" value="Cancel" class="btn btn-default pull-right" onclick="window.location.replace('customer-management.php');" />
								</div>
							</div>
						</form>
					</div><!--form-->
					<div class="clear"></div>
				</div><!--End bodyTab1-->
			</div><!-- End summery_body -->
		</div><!-- End content-box -->
	</div><!--End body-wrapper-->
	<div id="xfade"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
