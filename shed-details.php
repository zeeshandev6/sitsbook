<?php
  $out_buffer = ob_start();
  include('common/connection.php');
  include('common/config.php');
  include('msgs.php');
  include('common/classes/accounts.php');
  include('common/classes/sheds.php');
  include('common/classes/machines.php');

  //Permission
  if(!in_array('shed-management',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objSheds               = new Sheds();
	$objMachines            = new Machines();
	$objChartOfAccounts     = new ChartOfAccounts();

    if(isset($_POST['save'])){
        $shed_id                 = (int)mysql_real_escape_string($_POST['shed_id']);
        $objSheds->account_title = mysql_real_escape_string($_POST['shed_title']);
        if($shed_id==0){
          $objChartOfAccounts->addSubMainChild($objSheds->account_title,'040107');
          $objSheds->shed_acc_code = $objChartOfAccounts->new_account_code;
        }else{
          $objSheds->shed_acc_code = mysql_real_escape_string($_POST['shed_code']);;
        }
        $objSheds->shed_title    = mysql_real_escape_string($_POST['shed_title']);
        $objSheds->location      = mysql_real_escape_string($_POST['location']);
        $objSheds->description   = mysql_real_escape_string($_POST['description']);
        $objSheds->email         = mysql_real_escape_string($_POST['email']);
        $objSheds->phones        = mysql_real_escape_string($_POST['phones']);

        if($shed_id == 0){
            $shed_id = $objSheds->save();
        }else{
            $objSheds->update($shed_id);
        }
        header('location:shed-details.php?id='.$shed_id);
        exit();
    }

    $shed_id      = 0;
    $shed_details = NULL;
	if(isset($_GET['id'])){
		$shed_id                 = (int)$_GET['id'];
		$shed_details            = mysql_fetch_array($objSheds->getRecordDetails($shed_id));
		$machinesInstalledInShed = $objMachines->countMachineByShed($shed_id);
	}

?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin Panel</title>
    <link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<style media="screen">
		table th{
		font-weight: bold !important;
		}
		table th,table td{
		padding: 10px !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p> <i class="fa fa-home"></i> Sheds Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="sheds-management.php?tab=list"><div class="tab">List</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                    <?php if($shed_details!=NULL){ ?>
                    <div class="form-group">
                      <label class="control-label col-sm-3"> Shed Account Code :</label>
                      <div class="col-sm-9">
                        <input type="text" name="shed_code" class="form-control " value="<?php echo (isset($shed_details))?$shed_details['SHED_ACC_CODE']:""; ?>" readonly="readonly" />
                      </div>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                      <label class="control-label col-sm-3"> Shed Account Title :</label>
                      <div class="col-sm-9">
                        <input type="text" name="shed_title" class="form-control" value="<?php echo (isset($shed_details))?$shed_details['SHED_TITLE']:""; ?>" required />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-3"> Shed Location :</label>
                      <div class="col-sm-9">
                        <input type="text" name="location" class="form-control" value="<?php echo (isset($shed_details))?$shed_details['LOCATION']:""; ?>" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-3"> Description :</label>
                      <div class="col-sm-9">
                        <textarea name="description" style="height:60px;" class="form-control"><?php echo (isset($shed_details))?$shed_details['DESCRIPTION']:""; ?></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-3"> Total Machines  :</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control " value="<?php echo (isset($shed_details))?$machinesInstalledInShed:"0"; ?>" readonly="readonly" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-3"> Contact No. :</label>
                      <div class="col-sm-9">
                        <input type="text" name="phones" class="form-control" value="<?php echo (isset($shed_details))?$shed_details['PHONES']:""; ?>" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-3"></label>
                      <div class="col-sm-9">
                        <input type="hidden" name="shed_id" value="<?php echo $shed_id; ?>" />
                        <input type="submit" value="<?php echo ($shed_details==NULL)?"Save":"Update"; ?>" name="save" class="btn btn-primary pull-left"/>
                        <a href="shed-details.php" class="btn btn-default pull-right">New Form</a>
                      </div>
                    </div>
                    </form>
                    </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
