<?php
	ob_start();
	include ('msgs.php');
	include ('common/connection.php');
	include ('common/config.php');
	include ('common/classes/items.php');
	include('common/classes/services.php');
	include ('common/classes/itemCategory.php');
	include ('common/classes/suppliers.php');
	include ('common/classes/ordering.php');
	include ('common/classes/rental_booking.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');

	$objServices       = new Services();
	$objItems 				 = new Items();
	$objItemCategory   = new itemCategory();
	$objJournalVoucher = new JournalVoucher();
	$objOrdering       = new Ordering();
  $objSupplier       = new Suppliers();
	$objRentalBooking  = new RentalBooking();
	$objChartOfAccounts= new ChartOfAccounts();

	$supplier_list 			 = $objSupplier->getList();
	$servicesList        = $objServices->getList();
	$itemsCategoryList   = $objItemCategory->getList();

	$booking_number = 0;
	$mode = 'form';

	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}

	if(isset($_POST['get_booking_title_by_no'])){
		$booking_no = mysql_real_escape_string($_POST['get_booking_title_by_no']);
		$detail     = $objRentalBooking->getDetailByNumber($booking_no);
		echo $detail['CLIENT_NAME'];
		exit();
	}

	$rbdid 								 = (isset($_GET['rbdid']))?$_GET['rbdid']:"0";
	$booking_detail_detail = $objRentalBooking->getDetailDetail($rbdid);
	$rbid 								 = (int)$booking_detail_detail['RENTAL_BOOKING_ID'];
	$rentalDetail 				 = $objRentalBooking->getDetail($rbid);
	$booking_number 			 = $rentalDetail['BOOKING_ORDER_NO'];

	$order_record = NULL;

	if(isset($_GET['id'])){
		$order_record   = $objOrdering->getDetail($_GET['id']);
		$booking_number = $order_record['ORDER_NO'];
		$booking_title  = $objRentalBooking->getRentalTitle($order_record['BOOKING_ID']);
	}

	if(isset($_POST['save'])){
		$order_id 								= mysql_real_escape_string($_POST['order_record']);
		$objOrdering->order_no 		= mysql_real_escape_string($_POST['order_no']);
		$objOrdering->booking_id 	= mysql_real_escape_string($_POST['booking_no']);
		$objOrdering->rbd_id 			= mysql_real_escape_string($_POST['rbd_id']);
		$objOrdering->user_id 		= $user_id;
		$objOrdering->title 			= '';
		$objOrdering->item_id 		= mysql_real_escape_string($_POST['item_id']);
		$objOrdering->item_type   = mysql_real_escape_string($_POST['item_type']);
		$objOrdering->quantity 	  = mysql_real_escape_string($_POST['quantity']);

		if($_POST['order_date_time'] == ""){
			$objOrdering->order_date = date('Y-m-d');
		}else{
			$objOrdering->order_date = date("Y-m-d",strtotime($_POST['order_date_time']));
		}
		$objOrdering->deliver_date = "0000-00-00";
		if($_POST['receiving_date_time'] == ""){
			$objOrdering->receiving_date = "0000-00-00";
		}else{
			$objOrdering->receiving_date = date("Y-m-d",strtotime($_POST['receiving_date_time']));
		}
		$objOrdering->supplier_id 		= mysql_real_escape_string($_POST['supplier']);
		$objOrdering->total_price 		= mysql_real_escape_string($_POST['total_price']);
		$objOrdering->advance 				= mysql_real_escape_string($_POST['advance']);
		$objOrdering->remaining 			= mysql_real_escape_string($_POST['remaining']);
		$objOrdering->design_comments = mysql_real_escape_string($_POST['design_comments']);

		if($order_id>0){
			//$objOrdering->comments 		 = mysql_real_escape_string($_POST['comments']);
			$objOrdering->update($order_id);
			$action = 'updated';
		}else{
			$objOrdering->order_status = 'N';
			$order_id 								 = $objOrdering->save();
			$action = 'saved';
		}

		if($order_id>0){
			$voucher_id 											   = $objOrdering->getVoucherId($order_id);

			$objJournalVoucher->jVoucherNum      = $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate      = date('Y-m-d',strtotime($objOrdering->order_date));
			$objJournalVoucher->voucherType      = 'SV';
			$objJournalVoucher->reference        = 'Ordering';
			$objJournalVoucher->reference_date   = date('Y-m-d',strtotime($objOrdering->order_date));
			$objJournalVoucher->status	  	     = 'P';

			if($voucher_id==0){
				$voucher_id = $objJournalVoucher->saveVoucher();
				$objOrdering->insertVoucherId($order_id,$voucher_id);
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			if($voucher_id>0){
				$objJournalVoucher->voucherId       = $voucher_id;

				$objJournalVoucher->accountCode     = $objOrdering->supplier_id;
				$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
				$objJournalVoucher->narration       = " Amount paid in advance against order # ".$objOrdering->order_no;
				$objJournalVoucher->transactionType = 'Dr';
				$objJournalVoucher->amount          = $objOrdering->advance;

				$objJournalVoucher->saveVoucherDetail();

				$objJournalVoucher->accountCode     = '0101010001';
				$objJournalVoucher->accountTitle    = $objChartOfAccounts->getAccountTitleByCode($objJournalVoucher->accountCode);
				$objJournalVoucher->narration       = " Amount paid in advance against order # ".$objOrdering->order_no;
				$objJournalVoucher->transactionType = 'Cr';
				$objJournalVoucher->amount          = $objOrdering->advance;

				$objJournalVoucher->saveVoucherDetail();
			}
		}

		if($order_id>0){
			echo "<script> window.location.href = 'ordering-details.php?id=".$order_id."&action=$action' </script>";
		}
		exit();
	}
	$current_order_no = $objOrdering->getLastOrderNo();
	$current_order_no = $current_order_no+1;
?>
<!DOCTYPE html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Admin Panel</title>
  <link rel="stylesheet" href="resource/css/reset.css"                         type="text/css" />
  <link rel="stylesheet" href="resource/css/style.css"                         type="text/css" />
  <link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css" />
  <link rel="stylesheet" href="resource/css/form.css"                          type="text/css" />
  <link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css" />
  <link rel="stylesheet" href="resource/css/reports.css"                       type="text/css" />
  <link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css" />
  <link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css" />
  <link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css" />
  <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.css"      type="text/css" />
	<style media="screen">
		table.prom td,table.prom th{
			padding: 7px 5px !important;
		}
	</style>
  <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
  <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
  <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
  <script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/rental.order.configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('input[name=delivery_date_time], input[name=order_date_time],input[name=receiving_date_time]').datetimepicker({
				format: "dd-mm-yyyy",
				weekStart: 1,
        todayBtn:  1,
				minView: 2,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
        showMeridian: 1
      });
			$("input[name=booking_no]").on('blur change',function(){
			 var booking_no =	$(this).val();
			 if(booking_no != ''){
				 $.post('ordering-details.php',{get_booking_title_by_no:booking_no},function(data){
					 $("input[name=booking_detail]").val(data);
					 $("input[name=title]").val(data);
				 });
			 }
			});
			$("input[name=total_price],input[name=advance]").on("change keyup",function(){
				var tp = $("input[name=total_price]").val();
				var a  = $("input[name=advance]").val();
				var r  = tp - a;
				$("input[name=remaining]").val((r).toFixed(2));
			});
			$("select").selectpicker();
			setTimeout(function(){
				$("div.supplier_id button").focus();
				$("div.supplier_id button").attr("tabindex",1);
				$("div.itemSelector button").attr("tabindex",2);
			},300);
			$("select[name=item_id]").change(function(){
				var item_type = $("select[name=item_id] option:selected").attr('data-type');
				$("input[name=item_type]").val(item_type);
			});
			$("input.addDetailRow").click(function(){
				$(this).quickSave();
			});
			$("select[name=odering_status]").change(function(){
				var status   = $(this).find("option:selected").val();
				var order_id = parseInt($(this).attr("data-row-id"))||0;
				$.post("ordering-management.php",{order_status_change:status,order_id:order_id},function(){});
			});
			$("select[name=item_id]").change();
    });
  </script>
</head>
<body>
  <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
        <div id="bodyWrapper">
          <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
              <div class = "content-box-header">
                <p style="font-size:15px;"><B>Ordering Details</B></p>
								<span id="tabPanel">
										<div class="tabPanel">
												<a href="ordering-management.php?tab=list"><div class="tab">List</div></a>
												<a href="ordering-management.php?tab=search"><div class="tab">Search</div></a>
												<a href="ordering-details.php"><div class="tabSelected">New</div></a>
										</div>
								</span>
								<div class="clear"></div>
              </div><!-- End .content-box-header -->
              <div class="clear"></div>
							<?php if($rbid > 0){ ?>
							<div class="content-box-header">
	                <span id="tabPanel" class="pull-left ml-10">
	                    <div class="tabPanel">
	                        <a href="rental-booking-details.php?id=<?php echo $rbid; ?>" class="tab"><i class="fa fa-book"></i> Booking</a>
													<a href="<?php echo ($rbid>0)?"ordering-details.php?rbid=".$rbid:"#"; ?>" class="tabSelected"><i class="fa fa-shopping-cart"></i> Ordering</a>
	                        <a href="<?php echo ($rbid>0)?"rental-booking-receipts.php?rbid=".$rbid:"#"; ?>" class="tab"><i class="fa fa-random"></i> Receipts</a>
													<a href="<?php echo ($rbid>0)?"rental-booking-ledger.php?rbid=".$rbid:"#"; ?>" class="tab"><i class="fa fa-file-o"></i> Ledger</a>
	                    </div>
	                </span>
	                <div class="clear"></div>
	            </div> <!-- End .content-box-header -->
	            <div class="clear"></div>

							<div class="content-box-header" style="background: none;;">
	                <span id="tabPanel" class="pull-left" style="margin-left: 70px;">
	                    <div class="tabPanel">
	                        <a href="ordering-details.php?mode=form&rbdid=<?php echo $rbdid; ?>" class="<?php echo ($mode == 'form')?"tabSelected":"tab"; ?> tab-small" id="entry-form" ><i class="fa fa-file-text text-primary"></i></a>
	                        <a href="ordering-details.php?mode=list&rbdid=<?php echo $rbdid; ?>" class="<?php echo ($mode == 'list')?"tabSelected":"tab"; ?> tab-small" id="receipts-list" ><i class="fa fa-list text-primary"></i></a>
	                    </div>
	                </span>
	                <div class="clear"></div>
	            </div> <!-- End .content-box-header -->
	            <div class="clear"></div>

							<?php } ?>
<?php
              //msgs
              if(isset($_GET['action'])){
                if($_GET['action']=='updated')  { successMessage("Order contract updated.");}
                elseif($_GET['action']=='added'){ successMessage("Order contract added.");}
                elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
              }
?>
<?php
						if($mode == 'list'){
							$rentalList = $objOrdering->getListByRental($rbid);
?>
							<div id = "bodyTab1">
								<div id="form">
									<table width="100%" cellspacing="0" >
										<thead>
											<tr>
												<th style="text-align:center;width:5%;">Sr#</th>
												<th style="text-align:center;width:5%;">Order#</th>
												<th style="text-align:center;width:5%;">Booking#</th>
												<th style="text-align:center;width:20%;" >Order/Delivery Time</th>
												<th style="text-align:center;width:30%;">Tilte</th>
												<th style="text-align:center;width:10%;" >Ordering Status</th>
												<th style="text-align:center;width:10%;">Action</th>
											</tr>
										</thead>
										<tbody>
									<?php
									$countx = 1;
									if(mysql_num_rows($rentalList)){
										while($rentalRow = mysql_fetch_array($rentalList)){
											?>
											<tr data-row-id="<?php echo $rentalRow['ID'] ?>" >
												<td style="text-align:center"><?php echo $countx; ?></td>
												<td style="text-align:center"><?php echo $rentalRow['ID']; ?></td>
												<td style="text-align:center"><?php echo $rentalRow['ORDER_NO']; ?></td>
												<td>
													Order Date: <?php if($rentalRow['ORDER_DATE'] != '0000-00-00 00:00:00'){ echo date('d-m-Y h:i A',strtotime($rentalRow['ORDER_DATE'])); }else{ echo ""; } ?><br/>
													Delivery Date: <?php if($rentalRow['DELIVERY_DATE'] != '0000-00-00 00:00:00'){ echo date('d-m-Y h:i A',strtotime($rentalRow['DELIVERY_DATE']));}else{ echo ""; } ?><br/>
													Receiving Date: <?php if($rentalRow['RECEIVING_DATE'] != '0000-00-00 00:00:00'){  echo date('d-m-Y h:i A',strtotime($rentalRow['RECEIVING_DATE'])); }else{ echo ""; } ?><br/>
												</td>
												<td><?php echo $rentalRow['TITLE']; ?></td>
												<td class="text-center">
													<select class="form-control show-tick" name="odering_status" data-row-id="<?php echo $rentalRow['ID'] ?>">
														<option value="N" <?php echo $rentalRow['ORDER_STATUS']=='N'?"selected":""; ?>>New</option>
														<option value="R" <?php echo $rentalRow['ORDER_STATUS']=='R'?"selected":""; ?>>Received</option>
													</select>
												</td>


												<td style="text-align:center">
													<a id="view_button" href="ordering-details.php?mode=form&rbid=<?php echo $rentalRow['BOOKING_ID']; ?>&id=<?php echo $rentalRow['ID']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
													<a id="view_button" target="_blank" href="<?php //echo $invoiceFile; ?>?id=<?php echo $rentalRow['ID']; ?>" title="Print"><i class="fa fa-print"></i></a>

													<?php if(in_array('delete-purchase',$permissionz) || $admin == true){ ?>
														<a do="<?php echo $rentalRow['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
														<?php } ?>
														<?php if(in_array('purchase-return',$permissionz) || $admin == true){ ?>
															<!--<a class="button-orange" href="inventory-return-details.php?pid=<?php echo $rentalRow['ID']; ?>" title="Return" ><i class="fa fa-reply"></i></a>-->
															<?php } ?>
														</td>
													</tr>
											<?php
												$countx++;
												}
											}elseif(isset($_GET['search'])){
												?>
												<tr>
													<th colspan="100" style="text-align:center;">
														<?php echo (isset($_GET['search']))?"0 Records Found!!":"0 Records!!" ?>
													</th>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
<?php
						}
?>
<?php
						if($mode == 'form'){
?>
              <div id = "bodyTab1">
                <div id="form">
                  <form method = "post" action = "" name="add_fabric" id="add_fabric" >
                    <!--<div class="title">Ordering Record :</div>-->

										  <div class="caption"> Order # :</div>
											<div class="form-group field" style="margin-bottom:0px;width:350px;">
												<input type="text" class="form-control" readonly="" name="order_no" id="pon" value="<?php echo (isset($order_record))?$order_record['ORDER_NO']:$current_order_no; ?>">
											</div>

											<div class="caption" style="width:190px;"> Order Date :</div>
                      <div class="field" style="width: 180px;">
												<input type="text" class="form-control datepicker_style" name="order_date_time" value="<?php echo ($order_record==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($order_record['ORDER_DATE'])); ?>" />
											</div>

											<div class="clear"></div>

											<div class="caption">Booking Id :</div>
											<div class="form-group field" style="margin-bottom:0px;width:350px;">
												<input type="hidden" name="rbd_id" value="<?php echo $booking_detail_detail['ID']; ?>" />
												<input type="text" class="form-control" name="booking_no" value="<?php echo (isset($order_record))?$order_record['BOOKING_ID']:$rentalDetail['BOOKING_ORDER_NO']; ?>" style="width: 100px; text-align:center;float:left" />
												<input type="text" class="form-control ml-10" readonly="" name="booking_detail" id="pon" value="<?php echo (isset($booking_title))?$booking_title:$rentalDetail['CLIENT_NAME']; ?>" style="width: 240px;float:left;" >
											</div>
											<div class="caption" style="width:190px;"> Return Date :</div>
                      <div class="field" style="width: 180px;">
                        <input type="text" class="form-control datepicker_style" name="receiving_date_time" value="<?php echo ($order_record==NULL)?date('d-m-Y'):date('d-m-Y',strtotime($order_record['RECEIVING_DATE'])); ?>" />
                      </div>

											<div class="clear"></div>

											<div class="caption"> Supplier :</div>
                      <div class="field" style="width: 350px;">
                        <select class="show-tick form-control supplier_id" name="supplier" data-live-search="true" data-width="265" >
                          <option value="" ></option>
<?php
                          if(mysql_num_rows($supplier_list)){
                            while($supplierDetails = mysql_fetch_assoc($supplier_list)){
?>
                              <option value="<?php echo $supplierDetails['SUPP_ACC_CODE']; ?>" <?php echo ($supplierDetails['SUPP_ACC_CODE']== (isset($order_record)?$order_record['SUPPLIER_ID']:""))?"selected":""; ?> ><?php echo $supplierDetails['SUPP_ACC_TITLE']; ?></option>
<?php
                            }
                          }
?>
                        </select>
                        <a onclick="reloadDropdown('select[name=supplier]');" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                        <a href="supplier-details.php?type=F" target="_blank" class="btn btn-default" ><i class="fa fa-plus"></i></a>
                      </div>
											<div class="clear"></div>

											<div class="caption"> Item :</div>
                      <div class="field" style="width: 350px;">
												<input type="hidden" name="item_type" value="I" />
												<select class="itemSelector show-tick form-control" name="item_id"
																data-style="btn-default"  data-width="265"
																data-live-search="true" style="border:none">
													 <option selected value=""></option>
<?php
										if(mysql_num_rows($itemsCategoryList)){
												while($ItemCat = mysql_fetch_array($itemsCategoryList)){
			$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
				<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
			if(mysql_num_rows($itemList)){
				while($theItem = mysql_fetch_array($itemList)){
					if($theItem['ACTIVE'] == 'N'){
						continue;
					}
					if($theItem['INV_TYPE'] == 'B'){
						continue;
					}
					$item_selected = ($booking_detail_detail['ITEM_ID']==$theItem['ID']||$order_record['ITEM_ID']==$theItem['ID'])?"selected":"";
?>
																			 <option data-type="I" value="<?php echo $theItem['ID']; ?>" <?php echo $item_selected; ?>><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
				}
			}
?>
				</optgroup>
<?php
												}
										}
?>
<?php
										if(mysql_num_rows($servicesList)){
												?><optgroup label="Services"><?php
												while($srv = mysql_fetch_assoc($servicesList)){
													$srv_selected = (($booking_detail_detail['SERVICE_ID']==$srv['ID'])||($order_record['ITEM_TYPE']=='S'&&$order_record['ITEM_ID']==$srv['ID']))?"selected":"";
?>
														<option data-type="S" value="<?php echo $srv['ID']; ?>" <?php echo $srv_selected; ?>><?php echo $srv['TITLE']; ?></option>
<?php
												}
												?></optgroup><?php
										}
?>
												</select>
												<a onclick="reloadDropdown('select[name=item_id]');" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                        <a href="item-details.php" target="_blank" class="btn btn-default" ><i class="fa fa-plus"></i></a>
                      </div>
											<div class="clear"></div>

											<div class="caption"> Quantity :</div>
                      <div class="field" style="width: 350px;">
                        <input type="text" class="form-control" name="quantity" tabindex="3" value="<?php echo ($order_record==NULL)?$booking_detail_detail['QUANTITY']:$order_record['QUANTITY']; ?>" />
                      </div>
											<div class="clear"></div>

											<div class="caption">Design Comments :</div>
                      <div class="field" style="width: 350px;">
                        <textarea style="width:750px;height:100px;" class="form-control" tabindex="4" name="design_comments"><?php echo (isset($order_record))?$order_record['DESIGN_COMMENTS']:""; ?></textarea>
                      </div>
                      <div class="clear"></div>

                      <div class="caption">Total Cost :</div>
                      <div class="field" style="width:129px">
                        <input class="form-control" name="total_price" tabindex="5" value="<?php echo (isset($order_record))?$order_record['TOTAL_PRICE']:""; ?>" />
                      </div>

                      <div class="caption">Advance :</div>
                      <div class="field" style="width:129px">
                        <input class="form-control" name="advance" tabindex="6" value="<?php echo (isset($order_record))?$order_record['ADVANCE']:""; ?>"  />
                      </div>

											<div class="caption">Remaining :</div>
                      <div class="field" style="width:129px">
                        <input class="form-control" name="remaining" readonly value="<?php echo (isset($order_record))?$order_record['REMAINING']:""; ?>" />
                      </div>
                      <div class="clear"></div>
											<?php
											/*
											if(isset($order_record)&& $order_record>0){


												?>
											<hr />
											<div class="caption">Odrer Comments :</div>
                      <div class="field" style="width: 350px;">
                        <textarea style="width:750px;height:100px;" class="form-control" name="comments"><?php echo (isset($order_record))?$order_record['COMMENTS']:""; ?></textarea>
                      </div>
                      <div class="clear"></div>
											<?php
										}  */
											?>
                      <div class="caption"></div>
                      <div class="field" style="width:350px">
												<input type="hidden" name="order_record" value="<?php echo (isset($order_record))?$order_record['ID']:"0"; ?>">
                        <input type="submit" name="save" tabindex="7" value="<?php echo (isset($order_record)&& $order_record>0)?"Update":"Save"; ?>" class="button" />
                        <a href="ordering-details.php<?php echo (isset($rbdid)&&$rbdid>0)?"?mode=form&rbdid=".$rbdid:""; ?>"><input type="button" value="New Form" class="button" /></a>
                      </div>
                      <div class="clear"></div>
                    </form>
                  </div><!--form-->
                </div><!--bodyTab1-->
<?php
						}
?>
              </div><!--summery_body-->
            </div><!--content-box-top-->
            <div style = "clear:both; height:20px"></div>
          </div><!--bodyWrapper-->
					<div id="fade"></div>
					<div id="xfade"></div>
  </body>
  </html>
  <?php ob_end_flush(); ?>
  <?php include("conn.close.php"); ?>
