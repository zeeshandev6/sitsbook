<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Contact Us - Sitsbook.com</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />

    <script src="resource/scripts/jquery.1.11.min.js" type="text/javascript"></script>
    <script src="resource/scripts/jquery-ui.min.js" type="text/javascript"></script>
		<script src="resource/scripts/configuration.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function(){
				$(".center-content-div").centerThisDiv();
				$(window).resize(function(){
					$(".center-content-div").centerThisDiv();
				});
			});
		</script>
</head>

	<body style="padding:0px;overflow:visible;">
		<div class="row text-center">
			<div class="clear"></div>
			<div class="panel panel-default text-center col-xs-12 col-md-8 center-content-div ">
				<div class="col-xs-12 text-center">
					<h2 class="text-danger">System Error!</h2>
					<div class="panel panel-info">
						<div class="panel-body">
							Contact SITSBOOK Dev Team.
						</div>
						<div class="panel-body">
							<a href="login.php" class="btn btn-warning"> <i class="fa fa-refresh"></i> Retry</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
  </body>
</html>
<?php include('conn.close.php'); ?>
