<?php
    $out_buffer = ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/currency_types.php');

    $objCurrecyTypes    = new CurrecyTypes();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>

        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div>

<?php
        if(isset($_GET['id'])){
            if(!empty($_GET['id'])){
                $id = $_GET['id'];
                $array = $objCurrecyTypes->getRecordDetailsByID($id);
                $id             =   $array['ID'];
                $name           =   $array['TITLE'];
                $symbol         =   $array['SYMBOL'];
            }
        }

        if(isset($_POST['save'])){
            //add new
            $id     = $_GET['id'];
            $objCurrecyTypes->title   = ucfirst($_POST['title']);
            $objCurrecyTypes->symbol  = mysql_real_escape_string(ucfirst($_POST['symbol']));

            if($id){
                $itemid = $objCurrecyTypes->update($id);
                exit(header('location: add-currency-type.php?id='.$id.'&action=updated'));
            }else{
                $idd = $objCurrecyTypes->save();
                exit(header('location: add-currency-type.php?id='.$idd.'&action=added'));
            }
        }

    ?>


    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p >Currency Details</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="currency-type-management.php"><div class="tab">List</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Currency Type Updated.");}
                    elseif($_GET['action']=='added'){ successMessage("Currency Type Added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "" class="form-horizontal">
                        <input type="hidden" name="item-category" value="<?php if(isset($_GET['pid'])){ echo 'update';} else { echo 'add'; } ?>" />
                        <input type="hidden" name="pid" value="<?php if(isset($_GET['pid'])){ echo $pid;} ?>" />
                        <div class="col-xs-12 mt-20">
                          <div class="form-group">
                            <label class="control-label col-sm-2">Currency </label>
                            <div class="col-sm-10">
                              <input class="form-control" name="title" value="<?php if(isset($name)){ echo $name;} ?>" required="required"/>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2">Symbol </label>
                            <div class="col-sm-10">
                              <input class="form-control" type="text" name="symbol" value="<?php if(isset($symbol)){ echo $symbol;}  ?>" />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                              <input type="submit" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="btn btn-primary" />
                              <input type="button" value="New Form" onclick="window.location.href='add-currency-type.php'"  class="btn btn-default" />
                            </div>
                          </div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>
        </div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
