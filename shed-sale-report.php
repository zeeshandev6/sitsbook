<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/emb_outward.php';
	include 'common/classes/machines.php';
	include 'common/classes/measure.php';
	include('common/classes/emb_stitch_account.php');
	include 'common/classes/machine_sales.php';
	include('common/classes/sheds.php');
	include('common/classes/emb_lot_register.php');

	$objOutwards 			= new outward();
	$objMachine  			= new machines();
	$objMeasure  			= new Measures();
	$objStitch   			= new EmbStitchAccount();
	$objMachineSales 		= new machineSales();
	$objSheds 				= new Sheds();
	$objLotRegisterDetails  = new EmbLotRegister();

	$thisMonth1st = date('01-m-Y');

	if(isset($_POST['search'])){
		$fromDate = ($_POST['fromDate']=="")?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$toDate = ($_POST['toDate']=="")?"":date('Y-m-d',strtotime($_POST['toDate']));

		$shed_id = $_POST['shed_id'];
		$firstDayOfMonth = date('d',strtotime($fromDate));
		if(strtotime($toDate)<=strtotime(date('Y-m-d'))){
			if($shed_id == ''){
				if($fromDate==''){
					$thisYear = date('Y');
					$firstJanuary = '01-01';
					$firstJanuaryThisYear = date('Y-m-d',strtotime($firstJanuary."-".$thisYear));
					$fromDate = $firstJanuaryThisYear;
				}
				$report = true;
			}else{
				if($firstDayOfMonth==01&&$fromDate!==''){
					$report = true;
				}else{
					$message = 'Start Date Must be 1st Day Of The Month!';
				}
			}
		}else{
			$message = 'Ending Date Can not Exceed Current Date!';
		}
	}


	$machineList = $objMachine->getList();
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />

	<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/emb.lot.register.config.js"></script>
    <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/printThis.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p> <i class="fa fa-home"></i> Sheds Sale Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form" style="width: 900px; margin: 0 auto;">
						<div class="clear"></div>
                    <form method="post" action="">
                        <div class="caption">Start Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($fromDate))?date('d-m-Y',strtotime($fromDate)):$thisMonth1st; ?>" name="fromDate" class="form-control datepicker"  />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">End Date</div>
                        <div class="field">
                            <input type="text" value="<?php echo (isset($toDate))?date('d-m-Y',strtotime($toDate)):date('d-m-Y'); ?>" name="toDate" class="form-control datepicker"  />
                        </div>
                        <div class="clear"></div>
                        <div class="caption">Shed Name</div>
                        <div class="field">
                            <select name="shed_id" class="form-control" style="width: 155px;height:30px;padding: 2px 5px;">
<?php
						$shedList = $objSheds->getList();
						if(mysql_num_rows($shedList)){
							while($shed = mysql_fetch_array($shedList)){
?>
								<option value="<?php echo $shed['SHED_ID']; ?>"><?php echo $shed['SHED_TITLE']; ?></option>
<?php
							}
						}
?>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
<?php
	if(isset($report)){

		if($shed_id == ''){
			$shedList = $objSheds->getList();
		}else{
			$shedList = $objSheds->getRecordDetails($shed_id);
		}

?>
        	<div class="content-box-content">
	            <span style="float:right;"><button class="button printThis">Print</button></span>
                <div id="bodyTab" class="printTable">
                	<div style="text-align:left;margin-bottom:20px;">
                        <p style="font-size:25px;padding:0;">Shed Sale Report - <?php echo $objSheds->getTitleStr($shed_id); ?></p>
                        <p>From <?php echo ($fromDate=="")?"Beginging":date('d-m-Y',strtotime($fromDate)); ?> to <?php echo date('d-m-Y',strtotime($toDate)); ?><span style="float:right;font-size:14px">Report generated on: <?php echo date('d-m-Y'); ?></span></p>
					</div>
<?php
	if(mysql_num_rows($shedList)){
	while($shedRow = mysql_fetch_array($shedList)){
		$machineList = $objMachine->getMachineListByShed($shedRow['SHED_ID']);
		if(mysql_num_rows($machineList)){
?>
                    <table>
                        <thead>
                            <tr style="background:#EEE;">
                            	<th width="8%" class="text-center">Machine Name</th>
                                <th width="6%" class="text-center">Processed Length</th>
                                <th width="9%" class="text-center">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
					$newMLength = 0;
					$embAmountTotal = 0;
					while($mahcineRow = mysql_fetch_array($machineList)){
					$objOutwards->machineNum = $mahcineRow['ID'];
					$report = $objOutwards->getOutwardDetailListByDateRangeShedReport($fromDate,$toDate);
						if(mysql_num_rows($report)){
							while($row = mysql_fetch_array($report)){
								$machDetail 	 = $objMachine->getRecordDetailsByID($row['MACHINE_ID']);
								$prevMachSale 	 = $objMachineSales->getPreviousMonthMachineSaleSingle($fromDate,$machDetail['ID']);
								$currentMachSale = $objMachineSales->getMonthEndMachineSaleSingle($toDate,$machDetail['ID']);
								$currentClaims   = $objLotRegisterDetails->getClaimedLengthOfMachineDateRange($machDetail['ID'],$fromDate,$toDate);
								$currentClaims['CLOTH_LENGTH']  = str_replace('-','',$currentClaims['CLOTH_LENGTH']);
								$currentClaims['AMOUNT'] 		= str_replace('-','',$currentClaims['AMOUNT']);



								settype($row['MEASURE_LENGTH'],'integer');
								settype($prevMachSale['CLOTH_LENGTH'],'integer');
								settype($currentMachSale['CLOTH_LENGTH'],'integer');
								settype($currentMachSale['AMOUNT'],'integer');

								$actLength = ($row['MEASURE_LENGTH']-$prevMachSale['CLOTH_LENGTH'])+$currentMachSale['CLOTH_LENGTH'];
								$actAmount = ($row['EMB_AMOUNT']-$prevMachSale['AMOUNT'])+$currentMachSale['AMOUNT'];

								$actLength -= $currentClaims['CLOTH_LENGTH'];
								$actAmount -= $currentClaims['AMOUNT'];
?>
                                <tr id="recordPanel" class="alt-row">
                                    <td style="text-align:center"><?php echo $machDetail['MACHINE_NO']." - ".$machDetail['NAME']; ?></td>
                                    <td style="text-align:center;"><?php echo $actLength; ?></td>
                                    <td style="text-align:center;"><?php echo $actAmount; ?></td>
                                </tr>
<?php
								$newMLength  += $actLength;
								$embAmountTotal  += $actAmount;
							}
						}
					}// while machine list

				}// if machine list num rows
?>
                        </tbody>
<?php
							if(!isset($newMLength)&&!isset($embAmountTotal)){
								$newMLength = 0;
								$embAmountTotal = 0;
							}
?>
                        <tfoot>
<?php
							if(isset($objOutwards->machineNum)){
?>
                           <!-- <tr>
                                <td style="text-align:right; font-weight:bold" colspan="2">Total:</td>
                                <td style="text-align:center;" ><?php echo (isset($newMLength))?$newMLength:0; ?></td>
                                <td style="text-align:center;" ><?php echo (isset($embAmountTotal))?$embAmountTotal:0; ?></td>
                            </tr>-->

<?php
							}
?>
<?php
							if(isset($objOutwards->machineNum)&&$objOutwards->machineNum!==''){
								$machineSale = $objMachineSales->getMonthEndMachineSale($toDate);
								$prevMachineSales = $objMachineSales->getPreviousMonthMachineSale($fromDate);
								$prevMonthAccountedFor = $objOutwards->getMahcineSaleAlreadyAccounted($fromDate);
								if($machineSale==''){
									unset($machineSale);
									$machineSale = array();
									$machineSale['CLOTH_LENGTH'] = 0;
									$machineSale['AMOUNT'] =  0;
								}
									//$newMLength    += $machineSale['CLOTH_LENGTH'];
									//$embAmountTotal+= $machineSale['AMOUNT'];
?>
                            <!--<tr>
                                <td style="text-align:right; font-weight:bold" colspan="2">Current Month Adjustment</td>
                                <td style="text-align:center;"><?php echo $machineSale['CLOTH_LENGTH']; ?></td>
                                <td style="text-align:center;"><?php echo $machineSale['AMOUNT']; ?></td>
                            </tr>-->

                            <!--<tr>
                                <td style="text-align:right; font-weight:bold" colspan="2">Available Machine Sale</td>
                                <td style="text-align:center;"><?php echo $newMLength; ?></td>
                                <td style="text-align:center;border-top:1px solid #000;"><?php echo $embAmountTotal; ?></td>
                            </tr>-->
<?php
								if($prevMachineSales==''){
									unset($prevMachineSales);
									$prevMachineSales = array();
									$prevMachineSales['CLOTH_LENGTH'] = 0;
									$prevMachineSales['AMOUNT']=  0;
								}
									//$newMLength    -= $prevMachineSales['CLOTH_LENGTH'];
									//$embAmountTotal-= $prevMachineSales['AMOUNT'];
?>
							<!--<tr>
                                <td style="text-align:right; font-weight:bold" colspan="2">Previous Month Adjustment</td>
                                <td style="text-align:center;"><?php echo ($prevMachineSales['CLOTH_LENGTH']=='')?"0":$prevMachineSales['CLOTH_LENGTH']; ?></td>
                                <td style="text-align:center;"><?php echo ($prevMachineSales['AMOUNT']=='')?"0":$prevMachineSales['AMOUNT']; ?></td>
                            </tr> -->
                            <tr>
                                <td style="text-align:right; font-weight:bold" colspan="1">Net Shed Sale</td>
                                <td style="text-align:center;"><?php echo $newMLength; ?></td>
                                <td style="text-align:center;border-top:1px solid #000;"><?php echo $embAmountTotal; ?></td>
                            </tr>
<?php
							}
?>
                        </tfoot>
                    </table>
                    <div class="clear"></div>
<?php
	} // shed list while loop
	} // if sheds num_rows
	}// if report
?>
                </div> <!--End bodyTab-->
                <div style="height:0px;clear:both"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
</html>
<script>
	$(function(){
		$("select").selectpicker();
		$(".printThis").click(function(){
			$(".printTable").printThis({
			  debug: false,
			  importCSS: false,
			  printContainer: true,
			   loadCSS: 'resource/css/reports.css',
			  pageTitle: "Emque Embroidery",
			  removeInline: false,
			  printDelay: 500,
			  header: null
		  });
		});
	});
<?php
	if(isset($message)){
?>
		displayMessageReport('<?php echo $message; ?>','shed-sale-report.php');
<?php
	}
?>
</script>
