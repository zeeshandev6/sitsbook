<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/design.php');

	$ObjDesign 		   = new design();

	$design_path 	   = "uploads/designs/";

	$total 	   = $objConfigs->get_config('PER_PAGE');

	$totalRows = $ObjDesign->countRows();
	$error_msg = array();

	if(isset($_POST['term'])){
		$search_term = $_POST['term'];
		$designList = $ObjDesign->search($search_term);
		$this_page = 0;
		if(!(mysql_num_rows($designList)) > 0){
			$error_msg['search_error'] = " No Match Found!";
		}
	}else{
		if(isset($_GET['page'])){
			$this_page = $_GET['page'];
			if($this_page>=1){
				$this_page--;
				$start = $this_page * $total;
			}else{
				$start = 0;
			}
			$designList = $ObjDesign->getPaged($start,$total);
		}else{
			$this_page = 0;
			$designList = $ObjDesign->getPaged($this_page,$total);
		}
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/lightbox.css" type="text/css"  />
	<style media="screen">
		table th{
		font-weight: bold !important;
		}
		table th,table td{
		padding: 10px !important;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
				<p> <i class="fa fa-image"></i> Designs Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="emb-design-details.php"><div class="tab">Add</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1">
                    <table style="margin:0 50px; width:90%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th width="2%" 	style="text-align:center">Image</th>
                               <th width="2%" 	style="text-align:center">Design Code</th>
                               <th width="12%" 	style="text-align:left">Design Name</th>
                               <th width="3%" 	style="text-align:center">Action</th>
                            </tr>
                        </thead>

                        <tbody>
<?php
						$serial = 1;
					if(mysql_num_rows($designList)){
						while($row = mysql_fetch_array($designList)){
?>
                            <tr id="recordPanel">
                                <td style="text-align:center;">
								<?php if(is_file($design_path.$row['IMAGE'])){ ?>
                                    <a href="<?php echo $design_path.$row['IMAGE']; ?>" data-lightbox="SameName">
                                        <img src="<?php echo $design_path.$row['IMAGE']; ?>" class="list_img" />
                                    </a>
								<?php }else{ ?>
									<p class="text-primary" style="font-size: 32px;"><i class="fa fa-image text-primary"></i></p>
								<?php } ?>
                                </td>
                                <td style="text-align:center"><?php echo $row['DESIGN_CODE']; ?></td>
                                <td style="text-align:left"><?php echo $row['TITLE']; ?></td>
                                <td class="appendable" style="text-align:center;position:relative;">
                                    <a href="emb-design-details.php?did=<?php echo $row['DESIGN_ID']; ?>&page=<?php echo $this_page+1; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                    <a do="<?php echo $row['DESIGN_ID']; ?>" class="pointer" title="Delete"> <i class="fa fa-times"></i> </a>
                            	</td>
                            </tr>
<?php
						$serial++;

						}//end of while
					}else{
?>
						<tr id="recordPanel">
                            <td style="text-align:left;" colspan="5">
                                <?php echo mysql_num_rows($designList)." Records Found!! "; ?>
                            </td>
                        </tr>
<?php

					}
?>
                        </tbody>
<?php
					if(!isset($_POST['search'])){
?>
	                    <tfoot>
                        	<tr>
                            	<td colspan="4" class="pagination_row">
                                <p style="padding: 10px;">
<?php
						$count 		 = (mysql_num_rows($designList))?mysql_fetch_array($totalRows):mysql_num_rows($designList);
						$total_pages = ceil($count[0]/$total);
						$i 			 = 1;
						$thisFileName = $_SERVER['PHP_SELF'];
						if(isset($this_page) && $this_page>0){
?>
							<a href="emb-design-list.php?page=1" class="nav_page">First</a>
<?php
						}
						if(isset($this_page) && $this_page>=1){
							$prev = $this_page;
?>
							<a href="emb-design-list.php?page=<?php echo $prev; ?>" class="nav_page">Prev</a>
<?php
						}
						$this_page_act = $this_page;
						$this_page_act++;
						while($total_pages>=$i){
							$left = $this_page_act-3;
							$right = $this_page_act+3;
							if($left<=$i && $i<=$right){
							$current_page = ($i == $this_page_act)?"current_nav_page":"nav_page";
?>

                                	<?php echo "<a class=\"".$current_page."\" href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
<?php
							}
							$i++;
						}
						$this_page++;
						if(isset($this_page) && $this_page<$total_pages){
							$next = $this_page;
							echo " <a href='".$thisFileName."?page=".++$next."' class='nav_page'>Next</a>";
						}
						if(isset($this_page) && $this_page<$total_pages){
?>
							<a href="emb-design-list.php?page=<?php echo $total_pages; ?>" class="nav_page">Last</a>
<?php
						}
						echo "<a class=\"total_records_pagin\">Total Records : ".$count[0]."</a>";
?>
								</p>
								</td>
							</tr>
						</tfoot>
<?php
					}
?>
                    </table>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>

                <div id="bodyTab2" style="display:none; margin-top:-10px">
                    <div id="form">
                    <form method="post" action="" class="form-horizontal">
											<div class="form-group">
												<label class="control-label col-sm-2">Design Name</label>
												<div class="col-sm-10">
													<input type="text" name="term" class="form-control" />
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2"></label>
												<div class="col-sm-10">
													<input type="submit" value="Search" name="search" class="button"/>
												</div>
											</div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
</body>
<script>
	//$ is defined in simpla.jquery.configuration.js
	$(document).ready(function() {
			$("a.pointer").click(function(){
				var idValue = $(this).attr("do");
				$("#xfade").fadeOut();
				$("#popUpDel").remove();
				var appendable_td = $(this).parent(".appendable");
				$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Confirm Delete?</p><a class='dodelete'>Confirm</a><a class='nodelete'>Cancel</a></div>");
				var win_hi = $(window).height()/2;
				var win_width = $(window).width()/2;
				win_hi = win_hi-$("#popUpDel").height()/2;
				win_width = win_width-$("#popUpDel").width()/2;
				$("#popUpDel").css({
					'position': 'fixed',
					'top': win_hi,
					'left': win_width
				});
				$("#popUpDel").hide();
				$("#xfade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$("#xfade").fadeIn('slow');
						$("#popUpDel").hide();
						$.get("db/del-design.php", {id : idValue}, function(data){
							window.location.replace("emb-design-list.php");
							$("#xfade").fadeOut('fast');
						});
					});
				$(".nodelete").click(function(){
					$("#popUpDel").slideUp();
					$("#xfade").fadeOut('fast');
					});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#xfade").fadeOut('fast');
					});
			});
			$("#tab1,#tab2").click(function(){
				$(".h3_php_error").show();
				});
    });

<?php
	if(isset($_GET['goSearch'])){
		echo "tab('2', '1', '2');";
	}
?>
</script>
</html>
