<?php
    include 'common/connection.php';
    include 'common/config.php';
    include 'common/classes/accounts.php';

    //Permission
    if(!in_array('chart-of-accounts',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objAccounts = new ChartOfAccounts();

    if(isset($_POST['supp_name'])){
        $return = array();
        $bank_name = mysql_real_escape_string($_POST['supp_name']);
        $account_code_id = $objAccounts->addSubMainChild($bank_name,'010102');
        if($account_code_id){
            $accountDetailArray = mysql_fetch_array($objAccounts->getAccountCodeById($account_code_id));
            $return['ACC_CODE'] = $accountDetailArray['ACC_CODE'];
            $return['ACC_TITLE'] = $accountDetailArray['ACC_TITLE'];
            $return['OK'] = 'Y';
        }else{
            $return['OK'] = 'Y';
        }
        echo json_encode($return);
        exit();
    }

    if(isset($_POST['addAccount'])){
        $general  = mysql_real_escape_string($_POST['general']);
        $main     = mysql_real_escape_string($_POST['main']);
        $submain  = mysql_real_escape_string($_POST['subMain']);
        $title    = mysql_real_escape_string($_POST['title']);
        if($general==00 && $title !== ""){
            //$objAccounts->addGeneral($title);
            $message = 'Chart Of Accounts Is Limited To 5 General Categories.';
        }elseif($main==00 && $title !== ""){
            $objAccounts->addMain($title,$general);
        }elseif($submain==00 && $title !== ""){
            $objAccounts->addSubMain($title,$main);
        }elseif($title !== "" && $submain !== 00){
            $objAccounts->addSubMainChild($title,$submain);
        }
        $newAccountId       = mysql_insert_id();
        $accountDetailArray = mysql_fetch_array($objAccounts->getAccountCodeById($newAccountId));

        $accountDetailArray = mysql_fetch_array($objAccounts->getAccountCodeById($newAccountId));
        if(strlen($accountDetailArray['ACC_CODE'])>6){
            if(substr($accountDetailArray['ACC_CODE'],0,6)=='010104'){  //customers
                $newCustomerId = $objAccounts->creatCustomer($accountDetailArray['ACC_CODE'],$accountDetailArray['ACC_TITLE']);
                echo "<script>window.location.href = 'customer-detail.php?cid=".$newCustomerId."';</script>";
                exit();
            }
            if(substr($accountDetailArray['ACC_CODE'],0,6)=='040101'){  //Suppliers
                $newSupplierId = $objAccounts->creatSupplier($accountDetailArray['ACC_CODE'],$accountDetailArray['ACC_TITLE']);
                echo "<script>window.location.href = 'supplier-details.php?cid=".$newSupplierId."';</script>";
                exit();
            }
        }

        if(!isset($message)){
            echo "<script>window.location.href = 'accounts-management.php';</script>";
            exit();
        }
    }

    if(isset($_GET['hrchi'])){
        $heirarchi = mysql_real_escape_string($_GET['hrchi']);
        if(strlen($heirarchi)>6){
            unset($heirarchi);
            $hrchi_general  = '';
            $hrchi_main     = '';
            $hrchi_sub_main = '';
        }else{
            if(strlen($heirarchi)==6){
                $hrchi_general  = substr($heirarchi,0,2);
                $hrchi_main     = substr($heirarchi,0,4);
                $hrchi_sub_main = substr($heirarchi,0,6);
            }
            if(strlen($heirarchi)==4){
                $hrchi_general  = substr($heirarchi,0,2);
                $hrchi_main     = substr($heirarchi,0,4);
                $hrchi_sub_main = '';
            }
            if(strlen($heirarchi)==2){
                $hrchi_general  = substr($heirarchi,0,2);
                $hrchi_main     = '';
                $hrchi_sub_main = '';
            }
        }
    }
    $accountList    = $objAccounts->getList();
    $array_exculde  = '["01","0101","010101","0101010001","010102","010103","010104","010105","010106","0101060001","0101060002","010107","010108","0101080001","010109","010110","010111","0102","010201","010202","010203","010204","010205","02","0201","020101","0201010001","0201010002","0201010003","020102","0201020001","03","0301","030101","0301010001","030102","030103","030104","0301040001","030105","04","0401","040101","040102","040103","040104","040105","040106","0402","040201","040202","05","0501","050101","050102","0501010001","0501020001"]';
    $array_exculde  = json_decode($array_exculde);
?>
<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/daily-report.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
            table tbody tr:hover{
                background-color: #EEE;
            }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/printThis.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#print_it").click(function(){
                    $(".printable").printThis({
                          debug: false,
                          importCSS: false,
                          printContainer: true,
                          loadCSS: 'resource/css/reports.css',
                          pageTitle: "Smart IT Solutions",
                          removeInline: false,
                          printDelay: 500,
                          header: null
                    });
                });
            });
        </script>
   </head>
   <body>
        <div id="body-wrapper">
            <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
        <div id="bodyWrapper">
            <div class = "content-box-top">
                <div class = "summery_body">
                    <div class = "content-box-header">
                        <p>Accounts Management</p>
                        <a style="margin: 8px 10px;" class="btn btn-sm btn-primary pull-right" id="print_it"><i class="fa fa-print"></i></a>
                        <div class="clear"></div>
                    </div><!-- End .content-box-header -->
                    <div style="height:50px;"></div>
                    <div id="bodyTab1" class="printable">
                        <div class="pageHeader">
                            <p style="font-size:18px;padding:0;text-align:center;">Chart of Accounts</p>
                            <p style="float:left; margin-left:0px; font-size:12px" class="repoGen">Report generated on : <?php echo date('d-m-Y'); ?></p>
                        </div>
                        <div class="clear"></div>
                        <table class="table table-striped table-hover tableBreak" width="90%" cellspacing="0" align="left" style="margin:0px auto; margin-bottom:20px">
                            <thead class="tHeader">
                                <tr>
                                    <th style="width:20%;text-align:right;padding-right:10px !important;">Account Code</th>
                                    <th style="width:80%;text-align:left;padding-left:10px !important; ">Account Title</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                if(mysql_num_rows($accountList)){
                    while($row = mysql_fetch_array($accountList)){
                        $inherit = $objAccounts->checkInherited($row['ACC_CODE']);
?>
                                <tr>
                                    <td class="acc_code " style="text-align:right;"><?php echo $row['ACC_CODE']; ?></td>
                                    <td class="acc_name " align="left" style="position: relative;">
                                        <?php echo $row['ACC_TITLE']; ?>
                                    </td>
                                </tr>
<?php
                    }
                }
?>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                    </div><!--End bodyTab1-->
                </div><!-- End summer -->
            </div><!-- End .content-box -->
        </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
