<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/gate_pass.php');
	include('common/classes/gate_pass_details.php');

	//Permission
  if(!in_array('gate-pass',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objGatePass 				= new GatePass();
	$objGatePassDetails = new GatePassDetails();

	if(isset($_POST['spec_data'])){
			$return_array           	= array();
			$return_array['ID']     	= 0;
			$return_array['ACTION'] 	= '';

			$id   										= isset($_POST['id'])?(int)mysql_real_escape_string($_POST['id']):0;

			$objGatePass->entry_date 	= date("Y-m-d",strtotime($_POST['entry_date']));
			$objGatePass->lot_no			= mysql_real_escape_string($_POST['lot_no']);
			$objGatePass->gp_no				= (int)mysql_real_escape_string($_POST['gp_no']);
			$objGatePass->to_address	= mysql_real_escape_string($_POST['to_address']);

			if($id == 0){
					$id = $objGatePass->save();
					$return_array['ID']     = $id;
					if($id > 0){
							$return_array['ACTION'] = 'a';
					}
			}else{
					$updated = $objGatePass->update($id);
					$return_array['ID']         = $id;
					if($updated){
							$return_array['ACTION'] = 'u';
					}
			}
			if(isset($_POST['deleted'])&&count($_POST['deleted'])){
				$deleted = json_decode($_POST['deleted']);
				foreach($deleted as $key => $gp_detail_id){
					$objGatePassDetails->delete($gp_detail_id);
				}
			}
			if($_POST['spec_data'] != ''&&$id>0){
					$json_data = json_decode($_POST['spec_data']);
					foreach($json_data as $key => $row){
						$objGatePassDetails->gp_id 			 = (int)$id;
						$objGatePassDetails->particulars = $row->particulars;
						$objGatePassDetails->quantity    = (float)$row->quantity;

						if($row->row_id == 0){
							$objGatePassDetails->save();
						}else{
							$objGatePassDetails->update($row->row_id);
						}
					}
			}
			echo json_encode($return_array);
			exit();
	}
	$gate_pass_number = $objGatePass->genGatePassNumber();
	$id 							= 0;
	$record_details 	= NULL;
	if(isset($_GET['id'])){
		$id 						 = mysql_real_escape_string($_GET['id']);
		$record_details  = $objGatePass->getDetail($id);
		$particular_list = $objGatePassDetails->getList($id);
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
				<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
				<script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript">
					$(document).ready(function(){
						<?php
							if(isset($particular_list)){
								if(mysql_num_rows($particular_list)){
									$counter = 0;
									while($particulars = mysql_fetch_assoc($particular_list)){
										if($counter>0){
											?>
											add_new_row();
							<?php
										}
							?>
										$(".spec_row").last().attr('data-row-id','<?php echo $particulars['ID'] ?>');
										$(".spec_row").last().find("td.particulars input").val('<?php echo $particulars['PARTICULARS'] ?>');
										$(".spec_row").last().find("td.quantity    input").val('<?php echo $particulars['QUANTITY'] ?>');
									  <?php
										$counter++;
									}
								}
						 	}
						?>
						$("input.save_gp").click(function(){
							save_form();
						});
					});
					var add_new_row = function(){
							$(".spec_row").first().clone().insertAfter($(".spec_row").last());
							$(".spec_row").last().find(".add_btn").remove();
							$(".spec_row").last().attr("data-row-id",'0');
							$(".spec_row").last().find("td.spec_act").append('<button class="btn btn-danger rm_btn"  onclick="remove_row(this);"> <i class="fa fa-times"></i> </button>');
					}
					var remove_row = function(elm){
						var row_id = parseInt($(elm).parent().parent().attr("data-row-id"))||0;
						$(elm).parent().parent().remove();
						if(row_id>0){
							$("div#form").append('<input type="hidden" class="deleted_rows" value="'+row_id+'" />');
						}
					}
					var save_form = function(){
							var id         = $("input[name=id]").val();
							var entry_date = $("input.entry_date").val();
							var gp_no 		 = $("input.gp_no").val();
							var lot_no 		 = $("input.lot_no").val();
							var to_address = $("input.to_address").val();

							var spec_data = {};
							var deleted   = {};
							$("tr.spec_row").each(function(tr_i,tr_e){
									spec_data[tr_i]          				= {};
									spec_data[tr_i]['row_id']  			= parseInt($(this).attr("data-row-id"))||0;
									spec_data[tr_i]['particulars']  = $(this).find("td.particulars input").val();
									spec_data[tr_i]['quantity']  		= $(this).find("td.quantity input").val();
							});
							$("input.deleted_rows").each(function(i,e){
								deleted[i] = parseFloat($(this).val())||0;
							});
							spec_data = JSON.stringify(spec_data);
							deleted   = JSON.stringify(deleted);
							$.post('gate-pass-details.php',{id:id,spec_data:spec_data,deleted:deleted,gp_no:gp_no,entry_date:entry_date,lot_no:lot_no,to_address:to_address},function(data){
									data = $.parseJSON(data);
									if(data['ID'] > 0){
											window.location.href = 'gate-pass-details.php?id='+id+'&action='+data['ACTION'];
									}
							});
							return;
					}
				</script>
   </head>
   <body>
        <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Gatepass Management</p>
                  		<span id="tabPanel">
                     		<div class="tabPanel">
                        		<a href="gate-pass.php?tab=list" ><div class="tab">List</div></a>
                            <a href="gate-pass.php?tab=search" ><div class="tab">Search</div></a>
													  <div class="tabSelected">Details</div>
                     		</div>
                  		</span>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
	               	<div class="clear"></div>
               		<div id = "bodyTab1">
                  		<div id = "form">
<?php  if(isset($_GET['added']) || isset($_GET['updated'])){

?>
					<span class="addedNote"><?php echo (isset($_GET['updated']))?"Record Updated. ":"Record Added. "; ?></span>
<?php
		}
?>
												<input type="hidden" name="id" value="<?php echo isset($id)?$id:"0"; ?>" />

												<div class="caption">Gate Pass No.</div>
												<div class="field">
													<input type="text" class="form-control gp_no" value="<?php echo $record_details == NULL?$gate_pass_number:$record_details['GP_NO']; ?>" />
												</div>

												<div class="caption">Entry Date</div>
												<div class="field" style="width: 220px;">
													<input type="text" class="form-control entry_date datepicker" value="<?php echo isset($record_details)?date('d-m-Y',strtotime($record_details['ENTRY_DATE'])):""; ?>" />
												</div>
												<div class="clear"></div>

												<div class="caption">Lot No.</div>
												<div class="field">
													<input type="text" class="form-control lot_no" value="<?php echo isset($record_details)?$record_details['LOT_NO']:""; ?>" />
												</div>
												<div class="clear"></div>

												<div class="caption">To Address</div>
												<div class="field" style="width:650px;">
													<input type="text" class="form-control to_address" value="<?php echo isset($record_details)?$record_details['TO_ADDRESS']:""; ?>" />
												</div>
												<div class="clear"></div>

												<div class="caption"> Description :</div>
												<div class="field" style="width:650px;">
														<table class="pull-left col-xs-12">
																<thead>
																		<tr>
																				<th class="col-xs-7 text-center">Particulars</th>
																				<th class="col-xs-3 text-center">Quantity</th>
																				<th class="col-xs-1 text-center">Action</th>
																		</tr>
																</thead>
																<tbody>
																		<tr class="spec_row" data-row-id="0">
																				<td class="particulars text-right"><input type='text' class="form-control"/></td>
																				<td class="quantity  text-left" ><input type='text' class="form-control"/></td>
																				<td class="spec_act   text-center"  >
																						<button class="btn btn-primary btn-sm add_btn" onclick="add_new_row();return false;"> <i class="fa fa-plus"></i> </button>
																				</td>
																		</tr>
																</tbody>
														</table>
												</div>
												<div class="clear"></div>

												<div class="caption"></div>
												<div class="field" style="width:650px;">
													<a href="gate-pass-details.php" class="btn btn-default pull-left">New Form</a>
													<input type="button" class="btn btn-primary btn-sm pull-right save_gp"  value="<?php echo ($id==0)?"Save":"Update"; ?>">
													<?php if($id > 0){ ?>
													<a href="print-gate-pass.php?id=<?php echo $id; ?>" target="_blank" class="btn btn-default pull-right mr-10"><i class="fa fa-print"></i> Print</a>
													<?php } ?>
												</div>

                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab1-->
            	</div><!-- End summery_body -->
         	</div><!-- End content-box -->
      	</div><!--End body-wrapper-->
        <div id="xfade"></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
