<?php
	include("common/connection.php");
    include("common/classes/items.php");
    include("common/classes/company_details.php");

    $objItems = new Items;
    $objCompanyInfo = new CompanyDetails;

    $companyName = $objCompanyInfo->getTitle(1);
    $companyName = substr($companyName, 0,21);
	if(!isset($_GET['id'])){
?>
<!DOCTYPE html 
>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="resource/css/labels.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
    <script src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
</head>
<body style="background-image:none !important;background-color:#FFF !important;">
<?php
    if(!isset($_GET['submit'])){
    ?>
    <div class="col-md-8">
        <div class="panel panel-default" style="padding:1em;">
            <form>
                <label></label>
                <div class="clearfix"></div>
                <div class="col-md-8">
                    <div class="caption">From</div>
                    <input type="text"  name="from" value="" class="form-control" style="width:150px;"  />
                </div>
                <div class="col-md-8">
                    <div class="caption">To</div>
                    <input type="text"  name="end" value="" class="form-control" style="width:150px;"  />
                </div>
                <div class="clearfix"></div>
                <div class="col-md-8">
                    <div class="caption"  style="width:100px;">Skip</div>
                    <input type="text"  name="skip" value="0" class="form-control" style="width:150px;" />
                </div>
                <div class="clearfix"></div>
                <div style="height:20px;"></div>
                <div class="col-md-8">
                    <input type="submit" name="submit" value="Generate" class="button" />
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
<?php 
    }else{
        $from = (int)$_GET['from'];
        $to   = (int)$_GET['end'];
        $skips  = isset($_GET['skip'])?$_GET['skip']:0;
    ?>
        <?php
            for ($i=1; $i <= $skips; $i++){
                /*
            ?>
                    <div id="lable"></div>
        <?php
                */
            }
            ?>
        <div class="barcode-print">
        <?php
            for ($i=$from; $i <= $to; $i++){
                /*
                $max_length  = 4;
                $this_length = strlen($i);
                $zero_num    = $max_length-$this_length;
                $item_code   = (int)$i;
                $zeros       = '';
                for($v=0; $v < $zero_num; $v++){
                    $zeros .= '0';
                }
                $item_code = $zeros.$item_code;
                */
            ?>
                <div id="lable">
                    <div class="line"> </div>
                    <div class="line"><?php echo $companyName; ?></div>
                        <div class="left"></div>
                    <div class="right">
                     <div class="barcode">
                         <img src="lib/barcode.php?size=30&text=<?php echo $i; ?>" />
                        </div>
                        <div class="barcodeNo pull-right"><?php echo $i; ?></div>
                    </div>
                    <div class="clear"></div>
                </div><!--lable-->
        <?php
            }
            ?>
            <div class="clear"></div>
        </div><!--barcode-print-->
    <?php
        }
        ?>
</body>
</html>
<?php include('conn.close.php'); ?>
<?php
    }
?>