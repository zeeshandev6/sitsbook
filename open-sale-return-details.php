<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
  include('common/classes/sale.php');
	include('common/classes/sale_return.php');
	include('common/classes/sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sale-returns',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes	 = new ChartOfAccounts();
    $objSale             = new Sale();
	$objInventory        = new SaleReturn();
	$objInventoryDetails = new SaleReturnDetails();
	$objItems            = new Items();
	$objItemCategory 	 = new itemCategory();
	$objTaxRates         = new TaxRates();
	$objDepartments      = new Departments();

	$suppliersList   			= $objAccountCodes->getAccountByCatAccCode('010104');
	$itemsCategoryList   	= $objItemCategory->getList();
	$taxRateList     			= $objTaxRates->getList();

    $sale_id = 0;
	$sale_return_id = 0;
	if(isset($_GET['id'])){
		$sale_return_id = mysql_real_escape_string($_GET['id']);
		if($sale_return_id != '' && $sale_return_id != 0){
			$inventory        = mysql_fetch_array($objInventory->getRecordDetails($sale_return_id));
			$inventoryDetails = $objInventoryDetails->getList($sale_return_id);
            $sale_id          = $inventory['SALE_ID'];
		}else{
			$sale_return_id = 0;
		}
	}
    if(isset($_GET['sale'])){
        $sale_id      = (int)$_GET['sale'];
        $inventory    = $objSale->getDetail($sale_id);
    }
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  											  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  											  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  										  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  												  />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  												  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  										  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  		 						  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  	 						  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  						  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
		<style media="screen">
			table.prom th,table.prom td{
				padding: 10px !important;
			}
			tr.quickSubmit td{
				padding: 1px !important;
			}
		</style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/open.sale.return.config.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>
<body>
    <div id="body-wrapper">
				<div id="sidebar">
					<?php include("common/left_menu.php") ?>
				</div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sale Returns Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="sale-return.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="sale-return.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Details</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >

                <div id="bodyTab1">
                    <div id="form" style="width: 1000px;margin: 20px auto;">
                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Return Date</div>
                            <div class="field" style="width:150px;margin-left:0px;padding: 0px;">
                                <input type="text" name="return_date" value="<?php echo (isset($inventory))?date("d-m-Y",strtotime($inventory['SALE_DATE'])):date('d-m-Y'); ?>" class="form-control return_date datepicker" style="width:150px" />
                            </div>
                            <div class="caption" style="width:100px;">Memo #</div>
                            <div class="field" style="width:118px">
                            	<input type="text" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>" class="form-control bill_num" />
                            </div>
														<?php
															if($saleTaxModule == 'Y'){
														?>
                            <div class="field" style="width:118px;margin-left: 30px;">
                            	<input id="cmn-toggle-unreg" value="Y" class="css-checkbox registered_tax" type="checkbox" <?php echo (isset($inventory)&&$inventory['REGISTERED_TAX'] == 'Y')?"checked":""; ?> />
						        					<label for="cmn-toggle-unreg" class="css-label" style="margin-top:5px;margin-right: 20px;">Registered(FBR)</label>
                            </div>
														<?php
															}
														?>
                            <div class="clear"></div>

                            <div class="caption" style="width:100px;margin-left:0px;padding: 0px;">Account</div>
                            <div class="field" style="width:200px;position:relative;">
                                <select class="account_code_selector form-control "
                                        data-style="btn-default"
                                        data-live-search="true" data-hide-disabled="true" style="border:none" >
                                   <option selected value=""></option>
<?php
                            if(mysql_num_rows($suppliersList)){
                                while($account = mysql_fetch_array($suppliersList)){
                                    $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
<?php
                                }
                            }
                            if(mysql_num_rows($cash_in_hand_list)){
                                while($cash_rows = mysql_fetch_array($cash_in_hand_list)){
                                    $cash_rows['CASH_ACC_TITLE'] = 'Cash In Hand A/c'." ".$cash_rows['FIRST_NAME']." ".$cash_rows['LAST_NAME'];
                                    if(!$admin&&$cash_in_hand_acc_code!=$cash_rows['CASH_ACC_CODE']){
                                        continue;
                                    }
                                    $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$cash_rows['CASH_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                    <option <?php echo $selected; ?> disabled data-subtext="<?php echo $cash_rows['CASH_ACC_CODE']; ?>" value="<?php echo $cash_rows['CASH_ACC_CODE']; ?>"><?php echo $cash_rows['CASH_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
                            </div>
                            <div class="caption">
                                <input type="checkbox" id="mor1" <?php echo (isset($inventory) && substr($inventory['CUST_ACC_CODE'],0,6)== '010101')?"checked=\"checked\"":""; ?> onchange="makeItCash(this);" class="trasactionType css-checkbox" value="0101010001" title="Cash In Hand" />
                                <label id="mor1" for="mor1" class="css-label" title="Ctrl+Space"><small>Cash Transaction</small></label>
                            </div>
                            <div class="supplier_name_div" style="display: none;">
                                <div class="field" style="width:195px;margin-left:10px;">
                                    <input class="form-control customer_name" placeholder="Name" value="<?php echo (isset($inventory))?$inventory['BILL_NO']:''; ?>"  />
                                </div>
                            </div>
                            <input type="hidden" class="sale_return_id" value="<?php echo $sale_return_id; ?>" />
                            <input type="hidden" class="sale_id" value="<?php echo $sale_id; ?>" />
                            <div style="height: 50px;"></div>
                            <table class="prom">
                            <thead>
                                <tr>
                                   <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">Item</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Quantity</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Unit Price</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Discount %</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Sub Total</th>
                                   <th width="12%"  style="font-size:12px;font-weight:normal;text-align:center"> Tax @ </th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Tax Amount</th>
                                   <th width="10%"  style="font-size:12px;font-weight:normal;text-align:center">Total Amount</th>
                                   <th width="5%"  style="font-size:12px;font-weight:normal;text-align:center">StockInHand</th>
                                   <th width="15%" style="font-size:12px;font-weight:normal;text-align:center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="quickSubmit" style="background:none">
                                    <td>
																			<select class="itemSelector show-tick form-control" data-style="btn-default" data-live-search="true">
																				 <option selected value=""></option>
<?php
																	if(mysql_num_rows($itemsCategoryList)){
																			while($ItemCat = mysql_fetch_array($itemsCategoryList)){
										$itemList = $objItems->getActiveListCatagorically($ItemCat['ITEM_CATG_ID']);
?>
											<optgroup label="<?php echo $ItemCat['NAME']; ?>">
<?php
										if(mysql_num_rows($itemList)){
											while($theItem = mysql_fetch_array($itemList)){
												if($theItem['ACTIVE'] == 'N'){
													continue;
												}
												if($theItem['INV_TYPE'] == 'B'){
													continue;
												}
?>
																										 <option data-type="I" value="<?php echo $theItem['ID']; ?>"><?php echo ($theItem['ITEM_BARCODE']!='')?$theItem['ITEM_BARCODE']."-":""; ?><?php echo $theItem['NAME']; ?></option>
<?php
											}
										}
?>
											</optgroup>
<?php
																			}
																	}
?>
																			</select>
                                    </td>
                                    <td>
                                        <input type="text" class="quantity form-control text-center" />
                                    </td>
                                    <td>
                                        <input type="text" class="unitPrice form-control  text-center" />
                                    </td>
                                    <td>
                                        <input type="text" class="discount form-control text-center" />
                                    </td>
                                    <td>
                                        <input type="text"  readonly value="0" class="subAmount form-control text-center" />
                                    </td>
                                    <td class="taxTd">
                                        <input class="taxRate form-control text-center" value="" />
                                    </td>
                                    <td>
                                        <input type="text"  readonly value="0" class="taxAmount form-control text-center" />
                                    </td>
                                    <td>
                                        <input type="text"  readonly  value="0" class="totalAmount form-control text-center" />
                                    </td>
                                    <td>
                                        <input type="text" class="inStock form-control text-center" theStock="0" readonly />
                                    </td>
                                    <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                </tr>
                            </tbody>
                            <tbody class="transcations_list">
<?php
						if(isset($inventoryDetails) && mysql_num_rows($inventoryDetails)){
							while($invRow = mysql_fetch_array($inventoryDetails)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
?>
                                <tr class="alt-row transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td class="text-center itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
                                    <td style="text-align:center;" class="quantity"><?php echo $invRow['QUANTITY'] ?></td>
                                    <td style="text-align:center;" class="unitPrice"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td style="text-align:center;" class="discount"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                    <td style="text-align:right;" class="subAmount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td style="text-align:center;" class="taxRate"><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td style="text-align:right;" class="taxAmount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td style="text-align:right;" class="totalAmount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                    <td style="text-align:center;"> - - - </td>
                                    <td style="text-align:center;">
																			<a id="view_button" onClick="editThisRow(this);" title="Update"><i class="fa fa-pencil"></i></a>
																			<a class="pointer" do="" title="Delete" onclick="delete_row(this);"><i class="fa fa-times"></i></a>
																		</td>
                                </tr>
<?php
							}
						}
?>
                            </tbody>
														<tfoot>
															<tr class="totals">
																<td style="text-align:center;background-color:#EEEEEE;">Total</td>
																	<td style="text-align:center;background-color:#f5f5f5;" class="qtyTotal"></td>
																	<td style="text-align:center;background-color:#EEE;">- - -</td>
																	<td style="text-align:center;background-color:#f5f5f5;" class="discountAmount"></td>
																	<td style="text-align:right;background-color:#f5f5f5;" class="amountSub"></td>
																	<td style="text-align:center;background-color:#EEE;">- - -</td>
																	<td style="text-align:right;background-color:#f5f5f5;" class="amountTax"></td>
																	<td style="text-align:right;background-color:#f5f5f5;" class="amountTotal"></td>
																	<td style="text-align:center;background-color:#EEE;">- - -</td>
																	<td style="text-align:center;background-color:#EEE;">- - -</td>
															</tr>
														</tfoot>
                        </table>
												<div class="clear m-10"></div>
                        <div class="underTheTable">
                            <?php   if($sale_return_id == 0){  ?>
                            <div class="button pull-right saveSale">Save</div>
                            <?php   } ?>
                            <div class="button pull-left" onclick="window.location.href='open-sale-return-details.php';">New Form</div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
                        <div style="margin-top:10px"></div>
					 					</div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
		$('.account_code_selector').selectpicker();
		$('.itemSelector').selectpicker();
		$(document).calculateColumnTotals();
		//Numericalize
		$("input.bill_num").numericOnly();
		$("input.quantity").numericOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.account_code_selector button");

		$("div.account_code_selector button").keyup(function(e){
            var supp = $("select.account_code_selector option:selected").val();
            if(e.keyCode == 13 && supp != ''){
                if($("select.account_code_selector option:selected[value^='010101']").length){
                    console.log('e');
                    $(".supplier_name_div").show();
                    $(".supplier_name_div input").focus();
                }else{
                    $(".supplier_name_div").hide();
                    $('div.itemSelector button').focus();
                }
            }
        });

		$("input.bill_num").setFocusTo("div.account_code_selector button");
		$("input.customer_name").setFocusTo("div.itemSelector button");
		$('.itemSelector').change(function(){
			$(this).getItemDetails();
			$("input.quantity").focus();
		});

		$("div.itemSelector button").keyup(function(e){
			var item = $('.itemSelector option:selected').val();
			if(e.keyCode == 13 && item != ''){
				if($(".updateMode").length == 0){
					$(this).getItemDetails();
				}
				$("input.quantity").focus();
			}
		});

		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13&&$(this).val()!=''){
				$("input.unitPrice").focus();
			}
		}).keyup(function(e){
			stockOs();
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});

		$("input.taxRate").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$(".addDetailRow").focus();
			}
		});

		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
		});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".saveSale").click(function(){
			saveSale();
		});
        $("input.bill_num").focus();
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Memo Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Memo Updated Successfully!');
<?php
		}
?>

</script>
