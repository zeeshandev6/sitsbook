<?php
    ob_start();
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/processing_type.php');
    include ("common/classes/notifications.php");
    include ("common/classes/alerts.php");
?>
    <!DOCTYPE html 
        >

    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();


            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

<?php
    if(isset($_SESSION['classuseid'])){

        $objCurrecyTypes        = new ProcessingTypes();
        $objNotifications       = new Notifications();
        $objAlerts              = new Alerts();

        //get one record by id
        if(isset($_GET['id'])){
            if(!empty($_GET['id'])){
                $id = $_GET['id'];
                $array = $objCurrecyTypes->getRecordDetailsByID($id);
                $id             =   $array['ID'];
                $name           =   $array['TITLE'];
            }
        }

        if(isset($_POST['save'])){
            //add new
            $id     = $_GET['id'];
            $objCurrecyTypes->title          =   ucfirst($_POST['title']);

            if($id){
                $itemid = $objCurrecyTypes->update($id);
                if($itemid){
                    //notification
                    $alert_details  = $objAlerts->getRecordDetailsByID(63);

                    $alert_details['NOTIFICATION'] = str_replace('[TITLE]', $objCurrecyTypes->title, $alert_details['NOTIFICATION']);
                    $alert_details['NOTIFICATION'] = str_replace('[OPERATOR]',$sessionUserDetails['FIRST_NAME']." ".$sessionUserDetails['LAST_NAME'] , $alert_details['NOTIFICATION']);

                    foreach ($admins_for_notifications as $key => $admin_notify_id){
                        $objNotifications->notification_date   = date('Y-m-d H:i:s');
                        $objNotifications->notification_status = ($alert_details['SHOW_NOTIFICATION']=='Y')?"N":"Y";
                        $objNotifications->from_account        = $user_id;
                        $objNotifications->to_account          = $admin_notify_id;
                        $objNotifications->sc_id               = isset($sc_id)?$sc_id:0;
                        $objNotifications->notification_type   = $alert_details['ALERT'];
                        $objNotifications->title               = $alert_details['NOTIFICATION'];
                        $objNotifications->save();
                    }
                }
                exit(header('location: add-processing-type.php?id='.$id.'&action=updated'));
            }else{
                $idd = $objCurrecyTypes->save();
                if($idd){
                    //notification
                    $alert_details  = $objAlerts->getRecordDetailsByID(62);

                    $alert_details['NOTIFICATION'] = str_replace('[TITLE]', $objCurrecyTypes->title, $alert_details['NOTIFICATION']);
                    $alert_details['NOTIFICATION'] = str_replace('[OPERATOR]',$sessionUserDetails['FIRST_NAME']." ".$sessionUserDetails['LAST_NAME'] , $alert_details['NOTIFICATION']);

                    foreach ($admins_for_notifications as $key => $admin_notify_id){
                        $objNotifications->notification_date   = date('Y-m-d H:i:s');
                        $objNotifications->notification_status = ($alert_details['SHOW_NOTIFICATION']=='Y')?"N":"Y";
                        $objNotifications->from_account        = $user_id;
                        $objNotifications->to_account          = $admin_notify_id;
                        $objNotifications->sc_id               = isset($sc_id)?$sc_id:0;
                        $objNotifications->notification_type   = $alert_details['ALERT'];
                        $objNotifications->title               = $alert_details['NOTIFICATION'];
                        $objNotifications->save();
                    }
                }
                exit(header('location: add-processing-type.php?id='.$idd.'&action=added'));
            }
        }

    ?>


    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p style="font-size:15px;"><b>Process Type Details</b></p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="processing-type-management.php"><div class="tab">List</div></a>
                                <a href="processing-type-management.php"><div class="tab">Search</div></a>
                                <div class="tabSelected">Details</div>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div style = "clear:both; height:20px"></div>

                <?php
                //msgs
                if(isset($_GET['action'])){
                    if($_GET['action']=='updated')  { successMessage("Process Type updated.");}
                    elseif($_GET['action']=='added'){ successMessage("Process Type added.");}
                    elseif($_GET['action']=='error'){ errorMessage("Something went wrong, record not inserted.");}
                }
                ?>

                <div id = "bodyTab1">
                    <form method = "post" action = "">
                        <input type="hidden" name="item-category" value="<?php if(isset($_GET['pid'])){ echo 'update';} else { echo 'add'; } ?>" />
                        <input type="hidden" name="pid" value="<?php if(isset($_GET['pid'])){ echo $pid;} ?>" />
                        <div id="form">
                            <div style="width:450px; float:left">
                                <div class="caption"> Process Type Name:</div>
                                <div class="field">
                                    <input class="form-control pull-left" name="title" value="<?php if(isset($name)){ echo $name;} ?>" required="required"/>
                                </div>
                                <div class="clear"></div>

                                <div class="caption"></div>
                                <div class="field">
                                    <input type="submit" name="save" value="<?php if(isset($_GET['id'])){ echo "Update"; }else{ echo "Save"; } ?>" class="button" />
                                    <input type="button" value="New" class="button" onclick="window.location.href='add-processing-type.php'";  />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div><!--form-->
                    </form>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div style = "clear:both; height:20px"></div>

        </div><!--bodyWrapper---->
    <?php
    }else{//End isset session
        exit(header('location: login.php'));
    }

?>
</body>
</html>
<?php ob_end_flush(); ?>
<?php include("common/close.con.php"); ?>
