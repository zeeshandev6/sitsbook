<?php
	include ('common/classes/settings.php');
	include 'common/classes/back-up.php';

	$objBackup = new Backup();
	$objConfigs= new Configs();

	session_start();
	error_reporting(E_ALL ^ E_DEPRECATED);

	if(isset($_POST['key'])){
		$sits     = $_POST['key'];
		if(is_file("st-config.php")){
			include 'st-config.php';
			$con = mysql_connect(DB_HOST,DB_USER,DB_PASS) or die('Invalid host name!');
			$db_selected = mysql_select_db(DB_NAME,$con)  or die('Cannot connect to Database.');
			$objConfigs->set_config($sits,'SITS');
		}
		exit();
	}

	if(isset($_POST['dump_db'])){
		if(is_file("st-config.php")){
			include 'st-config.php';
			$con = mysql_connect(DB_HOST,DB_USER,DB_PASS) or die('Invalid host name!');
			$db_selected = mysql_select_db(DB_NAME,$con)  or die('Cannot connect to Database.');
			$objBackup->restoreFileProgressBar('sits.db');
		}else{
			echo "File Not Found!";
		}
		exit();
	}

	if(isset($_POST['host'])){
		$contents = '';
		$db_host  = $_POST['host'];
		$db_name  = $_POST['db'];
		$db_user  = $_POST['username'];
		$db_pass  = $_POST['password'];

		$configs = "<?php 							     \n";
		$configs.= "      define('DB_HOST', '{DBHOST}'); \n";
		$configs.= "      define('DB_NAME', '{DBNAME}'); \n";
		$configs.= "      define('DB_USER', '{DBUSER}'); \n";
		$configs.= "      define('DB_PASS', '{DBPASS}'); \n";
		$configs.= "?>";

		$configs = str_replace("{DBHOST}", $db_host, $configs);
		$configs = str_replace("{DBNAME}", $db_name, $configs);
		$configs = str_replace("{DBUSER}", $db_user, $configs);
		$configs = str_replace("{DBPASS}", $db_pass, $configs);

		$read   = fopen("st-config.txt", 'w');
		$written = fwrite($read, $configs);
		fclose($read);
		rename('st-config.txt', 'st-config.php');
		chmod('st-config.php', 0777);
		include 'st-config.php';
		$con = mysql_connect(DB_HOST,DB_USER,DB_PASS) or die('Invalid Host Name! <br />');
		$db_selected = mysql_select_db(DB_NAME,$con)  or die('Cannot connect to Database.');
		exit();
	}
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SITSBOOK Installation</title>
    <link rel="stylesheet" href="resource/css/visit.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<style type="text/css">
		.form-control{
			font-size:16px;
			font-weight: bold;
		}
	</style>
	<script type="text/javascript">
		$(function(){
<?php
		if(!isset($_POST['key'])){
?>
			$(".steps").hide();
<?php
		}
?>
			$(".curren-step").val(1);
			$(".step_back").hide();
		});
		$(document).ready(function(){
			 $('input[name=db]').tooltip();
			$("input[name=host]").focus();
			$(".the-form").slideDown();
			$(".steps#step-1").slideDown();

			$("button.submit").click(function(){
				next_step();
				$(".step_back").show();
			});
			$(".step_back").click(function(){
				prev_step();
			});
			$("button.submit2").click(function(){
				set_key();
			});
			$(".dump_it").click(function(){
				$(".steps").hide();
				dump_db();
			});
		});
		var next_step = function(){
			var current_step = parseInt($(".curren-step").val())||0;
			$(".steps").hide();
			$(".alert-danger,.alert-success").hide('slide');
			++current_step;
			$(".curren-step").val(current_step);
			if(current_step == 3){
				hostcheck(current_step);
			}else if(current_step == 4){
				$("#step-"+current_step).show();
				$("button.submit").hide();
				$(".submit2").show();
			}else{
				$("#step-"+current_step).show();
			}
		}
		var prev_step = function(){
			var current_step = parseInt($(".curren-step").val())||0;
			if(current_step > 1){
				$(".steps").hide();
				$(".alert-danger,.alert-success").hide('slide');
				--current_step;
				$("#step-"+current_step).show();
				$(".curren-step").val(current_step);
				if(current_step == 1){
					$(".step_back").hide();
				}
			}
			if(current_step < 4){
				$("button.submit").show();
				$("button.submit2").hide();
			}
		}
		var hostcheck = function(current_step){
			var host 	 = $("input[name=host]").val();
			var db 		 = $("input[name=db]").val();
			var username = $("input[name=username]").val();
			var password = $("input[name=password]").val();

			$.post("<?php echo basename($_SERVER['PHP_SELF']); ?>",{host:host,db:db,username:username,password:password},function(data){
				if(data != ''){
					$(".alert-danger").show('slide');
					$(".alert-danger span").text(data);
				}else{
					$(".alert-success").show('slide');
					$(".alert-success span").text("Connected to database.");
					$("#step-"+current_step).show();
				}
				$("button.submit").hide();
			});
		}

		var dump_db = function(){
			$("div.loading").show('slide');
			$.post("<?php echo basename($_SERVER['PHP_SELF']); ?>",{dump_db:'Y'},function(data){
				$("div.loading").hide('slide');
				if(data == ''){
					$(".alert-success").show('slide');
					$(".alert-success span").html("Database Created successfully!");
					$("button.submit").show();
				}else{
					$(".alert-danger").show('slide');
					$(".alert-danger span").html(data);
				}
			});
		}

		var set_key = function(){
			var key = $("input[name=key]").val();
			$.post("<?php echo basename($_SERVER['PHP_SELF']); ?>",{key:key},function(data){
				if(data == ''){
					$(".alert-success").show('slide');
					$(".alert-success span").html("Product key successfully set.");
					$(".steps").hide();
					$(".step_back").hide();
					$(".gotologin").show();
					$(".submit").hide();
					$(".submit2").hide();
				}else{
					$(".alert-danger").show('slide');
					$(".alert-danger span").text(data);
				}
			});
		}
	</script>
</head>
<body>
	<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-7" style="float:none;margin:0px auto;">
				<div style="height:40px;"></div>
				<div class="panel panel-default">
					<div class="company-logo"><img src="resource/images/sits-book.png" /></div>
					<hr />
					<a class="pull-right" style="padding:0px 30px;font-weight:bold;" href="http://sitsbook.com/how-to-install-sitsbook.php" target="_blank" > How To Install ?</a>
					<div class="clear"></div>
					<div class="panel-body the-form" style="display:none;">
					 <div class="alert alert-danger" style="display:none;">
					 	<i class="fa fa-exclamation-circle" style="font-size:16px;"></i>
					 	<span>Error! Can not connect to host.</span>
					 </div>
					 <div class="alert alert-success" style="display:none;">
					 	<i class="fa fa-check" style="font-size:16px;"></i>
					 	<b><span>Error! Can not connect to host.</span></b>
					 </div>
						<div class="loading">
							<div class="spinner">
							  <div class="rect1"></div>
							  <div class="rect2"></div>
							  <div class="rect3"></div>
							  <div class="rect4"></div>
							  <div class="rect5"></div>
							</div>
						</div>
						<div class="steps" id="step-1">
							<h3>Step 1: Host Name &amp; Database Information</h3>
							<div class="col-xs-12 field-group">
								<label class="caption">Host : <span class="info-mini">e.g localhost</span> </label>
								<input class="form-control field" value="" type="text" name="host" required />
								<div class="clear"></div>
							</div>

							<div class="col-xs-12 field-group">
								<label class="caption">Database : <span class="info-mini">e.g sale_purchase</span> </label>
								<input class="form-control field" data-toggle="tooltip" data-trigger="focus" data-placement="bottom" title="Please Create Empty database before proceeding." value="" type="text" name="db" required />
								<div class="clear"></div>
							</div>
						</div>
						<div class="steps" id="step-2">
							<h3>Step 2: Database Login Information</h3>
							<div class="col-xs-12 field-group">
								<label class="caption">User Name : <span class="info-mini">e.g root</span> </label>
								<input class="form-control field" value="" type="text" name="username" required />
								<div class="clear"></div>
							</div>
							<div class="col-xs-12 field-group">
								<label class="caption">Password :</label>
								<input class="form-control field" value="" type="text" name="password" />
								<div class="clear"></div>
							</div>
						</div>
						<div class="steps" id="step-3">
							<h3>Step 3: Database Creation Process</h3>
							<div class="col-xs-12 field-group text-center">
								<button class="dump_it button">Create Database</button>
							</div>
						</div>
						<div class="steps" id="step-4">
							<h3>Step 4: Please Enter Your Produt Key</h3>
							<div class="col-xs-12 field-group">
								<label class="caption">Product Key : <span class="info-mini">Please enter your product key containing 64 charactors.</span></label>
								<input class="form-control field" value="" type="text" name="key" required />
								<div class="clear"></div>
							</div>
						</div>
						<input type="hidden" class="curren-step" value="1" />
						<div class="clear"></div>
						<hr />
						<div class="col-xs-12">
							<button type="button" class="btn pull-left btn-default step_back"><i class="fa fa-arrow-left"></i> Prev</button>
							<button type="button" class="btn pull-right btn-default submit">Next <i class="fa fa-arrow-right"></i></button>
							<button type="button" style="display:none;" class="btn pull-right btn-default submit2">Done</button>
							<button style="display:none;" class="gotologin button" onclick="window.location.href = 'login.php'">Login</button>
						</div>
					</div>
				</div>
			</div>
	</div>
</body>
</html>
<?php include('conn.close.php'); ?>
