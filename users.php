<?php 
	include 'common/connection.php';
	include 'common/config.php';
?>
<!DOCTYPE html 
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <style>
          a.poplight{
            color:#666;
          }
		    </style>
      	<!-- jQuery -->
      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>

   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
         	<div id = "bodyWrapper">
            	<div class="content-box-top"  style="padding-bottom: 30px;">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >User Management</p>
                  		</div><!-- End .content-box-header -->
               			<div style=" clear:both;"></div>
                        <a href="users-management.php">
                            <div class="panel1">
                                <img src="resource/images/MedIcons/users.png" class="panelIcon" />
                                <h3>Users List</h3>
                            </div>
                        </a>
                        <a href="change-password.php">
                            <div class="panel1">
                                <img src="resource/images/MedIcons/safe.png" class="panelIcon" />
                                <h3>Change Password</h3>
                            </div>
                        </a>
        				<div style=" clear:both;"></div>
                        
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
	</body>
</html>
<?php include('conn.close.php'); ?>