<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/mobile-sale.php');
	include('common/classes/mobile-sale-details.php');
	include('common/classes/mobile-purchase-details.php');
	include('common/classes/items.php');
	include('common/classes/customers.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('mobile-sale-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes        = new ChartOfAccounts;
	$objScanSale            = new ScanSale;
	$objScanSaleDetails     = new ScanSaleDetails;
	$objScanPurchaseDetails = new ScanPurchaseDetails;
	$objItems               = new Items;
	$objItemCategory        = new itemCategory;
	$objCustomers		    = new Customers;
	$objConfigs 		    = new Configs();

	$customersList   = $objCustomers->getList();
	$item_categories = $objItems->getCategoryListFull();
	$cashAccounts  = $objAccountCodes->getAccountByCatAccCode('010101');

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	$titleRepo = '';

	if(isset($_POST['search'])){
		$objScanSale->fromDate 		= ($_POST['fromDate']=='')?"":date('Y-m-d',strtotime($_POST['fromDate']));
		$objScanSale->toDate   		= ($_POST['toDate']=='')?"":date('Y-m-d',strtotime($_POST['toDate']));
		$objScanSale->item_id     	= $_POST['item'];
		$objScanSale->cust_acc_code = $_POST['supplier'];

		$state = $_POST['state'];

		$current_user  = $user_id;
		if($current_user==1){
			$objScanSale->user_id = $_POST['user_id'];
		}else{
			$objScanSale->user_id = $current_user;
		}

		if($objScanSale->fromDate == ''){
			$thisMonthYear = date('m-Y');
			$beginThisMonth = '01';
			$startThisMonth = date('Y-m-d',strtotime($beginThisMonth."-".$thisMonthYear));
			$objScanSale->fromDate = $startThisMonth;
		}
		$purchaseReport = $objScanSale->report();
	}
	$users_list = $objAccounts->getActiveList();
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
			$(".printThis").click(function(){
				var MaxHeight = 884;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){

					if(PageNo == 1){
						MaxHeight = 750;
					}

					if(PageNo != 1){
						MaxHeight = 820;
					}

					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){

					if(i == 1){
						MaxHeight = 750;
					}

					if(i != 1){
						MaxHeight = 820;
					}

					$('table.tableBreak').parent().append("<div class='tablePage' style='height:"+MaxHeight+"px'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\">"+repoDate+"</p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/reports.css',
				  pageTitle: "Sitsbook.com",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('select').selectpicker();
		});
    </script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Sale Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="post" action="mobile-sale-report.php">
                        <div class="caption">From Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="fromDate" value="" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

                        <div class="caption">To Date </div>
                        <div class="field" style="width:300px;">
                        	<input type="text" name="toDate" value="<?php echo date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px;" />
                        </div><!--field-->
                        <div class="clear"></div>

<?php
                        if($admin){
?>
                        <div class="caption">User Name</div>
                        <div class="field" style="width:300px;position:relative;">
                                <select class="user_id form-control "
                                		name="user_id"
                                        data-style="btn-default"
                                        data-live-search="true" style="border:none" >
                                        <option selected value=""></option>
<?php
                            if(mysql_num_rows($users_list)){
                                while($account = mysql_fetch_array($users_list)){
?>
                                   <option value="<?php echo $account['ID']; ?>" ><?php echo $account['FIRST_NAME']." ".$account['LAST_NAME']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                        </div>
                        <div class="clear"></div>
<?php
						}
?>

                        <div class="caption">Account</div>
                        <div class="field" style="width:300px;position:relative;">
                                <select class="supplierSelector form-control "
                                		name="supplier"
                                        data-style="btn-default"
                                        data-live-search="true" style="border:none" >
                                        <option selected value=""></option>
<?php
                        if(mysql_num_rows($cashAccounts)){
                            while($cash_ina_hand = mysql_fetch_array($cashAccounts)){
?>
                               <option data-subtext="<?php echo $cash_ina_hand['ACC_CODE']; ?>" value="<?php echo $cash_ina_hand['ACC_CODE']; ?>" ><?php echo $cash_ina_hand['ACC_TITLE']; ?></option>
<?php
                            }
                        }
?>
<?php
                            if(mysql_num_rows($customersList)){
                                while($account = mysql_fetch_array($customersList)){
                                    $selected = (isset($inventory)&&$inventory['CUST_ACC_CODE']==$account['CUST_ACC_CODE'])?"selected=\"selected\"":"";
?>
                                   <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
    <?php
                                }
                            }
    ?>
                                </select>
                        </div>
                        <div class="clear"></div>
                        <div class="caption">Item </div>
                        <div class="field" style="width:300px;">
                        <select class="itemSelector show-tick form-control"
                        	name="item"
                            data-style="btn-default"
                            data-live-search="true" style="border:none">
        	               <option selected value=""></option>
<?php
					if(mysql_num_rows($item_categories)){
					    while($category = mysql_fetch_array($item_categories)){
							$itemList = $objItems->getListByCategory($category['ITEM_CATG_ID']);
							$category_name = $objItemCategory->getTitle($category['ITEM_CATG_ID']);
?>
							<optgroup label="<?php echo $category_name; ?>">
<?php
						if(mysql_num_rows($itemList)){
							while($theItem = mysql_fetch_array($itemList)){
								if($theItem['ACTIVE'] == 'N' || $theItem['INV_TYPE'] != 'B'){
									continue;
								}
?>
	       						<option value="<?php echo $theItem['ID']; ?>" data-subtext="<?php echo $theItem['COMPANY']; ?>" ><?php echo $theItem['NAME']; ?></option>
<?php
							}
						}
?>
							</optgroup>
<?php
    				}
}
?>
                    		</select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"> Status </div>
                        <div class="field">
                        	<select name="state">
                        		<option value=""></option>
                        		<option value="S">Sold</option>
                        		<option value="A">Returned</option>
                        	</select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                    <hr />

<?php
					if(isset($purchaseReport)){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;">
                    	<div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                        	<p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Sale Report</p>
                            <p style="font-size:16px;text-align:left;padding: 0px;">
                            	<?php echo ($objScanSale->fromDate=="")?"":"From ".date('d-m-Y',strtotime($objScanSale->fromDate)); ?>
                                <?php echo ($objScanSale->toDate=="")?" To ".date('d-m-Y'):"To ".date('d-m-Y',strtotime($objScanSale->toDate)); ?>
                            </p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                    	</div>
<?php
						if(mysql_num_rows($purchaseReport)){
?>
                        <table class="tableBreak">
                            <thead class="tHeader">
                                <tr style="background:#EEE;">
                                	<th width="5%" style="font-size:12px !important ;text-align:center">Bill No</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Sale Date</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">User</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Account</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Barcode</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Item</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Price</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Discount</th>
									<th width="10%" style="font-size:12px !important ;text-align:center">Tax</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Amount(<?php echo $currency_type; ?>)</th>
                                	<th width="10%" style="font-size:12px !important ;text-align:center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
								$total_amount = 0;
								$prev_bill_no = 0;
								while($row = mysql_fetch_assoc($purchaseReport)){
									$pd_row = $objScanPurchaseDetails->getDetails($row['SP_DETAIL_ID']);
									$status = $pd_row['STOCK_STATUS'];
									if($state != ""){
										if($status != $state){
											continue;
										}
									}
									$supplierTitle = $objAccountCodes->getAccountTitleByCode($row['CUST_ACC_CODE']);
									$item = $objItems->getRecordDetails($row['ITEM_ID']);
									if($status=='R'){
										$status = 'Retruned';
									}elseif($status=='S'){
										$status = 'Sold';
									}else{
										$status = 'Retruned to Stock';
									}
									$u_full_name = $objAccounts->getFullName($row['USER_ID']);
									if($row['BILL_NO'] != $prev_bill_no){
										$this_bill = $row['BILL_NO'];
									}else{
										$this_bill = '';
									}

									$returned = $objScanSaleDetails->checkForReturnedAgainstCurrentBill($row['MSDID']);
									if($returned){
										$status = "Returned";
									}
?>
	                                <tr id="recordPanel" class="alt-row">
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo $this_bill; ?></td>
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo date('d-m-Y',strtotime($row['SALE_DATE'])); ?></td>
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo $u_full_name['FIRST_NAME']; ?></td>
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo $supplierTitle; ?></td>
	                                	<td style="text-align:center;font-size:10px !important;"><?php echo $pd_row['BARCODE']; ?></td>
	                                    <td style="text-align:left;font-size:10px !important;"><?php echo $item['NAME']; ?><?php echo ($pd_row['COLOUR']=='')?"":"-".$pd_row['COLOUR']; ?></td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php echo number_format($row['SALE_PRICE'],2); ?></td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php echo number_format($row['CDISCOUNT'],2); ?> %</td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php echo number_format($row['CTAX'],2); ?> %</td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php echo number_format($row['customer_price'],2); ?></td>
	                                    <td style="text-align:center;font-size:10px !important;"><?php echo $status; ?></td>
	                                </tr>
<?php
									$total_amount += $row['customer_price'];
									$prev_bill_no = $row['BILL_NO'];
								}
?>

                            </tbody>
<?php
						}//end if
						if(mysql_num_rows($purchaseReport)){
?>
                    	<tfoot class="tableFooter">
                            <tr>
                                <td style="text-align:right;" colspan="9">Total:</td>
                                <td style="text-align:center;"> <?php echo number_format($total_amount,2); ?> </td>
                                <td></td>
                            </tr>
                        </tfoot>
<?php
						}
?>
                    </table>
                    <div class="clear"></div>
                	</div> <!--End bodyTab-->
<?php
					} //end if is generic report
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
