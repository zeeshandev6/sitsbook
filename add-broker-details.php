<?php
	ob_start();

	include ('common/connection.php');
	include ('common/config.php');
	include ('common/classes/accounts.php');
	include ('common/classes/brokers.php');

	$objBroker        = new brokers();
	$objAccountCodes  = new ChartOfAccounts();

	$bid = 0;
	//get one record by id
	if(isset($_GET['bid'])){
		if(!empty($_GET['bid'])){
			$bid = $_GET['bid'];
			$array = $objBroker->getRecordDetailsByID($bid);

			$broker_account_code = $array['ACCOUNT_CODE'];
			$account_title 		 	 = $array['ACCOUNT_TITLE'];
			$first_name 		 		 = $array['FIRST_NAME'];
			$last_name   		 		 = $array['LAST_NAME'];
			$email       		 		 = $array['EMAILS'];
			$mobile      		 		 = $array['MOBILES'];
			$phone       		 		 = $array['PHONES'];
			$city        		 		 = $array['CITY'];
			$address     		 		 = $array['ADDRESS'];
			$broker_locale 	 		 = $array['BROKER_LOCALE'];
			$country     		 		 = $array['COUNTRY'];
			$postcode    		 		 = $array['POSTCODE'];
		}
	}

	if(isset($_POST['account_title'])){
		$bid 														= (int)(mysql_real_escape_string($_POST['bid']));
	  $objAccountCodes->account_code  = (isset($_POST['ac_code']))?mysql_real_escape_string(ucfirst($_POST['ac_code'])):"";
		$objAccountCodes->account_title = mysql_real_escape_string(ucfirst($_POST['account_title']));

		if($bid == 0){
			$objAccountCodes->addSubMainChild($objAccountCodes->account_title,'040102');
		}else{
			$objAccountCodes->updateTitleByCode($objAccountCodes->account_code,$objAccountCodes->account_title);
		}

		$objBroker->account_code   = mysql_real_escape_string($objAccountCodes->account_code);
		$objBroker->account_title  = mysql_real_escape_string($objAccountCodes->account_title);
		$objBroker->first_name     = mysql_real_escape_string(ucfirst($_POST['first_name']));
		$objBroker->last_name      = mysql_real_escape_string(ucfirst($_POST['last_name']));
		$objBroker->email          = mysql_real_escape_string($_POST['email']);
		$objBroker->mobiles        = mysql_real_escape_string($_POST['mobiles']);
		$objBroker->phones         = mysql_real_escape_string($_POST['telephone_number']);
		$objBroker->city           = mysql_real_escape_string($_POST['city']);
		$objBroker->address        = mysql_real_escape_string($_POST['address']);
		$objBroker->broker_locale  = mysql_real_escape_string($_POST['broker_locale']);
		$objBroker->country        = mysql_real_escape_string($_POST['country']);
		$objBroker->postcode       = mysql_real_escape_string($_POST['post_code']);

		if($bid > 0){
			$objBroker->update($bid);
		}else{
			$bid = $objBroker->save();
		}
		exit(header('location: add-broker-details.php?bid='.$bid.'&action=added'));
		exit();
	}
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
		<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".form-control").keydown(function(e){
					if(e.keyCode == 13){
						e.preventDefault();
					}
				});
				$("select").selectpicker();
			});
		</script>
	</head>
	<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<div id="bodyWrapper">
		<div class="content-box-top" style="overflow:visible;">
			<div class="summery_body">
				<div class = "content-box-header">
					<p style="font-size:15px;"><b>Broker/Agent Details</b></p>
						<span id="tabPanel">
							<div class="tabPanel">
								<a href="brokers-management.php"><div class="tab">List</div></a>
								<a href="brokers-management.php?tab=search"><div class="tab">Search</div></a>
								<div class="tabSelected">Details</div>
							</div>
						</span>
					<div style = "clear:both;"></div>
				</div><!-- End .content-box-header -->
				<div style = "clear:both; height:20px"></div>

				<?php
				if(isset($_GET['action'])){
					if($_GET['action']=='updated'){
						?>
						<div id="msg" class="alert alert-success">
							<button type="button" class="close">&times;</button>
							<span style="font-weight: bold;">Success!</span> Broker updated.
						</div>
					<?php
					}elseif($_GET['action']=='added'){
						?>
						<div id="msg" class="alert alert-success">
							<button type="button" class="close">&times;</button>
							<span style="font-weight: bold;">Success!</span> Broker added.
						</div>
					<?php
					}
				}
				?>
				<div id="bodyTab1">
					<form method="post" action="" name="cust" class="form-horizontal">
						<input type="hidden" name="bid"    value="<?php if(isset($_GET['bid'])){ echo $bid;} ?>" />
						<div class="col-xs-12">
							<div class="form-group">
								<label class="control-label col-sm-2">Broker Type</label>
								<div class="col-sm-10">
									<select  class="form-control" name="broker_locale" style="width: 259px;" required="required">
										<option value="L" <?php echo (isset($broker_locale)&&$broker_locale=='L')?"selected":""; ?> >Local</option>
										<option value="F" <?php echo (isset($broker_locale)&&$broker_locale=='F')?"selected":""; ?> >Foreign</option>
									</select>
								</div>
							</div>
							<?php if((isset($broker_account_code))){ ?>
							<div class="form-group">
								<label class="control-label col-sm-2">Account Code</label>
								<div class="col-sm-10">
									<input class="form-control" value="<?php if(isset($broker_account_code)){ echo $broker_account_code;} ?>" name="ac_code" readonly />
								</div>
							</div>
							<?php } ?>

							<div class="form-group">
								<label class="control-label col-sm-2">Account Title</label>
								<div class="col-sm-10">
									<input class="form-control" name="account_title" value="<?php if(isset($account_title)){ echo $account_title;} ?>" required="required"/>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="control-label col-sm-2">First Name</label>
								<div class="col-sm-10">
									<input class="form-control" name="first_name" value="<?php if(isset($first_name)){ echo $first_name;} ?>"/>
									<input type="hidden" name="broker_code" value="<?php echo (isset($broker_account_code))?$broker_account_code:"" ?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2">Last Name</label>
								<div class="col-sm-10">
									<input class="form-control" name="last_name" value="<?php if(isset($last_name)){ echo $last_name;} ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Mobile</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="mobiles" value="<?php if(isset($mobile)){ echo $mobile;} ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Phone </label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="telephone_number" value="<?php if(isset($phone)){ echo $phone;} ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Email </label>
								<div class="col-sm-10">
									<input class="form-control" type="email" name="email" value="<?php if(isset($email)){ echo $email;}  ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Post Code </label>
								<div class="col-sm-10">
									<input class="form-control" type="text" name="post_code" value="<?php if(isset($postcode)){ echo $postcode;}  ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Country </label>
								<div class="col-sm-10">
									<input class="form-control" name="country" value="<?php if(isset($country)){ echo $country;}  ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">City </label>
								<div class="col-sm-10">
									<input class="form-control" name="city" value="<?php if(isset($city)){ echo $city;}  ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2">Address</label>
								<div class="col-sm-10">
									<textarea class="form-control" style="height:100px;" name="address"><?php if(isset($address)){ echo $address;}  ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2"></label>
								<div class="col-sm-10">
									<input type="submit" name="save" value="<?php echo ($bid>0)?"Update":"Save"; ?>" class="btn btn-primary pull-left" />
									<button class="btn btn-default pull-right" name="broker" value="" >Add New</button>
								</div>
							</div>
						</div><!--form-->
					</form>
				</div><!--bodyTab1-->
			</div><!--summery_body-->
		</div><!--content-box-top-->
		<div style = "clear:both; height:20px"></div>
	</div><!--bodyWrapper-->
	</body>
	</html>

<?php ob_end_flush(); ?>
<?php include("conn.close.php"); ?>
