<?php
	ob_start();
	include ('common/connection.php');
	include ('common/config.php');
    include ('common/classes/demand_list.php');
    include ('common/classes/demand_list_detail.php');
    include ('common/classes/items.php');
    include ('common/classes/itemCategory.php');
    include ('common/classes/company_details.php');

    $objCompanyDetails 	 = new CompanyDetails();
    $objDemandList       = new DemandList();
    $objDemandListDetail = new DemandListDetail();
    $objItems            = new Items();
    $objItemCategory     = new ItemCategory();

	if(isset($_GET['id'])){
        $id = (int)($_GET['id']);
        $demand_details   = $objDemandList->getDetail($id);
        $demand_item_list = $objDemandListDetail->getListByDemand($id);
    }
?>
<!DOCTYPE html   >

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Demand List Print</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css"  />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/invoiceStyle.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/invoiceStyle.css",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100% 100%;background-repeat: n-repeat;background-attachment: fixed;">
<?php
	if(isset($id)){
		$companyLogo   = $objCompanyDetails->getLogo('1');
		$company       = $objCompanyDetails->getRecordDetails('1'); 
?>

<div class="invoiceBody invoiceReady">
	<div class="header">
    	<div class="headerWrapper">
            <button class="btn btn-info printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print View </button>
        </div><!--headerWrapper-->
    </div><!--header-->
    <div class="clear"></div>
    <div class="printThisDiv" style="position: relative;height:9.7in;">
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint" style="height:10.6in !important;">
            <div class="invoiceHead" style="width: 100%;margin: 0px auto;">
<?php
				if($companyLogo != ''){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
            		<div style="font-size:26px;"> <?php echo $company['NAME']; ?> </div> 
                    <?php echo $company['ADDRESS']; ?>
                    <br />
                    Contact : <?php echo $company['CONTACT']; ?>
                    <br />
                    Email : <?php echo $company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
                     PURCHASE ORDERING REQUISITION
                </div>

                <div class="clear" style="height: 5px;"></div>
                <div class="infoPanel pull-left" style="height:auto !important;">
                    <b>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">PO # : <?php echo $demand_details['PO_NUMBER']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Record Date : <?php echo date('d-m-Y',strtotime($demand_details['ENTRY_DATE'])); ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Availability Date : <?php echo date('d-m-Y',strtotime($demand_details['REQUIRED_DATE'])); ?></span>
                	<div class="clear"></div>
                    </b>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right" style="width:200px;height:auto !important;">
                    <b>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"></span>
                	<div class="clear"></div>
	                <span class="pull-left" style="padding:0px 0px;font-size:14px;"></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"></span>
                	<div class="clear"></div>
                    </b>
                </div><!--partyTitle-->
				<div class="clear"></div>
            </div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 100%;margin: 0px auto;">
                <table>
                    <thead>
                        <tr>
                        	<th width="5%">Sr#</th>
                            <th width="40%">Particulars</th>
                            <th width="10%">Quantity Order</th>
                            <th width="10%">Unit Rate</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
						$quantity    = 0;
						$subAmount   = 0;
						$taxAmount   = 0;
						$totalAmount = 0;
						$counter = 1;
						if(mysql_num_rows($demand_item_list)){
							while($row = mysql_fetch_array($demand_item_list)){
                                $item_name = $objItems->getItemTitle($row['ITEM_ID']);
?>
								<tr>
                                	<td class="text-center"><?php echo $counter; ?></td>
                                    <td class="text-left"><?php echo $item_name; ?></td>
									<td class="text-center"><?php echo $row['QUANTITY']; ?></td>
									<td class="text-center"><?php echo $row['UNIT_RATE']; ?></td>
								</tr>
<?php
									$quantity    += $row['QUANTITY'];
									$counter++;
							}
						}
?>
                    </tbody>
                    <tfoot>
                        <tr>
                        	<td colspan="2" style="text-align: right;"><span style="float:right;margin-right:15px;"> Total </span></td>
                        	<td style="text-align: right;padding-right: 10px;">
                        		<?php echo $quantity; ?>
                        	</td>
                            <td class="text-center">- - -</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>

                <div class="invoice_footer" style="position:absolute !important;bottom:5px !important;left:0px;">
	                <div class="auths pull-left">
	                	Prepared By
	                	<span class="authorized">____________</span>
	                </div><!--partyTitle-->
	                <div class="auths pull-right">
	                	Authorized By
                        <span class="authorized">____________</span>
	                </div><!--partyTitle-->
	                <div class="clear" style="height: 10px;"></div>
	                <div class="invoice-developer-info text-right pr-10"> Designed &amp; Developed By <b>SIT SOLUTIONS</b></div>
	        	</div>
            </div><!--invoiceBody-->
        </div><!--invoiceLeftPrint-->
        <div class="clear"></div>
    </div><!--invoiceContainer-->
    </div><!--printThisDiv-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php if(isset($ob_var)){ ob_end_flush($ob_var); } ?>
<?php include("conn.close.php"); ?>