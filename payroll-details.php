<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/payrolls.php');
	include('common/classes/accounts.php');

	if(isset($permissionDetails)&&!in_array('customers-management',$permissionDetails)&&$admin == false){
		header( 'location:dashboard.php' );
	}
	$objPayrolls 	 = new Payrolls();
	$objAccountCodes = new ChartOfAccounts();

	if(isset($_POST['addEmployee'])){
		$objPayrolls->account_title = mysql_real_escape_string($_POST['title']);
		$objAccountCodes->addSubMainChild($objPayrolls->account_title,'030105');

		if($objAccountCodes->new_account_code!=''){
			$objPayrolls->account_code 		= mysql_real_escape_string($objAccountCodes->new_account_code);
			$objPayrolls->contact_person 	= mysql_real_escape_string($_POST['contact_person']);
			$objPayrolls->wage_type 			= mysql_real_escape_string($_POST['wage_type']);
			$objPayrolls->wage_amount 		= mysql_real_escape_string($_POST['wage_amount']);
			$objPayrolls->email 					= mysql_real_escape_string($_POST['email']);
			$objPayrolls->mobile 					= mysql_real_escape_string($_POST['mobile']);
			$objPayrolls->phones 					= mysql_real_escape_string($_POST['phones']);
			$objPayrolls->city 						= mysql_real_escape_string($_POST['city']);
			$objPayrolls->address 				= mysql_real_escape_string($_POST['address']);

			$inserted 										= $objPayrolls->save();
		}else{
			$inserted = false;
		}
		if($inserted){
			$insert_id = mysql_insert_id();
			echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$insert_id."&added');</script>";
			exit();
		}else{
			$message = 'Error! Employee Detail Could Not Be Saved.';
		}
	}
	if(isset($_POST['update'])){
		$customerId 				= (int)mysql_real_escape_string($_POST['customer_id']);
		$objPayrolls->account_title = mysql_real_escape_string($_POST['title']);
		if($objPayrolls->account_title == ''){
			$customerId = 0;
		}
		$objPayrolls->account_code 		= mysql_real_escape_string($_POST['code']);
		$objPayrolls->contact_person 	= mysql_real_escape_string($_POST['contact_person']);
		$objPayrolls->wage_type 			= mysql_real_escape_string($_POST['wage_type']);
		$objPayrolls->wage_amount 		= mysql_real_escape_string($_POST['wage_amount']);
		$objPayrolls->email 					= mysql_real_escape_string($_POST['email']);
		$objPayrolls->mobile 					= mysql_real_escape_string($_POST['mobile']);
		$objPayrolls->phones 					= mysql_real_escape_string($_POST['phones']);
		$objPayrolls->city 						= mysql_real_escape_string($_POST['city']);
		$objPayrolls->address 				= mysql_real_escape_string($_POST['address']);
		if($customerId && $objPayrolls->account_title != ''){
			$customer_details = mysql_fetch_array($objPayrolls->getRecordDetails($customerId));
			$objAccountCodes->updateTitleByCode($customer_details['CUST_ACC_CODE'],$objPayrolls->account_title);
			$updated = $objPayrolls->update($customerId);
			if($updated){
				echo "<script>window.location.replace('".$_SERVER['PHP_SELF']."?cid=".$customerId."&updated');</script>";
				exit();
			}else{
				$message = 'Error! Employee Detail Could Not Be Saved.';
			}
		}else{
			$message = 'Error! Employee Detail Could Not Be Saved.';
		}
	}

	if(isset($_GET['cid'])){
		$did 			= (int)mysql_real_escape_string($_GET['cid']);
		$customerDetail = $objPayrolls->getRecordDetails($did);
		$customerRow 	= mysql_fetch_array($customerDetail);
	}
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
					$(document).ready(function(){
		        $("input[name='wage_amount']").numericOnly();
		        $("input[name='mobile']").numericOnly();
		        $("input[name='phones']").numericOnly();

						$(".insert_title").focus();
		<?php
							if(isset($message)){
		?>
								displayMessage('<?php echo $message; ?>');
		<?php
							}
		?>
					});
				</script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>

   <body>
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Payrolls Management</p>
                  		<span id = "tabPanel">
                     		<div class = "tabPanel">
                        		<a href="payroll-management.php?tab=list" ><div class="tab">List</div></a>
                                <a href="payroll-management.php?tab=search" ><div class="tab">Search</div></a>
								<div class = "tabSelected">Details</div>
                     		</div>
                  		</span>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
	               	<div class="clear"></div>
               		<div id = "bodyTab1">
                  		<div id = "form">
                     	<form method="post" action="" class="form-horizontal" >
<?php
							if(isset($customerRow)){
?>
														<div class="form-group">
															<label class="control-label col-sm-2">Account Code</label>
															<div class="col-sm-10">
																<input name="code" value="<?php echo (isset($customerRow))?$customerRow['CUST_ACC_CODE']:""; ?>" type="text" readonly="readonly" class="form-control" style="border:1px dashed #CCC;" />
															</div>
														</div>
<?php
							}
?>
														<div class="form-group">
															<label class="control-label col-sm-2">Account Title</label>
															<div class="col-sm-10">
																<input name="title" value="<?php echo (isset($customerRow))?$customerRow['CUST_ACC_TITLE']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<hr />

														<div class="form-group">
															<label class="control-label col-sm-2">Designation</label>
															<div class="col-sm-10">
																<input name="contact_person" value="<?php echo (isset($customerRow))?$customerRow['CONT_PERSON']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Salary Type</label>
															<div class="col-sm-10">
																<input name="wage_type" value="<?php echo (isset($customerRow))?$customerRow['WAGE_TYPE']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Amount</label>
															<div class="col-sm-10">
																<input name="wage_amount" value="<?php echo (isset($customerRow))?$customerRow['WAGE_AMOUNT']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<hr />

														<div class="form-group">
															<label class="control-label col-sm-2">Email</label>
															<div class="col-sm-10">
																<input name="email"  value="<?php echo (isset($customerRow))?$customerRow['EMAIL']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Mobile</label>
															<div class="col-sm-10">
																<input name="mobile" value="<?php echo (isset($customerRow))?$customerRow['MOBILE']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Telephones</label>
															<div class="col-sm-10">
																<input name="phones" value="<?php echo (isset($customerRow))?$customerRow['PHONES']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">Address</label>
															<div class="col-sm-10">
																<textarea class="textarea_fieldz" name="address"><?php echo (isset($customerRow))?$customerRow['ADDRESS']:""; ?></textarea>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2">City</label>
															<div class="col-sm-10">
																<input name="city" value="<?php echo (isset($customerRow))?$customerRow['CITY']:""; ?>" type="text" class="form-control" />
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-sm-2"></label>
															<div class="col-sm-10">
<?php
															if(isset($customerRow)){
?>
						   							 		<input type="hidden" name="customer_id" value="<?php echo $customerRow['CUST_ID']; ?>"  />
                             		<input name="update" type="submit" value="Update" class="btn btn-primary" />
                             		<input type="button" value="Cancel" class="btn btn-default pull-right" onclick="window.location.replace('payroll-management.php');" />
<?php
						}else{
?>
                             		<input name="addEmployee" type="submit" value="Save" class="btn btn-primary" />
                             		<input type="reset" value="Reset" class="btn btn-default pull-right ml-10" />
                             		<input type="button" value="Cancel" class="btn btn-default pull-right" onclick="window.location.replace('payroll-management.php');" />
<?php
						}
?>
															</div>
														</div>
                     	</form>
                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab1-->
            	</div><!-- End summery_body -->
         	</div><!-- End content-box -->
      	</div><!--End body-wrapper-->
        <div id="xfade"></div>
   	</body>
</html>
<?php include('conn.close.php'); ?>
