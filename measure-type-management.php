<?php
  include('common/connection.php');
  include('common/config.php');
  include('common/classes/measure-type.php');

  $objMeasure_Type  = new MeasureType();

  if(isset($_POST['delete_id'])){
      $delete_id =  mysql_real_escape_string($_POST['delete_id']);
      $name = $objMeasure_Type->getName($delete_id);
      $deleted = $objMeasure_Type->delete($delete_id);
      exit();
  }

    $mode = "";
    if(!isset($_GET['search']))
    {
        $path = "measure-type-management.php?title=&status=&search=Search";
        if(isset($_GET['tab'])){
            $path .= "&tab=search";
        }
        echo "<script language='javascript'>window.location.href = '".$path."'</script>";
        exit();
    }

    if(isset($_GET['search'])){
        $end = 20;

        $objMeasure_Type->title    = $_GET['title'];
        $objMeasure_Type->status   = $_GET['status'];

        $myPage = 1;
        $start = 0;
        if(isset($_GET['page']))
        {
            $page = $_GET['page'];
            $myPage = $page;
            $start = ((int)$page * $end) - $end;
        }
        $measure_typeList = $objMeasure_Type->searchMeasure_Type($start,$end);
        $row          = mysql_num_rows($measure_typeList);

        $queryCount = $objMeasure_Type->totalMatchRecords;
        $pageCount = ceil($queryCount/$end);
    }else{
        $measure_typeList = $objMeasure_Type->getList();
        $row          = mysql_num_rows($measure_typeList);
    }
?>
    <!DOCTYPE html 
        >
    

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/measure-type.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select[name='gender']").selectpicker();
                $(".form-control").keydown(function(e){
                    if(e.keyCode == 13){
                        e.preventDefault();
                    }
                });
                $("input[name='mobile']").focus();
            });
        </script>
    </head>

    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class = "content-box-top" style="overflow:visible;">
            <div class = "summery_body">
                <div class = "content-box-header">
                    <p ><B>Measure Type Management</B> <span class="search-heading"><?php if(isset($_GET['search'])){ echo "Records: ".$queryCount; } ?></span> </p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">List</div>
                                <a href="add-measure-type-details.php"><div class="tab">New</div></a>
                            </div>
                        </span>
                    <div class="clear"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>
                <div id = "bodyTab1">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="10%" style="text-align:center">Sr No.</th>
                            <th width="20%" style="text-align:center">Name</th>
                            <th width="10%" style="text-align:center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if($row>0){
                            $counter = 1;
                            while($measure_typeDetails = mysql_fetch_assoc($measure_typeList)){
                                ?>
                                <tr id="recordPanel">
                                    <td class="text-center"><?php echo $counter; ?></td>
                                    <td class="measure_type-list text-center"><?php echo $measure_typeDetails['NAME']; ?></td>
                                    <td style="text-align:center">
                                        <a href="add-measure-type-details.php?id=<?php echo $measure_typeDetails['ID']; ?>">
                                            <input type="button" value="View" id="view_button" title="View/Update" />
                                        </a>
                                        <a onClick="deleteMeasureType(this);" id="del" value="<?php echo $measure_typeDetails['ID']; ?>" class="pointer" title="Delete"><i class="fa fa-times" ></i></a>
                                    </td>
                                </tr>
                            <?php
                                $counter++;
                            }
                        }
                        else{
                            ?>
                            <tr id="recordPanel">
                                <td style="text-align:center" colspan="6"> No Record Found!</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <table align="center" border="0" id="navbar">
                        <tr>
                            <td style="text-align: center">
                                <?php
                                if(isset($queryCount)){
                                    if($queryCount>$pageCount){
                                        ?>
                                        <nav>
                                            <ul class="pagination">
                                                <?php
                                                if($myPage > 1)
                                                {
                                                    echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=1'>Fst</a></li>";
                                                    echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage-1) ."'>Prv</a></li>";
                                                }

                                                for($x=$myPage-4; $x<$myPage; $x++)
                                                {
                                                    if($x > 0){
                                                        echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $x ."'>". $x ."</a></li>";
                                                    }
                                                }

                                                print "<li><a href='#' class='current'>". $myPage ."</a></li>";

                                                for($y=$myPage+1; $y<$myPage+5; $y++)
                                                {
                                                    if($y <= $pageCount){
                                                        echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $y ."'>". $y ."</a></li>";
                                                    }
                                                }

                                                if($myPage < $pageCount)
                                                {
                                                    echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". ($myPage+1) ."'>Nxt</a></li>";
                                                    echo "<li><a href='measure-type-management.php?title=".$_GET['title']."&status=".$_GET['status']."&search=Search&page=". $pageCount ."'>Lst</a></li>";
                                                }

                                                ?>
                                            </ul>
                                        </nav>

                                    <?php }
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </div> <!--bodyTab1-->

                <div id = "bodyTab2" style=" display:none">
                    <form method="get" action="">
                        <div id="form">
                            <div class="title">Search Measure Type :</div>

                            <div class="caption">Measure Type Title :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="title" />
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Status :</div>
                            <div class="field">
                                <input type="text" class="form-control" name="status" />
                            </div>
                            <div class="clear"></div>

                            <div class = "caption"></div>
                            <div class = "field">
                                <input type="submit" name="search" value="Search" class="button" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div> <!--bodyTab1-->
                <div class="clear" style="height:30px"></div>
            </div><!-- End summer -->
        </div><!-- End .content-box-top-->
        <!-- Delete confirmation popup -->
        <div id="myConfirm" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning" style="font-weight: bold;">Do you want to delete it?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id='delete' name='delme' >Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id='cancell' value="cancel" name="cancel">Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div><!--bodyWrapper-->
    </body>
    </html>
<?php include('conn.close.php'); ?>
