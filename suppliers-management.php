<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/suppliers.php');

	//Permission
	if(!in_array('suppliers-management',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objSuppliers = new suppliers();

	$total_per_page = $objConfigs->get_config('PER_PAGE');

	if(isset($_POST['search'])){
		$objSuppliers->account_title = mysql_real_escape_string($_POST['supplierName']);
		$supplierList = $objSuppliers->search($objSuppliers->account_title);
	}else{
		$supplierList = $objSuppliers->getList();
	}
?>
<!DOCTYPE html>
<html>
	 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>SIT Solutions</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
		<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			th{
				font-size: 14px !important;
				padding: 10px !important;
			}
			td{
				font-size: 14px !important;
				padding: 10px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
	 </head>
	 <body>
				<div id = "body-wrapper">
					<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
				<div id = "bodyWrapper">
					<div class = "content-box-top">
							<div class = "summery_body">
									<div class = "content-box-header">
										<p>Suppliers List</p>
										<span id = "tabPanel">
											<div class = "tabPanel">
													<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
													<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
													<a href="supplier-details.php"><div class = "tab">New</div></a>
											</div>
										</span>
										<div class="clear"></div>
									</div><!-- End .content-box-header -->

									<div id = "bodyTab1">
												<div class="clear"></div>
												<table class="table-responsive">
														<thead>
																<tr>
																		<th width="5%" style="text-align:center">Sr#</th>
																		<th width="10%"  style="text-align:center">AccountCode</th>
																		<th width="15%">Account Title</th>
																		<th width="15%">Contact Person</th>
																		<th width="15%" style="text-align:center">Email</th>
																		<th width="15%" style="text-align:center">Contact No.</th>
																		<th width="10%" style="text-align:center">Action</th>
																</tr>
														</thead>

														<tbody class="body_t">
<?php
						if(isset($supplierList)){
							$counter = 1;
							while($supplierRow = mysql_fetch_array($supplierList)){
?>
																<tr id="recordPanel">
																		<td style="text-align:center"><?php echo $counter; ?></td>
																		<td style="text-align:center"><?php echo $supplierRow['SUPP_ACC_CODE']; ?></td>
																		<td>
																			<span class="pull-left"><?php echo $supplierRow['SUPP_ACC_TITLE'];?></span>
																			<span class="pull-right urdu-font"><?php echo $supplierRow['URDU_TITLE'];?></span>
																		</td>
																		<td><?php echo $supplierRow['CONT_PERSON']; ?></td>
																		<td style="text-align:center"><?php echo $supplierRow['EMAIL']; ?></td>
																		<td style="text-align:left;color:#69F">
																			<?php echo $supplierRow['MOBILE']."<br />".$supplierRow['PHONES']; ?>
																		</td>
																		<td class="appendable" style="text-align:center;position:relative;">
																	<a  id="view_button" href="supplier-details.php?cid=<?php echo $supplierRow['SUPP_ID']; ?>"><i class="fa fa-pencil"></i></a>
																	<a do="<?php echo $supplierRow['SUPP_ID']; ?>" value="Delete" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
															</td>
																</tr>
<?php
							$counter++;
							}
						}
?>
														</tbody>
												</table>
												<div class="text-center"><div class="paging"></div></div>
												<div style = "clear:both; height:20px"></div>
									</div><!-- End #bodyTab1 -->

									<div id = "bodyTab2" style="display:none">
											<div class="col-xs-12 col-sm-12 col-md-6 mt-20">
											<form method="post" action="suppliers-management.php?tab=list" class="form-horizontal">
												<div class="form-group">
													<label class="control-label col-sm-2"> Title :</label>
													<div class="col-sm-10">
														<input name="supplierName" type="text" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-2"></label>
													<div class="col-sm-10">
														<input name="search" type="submit" value="Search" class="btn btn-primary" />
													</div>
												</div>
											</form>
											</div><!--form-->
											<div class="clear"></div>
									</div><!--End bodyTab2-->
							</div><!-- End summer -->
					</div><!-- End .content-box -->
				</div><!--body-wrapper-->
				<div id="xfade"></div>
				<div id="fade"></div>
		</body>
		<script type="text/javascript">
			$(document).ready(function(){
				<?php if(isset($_GET['tab'])){ $tabs = $_GET['tab'];echo ($tabs=='list')?"tab('1', '1', '2')":"tab('2', '1', '2')";}?>
			});
			$(window).load(function(){
				$('#bodyTab1').on('click','.pointer',function(){
				var id = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.get('db/del-account-code.php', {sid : id}, function(data){
							data = $.parseJSON(data);
							$("#popUpDel").children(".confirm").text(data['MSG']);
							$("#popUpDel").children(".dodelete").hide();
							$("#popUpDel").children(".nodelete").text("Close");
							if(data['OK'] == 'Y'){
								clickedDel.parent('td').parent('tr').remove();
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
				});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#fade").fadeOut('fast');
				});
				});
			});
		</script>
</html>
<?php include('conn.close.php'); ?>
