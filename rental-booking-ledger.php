<?php
    include('common/connection.php');
    include 'common/config.php';
    include('common/classes/accounts.php');
    include('common/classes/j-voucher.php');
    include('common/classes/items.php');
    include('common/classes/itemCategory.php');
    include('common/classes/customers.php');
    include('common/classes/rental_booking.php');
    include('common/classes/rental_booking_receipts.php');

    //Permission
    if(!in_array('rental-booking',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objJournalVoucher                = new JournalVoucher();
    $objAccountCodes                  = new ChartOfAccounts();
    $objItems                         = new Items();
    $objItemCategory                  = new ItemCategory();
    $objCustomers                     = new Customers();
    $objRentalBooking                 = new RentalBooking();
    $objRentalBookingReceipts         = new RentalBookingReceipts();
    $objConfigs                       = new Configs();

    $rb_id     = (int)mysql_real_escape_string($_GET['rbid']);

    $vouchers_arr = $objRentalBookingReceipts->getVoucherListByRbId($rb_id);

    if(count($vouchers_arr)>1){
      $vouchers_str = implode(',',$vouchers_arr);
    }elseif(count($vouchers_arr)==1){
      $vouchers_str = $vouchers_arr[0];
    }

    if(isset($vouchers_str)){
      $voucher_list        = $objJournalVoucher->getLedgerByVouchersIN($vouchers_str);
    }
    $rentalDetail = $objRentalBooking->getDetail($rb_id);
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/rental.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" >
        $(document).ready(function() {
            $("select").selectpicker();
            $("*[data-toggle=tooltip]").tooltip();
            $("a.pointer").click(function(){
                //$(this).deleteMainRow("db/del-quotation.php");
            });
            $("ul#quot_pagination a").each(function(i,e){
                $(this).attr("href",$(this).attr("href")+"&mode=list");
            });
            $(".pointer").click(function(){
              var id = $(this).attr("do");
        			var clickedDel = $(this);
        			$("#fade").hide();
        			$("#popUpDel").remove();
        			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
        			$("#popUpDel").hide();
        			$("#popUpDel").centerThisDiv();
        			$("#fade").fadeIn('slow');
        			$("#popUpDel").fadeIn();
        			$(".dodelete").click(function(){
                  $(this).hide();
                  $(".nodelete").text("Please Wait....");
        					$.post("rental-booking-receipts.php", {delete_id : id}, function(data){
                    $("#fade").fadeOut();
            				$("#popUpDel").fadeOut(function(){
                      $(this).remove();
                    });
                    clickedDel.parent('td').parent('tr').remove();
        					});
        				});
        			$(".nodelete").click(function(){
        				$("#fade").fadeOut();
        				$("#popUpDel").fadeOut();
        			});
        			$(".close_popup").click(function(){
        				$("#popUpDel").slideUp();
        				$("#fade").fadeOut('fast');
        			});
            });
        });
        var show_form = function(elm){
          $("#bodyTab1").hide();
          $("#bodyTab2").show();
          $(".tabSelected.tab-small").addClass('tab');
          $(".tabSelected.tab-small").removeClass('tabSelected');
          $(elm).removeClass('tab');
          $(elm).addClass('tabSelected');
        };
        var show_list = function(elm){
          $("#bodyTab2").hide();
          $("#bodyTab1").show();
          $(".tabSelected.tab-small").addClass('tab');
          $(".tabSelected.tab-small").removeClass('tabSelected');
          $(elm).removeClass('tab');
          $(elm).addClass('tabSelected');
        };
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Rental Booking Details</p>
								<span id="tabPanel">
										<div class="tabPanel">
												<a href="rental-booking-management.php?tab=list"><div class="tab">List</div></a>
												<a href="rental-booking-management.php?tab=search"><div class="tab">Search</div></a>
												<a href="rental-booking-details.php"><div class="tabSelected">Detail</div></a>
										</div>
								</span>
								<div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-header">
                <span id="tabPanel" class="pull-left ml-10">
                    <div class="tabPanel">
                        <a href="rental-booking-details.php?id=<?php echo $rb_id; ?>" class="tab"><i class="fa fa-cube"></i> Booking</a>
                        <a href="rental-booking-receipts.php?rbid=<?php echo $rb_id; ?>" class="tab"><i class="fa fa-random"></i> Receipts</a>
                        <a href="<?php echo ($rb_id>0)?"rental-booking-ledger.php?rbid=".$rb_id:"#"; ?>" class="tabSelected"><i class="fa fa-file-o"></i> Ledger</a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>

            <div class="content-box-content">
                <div id="bodyTab1">
                  <table width="100%" cellspacing="0" >
                      <thead>
                          <tr>
                             <th width="10%" class="text-center">EntryDate</th>
                             <th width="10%" class="text-center">AccountCode</th>
                             <th width="20%" class="text-center">AccountTitle</th>
                             <th width="30%" class="text-center">Description</th>
                             <th width="10%" class="text-center">Debit</th>
                             <th width="10%" class="text-center">Credit</th>
                          </tr>
                      </thead>
                      <tbody>
<?php

?>
                        <tr class="transactions">
                            <td class="text-center"><?php echo date('d-m-Y',strtotime($rentalDetail['BOOKING_DATE_TIME'])); ?></td>
                            <td class="text-center">010111001</td>
                            <td class="text-left">Rent Receivable A/c</td>
                            <td class="text-left"><?php echo "Booking Order Number : ".$rentalDetail['BOOKING_ORDER_NO']." Started." ?></td>
                            <td class="text-center"><?php echo $rentalDetail['TOTAL_PRICE']; ?></td>
                            <td class="text-center">- - -</td>
                        </tr>
                        <tr class="transactions">
                            <td class="text-center"><?php echo date('d-m-Y',strtotime($rentalDetail['BOOKING_DATE_TIME'])); ?></td>
                            <td class="text-center">0101010001</td>
                            <td class="text-left">Cash in Hand A/c Admin</td>
                            <td class="text-left"><?php echo "Booking Order Number : ".$rentalDetail['BOOKING_ORDER_NO']." Advance Received." ?></td>
                            <td class="text-center">- - -</td>
                            <td class="text-center text-success"><?php echo $rentalDetail['ADVANCE']; ?></td>
                        </tr>
<?php
  $total_expense = $rentalDetail['TOTAL_PRICE'];
  $total_income  = $rentalDetail['ADVANCE'];

  if(isset($voucher_list) && mysql_num_rows($voucher_list)){
      while($expense_row = mysql_fetch_array($voucher_list)){
          if(strtoupper($expense_row['TRANSACTION_TYPE']) == 'CR'){
              continue;
          }
          $account_title = $objAccountCodes->getAccountTitleByCode($expense_row['ACCOUNT_CODE']);
          $amount_dr     = "- - -";
          $amount_cr     = number_format($expense_row['AMOUNT'],2);
          $total_income   += $expense_row['AMOUNT'];
?>
                          <tr class="transactions">
                              <td class="text-center"><?php echo date('d-m-Y',strtotime($expense_row['VOUCHER_DATE'])); ?></td>
                              <td class="text-center"><?php echo $expense_row['ACCOUNT_CODE']; ?></td>
                              <td class="text-left"><?php echo $account_title; ?></td>
                              <td class="text-left"><?php echo $expense_row['NARRATION']; ?></td>
                              <td class="text-center"><?php echo $amount_dr; ?></td>
                              <td class="text-center"><?php echo $amount_cr; ?></td>
                          </tr>
<?php
      }
  }
?>
                      </tbody>
                      <tfoot>
                          <?php
                              $loss   = 0;
                              $profit = 0;
                              $type   =  "";
                              if($total_expense > $total_income){
                                  $loss = $total_expense - $total_income;
                              }
                              if($total_income > $total_expense){
                                  $profit = $total_income - $total_expense;
                              }
                          ?>
                          <tr>
                              <th colspan="4" class="text-right pr-10"><span class="pull-right ml-10"> Remaining Balance</span></th>
                              <th class="text-center" style="color:black;"><?php echo number_format($profit,2); ?></th>
                              <th class="text-center" style="color:red;"><?php echo number_format($loss,2); ?></th>
                          </tr>
                          <tr>
                              <th colspan="4" class="text-right pr-10"><span class="pull-right ml-10">Column Total</span> </th>
                              <th class="text-center"><?php echo number_format($total_expense+$profit,2); ?></th>
                              <th class="text-center"><?php echo number_format($total_income+$loss,2); ?></th>
                          </tr>
                      </tfoot>
                  </table>
                </div>
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script>
    $(function(){
        $(".shownCode,.loader").hide();
        $("input.insert_row").on('keydown',function(e){
            if(e.keyCode == 13){
                $(this).quickSave();
            }
        });
        $("button.saveExpense").click(function(){
            saveProjectExpense();
        });
        $("div.account_code button").keydown(function(e){
            if(e.keyCode == '13'){
                if($("select.account_code option:selected").val() != ''){
                    $("input.narration").focus();
                }
            }
        });
        $("input.narration").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.amount").focus();
            }
        });
        $("input.amount").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.charged").focus();
            }
        });
        $("input.charged").keydown(function(e){
            if(e.keyCode == '13'){
                $("input.insert_row").focus();
            }
        });
        $(window).keydown(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                $("button.saveExpense").click();
                return false;
            }
        });
        $("#fromDatepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            showAnim : 'show',
            changeMonth: true,
            changeYear: true,
            yearRange: '2000:+10'
        });
        $("#toDatepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            showAnim : 'show',
            changeMonth: true,
            changeYear: true,
            yearRange: '2000:+10'
        });
    });
</script>
