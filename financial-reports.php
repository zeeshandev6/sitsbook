<?php
	include('common/connection.php');
	include('common/config.php');

  $objConfigs         = new Configs();
  $use_branches       = $objConfigs->get_config('USE_BRANCHES');
	$specific_salesman  = $objConfigs->get_config('SPECIFIC_SALESMAN');
?>
<!DOCTYPE html>
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <style>
			a.poplight{
				color:#666;
			}
			h3{
				font-size: 14px;
			}
		</style>
  	<!-- jQuery -->
  	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
   </head>
   <body>
      	<div id="body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div><!-- End #sidebar -->
         	<div id="bodyWrapper">
            	<div class = "content-box-top"  style="padding-bottom: 30px;">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p >Financial Accounting</p>
                  		</div><!-- End .content-box-header -->
               			<div class="clear"></div>

										<div class="col-xs-12" style="padding: 15px;">
<?php
                    if(in_array('print-coa',$permissionz) || $admin == true){
?>
                        <a href="print-coa.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/wip.png" class="panelIcon" />
                                    <h3>Chart Of Accounts</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }
?>
<?php
                    /*
                    if(in_array('print-customer-contacts',$permissionz) || $admin == true){
?>
                        <a href="print-customer-contacts.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/wip.png" class="panelIcon" />
                                    <h3>Customer Contacts List</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }

                    */
?>
<?php
                    if(in_array('daily-report',$permissionz) || $admin == true){
?>
                        <a href="daily-report-summary.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/wip.png" class="panelIcon" />
                                    <h3>Daily Report Summary</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }
?>
<?php
                        if(in_array('expense-report',$permissionz) || $admin == true){
?>
                        <a href="expense-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/lr.png" class="panelIcon" />
                                    <h3>Monthly Expense Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
					if($saleTaxModule == 'Y'){
						if(in_array('tax-purchase-report',$permissionz) || $admin == true){
?>
								<a href="tax-purchase-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
												    <h3>Data Export Purchases</h3>
                                                </div>
										</div>
								</a>
<?php
						}
						if(in_array('tax-sale-report',$permissionz) || $admin == true){
?>
								<a href="tax-sale-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
												    <h3>Data Export Sales</h3>
                                                </div>
										</div>
								</a>
<?php
						}
					}
					if($embSalesAddon == 'Y'){
						if(in_array('emb-sale-report',$permissionz) || $admin == true){
?>
								<a href="emb-sale-report.php">
                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                        <div class="panel1 hvr-underline-from-center">
                                                <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                                <h3>Embroidery Sales</h3>
                                            </div>
                                    </div>
								</a>
<?php
						}
					}
					if($gatePassAddon == 'Y'){
						if(in_array('gate-pass-report',$permissionz) || $admin == true){
?>
								<a href="gate-pass-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
												    <h3>Gate Pass</h3>
                                                </div>
										</div>
								</a>
<?php
						}
					}
					if($distSalesAddon == 'Y'){
						if(in_array('po-purchases-report',$permissionz) || $admin == true){
?>
								<a href="po-purchases-report.php">
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                            <div class="panel1 hvr-underline-from-center">
                        <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                        <h3>P.O Purchases</h3>
                      </div>
                  </div>
								</a>
<?php
						}
						if(in_array('po-stock-report',$permissionz) || $admin == true){
?>
								<a href="po-stock-report.php">
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                            <div class="panel1 hvr-underline-from-center">
                        <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                        <h3>P.O Stock</h3>
                      </div>
                  </div>
								</a>
<?php
						}
					}
					if($distSalesAddon == 'Y'){
						if(in_array('distributor-sale-report',$permissionz) || $admin == true){
?>
								<a href="dist-salesman-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
												    <h3>Distributor Sales</h3>
                                                </div>
										</div>
								</a>
                                <a href="saleman-sales-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
												    <h3>Salesman - Sale Report</h3>
                                                </div>
										</div>
								</a>
<?php
						}
					}
					if($salePurchaseModule == 'Y'){
?>
<?php
                        if(in_array('customers-report',$permissionz) || $admin == true){
?>
                        <a href="top-customers-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/customer.png" class="panelIcon" />
                                    <h3>Top Customers Reports</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
					}
?>
<?php
                        if(in_array('party-receivables-report',$permissionz) || $admin == true){
?>
                        <a href="party-receivables-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/customer.png" class="panelIcon" />
                                    <h3>Party Receivables</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
<?php
                        if(in_array('party-payables-report',$permissionz) || $admin == true){
?>
                        <a href="party-payables-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/supplier.png" class="panelIcon" />
                                    <h3>Party Payables</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
					if($chequesAddon == 'Y'){
                        if(in_array('cheques-receipts-report',$permissionz) || $admin == true){
?>
                        <a href="cheques-receivable-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/cheque_book.png" class="panelIcon" />
                                    <h3>Cheques Receivable Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
												if(in_array('cheques-payments-report',$permissionz) || $admin == true){
?>
                        <a href="cheques-payable-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                    <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/cheque_book.png" class="panelIcon" />
                                    <h3>Cheques Payable Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
					}
					if($salePurchaseModule == 'Y'||$distSalesAddon == 'Y'){
                        if(in_array('purchase-report',$permissionz) || $admin == true){
?>
                        <a href="stocks-purchase-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/wip.png" class="panelIcon" />
                                    <h3>Purchase Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
					}
					if($salePurchaseModule == 'Y' || $fabricProcessAddon == 'Y'){
                    if(in_array('sales-report',$permissionz) || $admin == true){
?>
                        <a href="stocks-sale-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Sale Report</h3>
                                </div>
                            </div>
                        </a>
<?php
					   				}
					}
					if($medicalStoreAddon=='Y'){
						if(in_array('batch-no-report',$permissionz) || $admin == true){
						?>
						<a href="batch-no-report.php">
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
								    <img src="resource/images/MedIcons/tests.png" class="panelIcon" />
								    <h3>Batch Stock Report</h3>
                                </div>
							</div>
						</a>
						<?php
						}
					}
				//if($fabricProcessAddon == 'Y'){
                if(false){
                    if(in_array('sales-report',$permissionz) || $admin == true){
?>
                        <a href="fabric-sale-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Sale Report</h3>
                                </div>
                            </div>
                        </a>
<?php
					}
				}
				if($distSalesAddon == 'Y'){
                    if(in_array('sales-report',$permissionz) || $admin == true){
?>
                        <a href="dist-sale-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Sale Report</h3>
                                </div>
                            </div>
                        </a>
                        <a href="dist-scheme-sale-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Scheme Sale Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }
                    if(in_array('sales-report',$permissionz) || $admin == true){
?>
                        <a href="dist-sale-cash-management.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Sale Cash Management</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }
                    if(in_array('sales-report',$permissionz) || $admin == true){
?>
                        <a href="dist-sale-cash-report-print.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Sale Cash Mgmt Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                    }
				}
									if($salePurchaseModule == 'Y'){
										if($specific_salesman == 'Y'){
?>
											<a href="salesman-sale-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
															    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
															    <h3>Salesman Report</h3>
                                                            </div>
													</div>
											</a>
<?php
										}
									if($saleTaxModule == 'Y'){
										if(in_array('sales-tax-report',$permissionz) || $admin == true){
?>
											<a href="sale-tax-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
															    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
															    <h3>FBR Stock Position</h3>
                                                            </div>
													</div>
											</a>
<?php
										}
									}
          }
                        if($mobileAddon == 'Y'){
?>
                        <?php if(in_array('mobile-purchase-report',$permissionz) || $admin == true){ ?>
                        <a href="mobile-purchase-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/wip.png" class="panelIcon" />
                                    <h3>Scan Purchase Report</h3>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
                        <?php if(in_array('mobile-sale-report',$permissionz) || $admin == true){ ?>
                        <a href="mobile-sale-report.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
                                    <h3>Scan Sale Report</h3>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
<?php
                        if(in_array('barcode-check',$permissionz) || $admin == true){
?>
                        <a href="barcode-check.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/barcode_icon.png" class="panelIcon" />
                                    <h3>Check Barcode</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
<?php
                        }
											if($invoiceSalesAddon=='Y'){
												if(in_array('new-invoice-report',$permissionz) || $admin == true){
?>
												 <a href="new-invoice-report.php">
														 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                            <div class="panel1 hvr-underline-from-center">
																    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
																    <h3>General Invoices</h3>
                                                                 </div>
														 </div>
												 </a>
<?php
												}
											}
?>
                        <div class="clear"></div>
                        <div class="divider"></div>
<?php
										if($embProdAddon == 'Y'){
													if(in_array('emb-inward-report',$permissionz) || $admin == true){
											?>
												<a href="emb-inward-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
																    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
																    <h3>Cloth Inward</h3>
                                                                </div>
													</div>
												</a>
											<?php
											}
											if(in_array('emb-outward-report',$permissionz) || $admin == true){
											?>
												<a href="emb-outward-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
																    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
																    <h3>Cloth Outward</h3>
                                                                </div>
													</div>
												</a>
											<?php
											}
											if(in_array('emb-billing-report',$permissionz) || $admin == true){
											?>
												<a href="emb-billing-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
																    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
																    <h3>Emb Billing</h3>
                                                                </div>
													</div>
												</a>
											<?php
											}
											if(in_array('emb-lot-register-report',$permissionz) || $admin == true){
											?>
												<a href="lot-register-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
														    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
														    <h3>Lot Register</h3>
                                                        </div>
													</div>
												</a>
											<?php
											}
											if(in_array('machine-production-report',$permissionz) || $admin == true){
											?>
												<a href="machine-production-report-list.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
														    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
														    <h3>Machine Production</h3>
                                                        </div>
													</div>
												</a>
											<?php
											}
											if(in_array('emb-stocks-issue-report',$permissionz) || $admin == true){
											?>
												<a href="stocks-issue-report.php">
													<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                        <div class="panel1 hvr-underline-from-center">
															    <img src="resource/images/MedIcons/pr.png" class="panelIcon" />
															    <h3>Inventory Issue</h3>
                                                            </div>
													</div>
												</a>
											<?php
											}
											?>
											<div class="clear"></div>
											<div class="divider"></div>
											<div class="clear"></div>
											<?php
										}
?>
<?php
							if($fabricProcessAddon == 'Y'){
								if(in_array('fabric-lot-report',$permissionz) || $admin == true){
?>
								<a href="fabric-lot-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
												    <h3>Fabric Lots</h3>
                                                </div>
										</div>
								</a>
<?php
								}
								if(in_array('fabric-purchase-report',$permissionz) || $admin == true){
?>
								<a href="fabric-purchase-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
												    <h3>Fabric Purchase</h3>
                                                </div>
										</div>
								</a>
<?php
								}
								if(in_array('fabric-process-report',$permissionz) || $admin == true){
?>
								<a href="fabric-processing-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
												    <h3>Fabric Processing</h3>
                                                </div>
										</div>
								</a>
<?php
								}
								if(in_array('fabric-packing-report',$permissionz) || $admin == true){
?>
								<a href="fabric-packing-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
												    <h3>Fabric Packing</h3>
                                                </div>
										</div>
								</a>
<?php
								}
								if(in_array('fabric-embroidery-report',$permissionz) || $admin == true){
?>
								<a href="fabric-embroidery-report.php">
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                            <div class="panel1 hvr-underline-from-center">
												    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
												    <h3>Fabric Embroidery</h3>
                                                </div>
										</div>
								</a>
<?php
								}
?>
								<div class="clear"></div>
								<div class="divider"></div>
								<div class="clear"></div>
<?php
							}

                        if(in_array('aging-report',$permissionz) || $admin == true){
?>
                        <a href="aging-report-summary.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/report.png" class="panelIcon" />
                                    <h3>Aging Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if(false){
?>
                        <a href="cash-book.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/tests.png" class="panelIcon" />
                                    <h3>Cash Book</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if(in_array('general-ledger',$permissionz) || $admin == true){
?>
                        <a href="ledger-details.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/lr.png" class="panelIcon" />
                                    <h3>Ledger Details</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if(in_array('general-ledger',$permissionz) || $admin == true){
?>
                        <a href="linked-ledger-details.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/lr.png" class="panelIcon" />
                                    <h3>Linked A/c Ledger</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
<?php
                        if(in_array('trial-balance',$permissionz) || $admin == true){
?>
                        <a href="trial-balance.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/tests.png" class="panelIcon" />
                                    <h3>Trial Balance</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if(in_array('profitAndLoss',$permissionz) || $admin == true){
?>
                        <a href="profit-loss-statement.php">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/pnl.png" class="panelIcon" />
                                    <h3>Profit &amp; Loss</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if(in_array('balanceSheet',$permissionz) || $admin == true){
?>
                        <a href="balance-sheet.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/bs.png" class="panelIcon" />
                                    <h3>Balance Sheet</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
                        <div class="clear"></div>
                        <div class="divider"></div>
<?php
                    if($inwardOutwardAddon=='Y'){
                        if(in_array('stock-shifting-report',$permissionz) || $admin == true){
?>
                        <a href="stock-shifting-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/shifting-stock.png" class="panelIcon" />
                                    <h3>Stock Transactions</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                    }
                        if(in_array('supplier-stock-report',$permissionz) || $admin == true){
?>
                        <a href="supplier-stock-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                    <h3>Supplier Stock</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
                        <a href="items-search.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                    <h3>Inventory Stock</h3>
                                </div>
                            </div>
                        </a>

                        <a href="dist-stock-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                    <h3>Distributor Stock Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        if($mobileAddon == 'Y'){
                            if(in_array('available-stock-report',$permissionz) || $admin == true){
    ?>
                            <a href="available-stock-report.php" target="_blank">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                    <div class="panel1 hvr-underline-from-center">
                                        <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                        <h3>Available Stock</h3>
                                    </div>
                                </div>
                            </a>
    <?php
                            }
                        }
                        if(in_array('demand-report',$permissionz) || $admin == true){
?>
                        <a href="demand-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                    <h3>Demand Report</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
                        if($use_branches=='Y'){
                            if(in_array('branch-stock-report',$permissionz) || $admin == true){
?>
                        <a href="branch-stock-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/items.png" class="panelIcon" />
                                    <h3>Branch Stock</h3>
                                </div>
                            </div>
                        </a>
<?php
                            }
                        }
                        if(in_array('branch-stock-report',$permissionz) || $admin == true){
?>
                        <a href="customers-report.php" target="_blank">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                <div class="panel1 hvr-underline-from-center">
                                    <img src="resource/images/MedIcons/customer.png" class="panelIcon" />
                                    <h3>Customers List</h3>
                                </div>
                            </div>
                        </a>
<?php
                        }
?>
        				<div style=" clear:both;"></div>
											</div>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
	</body>
</html>
<?php include('conn.close.php'); ?>
