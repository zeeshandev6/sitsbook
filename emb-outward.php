<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/emb_products.php';
	include 'common/classes/emb_outward.php';
	include 'common/classes/machines.php';
	include 'common/classes/measure.php';
	include('common/classes/customers.php');
	include 'common/classes/emb_lot_register.php';
	include 'common/classes/j-voucher.php';
	include('common/classes/accounts.php');

	//Permission
	if(!in_array('emb-outward',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objEmbProducts 		= new EmbProducts();
	$objOutwards 				= new outward();
	$objVoucher  				= new JournalVoucher();
	$objMachines 				= new Machines();
	$objMeasure  				= new Measures();
	$objCustomers 			= new customers();
	$objLotRegister 		= new EmbLotRegister();
	$objChartOfAccounts = new ChartOfAccounts();

	$total 					= $objConfigs->get_config('PER_PAGE');

	$totalRows 			= $objOutwards->countRows();
	$customers 			= $objCustomers->getList();
	$machineList    = $objMachines->getList();

	$third_parties_suppliers = $objChartOfAccounts->getAccountByCatAccCode('040101');

	$this_page = 1;
	$start = 0;

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}

	$objOutwards->fromDate 				 = (isset($_POST['from_date']) && $_POST['from_date'] != '' )?date("Y-m-d",strtotime($_POST['from_date'])):"";
	$objOutwards->toDate 				 	 = (isset($_POST['to_date']) && $_POST['to_date'] != '' )?date("Y-m-d",strtotime($_POST['to_date'])):"";
	$objOutwards->billNum 				 = isset($_POST['bill_number'])?mysql_real_escape_string($_POST['bill_number']):"";
	$objOutwards->customerAccCode  = isset($_POST['customer'])? mysql_real_escape_string($_POST['customer']):"";
	$objOutwards->third_party_code = isset($_POST['third_party_code'])? mysql_real_escape_string($_POST['third_party_code']):"";
	$objOutwards->machineNum		   = isset($_POST['machine_id'])? mysql_real_escape_string($_POST['machine_id']):"";

	$outwardList = $objOutwards->search($start,$total);

	$found_records   = $objOutwards->found_records;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       							/>
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     							/>
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     							/>
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  								/>
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  							/>
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
		<link rel="stylesheet" href="resource/css/tooltipster.css" type="text/css"  									/>
		<style type="text/css">
		table th,table td{
			font-size: 12px !important;
			padding: 7px !important;
		}
		</style>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.outward.config.js"></script>
		<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript">
    	$(function(){
			$('select').selectpicker();
			$("ul.selectpicker:first li a").click(function(){
				var accountTitle = $(this).children("span.text").text();
				accountsCode = accountTitle.substr(-9,9);
				accountTitle = accountTitle.replace(accountTitle.substr(-9,9),'');
				if(accountTitle!==""){
					$("input[name='custAccCode']").val(accountsCode);
				}else{
					$("input[name='custAccCode']").val("");
				}
			});
			<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?>
			tab('2', '1', '2');
			<?php } ?>
		});
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Cloth Outward Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        <a href="emb-outward-details.php"><div class="tab">Add</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"none":""; ?>;">
                    <table width="100%" cellspacing="0" >
                        <thead>

                            <tr>
                               	<th width="5%"  class="text-center">OGP #</th>
                               	<th width="7%"  class="text-center">Outward Date</th>
																<th width="10%" class="text-center">3rd Party Production</th>
                                <th width="10%" class="text-center">Customer</th>
                               	<th width="10%" class="text-center">Product</th>
                                <th width="5%"  class="text-center">Lot#</th>
                                <th width="7%"  class="text-center">Quality</th>
                                <th width="7%"  class="text-center">Measure</th>
                                <th width="5%"  class="text-center">Thaan</th>
                                <th width="5%"  class="text-center">Production</th>
                                <th width="5%"  class="text-center">Design#</th>
                                <th width="5%"  class="text-center">Machine</th>
                                <th width="7%"  class="text-center">Claim Status</th>
                               	<th width="10%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
					if(mysql_num_rows($outwardList)){
						while($row = mysql_fetch_array($outwardList)){
							$detailRow = $objOutwards->getSummaryById($row['OUTWD_ID']);
							$claimClassRow = '';
							$voucherStatus = '';
							if($row['CLAIM_ID'] != 0){
								$claim_id_array = $objOutwards->getClaimIdArray($row['OUTWD_ID']);
								$detailRow = $objLotRegister->getDetail($claim_id_array[0]);
								$detailRow['LENGTH_TOTAL'] = $detailRow['MEASURE_LENGTH'];
								$detailRow['TOTAL_AMOUNT'] = $detailRow['EMB_AMOUNT'];
								$claimClassRow = 'innerTdRed';
								$voucherStatus = "<span class='red'>CLAIMED</span>";
							}
							$machineNumber  = $objMachines->getRecordDetailsValue($row['MACHINE_ID'],'MACHINE_NO');
							$machineName 	= $objMachines->getRecordDetailsValue($row['MACHINE_ID'],'NAME');
							$measureName 	= $objMeasure->getName($row['MEASURE_ID']);
?>
                            <tr class="<?php echo $claimClassRow; ?>">
                                <td class="text-center"><?php echo $row['BILL_NO'] ?></td>
                                <td class="text-center"><?php echo date('d-m-Y',strtotime($row['OUTWD_DATE'])) ?></td>
                                <td ><?php echo $objChartOfAccounts->getAccountTitleByCode($row['THIRD_PARTY_CODE']) ?></td>
																<td ><?php echo $row['CUST_ACC_TITLE'] ?></td>
                                <td class="text-left"><?php echo $objEmbProducts->getTitle($row['PRODUCT_ID']) ?></td>
                                <td class="text-center"><?php echo $detailRow['LOT_NO'] ?></td>
                                <td class="text-center"><?php echo $row['QUALITY'] ?></td>
                                <td class="text-center"><?php echo $measureName; ?></td>
                                <td class="text-center"><?php echo $row['MEASURE_LENGTH'] ?></td>
                                <td class="text-center"><?php echo $row['TOTAL_LACES'] ?></td>
                                <td class="text-center"><?php echo $detailRow['DESIGN_CODE'] ?></td>
                                <td class="text-center"><?php echo $machineName; ?></td>
                                <td class="text-center"><?php echo $voucherStatus; ?></td>
                                <td class="text-center">
                                	<a type="button" id="view_button" value="view" href="emb-outward-details.php?wid=<?php echo $row['OUTWD_ID']; ?>&page=<?php echo $this_page+1; ?>" ><i class="fa fa-pencil"></i></a>
<?php
									if($row['VOUCHER_ID'] !== 0){
										if(in_array('emb-outward-print',$permissionz) || $admin == true){
?>
                                    <a type="button" id="view_button" value="Invoice" href="emb-outward-invoice.php?id=<?php echo $row['OUTWD_ID']; ?>" target="_blank" title="Invoice" ><i class="fa fa-print"></i></a>
<?php
										}
									}
?>
<?php
									if(in_array('emb-outward-delete',$permissionz) || $admin == true){
?>
                                    <a class="pointer" do="<?php echo $row['OUTWD_ID']; ?>"><i class="fa fa-times"></i></a>
<?php
									}
?>
                            	</td>
                            </tr>
<?php
						}
					}
?>
                        </tbody>
                    </table>
					<div class="col-xs-12 text-center">
						<?php
						if($found_records > $total){
							$get_url = "";
							foreach($_GET as $key => $value){
								$get_url .= ($key == 'page')?"":"&".$key."=".$value;
							}
							?>
							<nav>
								<ul class="pagination">
									<?php
									$count = $found_records;
									$total_pages = ceil($count/$total);
									$i = 1;
									$thisFileName = basename($_SERVER['PHP_SELF']);
									if(isset($this_page) && $this_page>0){
										?>
										<li>
											<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
										</li>
										<?php
									}
									if(isset($this_page) && $this_page>=1){
										$prev = $this_page;
										?>
										<li>
											<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev.">Prev</a>"; ?>
										</li>
										<?php
									}
									$this_page_act = $this_page;
									$this_page_act++;
									while($total_pages>=$i){
										$left = $this_page_act-5;
										$right = $this_page_act+5;
										if($left<=$i && $i<=$right){
											$current_page = ($i == $this_page_act)?"active":"";
											?>
											<li class="<?php echo $current_page; ?>">
												<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
											</li>
											<?php
										}
										$i++;
									}
									$this_page++;
									if(isset($this_page) && $this_page<$total_pages){
										$next = $this_page;
										?>
										<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next.">Next</a>"; ?></li>
										<?php
									}
									if(isset($this_page) && $this_page<$total_pages){
										?>
										<li>
											<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
										</li>
									</ul>
								</nav>
								<?php
							}
						}
						?>
					</div>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>

                <div id="bodyTab2" style="display:<?php echo (isset($_GET['tab'])&&$_GET['tab']=='search')?"block":"none"; ?>;">
                    <div id="form">
                    <form method="post" action="emb-outward.php">

                        <div class="caption"></div>
                        <div class="message red"><?php echo (isset($message))?$message:""; ?></div>
                        <div class="clear"></div>

												<div class="caption">From Date</div>
                        <div class="field">
                        	<input type="text" value="" name="from_date" class="form-control datepicker" />
                        </div>
                        <div class="clear"></div>

												<div class="caption">To Date</div>
                        <div class="field">
                        	<input type="text" value="<?php echo date("d-m-Y"); ?>" name="to_date" class="form-control datepicker"  required/>
                        </div>
                        <div class="clear"></div>

												<div class="caption">OGP #</div>
                        <div class="field">
                        	<input type="text" value="" name="bill_number" class="form-control" />
                        </div>
                        <div class="clear"></div>

												<div class="caption">Customer :</div>
                        <div class="field">
                        	<select name="customer" class="form-control show-tick" data-live-search="true" >
                            <option value=""></option>
													<?php
													if(mysql_num_rows($customers)){
														while($customer = mysql_fetch_array($customers)){
													?>
														<option value="<?php echo $customer['CUST_ACC_CODE']; ?>" data-subtext="<?php echo $customer['CUST_ACC_CODE']; ?>"><?php echo $customer['CUST_ACC_TITLE']; ?></option>
													<?php
														}
													}
													?>
                            </select>
                        </div>
                        <div class="clear"></div>


												<div class="caption">Third Party :</div>
                        <div class="field">
													<select class="third_party_code show-tick form-control" name="third_party_code" data-live-search="true">
														 <option selected value=""></option>
	<?php
											if(mysql_num_rows($third_parties_suppliers)){
													while($account = mysql_fetch_array($third_parties_suppliers)){
															$selected = (isset($outward)&&$outward['THIRD_PARTY_CODE']==$account['ACC_CODE'])?"selected=\"selected\"":"";
	?>
														 <option data-subtext="<?php echo $account['ACC_CODE']; ?>" value="<?php echo $account['ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['ACC_TITLE']; ?></option>
	<?php
													}
											}
	?>
													</select>
                        </div>
                        <div class="clear"></div>

												<div class="caption">Machine :</div>
												<div class="field">
													<select name="machine_id" class="machine_id form-control" data-live-search="true" >
														<option value=""></option>
													<?php
														if(mysql_num_rows($machineList)){
															while($machine_row = mysql_fetch_array($machineList)){
													?>
														<option value="<?php echo $machine_row['ID']; ?>"><?php echo $machine_row['MACHINE_NO']."-".$machine_row['NAME']; ?></option>
													<?php
															}
														}
													?>
													</select>
												</div>
												<div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="xfade"></div>
    <div class="invoicePopup"></div>
</body>
</html>
<script>
	$(document).ready(function() {
		$(".innerTdRed").find('td').css('color','#F03 !impurtant');
		$("a.pointer").click(function(){
			var idValue = $(this).attr("do");
			var hideTr = $(this).parent().parent();
			$("#xfade").fadeOut();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'>Are You Sure?</p><a class='dodelete btn btn-danger'>Confirm</a><a class='nodelete btn btn-info'>Cancel</a></div>");
			var win_hi = $(window).height()/2;
			var win_width = $(window).width()/2;
			win_hi = win_hi-$("#popUpDel").height()/2;
			win_width = win_width-$("#popUpDel").width()/2;
			$("#popUpDel").css({
				'position': 'fixed',
				'top': win_hi,
				'left': win_width
			});
			$("#popUpDel").hide();
			$("#xfade").fadeIn('slow');
			$.get("db/del-outward.php", {wid : idValue}, function(data){
				if(data == true){
					$("#popUpDel").children("p.confirm").text("Do you want to delete outward record ? ");
					$("#popUpDel").children("a.dodelete").text("Delete");
					$("#popUpDel").fadeIn();
					$(".dodelete").click(function(){
						$.get("db/del-outward.php", {cid : idValue}, function(data){
							if(data == true){
								$("#popUpDel").children("a.dodelete").hide();
								hideTr.slideUp();
								var modSerial = hideTr.nextAll("tr");
								modSerial.each(function(){
									var getSerial = $(this).find("td").first().text();
									getSerial--;
									$(this).find("td").first().text(getSerial);
								});
								var changePerPage = $(".total_records_pagin").text();
								changePerPage = changePerPage.replace( /^\D+/g, '');
								changePerPage--;
								$(".total_records_pagin").text("Total Record : "+changePerPage);
								$("#popUpDel").children("a.nodelete").text("Close");
								$("#popUpDel").children("p.confirm").text("Record Deleted!");
							}else if(data == false){
								$("#popUpDel").children("a.dodelete").hide();
								$("#popUpDel").children("a.nodelete").text("Close");
								$("#popUpDel").children("p.confirm").text("Unable to Delete!");
							}
						});
					});
				}else{
					$("#popUpDel").fadeIn();
					$("#popUpDel").children("p.confirm").text("Do you want to delete outward record ? ");
					$("#popUpDel").children(".dodelete").click(function(){
						$.get("db/del-outward.php", {cid : idValue}, function(data){
								if(data == true){
									$("#popUpDel").children("a.dodelete").hide();
									hideTr.slideUp();
									var modSerial = hideTr.nextAll("tr");
									modSerial.each(function(){
										var getSerial = $(this).find("td").first().text();
										getSerial--;
										$(this).find("td").first().text(getSerial);
									});
									var changePerPage = $(".total_records_pagin").text();
									changePerPage = changePerPage.replace( /^\D+/g, '');
									changePerPage--;
									$(".total_records_pagin").text("Total Record : "+changePerPage);
									$("#popUpDel").children("a.nodelete").text("Close");
									$("#popUpDel").children("p.confirm").text("Record Deleted!");
								}else if(data == false){
									$("#popUpDel").children("a.dodelete").hide();
									$("#popUpDel").children("a.nodelete").text("Close");
									$("#popUpDel").children("p.confirm").text("Unable to Delete!");
								}
							});
						});
				}
			});
			$(".nodelete").click(function(){
				$("#xfade").fadeOut();
				$("#popUpDel").fadeOut();
				});
			$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#xfade").fadeOut('fast');
			});
		});
		$("input[type='text']").keyup(function(e){
			if(e.keyCode==27){
				$(this).blur();
			}
		});
<?php
		if(isset($_GET['tab'])&&$_GET['tab']=="search"){
?>
			$("#bodyTab2 input[name='billNum']").focus();
<?php
		}
?>
	});
</script>
<script><?php echo (isset($_GET['tab'])&&$_GET['tab']=="search")?"":"tab('1', '1', '2');";?></script>
