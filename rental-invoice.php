<?php
	include 'common/connection.php';
	include 'common/config.php';
	include ('common/classes/items.php');
	include ('common/classes/rental_booking.php');
	include('common/classes/company_details.php');

	$objRentalBooking 		 = new RentalBooking();
	$objItems 						 = new Items();
  $objConfigs       		 = new Configs();
	$objCompanyDetails     = new CompanyDetails();

	if(isset($_GET['id'])){
		$rbid            = mysql_real_escape_string($_GET['id']);
		$rentalDetail = $objRentalBooking->getDetail($rbid);
		$rental_detail_record = $objRentalBooking->getRentalDetailListByRentalId($rentalDetail['ID']);
		//$purchaseDetails        = mysql_fetch_array($objPurchase->getAny($purchase_id));
		//$purchaseDetailList     = $objPurchaseDetails->getList($purchase_id);
		//$supplier_balance       = $objJournalVoucher->getInvoiceBalance($purchaseDetails['SUPP_ACC_CODE'],$purchaseDetails['VOUCHER_ID']);
		//$supplier_balance_array = $objJournalVoucher->getBalanceType($purchaseDetails['SUPP_ACC_CODE'], $supplier_balance);
	}
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/invoiceStyle.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$(".printThis").click(function(){
				$(".printThisDiv").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/invoiceStyle.css",
			      pageTitle: "Software Power By SIT Solution",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			//$(".printThis").click();
		});
	</script>
</head>
<body style="background-image: url('resource/images/25x.jpg');background-size: 100%;background-repeat: no-repeat;">
<?php
	if(isset($rbid)){
		$company  = $objCompanyDetails->getActiveProfile();
		$companyLogo = $company['LOGO'];
?>
<div class="invoiceBody invoiceReady">
	<div class="header">
		<div class="headerWrapper">
			<button class="button printThis pull-right" title="Print"> <i class="fa fa-print"></i> Print </button>
		</div><!--headerWrapper-->
	</div><!--header-->
	<div class="invoiceContainer">
    	<div class="invoiceLeftPrint">
        <div class="printThisDiv">
            <div class="invoiceHead" style="width: 720px;margin: 0px auto;">
<?php
				if($companyLogo != ''){
?>
            	<img class="invLogo pull-left" src="uploads/logo/<?php echo $companyLogo; ?>" />
<?php
				}
?>
            	<div class="partyTitle pull-left" style="text-align:<?php echo $companyLogo == ''?'center':'left'; ?>;padding-left:25px;font-size: 12px;width: auto;line-height:22px;">
            		<div style="font-size:26px;padding-bottom: 10px;"> <?php echo $company['NAME']; ?> </div>
                    <?php echo $company['ADDRESS']; ?>
                    <br />
                    Contact : <?php echo $company['CONTACT']; ?>
                    <br />
                    Email : <?php echo $company['EMAIL']; ?>
                    <br />
                </div>
                <div class="clear"></div>
                <div class="partyTitle" style="text-align:center;font-size:16px;font-weight: bold;border-bottom:1px dotted #333; padding: 5px 0px;margin: 0px auto;">
									Rental Booking Invoice
                </div>
                <div class="clear" style="height: 5px;"></div>
                <div class="infoPanel pull-left">
                	<div class="infoHead">Client Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $rentalDetail['CLIENT_NAME']; ?></span>
                	<div class="clear"></div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $rentalDetail['CLIENT_MOBILE']; ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $rentalDetail['CLIENT_PHONE']; ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php echo $rentalDetail['ADDRESS']; ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="infoPanel pull-right">
                	<div class="infoHead">Invoice Details</div>
                	<span class="pull-left" style="padding:0px 0px;font-size:14px;">Booking No. <?php echo $rentalDetail['BOOKING_ORDER_NO']; ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php if($rentalDetail['BOOKING_DATE_TIME'] != "0000-00-00"){ echo "Booking Time. ".date("d-m-Y h:i A",strtotime($rentalDetail['BOOKING_DATE_TIME'])); } ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php if($rentalDetail['TRY_DATE_TIME'] != "0000-00-00"){ echo "Try Time.".date("d-m-Y h:i A",strtotime($rentalDetail['TRY_DATE_TIME']));} ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php if($rentalDetail['DELIVERY_DATE_TIME'] != "0000-00-00"){ echo "Delivery Time.".date("d-m-Y h:i A",strtotime($rentalDetail['DELIVERY_DATE_TIME']));} ?></span>
                	<div class="clear"></div>
									<span class="pull-left" style="padding:0px 0px;font-size:14px;"><?php if($rentalDetail['RETURNING_DATE_TIME'] != "0000-00-00"){ echo "Return Time. ".date("d-m-Y h:i A",strtotime($rentalDetail['RETURNING_DATE_TIME']));} ?></span>
                	<div class="clear"></div>
                </div><!--partyTitle-->
                <div class="clear;"></div>
						</div><!--invoiceHead-->
            <div class="clear"></div>
            <div class="invoiceBody" style="width: 720px;margin: 30px auto 10px auto;">
							<div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
								Total Price : Rs.<?php echo $rentalDetail['TOTAL_PRICE'] ?>
							</div><!--partyTitle-->
							<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
								Advance : Rs.<?php echo $rentalDetail['ADVANCE'] ?>
							</div><!--partyTitle-->
							<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
								Ramaining : Rs.<?php echo $rentalDetail['REMAINING'] ?>
							</div><!--partyTitle-->
							<div class="clear"></div>

                <table style="margin-bottom:20px;">
                    <thead>
                        <tr>
                        	<th width="5%">Sr#</th>
                          <th width="25%">Items/Services</th>
                          <th width="10%">Qty</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
									$counter = 1;
									if($rental_detail_record != NULL && mysql_num_rows($rental_detail_record)){
										while($row = mysql_fetch_array($rental_detail_record)){
											$itemName = $objItems->getItemTitle($row['ITEM_ID']);
?>
												<tr>
                          <td><?php echo $counter; ?></td>
													<td style="text-align:left !important"><?php echo $itemName; ?></td>
													<td><?php echo $row['QUANTITY']; ?></td>
												</tr>
<?php
									$counter++;
									}
								}
?>
                    </tbody>
                </table>
                <div class="clear"></div>

								<?php
									if($rentalDetail['DESCRIPTION'] != ''){
								?>
									<div class="">
										<b>Description :</b>
										<span><?php echo $rentalDetail['DESCRIPTION'] ?></span>
									</div>
								<?php
									}
								?>
                <div class="partyTitle pull-left" style="font-size: 14px;width: 33%;padding: 5px;">
                	Prepared By : ___________
                </div><!--partyTitle-->
                <!--<div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
                	Checked By : ___________
                </div><!--partyTitle-->
                <div class="partyTitle pull-left" style="font-size: 14px;width: 30%;padding: 5px;">
                	Authorized By : ___________
                </div><!--partyTitle-->
                <div class="clear"></div>
            </div><!--invoiceBody-->
					</div><!--printThisDiv-->
        </div><!--invoiceLeftPrint-->
    </div><!--invoiceContainer-->
</div><!--invoiceBody-->
<?php
	}
?>
</body>
</html>
<?php include('conn.close.php'); ?>
