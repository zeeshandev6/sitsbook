<?php
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/accounts.php');
	include('common/classes/stock_history.php');
	include('common/classes/items.php');
	include('common/classes/suppliers.php');
	include('common/classes/itemCategory.php');

	//Permission
	if(!in_array('stock-shifting-report',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccountCodes        = new ChartOfAccounts();
	$objStockHistory        = new StockHistory();
	$objItems               = new Items();
	$objItemCategory        = new itemCategory();
	$objSuppliers		    		= new suppliers();
	$objConfigs 		    		= new Configs();

	$suppliersList   			= $objSuppliers->getList();
	$itemsCategoryList   	= $objItemCategory->getList();
	$cashAccounts    			= $objAccountCodes->getAccountByCatAccCode('010101');

	$currency_type = $objConfigs->get_config('CURRENCY_TYPE');

	$titleRepo = '';

	if(isset($_GET['search'])){

			$objStockHistory->from_date     = (isset($_GET['fromdate']))?$_GET['fromdate']:date('Y-m-01');
			$objStockHistory->to_date       = (isset($_GET['todate']))?$_GET['todate']:"";
			$objStockHistory->pon 				  = mysql_real_escape_string($_GET['pon']);

			$getItemsMovedDuringPeriod      = $objStockHistory->movedItemsReport();
	}
	$less = false;
	$users_list = $objAccounts->getActiveList();
?>
<!DOCTYPE html 
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/landscape_reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
		<link href="resource/css/lightbox.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
		html{
		}
		.ui-tooltip{
			font-size: 12px;
			padding: 5px;
			box-shadow: none;
			border: 1px solid #999;
		}
		.input_sized{
			float:left;
			width: 152px;
			padding-left: 5px;
			border: 1px solid #CCC;
			height:30px;
			-webkit-box-shadow:#F4F4F4 0 0 0 2px;
			border:1px solid #DDDDDD;

			border-top-right-radius: 3px;
			border-top-left-radius: 3px;
			border-bottom-right-radius: 3px;
			border-bottom-left-radius: 3px;

			-moz-border-radius-topleft:3px;
			-moz-border-radius-topright:3px;
			-moz-border-radius-bottomleft:3px;
			-moz-border-radius-bottomright:5px;

			-webkit-border-top-left-radius:3px;
			-webkit-border-top-right-radius:3px;
			-webkit-border-bottom-left-radius:3px;
			-webkit-border-bottom-right-radius:3px;

			box-shadow: 0 0 2px #eee;
			transition: box-shadow 300ms;
			-webkit-transition: box-shadow 300ms;
		}
		.input_sized:hover{
			border-color: #9ecaed;
			box-shadow: 0 0 2px #9ecaed;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script src="resource/scripts/bootstrap-select.js" type="text/javascript"></script>
	<script src="resource/scripts/bootstrap.min.js"></script>
	<script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script type="text/javascript">
    	$(window).on('load',function(){
				$("td.typee_column").each(function(){
					$(this).text($("option[value='"+$(this).text()+"']").text());
				});
			$(".printThis").click(function(){
				var MaxHeight = 820;
				var RunningHeight = 0;
				var PageNo = 1;
				//Sum Table Rows (tr) height Count Number Of pages
				$('table.tableBreak>tbody>tr').each(function(){
					if (RunningHeight + $(this).height() > MaxHeight){
						RunningHeight = 0;
						PageNo += 1;
					}

					RunningHeight += $(this).height();
					//store page number in attribute of tr
					$(this).attr("data-page-no", PageNo);
				});
				//Store Table thead/tfoot html to a variable
				var tableHeader = $(".tHeader").html();
				var tableFooter = $(".tableFooter").html();
				//var repoDate = $(".repoDate").text();
				//remove previous thead/tfoot
				$(".tHeader").remove();
				$(".tableFooter").remove();
				//$(".repoDate").remove();
				//Append .tablePage Div containing Tables with data.
				for(i = 1; i <= PageNo; i++){
					$('table.tableBreak').parent().append("<div class='tablePage'><table id='Table" + i + "' class='newTable'><thead></thead><tbody></tbody></table><div class='pageFoooter'><p style=\"float:left; margin-left:0px; font-size:14px\" class=\"repoGen\"></p><span class='pazeNum'>Page. "+i+"/"+PageNo+"</span></div><div class='clear'></div></div>");
					//get trs by pagenumber stored in attribute
					var rows = $('table tr[data-page-no="' + i + '"]');
					$('#Table' + i).find("thead").append(tableHeader);
					$('#Table' + i).find("tbody").append(rows);
				}

				$(".newTable").each(function(){
					$("div.pageHeader").first().clone().insertBefore($(this));

				});
				$("div.pageHeader").first().remove();
				$(".newTable").last().append(tableFooter);
				$('table.tableBreak').remove();
				$(".printTable").printThis({
				  debug: false,
				  importCSS: false,
				  printContainer: true,
				  loadCSS: 'resource/css/landscape_reports.css',
				  pageTitle: "http://www.sitsbook.net",
				  removeInline: false,
				  printDelay: 500,
				  header: null
			  });
			});
			$('select').selectpicker();
		});
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Stock Movement Report</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    <form method="get" action="">
												<div class="caption">From Date :</div>
												<div class="field" style="width:150px;">
														<input type="text" name="fromdate" class="form-control datepicker" />
												</div>
												<div class="clear"></div>

												<div class="caption">To Date :</div>
												<div class="field" style="width:150px;">
														<input type="text" name="todate" class="form-control datepicker" value="<?php echo date('d-m-Y')  ?>" />
												</div>
												<div class="clear"></div>

												<div class="caption">PO #</div>
												<div class="field" style="width:150px;">
														<input type="text" name="pon" class="form-control" value="" />
												</div>
												<div class="clear"></div>

												<div class="caption"></div>
												<div class="field">
														<input type="submit" name="search" value="Search" class="button" />
												</div>
												<div class="clear"></div>
                    </form>
                    </div><!--form-->
										<hr />
<?php
                    if(isset($_GET['search'])){
?>

                    <span style="float:right;"><button class="button printThis">Print</button></span>
                    <div class="clear"></div>
                    <div id="bodyTab" class="printTable" style="margin: 0 auto;height: 5.6in;">
                        <div style="text-align:left;margin-bottom:0px;" class="pageHeader">
                            <p style="text-align: left;font-size:24px;margin: 0px;padding:0px;">Stock Movement Report</p>
                            <p class="repoDate">Report Generated On: <?php echo date('d-m-Y'); ?></p>
                        </div>
                        <table class="table tableBreak">
                            <thead class="tHeader">
                                <tr>
                                    <th width="20%" style="text-align:center">Item</th>
                                    <th width="10%" style="text-align:center">Opening</th>
																		<th width="10%" style="text-align:center">Inward</th>
																		<th width="10%" style="text-align:center">Available</th>
																		<th width="10%" style="text-align:center">Outward</th>
																		<th width="10%" style="text-align:center">Closing</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
														$total_opening    = 0;
														$total_inward     = 0;
														$total_avail      = 0;
														$total_outward    = 0;
														$total_closing    = 0;
                            if(mysql_num_rows($getItemsMovedDuringPeriod)){
                                while($row = mysql_fetch_assoc($getItemsMovedDuringPeriod)){
                                    $item_name      = $objItems->getItemTitle($row['ITEM']);

																		$inward_before  = $objStockHistory->getOpeningStockSum($row['ITEM'],$objStockHistory->from_date,'I');
																		$outward_before = $objStockHistory->getOpeningStockSum($row['ITEM'],$objStockHistory->from_date,'O');
																		$opening  			= $inward_before - $outward_before;
																		$total_opening += $opening;
																		$inward_current = $objStockHistory->getStockSum($row['ITEM'],$objStockHistory->from_date,$objStockHistory->to_date,'I');
																		$outward_current= $objStockHistory->getStockSum($row['ITEM'],$objStockHistory->from_date,$objStockHistory->to_date,'O');
																		$total_inward  += $inward_current;
																		$total_outward += $outward_current;
																		$available_stock= $inward_current + $opening;
																		$total_avail   += $available_stock;
																		$closing_stock  = $available_stock - $outward_current;
																		$total_closing += $closing_stock;
?>
                                <tr id="recordPanel">
                                  <td class="text-center"><?php echo $item_name; ?></td>
                                  <td class="text-center"><?php echo $opening; ?></td>
																	<td class="text-center"><?php echo $inward_current; ?></td>
																	<td class="text-center"><?php echo $available_stock; ?></td>
																	<td class="text-center"><?php echo $outward_current; ?></td>
																	<td class="text-center"><?php echo $closing_stock; ?></td>
                                </tr>
<?php
                                }
                            }else{
                                echo "<tr><td colspan='8' style='text-align: center;'>No Record Found.</td></tr>";
                            }
?>
                            </tbody>
														<tfoot class="tableFooter">
															<th class="text-right pr-10"><b>Total</b></th>
															<th class="text-center"><b><?php echo $total_opening; ?></b></th>
															<th class="text-center"><b><?php echo $total_inward; ?></b></th>
															<th class="text-center"><b><?php echo $total_avail; ?></b></th>
															<th class="text-center"><b><?php echo $total_outward; ?></b></th>
															<th class="text-center"><b><?php echo $total_closing; ?></b></th>
														</tfoot>
                        </table>
                    <div class="clear"></div>
                    </div> <!--End bodyTab-->
<?php
                    }
?>
                </div> <!-- End bodyTab1 -->
            </div> <!-- End .content-box-content -->
		</div><!--content-box-->
    </div><!--body-wrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
<script>
<?php
	if(isset($reportType)&&$reportType=='generic'){
?>
		tab('1', '1', '2')
<?php
	}
?>
<?php
	if(isset($reportType)&&$reportType=='specific'){
?>
		tab('2', '1', '2')
<?php
	}
?>
</script>
