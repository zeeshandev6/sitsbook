<?php
    include ('msgs.php');
    include ('common/connection.php');
    include ('common/config.php');
    include ('common/classes/customers.php');
    include ('common/classes/items.php');
    include ('common/classes/itemCategory.php');
    include ('common/classes/departments.php');
    include ('common/classes/scheduling.php');

    $objScheduling   = new Scheduling();
    $objCustomers    = new Customers();
    $objItems        = new Items;
    $objItemCategory = new itemCategory;
    $objDepartments  = new Departments;


    if(isset($_GET['tab']) && $_GET['tab'] == 'search'){
        $tab = 'search';
    }else{
        $tab = 'list';
    }

?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("select").selectpicker();
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    showAnim : 'show',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '2000:+10'
                });
            });
        </script>
    </head>
    <body>
    <div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
    <div id="bodyWrapper">
        <div class="content-box-top" style="overflow:visible;">
            <div class="summery_body">
                <div class = "content-box-header">
                    <p>Scheduling Management</p>
                        <span id="tabPanel">
                            <div class="tabPanel">
                                <a href="scheduling-list.php"><div class="tabSelected">List</div></a>
                                <a href="scheduling-list.php?tab=search"><div class="tab">Search</div></a>
                                <a href="scheduling.php"><div class="tab">New</div></a>
                            </div>
                        </span>
                    <div style = "clear:both;"></div>
                </div><!-- End .content-box-header -->
                <div class="clear"></div>
                <div id="bodyTab1">
                <?php if($tab == 'list'){ ?>
                    <div id="form">
                        <table>
                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Customer</th>
                                    <th>Item</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>



                            </tbody>
                        </table>
                    </div>
                <?php } ?>
                </div><!--bodyTab1-->
            </div><!--summery_body-->
        </div><!--content-box-top-->
        <div class="clear"></div>
        </div><!--bodyWrapper-->
</body>
</html>
<?php include('conn.close.php'); ?>
