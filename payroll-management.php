<?php
	include( 'common/connection.php' );
	include( 'common/config.php' );
	include( 'common/classes/payrolls.php' );

	$objPayrolls = new Payrolls();

	//Pagination Settings
	$objPagination = new Pagination;
	$items_per_page = $objPagination->getPerPage();
	//Pagination Settings { END }

	if(isset($_POST['customerSearch'])){
		$objPayrolls->account_title = $_POST['accTitle'];
		$customerDetail = $objPayrolls->search($objPayrolls->account_title);
	}else{
		$customerDetail = $objPayrolls->getList();
	}
?>
<!DOCTYPE html 
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" />
		<link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />

      	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/tab.js"></script>
				<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
      	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>
   </head>
   <body>
      	<div id = "body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->

      	<div id = "bodyWrapper">
         	<div class = "content-box-top">
            	<div class = "summery_body">
               		<div class = "content-box-header">
                  		<p>Payrolls Management</p>
                  		<span id = "tabPanel">
                     		<div class = "tabPanel">
                        		<div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
														<div class="tab" id="tab2" onClick="tab('2', '1', '2');">Search</div>
                        		<a href="payroll-details.php"><div class="tab">New</div></a>
                     		</div>
                  		</span>
                  		<div class="clear"></div>
               		</div><!-- End .content-box-header -->
               		<div class="clear"></div>
               		<div id="bodyTab1">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                   	<th width="5%" style="text-align:center">Sr#</th>
																		<th width="10%" style="text-align:center">AccountCode</th>
                                    <th width="15%" style="text-align:center">Title</th>
                                   	<th width="15%">Designation</th>
                                   	<th width="15%" style="text-align:center">Email</th>
                                   	<th width="15%" style="text-align:center">Contact No.</th>
                                   	<th width="10%" style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="body_t">
<?php
						if(isset($customerDetail)){
							$counter = 1;
							while($customerRow = mysql_fetch_array($customerDetail)){
?>
                                <tr id="recordPanel">
                                    <td style="text-align:center;"><?php echo $counter; ?></td>
                                    <td style="text-align:center;"><?php echo $customerRow['CUST_ACC_CODE']; ?></td>
                                    <td class="customerNametd"><?php echo $customerRow['CUST_ACC_TITLE']; ?></td>
                                    <td><?php echo $customerRow['CONT_PERSON']; ?></td>
                                    <td style="text-align:center;width:10%;"><?php echo $customerRow['EMAIL']; ?></td>
                                    <td style="text-align:left;color:#69F;"><?php echo "Cell#:".$customerRow['MOBILE']."<br />tel@: ".$customerRow['PHONES']; ?></td>
                                    <td class="appendable" style="text-align:center;position:relative;">
	                                	<a id="view_button"  href="payroll-details.php?cid=<?php echo $customerRow['CUST_ID']; ?>"><i class="fa fa-pencil"></i></a>
	                                    <a type="button" do="<?php echo $customerRow['CUST_ACC_CODE']; ?>" onClick="delete_row(this);" class="pointer" title="Delete"><i class="fa fa-times"></i></a>
                            		</td>
                                </tr>
<?php
							$counter++;
							}
						}
?>
                            </tbody>
                        </table>
                        <div class="text-center">
                        	<nav class="paging"></nav>
                        </div>
                        <div style = "clear:both; height:20px"></div>
               		</div><!-- End #bodyTab1 -->

               		<div id = "bodyTab2" style="display:none">
                  		<div class=" col-md-6 mt-20 ">
                     	<form method="post" action="payroll-management.php?tab=list" class="form-horizontal">
												<div class="form-group">
			    								<label class="control-label col-sm-2">Title</label>
			    								<div class="col-sm-10">
			                      <input name="accTitle" type="text" class="form-control" />
			    								</div>
			    							</div>
												<div class="form-group">
			    								<label class="control-label col-sm-2"></label>
			    								<div class="col-sm-10">
			                      <input name="customerSearch" type="submit" value="Search" class="btn btn-default" />
			    								</div>
			    							</div>
                     	</form>
                  		</div><!--form-->
                  		<div class="clear"></div>
               		</div><!--End bodyTab2-->
            	</div><!-- End summer -->
         	</div><!-- End .content-box -->
      	</div><!--body-wrapper-->
        <div id="xfade"></div>
        <div id="fade"></div>
   	</body>
    <script>
		$(document).ready(function() {
			//dataTableInit();
			$(document).keyup(function(e){
				if(e.keyCode==27){
					$("#popUpDel,#xfade").fadeOut();
				}
			});
			$("#tab1,#tab2").click(function(){
				$(".h3_php_error").show();
			});
			$(".getAccTitle").keyup(function(){
				var codeCode = $(this).val();
				$.post('db/get-customer-title.php',{title:codeCode},function(data){
					var gotCode = $.parseJSON(data);
					var titles = [];
					$.each(gotCode,function(index,value){
						$.each(value,function(i,v){
							if(i=='CUST_ACC_TITLE'){
								titles.push(v);
							}
						});
					});
					$(".getAccTitle").autocomplete({
							source: titles,
							width: 300,
							scrollHeight:220,
							matchContains: true,
							selectFirst: false,
							minLength: 1
					});
					$.each(titles,function(i,v){
						if(codeCode==v){
							$(".insertAccCode").val(gotCode[0]['CUST_ACC_CODE']);
						}else{
							$(".insertAccCode").val("");
						}
					});
				});
			});
		});
	</script>
    <script>
    	var delete_row = function(clickedDel){
    		clickedDel = $(clickedDel);
			var acc_code = clickedDel.attr("do");
			$("#fade").hide();
			$("#popUpDel").remove();
			$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
			$("#popUpDel").hide();
			$("#popUpDel").centerThisDiv();
			$("#fade").fadeIn('slow');
			$("#popUpDel").fadeIn();
			$(".dodelete").click(function(){
					$.get('db/del-account-code.php', {acc_code : acc_code}, function(data){
						data = $.parseJSON(data);
						$("#popUpDel").children(".confirm").text(data['MSG']);
						$("#popUpDel").children(".dodelete").hide();
						$("#popUpDel").children(".nodelete").text("Close");
						if(data['OK'] == 'Y'){
							clickedDel.parent('td').parent('tr').remove();
						}
					});
				});
			$(".nodelete").click(function(){
				$("#fade").fadeOut();
				$("#popUpDel").fadeOut();
			});
			$(".close_popup").click(function(){
				$("#popUpDel").slideUp();
				$("#fade").fadeOut('fast');
			});
		};
		var dataTableInit = function(){
			$('.table_pagination').datatable({
			    pageSize: <?php echo $items_per_page; ?>,
			    afterRefresh:function(){
			    	$(".body_t tr").each(function(){
			    		$(this).find("td").eq(0).css({'text-align':'center'});
			    		$(this).find("td").eq(1).css({'text-align':'center'});
			    		$(this).find("td").eq(6).css({'text-align':'center'});
			    	});
			    }
		    });
		};
<?php
	if(isset($_GET['tab']) && $_GET['tab'] == 'list'){
?>
		tab('1', '1', '2');
<?php
	}elseif(isset($_GET['tab']) && $_GET['tab'] == 'search'){
?>
		tab('2', '1', '2');
<?php
	}
?>
	</script>
</html>
<?php include('conn.close.php'); ?>
