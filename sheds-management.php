<?php

	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/sheds.php');
	include('common/classes/machines.php');

	$objSheds 	 = new Sheds();
	$objMachines = new machines();
	$objAccounts = new ChartOfAccounts();

	$totalRows = $objSheds->countRows();

	$error_msg = array();
	$total = 10;
	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}else{
			$start = 0;
		}
		$shedList = $objSheds->getPaged($start,$total);
	}else{
		$this_page = 0;
		$shedList = $objSheds->getPaged($this_page,$total);
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css"                         type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css"                          type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css"                       type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css"                  type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css"                 type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css"              type="text/css"  />
	<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
	<style media="screen">
		table th{
		font-weight: bold !important;
		}
		table th,table td{
		padding: 10px !important;
		}
	</style>
	<!-- jQuery -->
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
	<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/tab.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p> <i class="fa fa-home"></i> Sheds Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
                        <a href="shed-details.php"><div class="tab">New</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1">
                    <table class="table" >
                        <thead>
                            <tr>
                               <th class="text-center">ShedAccCode</th>
                               <th class="text-center">Shed Name</th>
                               <th class="text-center">Location</th>
                               <th class="text-center">Machines</th>
                               <th class="text-center">Contacts</th>
                               <th class="text-center">Action</th>
                            </tr>
                        </thead>

                        <tbody>
<?php
						if(mysql_num_rows($shedList)){
							while( $row = mysql_fetch_array($shedList)){
								$machinesInstalledInShed = $objMachines->countMachineByShed($row['SHED_ID']);
?>
                            <tr id="recordPanel">
                                <td class="text-center"><?php echo $row['SHED_ACC_CODE']; ?></td>
                                <td class="text-center"><?php echo $row['SHED_TITLE']; ?></td>
                                <td class="text-center"><?php echo $row['LOCATION']; ?></td>
                                <td class="text-center"><?php echo $machinesInstalledInShed; ?></td>
                                <td class="text-center"><?php echo $row['PHONES']; ?></td>
                                <td class="text-center">
									<a href="shed-details.php?id=<?php echo $row['SHED_ID']; ?>" id="view_button" ><i class="fa fa-pencil"></i></a>
									<a class="pointer" do="<?php echo $row['SHED_ID']; ?>" ><i class="fa fa-times"></i></a>
                            	</td>
                            </tr>
<?php
							}
						}else{
?>
							<tr><th colspan="10" class="text-center">0 Sheds</th></tr>
<?php
						}
?>
                        </tbody>
<?php
					if(!isset($_POST['search'])){
?>
	                    <tfoot>
                        	<tr>
                            	<td colspan="8" class="pagination_row">
                                <p>
<?php
						$count = (mysql_num_rows($shedList))?mysql_fetch_array($totalRows):mysql_num_rows($shedList);
						$total_pages = ceil($count[0]/$total);
						$i = 1;
						$thisFileName = $_SERVER['PHP_SELF'];
						if(isset($this_page) && $this_page>0){
?>
							<a href="sheds-management.php?page=1" class="nav_page">First</a>
<?php
						}
						if(isset($this_page) && $this_page>=1){
							$prev = $this_page;
?>
							<a href="sheds-management.php?page=<?php echo $prev; ?>" class="nav_page">Prev</a>
<?php
						}
						$this_page_act = $this_page;
						$this_page_act++;
						while($total_pages>=$i){
							$left = $this_page_act-3;
							$right = $this_page_act+3;
							if($left<=$i && $i<=$right){
							$current_page = ($i == $this_page_act)?"current_nav_page":"nav_page";
?>

                                	<?php echo "<a class=\"".$current_page."\" href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
<?php
							}
							$i++;
						}
						$this_page++;
						if(isset($this_page) && $this_page<$total_pages){
							$next = $this_page;
							echo " <a href='".$thisFileName."?page=".++$next."' class='nav_page'>Next</a>";
						}
						if(isset($this_page) && $this_page<$total_pages){
?>
							<a href="sheds-management.php?page=<?php echo $total_pages; ?>" class="nav_page">Last</a>
<?php
						}
						echo "<a class=\"total_records_pagin\">Total Records : ".$count[0]."</a>";
?>
								</p>
								</td>
							</tr>
						</tfoot>
<?php
					}
?>
                    </table>
                </div> <!--End bodyTab1-->
                <div style="height:0px;clear:both"></div>

                <div id="bodyTab2" style="display:none; margin-top:-10px">
                    <div id="form">
                    <form method="post" action="">
                        <div class="title" style="font-size:20px; margin-bottom:20px">Search Shed:</div>

                        <div class="caption">Shed Name:</div>
                        <div class="field">
                        	<input type="text" name="shedName" class="input_size" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Contact No:</div>
                        <div class="field">
                        	<input type="text" name="shedContact" class="input_size" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" value="Search" name="search" class="button"/>
                        </div>
                        <div class="clear"></div>
                    </form>
                    </div><!--form-->
                </div> <!-- End bodyTab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
	</div><!--body-wrapper-->
    <div id="fade"></div>
</body>
</html>
<script>
	$(document).ready(function(){
		$("a.pointer").click(function(){
			$(this).deleteRowDefault('db/del-shed.php');
		});
	});
</script>
<?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?>
<script>
	tab('2', '1', '2');
</script>
<?php } ?>
