<?php
	ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/suppliers.php');
	include('common/classes/customers.php');
	include('common/classes/j-voucher.php');
	include('common/classes/fabric_contracts.php');
	include('common/classes/fabric_contract_details.php');

	//Permission
  if(!in_array('fabric-contract',$permissionz) && $admin != true){
  echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
  echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
  echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
  echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
  echo '</div>';
  exit();
  }
  //Permission ---END--

	$objAccount         			 = new UserAccounts();
	$objCustomers       			 = new Customers();
	$objSupplier        			 = new Suppliers();
	$objAccounts        			 = new UserAccounts();
	$objJournalVoucher 				 = new JournalVoucher();
	$objFabricContract  			 = new FabricContracts();
	$objFabricContractDetails  = new FabricContractDetails();

	if(isset($_GET['lot_no'])){
		$lot_no = (int)mysql_real_escape_string($_GET['lot_no']);
	}

	if(isset($_POST['delete_contract'])){
		$contract_id = (int)(mysql_real_escape_string($_POST['delete_contract']));
		if($contract_id>0){
			$voucher_id = $objFabricContract->getVoucherId($contract_id);
			$objFabricContract->delete($contract_id);
			$objFabricContractDetails->deleteContract($contract_id);
			if($voucher_id>0){
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->deleteJv($voucher_id);
			}
			echo json_encode(array('OKAY'=>'Y','MSG'=>'Success! Record Deleted.'));
		}else{
			echo json_encode(array('OKAY'=>'N','MSG'=>'Error! Unable to delete record.'));
		}
		exit();
	}

	$total  = $objConfigs->get_config('PER_PAGE');

	$from_date 			= isset($_POST['from_date'])?$_POST['from_date']:"";
	$to_date 	 			= isset($_POST['to_date'])?$_POST['to_date']:"";
	$supplier_code 	= isset($_POST['supplier_code'])?$_POST['supplier_code']:"";
	$broker_code 		= isset($_POST['broker_code'])?$_POST['broker_code']:"";

	$objFabricContract->lot_no 		= isset($lot_no)?$lot_no:"";

	if(isset($_GET['page'])){
		$this_page = $_GET['page'];
		if($this_page>=1){
			$this_page--;
			$start = $this_page * $total;
		}
	}else{
		$start = 0;
		$this_page = 0;
	}

	$fabric_contract_list = $objFabricContract->search($from_date,$to_date,$supplier_code,$broker_code,$start,$total);
	$found_records 				= $objFabricContract->found_records;
?>
<!DOCTYPE html 
>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Admin Panel</title>
	<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/form.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  />
	<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  />
	<link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<!-- jQuery -->
	<style>
		td{
			padding: 10px !important ;
		}
	</style>
	<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
	<script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
	<script type="text/javascript" src="resource/scripts/configuration.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select").selectpicker();
			$(".delete_contract").click(function(){
				var id = $(this).attr("do");
				var clickedDel = $(this);
				$("#fade").hide();
				$("#popUpDel").remove();
				$("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
				$("#popUpDel").hide();
				$("#popUpDel").centerThisDiv();
				$("#fade").fadeIn('slow');
				$("#popUpDel").fadeIn();
				$(".dodelete").click(function(){
						$.post("<?php echo basename($_SERVER['PHP_SELF']); ?>", {delete_contract : id}, function(data){
							data = $.parseJSON(data);
							$("#popUpDel").children(".confirm").text(data['MSG']);
							$("#popUpDel").children(".dodelete").hide();
							$("#popUpDel").children(".nodelete").text("Close");
							if(data['OKAY'] == 'Y'){
								clickedDel.parent('td').parent('tr').remove();
							}
						});
					});
				$(".nodelete").click(function(){
					$("#fade").fadeOut();
					$("#popUpDel").fadeOut();
				});
				$(".close_popup").click(function(){
					$("#popUpDel").slideUp();
					$("#fade").fadeOut('fast');
				});
			});
			$(".form-control").keydown(function(e){
				if(e.keyCode == 13){
					e.preventDefault();
				}
			});
			$("input[name='mobile']").focus();
		});
	</script>
</head>
<body>
	<div id="sidebar"><?php include("common/left_menu.php") ?></div> <!-- End #sidebar -->
	<?php
	if(isset($_GET['scid'])){
		$scId = $_GET['scid'];
		$getFabricContractList      = $objFabricContract->getListByScid($scId);
		$getFabricContractListRow   = mysql_num_rows($getFabricContractList);
	}

	$counter_start = $start;
	$counter_start++;
	?>
	<div id="bodyWrapper">
		<div class = "content-box-top" style="overflow:visible;">
			<div class = "summery_body">
				<div class="content-box-header">
					<p>Fabric Contract</p>
					<span id="tabPanel">
						<div class="tabPanel">
							<a href="lot-details.php?tab=list"><div class="tab">List</div></a>
							<a href="lot-details.php?tab=search"><div class="tab">Search</div></a>
							<a href="lot-details.php?tab=form&lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">Details</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!--End.content-box-header-->
				<div class="clear"></div>

				<?php include('lot.tabs.inc.php'); ?>
				<div class="clear"></div>

				<div class = "content-box-header" style="margin-top: 0px !important;">
					<span id="tabPanel" class="pull-left" style="margin-left: 70px;">
						<div class="tabPanel">
							<a href="fabric-contract-list.php?lot_no=<?php echo $lot_no; ?>"><div class="tabSelected">List</div></a>
							<a href="fabric-contract-details.php?lot_no=<?php echo $lot_no; ?>"><div class="tab">Form</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('fabric-contract-list.php'); ?>&rf=<?php echo base64_encode('fabric-contract-details.php'); ?>&io_status=i&typee=fc&lot_no=<?php echo $lot_no; ?>"><div class="tab">Inward</div></a>
							<a href="lot-stock-history.php?rl=<?php echo base64_encode('fabric-contract-list.php'); ?>&rf=<?php echo base64_encode('fabric-contract-details.php'); ?>&io_status=o&typee=fc&lot_no=<?php echo $lot_no; ?>"><div class="tab">Outward</div></a>
						</div>
					</span>
					<div class="clear"></div>
				</div><!-- End .content-box-header -->

				<div id = "bodyTab1">
					<table cellspacing="0" width="100%" >
						<thead>
							<tr>
								<th width="5%" style="text-align:center">Sr</th>
								<th width="10%" style="text-align:center">Lot #</th>
								<th width="10%" style="text-align:center">Contract Date</th>
								<th width="15%" style="text-align:center">Supplier</th>
								<th width="15%" style="text-align:center">Agent</th>
								<th width="10%" style="text-align:center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(mysql_num_rows($fabric_contract_list)){
								while($fabricDetails = mysql_fetch_assoc($fabric_contract_list)){
									$user_deta                       = $objAccount->getDetails($fabricDetails['USER_ID']);
									?>
									<tr id="recordPanel">
										<td style="text-align:center"><?php echo $counter_start; ?></td>
										<td style="text-align:center"><?php echo $fabricDetails['LOT_NO']; ?></td>
										<td style="text-align:center"><?php echo date('d-m-Y', strtotime($fabricDetails['CONTRACT_DATE'])); ?></td>
										<td style="text-align:center"><?php echo $fabricDetails['SUPPLIER_TITLE']; ?></td>
										<td style="text-align:center"><?php echo $fabricDetails['BROKER_ACCOUNT_TITLE']; ?></td>
										<td style="text-align:center">
											<a href="fabric-contract-details.php?id=<?php echo $fabricDetails['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
											<a id="view_button" target="_blank" href="print-fabric-details.php?id=<?php echo $fabricDetails['ID']; ?>"><i class="fa fa-print"></i></a>
											<a do="<?php echo $fabricDetails['ID']; ?>" class="pointer delete_contract"><i class="fa fa-times"></i></a>
										</tr>
										<?php
										$counter_start++;
									}
								}else{
									echo "<tr><td colspan='7' style='text-align: center;'>No Record Found.</td></tr>";
								}
								?>
							</tbody>
						</table>
						<div class="col-xs-12 text-center">
							<?php
							if($found_records > $total){
								$get_url = "";
								foreach($_GET as $key => $value){
									$get_url .= ($key == 'page')?"":"&".$key."=".$value;
								}
								?>
								<nav>
									<ul class="pagination">
										<?php
										$count = $found_records;
										$total_pages = ceil($count/$total);
										$i = 1;
										$thisFileName = basename($_SERVER['PHP_SELF']);
										if(isset($this_page) && $this_page>0){
											?>
											<li>
												<?php echo "<a href=".$thisFileName."?".$get_url."&page=1>First</a>"; ?>
											</li>
											<?php
										}
										if(isset($this_page) && $this_page>=1){
											$prev = $this_page;
											?>
											<li>
												<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$prev."><i class='fa fa-arrow-left'></i></a>"; ?>
											</li>
											<?php
										}
										$this_page_act = $this_page;
										$this_page_act++;
										while($total_pages>=$i){
											$left = $this_page_act-5;
											$right = $this_page_act+5;
											if($left<=$i && $i<=$right){
												$current_page = ($i == $this_page_act)?"active":"";
												?>
												<li class="<?php echo $current_page; ?>">
													<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$i.">".$i."</a>"; ?>
												</li>
												<?php
											}
											$i++;
										}
										$this_page++;
										if(isset($this_page) && $this_page<$total_pages){
											$next = $this_page;
											?>
											<li><?php echo "<a href=".$thisFileName."?".$get_url."&page=".++$next."><i class='fa fa-arrow-right'></i></a>"; ?></li>
											<?php
										}
										if(isset($this_page) && $this_page<$total_pages){
											?>
											<li>
												<?php echo "<a href=".$thisFileName."?".$get_url."&page=".$total_pages.">Last</a>"; ?>
											</li>
										</ul>
									</nav>
									<?php
								}
							}
							?>
						</div>
					</div> <!--bodyTab1-->
					<div class="clear mt-20"></div>
				</div>     <!-- End summer -->
			</div>   <!-- End .content-box-top -->
		</div>
		<div id="xfade"></div>
		<div id="fade"></div>
	</body>
	</html>
	<?php ob_end_flush(); ?>
	<?php include("conn.close.php"); ?>
