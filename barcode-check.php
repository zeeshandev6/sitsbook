<?php
	include('common/connection.php');
	include 'common/config.php';
	//Permission
	if(!in_array('barcode-check',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		
		echo '</div>';
		exit();
	}
	//Permission ---END--
	
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
	<style>
		.the_row{
			text-align:left;
			padding: 0.5em 1em;
		}
	</style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script><!--its v1.11 jquery-->
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/barcode.config.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>    
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Barcode Checking</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                	<div id="form" class="scanner_div" style="text-align: center;">
                		<h3>Scanner</h3>
		                <input type="text" class="barcode_input" value="">
		                <button class="btn btn-danger btn-lg" onclick="$('.barcode_input').val('');" style="position: relative;top: -5px;" title="Clear"><i class="fa fa-arrow-left" style="color:#FFF;"></i></button>
		                <div class="clear"></div>
					</div> <!-- End form -->
					<div class="clear"></div>
                    <div id="form" style="width: 100%;margin: 20px auto;" class="result"></div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
   </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".barcode_input").keyup(function(e){
			if(e.keyCode ==13){
				$(this).getBarcodeRecord();
			}
		});
	});
</script>
