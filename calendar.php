<?php
    include('common/connection.php');
    include('common/classes/md.php');
    include('common/config.php');
    include('common/classes/events.php');

    $objEvents     = new Events();

    //Permission
    if($admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    if(isset($_POST['delete_event'])){
        $id = mysql_real_escape_string($_POST['delete_event']);
        $objEvents->delete($id);
        exit();
    }

    if(isset($_POST['status'])){
        $status = mysql_real_escape_string($_POST['status']);
        $id     = mysql_real_escape_string($_POST['id']);
        $objEvents->set_status($id,$status);
        exit();
    }

    if(isset($_POST['id'])){
        $id                     = mysql_real_escape_string($_POST['id']);
        $objEvents->title       = mysql_real_escape_string($_POST['event_title']);
        $objEvents->description = mysql_real_escape_string($_POST['event_description']);
        $objEvents->event_date  = date('Y-m-d H:i:s',strtotime($_POST['event_datetime']));

        if($id == 0){
            $objEvents->save();
        }

        if($id > 0){
            $objEvents->update($id);
        }

        exit();
    }

    if(isset($_POST['get_events_of_month'])){
        $month  = mysql_real_escape_string($_POST['get_events_of_month']);
        $events = $objEvents->count_by_month($month);
        $event_Array = array();
        if(mysql_num_rows($events)){
            while($row = mysql_fetch_assoc($events)){
                $d = (int)date('d',strtotime($row['EVENT_DATE']));
                $event_Array[$d] = $row['TOTAL'];
            }
        }
        echo json_encode($event_Array);
        exit();
    }


    if(isset($_POST['get_events'])){
        $event_date = date("Y-m-d",strtotime($_POST['get_events']));
        $event_list = $objEvents->getList($event_date);
        if(mysql_num_rows($event_list)){
            while($event = mysql_fetch_assoc($event_list)){
  ?>
            <div class="event col-md-12" data-row="<?php echo $event['ID']; ?>">
                <p class="pull-left col-xs-12">
                    <span><?php echo $event['TITLE']; ?></span>
                    <button class="pointer pull-right ml-5" onclick="delete_event(<?php echo $event['ID']; ?>);"> <i class="fa fa-times"></i> </button>
                    <button id="view_button" class="pull-right" onclick="edit_event(<?php echo $event['ID']; ?>);"> <i class="fa fa-pencil"></i> </button>
                    <small class="pull-right mr-10 is_time"><?php echo date('d-m-Y h:i A',strtotime($event['EVENT_DATE'])); ?></small>
                </p>
                <div class="clear"></div>
                <small class="col-xs-12 is_desc"><?php echo $event['DESCRIPTION']; ?></small>
            </div>
  <?php
            }
        }
    exit();
    }
?>
<!DOCTYPE html 
   >
<html>
   <head>
      	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/sits-calendar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-datetimepicker.min.css" type="text/css" media="screen" />

      	<script type = "text/javascript" src = "resource/scripts/jquery.1.11.min.js"></script>
        <script type = "text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
        <script type="text/javascript" src="resource/scripts/moment.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-datetimepicker.js"></script>
        <script type = "text/javascript" src = "resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <style type="text/css">
            .ui-datepicker-title select{
                height: 36px;
                width: 100px;
            }
        </style>
        <script type="text/javascript">
            (function($){
                var datepickerExtensions = {
                    _oldAdjustDate: $.datepicker._adjustDate,
                    _adjustDate: function(id, offset, period) {
                        var target = $(id);
                        var inst = this._getInst(target[0]);
                        var afterAdjustDate = this._get(inst, 'afterAdjustDate');
                        this._oldAdjustDate(id, offset, period);
                        if(afterAdjustDate && typeof afterAdjustDate === 'function'){
                            afterAdjustDate(id, offset, period);
                        }
                    }
                }
                $.extend($.datepicker, datepickerExtensions);
                $.fn.serializeObject = function(){
                    var o = {};
                    var a = this.serializeArray();
                    $.each(a, function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                };
            })(jQuery);
            $(document).ready(function(){
                $("input[name=event_datetime]").datetimepicker({
                    format: 'DD-MM-YYYY h:mm A'
                });
                $("input#alternate").change(function(){
                    var this_date = $(this).val();
                    $("#events .panel-body").html('').addClass("loadingContent");
                    $.post("calendar.php",{get_events:this_date},function(data_html){
                        $("#events .panel-body").removeClass("loadingContent").html(data_html);
                    })
                    var dsplit = this_date.split("-");
                    var d=new Date(dsplit[0],dsplit[1]-1,dsplit[2]);
                    get_events_of_month(d.getMonth()+1);
                });
                $('#calendar').datepicker({
                    dateFormat: 'dd-mm-yy',
                    showAnim : 'show',
                    changeMonth: true,
                    changeYear: true,
                    altField: "#alternate",
                    altFormat:  'yy-mm-dd',
                    onSelect:function(i,o,p){
                        setTimeout(function(){
                            $("select").selectpicker();
                        },100);
                        $("#alternate").change();
                    },
                    afterAdjustDate:function(i,o,p){
                        $("select").selectpicker();
                        $("#alternate").change();
                    }
                });
                setTimeout(function(){
                    $("select").selectpicker();
                },100);
                $("#alternate").change();
            });
            var save_event    = function(){
                var form_Data = $("#popUpBox form").serializeObject();
                $(".removeonsave").hide();
                $.post("<?php echo basename($_SERVER['PHP_SELF']); ?>",form_Data,function(){
                    hidePopUpBox();
                    $("#popUpBox form")[0].reset();
                    $(".removeonsave").show();
                    $("#alternate").change();
                });
            };
            var new_event    = function(){
                show_popUpBox();
                $("#popUpBox form")[0].reset();
                $("#popUpBox form input.id").val(0);
            };
            var edit_event   = function(id){
                id = parseInt(id)||0;
                var title       = $(".event[data-row='"+id+"'] p span").text();
                var description = $(".event[data-row='"+id+"'] small.is_desc").text();
                var datetime    = $(".event[data-row='"+id+"'] small.is_time").text();
                if(id > 0){
                    new_event();
                    $("#popUpBox input[name=id]").val(id);
                    $("#popUpBox input[name=event_datetime]").val(datetime);
                    $("#popUpBox input[name=event_title]").val(title);
                    $("#popUpBox textarea[name=event_description]").val(description);
                }
            };
            var delete_event = function(id){
                id = parseInt(id)||0;
                if(id > 0){
                    $("#xfade").hide();
                    $("#popUpDel").remove();
                    $("body").append("<div id='popUpDel'><p class='confirm'>Do you Really want to Delete?</p><a class='dodelete button'>Delete</a><a class='nodelete button'>Cancel</a></div>");
                    $("#popUpDel").hide();
                    $("#popUpDel").centerThisDiv();
                    $("#xfade").fadeIn('slow');
                    $("#popUpDel").fadeIn();
                    $(".dodelete").click(function(){
                        $.post("calendar.php",{delete_event:id},function(data){
                            $(".event[data-row='"+id+"']").hide('clip');
                            $("#popUpDel").slideUp();
                            $("#xfade").fadeOut();
                            $("#alternate").change();
                        });
                    });
                    $(".nodelete").click(function(){
                        $("#xfade").fadeOut();
                        $("#popUpDel").fadeOut();
                    });
                    $(".close_popup").click(function(){
                        $("#popUpDel").slideUp();
                        $("#xfade").fadeOut('fast');
                    });
                }
            };
            var show_popUpBox= function(){
                $("#xfade").fadeIn();
                $("#popUpBox").css({'transform':'scaleY(1.0)'});
                $(".event_datetime").val($("#alternate").val());
                $("#popUpBox").fadeIn(function(){
                    var win_hi = $(window).height()/2;
                    var win_width = $(window).width()/2;
                    win_hi = win_hi-$(this).height()/2;
                    win_width = win_width-$(this).width()/2;
                    $(this).css({
                        'position': 'absolute',
                        'top': '100px',
                        'left': win_width
                    });
                });
            };
            var get_events_of_month = function(themonth){
                themonth = parseInt(themonth)||0;
                $.post("calendar.php",{get_events_of_month:themonth},function(eventdata){
                    eventdata = $.parseJSON(eventdata);
                    $("#calendar table td").each(function(){
                        var a = parseInt($(this).find("a").text())||0;
                        if(eventdata[a] > 0){
                            $(this).addClass('high_ligh');
                        }else{
                            $(this).removeClass('high_ligh');
                        }
                    });
                })
            };
            var hidePopUpBox = function(){
                $("#popUpBox").removeAttr('style');
                $("#popUpBox").css({'transform':'scaleY(0.0)'});
                $("#popUpBox").fadeOut();
                $("#xfade").fadeOut();
            };
        </script>
   </head>
   <body>
      	<div id="body-wrapper">
        	<div id="sidebar"><?php include("common/left_menu.php") ?></div>
         	<div id="bodyWrapper">
            	<div class="content-box-top"  style="padding-bottom: 30px;">
               		<div class="summery_body">
                  		<div class = "content-box-header">
                     		<p> <i class="fa fa-calendar"></i> Calendar</p>
                  		</div><!-- End .content-box-header -->
                        <div id="form">
                            <input type="hidden" value="" id="alternate" value="<?php echo date("d-m-Y"); ?>" />
                            <div class="clear" style="height:10px;"></div>
                            <div class="col-md-8" id="events">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <p class="pull-left" style="padding-top:5px;">Events</p>
                                        <a class="btn  btn-default pull-right" onclick="new_event();"><i class="fa fa-plus"></i> Add Event </a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="panel-body"><p>No Events </p></div>
                                </div>
                            </div>
                            <div class="col-md-4" id="calendar">
                                <div id="calendar"></div>
                            </div>
                        </div>
               		</div><!--summery_body-->
            	</div><!-- End .content-box -->
         	</div><!--bodyWrapper-->
      	</div><!--body-wrapper-->
        <div id="xfade" onclick="hidePopUpBox();"></div>
        <div id="popUpBox" class="custom" style="display:none;">
            <button class="pointer pull-right" onclick="hidePopUpBox();"><i class="fa fa-times"></i></button>
            <div class="clear"></div>
            <form action="" method="post">
                <input type="hidden" class="id" value="0" name="id" />

                <div style="position:relative">
                    <div class="caption" style="text-align:left;">Time</div>
                    <div class="clear"></div>
                    <div class="field">
                        <input type="text" class="form-control event_datetime" name="event_datetime" value="" />
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="caption" style="text-align:left;">Title</div>
                <div class="clear"></div>
                <div class="field">
                    <input type="text" class="form-control event_title" name="event_title" value="" />
                </div>
                <div class="clear"></div>

                <div class="caption" style="text-align:left;">Description</div>
                <div class="clear"></div>
                <div class="field">
                    <textarea class="form-control description" name="event_description" style="min-height:100px;"></textarea>
                </div>
                <div class="clear" style="height:10px;"></div>

                <div class="field">
                    <button type="button" class="btn btn-default removeonsave" onclick="save_event();">Save</button>
                    <button type="button" class="btn btn-default" onclick="hidePopUpBox();">Cancel</button>
                </div>
                <div class="clear"></div>

            </form>
        </div>
	</body>
</html>
<?php include('conn.close.php'); ?>
