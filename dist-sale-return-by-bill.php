<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include 'common/config.php';
	include('common/classes/dist_sale.php');
	include('common/classes/dist_sale_details.php');
	include('common/classes/dist_sale_return.php');
	include('common/classes/dist_sale_return_details.php');
	include('common/classes/items.php');
	include('common/classes/itemCategory.php');
	include('common/classes/departments.php');
	include('common/classes/accounts.php');
	include('common/classes/tax-rates.php');

	//Permission
	if(!in_array('sale-returns',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objAccounts          = new ChartOfAccounts();
	$objSale              = new DistributorSale();
	$objSaleDetails       = new DistributorSaleDetails();
	$objSaleReturn        = new DistributorSaleReturn();
	$objSaleReturnDetails = new DistributorSaleReturnDetails();
	$objItems             = new Items();
	$objItemCategory      = new itemCategory();
	$objTaxRates          = new TaxRates();
	$objDepartments       = new Departments();

	$suppliersList   = $objSaleReturn->getCustomersList();
	$departmentList  = $objDepartments->getList();
	$taxRateList     = $objTaxRates->getList();

	$sale_id = 0;
	$sale_return_id = 0;

	if(isset($_GET['sid'])){
		$sale_id = mysql_real_escape_string($_GET['sid']);
		if($sale_id != '' && $sale_id > 0){
			$sale                    = $objSale->getDetail($sale_id);
			$saleDetails             = $objSaleDetails->getList($sale_id);
            $sale_discount_type      = $sale['DISCOUNT_TYPE'];
            $sale_discount_type_name = ($sale['DISCOUNT_TYPE'] == 'P')?"Percentage":"Amount";
		}
	}else{
		header('location:sale.php');
		eixt();
	}
	$items_array = array();
	$ref_url     = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'sale.php';
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <style>
    	table td{
    		text-align:  center !important;
				padding: 5px !important;
    	}
    	.panel{
    		padding: 0px;
    	}
    	hr{
    		margin: 10px;
    	}
        input.taxRate{
            text-align: center;
        }
        table input{
            text-align: center;
        }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script src="resource/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/dist.sale.returnbybill.config.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".over_discount").calculateFinalAmount();
            $("input.select-all-cbox").change(function(){
                $("input.select-item-row").prop("checked",$(this).prop("checked"));
            });
            $("input.return-selected-items").click(function(){
                $("input.return-selected-items").prop('disabled',true);
                $("table.input_table tbody.trans-rows").html("");
                $("tr.display-row").each(function(){
                    if(!$(this).find("input.select-item-row").prop("checked")){
                        return;
                    }
                    var tr='';
                    tr =  '<tr class="alt-row calculations transactions" data-row-id="0">';
                    tr +=  $(this).html();
                    tr +=  '    <td class="text-center"> - - - </td>';
                    tr +=  '    <td class="text-center">';
                    tr +=  '        <a id="view_button" onclick="editThisRow(this);" title="Update">';
                    tr +=  '            <i class="fa fa-pencil"></i>';
                    tr +=  '        </a>';
                    tr +=  '        <a class="pointer" do="" title="Delete">';
                    tr +=  '            <i class="fa fa-times"></i>';
                    tr +=  '        </a>';
                    tr +=  '    </td>';
                    tr +=  '</tr>';
                    $("table.input_table tbody.trans-rows").append(tr);
                    $("table.input_table td.checkbox-td").remove();
                    setTimeout(function(){
                        $("input.return-selected-items").prop('disabled',false);
                    }, 2000);
                });
                $(this).calculateColumnTotals();
                $(".over_discount").trigger("keyup");
            });
        });
    </script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Distributor Sale Return Panel</p>
                <span id="tabPanel">
                    <div class="tabPanel">
<?php
						if(isset($_GET['page'])){
							$page = "&page=".$_GET['page'];
						}else{
							$page = '';
						}
?>
                        <a href="dist-sale.php?tab=list<?php echo $page; ?>"><div class="tab">List</div></a>
                        <a href="dist-sale.php?tab=search"><div class="tab">Search</div></a>
                        <div class="tabSelected">Return</div>
                        <a href="dist-sale-details.php"><div class="tab">Sale</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding: 5px;" >
                <div id="bodyTab1">
                    <div class="col-md-12">
												<input type="hidden" class="discount_type" value="<?php echo $sale_discount_type; ?>" />
                    		<div class="panel_title">Sale Details </div>
                            <div class="panel panel-default">
                            	<div class="panel-heading">
                                    <input type="hidden" name="order_taker_id" value="<?php echo $sale['ORDER_TAKER'] ?>" />
                                    <input type="hidden" name="salesman_id" value="<?php echo $sale['USER_ID'] ?>" />
                                    <div class="caption_simple pull-left">Order Date :  <?php echo date("d-m-Y",strtotime($sale['ORDER_DATE'])); ?></div>
                            		<div class="caption_simple pull-left">Exe. Date :  <?php echo date("d-m-Y",strtotime($sale['SALE_DATE'])); ?></div>
                            		<?php
                            				if(substr($sale['CUST_ACC_CODE'], 0,6) == '010101'){
                            					$customer_name = $sale['CUSTOMER_NAME'];
											}else{
												$customer_name = $objAccounts->getAccountTitleByCode($sale['CUST_ACC_CODE']);
											}
											if(substr($sale['CUST_ACC_CODE'], 0,6) == '010101'){
                            		?>
                                    <div class="caption_simple pull-left">Cash Sales</div>
                            		<?php
											}
                                            if(substr($sale['CUST_ACC_CODE'], 0,6) != '010101'){
                            		?>
                            		<div class="caption_simple pull-left">Customer :  	   <?php echo $customer_name; ?></div>
                                    <?php
                                            }
                                    ?>
                            		<div class="caption_simple pull-left">Bill # :        <?php echo (isset($sale))?$sale['BILL_NO']:''; ?></div>
                                    <div class="caption_simple pull-left">Disc. Type :    <?php echo (isset($sale_discount_type_name))?$sale_discount_type_name:''; ?></div>
                                    <div class="caption_simple pull-left">PO # :          <?php echo (isset($sale))?$sale['PO_NUMBER']:''; ?></div>
	                            	<div class="clearfix"></div>
                            	</div>
		                        <input type="hidden" class="sale_id" value="<?php echo $sale_id; ?>" />
		                        <input type="hidden" class="sale_return_id" value="<?php echo $sale_return_id; ?>" />
                                <input type="hidden" class="po_number" value="<?php echo (isset($sale))?$sale['PO_NUMBER']:""; ?>" />
<?php
						if(isset($saleDetails) && mysql_num_rows($saleDetails)){
							$crtns          = 0;
							$qty  	        = 0;
                            $dzns  	        = 0;
							$stotal         = 0;
							$ttotal         = 0;
							$atotal         = 0;
                            $dicount_stotal = 0;
?>
                            <table class="wel_made">
                            <thead>
                                <tr>
                                   <th width="4%"  class="text-center bold-text">
                                    <input type="checkbox" class="select-all-cbox" />
                                   </th>
                                   <th width="15%" class="text-center bold-text">Item</th>
								   <th width="8%"  class="text-center bold-text">Cartons</th>
								   <th width="8%"  class="text-center bold-text">CartonRate</th>
                                   <th width="5%"  class="text-center bold-text">Dzns</th>
								   <th width="5%"  class="text-center bold-text">DznsRate</th>
                                   <th width="8%"  class="text-center bold-text">Quantity</th>
                                   <th width="8%"  class="text-center bold-text">Unit Price</th>
                                   <th width="8%"  class="text-center bold-text">Discount</th>
                                   <th width="8%"  class="text-center bold-text">Sub Total</th>
                                   <th width="8%"  class="text-center bold-text"> Tax @ </th>
                                   <th width="8%"  class="text-center bold-text">Tax Amount</th>
                                   <th width="9%"  class="text-center bold-text">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                            $data_desc = 0;
							while($invRow = mysql_fetch_array($saleDetails)){
								$itemName = $objItems->getItemTitle($invRow['ITEM_ID']);
								$items_array[$invRow['ITEM_ID']] = $objItems->getRecordDetails($invRow['ITEM_ID']);
                                if($sale_discount_type == 'P'){
                                    $data_desc =  ($invRow['QUANTITY']*$invRow['UNIT_PRICE']) - (($invRow['QUANTITY']*$invRow['UNIT_PRICE']*$invRow['SALE_DISCOUNT'])/100);
                                }else{
                                    $data_desc = $invRow['SALE_DISCOUNT'];
                                }
								$qty_carton = $objItems->getItemQtyPerCarton($invRow['ITEM_ID']);
?>

	                <tr class="alt-row display-row" data-row-id='<?php echo $invRow['ID']; ?>'>
                        <td class="text-center checkbox-td">
                            <input type="checkbox" class="select-item-row" />
                        </td>
                        <td class="text-left itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
                        <td class="text-center cartons" data-qty="<?php echo $qty_carton; ?>"><?php echo $invRow['CARTONS'] ?></td>
                        <td class="text-center rate_carton"><?php echo $invRow['RATE_CARTON'] ?></td>
                        <td class="text-center dozens"><?php echo $invRow['DOZENS'] ?></td>
                        <td class="text-center dozen_price"><?php echo $invRow['DOZEN_PRICE'] ?></td>
	                    <td class="text-center quantity"><?php echo $invRow['QUANTITY'] ?></td>
	                    <td class="text-center unit_price" data-saleprice-id="<?php echo $invRow['ITEM_ID']; ?>"><?php echo $invRow['UNIT_PRICE'] ?></td>
	                    <td class="text-center sale_discount" data-dsct="<?php echo $data_desc; ?>" ><?php echo $invRow['SALE_DISCOUNT'] ?></td>
	                    <td class="text-center sub_amount" ><?php echo $invRow['SUB_AMOUNT'] ?></td>
	                    <td class="text-center tax_rate" ><?php echo $invRow['TAX_RATE'] ?></td>
	                    <td class="text-center tax_amount" ><?php echo $invRow['TAX_AMOUNT'] ?></td>
	                    <td class="text-center total_amount" ><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
	                </tr>
<?php
								$crtns  += $invRow['CARTONS'];
                                $dzns   += $invRow['DOZENS'];
								$qty    += $invRow['QUANTITY'];
								$stotal += $invRow['SUB_AMOUNT'];
								$ttotal += $invRow['TAX_AMOUNT'];
								$atotal += $invRow['TOTAL_AMOUNT'];

                                if($sale_discount_type == 'P'){
                                    $dicount_stotal += (($invRow['QUANTITY']*$invRow['UNIT_PRICE'])*$invRow['SALE_DISCOUNT'])/100;
                                }else{
                                    $dicount_stotal += $invRow['SALE_DISCOUNT'];
                                }
							}
?>
                            </tbody>
                            <tfoot>
                                <tr class="totals">
                                    <td class="text-center">Total</td>
                                    <td class="text-center"><?php echo $crtns; ?></td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"><?php echo $dzns; ?></td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"><?php echo $qty; ?></td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"><?php echo number_format($stotal,2,'.',''); ?></td>
                                    <td class="text-center"> - - - </td>
                                    <td class="text-center"><?php echo number_format($ttotal,2,'.',''); ?></td>
                                    <td class="text-center"><?php echo number_format($atotal,2,'.',''); ?></td>
                                </tr>
                            </tfoot>
                        </table>

                        <div class="clear"></div>
                        </div>

                        <div class="clear"></div>

                        <?php
                            $grand_total = 0;
                            $grand_total+= $atotal;

                            if($sale_discount_type == 'P'){
                                $grand_total-= (($atotal*$sale['DISCOUNT'])/100);
                            }else{
                                $grand_total-= $sale['DISCOUNT'];
                            }

                            $grand_total+= $sale['CHARGES'];
                        ?>

                        <div class="pull-right" style="padding: 0em 1em;width:80%;">
                            <div class="pull-right">

                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;opacity:0.0;height: 30px;">Discount :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right"  value="<?php echo $sale['DISCOUNT']; ?>" style="opacity:0.0;" />
                                </div>

                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Total Discount :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right total_discount"  value="<?php echo $sale['SALE_DISCOUNT']; ?>" />
                                </div>
                                <div class="clear" style="height:5px;"></div>
                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Delivery/Other Charges :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right inv_charges"  value="<?php echo $sale['CHARGES']; ?>" />
                                </div>
                                <div class="caption pull-left text-right" style="padding: 5px;width: 180px;height: 30px;">Net Total :</div>
                                <div class="caption pull-left" style="width: 150px;margin-left:0px;">
                                    <input type="text" class="form-control text-right" readonly="readonly" value="<?php echo number_format($grand_total,2,'.',''); ?>" />
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="clear"></div>

<?php
						}
?>
						<div class="panel_title">Sale Return Panel </div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<div class="caption_simple">
								Return Date
								<input type="text" name="rDate" value="<?php echo date('d-m-Y'); ?>" class="datepicker input_ghost" style="width:150px;padding-left: 40px;margin-left: -30px;" />
							</div>

                            <div class="caption_simple">
                                <input type="button" class="return-selected-items button-orange" value="Return Selected Items"  />
                            </div>
							<div class="clear"></div>
						</div>
                        <table class="input_table">
                            <thead>
                                <tr>
                                   <th width="10%" class="text-center bold-text">Item</th>
                                   <th width="8%"  class="text-center bold-text">Cartons</th>
                                   <th width="8%"  class="text-center bold-text">CartonRate</th>
                                   <th width="5%"  class="text-center bold-text">Dzns</th>
								   <th width="5%"  class="text-center bold-text">DznsRate</th>
                                   <th width="8%"  class="text-center bold-text">Quantity</th>
                                   <th width="8%"  class="text-center bold-text">Unit Price</th>
                                   <th width="8%"  class="text-center bold-text">Discount</th>
                                   <th width="8%"  class="text-center bold-text">Sub Total</th>
                                   <th width="8%"  class="text-center bold-text"> Tax @ </th>
                                   <th width="8%"  class="text-center bold-text">Tax Amount</th>
                                   <th width="8%"  class="text-center bold-text">Total Amount</th>
                                   <th width="8%"  class="text-center bold-text">Stock</th>
                                   <th width="8%"  class="text-center bold-text">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr class="quickSubmit" style="background:none">
                                    <td class="p-0">
                                        <select class="itemSelector show-tick form-control"
                                                data-style="btn-default"
                                                data-live-search="true" style="border:none">
                                           <option selected value=""></option>
<?php
											foreach ($items_array as $key => $value){
												$cat = $objItemCategory->getTitle($key)

?>
                                           			<option  value="<?php echo $value['ID']; ?>" ><?php echo $value['NAME']; ?></option>
<?php
											}
?>
                                        </select>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="cartons form-control"/>
                                        <input type="hidden" class="qty_carton" />
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="rate_carton form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="dozens form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="dozen_price form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="quantity form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="unitPrice form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="discount form-control"/>
                                    </td>
                                    <td style="padding:0; line-height:0;">
                                        <input type="text"  readonly value="0" class="subAmount form-control"/>
                                    </td>
                                    <td style="padding:0; line-height:0;" class="taxTd">
                                    	<input class="taxRate form-control"></input>
                                    </td>
                                    <td style="padding:0; line-height:0;">
                                        <input type="text"  readonly value="0" class="taxAmount form-control"/>
                                    </td>
                                    <td style="padding:0; line-height:0;">
                                        <input type="text"  readonly value="0" class="totalAmount form-control"/>
                                    </td>
                                    <td class="p-0">
                                        <input type="text" class="inStock form-control" theStock="0" readonly />
                                        <input type="hidden" class="qty_limit" />
                                        <input type="hidden" class="qty_returned" />
                                        <input type="hidden" class="carton_limit" />
                                        <input type="hidden" class="dozen_limit" />
                                    </td>
                                    <td style="text-align:center;background-color:#f5f5f5;"><input type="button" class="addDetailRow" value="Enter" id="clear_filter" /></td>
                                </tr>
                         	</tbody>
                            <tbody class="trans-rows">
                            	<!--doNotRemoveThisLine-->
                                  <tr style="display:none;" class="calculations"></tr>
                              <!--doNotRemoveThisLine-->
<?php
						if(isset($saleDetails) && mysql_num_rows($saleDetails)){
							while($invRow  = mysql_fetch_array($saleDetails)){
								$itemName    = $objItems->getItemTitle($invRow['ITEM_ID']);
								$qty_carton  = $objItems->getItemQtyPerCarton($invRow['ITEM_ID']);
?>
                                <tr class="alt-row calculations transactions" data-row-id='<?php echo $invRow['ID']; ?>'>
                                    <td class="text-left itemName" data-item-id='<?php echo $invRow['ITEM_ID']; ?>'><?php echo $itemName; ?></td>
                                    <td class="text-center cartons" data-qty="<?=$qty_carton;?>"><?php echo $invRow['CARTONS'] ?></td>
                                    <td class="text-center rate_carton"><?php echo $invRow['RATE_CARTON'] ?></td>
                                    <td class="text-center quantity"><?php echo $invRow['QUANTITY'] ?></td>
                                    <td class="text-center unit_price"><?php echo $invRow['UNIT_PRICE'] ?></td>
                                    <td class="text-center sale_discount"><?php echo $invRow['SALE_DISCOUNT'] ?></td>
                                    <td class="text-center sub_amount"><?php echo $invRow['SUB_AMOUNT'] ?></td>
                                    <td class="text-center tax_rate"><?php echo $invRow['TAX_RATE'] ?></td>
                                    <td class="text-center tax_amount"><?php echo $invRow['TAX_AMOUNT'] ?></td>
                                    <td class="text-center total_amount"><?php echo $invRow['TOTAL_AMOUNT'] ?></td>
                                    <td style="text-align:center;"> - - - </td>
                                </tr>
<?php
							}
						}
?>
                            </tbody>
                            <tfoot>
                                <tr class="totals">
                                    <td class="text-center" >Total</td>
                                    <td class="text-center cartnTotal"></td>
                                    <td class="text-center" >- - -</td>
                                    <td class="text-center dzn_total"></td>
                                    <td class="text-center" >- - -</td>
                                    <td class="text-center qtyTotal"></td>
                                    <td class="text-center" >- - -</td>
                                    <td class="text-center discountAmount"></td>
                                    <td class="text-center amountSub"></td>
                                    <td class="text-center" >- - -</td>
                                    <td class="text-center amountTax"></td>
                                    <td class="text-center amountTotal"></td>
                                    <td class="text-center" >- - -</td>
                                    <td class="text-center" >- - -</td>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                        <div class="underTheTable">
                            <div class="pull-right col-xs-4">
                                <div class="col-xs-6 text-right hide" style="line-height:30px;">Discount</div>
                                <div class="pull-right col-xs-6 hide">
                                    <input type="text" class="form-control text-right over_discount" />
                                </div>
                                <div class="clear"></div>
                                <div class="col-xs-6 text-right" style="line-height:30px;">Total Discount</div>
                                <div class="pull-right col-xs-6">
                                    <input type="text" class="form-control text-right bill_discount" />
                                </div>
                                <div class="clear" style="height:10px;"></div>
                                <div class="col-xs-6 text-right" style="line-height:30px;">Total</div>
                                <div class="pull-right col-xs-6">
                                    <input type="text" class="form-control text-right over_total" readonly />
                                </div>
                                <div class="clear" style="height:10px;"></div>
                            </div>
                            <div class="clear"></div>
			                <div class="button pull-right saveSale">Save</div>
                        </div><!--underTheTable-->
                        <div class="clear"></div>
                        <div style="margin-top:10px"></div>
                        <div class="panel_title">Recent Sale Return </div>
						<div class="recent_returns"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="dialog-confirm" style="display:none;" title="Empty the recycle bin?">
    	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<?php include('conn.close.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
        $(window).keyup(function(e){
            if(e.altKey == true&&e.keyCode == 83){
                e.preventDefault();
                $("#form").click();
                saveSale();
                return false;
            }
        });
		$('select').selectpicker();
		$(document).calculateColumnTotals();
		 $("div.recent_returns").on('click','button.delete_sale_return',function(){
			$(this).deletePanel("db/del-dist-sale-return.php");
		});
		$("#form").change(function(){
			get_recent_sale_returns("div.recent_returns");
		});
		$("input.bill_number").numericOnly();
		$("input.cartons").numericFloatOnly();
		$("input.rate_carton").numericFloatOnly();
		$("input.quantity").numericFloatOnly();
		$("input.unitPrice").numericFloatOnly();
		$("input.discount").numericFloatOnly();
		$("#datepicker").setFocusTo("div.supplierSelector button");
		$("div.supplierSelector button").keyup(function(e){
			var supp = $("select.supplierSelector option:selected").val();
			if(e.keyCode == 13 && supp != ''){
				$(".bill_number").focus();
			}
		});
		$("input.bill_number").setFocusTo("div.itemSelector button");
		$('div.itemSelector button').keyup(function(e){
			if(e.keyCode ==13 && $("select.itemSelector option:selected").val() != ''){
				$(".barcode_input").attr('data-focus','');
				$(this).getItemReturnDetails();
				$("input.quantity").focus();
			}
		});
    $('select.itemSelector').change(function(){
        if($("select.itemSelector option:selected").val() != ''){
            $(this).getItemReturnDetails();
            $("input.cartons").focus();
        }
    });
		$("input.quantity,input.cartons").on('keyup blur',function(e){
			stockOs();
		});
		$("input.cartons").keydown(function(e){
			if(e.keyCode == 13){
				$("input.rate_carton").focus();
			}
		});
		$("input.rate_carton").keydown(function(e){
			if(e.keyCode == 13){
				$("input.dozens").focus();
			}
		});
        $("input.dozens").keydown(function(e){
			if(e.keyCode == 13){
                $(this).calculateRowTotal();
				$("input.dozen_price").focus();
			}
		});
        $("input.dozen_price").keydown(function(e){
			if(e.keyCode == 13){
                $(this).calculateRowTotal();
				$("input.quantity").focus();
			}
		});
		$("input.quantity").keydown(function(e){
			if(e.keyCode == 13){
				$("input.unitPrice").focus();
			}
		});
		$("input.unitPrice").keydown(function(e){
			if(e.keyCode == 13){
				$("input.discount").focus();
			}
		});
		$("input.discount").keydown(function(e){
			if(e.keyCode == 13){
				$(this).calculateRowTotal();
				$("input.taxRate").focus();
			}
		});
    $("input.taxRate").keydown(function(e){
        if(e.keyCode == 13){
            $(this).calculateRowTotal();
            $(".addDetailRow").focus();
        }
    });
		$("input.quantity").multiplyTwoFloatsCheckTax("input.unitPrice","input.totalAmount","input.subAmount","input.taxAmount");
		$("input.unitPrice").multiplyTwoFloatsCheckTax("input.quantity","input.totalAmount","input.subAmount","input.taxAmount");
		$(".addDetailRow").keydown(function(e){
			if(e.keyCode == 13){
				$(this).quickSave();
			}
		});
		$(".addDetailRow").click(function(){
			$(this).quickSave();
		});
		$(".saveSale").click(function(){
			saveSale();
		});
		get_recent_sale_returns("div.recent_returns");
    });
<?php
		if(isset($_GET['saved'])){
?>
			displayMessage('Sales Return Saved Successfully!');
<?php
		}
?>
<?php
		if(isset($_GET['updated'])){
?>
			displayMessage('Sales Return Updated Successfully!');
<?php
		}
?>

</script>