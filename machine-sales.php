<?php
	$out_buffer = ob_start();
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/machine_sales.php');
	include('common/classes/sheds.php');
	include('common/classes/machines.php');
	include('common/classes/items.php');
	include('common/classes/j-voucher.php');
	include('common/classes/accounts.php');

	//Permission
	if( (!in_array('stock-taking',$permissionz)) && ($admin != true) ){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objMachineSales   = new machineSales();
	$objJournalVoucher = new JournalVoucher();
	$objAccountCodes   = new ChartOfAccounts();
	$objMachines       = new machines();
	$objSheds 		   	 = new Sheds();

	if(isset($_GET['delete'])){
		$machine_sale_id = (int)mysql_real_escape_string($_POST['id']);
		$voucher_id 		 = $objMachineSales->getVoucherId($machine_sale_id);
		$objJournalVoucher->reverseVoucherDetails($voucher_id);
		$objJournalVoucher->deleteJv($voucher_id);
		$objMachineSales->delete($machine_sale_id);
		echo json_encode(array('OKAY'=>'Y'));
		exit();
	}

	$total 			  = $objConfigs->get_config('PER_PAGE');
	$totalRows    = $objMachineSales->countRows();
	$machineList  = $objMachines->getList();
	$machine_list = array();

	if(isset($_POST['submit'])){

		$machine_sale_id  							 = isset($_POST['machine_sale_id'])?(int)mysql_real_escape_string($_POST['machine_sale_id']):0;
		$objMachineSales->entry_date 	 	 = date('Y-m-d',strtotime($_POST['entry_date']));
		$objMachineSales->machine_id     = (int)$_POST['machine_id'];

		$objMachineSales->issued_length  = 0;
		$objMachineSales->issued_amount  = 0;
		$objMachineSales->clothLength 	 = mysql_real_escape_string($_POST['clothLength']);
		$objMachineSales->measure 		 	 = mysql_real_escape_string($_POST['measure']);
		$objMachineSales->unitRate 		 	 = mysql_real_escape_string($_POST['unitRate']);
		$objMachineSales->amount 		 		 = mysql_real_escape_string($_POST['amount']);

		$shedAccCode 	= $objMachines->getShedAccCode($objMachineSales->machine_id);
		$shedAccTitle = $objAccountCodes->getAccountTitleByCode($shedAccCode);

		$voucher_id = 0;
		if($machine_sale_id==0){
			$machine_sale_id = $objMachineSales->save();
		}else{
			$objMachineSales->update($machine_sale_id);
			$voucher_id = $objMachineSales->getVoucherId($machine_sale_id);
		}

		if($machine_sale_id){

			$objJournalVoucher->jVoucherNum    	= $objJournalVoucher->genJvNumber();
			$objJournalVoucher->voucherDate    	= date('Y-m-d',strtotime($objMachineSales->entry_date));
			$objJournalVoucher->reference   	  = "Shed ".$shedAccTitle;
			$objJournalVoucher->voucherType   	= 'PV';
			$objJournalVoucher->reference_date 	= date('Y-m-d',strtotime($objMachineSales->entry_date));
			$objJournalVoucher->status	  			= 'P';

			if($voucher_id==0){
				$voucher_id = $objJournalVoucher->saveVoucher();
			}else{
				$objJournalVoucher->reverseVoucherDetails($voucher_id);
				$objJournalVoucher->updateVoucherDate($voucher_id);
			}

			$objJournalVoucher->voucherId    		= $voucher_id;

			$objJournalVoucher->accountCode  		= '0101060004';
			$objJournalVoucher->accountTitle 		= $objAccountCodes->getAccountTitleByCode($objJournalVoucher->accountCode);
			$objJournalVoucher->narration    		= "Month Ending  ".$objMachineSales->entry_date." Stock Adjusting Entry In ".$shedAccTitle;
			$objJournalVoucher->transactionType = 'Dr';
			$objJournalVoucher->amount       		= $objMachineSales->amount;

			$objJournalVoucher->saveVoucherDetail();

			$objJournalVoucher->accountCode  		= $shedAccCode;
			$objJournalVoucher->accountTitle 		= $shedAccTitle;
			$objJournalVoucher->narration    		= "Closing Stock For The Month Ending ".$objMachineSales->entry_date;
			$objJournalVoucher->transactionType = 'Cr';

			$objJournalVoucher->saveVoucherDetail();

			$objMachineSales->insertVoucherId($machine_sale_id,$voucher_id);
			$pagePreFix = (isset($_GET['page']))?"&page=".$_GET['page']:"";
			echo "<script>window.location.href = \"machine-sales.php?saved".$pagePreFix."\";</script>";
		}
		exit();
	}

	if(isset($_GET['search'])){
		$fromDate = date('Y-m-d',strtotime($_GET['fromDate']));
		$toDate   = date('Y-m-d',strtotime($_GET['toDate']));
		$machine_id  = $_GET['machine_id'];
		$machineSalesList = $objMachineSales->search($fromDate,$toDate,$machine_id);
	}else{
		$fromDate = date('Y-m-01');
		$toDate   = date('Y-m-d');
		$machineSalesList = $objMachineSales->search($fromDate,$toDate,'');
	}
	if(isset($_GET['id'])){
		$machine_sale_id = (int)mysql_real_escape_string($_GET['id']);
		$machineSale = $objMachineSales->getDetails($machine_sale_id);
	}
?>
<!DOCTYPE html 
>



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/style.css" type="text/css"  			     							/>
		<link rel="stylesheet" href="resource/css/invalid.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/form.css" type="text/css" 	 		       							/>
		<link rel="stylesheet" href="resource/css/tabs.css" type="text/css"  				     							/>
		<link rel="stylesheet" href="resource/css/reports.css" type="text/css"  		     							/>
		<link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css"  	     							/>
		<link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/animate.css" type="text/css" 			  					/>
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css"  								/>
		<link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css"  							/>
		<link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css"  />
		<style media="screen">
			#bodyTab2 table td{
				font-size: 12px !important;
				padding: 6px !important;
			}
			#bodyTab2 table th{
				font-size: 12px !important;
				padding: 6px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript">
			$(function () {
				$('select').selectpicker();
				$("button.dropdown-toggle").focus();
				$("ul.selectpicker:first li a").click(function(){
					var accountTitle = $(this).children("span.text").text();
					if(accountTitle!==""){
							$("select.selectpicker option").each(function(index, element){
								if($(this).text()==accountTitle){
									var accountsCode = $(this).val();
									$("input.machineAccCode").val(accountsCode);
									setTimeout(function(){
										$("input[name='issuedLength']").last().focus();
									},300);
								}
							});
					}else{
						$("input.machineAccCode").val("");
					}
				});
				$("input.datepicker").setFocusTo("button.dropdown-toggle");
				$("select[name='machine_id']").change(function(){
					$("input[name='measure']").focus();
				});
				$("input[name='issuedLength']").setFocusTo("input[name='issuedAmount']");
				$("input[name='issuedAmount']").setFocusTo("input[name='measure']");
				$("input[name='measure']").setFocusTo("input[name='clothLength']");
				$("input[name='clothLength']").setFocusTo("input[name='unitRate']");
				$("input[name='unitRate']").setFocusTo("input[type='submit']");
				$("input[name='clothLength']").numericInputOnly();
				$("input[name='unitRate']").numericInputOnly();
				$("input[name='amount']").numericInputOnly();
				$("input[name='clothLength']").keyup(function(){
					$(this).multiplyTwoFeilds("input[name='unitRate']","input[name='amount']");
				});
				$("input[name='unitRate']").keyup(function(){
					$(this).multiplyTwoFeilds("input[name='clothLength']","input[name='amount']");
				});
			});
		</script>
		<!-- jQuery Configuration -->
		<script type="text/javascript" src="resource/scripts/machine.sales.configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?PHP include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Machine Cloth Adjusting Entry</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="<?php echo $fileName; ?>"><div class="tab" id="tab2">List</div></a>
                        <div class="tab" data-toggle="modal" data-target="#myModal">Search</div>
                        <div class="tabSelected" id="tab1" onclick="tab('1','1','2');">Form</div>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">
                            <div class="caption">Entry Date :</div>
                            <div class="field" style="width:150px">
                                <input type="text" name="entry_date" value="<?php echo (isset($machineSale))?date('d-m-Y',strtotime($machineSale['ENTRY_DATE'])):date('d-m-Y'); ?>" class="form-control datepicker" style="width:145px" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Machine Title:</div>
                            <div class="field" >
                                <select class="selectpicker show-tick form-control"  data-live-search="true" name="machine_id">
                                       <option selected value=""></option>
<?php
                                if(mysql_num_rows($machineList)){
                                    while($machine_row = mysql_fetch_array($machineList)){
																			$machineSelected = '';
																			if(isset($machineSale)){
																				$machineSelected = ($machine_row['ID'] == $machineSale['MACHINE_ID'])?"selected='selected'":"";;
																			}
																			$machine_list[] = $machine_row;
?>
                                       <option value="<?php echo $machine_row['ID']; ?>" <?php echo $machineSelected; ?> ><?php echo $machine_row['MACHINE_NO']." ".$machine_row['NAME']; ?></option>
<?php
                                    }
                                }
?>
                                </select>
                            </div>
                            <div class="clear"></div>

                            <div class="caption">Measure:</div>
                            <div class="field" >
                                <input type="text" name="measure" value="<?php echo (isset($machineSale))?$machineSale['MEASURE']:""; ?>" class="form-control" />
                            </div>
                            <div class="clear"></div>



                            <div class="caption">Cloth length:</div>
                            <div class="field" >
                                <input type="text" name="clothLength" value="<?php echo (isset($machineSale))?$machineSale['CLOTH_LENGTH']:""; ?>" class="form-control" />
                            </div>
                            <div class="clear"></div>


                            <div class="caption">EmbRate:</div>
                            <div class="field" >
                                <input type="text" name="unitRate" value="<?php echo (isset($machineSale))?$machineSale['EMB_RATE']:""; ?>" class="form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption">Amount:</div>
                            <div class="field" >
                                <input type="text" name="amount" value="<?php echo (isset($machineSale))?$machineSale['AMOUNT']:""; ?>" class="form-control" readonly="readonly" />
                            </div>
                            <div class="clear"></div>
                            <div class="caption"></div>
                            <div class="field">
<?php
							if(isset($machine_sale_id)){
?>
								<input type="hidden" name="machine_sale_id"  value="<?php echo $machine_sale_id; ?>" />
								<input type="submit" name="submit" value="Update" class="btn btn-default" />
								<input type="button" name="" value="New Form" class="btn btn-info" onclick="window.location.href = 'machine-sales.php';"  />
<?php
							}else{
?>
								<input type="submit" name="submit" value="Save" class="btn btn-default" />
<?php
							}
?>
                            </div>
                            <div class="clear"></div>
                        </form>
                        <div style="margin-top:10px"></div>
					 </div> <!-- End form -->
                </div> <!-- End #tab1 -->
                <div id="bodyTab2" style="display: none;">
                   <table style="margin:0 25px; width:95%" cellspacing="0" >
                        <thead>
                            <tr>
                               <th width="15%" style="text-align:center">Adjusting Date</th>
                               <th width="20%" style="text-align:center">Machine Title</th>
                               <th width="15%" style="text-align:center">Measure</th>
                               <th width="15%" style="text-align:center">Cloth Length</th>
                               <th width="10%" style="text-align:center">EmbRate</th>
                               <th width="10%" style="text-align:center">Amount</th>
                               <th width="10%" style="text-align:center">Action</th>

                            </tr>
                        </thead>
                        <tbody>
<?php
					$numberOfRows = mysql_num_rows($machineSalesList);
					$clothLengthTotal = 0;
					$embRateTotal 	  = 0;
					$embAmountTotal   = 0;
					if(mysql_num_rows($machineSalesList)){
						while($row = mysql_fetch_array($machineSalesList)){
							$machineDetail = $objMachines->getRecordDetailsByID($row['MACHINE_ID']);
							if(isset($_GET['shed_id']) && $_GET['shed_id'] != $machineDetail['SHED_ID']){
								if($_GET['shed_id'] != ''){
									continue;
								}
							}
?>
                        	<tr>
                            		<td class="text-center"><?php echo date('d-m-Y',strtotime($row['ENTRY_DATE'])); ?></td>
                                <td class="text-center"><?php echo $machineDetail['MACHINE_NO']."-".$machineDetail['NAME']; ?></td>
                                <td class="text-center"><?php echo $row['MEASURE']; ?></td>
                                <td class="text-center"><?php echo $row['CLOTH_LENGTH']; ?></td>
                                <td class="text-center"><?php echo $row['EMB_RATE']; ?></td>
                                <td class="text-center"><?php echo $row['AMOUNT']; ?></td>
                                <td class="text-center">
<?Php
								if(isset($_GET['page'])){
									$page = "&page=".$_GET['page'];
								}else{
									$page = "";
								}
?>
                                	<a id="view_button" href="machine-sales.php?id=<?php echo $row['ID'].$page; ?>" title="View"><i class="fa fa-pencil"></i></a>
                                	<a class="pointer deleteRow" do="<?php echo $row['ID']; ?>" title="Delete"><i class="fa fa-times"></i></a>
                                </td>
							</tr>

<?php
						$clothLengthTotal += $row['CLOTH_LENGTH'];
						$embRateTotal 	  += $row['EMB_RATE'];
						$embAmountTotal   += $row['AMOUNT'];
						}
					}
?>
                        </tbody>

						<tfoot>
                        	<tr>
                            	<td colspan="3" style="text-align:right;">Total</td>
                            	<td style="text-align: center;"><?php echo $clothLengthTotal; ?></td>
                                <td style="text-align: center;"><?php echo $embRateTotal; ?></td>
                                <td style="text-align: center;"><?php echo $embAmountTotal; ?></td>
                                <td style="text-align: center;"> - - - </td>
                            </tr>
<?php
						if(isset($_GET['search'])){
?>
                        	<tr>
                          	<td class="text-center" colspan="7">
                              	<button class="button" onclick="clearSearchResults();"><i class="fa fa-refresh"></i> Clear Search</button>
                              </td>
                          </tr>
<?php
						}
?>
                        </tfoot>
                    </table>
                </div> <!-- End #tab2 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog">
				  <div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <h4 class="modal-title">Search</h4>
				      </div>
				      <div class="modal-body">
								<form method="post" action="<?php echo $fileName; ?>">
												<div class="form-group col-xs-6">
											    <label for="fromDate">From Date:</label>
											    <input type="text" name="fromDate" class="form-control datepicker" id="fromDate">
											  </div>
												<div class="clear"></div>

												<div class="form-group col-xs-6">
											    <label for="toDate">To Date:</label>
											    <input type="text" name="toDate" class="form-control datepicker" id="toDate" value="<?php echo date('d-m-Y'); ?>">
											  </div>
												<div class="clear"></div>

												<div class="form-group col-xs-6">
											    <label for="shed_id_select">Sheds:</label>
													<select class="form-control show-tick" name="shed_id" id="shed_id_select">
														<option value="0"></option>
								<?php
													$shedList 	 = $objSheds->getList();
													if(mysql_num_rows($shedList)){
														while($shed = mysql_fetch_array($shedList)){
								?>
														<option value="<?php echo $shed['SHED_ID']; ?>"><?php echo $shed['SHED_TITLE']; ?></option>
								<?php
														}
													}
								?>
													</select>
											  </div>
												<div class="clear"></div>


												<div class="form-group col-xs-6">
											    <label for="toDate">Machine:</label>
													<select class="form-control show-tick popMech" name="machine_id">
				                    	<option value="0"></option>
<?php
									foreach($machine_list as $key => $mech){
?>
										<option value="<?php echo $mech['ID']; ?>"><?php echo $mech['MACHINE_NO']; ?> - <?php echo $mech['NAME']; ?></option>
<?php
									}
?>
				                    </select>
											  </div>
												<input type="hidden" class="button" name="search" value="Search" />
												<div class="clear"></div>
					    	</form>
				      </div>
				      <div class="modal-footer">
								<button type="button" class="btn btn-primary btn-sm" onclick="$('#myModal form').submit();"> <i class="fa fa-search"></i>	 </button>
				        <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
				      </div>
				    </div>

				  </div>
				</div>
    </div><!--body-wrapper-->
    <div id="xfade"></div>
    <div id="fade"></div>
    <div id="popUpForm"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(".deleteRow").click(function(){
			$(this).deleteRowDefault('<?php echo $fileName; ?>?delete');
		});
  });
	<?php echo isset($machine_sale_id)?"tab('1','1','2');":"tab('2','1','2');"; ?>
</script>
