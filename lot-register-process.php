<?php
	include 'common/connection.php';
	include 'common/config.php';
	include 'common/classes/emb_lot_register.php';
	include 'common/classes/measure.php';
	include 'common/classes/emb_products.php';
	include 'common/classes/machines.php';
	include 'common/classes/emb_stitch_account.php';
	include 'common/classes/customers.php';
	include 'common/classes/emb_inward.php';
	include 'common/classes/accounts.php';
	include 'common/classes/j-voucher.php';

	$objLotRegister 			= new EmbLotRegister;
	$objMeasure  					= new Measures;
	$objMachines 					= new machines;
	$objEmbStitchAccount	= new EmbStitchAccount;
	$objEmbProducts 			= new EmbProducts;
	$objCustomers  				= new customers;
	$objInward    				= new EmbroideryInward();
	$objJournalVoucher 		= new JournalVoucher;
	$objChartOfAccounts 	= new ChartOfAccounts;

	$customersList = $objCustomers->getList();
	$productList   = $objEmbProducts->getList();
	$measureList   = $objMeasure->getList();
	$machineList   = $objMachines->getList();

	if(isset($_GET['lid'])){
		$lid = $_GET['lid'];
		$lot_details = $objLotRegister->getDetail($lid);
	}
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Admin Panel</title>
		<link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
		<style media="screen">
			table.input_table th,table.input_table td{
				padding: 10px !important;
			}
			tr.quickSubmit td{
				padding: 1px !important;
			}
			div.red{
				color: red;
			}
			span.red{
				color:red;
			}
			span.green{
				color:green;
			}
			tr{
				transition: all 300ms ease-in;
				-webkit-transition: all 300ms ease-in;
				-moz-transition: all 300ms ease-in;
			}
			tr td{
				font-size:12px !important;
			}
		</style>
		<script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
		<script type="text/javascript" src="resource/scripts/emb.lot.register.config.js"></script>
    <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
		<script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript">
    	$(window).on('load', function () {
			function redirectTo(fileLocation){
				window.location.href = fileLocation;
			}
			$('select').selectpicker();
			$("button.dropdown-toggle").last().focus();
        });
    </script>
    <script type="text/javascript" src="resource/scripts/outwardQuickInserts.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
    </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Lot Process</p>
                <span id="tabPanel">
                    <div class="tabPanel">
	                    <a href="lot-register.php?tab=list"><div class="tab">List</div></a>
                        <a href="lot-register.php?tab=search"><div class="tab">Search</div></a>
                        <a href="emb-lot-register.php"><div class="tab">Add</div></a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <div id="bodyTab1">
                    <div id="form">
                    	<form method="post" action="">
                            <div class="caption" style="width:140px">Customer Title</div>
                            <div class="field" style="width:300px;position:relative;">
                                <select class="custCodeSelector show-tick form-control" data-live-search="true" data-width="250" >
                                   <option selected value=""></option>
<?php
                            if(mysql_num_rows($customersList)){
                                while($account = mysql_fetch_array($customersList)){
									$selected = '';
									if(isset($lot_details)){
										$selected = ($lot_details['CUST_ACC_CODE'] == $account['CUST_ACC_CODE'])?"selected='selected'":"";
									}elseif(isset($fetch_customer_lots)){
										$selected = ($fetch_customer_lots == $account['CUST_ACC_CODE'])?"selected='selected'":"";
									}
?>
                                   <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>" <?php echo $selected; ?> ><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                                }
                            }
?>
                                </select>
																<input type="button" onclick="getLotList();" class="btn btn-primary btn-sm" value="Go" />
                            </div>
                        		<div class="clear"></div>
                    	</form>
                    </div> <!-- End form -->
                </div> <!-- End #tab1 -->
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box-top-->

        <div class="content-box-body" style="display:none;top: 220px;">
            <div class="content-box-header">
                <p>Lots Under Process</p>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->

            <div class="content-box-content" style="padding:0;">

                <div id="bodyTab1">
                    <div id="form">
                            <div class="clear"></div>
                            <table class="prom">
                                <thead>
                                    <tr>
                                       <th width="5%"  style="font-size:11px; text-align:center">Prod<br />Code</th>
                                       <th width="15%" style="font-size:11px; text-align:center">Product</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Lot#</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Quality</th>
                                       <th width="5%"  style="font-size:11px;text-align:center;">Measure</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Length</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">@Emb</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">@Stitch</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Amount</th>
                                       <th width="10%"  style="font-size:11px; text-align:center;">Stitch A/c</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Design#</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">Machine</th>
                                       <th width="5%"  style="font-size:11px; text-align:center">GatePass#</th>
                                       <th width="8%"  style="font-size:11px; text-align:center">Action</th>
                                       <th width="8%"  style="font-size:11px; text-align:center">
                                       		<input type="checkbox" onclick="checkLots(this);" />
                                       </th>
                                    </tr>
                                </thead>
                            </table>
														<div class="clear"></div>
                            <div class="col-xs-12 mt-20">
                							<button  class="btn btn-info" onclick="processSelectedLots();" >Lots Proccessed <i class="fa fa-arrow-right"></i></button>
                    				</div>
		                    <div class="clear"></div>
                    </div><!--form-->
                </div><!--bodyab1-->
            </div><!--content-box-content-->
      </div><!--content-box-body-->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm3" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
            	<p>New Machine:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="newMachineTitle" class="input_size newMachineTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm2" class="popUpFormStyle">
    	<i class="fa fa-times"></i>
    	<div id="form">
            	<p>New Stitch Account:</p>
                <p class="textBoxGo">
                	<input type="text" value="" name="newStitchTitle" class="input_size newStitchTitle" />
                    <i class="fa fa-arrow-right"></i>
                </p>
        </div>
    </div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		$(".lotNum").keyup(function(e){
			if(e.keyCode==13){
				$(this).getProductQualities();
			}
		});
		$(".measure").keydown(function(e){
			if(e.keyCode==13){
				$(".length").focus();
			}
		});
		$(".length").keyup(function(e){
			if(e.keyCode==13&&$(this).val()!==""){
				$(".embRate").focus();
			}
			$(".thaanLengthOS").val($(".thaanLengthOS").attr("inwardOS")-$(".length").val());
			if($(".thaanLengthOS").val()<0){
				$("#popUpDel").each(function(index, element) {
                    $(this).remove();
                });
				$("body").append("<div id='popUpDel'><span class='close_popup'></span><p class='confirm'> You Can Not Issue Quantity exceeding Total Quantity!</p><a class='nodelete'>Ok</a></div>");
				$("#popUpDel").hide();
				$("#xfade").fadeIn();
				$("#popUpDel").fadeIn();
				$("#popUpDel").centerThisDiv();
				$(".nodelete").click(function(){
					$("#popUpDel").slideUp(function(){
						$("#popUpDel").remove();
					});
					$("#xfade").fadeOut();
					$(".getItemTitle").focus();
				});
			}
		});
		$(".embRate").keyup(function(e){
			if(e.keyCode==13&&$(this).val()!==""){
				$(".stitchRate").focus();
			}else{
				$(this).multiplyTwoFeilds(".length",".embAmount");
			}
		});
		$(".stitchRate").keyup(function(e){
			if(e.keyCode==13&&$(this).val()!==""){
				$("select[name='stitchAccount']").focus();
			}else{
				$(this).multiplyTwoFeilds(".length",".stitchAmount");
			}
		});
		$("select[name='stitchAccount']").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				$(".designNum").focus();
			}
		});
		$(".designNum").keydown(function(e){
			if(e.keyCode==13&&$(this).val()!==""){
				$(".machineNum").focus();
			}
		});
		$(".machineNum").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				$(".gp_numer").focus();
			}
		});
		$(".gp_numer").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				$(".addDetailsQuick").focus();
			}
		});
		$("a.pointer").click(function(){
			$(this).deleteRow();
		});
		$(".editDetailsFrom").click(function(){
			$(this).editOutwardDetails();
		});
		$(".addDetailsQuick").click(function(){
			$(this).quickSubmit();
		});
		$(".addDetailsQuick").keydown(function(e){
			if(e.keyCode==13){
				$(this).quickSubmit();
			}
		});
		var selectedFlag = false;
		$(".qualityDropDown").keydown(function(e){
			if(e.keyCode==13){
				e.preventDefault();
				if(selectedFlag==false){
					selectedFlag = true;
				}else{
					$("select.measure").focus();
					selectedFlag = false;
				}
			}else{
				selectedFlag = false;
			}
		});
		$(".qualityDropDown").change(function(e){
			$(this).getProductDetail();
		});
		$("#datepicker").setFocusTo(".getCustomerTitle");
		$(".getCustomerTitle").keydown(function(e){
			if(e.keyCode==13){
				if($(this).val()=="")
				{
					e.preventDefault();
				}
			}
		});
		$("input.insert_title").keydown(function(e){
				if(e.keyCode==16){
					$("div.listEye").accountList();
				}
			});
			$("input.insert_title").keyup(function(e){
				if(e.keyCode==16){
					$("div#fade").fadeOut('fast');
					$("div.popupList").fadeOut('fast');
				}
			});
			$("div.listEye").click(function(){
				$("div.listEye").accountList();
			});
		$(".productSelector").change(function(){
			$(".get_code_for_prod").val($(this).val());
			$(".lotNum").focus();
		});
		$("select.custCodeSelector").change(function(){
			$(".insertCustomerCode").val($(this).val());
			//$(this).getAllLotsByCustomer();
		});
<?php
		if(isset($fetch_customer_lots)){
?>
			getLotList();
			$(".insertCustomerCode").val('<?php echo $fetch_customer_lots; ?>');
<?php
		}
?>
		$(".lengthColumn").sumColumn(".sumLenTotal");
		$(".embAmountColumn").sumColumn(".embAmountTotal");
		$(".stichAmountColumn").sumColumn(".stichAmountTotal");
		$("div.journalizeThis").click(function(){
			$(this).journalizeThis("input[name='outwardDate']","input[name='custAccCode']","input[name='outwd_id']");
		});
		$(".reverseThisVoucher").click(function(){
			$(this).reverseVoucher("input[name='outwd_id']","input[name='custAccCode']","input[name='outwardDate']");
		});
    });
</script>
