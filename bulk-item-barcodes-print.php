<?php
	include("common/connection.php");
    include("common/classes/items.php");
    include("common/classes/company_details.php");

    $objItems = new Items;
    $objCompanyInfo = new CompanyDetails;

    $companyName = $objCompanyInfo->getTitle(1);
    $companyName = substr($companyName, 0,21);
?>
<!DOCTYPE html 
>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link href="resource/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="resource/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="resource/css/labels.css" type="text/css" />

    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="resource/scripts/printThis.js"></script>
    <script>
    	$(document).ready(function(){
            $("select").selectpicker();
    		$(".startPrint").click(function(){
    			window.print();
    		});
            $(".submit_bar").click(function(e){
                e.preventDefault();
                $('div.make_clone').each(function(){
                    var id = $(this).find("select.sector_bash option:selected").val();
                    $(this).find("input.the_num").prop('name','number'+id);
                });
                $("form[name='grow-form']").submit();
                return false;
            });
    	});
        var add_item = function(){
            var elm = $('div.make_clone').last().clone();
            $(elm).insertAfter($('div.make_clone').last());
            $('div.make_clone').last().find("div.sector_bash").remove();
            $("select").selectpicker('refresh');
        }
    </script>
    <style type="text/css">
        .make_clone{
            float: left;
            margin: 5px 0px !important;
        }
        .form-control{
            width: 100%;
        }
        div.sector_bash{
            width: 100%;
        }
    </style>
</head>
<body style="background-image:none !important;background-color:#FFF !important;">
<?php
    if(!isset($_POST['id'])){
        $items_list = $objItems->getListAll();
    ?>
    <div class="col-xs-12">
        <div class="panel panel-default" style="padding-bottom:1em;">
            <div class="panel-heading">
                <h5 class="pull-left">Print Item Barcodes</h5>
                <button type="button" onclick="add_item();" class="btn btn-default pull-right"><i class="fa fa-plus"></i></button>
                <div class="clear"></div>
            </div>
            <form method="post" accept="" name="grow-form">
                <div class="clear"></div>
                <div class="col-xs-12">
                    <div class="col-xs-10">Items</div>
                    <div class="col-xs-2">Quantity</div>
                </div>
                <div class="clear"></div>
                <div class="col-xs-12 make_clone">
                    <div class="col-xs-10">
                        <select class="sector_bash form-control show-tick" data-live-search="true" name="id[]" data-show-subtext="true">
                        <?php
                        if(mysql_num_rows($items_list)){
                            while($item_row = mysql_fetch_assoc($items_list)){
                                if($item_row['ITEM_BARCODE'] == ''){
                                    continue;
                                }
                        ?>
                            <option data-subtext="<?php echo $item_row['ITEM_BARCODE'] ?>" value="<?php echo $item_row['ID'] ?>"><?php echo $item_row['NAME'] ?></option>
                        <?php
                            }
                        }
                        ?>
                        </select>
                    </div>
                    <div class="field col-xs-2"><input type="text" value="0" class="form-control the_num"  /></div>
                </div>
                <div class="clear" style="height:10px;"></div>

                <div class="col-xs-12">
                    <div class="col-xs-10">
                        Company Title
                    </div>
                    <div class="col-xs-2 text-center">
                        <input id="cmn-toggle-1" checked name="company" class="cmn-toggle cmn-toggle-round checkActive" type="checkbox" >
                        <label for="cmn-toggle-1"></label>
                    </div>
                    <div class="clear"></div>

                    <div class="col-xs-10">
                        Item Title
                    </div>
                    <div class="col-xs-2 text-center">
                        <input id="cmn-toggle-2" checked name="item-name" class="cmn-toggle cmn-toggle-round checkActive" type="checkbox" >
                        <label for="cmn-toggle-2"></label>
                    </div>
                    <div class="clear"></div>

                    <div class="col-xs-10">
                        Price
                    </div>
                    <div class="col-xs-2 text-center">
                        <input id="cmn-toggle-3" checked name="sale-price" class="cmn-toggle cmn-toggle-round checkActive" type="checkbox" >
                        <label for="cmn-toggle-3"></label>
                    </div>
                    <div class="clear"></div>

                    <div class="col-xs-10">
                        Code
                    </div>
                    <div class="col-xs-2 text-center">
                        <input id="cmn-toggle-4" checked name="code" class="cmn-toggle cmn-toggle-round checkActive" type="checkbox" >
                        <label for="cmn-toggle-4"></label>
                    </div>
                    <div class="clear"></div>

                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12"><input type="submit" value="Generate" class="button submit_bar" /></div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
<?php 
    }else{
    ?>
    <button class="btn btn-warning dont-print" onclick="window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>';"><i class="fa fa-arrow-left"></i> Back </button>
    <button class="button startPrint dont-print"><i class="fa fa-print"></i> Print </button>

	<div class="barcode-print">
		<?php
            foreach($_POST['id'] as $key => $id){
                $item_details = $objItems->getRecordDetails($id);
                if($item_details == NULL){
                    continue;
                }
                $number = isset($_POST['number'.$id])?$_POST['number'.$id]:0;
                $item_details['NAME'] = substr($item_details['NAME'], 0,21);
                for ($i=1; $i <= $number; $i++){
                ?>
                    <div id="lable">
                        <div class="line">
                            <?php if(isset($_POST['company'])){ ?>
                                <?php echo $companyName; ?>
                            <?php } ?>
                        </div>
                        <div class="line">
                            <?php if(isset($_POST['item-name'])){ ?>
                                <?php echo $item_details['NAME']; ?>
                            <?php } ?>
                        </div>
                        <div class="right">
                         <div class="barcode">
                             <img src="lib/barcode.php?size=30&text=<?php echo $item_details['ITEM_BARCODE']; ?>" />
                            </div>
                            <div class="barcodeNo pull-left" style="font-size:10px;margin-top:4px;">
                                <?php if(isset($_POST['sale-price'])){ ?>
                                    Rs.<?php echo $item_details['SALE_PRICE']; ?>/=
                                <?php } ?>
                            </div>
                            <div class="barcodeNo pull-right">
                                <?php if(isset($_POST['code'])){ ?>
                                    <?php echo $item_details['ITEM_BARCODE']; ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div><!--lable-->
        <?php
                }
            }
        ?>
        <div class="clear"></div>
	</div><!--barcode-print-->
    <?php
        }
        ?>
        <script type="'text/javascript'">
            $(document).ready(function(){
                $(".startPrint").click(function(){
                    window.print();
                });
            });
        </script>
</body>
</html>
<?php include('conn.close.php'); ?>