<?php
    include('common/connection.php');
    include 'common/config.php';
    include('common/classes/sale.php');
    include('common/classes/accounts.php');
    include('common/classes/j-voucher.php');
    include('common/classes/items.php');
    include('common/classes/itemCategory.php');
    include('common/classes/customers.php');
    include('common/classes/invoice.php');
    include('common/classes/invoice_details.php');

    //Permission
    if(!in_array('invoice-sales',$permissionz) && $admin != true){
        echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
        echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
        echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
        echo '</div>';
        exit();
    }
    //Permission ---END--

    $objSale           = new Sale();
    $objJournalVoucher = new JournalVoucher();
    $objAccountCodes   = new ChartOfAccounts();
    $objItems          = new Items();
    $objItemCategory   = new ItemCategory();
    $objCustomers      = new Customers();
    $objConfigs        = new Configs();
    $objInvoice        = new Invoice();
    $objInvoiceDetails = new InvoiceDetails();

    $total               = $objConfigs->get_config('PER_PAGE');
    $customersList       = $objCustomers->getList();

    //Pagination Settings
    $objPagination = new Pagination;
    $total = $objPagination->getPerPage();
    //Pagination Settings { END }

    if(isset($_GET['page'])){
        $this_page = $_GET['page'];
        if($this_page>=1){
            $this_page--;
            $start = $this_page * $total;
        }
    }else{
        $start = 0;
        $this_page = 0;
    }

    if(isset($_POST['cid'])){
        $array = array();
        $invoice_id = (int)(mysql_real_escape_string($_POST['cid']));
        $voucher_id = $objInvoice->getVoucherId($invoice_id);
        $objInvoice->delete($invoice_id);
        $objInvoiceDetails->deleteByMain($invoice_id);
        if($voucher_id){
            $objJournalVoucher->reverseVoucherDetails($voucher_id);
            $objJournalVoucher->deleteJv($voucher_id);
        }
        $array['OK'] = 'Y';
        $array['MSG'] = 'Invoice deleted successfully!';
        echo json_encode($array);
        exit();
    }

    if(isset($_GET['search'])){
        $objInvoice->from_date   = ($_GET['fromDate'] == '')?"":date('Y-m-d',strtotime($_GET['fromDate']));
        $objInvoice->to_date     = ($_GET['toDate'] == '')?"":date('Y-m-d',strtotime($_GET['toDate']));
        $objInvoice->customer   = mysql_real_escape_string($_GET['supplierAccCode']);
        $objInvoice->invoice_no = $_GET['billNum'];
    }

    $invoice_list = $objInvoice->getPages($start,$total);

    $found_records = $objInvoice->found_records;
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SIT Solutions</title>
    <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
    <style type="text/css">
        .ui-effects-transfer {
            border: 1px solid #CCC;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        }
    </style>
    <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
    <script type="text/javascript"  src="resource/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>
    <script type="text/javascript"  src="resource/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/scripts/configuration.js"></script>
    <script type="text/javascript" src="resource/scripts/project.expense.js"></script>
    <script type="text/javascript" src="resource/scripts/jquery.form.js"></script>
    <script type="text/javascript" src="resource/scripts/tab.js"></script>
    <script type="text/javascript" >
        $(document).ready(function() {
            $("select").selectpicker();
            $("*[data-toggle=tooltip]").tooltip();
            $("a.pointer").on("click",function(){
                $(this).deleteMainRow("invoice-sales-list.php");
            });
            var page_url = $("input.page-url").val();
            $("div.pagination-container a").each(function(i,e){
                var href = $(this).attr("href");
                $(this).attr("href",href+page_url);
            });
        });
    </script>
    <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
</head>
<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <?php include("common/left_menu.php") ?>
        </div> <!-- End #sidebar -->
        <div class="content-box-top">
            <div class="content-box-header">
                <p>Invoice Management</p>
                <span id="tabPanel">
                    <div class="tabPanel">
                        <a href="invoice-sales-list.php?mode=list"    class="<?php echo (isset($_GET['mode'])&&$_GET['mode'] == 'list')?"tabSelected":"tab"; ?>">List</a>
                        <a href="invoice-sales-list.php?mode=search"  class="<?php echo (isset($_GET['mode'])&&$_GET['mode'] == 'search')?"tabSelected":"tab"; ?>">Search</a>
                        <a href="invoice-sales-details.php?mode=form"    class="<?php echo (isset($_GET['mode'])&&$_GET['mode'] == 'form')?"tabSelected":"tab"; ?>">Details</a>
                    </div>
                </span>
                <div class="clear"></div>
            </div> <!-- End .content-box-header -->
            <div class="clear"></div>
            <div class="content-box-content">
                <div id="bodyTab1">
                    <div id="form">
                        <div class="clear"></div>

                    <?php if(isset($_GET['mode']) && $_GET['mode'] == 'search'){ ?>
                    <form>
                        <div class="title">Invoices Search</div>
                        <div class="clear"></div>

                        <input type="hidden" name="mode" value="list" />
                        <div class="caption">From Date</div>
                        <div class="field">
                            <input type="text" name="fromDate" class="form-control datepicker" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">To Date</div>
                        <div class="field">
                            <input type="text" name="toDate" class="form-control datepicker" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Customer</div>
                        <div class="field">
                            <select class="supplierAccCode form-control show-tick" name="supplierAccCode"
                                    data-style="btn-default" data-width="250"
                                    data-live-search="true" data-hide-disabled='true' style="border:none;" >
                               <option selected value=""></option>
<?php
                        if(mysql_num_rows($customersList)){
                            while($account = mysql_fetch_array($customersList)){
?>
                               <option data-subtext="<?php echo $account['CUST_ACC_CODE']; ?>" value="<?php echo $account['CUST_ACC_CODE']; ?>"><?php echo $account['CUST_ACC_TITLE']; ?></option>
<?php
                        }
                    }
?>
                            </select>
                        </div>
                        <div class="clear"></div>

                        <div class="caption">Invoice #</div>
                        <div class="field">
                            <input type="text" name="billNum" class="form-control" />
                        </div>
                        <div class="clear"></div>

                        <div class="caption"></div>
                        <div class="field">
                            <input type="submit" name="search" class="button" value="Search" />
                        </div>
                        <div class="clear"></div>
                    </form>
                    <?php } ?>

                    <?php if(isset($_GET['mode']) && $_GET['mode'] == 'list'){ ?>
                        <div class="title">Invoices List</div>
                        <div class="clear"></div>

                        <table class="table" id="invoice_rows">
                            <thead>
                                <tr>
                                    <th class="text-center col-xs-1">Invoice#</th>
                                    <th class="text-center col-xs-1">InvoiceDate</th>
                                    <th class="text-center col-xs-2">Narration</th>
                                    <th class="text-center col-xs-3">Customer</th>
                                    <th class="text-center col-xs-1">Amount</th>
                                    <th class="text-center col-xs-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total_amount = 0.00;
                                    $counter = $start+1;
                                    if(isset($invoice_list)&&mysql_num_rows($invoice_list)){
                                        while($row = mysql_fetch_assoc($invoice_list)){
                                            ?>
                                                <tr class="invoice_rows alt-row" data-row-id="0">
                                                    <td class="text-center"><?php echo "G-".$row['INVOICE_NO'] ?></td>
                                                    <td class="text-center description"><?php echo date("d-m-Y",strtotime($row['INVOICE_DATE'])); ?></td>
                                                    <td class="text-center"><?php echo $row['SUBJECT'] ?></td>
                                                    <td class="text-center"><?php echo $objAccountCodes->getAccountTitleByCode($row['CUSTOMER']); ?></td>
                                                    <td class="text-center"><?php echo $row['TOTAL_AMOUNT'] ?></td>
                                                    <td class="text-center">
                                                        <a href="invoice-sales-details.php?id=<?php echo $row['ID']; ?>" id="view_button"><i class="fa fa-pencil"></i></a>
                                                        <a href="new-invoice-print.php?id=<?php echo $row['ID']; ?>" target="_blank" id="view_button"><i class="fa fa-print"></i></a>
                                                        <a class="pointer" do="<?php echo $row['ID']; ?>"> <i class="fa fa-times"></i> </a>
                                                    </td>
                                        </tr>
                                            <?php
                                            $counter++;
                                            $total_amount+=$row['TOTAL_AMOUNT'];
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                        <div class="pagination-container">
                <?php
                        if($found_records > $total){

                            $url = "";
                            foreach ($_GET as $key => $value){
                                if($key == 'page'){
                                    continue;
                                }
                                $url .= "&".$key."=".$value;
                            }
    ?>
                            <input type="hidden" value="<?php echo $url; ?>" class="page-url" />
                                  <ul class="pagination">
    <?php
                            $count = $found_records;
                            $total_pages = ceil($count/$total);
                            $i = 1;
                            $thisFileName = $_SERVER['PHP_SELF'];
                            if(isset($this_page) && $this_page>0){
    ?>
                                <li>
                                    <a href="<?php echo $thisFileName; ?>?page=1">First</a>
                                </li>
    <?php
                            }
                            if(isset($this_page) && $this_page>=1){
                                $prev = $this_page;
    ?>
                                <li>
                                    <a href="<?php echo $thisFileName; ?>?page=<?php echo $prev; ?>">Prev</a>
                                </li>
    <?php
                            }
                            $this_page_act = $this_page;
                            $this_page_act++;
                            while($total_pages>=$i){
                                $left = $this_page_act-5;
                                $right = $this_page_act+5;
                                if($left<=$i && $i<=$right){
                                $current_page = ($i == $this_page_act)?"active":"";
    ?>
                                    <li class="<?php echo $current_page; ?>">
                                        <?php echo "<a href=\"".$thisFileName."?page=".$i."\">".$i."</a>"; ?>
                                    </li>
    <?php
                                }
                                $i++;
                            }
                            $this_page++;
                            if(isset($this_page) && $this_page<$total_pages){
                                $next = $this_page;
                                echo " <li><a href='".$thisFileName."?page=".++$next."'>Next</a></li>";
                            }
                            if(isset($this_page) && $this_page<$total_pages){
    ?>
                                    <li><a href="<?php echo $thisFileName; ?>?page=<?php echo $total_pages; ?>">Last</a></li>
                                </ul>
    <?php
                            }
                        }
    ?>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                <?php } ?>
                </div> <!--End bodyTab1-->
                <div class="clear"></div>
            </div> <!-- End .content-box-content -->
        </div> <!-- End .content-box -->
    </div><!--body-wrapper-->
    <div id="fade"></div>
    <div id="xfade"></div>
    <div id="popUpForm" class="popUpFormStyle"></div>
</body>
</html>
