<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/accounts.php');
	include('common/classes/emb_outward.php');
	include('common/classes/emb_inward.php');
	include('common/classes/ledger_report.php');
	include('common/classes/emb_lot_register.php');
	include('common/classes/emb_products.php');
	include('common/classes/company_details.php');

	//Permission
	if(!in_array('emb-outward-print',$permissionz) && $admin != true){
		echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
		echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
		echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
		echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
		echo '</div>';
		exit();
	}
	//Permission ---END--

	$objChartOfAccounts     		= new ChartOfAccounts();
	$objLedgerReport 				= new ledgerReport();
	$objOutwards 					= new outward();
	$objInwards  					= new EmbroideryInward();
	$objLotRegisterDetails  		= new EmbLotRegister();
	$objEmbProducts  				= new EmbProducts();
	$objCompanyDetails 				= new CompanyDetails();

	if(isset($_POST['gp_no'])){
		$gp_no 					= (int)mysql_real_escape_string($_POST['gp_no']);
		$outward_record =  $objOutwards->getRecordByBillNumber($gp_no);
		echo (int)$outward_record['OUTWD_ID'];
		exit();
	}

	$customersList  				= $objOutwards->getCustomersList();
	$productList 						= $objOutwards->getProductList();
	$company_details        = $objCompanyDetails->getActiveProfile();
?>
	<!DOCTYPE html>
	

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Admin Panel</title>
		<link rel="stylesheet" type="text/css" href="resource/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="resource/css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="resource/css/tooltipster.css" />
		<link rel="stylesheet" type="text/css" href="resource/css/emb_invoice_style.css" />
		<?php if(!isset($_GET['id'])){ ?>
		<link rel="stylesheet" type="text/css" href="resource/css/style.css" />
		<link rel="stylesheet" type="text/css" href="resource/css/form.css" />
		<link rel="stylesheet" type="text/css" href="resource/css/tabs.css" />
		<?php } ?>
		<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
		<script type="text/javascript" src="resource/scripts/jquery.tooltipster.min.js"></script>
		<script type="text/javascript" src="resource/scripts/printThis.js"></script>
		<script type="text/javascript">
		$(window).load(function(){
			$(".printThis").tooltipster({
			   animation: 'grow',
			   position: 'right',
			   theme : 'tooltipster-light',
			   delay: 100,
			   maxWidth : '250',
			   trigger: 'hover'
			});
			$(".printThis").click(function(){
				$(".invoiceContainer").printThis({
			      debug: false,
			      importCSS: false,
			      printContainer: true,
			      loadCSS: "resource/css/emb_invoice_style.css",
			      pageTitle: "Sitsbook",
			      removeInline: false,
			      printDelay: 500,
			      header: null
			  });
			});
			//$(".printThis").click();
			$("input#ogp_no").change(function(){
				var gp_no =  $(this).val();
				$.post('?',{gp_no:gp_no},function(data){
					data = parseInt(data)||0;
					if(data>0){
						window.open("emb-outward-invoice.php?id="+data,'Sitsbook').focus();
						$("input#ogp_no").val('');
					}
				});
			});
		});
	</script>
	</head>

	<body>
		<?php
	if(!isset($_GET['id'])){
?>
			<div id="body-wrapper">
				<div id="sidebar">
					<?php include("common/left_menu.php") ?>
				</div>
				<!-- End #sidebar -->
				<div class="content-box-top">
					<div class="content-box-header">
						<p>Outward Gate Pass Print</p>
						<div class="clear"></div>
					</div>
					<!-- End .content-box-header -->
					<div class="content-box-content">
						<div class="col-xs-offset-4 col-xs-4" style="margin-top: 1em;padding: 1em;">
							<div class="form-group">
								<label for="ogp_no">Outward GP # :</label>
								<input type="text" class="form-control text-center" id="ogp_no" autofocus>
							</div>
						</div>
					</div>
					<!-- End .content-box-content -->
				</div>
			</div>
			<?php
	}elseif(isset($_GET['id'])){
		$outward_id 			= mysql_real_escape_string($_GET['id']);
		$invoiceDetails 	= $objOutwards->getInvoice($_GET['id']);
		$claimList 				= $objOutwards->getClaimIdArray($_GET['id']);
		$invoiceHasRows 	= mysql_num_rows($invoiceDetails);
		if($invoiceHasRows || $claimList != NULL){
			$invoiceDetails = mysql_fetch_assoc($invoiceDetails);
			if(!$invoiceHasRows){
				$invoiceDetails = mysql_fetch_assoc($objOutwards->getSpecificRecord($_GET['id']));
			}
			$customerDetails 	= $objOutwards->getCutomer($invoiceDetails['CUST_ACC_CODE']);
			$ledgerOpeningBalance = $objLedgerReport->getInvoiceOpeningBalance($invoiceDetails['CUST_ACC_CODE'],$invoiceDetails['OUTWD_DATE'],$invoiceDetails['VOUCHER_ID']);
?>
				<button class="btn btn-info printThis pull-right no-print m-10"> <i class="fa fa-print"></i> Print </button>
				<?php if(in_array('emb-outward-modify',$permissionz) || $admin == true){ ?>
				<a href="emb-outward-details.php?wid=<?php echo $outward_id; ?>" target="_blank" class="btn btn-default pull-right no-print m-10">Edit</a>
				<?php } ?>
				<a href="embroidery-designs-print.php?id=<?php echo $outward_id; ?>" class="btn btn-default no-print m-10">Print Designs</a>
				<div class="clear"></div>
				<div class="invoiceContainer">
					<div class="invoiceLeftPrint">
						<div class="invoiceHead">
							<div class="biz_logo">
								<img src="uploads/logo/<?php echo $company_details['LOGO']; ?>" alt="company_logo" />
							</div>
							<aside class="logo_aside">
								<h1 class="h1" style="margin-top: 0px !important;">
									<?php echo $company_details['NAME']; ?>
								</h1>
								<p class="slogan">
									<?php echo $company_details['SLOGAN']; ?>
								</p>
								<p class="underSlogan">
									<?php echo $company_details['ADDRESS']; ?>
								</p>
								<p class="underSlogan">
									<?php echo ($company_details['CONTACT']=='')?"":"Contact # ".$company_details['CONTACT']; ?>
								</p>
							</aside>
							<div class="clear"></div>
							<div class="partyTitle" style="text-align:center;font-weight:bold;">
								OUTWARD GATE PASS
							</div>
							<div class="clear"></div>
							<div class="partyTitle" style="font-size: 16px;">
								<span class="pull-left" style="padding: 0px 0px;">Date. <?php echo date('d-M-y',strtotime($invoiceDetails['OUTWD_DATE'])); ?></span>
								<span class="pull-right" style="padding:0px 10px;">G.P No. <?php echo $invoiceDetails['BILL_NO']; ?></span>
							</div>
							<!--partyTitle-->
							<div class="clear mt-10"></div>
							<div style="width: 50%;float: left;padding-left: 5px;">
								<span class="goal"> <b>Buyer's Name </b> : M/s. <?php echo $invoiceDetails['CUST_ACC_TITLE']; ?></span>
								<span class="red page_format"></span>
							</div>
							<!--partyTitle-->
							<div style="width: 48%;float: left;">
								<span class="goal">  <b>Transport</b> : <?php echo $invoiceDetails['TRANSPORT']; ?></span>
								<span class="red page_format"></span>
							</div>
							<!--partyTitle-->
							<div class="clear mt-10"></div>
							<?php if($invoiceDetails['THIRD_PARTY_CODE'] != ''){ ?>
							<div style="width: 50%;float: left;padding-left: 5px;">
								<span class="goal"> <b> Party </b> : <?php echo $objChartOfAccounts->getAccountTitleByCode($invoiceDetails['THIRD_PARTY_CODE']); ?></span>
								<span class="red page_format"></span>
							</div>
							<!--partyTitle-->
							<div style="width: 48%;float: left;">
							</div>
							<!--partyTitle-->
							<div class="clear mt-10"></div>
							<?php } ?>
							<?php if($invoiceDetails['CLOTH'] != ''){ ?>
							<div style="width: 50%;float: left;padding-left: 5px;">
								<span class="goal"> <b>Cloth</b> : <?php echo $invoiceDetails['CLOTH']; ?></span>
								<span class="red page_format"></span>
							</div>
							<!--partyTitle-->
							<?php } ?>
							<?php if($invoiceDetails['YARN'] != ''){ ?>
							<div style="width: 48%;float: left;">
								<span class="goal">  <b>Yarn</b> : <?php echo $invoiceDetails['YARN']; ?></span>
								<span class="red page_format"></span>
							</div>
							<!--partyTitle-->
							<?php } ?>
							<div class="clear"></div>
						</div>
						<!--invoiceHead-->
						<div class="clear"></div>
						<div class="invoiceBody">
							<table style="width: 99%;margin: 0px auto;">
								<thead>
									<tr>
										<th width="10%">Design#</th>
										<th width="30%">Description</th>
										<th width="10%">Thaan</th>
										<th width="10%">Total Yards</th>
										<th width="10%">Production</th>
									</tr>
								</thead>
								<tbody>
									<?php
						$remarks 						= array();
						$outwardDetailsList = $objOutwards->getOutwardDetails($invoiceDetails['OUTWD_ID']);
						$yards 						  = 0;
						$thaan_suit_total   = 0;
						$totalAmount 			  = 0;
						$production_total   = 0;

						if($invoiceHasRows){
							if(mysql_num_rows($outwardDetailsList)){
								while($row = mysql_fetch_array($outwardDetailsList)){
										$stockInward 		   = $objInwards->getInwardLotStockInHandWhloleLot($invoiceDetails['CUST_ACC_CODE'],$row['LOT_NO']);
										$outwardStockDelivered = $objOutwards->getOutwardLotQtyDeliveredWholeLotPrevId($invoiceDetails['CUST_ACC_CODE'],$row['LOT_NO'],$invoiceDetails['OUTWD_DATE'],$row['OUTWD_DETL_ID']);
										if($row['REMARKS'] != ''){
											$remarks[] 			       = $row['REMARKS'];
										}
										$product_title  	   = $objEmbProducts->getTitle($row['PRODUCT_ID']);

										$thaan_suite_col     = $row['MEASURE_LENGTH'];
										$total_yards_col     = '';
										$stitch_yard_col     = '';
										$laces_thaan_col     = '';
										$rate_p_yard_col 		 = $row['EMB_RATE'];
										$total_value_col     = $row['MEASURE_LENGTH']*$row['EMB_RATE'];

										if($row['BILLING_TYPE'] == 'S' || $row['BILLING_TYPE'] == 'Y'){
											$thaan_suite_col = $row['MEASURE_LENGTH']*15;
											$rate_p_yard_col = (($row['STITCHES']*$row['STITCH_RATE'])/1000);
											$total_value_col = (($row['STITCHES']*$row['STITCH_RATE'])/1000)*($row['MEASURE_LENGTH']*15);
										}

										if($row['BILLING_TYPE'] == 'L'){
											$total_value_col += (($row['STITCHES']*$row['STITCH_RATE'])/1000)*($row['MEASURE_LENGTH']*15);
										}


?>
										<tr>
											<td>
												<?php echo $row['DESIGN_CODE']; ?>
											</td>
											<td style="font-size:12px;text-align:left;">
												<?php echo $product_title; ?>
											</td>
											<td style="text-align: center;">
												<?php echo $row['MEASURE_LENGTH']; ?>
											</td>
											<td style="text-align: center;">
												<?php echo $thaan_suite_col; ?>
											</td>
											<td style="text-align: center;">
												<?php echo $row['TOTAL_LACES']; ?>
											</td>
										</tr>
										<?php
										$yards       			+= $row['MEASURE_LENGTH'];
										$totalAmount 			+= $total_value_col;
										$thaan_suit_total += $thaan_suite_col;
										$production_total += $row['TOTAL_LACES'];
								}
							}
						}
						$claimListString = '';
						if($claimList != NULL){
							$claimPrevForLedgerBalance = 0;
							foreach($claimList as $k => $lot_detail_id){
								if($lot_detail_id == ''){
									continue;
								}
								if($k > 0){
									$claimListString .= ',';
								}
								$claimListString .=  $lot_detail_id;

								$row = $objLotRegisterDetails->getDetail($lot_detail_id);
								$stockInward = $objInwards->getInwardLotStockInHandWhloleLot($invoiceDetails['CUST_ACC_CODE'],$row['LOT_NO']);
								$outwardStockDelivered = $objOutwards->getOutwardLotQtyDeliveredWholeLotPrevId($invoiceDetails['CUST_ACC_CODE'],$row['LOT_NO'],$invoiceDetails['OUTWD_DATE'],$row['OUTWD_DETL_ID']);
								$lotTimeStamp = strtotime($row['LOT_DATE']);
								$outwardTimeStamp = strtotime($invoiceDetails['OUTWD_DATE']);
								if($lotTimeStamp < $outwardTimeStamp){
									$ledgerOpeningBalance += str_replace('-','',$row['EMB_AMOUNT']);
								}
?>
											<tr>
												<td>
													<?php echo $row['DESIGN_CODE']; ?>
												</td>
												<td style="font-size:12px;text-align:left;">
													<?php echo $objEmbProducts->getTitle($row['PRODUCT_ID']); ?> <span class="pull-right">*</span>
												</td>
												<td style="font-size:12px !important;text-align:center;">
													<?php echo $row['QUALITY']; ?>
												</td>
												<td style="text-align: center;">
													<?php echo $row['MEASURE_LENGTH']; ?>
												</td>
											</tr>
											<?php
									if($row['EMB_RATE'] < 0 && $row['MEASURE_LENGTH'] > 0){
										$totalAmount += $row['EMB_AMOUNT'];
									}elseif($row['MEASURE_LENGTH'] < 0 && $row['EMB_RATE'] > 0){
										$yards += $row['MEASURE_LENGTH'];
										$totalAmount += $row['EMB_AMOUNT'];
									}elseif($row['EMB_RATE'] < 0 ){
										$totalAmount += $row['EMB_AMOUNT'];
									}
                            }
						}

						$lot_list = $objLotRegisterDetails->getClaimLotListExcept($invoiceDetails['CUST_ACC_CODE'],$claimListString);
						if($claimListString != NULL){
							if(mysql_num_rows($lot_list)){
								while($claimRow = mysql_fetch_array($lot_list)){
									$rowDetail = $objLotRegisterDetails->getDetail($claimRow['ID']);
									$lotTimeStamp = strtotime($rowDetail['LOT_DATE']);
									$outwardTimeStamp = strtotime($invoiceDetails['OUTWD_DATE']);
									if($lotTimeStamp == $outwardTimeStamp){
										$ledgerOpeningBalance -= str_replace('-','',$rowDetail['EMB_AMOUNT']);
									}
								}
							}
						}else{
							//CONDITION IS CONFIRMED
							if(mysql_num_rows($lot_list)){
								while($claimRow = mysql_fetch_array($lot_list)){
									$rowDetail = $objLotRegisterDetails->getDetail($claimRow['ID']);
									$lotTimeStamp = strtotime($rowDetail['LOT_DATE']);
									$outwardTimeStamp = strtotime($invoiceDetails['OUTWD_DATE']);
									if($outwardTimeStamp == $lotTimeStamp){
										$ledgerOpeningBalance -= str_replace('-','',$rowDetail['EMB_AMOUNT']);
									}
								}
							}
						}
?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2" class="text-right"><b>TOTAL</b></td>
										<td>
											<?php echo $yards; ?>
										</td>
										<td>
											<?php echo $thaan_suit_total; ?>
										</td>
										<td>
											<?php echo $production_total; ?>
										</td>
									</tr>
								</tfoot>
							</table>
							<div class="clear" style="height:20px;"></div>
							<?php
				if(count($remarks)){
?>
								<h2 class="h2 textAlignLeft" style="width: 100%;text-decoration:underline;">Remarks :</h2>
								<?php
				}
				foreach($remarks as $k => $value){
					if($value != ''){
						echo "<br />";
						echo $value;
					}
				}
				echo "<br />";
				echo "<br />";
				echo "<br />";
?>
									<h2 class="h2 text-center" style="width: 25%;">
										Prepared By
									</h2>
									<h2 class="h2 text-center" style="width: 25%;">
										General Manager
									</h2>
									<h2 class="h2 text-center" style="width: 25%;">
										Director
									</h2>
									<h2 class="h2 text-center" style="width: 25%;">
										Received By
									</h2>
									<div class="clear" style="height: 20px;"></div>
						</div>
						<!--invoiceBody-->
					</div>
					<!--invoiceLeftPrint-->
				</div>
				<!--invoiceContainer-->
				<?php
		}
	}   //end if $_GET['id']
?>
	</body>

	</html>
