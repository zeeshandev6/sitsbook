<?php
	include('common/connection.php');
	include('common/config.php');
	include('common/classes/items.php');
	include('common/classes/measure.php');
	include('common/classes/itemCategory.php');
	include('common/classes/branch_stock.php');
	include('common/classes/departments.php');
	include('common/classes/mobile-purchase-details.php');

	//Permission
  if(!in_array('items-managment',$permissionz) && $admin != true){
    echo '<script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>';
    echo '<script type="text/javascript" src="resource/scripts/bootstrap.min.js"></script>';
    echo '<link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />';
    echo '<div class="col-md-offset-2 col-md-8 alert alert-danger" role="alert" style="text-align:center;margin-top:200px;">You Are Not Allowed To View This Panel!';
    echo '</div>';
    exit();
  }
  //Permission ---END--

	$objItems                 = new Items();
	$objItemsCategory         = new itemCategory();
	$objBranchStock           = new BranchStock();
	$objDepartments           = new Departments();
	$objMeasures              = new Measures();
	$objMobilePurchaseDetails = new ScanPurchaseDetails();
	$objConfigs               = new Configs();

	if(isset($_POST['search'])){
		$department_id 				 = 1;
		$objItems->name        = mysql_real_escape_string($_POST['item_name']);
		$objItems->itemCatId   = mysql_real_escape_string($_POST['item_cate']);
		$objItems->fromStock   = mysql_real_escape_string($_POST['from_stock']);
		$objItems->toStock     = mysql_real_escape_string($_POST['to_stock']);
		$objItems->inv_type    = mysql_real_escape_string($_POST['inv_type']);

		$searchData       		 = $objItems->searchParent();

		$items_exclude = array();
		if(mysql_num_rows($searchData)){
			while($item_row = mysql_fetch_array($searchData)){
				$items_exclude[] = $item_row['ID'];
			}
		}
	}
	$departmentsList 	= $objDepartments->getList();
	$categoryList 		= $objItemsCategory->getList();
	$sale_scanner 		= $objConfigs->get_config('SALE_SCANNER');
?>
    <!DOCTYPE html>
    

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SIT Solutions</title>
        <link rel="stylesheet" href="resource/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/invalid.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/form.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/tabs.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/reports.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/scrollbar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/font-awesome.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/jquery-ui/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="resource/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/bootstrap-select.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="resource/css/datatable-bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="resource/css/datatable.min.css" type="text/css" />
        <style>
            td {
                font-size: 12px !important;
                text-align: center !important;
                padding: 2px 2px !important;
            }

            th.category-td {
                background-color: #FFF !important;
            }

            th.category-td:hover {
                background-color: #2999ff !important;
                color: #FFF !important;
            }

            .price-text,
            .min-qty-text {
                position: absolute;
                top: 5px;
                left: -1px;
                text-align: center;
            }

            td[data-price-type],
            td[data-min-qty] {
                position: relative;
                text-align: center;
                vertical-align: center;
                cursor: pointer;
            }

            td[data-price-type]:hover,
            td[data-min-qty]:hover {
                background-color: rgba(41, 153, 255, 0.1);
            }
        </style>
        <script type="text/javascript" src="resource/scripts/jquery.1.11.min.js"></script>
        <script src="resource/scripts/jquery-ui.min.js"></script>
        <script src="resource/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="resource/scripts/bootstrap-select.js"></script>

        <script type="text/javascript" src="resource/scripts/configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/item.configuration.js"></script>
        <script type="text/javascript" src="resource/scripts/tab.js"></script>
        <script type="text/javascript" src="resource/scripts/sideBarFunctions.js"></script>
        <script type="text/javascript" src="resource/scripts/datatable.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("select").selectpicker();
                $("table").on('click', "td[data-price-type]", function() {
                    $('.price-text').remove();
                    var $td = $(this);
                    var prevPrice = parseFloat($(this).text()) || 0;
                    var priceType = $(this).attr('data-price-type');
                    var item_id = $(this).attr('data-item-id');
                    $(this).append('<input class="form-control price-text" value="' + prevPrice + '" />');
                    $(this).find('.price-text').focus();
                    $(this).find('.price-text').on('blur change', function() {
                        var $input = $(this);
                        $input.prop('readonly', true);
                        var newPrice = parseFloat($(this).val()) || 0;
                        if (newPrice != prevPrice) {
                            $.post('db/update-item-price.php', {
                                type: priceType,
                                id: item_id,
                                price: newPrice
                            }, function(data) {
                                $input.remove();
                                $td.text(newPrice);
                            });
                        } else {
                            $input.remove();
                        }
                    });
                });
                $("table").on('click', 'td[data-min-qty]', function() {
                    $('.min-qty-text').remove();
                    var $td = $(this);
                    var prevMinQty = parseInt($(this).text()) || 0;
                    var item_id = $(this).attr('data-item-id');
                    $(this).append('<input class="form-control min-qty-text" value="' + prevMinQty + '" />');
                    $(this).find('.min-qty-text').focus();
                    $(this).find('.min-qty-text').on('blur change', function() {
                        var $input = $(this);
                        $input.prop('readonly', true);
                        var newMinQty = parseFloat($(this).val()) || 0;
                        if (newMinQty != prevMinQty) {
                            $.post('db/update-item-min-qty.php', {
                                item_id: item_id,
                                min_qty: newMinQty
                            }, function(data) {
                                $input.remove();
                                $td.text(newMinQty);
                            });
                        } else {
                            $input.remove();
                        }
                    });
                });
            });
        </script>
    </head>

    <body>
        <div id="body-wrapper">
            <div id="sidebar">
                <?php include("common/left_menu.php") ?>
            </div>
            <div class="content-box-top">
	            <div class="content-box-header">
	              <p>Items Management</p>
	              <span id="tabPanel">
	                <div class="tabPanel">
	                    <div class="tabSelected" id="tab1" onClick="tab('1', '1', '2');">List</div>
	                    <a href="items-search.php"><div class="tab">Search</div></a>
	                    <a href="item-details.php"><div class="tab">Add New</div></a>
	                </div>
	          		</span>
	              <div class="clear"></div>
	            </div>
							<!-- End .content-box-header -->
                <div class="content-box-content">
                    <div id="bodyTab1">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th width="5%" rowspan="2" style="text-align:center">Sr#</th>
                                    <th width="10%" rowspan="2" style="text-align:center;">Company</th>
                                    <th width="15%" rowspan="2" style="text-align:center;">Item</th>
                                    <th colspan="3" width="20%" style="text-align:center;">Stock In Hand</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Min/Qty</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Purchase Price</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Disc</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Sale Price</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Cost</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">Active</th>
                                    <th width="5%" rowspan="2" style="text-align:center;">IMEI</th>
                                    <th width="10%" rowspan="2" style="text-align:center;">Action</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center;">Main</th>
                                    <th style="text-align:center;">Godowns</th>
                                    <th style="text-align:center;">Branches</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
							$costa_sum 	  = 0;
							$quantity_sum = 0;
							$categories 	= $objItems->getCategoryList(1);
							if(mysql_num_rows($categories)){
								while($category = mysql_fetch_array($categories)){
										$categoryName  = $objItemsCategory->getDetails($category['ITEM_CATG_ID']);
										$total_cost    = $objItems->sumCategoryTotalCost($category['ITEM_CATG_ID']);
										$total_cost  += $objBranchStock->getItemCategoryCostGodowns($category['ITEM_CATG_ID']);
										$total_cost  += $objBranchStock->getItemCategoryCostBranches($category['ITEM_CATG_ID']);

										$total_stock   = $objItems->sumCategoryTotalStock($category['ITEM_CATG_ID']);
										$total_stock  += $objBranchStock->getItemCategoryStockGodowns($category['ITEM_CATG_ID']);
										$total_stock  += $objBranchStock->getItemCategoryStockBranches($category['ITEM_CATG_ID']);

										$total_stock  += $objMobilePurchaseDetails->get_item_stock_by_category($category['ITEM_CATG_ID']);

										if($total_stock == 0){
											$total_cost = 0;
										}

?>
                                    <tr class="itemCategory" data-cat-id="<?php echo $category['ITEM_CATG_ID']; ?>">
                                        <th colspan="14" class="category-td" style="padding: 5px;">
                                            <span class="toggle_cat pull-right btn btn-default btn-xs ml-10" data-cat-id="<?php echo $category['ITEM_CATG_ID']; ?>"><i class="fa fa-plus"></i></span>
                                            <span class="pull-right" style="padding-left:10px;">Total Cost  : <?php echo number_format($total_cost,2); ?></span>
                                            <span class="pull-right" style="padding-left:10px;">Total Stock : <?php echo $total_stock; ?></span>
                                            <span class="pull-left" style="padding-left:10px;"><?php echo $categoryName['NAME']; ?></span>
                                        </th>
                                    </tr>
                                    <?php
									$costa_sum    += $total_cost;
									$quantity_sum += $total_stock;
								}
							}
?>
                            </tbody>
                            <tfoot>
                                <tr class="high-footer itemCategory">
                                    <th colspan="14">Total Stock :
                                        <?php echo $quantity_sum; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Cost :
                                        <?php echo number_format($costa_sum,2); ?> </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!--End bodyTab1-->
                    <div style="height:0px;clear:both"></div>
                </div>
                <!-- End .content-box-content -->
            </div>
            <!-- End .content-box -->
        </div>
        <!--body-wrapper-->
        <div id="xfade"></div>
        <div id="fade"></div>
    </body>

    </html>
    <?php include('conn.close.php'); ?>
    <script>
        <?php if(isset($_GET['tab'])&&$_GET['tab']=='search'){ ?>
        tab('2', '1', '2');
        <?php } ?>
    </script>
